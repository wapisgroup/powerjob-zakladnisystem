<?php
session_start();
uses('Folder');
class AppController extends Controller
{
    var $components = array('RequestHandler', 'History');
    var $helpers = array('Fastest');
    var $controller_permission = array();

    function beforeRender()
    {
        $this->set('languages', array('cz' => 'Česky', 'en' => 'Anglicky', 'sk' => 'Slovensky'));
        //pr($this->viewVars['logged_user']);
    }

    function beforeFilter()
    {
    
        include "select_config.php";
        if ($this->name == 'CmsExports' ||
            $this->name == 'Crons' ||
            (isset($this->params['no_login_access']) && $this->params['no_login_access'] == true) ||
            (isset($this->params['url']['no_login_access']) && $this->params['url']['no_login_access'] == true)
        ) {

        } else {

            // login session
            if (isset($_SESSION['logged_user']) /*($this->Session->check('logged_user') && $this->Session->read('logged_user'))*/ || isset($_GET['direct'])) {
                $this->logged_user = $_SESSION['logged_user']; //$this->Session->read('logged_user');
                $this->set('logged_user', $this->logged_user);
            } else {
                if ($this->params['action'] != 'login' && $this->params['action'] != 'profesia_export') {
                    $this->layout = 'login';
                    $this->redirect('/login/');
                }
            }

            if (isset($this->renderSetting) && isset($this->logged_user['CmsGroup']['permission'][$this->renderSetting['controller']]))
                $this->set('permission', $this->controller_permission = $this->logged_user['CmsGroup']['permission'][$this->renderSetting['controller']]);

            /*
                * Podminka na company pro koo a cm ve filtraci, aby videli pouze svoje
                */
            if (isset($this->logged_user) && $this->logged_user['CmsGroup']['cms_group_superior_id'] == 2) {
                $this->filtration_company_condition[] = array(
                    'or' => array(
                        'Company.coordinator_id' => $this->logged_user['CmsUser']['id'],
                        'Company.coordinator_id2' => $this->logged_user['CmsUser']['id'],
                        'Company.client_manager_id' => $this->logged_user['CmsUser']['id']
                    )
                );
            }


            if ($this->params['action'] != 'login' && $this->params['action'] != 'logout' && ($this->name == 'CmsGroups' || !$this->RequestHandler->isAjax())) {

                $folder = new Folder();
                if (!defined('MODULS')) {
                    define('MODULS', CONFIGS . 'permission');
                }
                $folder->cd(MODULS);
                $moduly = $folder->ls();
                $permission_list = array();
                $menu_items = array();
                foreach ($moduly[1] as $modul) {
                    include('app/config/permission/' . $modul);
                    if (isset($modul_name))
                        $permission_list[substr($modul, 4, -12)] = array('name' => $modul_name, 'permission' => $modul_permission, 'menu' => $modul_menu);

                    if (isset($modul_menu) && count($modul_menu) > 0)
                        $menu_items[substr($modul, 4, -12)] = $modul_menu;
                }
                $this->set('permission_list', $this->permission_list = $permission_list);

                $menu_out = $menu_top = $per_modul = array();

                if (isset($this->logged_user['CmsGroup']['permission'])) {
                    foreach ($this->logged_user['CmsGroup']['permission'] as $modul => $per) {
                        if (in_array(@$per['index'], array(2, 3))) {
                            $per_modul[] = $modul;
                        }
                    }
                }

                foreach ($per_modul as $modul) {
                    if(isset($menu_items[$modul])) {
                        $fk = array_search($menu_items[$modul]['name'], $menu_top);
                        if ($fk !== false) {
                            if (is_array($menu_items[$modul]['child']))
                                foreach ($menu_items[$modul]['child'] as $submenu) {
                                    $menu_out[$fk]['child'][] = $submenu;
                                }
                        } else {
                            $menu_out[] = $menu_items[$modul];
                            $menu_top[] = $menu_items[$modul]['name'];
                        }
                    }
                }
                $this->set('menu_items', $menu_out);

            }
        }
    }

    function SaveTranslation($data, $parent_id)
    {
        if (isset($data['Translation'])) {
            $this->loadModel('Translation');
            $this->Translation = new Translation();
            foreach ($data['Translation'] as $lang => $models) {
                foreach ($models as $model => $cols) {
                    foreach ($cols as $col => $value) {
                        $this->Translation->query('UPDATE fastest__translations SET history=1 WHERE parent_id=' . $this->$model->id . ' AND modul="' . $model . '" AND col="' . $col . '" AND lang="' . $lang . '"');
                        $save_data = array(
                            'lang' => $lang,
                            'col' => $col,
                            'value' => $value,
                            'history' => 0,
                            'modul' => $model,
                            'parent_id' => $parent_id
                        );
                        $this->Translation->save($save_data);
                        $this->Translation->id = null;
                    }
                }
            }
        }
    }

    function error_log($text)
    {
        $this->log($this->name . '::' . $this->action . '->' . $text . ' (CmsUser:' . $this->logged_user['CmsUser']['name'] . '{ID:' . $this->logged_user['CmsUser']['id'] . '})');
    }


    /**
     * funkce ktera nacte model
     * vrati jeho list a unsetne
     * @param $model - model pro ktery se ma list nacist
     * @param $con - array conditions, am with basic array
     * @param $order - classic order string, default is "name ASC"
     * @return array list
     */
    function get_list($model = null, $con = array(), $order = 'name ASC')
    {
        if ($model != null) {
            $this->loadModel($model);
            return $this->$model->get_list($con, $order);
            unset($this->$model);
        }
        else
            return false;
    }

    //funcke pro emaily predevsim
    function mail_date($datum = null, $format = 'd.m.Y')
    {
        if ($datum == null || $datum == '' || $datum == '0000-00-00')
            return false;
        else
            return date($format, strtotime($datum));
    }

    /**
     * Funkce ktera vraci Sales Managery v zavislosti na group_superior_id
     * @return array list
     */
    function get_sm()
    {
        $sm_group_id = 4;
        $this->loadModel('CmsUser');
        $this->CmsUser->bindModel(array(
            'belongsTo' => array(
                'CmsGroup'
            )
        ));
        return $this->CmsUser->find(
            'list',
            array(
                'fields' => array(
                    'CmsUser.id',
                    'CmsUser.name'
                ),
                'conditions' => array(
                    'CmsGroup.cms_group_superior_id' => $sm_group_id
                ),
                'recursive' => 1
            )
        );
    }

    function get_list_of_superrior($superior_id = null)
    {
        if ($superior_id != null) {
            $this->loadModel('CmsUser');
            $this->CmsUser->bindModel(array('belongsTo' => array('CmsGroup')));
            return $this->CmsUser->find(
                'list',
                array(
                    'fields' => array(
                        'CmsUser.id',
                        'CmsUser.name'
                    ),
                    'conditions' => array(
                        'CmsUser.kos' => 0,
                        'CmsUser.status' => 1,
                        'CmsGroup.cms_group_superior_id' => $superior_id
                    ),
                    'order' => 'CmsUser.name',
                    'recursive' => 1
                )
            );
        }
        else
            return array();
    }

    // vstupnim parametrem je tripismenny kod meny, jejiz kurz chceme zjistit
    function zjistiKurz($mena)
    {
        $soubor_cnb = "http://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.txt";
        $kurzy = file($soubor_cnb);
        foreach ($kurzy as $v) {
            $h = explode("|", $v);
            if ((count($h) >= 5) && ($h[3] == strtoupper($mena))) {
                return str_replace(",", ".", $h[4]);
            }
        }
    }

}

?>