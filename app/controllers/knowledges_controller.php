<?php
class KnowledgesController extends AppController {
	var $name = 'Knowledges';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload');
	var $uses = array('Knowledge');
	var $renderSetting = array(
		'controller'=>'knowledges',
		'SQLfields' => array('id','name','updated','created','file'),
		'page_caption'=>'Knowledge',
		'sortBy'=>'Knowledge.name.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Přidat položku|add',
		//	'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
		//	'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
		//	'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
		//	'Knowledge-status'	=>	'select|Stav|select_stav_zadosti',
		//	'Knowledge-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|Knowledge|id|text|',
			'name'		=>	'Název|Knowledge|name|text|',
			'file'		=>	'Soubor|Knowledge|file|download|attachs_download',
			'updated'	=>	'Upraveno|Knowledge|updated|datetime|',
			'created'	=>	'Vytvořeno|Knowledge|created|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
			'delete'	=>	'trash|Do košiku|trash',			
		),
		'domwin_setting' => array(
			'sizes' 		=> '[700,900]',
			'scrollbars'	=> true,
			'languages'		=> 'false'
		)
	);
	function index(){
		
		$this->set('fastlinks',array('ATEP'=>'/','Knowledge'=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->set('scripts',array('uploader/uploader','clearbox/clearbox'));
			$this->set('styles',array('../js/uploader/uploader','../js/clearbox/clearbox'));
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null)
				$this->data = $this->Knowledge->read(null,$id);
			$this->render('edit');
		} else {
			$this->Knowledge->save($this->data);
			die();
		}
	}
	
	function attachs_download($file,$file_name,$id=null){
		$this->autoLayout = false;
		if($id!=null){
			$fil = $this->Knowledge->find(array('file'=>$id));
			$file = $fil['Knowledge']['file'];
			$file_name = $fil['Knowledge']['name'];
			$pripona = strtolower(end(Explode(".", $file)));
			$filesize = filesize('./uploaded/knowledges/'.$file);
			$cesta = "http://".$_SERVER['SERVER_NAME']."/uploaded/knowledges/".$file;
		} else {
			$pripona = strtolower(end(Explode(".", $file)));
			$file = strtr($file,array("|"=>"/"));
			$filesize = filesize('./uploaded/'.$file);
			$cesta = "http://".$_SERVER['SERVER_NAME']."/uploaded/".$file;
		}
		if ($filesize>0){
			header("Pragma: public"); // požadováno
		    header("Expires: 0");
		    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		    header("Cache-Control: private",false); // požadováno u některých prohlížečů
		    header("Content-Transfer-Encoding: binary");
			header("Content-Length: " . $filesize);
			Header('Content-Type: application/octet-stream');
			Header('Content-Disposition: attachment; filename="'.$file_name.'.'.$pripona.'"');
			readfile($cesta);
		} else {
			echo '<script>alert("Nenalezen soubor")</script>';
		}		
		die();

	}
	
	function upload_attach() {
		$this->Upload->set('data_upload',$_FILES['upload_file']);
		if ($this->Upload->doit(json_decode($this->data['upload']['setting'],true))){
			echo json_encode(array('upload_file'=>array('name'=>$this->Upload->get('outputFilename')),'return'=>true));
		} else 
			echo json_encode(array('return'=>false,'message'=>$this->Upload->get('error_message')));		
		die();
	} 
	
	
}
?>