<?php
Configure::write('debug',1);
class InTrashAuditEstatesController extends AppController {
	var $name = 'InTrashAuditEstates';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('AuditEstate');
	var $renderSetting = array(
		'bindModel'	=> array('belongsTo'=>array('AtCompany')),
		'SQLfields' => '*',
		'SQLcondition' => array(
            'AuditEstate.elimination_date IS NOT NULL',
            
        ),
		'controller'=> 'in_trash_audit_estates',
		'page_caption'=>'Vyřazený majetek',
		'sortBy'=>'AuditEstate.name.ASC',
        'no_trash' => true, 
		'top_action' => array(
			//'add_item'		=>	'Přidat|edit|Pridat majetek|add',
		),
		'filtration' => array(
			'AuditEstate-name'				=>	'text|Jméno|',
			'AuditEstate-at_company_id'	    =>	'select|Firma|company_list',
		 	'AuditEstate-imei_sn_vin'		=>	'text|IMEI/SN/VIN|',
		),
		'items' => array(
			'id'		=>	'ID|AuditEstate|id|text|',
            'aaa'		=>	'aa|AuditEstate|cms_user_id|text|',
			'name'		=>	'Název|AuditEstate|name|text|',
			'company'	=>	'Firma|AtCompany|name|text|',
            'imei'	      =>	'IMEI/SN/VIN|AuditEstate|imei_sn_vin|text|',
			'first_cost'=>	'Pořizovací cena|AuditEstate|first_cost|money2|',
			'payment_date'	=>	'Datum pořízení|AuditEstate|payment_date|date|',
			'hire_price'	=>	'Měs. pronájem|AuditEstate|hire_price|money2|',
			'elimination_date'	=>	'Vyřazeno|AuditEstate|elimination_date|date|',
			'created'	=>	'Vytvořeno|AuditEstate|created|datetime|',
			'updated'	=>	'Změněno|AuditEstate|updated|datetime|'
		),
		'posibility' => array(
			//'history'		=>	'history|Historie přiřazení majetku|history',		
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Evidence majetku'=>'#',$this->renderSetting['page_caption']=>'#'));

            $this->set('company_list',$this->get_list('AtCompany'));

		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}

    /**
     * zobrazeni historie prideleni majetku
     */
     function history ($audit_estate_id = null){
        if($audit_estate_id != null){
            $this->loadModel('ConnectionAuditEstate');
            $this->ConnectionAuditEstate->bindModel(array('belongsTo'=>array('Company','CompanyPerson')));
            $history_list = $this->ConnectionAuditEstate->find('all',array(
                'conditions'=>array(
                    'ConnectionAuditEstate.kos'=>0,
                    'audit_estate_id'=>$audit_estate_id
                ),
                'order'=>'ConnectionAuditEstate.id DESC'
            ));            
            
            $this->set('history_list',$history_list);
            
            //render
            $this->render('../audit_estates/history');
        }
        else
            die('Chybi ID majetku ke kterému chcete zobrazit historii');
        
     }

}
?>