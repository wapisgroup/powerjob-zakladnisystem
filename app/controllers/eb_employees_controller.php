<?php
$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');
@define('CURRENT_YEAR', $_GET['current_year']); 
@define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);

Configure::write('debug',1);
class EbEmployeesController  extends AppController {
	var $name = 'EbEmployees';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('ConnectionEbcomClient');
	var $mesic = CURRENT_MONTH;
	var $rok = CURRENT_YEAR;
	var $renderSetting = array(
		'bindModel'	=> array(
			'belongsTo'	=>	array('Client','EbCompany'),
			'hasOne'	=>	array(
                'EbClientWorkingHour'=>array(
					'conditions' => array(
						'EbClientWorkingHour.rok'=> CURRENT_YEAR,
						'EbClientWorkingHour.mesic'=> CURRENT_MONTH
					)
				)
			)
		),
        'group_by'=>'ConnectionEbcomClient.client_id',
		'controller'	=>	'eb_employees',
		'SQLfields' 	=>	'*',
		'SQLcondition'	=>  array(
			"((DATE_FORMAT(ConnectionEbcomClient.start,'%Y-%m')<= '#CURRENT_YEAR#-#CURRENT_MONTH#'))",
			'((DATE_FORMAT(ConnectionEbcomClient.end,"%Y-%m") >= "#CURRENT_YEAR#-#CURRENT_MONTH#") OR (ConnectionEbcomClient.end = "0000-00-00"))',
			'ConnectionEbcomClient.stav'=> 2
		),
		'page_caption'=>'EB - Docházky zaměstnanců',
		'sortBy'=>'EbCompany.name.ASC',
		'top_action' => array(
			'type_works'		=>	'Typ práce|type_works|Typ práce|type_works',
		),
		'filtration' => array(
            'Client-name'						=>	'text|Klient|',
			'ConnectionEbcomClient-company_id'	=>	'select|Společnost|company_list',
            'GET-current_month'					=>	'select|Měsíc|mesice_list',
			'CompanyOrder-name|'				=>	'text|Zakázka|',	
			'Company-cms_user_id'				=>	'select|Uživatel|cms_user_list',
            'GET-current_year'					=>	'select|Rok|actual_years_list',
        ),
		'items' => array(
			'id'				=>	'ID|Client|id|hidden|',
            'name'			=>	'Investor|EbCompany|name|text|',
            'order_name'			=>	'Zakázka|EbCompany|order_name|text|',
			'client_id'			=>	'Klient ID|Client|id|text|',
			'client'			=>	'Klient|Client|name|text|',
			'stav'				=>	'Stav|EbClientWorkingHour|stav|var|kalkulace_stav_list',
			'formi'				=>	'Hodinovka|EbClientWorkingHour|salary_per_hour|text|',
            'form0'				=>	'Za hodiny|EbClientWorkingHour|salary_per_hour_p|text|',
			'forma'				=>	'Ukolovka|EbClientWorkingHour|salary_per_task|text|',
			'celkem'			=>	'Celkem|EbClientWorkingHour|money_sum|text|',
		),
		'posibility' => array(
			'edit'			=>	'odpracovane_hodiny|Editovat položku|edit',	
		    'potvrdit_uzavreni'	=>	'potvrdit_uzavreni|Potvrdit uzavření|uzavrit_odpracovane_hodiny',	
		),
        'posibility_link'=>array(
            'odpracovane_hodiny'=>'Client.id/#CURRENT_YEAR#/#CURRENT_MONTH#',
            'potvrdit_uzavreni'=>'Client.id/#CURRENT_MONTH#/#CURRENT_YEAR#'
        ),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'	=> true,
			'languages'		=> true,
		)
	);

	

	function beforeRender(){
		parent::beforeRender();

		// úprava nadpisu reportu a přidání do něj datumu
        if(isset($this->viewVars['renderSetting']))
		  $this->set('spec_h1', $this->viewVars['renderSetting']['page_caption'].' za měsíc '.$this->mesice_list[ltrim($this->mesic,'0')].' a rok '.$this->rok);
	}
	
	function index(){
		// set FastLinks
		$this->set('fastlinks',array('ATEP'=>'/','Klienti'=>'#','Docházky zaměstnanců'=>'#'));
		
		$this->loadModel('CmsUser');
		$this->set('cms_user_list',$cm_koo_list = $this->CmsUser->find('list',array('conditions'=>array('kos'=>0,'status'=>1))));
		
		if ($this->RequestHandler->isAjax()){
		//	$this->set('change_list_js', json_encode(array('filtr_ConnectionClientRequirement-company_id'=>$company_list)));
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
		
	}
	
	/**
	 * Nova verze dochazek
     * klientovi se generuje dochazka dynamicky a zadava se 
     * pro vsechny zamestnani na dane zakazky na jednou v danem mesici
     * 
	 * @param int $connection_id [optional]
	 * @param year $rok [optional]
	 * @param int $mesic [optional]
	 * @return 
	 */
	function odpracovane_hodiny($client_id=null, $rok=null, $mesic=null, $inner = null){
		if (empty($this->data)){
			/*
			 * prevod promennych do zakladniho view
			 */ 
			$this->set('client_id',$client_id);
			$this->set('rok',$rok);
			$this->set('mesic',$mesic);
			/*
			 * natahnuti info z DB ke connection
			 */
			$this->data = $this->read_connection($client_id, $rok, $mesic);
			$this->data['EbClientWorkingHour']['client_id'] = $this->data['ConnectionEbcomClient']['client_id'] ;
			$this->data['EbClientWorkingHour']['eb_company_id'] = $this->data['ConnectionEbcomClient']['eb_company_id'] ;
		
			/*
			 * overeni, zda je mozne zobrazit dany mesic dochazky
			 */
			list($start_y, $start_m, $start_d) 	= explode('-', $this->data['ConnectionEbcomClient']['start']);
			list($end_y, $end_m, $end_d) 		= explode('-', $this->data['ConnectionEbcomClient']['end']);
			
			// condition for kill rendering
			$render_element = 0;

			if (( $rok <= date('Y') && $mesic <= date('m') ) || ($mesic > date('m') && $rok < date('Y'))){

				if (($end_y == '0000' && $start_y <= $rok && $start_m <= $mesic) || ($end_y == '0000' && $start_y < $rok && $start_m > $mesic)) {
					$render_element = 1;	
				} else if ($end_y != '0000' && $start_y <= $rok && $start_m <= $mesic && $end_y >= $rok && $end_m >= $mesic){
					$render_element = 1;
				}
			} else {
				$render_element = -1;
			}
			
			/*
			 * Natahnuti listu svatku pro dany rok, mesic a stat
			 */
			$this->loadModel('SettingStatSvatek');
			$this->set('svatky',$s = $this->SettingStatSvatek->find('list', array('conditions'=>array( 'setting_stat_id'	=> current(Set::extract('/EbCompany/stat_id',$this->data)), 'mesic'=> $mesic, 'rok'=> $rok ),'fields'=>array('id','den'),'order'=>'den ASC'))); 				
			unset($this->Company);
			unset($this->SettingStatSvatek);
			
			/*
			 * seznam ukonu dle rozpoctu
			 */
			 $this->loadModel('RozpocetItem');
			 $this->loadModel('TypeWork');
			 $this->set('salary_zaco',$this->TypeWork->find('superlist',array('fields'=>array('id','name','price'),'separator'=>' :: ','conditions'=>array('kos'=>0))));
			 $salary_item_list = $this->RozpocetItem->find('all',array(
                'fields'=>array('id','name','pay_max','eb_company_id'),
                'conditions'=>array(
                    'eb_company_id'=>Set::extract($this->data['connections'],'{n}.ConnectionEbcomClient.eb_company_id'),
                    'kos'=>0
                )
            ));
			 
			 $this->set('salary_item_list',json_encode(Set::combine($salary_item_list,'{n}.RozpocetItem.id','{n}.RozpocetItem','{n}.RozpocetItem.eb_company_id')));
			 unset($this->RozpocetItem);
             
             
             /**
              * Vyhledavame, pokud mame nejake dochazke ve stavu otevreno,
              * uz tedy byli aspon ulozeny muzeme je vsechyn uzavrit
              */
             $this->loadModel('EbClientWorkingHour');
             $count = $this->EbClientWorkingHour->find('count',array(
                  'conditions'=>array('client_id'=>$client_id,'mesic'=>$mesic,'rok'=>$rok,'stav'=>1)
             ));

             $allow_uzavrit = false;
             if($count > 0){$allow_uzavrit = true;}
             $this->set('allow_uzavrit',$allow_uzavrit);
             
             $allow_ulozit = true;
             $count2 = $this->EbClientWorkingHour->find('count',array(
                  'conditions'=>array('client_id'=>$client_id,'mesic'=>$mesic,'rok'=>$rok,'stav'=>2)
             ));

             if($count2 > 0 && $this->logged_user['CmsGroup']['id'] != 1){$allow_ulozit = false;}
             $this->set('allow_ulozit',$allow_ulozit);
            
			/*
			 * render view, pokud inner je null, vykresli jak to ma
			 * pokud neni, tak vykresluj pouze vnitrni cast v zavislosti na tom
			 * zda je to umozneno, tzn. nejedna se o mesic, kdy nepracoval a nejedna se ani o 
			 * budoucnost.
			 */
			if ($inner != null)
				if ($render_element === 1)
					$this->render('element');
				else if ($render_element === -1)
					die('<p>Ty umíš věštit? Jak to kruci víš, kolik toho udělá?</p>');
				else 
					die('<p>Hej!! Tady vubec nic nedelal!!</p>');
			else
				$this->render('index');
		} else {
			$this->loadModel('EbClientWorkingHour');
            $this->loadModel('ConnectionEmployeeUkol');
            if(isset($this->data['connections']) && count($this->data['connections']) > 0){
               
                foreach($this->data['connections'] as $id_con=>$con_data){
                    if($con_data['EbClientWorkingHour']['id'] != '')//UPDATE
                        $this->EbClientWorkingHour->id = $con_data['EbClientWorkingHour']['id'];
                      
                    $con_data['EbClientWorkingHour']['connection_ebcom_client_id'] = $id_con;
                    $save_data = am($this->data['EbClientWorkingHour'],$con_data['EbClientWorkingHour']);
                    $this->EbClientWorkingHour->save($save_data);
                    
                    $exist_ukols = $this->ConnectionEmployeeUkol->find('list',array('conditions'=>array(
                        'eb_client_working_hour_id'=>$this->EbClientWorkingHour->id,
                        'rok'=>$this->data['EbClientWorkingHour']['rok'], 
                        'mesic'=>$this->data['EbClientWorkingHour']['mesic'], 
                        ),
                        'fields'=>array('id','id')
                     ));
                     //smazeme vsechny
                     $this->ConnectionEmployeeUkol->updateAll(
                        array('kos'=>1),
                        array('eb_client_working_hour_id'=>$this->EbClientWorkingHour->id, 
                                'rok'=>$this->data['EbClientWorkingHour']['rok'], 
                                'mesic'=>$this->data['EbClientWorkingHour']['mesic']
                        )
                     );
                    //UKOLY musime ukladat zvlast do DB       
                    if(isset($con_data['ukols'])){
                        foreach($con_data['ukols'] as $id_ukol=>$ukol){
                            $save_ukoly = array(   
                                'client_id'=>$this->data['EbClientWorkingHour']['client_id'],
                                'eb_client_working_hour_id'=>$this->EbClientWorkingHour->id, 
                                'rok'=>$this->data['EbClientWorkingHour']['rok'], 
                                'mesic'=>$this->data['EbClientWorkingHour']['mesic'], 
                                'cena_ks'=>$ukol['cena_ks'],
                                'ks'=>$ukol['ks'], 
                                'cena'=>$ukol['cena'], 
                                'rozpocet_item_id'=>$ukol['name'],
                                'kos'=>0              
                            );
                            if(in_array($id_ukol,$exist_ukols)){$save_ukoly['id'] = $id_ukol;}
                            
                            if($save_ukoly['rozpocet_item_id'] == '' || $save_ukoly['cena_ks'] == '' || $save_ukoly['ks'] == '')//prazdne ukoly se nepridavaji
                             continue;
                            
                            $this->ConnectionEmployeeUkol->save($save_ukoly);
                            $this->ConnectionEmployeeUkol->id = null;
                        }
                    }
                }
                
				die(json_encode(array('result'=>true, 'message' => 'Úspěšně uloženo do databáze.')));
			} else {
				die(json_encode(array('result'=>false, 'message' => 'Chyba: Nepodařilo se uložit do DB.')));
			}
		}
	}
	
	
	/**
	 * vnitrni funkce pro nacteni connection s workinghour
	 * @param object $connection_client_eb_company_id [optional]
	 * @param object $rok [optional]
	 * @param object $mesic [optional]
	 * @return array of model
	 */
	private function read_connection($client_id = null, $rok = null, $mesic = null){
	   $return = array();
       
		// loadmodel && bindModel
		$this->ConnectionEbcomClient->bindModel(array(
			'belongsTo'	=>	array('Client','EbCompany'),
			'hasOne'	=>	array('EbClientWorkingHour' => array('conditions' =>array('EbClientWorkingHour.rok'=>$rok,'EbClientWorkingHour.mesic'=>$mesic))),
		));

		// read from db
		$connections =  $this->ConnectionEbcomClient->find('all',array(
            'conditions'=>array(
                'ConnectionEbcomClient.client_id'=>$client_id,
                "((DATE_FORMAT(ConnectionEbcomClient.start,'%Y-%m')<= '".$rok."-".$mesic."'))",
    			'((DATE_FORMAT(ConnectionEbcomClient.end,"%Y-%m") >= "'.$rok.'-'.$mesic.'") OR (ConnectionEbcomClient.end = "0000-00-00"))',
    			'ConnectionEbcomClient.stav'=> 2
            )
        ));

        if(!$connections)
            die('Nenalezeny žádné connections ke klientovi!');
        
        $return['connections'] = Set::combine($connections,'{n}.ConnectionEbcomClient.id','{n}');
        
        //Nacteni ukolu k jednotlivym connections
        $this->loadModel('ConnectionEmployeeUkol');
        foreach($return['connections'] as $id_conn=>$conn){
                if($conn['EbClientWorkingHour']['id'] != ''){
                	$ukols =  $this->ConnectionEmployeeUkol->find('all',array(
                        'conditions'=>array(
                            'ConnectionEmployeeUkol.client_id'=>$client_id,
                            'ConnectionEmployeeUkol.eb_client_working_hour_id'=>$conn['EbClientWorkingHour']['id'],
                        	'ConnectionEmployeeUkol.mesic'=> $mesic,
                            'ConnectionEmployeeUkol.rok'=> $rok,
                            'ConnectionEmployeeUkol.kos'=>0,
                        )
                    ));
                    $return['connections'][$id_conn]['ukols'] = Set::combine($ukols,'{n}.ConnectionEmployeeUkol.id','{n}.ConnectionEmployeeUkol');
                }
        }
        $return = am($return,$connections[0]);
         
        return $return;    
        
	}
    
    /**
     * Cislenik typ praci pro dochazku
     */
    function type_works(){
        $this->loadModel('TypeWork');    
        $this->set('list_type_works',$this->TypeWork->find('all',array('conditions'=>array('kos'=>0))));
        $this->render('type_works/index');
    }
    
    
    /**
     * Cislenik typ praci pro dochazku
     */
    function edit_type_works($id = null){
        $this->loadModel('TypeWork');
        if(empty($this->data)){
            if($id != null)
                $this->data = $this->TypeWork->read(null,$id);

            $this->render('type_works/edit');
        }
        else{//SAVE
            $this->TypeWork->save($this->data);
            die(json_encode(array('result'=>true, 'message' => 'Úspěšně uloženo do databáze.')));
        }
    }
    
    /**
     * Cislenik typ praci pro dochazku
     */
    function trash_type_works($id = null){
        if($id != null){
            $this->loadModel('TypeWork');
            $this->TypeWork->id = $id;
            $this->TypeWork->saveField('kos',1);
            die(json_encode(array('result'=>true, 'message' => 'Úspěšně smázano z databáze.')));
        }
        else
            die(json_encode(array('result'=>false, 'message' => 'NEznáme ID')));
    }
    
    
    function uzavrit_odpracovane_hodiny($client_id = null, $month = null, $year = null){
		if ($client_id == null){
		  die(json_encode(array('result'=>false,'message'=>'Chyba! Nejsou známé všechny parametry.')));
		}
		
		$this->loadModel('EbClientWorkingHour');
        $this->EbClientWorkingHour->updateAll(
            array('stav'=>2,'closed'=>'"'.date("Y-m-d H:i:s").'"','closed_user'=>$this->logged_user['CmsUser']['id']),
            array('client_id'=>$client_id,'mesic'=>$month,'rok'=>$year,'stav'=>1)
        );
		
		die(json_encode(array('result'=>true)));
	}
    
    function potvrdit_uzavreni($client_id = null, $month = null, $year = null){
        if ($client_id == null){
		  die(json_encode(array('result'=>false,'message'=>'Chyba! Nejsou známé všechny parametry.')));
		}
		
		$this->loadModel('EbClientWorkingHour');
        $this->EbClientWorkingHour->updateAll(
            array('stav'=>3),
            array('client_id'=>$client_id,'mesic'=>$month,'rok'=>$year,'stav'=>2)
        );

        $this->loadModel('Client');
        $client = $this->Client->read('name',$client_id);
        
        
        $replace_list = array(
			'##Client.name##' 	    =>$client['Client']['name'],
			'##mesic##' 	    =>$month,
			'##rok##' 	=>$year,
			'##CmsUser.name##' 			    =>$this->logged_user['CmsUser']['name']
		);
        //0=>$this->logged_user['CmsUser']['email']
        $this->Email->send_from_template_new(16,array(),$replace_list);
     
		
		die(json_encode(array('result'=>true)));
    }
    
}?>