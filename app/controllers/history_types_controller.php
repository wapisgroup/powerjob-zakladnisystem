<?php
Configure::write('debug',1);
/**
 * 
 * @author Jakub Matuš - Fastest Solution s.r.o.
 * @created 12.11.2009
 */

class HistoryTypesController extends AppController {
	var $name = 'HistoryTypes';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','SmsClient');
	var $uses = array('HistoryType');
	var $renderSetting = array(
		'SQLfields' => array('*'),
		'controller'=> 'history_types',
		'page_caption'=>'Historie - typy',
		'sortBy'=>'HistoryType.created.DESC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'	=>	'Přidat|edit|Přidání nového typu historie|add'
		),
		'filtration' => array(
			'HistoryType-cms_user_id'		=>	'select|Uživatel|cms_user_list',
		),
		'items' => array(
			'id'			=>	'ID|HistoryType|id|text|',
			'name'	=>	'Název|HistoryType|name|text|',
			'datum'			=>	'Vytvořeno|HistoryType|created|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace typů historie|edit'		
		)
	);
	
	/**
	 * 
	 * @return view
	 */
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/',$this->renderSetting['page_caption']=>'#'));
	

		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	
	function edit($id = null){
		if (empty($this->data)){
			if ($id != null){
				$this->data = $this->HistoryType->read(null,$id);
			}	
		} else {
			$this->HistoryType->save($this->data);
			die();
		}
	}
	
	
}
?>