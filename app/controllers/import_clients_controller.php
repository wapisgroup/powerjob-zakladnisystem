<?php
//Configure::write('debug',1);
class ImportClientsController extends AppController {
	var $name = 'ImportClients';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload','Email','Clients');
	var $uses = array('ImportClient');
	var $renderSetting = array(
		'bindModel' => array('hasOne' => array(
			'ImportConnectionClientCareerItem' => array('foreignKey' => 'client_id')
		)),
		'SQLfields' => '*,GROUP_CONCAT(ImportConnectionClientCareerItem.setting_career_item_id SEPARATOR "|") as Profese',
		//'SQLfields' => '*',
		'controller'=> 'import_clients',
		'page_caption'=>'Klienti z www stranek',
		//'count_group_by' => 'ImportClient.id',
		'group_by' => 'ImportClient.id',
		'sortBy'=>'ImportClient.id.DESC',
	
		'top_action' => array(
			'export_excel' => 'Export Excel|export_excel|Export Excel|export_excel',   
		),
		'filtration' => array(
			'ImportClient-name|'		=>	'text|Jméno|',
			'ImportClient-save_ip|'		=>	'text|IP|',
			'ImportClient-stav'		=>	'select|Status|stav_client_list',
			'ImportConnectionClientCareerItem-setting_career_item_id'		=>	'select|Profese|profese_list'
		),
		'items' => array(
			'id'		=>	'ID|ImportClient|id|text|',
			'name'		=>	'Jméno|ImportClient|name|text|',
			'profese'	=>	'Profese|0|Profese|text|',
			'mobil1'		=>	'Telefon|ImportClient|mobil1|text|',
			'status'	=>	'Status|ImportClient|stav|var|stav_client_list',
			'status_imp'=>	'StatusImport|ImportClient|import_stav|var|stav_importu_list',
		//	'updated'	=>	'Změněno|ImportClient|updated|datetime|',
			'created'	=>	'Vytvořeno|ImportClient|created|datetime|',
			'save_ip'	=>	'IP|ImportClient|save_ip|text|'
		),
        'class_tr'=>array(
            'class'=>'color_red',
            'model'=>'ImportClient',
            'col'=>'prevod',
            'value'=>1
        ),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
			'auth'	=>	'auth|Autorizovat|auth',
			'overeni'	=>	'overeni|Ověření klienta|overeni',
			'delete'	=>	'trash|Odstranit položku|trash'			
		),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'	=> true,
			'languages'		=> true,
		)
	);
	
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/',$this->renderSetting['page_caption']=>'#'));
		
		
		$this->loadModel('SettingCareerItem');
		$this->SettingCareerItem->query("SET NAMES 'utf8'");
	
		$this->set('profese_list', $profese_list = $this->SettingCareerItem->find('list',
			array('conditions'=>array('kos'=>0), 'order'=>'name ASC')
		));

		foreach($this->viewVars['items'] as &$item){
			if($item[0]['Profese'] != ''){
				$profese = explode('|',$item[0]['Profese']);
				$tmp = '';
				foreach($profese as $prof)
					$tmp .= $profese_list[$prof].'<br />';
					
				$item[0]['Profese'] = $tmp;
			}
		}
				
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->set('scripts',array('uploader/uploader'));
			$this->set('styles',array('../js/uploader/uploader'));
			$this->render('../system/index');
		}
	}
	
	
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			$this->set('admin', $this->logged_user['CmsGroup']['id']);
		
			// nacteni kvalifikace
			$this->loadModel('SettingCareerItem');
			$this->SettingCareerItem->query("SET NAMES 'utf8'");
			$this->set('kvalifikace_list', $this->SettingCareerItem->find('list', array('order'=>'name')));
			unset($this->SettingCareerItem);
			
             // nacteni certifikatu
            $this->loadModel('SettingCertificate');
            $this->set('certifikaty_list', $this->SettingCertificate->find('list', array('order' =>
                'name')));
            unset($this->SettingCertificate);

            
			// load stat list
			$this->loadModel('SettingStat'); 
			$this->SettingStat->query("SET NAMES 'utf8'");
			$this->set('client_stat_list',$this->SettingStat->find('list',array('conditions'=>array('SettingStat.status'=>1,'SettingStat.kos'=>0))));
			unset($this->SettingStat);
			
            // load kraj list
			$this->loadModel('Province'); 
			$this->Province->query("SET NAMES 'utf8'");
			$this->set('client_kraj_list',$this->Province->find('list',array('conditions'=>array())));
			unset($this->Province);
			
            
			// nacteni vzdelani
			$this->loadModel('SettingEducation');
			$this->SettingEducation->query("SET NAMES 'utf8'");
			$this->set('dosazene_vzdelani_list', $this->SettingEducation->find('list', array('order'=>'name')));
			unset($this->SettingEducation);


			$temp_stat_id = 1; //defaultně načti okresy z ČR
			
			if ($id != null){
				$this->data = $this->ImportClient->read(null,$id);
			//	pr($this->data);
				$temp_stat_id = $this->data['ImportClient']['stat_id'];

				// nacteni modelu kvalifikace
				$this->loadModel('ImportConnectionClientCareerItem');
				// nacteni ulozenych pozic k danemu klientovi
				$this->set(
					'kvalifikace_list_item',
					$kval = 
					$this->ImportConnectionClientCareerItem->find(
						'all', 
						array(
							'conditions'=>array('client_id'=>$id),
							'fields'=>array(
                                    'ImportConnectionClientCareerItem.name',
                                    'ImportConnectionClientCareerItem.popis',
                                    'ImportConnectionClientCareerItem.od',
                                    'ImportConnectionClientCareerItem.do',
                                    'ImportConnectionClientCareerItem.id',
                                    'ImportConnectionClientCareerItem.setting_career_item_id'
                            ),
							'recursive'=>1
						)
					)
				);
                
                   // nacteni modelu kvalifikace
                $this->loadModel('ImportConnectionClientCertifikatyItem');
                // nacteni ulozenych pozic k danemu klientovi
                $this->set('certifikaty_list_item', $cert = $this->
                    ImportConnectionClientCertifikatyItem->find('all', array('conditions' => array('client_id' =>
                    $id))));


                  /**
                   * pokud jsou nastaveny request data
                   * nastav componentu
                   * vyvolej request data, zde se neposilaji pres request action tak je jen nastav
                   */
                   $this->Clients->set('duplicity_www',true);
                   $this->Clients->set_request_data(array('duplicity_domwin'=>true));
				
			}

			// load okresy list
			$this->loadModel('Countrie');
			$this->Countrie->query("SET NAMES 'utf8'");
			//$this->set('client_countries_list',null);
			$this->Countrie->bindModel(array('belongsTo'=>array('Province'=>array('foreignKey'=>'province_id'))));
			$this->set('client_countries_list',$cc = $this->Countrie->find('list',array(
				'conditions'=>array(
					'Countrie.status'=>1,
					'Countrie.kos'=>0,
					'Province.stat_id'=>$temp_stat_id
					
				), 'recursive'=>1
			)));
			unset($this->Countrie);
            
            
            /**
             * nove nastaveni prommenych pro view z komponenty client
             */
            $this->Clients->set_variables_for_view();
							
			$this->render('edit');
		} else {
		  //pr($this->data);
          
			$this->ImportClient->save($this->data);		
			
			$this->loadModel('ImportConnectionClientCareerItem');
		
        
			// smazani starych propojeni
			$this->ImportConnectionClientCareerItem->deleteAll(array('client_id'=>$this->ImportClient->id));
			if (isset($this->data['ClientCarrerItems'])){
				foreach($this->data['ClientCarrerItems'] as $career_name){
					$this->ImportConnectionClientCareerItem->save(
						array('ImportConnectionClientCareerItem'=>array(
						'name' => $career_name['name'],
						'popis' => $career_name['popis'],
						'od' => $career_name['od'],
						'do' => $career_name['do'],
						'client_id' => $this->ImportClient->id,
						'setting_career_item_id' => $career_name['setting_career_item_id'],
					
					)));
					$this->ImportConnectionClientCareerItem->id = null;
				}
			}	
            
            
            
             $this->loadModel('ImportConnectionClientCertifikatyItem');

            // smazani starych propojeni
            $this->ImportConnectionClientCertifikatyItem->deleteAll(array('client_id' => $this->
                ImportClient->id));


            if (isset($this->data['CertifikatyItem']))
            {
                foreach ($this->data['CertifikatyItem'] as $cert_name)
                {
                    $this->ImportConnectionClientCertifikatyItem->save(array('ImportConnectionClientCertifikatyItem' =>
                        array('komentar' => $cert_name['komentar'], 'platnost' => $cert_name['platnost'],
                        'client_id' => $this->ImportClient->id, 'setting_certificate_id' => $cert_name['list'], )));

                    //$papa[] = $cert_name['list'];
                    $this->ImportConnectionClientCertifikatyItem->id = null;
                    $this->ImportConnectionClientCertifikatyItem->data = null;
                }
            }
			die();
		}
	}
	
	
	//funkce prevadi klienty z at_web na klienty ATEPu
	function auth($client_id = null){
		if($client_id != null){
			// ulozeni do db Clientu pro ATEP
			$to_save = $this->ImportClient->read(null,$client_id);

			if(!$to_save)
				die(json_encode(false));
			
			$to_save['Client'] = $to_save['ImportClient'];
			$to_save['Client']['id'] = null; // vynulovani id -> dostane nove
			$to_save['Client']['prevod_date'] = date('Y-m-d H:i:s'); 

			$this->loadModel('Client');
			$this->Client->query("SET NAMES 'utf8'");
			$this->Client->save($to_save);
			
			//ulozeni connection profesi s novym klientem
			$this->loadModel('ImportConnectionClientCareerItem');
			$cc = $this->ImportConnectionClientCareerItem->find('all',array(
				'conditions'=>array(
					'client_id'=>$client_id
				)
			));
			
			if($cc){
				$this->loadModel('ConnectionClientCareerItem');
				foreach($cc as $career_item){
					$this->ConnectionClientCareerItem->id = null;
					$save_cc = array('ConnectionClientCareerItem'=>array(
							'name' => $career_item['ImportConnectionClientCareerItem']['name'],
							'popis' => $career_item['ImportConnectionClientCareerItem']['popis'],
							'od' => $career_item['ImportConnectionClientCareerItem']['od'],
							'do' => $career_item['ImportConnectionClientCareerItem']['do'],
							'client_id' => $this->Client->id,
							'setting_career_item_id' => $career_item['ImportConnectionClientCareerItem']['setting_career_item_id']
					));
					$this->ConnectionClientCareerItem->save($save_cc);
				}
			}
			
            
            //ulozeni connection certifikatu s novym klientem
			$this->loadModel('ImportConnectionClientCertifikatyItem');
			$client_certifikaty = $this->ImportConnectionClientCertifikatyItem->find('all',array(
				'conditions'=>array(
					'client_id'=>$client_id
				)
			));
			
			if($client_certifikaty){
				$this->loadModel('ConnectionClientCertifikatyItem');
				foreach($client_certifikaty as $certifikaty_item){
					$this->ConnectionClientCertifikatyItem->id = null;
					$save_cc = array('ConnectionClientCertifikatyItem'=>array(
							'platnost' => $certifikaty_item['ImportConnectionClientCertifikatyItem']['platnost'],
							'komentar' => $certifikaty_item['ImportConnectionClientCertifikatyItem']['komentar'],
							'client_id' => $this->Client->id,
							'setting_certificate_id' => $certifikaty_item['ImportConnectionClientCertifikatyItem']['setting_certificate_id']
					));
					$this->ConnectionClientCertifikatyItem->save($save_cc);
				}
			}
			
            
			
			//prideleni recruitera, ten kdo ho autorizoval
			$this->loadModel('ConnectionClientRecruiter');
			$rec_save["client_id"] = $this->Client->id;
			$rec_save["cms_user_id"] = $this->logged_user['CmsUser']['id'];
			$this->ConnectionClientRecruiter->save($rec_save);
			
			// i interni nabor
			$this->ConnectionClientRecruiter->id = null;
			$rec_save["client_id"] = $this->Client->id;
			$rec_save["cms_user_id"] = -1;
			$this->ConnectionClientRecruiter->save($rec_save);
			unset($this->ConnectionClientRecruiter);
		
			
			//ulozeni import klientu - pridany kos, uz ho nepotrebujeme ale uchovavame
			$this->ImportClient->id = $client_id;
			$this->ImportClient->saveField('prevod',1);	
			
			die(json_encode(true));
		}
		else
			die(json_encode(false));
	}
    
    
    
    /**
     * funkce pro overeni klienta v db, zda jiz neexistuje
     * popripade vypsat ktery jsou stejni
     */
    function overeni ($import_client_id){
        $data = $this->ImportClient->read(array('mobil1','datum_narozeni'),$import_client_id);
        
        if(!$data)
            die(json_encode(array('result' => false)));
        
        $result = array();
        $this->loadModel('Client');
        $this->Client->query("SET NAMES 'utf8'");
		
		$result = $this->Client->find('list',array(
            'conditions'=>array(
                'AND'=>array( 
                    'OR'=>array(
                        'datum_narozeni'=>$data['ImportClient']['datum_narozeni'],
                        'mobil1'=>$data['ImportClient']['mobil1'],
                        'mobil2'=>$data['ImportClient']['mobil1'],
                        'mobil3'=>$data['ImportClient']['mobil1'],
                    ),
                    'kos'=>0
                )
            ),
        ));
        
        if(!$result)
            $result = 0;
        
        die(json_encode(array('result' => true, 'data' => $result)));
    }
	

     /**
     * funkce pro vytvoreni exportu do excelu - CSV
     * podle filtrace vyber dane klienty a vygeneruj je do csv
     */
    function export_excel(){
        Configure::write('debug',1);
        
       $start = microtime ();
       $start = explode ( " " , $start );
       $start = $start [ 1 ]+ $start [ 0 ];  

         $fields_sql = '*,GROUP_CONCAT(ImportConnectionClientCareerItem.setting_career_item_id SEPARATOR "|") as Profese';
         
               
        $criteria = $this->ViewIndex->filtration();        
      
    
         header("Pragma: public"); // požadováno
         header("Expires: 0");
         header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
         header("Cache-Control: private",false); // požadováno u některých prohlížečů
         header("Content-Transfer-Encoding: binary");
         Header('Content-Type: application/octet-stream');
         Header('Content-Disposition: attachment; filename="'.date('Ymd_His').'.csv"');
     
      
        /*
         * Celkovy pocet zaznamu
         */
         
        $limit = 100;   
        $page = 1;     
        $this->ImportClient->bindModel($this->renderSetting['bindModel'],false);   
        $count = $this->ImportClient->find('first',
        	array(
        		'fields' =>  array("COUNT(DISTINCT ImportClient.id) as count"),
        		'conditions'=>$criteria,
        		'recursive'	=>1
        	)
        );    
        $count = $count[0]['count'];
        $this->renderSetting['items'] = am($this->renderSetting['items'],array(
            'email'=>'Email|ImportClient|email|text|',
            //'private_email'=>'Soukromý Email|ClientView|private_email|text|'
        )); 

        // hlavicka
        foreach($this->renderSetting['items'] as &$item_setting){
            list($caption, $model, $col, $type, $fnc) = explode('|',$item_setting);
        	$item_setting = compact(array("caption", "model","col","type","fnc"));	
            if($type != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',$caption).'";'; 
        }
        echo "\n";   
        unset($item_setting, $caption, $model, $col, $type, $fnc);
        
        $str_array=array("<br/>"=>', ',':'=>',',';'=>',','?'=>'', '#'=>' ');
        /*
         * Cyklicky vypis dat po $limit zaznamu
         */
        
        for ($exported = 0; $exported < $count; $exported += $limit){
            $this->ImportClient->bindModel($this->renderSetting['bindModel'],false);
            foreach($this->ImportClient->find('all',array(
                   'fields'=>$fields_sql,
                    'conditions'=>$criteria,
                    'group'=>$this->renderSetting['group_by'],
                    'limit'=>$limit,
                    'page'=>$page,
                    'recursive'=>1,
                    'order'=>'ImportClient.id DESC'
            )) as $item){
                foreach($this->renderSetting['items'] as $key => $td){
                    if($td['type'] != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',strtr($this->ViewIndex->generate_td($item,$td,$this->viewVars),$str_array)).'";';     
                }
                echo "\n";  
            }
            $page++;
        }
       
        //time
         $end = microtime ();
         $end = explode ( " " , $end );
         $end = $end [ 1 ]+ $end [ 0 ]; 
         echo 'Generate in '.($end - $start).'s';
         echo "\n"; 
        
        die();
    }
}
?>