<?php
//Configure::write('debug',1);
class AccommodationCmsUsersController  extends AppController {
	var $name = 'AccommodationCmsUsers';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('ConnectionAccommodationUser');
	var $renderSetting = array(
		'bindModel'	=> array(
			'belongsTo'	=>	array('Accommodation','CmsUser')
		),
		'controller'	=>	'accommodation_cms_users',
		'SQLfields' 	=>	'*',
		'page_caption'=>'Report přiřazených přístupu',
		'sortBy'=>'Accommodation.name.ASC',
		'top_action' => array(),
		'filtration' => array(
			'ConnectionAccommodationUser-accommodation_id'	=>	'select|Ubytovna|accommodation_list',   
            'ConnectionAccommodationUser-cms_user_id'		=>	'select|CM/KOO|cm_koo_list'
        ),
		'items' => array(
			'id'				=>	'ID|ConnectionAccommodationUser|id|hidden|',
			'accommodation'		=>	'Ubytovna|Accommodation|name|text|',
			'city'		=>	'Město|Accommodation|city|text|',
			'cms_user'			=>	'COO/CM|CmsUser|name|text|',
			'created'			=>	'Vytvořeno|ConnectionAccommodationUser|created|datetime|'
		),
		'posibility' => array(
		),
        'no_trash'=>true
	);

	

	
	function index(){
		
		$this->loadModel('CmsUser');
		$this->set('cm_koo_list',$cm_koo_list = $this->CmsUser->find('list',array(
			'conditions'=>array(
				'kos'=>0,
				'status'=>1,
				'cms_group_id IN(3,4)'
			)
		)));
		
		$this->loadModel('Accommodation');
		$accommodation_list = $this->Accommodation->find('list',array(
			 'conditions'=> array('kos'=>0),
			 'order'=>'name ASC'
		 ));
		 $this->set('accommodation_list',$accommodation_list);
	 
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
		
	}
	
}    
?>