<?php Configure::write('debug', 1);
class AtProjectsController extends AppController
{
    var $name = 'AtProjects';
    var $helpers = array('htmlExt', 'Pagination', 'ViewIndex');
    var $components = array('ViewIndex', 'RequestHandler', 'IntEmployee');
    var $uses = array('AtProject');
    var $renderSetting = array(
        'controller' => 'at_projects',
        'SQLfields' => '*',
        'page_caption' => 'Nastavení projektu, středisk, pozic',
        'sortBy' => 'AtProject.created.ASC',
        'top_action' => array(
            // caption|url|description|permission
            'add_item' => 'Přidat|edit|Pridat AtProject|add',
            'money_item' => 'Forma zaměstnání|money_item|Forma zaměstnání|money_item',
        ),
        'filtration' => array(
            //	'SettingAccommodationType-status'	=>	'select|Stav|select_stav_zadosti',
            //	'SettingAccommodationType-name'		=>	'text|jmeno|'
        ),
        'items' => array(
            'id' => 'ID|AtProject|id|text|',
            'name' => 'Název|AtProject|name|text|',
            'updated' => 'Upraveno|AtProject|updated|datetime|',
            'created' => 'Vytvořeno|AtProject|created|datetime|'
        ),
        'posibility' => array(
            'edit' => 'edit|Editace položky|edit',
            'office_manager' => 'office_manager|Office manager projektu|office_manager',
            'project_spravce_majetku' => 'project_spravce_majetku|Správce majetku projektu|project_spravce_majetku',
            'project_money_items' => 'project_money_items|Nastavení forem odměny|project_money_items',
            'project_info_mails' => 'project_info_mails|Příjemci emailu o int. zaměstancích|project_info_mails',
            'delete' => 'trash|Do košiku|trash'
        ),
        'only_his' => array(
            'all_set' => true,
            'in_col' => 'id',
            'col' => 'id',
            'table' => 'wapis__at_projects',
            'where_col' => 'manager_id',
        ),
        'domwin_setting' => array(
            'sizes' => '[800,800]',
            'scrollbars' => true,
            'languages' => true,
        )
    );

    function index()
    {
        $this->set('fastlinks', array('ATEP' => '/', 'Administrace' => '#', 'Nastavení projektu' => '#'));
        if ($this->RequestHandler->isAjax()) {
            $this->render('../system/items');
        } else {
            $this->render('../system/index');
        }
    }

    function edit($id = null)
    {
        $this->autoLayout = false;
        if (empty($this->data)) {
            if ($id != null)
                $this->data = $this->AtProject->read(null, $id);

            $this->set('manager_list', $this->get_list('CmsUser', array('kos' => 0, 'status' => 1)));

            $this->loadModel('AtProjectCentre');
            $this->set('centre_list', $this->AtProjectCentre->find('all', array(
                'conditions' => array('at_project_id' => $id, 'kos' => 0)
            )));

            $this->set('centre_list_all', $this->AtProjectCentre->find('list', array(
                'conditions' => array('kos' => 0)
            )));

            $this->set('project_id', $id);
            $this->render('edit');
        } else {
            $this->AtProject->save($this->data);
            die();
        }
    }

    function project_spravce_majetku($id = null)
    {
        $this->autoLayout = false;
        if (empty($this->data)) {
            if ($id != null)
                $this->data = $this->AtProject->read(null, $id);
            $this->set('manager_list', $this->get_list('CmsUser', array('kos' => 0, 'status' => 1)));
        } else {
            $this->AtProject->save($this->data);
            die();
        }
    }

    function office_manager($id = null)
    {
        $this->autoLayout = false;
        if (empty($this->data)) {
            if ($id != null)
                $this->data = $this->AtProject->read(null, $id);
            $this->set('manager_list', $this->get_list('CmsUser', array('kos' => 0, 'status' => 1)));
        } else {
            $this->AtProject->save($this->data);
            die();
        }
    }


    function work_position_edit($centre_id = null, $id = null)
    {
        $this->loadModel('AtProjectCentreWorkPosition');
        if (empty($this->data)) {
            if ($id != null) {
                $this->data = $this->AtProjectCentreWorkPosition->read(null, $id);
                $this->data['old_group_id'] = $this->data['AtProjectCentreWorkPosition']['cms_group_id'];
            }
            else {
                $this->data['AtProjectCentreWorkPosition']['at_project_centre_id'] = $centre_id;
            }

            $this->set('cms_group_list', $this->get_list('CmsGroup', array('kos' => 0, 'status' => 1)));
            $this->render('centre/work_position/edit');
        }
        else {
            if ($this->data['AtProjectCentreWorkPosition']['id'] == '')
                $this->data['AtProjectCentreWorkPosition']['cms_user_id'] = $this->logged_user['CmsUser']['id'];

            /**
             * Pokud se zmenila uzivatelska skupina
             * Je nutne vsechny takove int. zam a jejich ucty zmenit, dle profese na kterou jsou zamestnani!
             */
            if ($this->data['old_group_id'] != '' && $this->data['old_group_id'] != $this->data['AtProjectCentreWorkPosition']['cms_group_id']) {
                $this->IntEmployee->change_cms_group_for_employee($this->data['AtProjectCentreWorkPosition']);
            }

            $this->AtProjectCentreWorkPosition->save($this->data);
            die();
        }
    }

    function work_position_trash($id = null)
    {
        $this->loadModel('AtProjectCentreWorkPosition');
        $this->AtProjectCentreWorkPosition->id = $id;
        $this->AtProjectCentreWorkPosition->saveField('kos', 1);
        die();
    }


    function centre_edit($project_id = null, $id = null)
    {
        $this->loadModel('AtProjectCentre');
        if (empty($this->data)) {
            if ($id != null) {
                $this->data = $this->AtProjectCentre->read(null, $id);

                $this->loadModel('AtProjectCentreWorkPosition');
                $this->set('profesion_list', $this->AtProjectCentreWorkPosition->find('all', array(
                    'conditions' => array('at_project_centre_id' => $id, 'kos' => 0)
                )));

                $this->set('center_list', $this->AtProjectCentre->find('list', array('conditions' => array('kos' => 0, 'type_id' => 1), 'fields' => array('id', 'name'))));
            }
            else {
                $this->data['AtProjectCentre']['at_project_id'] = $project_id;
            }

            $this->set('cms_group_list', $this->get_list('CmsGroup', array('kos' => 0, 'status' => 1)));
            $this->set('spravce_list', $this->get_list('CmsUser', array('kos' => 0, 'status' => 1)));
            $this->set('centre_id', $id);
            $this->render('centre/edit');
        }
        else {
            if ($this->data['AtProjectCentre']['id'] == '')
                $this->data['AtProjectCentre']['cms_user_id'] = $this->logged_user['CmsUser']['id'];

            $this->AtProjectCentre->save($this->data);
            die();
        }
    }

    function centre_trash($id = null)
    {
        $this->loadModel('AtProjectCentre');
        $this->AtProjectCentre->id = $id;
        $this->AtProjectCentre->saveField('kos', 1);
        die();
    }

    /**
     * cislenik formy zamestani pro interni zamestnance
     */
    public function money_item()
    {
        $this->loadModel('AtCompanyMoneyItem');
        $this->set('money_item_list', $this->AtCompanyMoneyItem->find('all', array(
            'conditions' => array('kos' => 0)
        )));

        $this->render('money_item/index');
    }

    function money_item_edit($id = null)
    {
        $this->loadModel('AtCompanyMoneyItem');
        if (empty($this->data)) {
            if ($id != null) {
                $this->data = $this->AtCompanyMoneyItem->read(null, $id);
            }

            $this->render('money_item/edit');
        }
        else {
            if ($this->data['AtCompanyMoneyItem']['id'] == '')
                $this->data['AtCompanyMoneyItem']['cms_user_id'] = $this->logged_user['CmsUser']['id'];

            $this->AtCompanyMoneyItem->save($this->data);
            die();
        }
    }

    function money_item_trash($id = null)
    {
        $this->loadModel('AtCompanyMoneyItem');
        $this->AtCompanyMoneyItem->id = $id;
        $this->AtCompanyMoneyItem->saveField('kos', 1);
        die();
    }


    function get_project_id($project_centre_id = null)
    {
        if ($project_centre_id != null) {
            $this->loadModel('AtProjectCentre');
            $project = $this->AtProjectCentre->read(array('at_project_id'), $project_centre_id);
            die(json_encode(array('result' => true, 'at_project_id' => $project['AtProjectCentre']['at_project_id'])));
        }
        else
            die(json_encode(array('result' => false, 'message' => 'Chyba, ID firma je prazdne')));
    }

    function project_money_items($project_id = null)
    {
        $this->loadModel('AtProjectMoneyItem');

        if (empty($this->data)) {
            $money_items = $this->AtProjectMoneyItem->find('all', array(
                'conditions' => array(
                    'kos' => 0,
                    'at_project_id' => $project_id
                )
            ));

            $items = array(
                'stravenky' => array('type' => 'checkbox', 'label' => 'Stravenky'),
                'setting_shift_working_id' => array('type' => 'selectTag', 'select' => $this->smennost_list, 'label' => 'Směnnost'),
                'standard_hours' => array('type' => 'input', 'label' => 'Normo hodina'),
                'default_salary_fix' => array('max' => true, 'type' => 'input', 'label' => 'Hruba mzda'),
                'default_food_tickets' => array('max' => true, 'type' => 'input', 'label' => 'Stravné'),
                'default_odmena_1' => array('max' => true, 'type' => 'input', 'label' => 'Odměna 1'),
                'default_odmena_2' => array('max' => true, 'type' => 'input', 'label' => 'Odměna 2'),
                'default_odmena_3' => array('max' => true, 'type' => 'input', 'label' => 'Odměna 3'),

            );

            /**
             * Jelikoz nastavujeme vice radku najednou v DB
             * je treba pretransformovat nactene data do this->data
             */
            foreach ($money_items as $mi) {
                foreach ($items as $item => $item_setting) {
                    $this->data['AtProjectMoneyItem']['setting'][$mi['AtProjectMoneyItem']['at_company_money_item_id']][$item] = $mi['AtProjectMoneyItem'][$item];
                    if (isset($item_setting['max']))
                        $this->data['AtProjectMoneyItem']['setting'][$mi['AtProjectMoneyItem']['at_company_money_item_id']][$item . '_max'] = $mi['AtProjectMoneyItem'][$item . '_max'];
                }
            }


            $this->data['AtProjectMoneyItem']['at_project_id'] = $project_id;
            $this->set('forma_zamestani', $this->get_list('AtCompanyMoneyItem'));
            $this->set('items_setting', $items);
        }
        else { //save
            /**
             * Ulozeni postupne nastaveni vsech forem odmeny
             */
            if (isset($this->data['AtProjectMoneyItem']['setting'])) {

                $formy = $this->get_list('AtCompanyMoneyItem');
                foreach ($formy as $money_id => $money_name) {
                    $save_arr = array(
                        'at_project_id' => $this->data['AtProjectMoneyItem']['at_project_id'],
                        'at_company_money_item_id' => $money_id,
                    );
                    if (isset($this->data['AtProjectMoneyItem']['setting'][$money_id])) {
                        foreach ($this->data['AtProjectMoneyItem']['setting'][$money_id] as $col => $val) {
                            $save_arr[$col] = $val;
                        }

                        $exist_id = $this->AtProjectMoneyItem->find('first', array(
                            'conditions' => array(
                                'kos' => 0,
                                'at_project_id' => $this->data['AtProjectMoneyItem']['at_project_id'],
                                'at_company_money_item_id' => $money_id
                            )
                        ));
                        if ($exist_id) {
                            $this->AtProjectMoneyItem->id = $exist_id['AtProjectMoneyItem']['id'];
                        }

                        $this->AtProjectMoneyItem->save($save_arr);
                        $this->AtProjectMoneyItem->id = null;
                    }
                }

            }

            die(json_encode(array('result' => true)));
        }
    }

    public function project_info_mails($project_id)
    {
        $this->autoLayout = false;
        if (empty($this->data)) {
            if ($project_id != null)
                $this->data = $this->AtProject->read(null, $project_id);

            $this->set('users_list', $this->get_list('CmsUser', array('kos' => 0, 'status' => 1)));
        } else {
            $this->AtProject->save($this->data);
            die();
        }
    }
}

?>