<?php
Configure::write('debug', 1);
define('fields', 'AuditEstate.*,AtCompany.name, AtProjectCentre.spravce_majetku_id, SpravceMajetku.name,
        IF((
           (AuditEstate.first_cost/(SELECT SUM(price) FROM wapis__audit_estate_repairs Where audit_estate_id=AuditEstate.id AND kos=0)) <= 5
        ),1,0) as repair_high, 
        ( 
		  if(AuditEstate.stav = 1,
            IF(ConnectionAuditEstate.type = 1,(GROUP_CONCAT(Client.name SEPARATOR "<br />")),(GROUP_CONCAT(ClientForeign.name SEPARATOR "<br />"))),
            SpravceMajetku.name
          )  
		) as osoba,	
        ( 
		  if(AuditEstate.stav = 1,
             IF(ConnectionAuditEstate.connection_client_at_company_work_position_id IS NULL,
              (GROUP_CONCAT(ConnectionAuditEstate.centre SEPARATOR "<br />")),
              (SELECT ccat_c.name FROM wapis__connection_client_at_company_work_positions as ccat
                LEFT JOIN wapis__at_project_centres ccat_c ON (ccat_c.id = ccat.at_project_centre_id)
                WHERE ccat.id=ConnectionAuditEstate.connection_client_at_company_work_position_id AND ccat.kos=0
              )
             ),
             AtProjectCentre.name
          )  
		) as stredisko,
        ( 
		  if(AuditEstate.stav = 1,
            (GROUP_CONCAT(ConnectionAuditEstate.description SEPARATOR "<br />")),
            AuditEstate.add_info
          )  
		) as dopl_info,
        ( 
          CONCAT_WS(" ",AuditEstate.first_cost,if(AuditEstate.currency = 0,",- EUR",",- Kč"))  
		) as first_cost	,
        ( 
          CONCAT_WS(" ",IFNULL(AuditEstate.hire_price,0),if(AuditEstate.currency = 0,",- EUR",",- Kč"))  
		) as hire_price	
	');
class AuditEstatesController extends AppController
{
    var $name = 'AuditEstates';
    var $helpers = array('htmlExt', 'Pagination', 'ViewIndex');
    var $components = array('ViewIndex', 'RequestHandler', 'Email');
    var $uses = array('AuditEstate');
    var $renderSetting = array(
        'bindModel' => array(
            'belongsTo' => array('AtCompany', 'AtProjectCentre'),
            'hasOne' => array(
                'ConnectionAuditEstate' => array(
                    'conditions' => array(
                        'ConnectionAuditEstate.kos' => 0,
                        'ConnectionAuditEstate.to_date' => '0000-00-00'
                    )
                )
            ),
            'joinSpec' => array(
                'ClientZam' => array(
                    'className' => 'ConnectionClientAtCompanyWorkPosition',
                    'primaryKey' => 'ClientZam.client_id',
                    'foreignKey' => 'ConnectionAuditEstate.client_id',
                    'conditions' => array(
                        'ClientZam.kos' => 0,
                        'ClientZam.datum_to' => '0000-00-00'
                    )
                ),
                'Client' => array(
                    'className' => 'Client',
                    'primaryKey' => 'Client.id',
                    'foreignKey' => 'ConnectionAuditEstate.client_id'
                ),
                'ClientForeign' => array(
                    'className' => 'ClientForeign',
                    'primaryKey' => 'ClientForeign.id',
                    'foreignKey' => 'ConnectionAuditEstate.client_foreign_id'
                ),
                'SpravceMajetku' => array(
                    'className' => 'CmsUser',
                    'primaryKey' => 'SpravceMajetku.id',
                    'foreignKey' => 'AtProjectCentre.spravce_majetku_id'
                )
            )
        ),
        'SQLfields' => array(fields),
        'SQLcondition' => array(
            'AuditEstate.elimination_date IS NULL',
        ),
        'controller' => 'audit_estates',
        'page_caption' => 'Evidence majetku',
        'sortBy' => 'AuditEstate.id.DESC',
        'group_by' => 'AuditEstate.id',
        'top_action' => array(
            'add_item' => 'Přidat|edit|Pridat majetek|add',
            'export_excel' => 'Excel|export_excel|Export Excel|export_excel',
        ),
        'filtration' => array(
            'AuditEstate-name' => 'text|Název majetku|',

            'AuditEstate-imei_sn_vin' => 'text|IMEI/SN/VIN|',
            'AuditEstate-interni|internal_employee' => 'select|Inter. zam|inter_zam_list',
            'AuditEstate-odpisovatelny' => 'checkbox|Odpisovat.|',

            'Client-name|osoba' => 'text|Osoba|',
            'AuditEstate-at_company_id' => 'select|Pro. - firma|company_list',
            'AuditEstate-at_project_id' => 'select|Pro. - projekt|project_list',
            'AuditEstate-at_project_centre_id' => 'select|Pro. - středisko|centre_list',

            'AuditEstate-stav' => 'select|Stav|ae_stav_list',
            'ClientZam-at_company_id' => 'select|Náj. - firma|company_list',
            'ClientZam-at_project_id' => 'select|Náj. - projekt|project_list',
            'ClientZam-at_project_centre_id' => 'select|Náj. - středisko|centre_list',
        ),
        'items' => array(
            'id' => 'ID|AuditEstate|id|text|',
            'name' => 'Název|AuditEstate|name|text|',
            'imei' => 'IMEI/SN/VIN|AuditEstate|imei_sn_vin|text|',
            'int_numb' => 'Int. číslo|AuditEstate|internal_number|text|',
            'company' => 'Firma|AtCompany|name|text|',
            'company_centre' => 'Osoba - středisko|0|stredisko|text|',
            'company_person' => 'Osoba|0|osoba|text|',
            'first_cost' => 'Pořizovací cena|0|first_cost|text|',
            'payment_date' => 'Datum pořízení|AuditEstate|payment_date|date|',
            'hire_price' => 'Měs. pronájem|0|hire_price|text|',
            'dopl_info' => 'Dopl. info|0|dopl_info|text|orez3#30',
            //'updated'	=>	'Změněno|AuditEstate|updated|datetime|'
        ),
        'posibility' => array(
            'edit' => 'edit|Editace položky|edit',
            'print' => 'form_template|Šablony pro tisk|print',
            'manage_estate' => 'manage_estate|Správa majetku|manage_estate',
            'manage_estate_admin' => 'manage_estate_admin|Úprava historie|manage_estate_admin',
            'delete_estate' => 'delete_estate|Odstranit položku|delete_estate'
        ),
        'class_tr' => array(
            array(
                'class' => 'color_red',
                'model' => 0,
                'col' => 'repair_high',
                'value' => 1
            ),
            array(
                'class' => 'color_orange',
                'model' => 'AuditEstate',
                'col' => 'elimination_request',
                'value' => 1
            )
        ),
        'only_his' => array(
            'in_col' => 'at_project_centre_id',
            'subQuery' => '(SELECT id FROM wapis__at_project_centres Where spravce_majetku_id = #CmsUser.id# AND kos = 0)'
        ),
        'only_his2' => array(
            'select_connection' => true,
            'where_model' => 'AuditEstate',
            'where_col' => 'at_project_id'
        ),
        'sum_variables_on_top' => array(
            'path' => '../audit_estates/filtration_top'
        ),
        'domwin_setting' => array(
            'sizes' => '[800,900]',
            'scrollbars' => true,
            'languages' => true,
            'defined_lang' => "['cz','en','de']",
        )
    );

    function index()
    {
        $this->set('fastlinks', array('ATEP' => '/', 'Evidence majetku' => '#', $this->renderSetting['page_caption'] => '#'));
        $this->set('company_list', $this->get_list('AtCompany'));
        $this->set('project_list', $this->get_list('AtProject'));
        $this->set('centre_list', $this->get_list('AtProjectCentre'));
        $this->set('ae_stav_list', array(0 => 'u správce majetku', 1 => 'pujčený osobě'));

        // seznam internich zam
        $this->loadModel('ConnectionClientAtCompanyWorkPosition');
        $this->ConnectionClientAtCompanyWorkPosition->bindModel(array('belongsTo' => array('Client')));
        $inter_zam_list = $this->ConnectionClientAtCompanyWorkPosition->find('list', array(
            'conditions' => array('datum_to' => '0000-00-00'),
            'recursive' => 1,
            'fields' => array(
                'Client.id',
                'Client.name'
            ),
            'order' => array('name ASC')
        ));
        $this->set('inter_zam_list', $inter_zam_list);

        // celkova cena za polozky a pronajem
        $this->AuditEstate->bindModel($this->renderSetting['bindModel']);
        $rr = $this->AuditEstate->find('all', array(
            'conditions' => $this->ViewIndex->criteria,
            'fields' => array('currency,SUM(first_cost) as cena', 'SUM(hire_price) as pronajem'),
            'group' => 'currency'
        ));
        $this->set('filtration_sum_variables', $rr);


        if ($this->RequestHandler->isAjax()) {
            $this->render('../system/items');
        } else {
            $this->render('../system/index');
        }
    }

    function delete_estate($id)
    {
        $this->ViewIndex->render_trash($id);
        exit();
    }

    function edit($id = null)
    {
        if (empty($this->data)) {
            $cetre_list = array();

            if ($id != null) {
                $this->AuditEstate->bindModel(array(
                    'belongsTo' => array('AtProjectCentre', 'AtProject'),
                    'joinSpec' => array(
                        'SpravceMajetku' => array(
                            'className' => 'CmsUser',
                            'primaryKey' => 'SpravceMajetku.id',
                            'foreignKey' => 'AtProjectCentre.spravce_majetku_id'
                        )
                    )
                ));
                $this->data = $this->AuditEstate->read(array('AuditEstate.*', 'SpravceMajetku.name', 'SpravceMajetku.id', 'AtProject.project_spravce_majetku_id'), $id);
                if (in_array($this->logged_user['CmsUser']['id'], array($this->data['SpravceMajetku']['id'], $this->data['AtProject']['project_spravce_majetku_id']))) {
                    $permision = $this->controller_permission;
                    $permision['show_pin'] = true;
                    $this->set('permission', $permision);
                }

                $this->loadModel('ConnectionAuditEstate');
                $this->ConnectionAuditEstate->bindModel(array('belongsTo' => $this->ConnectionAuditEstate->standart_bind));
                $history_list = $this->ConnectionAuditEstate->find('all', array(
                    'conditions' => array(
                        'ConnectionAuditEstate.kos' => 0,
                        'audit_estate_id' => $id
                    ),
                    'fields' => am($this->ConnectionAuditEstate->spolecnost_klient_fields, array('ConnectionAuditEstate.*')),
                    'order' => 'ConnectionAuditEstate.id DESC'
                ));
                $this->set('history_list', $history_list);

                $this->loadModel('AuditEstateRepair');
                $repairs_list = $this->AuditEstateRepair->find('all', array(
                    'conditions' => array(
                        'AuditEstateRepair.kos' => 0,
                        'audit_estate_id' => $id
                    ),
                    'order' => 'AuditEstateRepair.date_repair DESC'
                ));

                $this->loadModel('AuditEstateStav');
                $stavs_list = $this->AuditEstateStav->find('all', array(
                    'conditions' => array(
                        'AuditEstateStav.kos' => 0,
                        'audit_estate_id' => $id
                    ),
                    'order' => 'AuditEstateStav.date_stav DESC'
                ));

                /**
                 * Nalezeni seznamu stredisek k dane firme
                 * musime prvni nalezst jaky porjekt patri pod danou firmu a podle nej najit strediska
                 */
                if ($this->data['AuditEstate']['at_company_id'] != '') {
                    $this->loadModel('AtProjectCompany');
                    $atpc = $this->AtProjectCompany->find('first', array(
                        'conditions' => array(
                            'AtProjectCompany.kos' => 0,
                            'at_company_id' => $this->data['AuditEstate']['at_company_id']
                        )
                    ));

                    if ($atpc)
                        $cetre_list = $this->get_list('AtProjectCentre', array('at_project_id' => $atpc['AtProjectCompany']['at_project_id']));
                }

                /**
                 * Nacteni emailoveho logu k majetku
                 */
                $this->loadModel('AuditEstateLog');
                $this->AuditEstateLog->bindModel(array(
                    'belongsTo' => array('MailTemplate')
                ));
                $mail_list = $this->AuditEstateLog->find('all', array(
                    'conditions' => array(
                        'AuditEstateLog.kos' => 0,
                        'audit_estate_id' => $id
                    ),
                    'order' => 'AuditEstateLog.created DESC'
                ));


                $this->set('repairs_list', $repairs_list);
                $this->set('stavs_list', $stavs_list);
                $this->set('mail_list', $mail_list);
                $this->data['AuditEstate']['naklady_na_opravy'] = array_sum(Set::extract($repairs_list, '{n}.AuditEstateRepair.price'));
            }

            /**
             * Podminka, pokud ma videt jen sve - vyber firmy takove,
             * u kterych existuje stredisko ve kterem je jako spravce majetku
             */
            $con = array();
            if ($this->logged_user['CmsGroup']['id'] != 1) {
                $this->loadModel('AtProjectCompany');
                $this->AtProjectCompany->bindModel(array(
                    'joinSpec' => array(
                        'AtProjectCentre' => array(
                            'className' => 'AtProjectCentre',
                            'primaryKey' => 'AtProjectCentre.at_project_id',
                            'foreignKey' => 'AtProjectCompany.at_project_id',
                            'conditions' => array('AtProjectCentre.kos' => 0)
                        ),
                        'AtProject' => array(
                            'className' => 'AtProject',
                            'primaryKey' => 'AtProject.id',
                            'foreignKey' => 'AtProjectCompany.at_project_id'
                        )
                    )
                ));
                $company_ids = $this->AtProjectCompany->find('all', array(
                    'conditions' => array(
                        'AtProjectCompany.kos' => 0,
                        'AtProjectCentre.kos' => 0,
                        'AtProject.kos' => 0,
                        'OR' => array(
                            'AtProjectCentre.spravce_majetku_id' => $this->logged_user['CmsUser']['id'],
                            'AtProject.project_spravce_majetku_id' => $this->logged_user['CmsUser']['id']
                        )
                    ),
                    'fields' => array('DISTINCT AtProjectCompany.at_company_id')
                ));
                //pr($company_ids);
                /*  
                $con = array(
                    'id IN(SELECT at_company_id FROM wapis__at_project_companies apc
                     LEFT JOIN wapis__at_project_centres as apcen ON(apcen.at_project_id = apc.at_project_id)
                     WHERE apcen.kos = 0 AND apc.kos = 0 
                     AND apcen.spravce_majetku_id='.$this->logged_user['CmsUser']['id'].')'
                );*/
                if ($company_ids != false) {
                    $con = array('id IN(' . implode(',', Set::extract('/AtProjectCompany/at_company_id', $company_ids)) . ')');
                }
                else if ($id != null) {
                    $con = array('id' => $this->data['AuditEstate']['at_company_id']);
                }

            }

            $this->set('ae_types_list', $this->get_list('AuditEstateType'));
            $this->set('company_list', $this->get_list('AtCompany', $con));
            $this->set('centre_list', $cetre_list);
            $this->render('edit');
        } else {
            if ($this->data['AuditEstate']['id'] != null) {
                $this->AuditEstate->bindModel(array(
                    'belongsTo' => array('AtCompany', 'AtProjectCentre')
                ));
                $old = $this->AuditEstate->read(null, $this->data['AuditEstate']['id']);
            }

            $this->AuditEstate->save($this->data['AuditEstate']);
            /**
             * Pokud pridavame novy majetek zmenime iterni cislo, ze nakonec pridame ID
             */
            if ($this->data['AuditEstate']['id'] == null) {
                $this->data['AuditEstate']['internal_number'] = $in = substr($this->data['AuditEstate']['internal_number'], 0, -4) . str_pad($this->AuditEstate->id, 4, "0", STR_PAD_LEFT);
                ;
                $this->AuditEstate->saveField('internal_number', $in);
            }


            $project = self::load_for_email($this->data['AuditEstate']['at_project_id'], 'AtProject', array('manager_id', 'project_spravce_majetku_id'));
            $comp = self::load_for_email($this->data['AuditEstate']['at_company_id'], 'AtCompany', array('name'));
            $st = self::load_for_email($this->data['AuditEstate']['at_project_centre_id'], 'AtProjectCentre', array('name', 'spravce_majetku_id', 'reditel_strediska_id'));

            echo '##' . $project['AtProject']['manager_id'] . '##';
            //data pro parovani pro sablony
            $this->Email->set_company_data(array(
                'project_manager_id' => $project['AtProject']['manager_id'],
                'spravce_majetku_id' => $st['AtProjectCentre']['spravce_majetku_id'],
                'project_spravce_majetku_id' => $project['AtProject']['project_spravce_majetku_id'],
                'reditel_strediska_id' => $st['AtProjectCentre']['reditel_strediska_id']
            ));

            //vytvorime log emailu do nasi tabulku kam potrebujeme
            $this->Email->set_log(array(
                'model' => 'AuditEstateLog',
                'col_name_parent_id' => 'audit_estate_id',
                'col_name_text' => 'text',
                'parent_id' => $this->AuditEstate->id
            ));

            $reciper = array();

            $this->loadModel('CmsUser');
            $usr = $this->CmsUser->read('email', $project['AtProject']['manager_id']);
            $reciper[] = $usr['CmsUser']['email'];

            // Odeslani emailu - novy majetek
            if ($this->data['AuditEstate']['id'] == '') {
                $replace_list = array(
                    '##AtCompany.name##' => $comp['AtCompany']['name'],
                    '##AtProjectCentre.name##' => $st['AtProjectCentre']['name'],
                    '##odpisovatelny##' => $this->ano_ne_checkbox[$this->data['AuditEstate']['odpisovatelny'] == 'on' || $this->data['AuditEstate']['odpisovatelny'] == 1 ? 1 : 0],
                    '##mena##' => $this->currency_list[$this->data['AuditEstate']['currency']],
                    '##porizeni##' => self::mail_date($this->data['AuditEstate']['payment_date']),
                    '##elimination_date##' => self::mail_date(isset($this->data['AuditEstate']['elimination_date']) ? $this->data['AuditEstate']['elimination_date'] : '0000-00-00'),
                    '##CmsUser.name##' => $this->logged_user['CmsUser']['name']
                );
                foreach ($this->data['AuditEstate'] as $item => $value) {
                    $replace_list['##AuditEstate.' . $item . '##'] = $value;
                }

                $this->Email->send_from_template_new(28, $reciper, $replace_list);
            } else {
                //poslat zmenovy email
                $replace_list = array(
                    '##AtCompany.name##' => $comp['AtCompany']['name'],
                    '##AtProjectCentre.name##' => $st['AtProjectCentre']['name'],
                    '##odpisovatelny##' => $this->ano_ne_checkbox[$this->data['AuditEstate']['odpisovatelny'] == 'on' || $this->data['AuditEstate']['odpisovatelny'] == 1 ? 1 : 0],
                    '##mena##' => $this->currency_list[$this->data['AuditEstate']['currency']],
                    '##porizeni##' => self::mail_date($this->data['AuditEstate']['payment_date']),
                    '##elimination_date##' => self::mail_date($old['AuditEstate']['elimination_date']),

                    '##AtCompany.name_old##' => $old['AtCompany']['name'],
                    '##AtProjectCentre.name_old##' => $old['AtProjectCentre']['name'],
                    '##odpisovatelny_old##' => $this->ano_ne_checkbox[$old['AuditEstate']['odpisovatelny'] == 'on' || $this->data['AuditEstate']['odpisovatelny'] == 1 ? 1 : 0],
                    '##mena_old##' => $this->currency_list[$old['AuditEstate']['currency']],
                    '##porizeni_old##' => self::mail_date($old['AuditEstate']['payment_date']),
                    '##elimination_date_old##' => self::mail_date($old['AuditEstate']['elimination_date']),

                    '##CmsUser.name##' => $this->logged_user['CmsUser']['name']
                );
                foreach ($this->data['AuditEstate'] as $item => $value) {
                    $replace_list['##AuditEstate.' . $item . '##'] = $value;
                }
                foreach ($old['AuditEstate'] as $item => $value) {
                    $replace_list['##AuditEstate.' . $item . '_old##'] = $value;
                }

                $this->Email->send_from_template_new(29, $reciper, $replace_list);

            }


            die();
        }
    }

    function load_for_email($id, $model, $fields = null)
    {
        if (!isset($this->{$model}))
            $this->loadModel($model);

        return $this->{$model}->read($fields, $id);
    }

    function mail_date($datum = null, $format = 'd.m.Y')
    {
        if ($datum == null || $datum == '' || $datum == '0000-00-00')
            return false;
        else
            return date($format, strtotime($datum));
    }


    /**
     * funkce pro vyrazeni majetku
     * zadava se datum ke kteremu bude vyrazeno
     */
    function elimination($id = null)
    {
        $list = $this->get_list('AuditEstateWithdrawalType');

        if (empty($this->data)) {
            $this->data['AuditEstate']['id'] = $id;
            $this->data['AuditEstate']['elimination_request'] = 1;

            $this->set('cat_list', $list);

            $this->render('add_eliminate');
        }
        else {
            $this->AuditEstate->save($this->data);

            $majetek = $this->AuditEstate->read('name', $this->data['AuditEstate']['id']);
            $st = self::load_for_email($majetek['AuditEstate']['at_project_centre_id'], 'AtProjectCentre', array('name', 'spravce_majetku_id', 'reditel_strediska_id'));

            $replace_list = array(
                '##AuditEstate.name##' => $majetek['AuditEstate']['name'],
                '##CmsUser.name##' => $this->logged_user['CmsUser']['name'],
                '##kategorie##' => $list[$this->data['AuditEstate']['elimination_withdraval_type']],
                '##text##' => $this->data['AuditEstate']['elimination_text']
            );
            //data pro parovani pro sablony
            $this->Email->set_company_data(array(
                'reditel_strediska_id' => $st['AtProjectCentre']['reditel_strediska_id']
            ));
            $this->Email->send_from_template_new(30, array(), $replace_list);
        }
    }


    /**
     * Funkce pro správu majetku - předělování a odebírání majetku
     */
    function manage_estate($audit_estate_id = null)
    {
        if ($audit_estate_id != null) {
            $this->set('id', $audit_estate_id);

            $this->loadModel('ConnectionAuditEstate');
            $this->ConnectionAuditEstate->bindModel(array(
                'belongsTo' => $this->ConnectionAuditEstate->standart_bind
            ));
            $connections = $this->ConnectionAuditEstate->find('all', array(
                'conditions' => array(
                    'ConnectionAuditEstate.audit_estate_id' => $audit_estate_id,
                    'ConnectionAuditEstate.kos' => 0,
                    'ConnectionAuditEstate.to_date' => '0000-00-00'
                ),
                'fields' => am($this->ConnectionAuditEstate->spolecnost_klient_fields, array('ConnectionAuditEstate.created', 'ConnectionAuditEstate.id'))
            ));
            $this->set('connections', $connections);

            $this->render('manage_estate/index');
        }
        else
            die('Nenalezneo ID');
    }

    /**
     * prirazovani majetku k jednotlivym firmam a jejich osob
     */
    function add_estate($audit_estate_id = null)
    {
        if (empty($this->data)) {
            $this->data['ConnectionAuditEstate']['audit_estate_id'] = $audit_estate_id;


            /**
             * pokud jiz je prirazen majetek hod chybu

            $this->loadModel('ConnectionAuditEstate');
            $this->ConnectionAuditEstate->bindModel(array(
            'belongsTo'=>array('AtCompany','Client')
            ));
            $f = $this->ConnectionAuditEstate->find('first',array(
            'conditions'=>array(
            'ConnectionAuditEstate.kos'=>0,
            'ConnectionAuditEstate.to_date'=>'0000-00-00',
            'audit_estate_id'=>$audit_estate_id
            )
            ));

            if($f){
            die('<br/><br/><br/><br/><p align="center"><em>Litujeme, ale tento majetek již má svého majitele.
            <br />Ve firmě: <strong>'.$f['AtCompany']['name'].'</strong>, osoba: <strong>'.$f['Client']['name'].'</strong>.</em></p>');
            }
             */
            //Informace o majetku
            $aa = $this->AuditEstate->read(null, $audit_estate_id);
            $this->data['AuditEstate'] = $aa['AuditEstate'];
            $this->set('company_list', $this->get_list('AtCompany'));
            $this->set('centre_list', $this->get_list('AtProjectCentre'));


            /*
            $con = array('kos'=>0);
            if($this->logged_user['CmsGroup']['id'] != 1){
                $con = array(
                    'kos'=>0,
                    'id IN(SELECT at_company_id FROM wapis__at_project_companies apc LEFT JOIN wapis__at_project_centres as apcen ON(apcen.id = apc.at_project_id) WHERE apcen.kos = 0 AND apc.kos = 0 AND apcen.spravce_majetku_id='.$this->logged_user['CmsUser']['id'].')'
                );
            }
            $this->set('company_list',$this->get_list('AtCompany',$con));
            */
            $this->loadModel('ConnectionClientAtCompanyWorkPosition');
            $this->ConnectionClientAtCompanyWorkPosition->query('SET NAMES UTF8');
            $this->ConnectionClientAtCompanyWorkPosition->bindModel(array(
                'belongsTo' => array('Client')
            ));
            $person_list = $this->ConnectionClientAtCompanyWorkPosition->find('all', array(
                'conditions' => array(
                    'ConnectionClientAtCompanyWorkPosition.datum_to' => '0000-00-00',
                    'Client.kos' => 0,
                    'Client.name !=' => ''
                )
            , 'fields' => array('ConnectionClientAtCompanyWorkPosition.id', 'Client.name', 'Client.id'),
                'order' => 'Client.name'
            ));
            $this->set('person_list', Set::combine($person_list, '{n}.Client.id', '{n}.Client.name'));
            $this->set('person_list_title', Set::combine($person_list, '{n}.Client.id', '{n}.ConnectionClientAtCompanyWorkPosition.id'));


            $this->set('company_foreign_list', $this->get_list('CompanyForeign'));
            $this->set('client_foreign_list', $this->get_list('ClientForeign'));
        }
        else {
            /**
             * $typ = 1 - normal, 2 - soukrome ucely
             */
            $typ = $this->data['ConnectionAuditEstate']['type'];

            /**
             * osetreni ze nesmi jit majetek jen na firmu nebo bez firmy
             * @TODO .. jaktoze zde neni zadna chybova hlaska?
             */
            if ($typ == 1 && $this->data['ConnectionAuditEstate']['client_id'] == null || $this->data['ConnectionAuditEstate']['at_company_id'] == null)
                die(json_encode(array('result' => false)));

            $this->loadModel('ConnectionAuditEstate');

            /**
             * Nastavim pro ulozeni, kdo prideluje majetek dle prihlaseneho uzivatel
             */
            $this->data['ConnectionAuditEstate']['cms_user_id'] = $this->logged_user['CmsUser']['id'];

            /**
             * Ulozeni spojeni mezi Majetkem a Uzivatelem
             * @TODO .. opravdu je to mezi majetkem a uzivatelem?
             */
            if ($this->ConnectionAuditEstate->save($this->data)) {

                /**
                 * Ulozeni stavu majetku na pronajmuto
                 * TODO .. opravdu pronajmuto
                 */
                $this->AuditEstate->save(array(
                    'id' => $this->data['ConnectionAuditEstate']['audit_estate_id'],
                    'stav' => 1
                ));

                /**
                 * Nacteni informaci o majetku
                 */
                $this->AuditEstate->bindModel(array(
                    'belongsTo' => array(
                        'AtProject' => array(
                            'foreignKey' => 'at_project_id'
                        )
                    )
                ));
                $majetek = $this->AuditEstate->read(null, $this->data['ConnectionAuditEstate']['audit_estate_id']);

                /**
                 * Vytahnuti informaci pro email
                 * TODO .. proverit co to presne dela
                 */
                $comp = self::load_for_email($majetek['AuditEstate']['at_company_id'], 'AtCompany', array('name'));
                $st = self::load_for_email($majetek['AuditEstate']['at_project_centre_id'], 'AtProjectCentre', array('name', 'reditel_strediska_id', 'spravce_majetku_id'));
                $cinnost = self::load_for_email($this->data['ConnectionAuditEstate']['cinnost_id'], 'AtProjectCentre', array('name'));
                $project = self::load_for_email($majetek['AuditEstate']['at_project_id'], 'AtProject', array('name', 'manager_id', 'project_spravce_majetku_id'));

                /**
                 * initializace seznamu prijemcu
                 */
                $cms_user_mail = array();
                $this->loadModel('CmsUser');
                $usr = $this->CmsUser->read('email', $majetek['AtProject']['manager_id']);
                $cms_user_mail[] = $usr['CmsUser']['email'];

                /**
                 * klasicke pujceni majetku v a&t
                 */
                if ($typ == 1) {
                    /**
                     * Vytahnuti informaci pro email
                     * TODO .. proverit co to presne dela
                     */
                    $client = self::load_for_email($this->data['ConnectionAuditEstate']['client_id'], 'Client', array('name', 'email'));
                    $comp_komu = self::load_for_email($this->data['ConnectionAuditEstate']['at_company_id'], 'AtCompany', array('name'));

                    /**
                     * Pokud client_id je prirazeno nejakemu cms_user, tak vyhledej jeho mail a zasli notifikaci
                     * TODO .. proc se tahaji vsechny informace o klientovi, prvde[podobne potreba pouze email
                     * TODO .. ta podminka nema else, tedy ze nevyhovuje ani jedne podmince, v tomto pripade se nejspise nepriradi email, ale muze toto nastat? A pokud ano je to ok?
                     */
                    $this->loadModel('CmsUser');
                    $f_cms = $this->CmsUser->find('first', array('conditions' => array('at_employee_id' => $this->data['ConnectionAuditEstate']['client_id'])));
                    if ($f_cms) {
                        $cms_user_mail[] = $f_cms['CmsUser']['email'];
                    }
                    else if ($client['Client']['email'] != '') { //pokud ne tak ho zkus vytahnout z interniho zamestance
                        $cms_user_mail[] = $client['Client']['email'];
                    }
                }
                /**
                 * soukrome ucely
                 */
                else {
                    /**
                     * Natahnuti informaci o soukrome spolecnosti, pokud je uvedena v connection. Pokud ano, natahne sez informaci pro mail
                     * Pokud neexistuje, uvede se "neuvedeno"
                     */
                    if ($this->data['ConnectionAuditEstate']['company_foreign_id'] != '') {
                        $_comp_komu = self::load_for_email($this->data['ConnectionAuditEstate']['company_foreign_id'], 'CompanyForeign', array('name'));
                        $comp_komu['AtCompany'] = $_comp_komu['CompanyForeign'];
                    }
                    else
                        $comp_komu['AtCompany']['name'] = '<neuvedeno>';

                    /**
                     * Natahnuti informaci o soukrome osobe, pokud je uvedena v connection. Pokud ano, natahne sez informaci pro mail
                     * Pokud neexistuje, uvede se "neuvedeno"
                     */
                    if ($this->data['ConnectionAuditEstate']['client_foreign_id'] != '') {
                        $_client = self::load_for_email($this->data['ConnectionAuditEstate']['client_foreign_id'], 'ClientForeign', array('name', 'email'));
                        $client['Client'] = $_client['ClientForeign'];
                        if (isset($client['Client']['email']) && $client['Client']['email'] == '')
                            $cms_user_mail[] = $client['Client']['email'];
                    }
                    else
                        $client['Client']['name'] = '<neuvedeno>';
                }


                $ucely = null;
                /**
                 * Pokud je zaskrtnuto "Majetek použi pro soukromé účely", propise se pospisek;
                 */

                if ($this->data['ConnectionAuditEstate']['private_use'] == 1 || $this->data['ConnectionAuditEstate']['private_use'] == 'on') {
                    $ucely = '<br />Majetek je použit pro soukromé účely.<br />';
                }

                /**
                 * Replace list pro emailovou sablonu
                 */
                $replace_list = array(
                    '##Komu.cinnost##' => $cinnost['AtProjectCentre']['name'],
                    '##AtCompany.name##' => $comp['AtCompany']['name'],
                    '##AtProjectCentre.name##' => $st['AtProjectCentre']['name'],
                    '##odpisovatelny##' => $this->ano_ne_checkbox[$majetek['AuditEstate']['odpisovatelny'] == 'on' || $majetek['AuditEstate']['odpisovatelny'] == 1 ? 1 : 0],
                    '##mena##' => $this->currency_list[$majetek['AuditEstate']['currency']],
                    '##porizeni##' => self::mail_date($majetek['AuditEstate']['payment_date']),
                    '##elimination_date##' => self::mail_date($majetek['AuditEstate']['elimination_date']),
                    '##Komu.name##' => $client['Client']['name'],
                    '##Komu.firma##' => $comp_komu['AtCompany']['name'],
                    '##Komu.stredisko##' => ($this->data['ConnectionAuditEstate']['centre'] != '' ? $this->data['ConnectionAuditEstate']['centre'] : '<neuvedeno>'),
                    '##CmsUser.name##' => $this->logged_user['CmsUser']['name'],
                    '##soukrome_ucely##' => $ucely,
                    '##ConnectionAuditEstate.created##' => self::mail_date($this->data['ConnectionAuditEstate']['created']),
                    '##ConnectionAuditEstate.description##' => $this->data['ConnectionAuditEstate']['description']
                );
                foreach ($majetek['AuditEstate'] as $item => $value) {
                    $replace_list['##AuditEstate.' . $item . '##'] = $value;
                }

                /**
                 * Pokud je zaskrtnuto "Majetek použi pro soukromé účely" odesila se emailova sablona #45
                 * pokud zaroven se jedna o typ 1 ("Majetek pro A&T"), natahnou se informace o Projekt Manageru
                 */
                if ($this->data['ConnectionAuditEstate']['private_use'] == 1 || $this->data['ConnectionAuditEstate']['private_use'] == 'on') {
                    if ($typ == 1)
                        $this->Email->set_company_data(array('project_manager_id' => $project['AtProject']['manager_id'], 'spravce_majetku_id' => $st['AtProjectCentre']['spravce_majetku_id'], 'project_spravce_majetku_id' => $project['AtProject']['project_spravce_majetku_id']));

                    $this->Email->send_from_template_new(45, array(), $replace_list);
                }
                else {
                    /**
                     * Pokud majetek neni pro soukrome ucely
                     * Pokud je typ 1, nastavi se do emailu informace o spolecnosti o Project Manageru, Spravce majetku, ...
                     * Nasledne se odesila emailova sablona #31 a #35
                     */
                    if ($typ == 1) {
                        $this->Email->set_company_data(array(
                            'project_manager_id' => $project['AtProject']['manager_id'],
                            'spravce_majetku_id' => $st['AtProjectCentre']['spravce_majetku_id'],
                            'project_spravce_majetku_id' => $project['AtProject']['project_spravce_majetku_id'],
                            'reditel_strediska_id' => $st['AtProjectCentre']['reditel_strediska_id']
                        ));
                    }

                    /**
                     * Najiti project managera dle zamestnaneho interniho zamestnance, jenz je napojeny
                     * na stredisko a od nej se bere projekt manager
                     */

                    if ($typ == 1) {
                        $client_id = $this->data['ConnectionAuditEstate']['client_id'];
                        $this->loadModel('ConnectionClientAtCompanyWorkPosition');

                        $this->ConnectionClientAtCompanyWorkPosition->bindModel(array(
                            'joinSpec' => array(
                                'AtProject' => array(
                                    'primaryKey' => 'ConnectionClientAtCompanyWorkPosition.at_project_id',
                                    'foreignKey' => 'AtProject.id'
                                ),
                                'CmsUser' => array(
                                    'primaryKey' => 'CmsUser.id',
                                    'foreignKey' => 'AtProject.manager_id'
                                )
                            )
                        ));

                        $connection = $this->ConnectionClientAtCompanyWorkPosition->find(
                            'first',
                            array(
                                'conditions' => array(
                                    'ConnectionClientAtCompanyWorkPosition.datum_to' => '0000-00-00',
                                    'ConnectionClientAtCompanyWorkPosition.client_id' => $client_id
                                ),
                                'fields' => array(
                                    'CmsUser.email',
                                    'CmsUser.name'
                                )
                            )
                        );
                        $cms_user_mail[] = $connection['CmsUser']['email'];
                    }


                    $this->Email->send_from_template_new(31, $cms_user_mail, $replace_list);

                    if ($typ == 1)
                        $this->Email->set_company_data(array('project_manager_id' => $project['AtProject']['manager_id'], 'spravce_majetku_id' => $st['AtProjectCentre']['spravce_majetku_id'], 'project_spravce_majetku_id' => $project['AtProject']['project_spravce_majetku_id']));

                    $this->Email->send_from_template_new(35, $cms_user_mail, $replace_list);

                }

                die(json_encode(array('result' => true)));
            } else
                die(json_encode(array('result' => false, 'message' => 'Cyba behem ukladani do DB')));
        }
    }

    function remove_estate($audit_estate_id = null, $connection_audit_estate_id = null)
    {
        if (empty($this->data)) {
            if ($connection_audit_estate_id == null)
                die('<br/><br/><br/><br/><p align="center"><em>Spojeni nebylo nalezeno.</em></p>');
            $this->data['ConnectionAuditEstate']['id'] = $connection_audit_estate_id;
            $this->data['AuditEstate']['id'] = $audit_estate_id;
        }
        else {
            $this->loadModel('ConnectionAuditEstate');
            if ($this->ConnectionAuditEstate->save($this->data)) {
                $all_is_return = $this->ConnectionAuditEstate->find('first', array(
                    'conditions' => array(
                        'ConnectionAuditEstate.kos' => 0,
                        'ConnectionAuditEstate.to_date' => '0000-00-00',
                        'audit_estate_id' => $this->data['AuditEstate']['id']
                    )
                ));
                /**
                 * Pokud uz neexistuje connection ktere by byla aktivni,
                 * prepni stav majetku do neprirazen
                 */
                if (!$all_is_return) {
                    $this->AuditEstate->save(array(
                        'id' => $this->data['AuditEstate']['id'],
                        'stav' => 0
                    ));
                }

                $connection = $this->ConnectionAuditEstate->read(array('client_id', 'at_company_id', 'centre', 'description'), $this->data['ConnectionAuditEstate']['id']);
                /**
                 * Nacteni informaci o majetku
                 */
                $this->AuditEstate->bindModel(array(
                    'belongsTo' => array(
                        'AtProject' => array(
                            'foreignKey' => 'at_project_id'
                        )
                    )
                ));
                $majetek = $this->AuditEstate->read(null, $this->data['AuditEstate']['id']);
                $client = self::load_for_email($connection['ConnectionAuditEstate']['client_id'], 'Client', array('name', 'email'));
                $comp = self::load_for_email($majetek['AuditEstate']['at_company_id'], 'AtCompany', array('name'));
                $st = self::load_for_email($majetek['AuditEstate']['at_project_centre_id'], 'AtProjectCentre', array('name', 'spravce_majetku_id', 'reditel_strediska_id'));
                $project = self::load_for_email($majetek['AuditEstate']['at_project_id'], 'AtProject', array('name', 'manager_id', 'project_spravce_majetku_id'));
                $comp_komu = self::load_for_email($connection['ConnectionAuditEstate']['at_company_id'], 'AtCompany', array('name'));


                $cms_user_mail = array();
                $this->loadModel('CmsUser');
                $usr = $this->CmsUser->read('email', $majetek['AtProject']['manager_id']);
                $cms_user_mail[] = $usr['CmsUser']['email'];

                /**
                 * Pokud client_id je prirazeno nejakemu cms_user, tak vyhledej jeho mail a zasli notifikaci
                 */
                $this->loadModel('CmsUser');
                $f_cms = $this->CmsUser->find('first', array('conditions' => array('at_employee_id' => $this->data['ConnectionAuditEstate']['client_id'])));
                if ($f_cms) {
                    $cms_user_mail[] = $f_cms['CmsUser']['email'];

                    /**
                     * Najiti project managera dle zamestnaneho interniho zamestnance, jenz je napojeny
                     * na stredisko a od nej se bere projekt manager
                     */
                    $client_id = $this->data['ConnectionAuditEstate']['client_id'];
                    $this->loadModel('ConnectionClientAtCompanyWorkPosition');

                    $this->ConnectionClientAtCompanyWorkPosition->bindModel(array(
                        'joinSpec' => array(
                            'AtProject' => array(
                                'primaryKey' => 'ConnectionClientAtCompanyWorkPosition.at_project_id',
                                'foreignKey' => 'AtProject.id'
                            ),
                            'CmsUser' => array(
                                'primaryKey' => 'CmsUser.id',
                                'foreignKey' => 'AtProject.manager_id'
                            )
                        )
                    ));

                    $connection = $this->ConnectionClientAtCompanyWorkPosition->find(
                        'first',
                        array(
                            'conditions' => array(
                                'ConnectionClientAtCompanyWorkPosition.datum_to' => '0000-00-00',
                                'ConnectionClientAtCompanyWorkPosition.client_id' => $client_id
                            ),
                            'fields' => array(
                                'CmsUser.email',
                                'CmsUser.name'
                            )
                        )
                    );
                    $cms_user_mail[] = $connection['CmsUser']['email'];
                }

                $replace_list = array(
                    '##AtCompany.name##' => $comp['AtCompany']['name'],
                    '##AtProjectCentre.name##' => $st['AtProjectCentre']['name'],
                    '##odpisovatelny##' => $this->ano_ne_checkbox[$majetek['AuditEstate']['odpisovatelny'] == 'on' || $majetek['AuditEstate']['odpisovatelny'] == 1 ? 1 : 0],
                    '##mena##' => $this->currency_list[$majetek['AuditEstate']['currency']],
                    '##porizeni##' => self::mail_date($majetek['AuditEstate']['payment_date']),
                    '##elimination_date##' => self::mail_date($majetek['AuditEstate']['elimination_date']),
                    '##vracen##' => self::mail_date($this->data['ConnectionAuditEstate']['to_date']),
                    '##Komu.name##' => $client['Client']['name'],
                    '##Komu.firma##' => $comp_komu['AtCompany']['name'],
                    '##Komu.stredisko##' => $connection['ConnectionAuditEstate']['centre'],
                    '##ConnectionAuditEstate.description##' => $connection['ConnectionAuditEstate']['description'],
                    '##CmsUser.name##' => $this->logged_user['CmsUser']['name']
                );
                foreach ($majetek['AuditEstate'] as $item => $value) {
                    $replace_list['##AuditEstate.' . $item . '##'] = $value;
                }
                $this->Email->set_company_data(array(
                    'project_manager_id' => $project['AtProject']['manager_id'],
                    'spravce_majetku_id' => $st['AtProjectCentre']['spravce_majetku_id'],
                    'project_spravce_majetku_id' => $project['AtProject']['project_spravce_majetku_id'],
                    'reditel_strediska_id' => $st['AtProjectCentre']['reditel_strediska_id']
                ));


                $this->Email->send_from_template_new(32, $cms_user_mail, $replace_list);

                die(json_encode(array('result' => true)));
            } else
                die(json_encode(array('result' => false)));
        }
    }


    /**
     * nacteni firmy a strediska k dane osobe
     */
    function load_ajax_list($connection_id = null)
    {
        if ($connection_id != null) {
            $this->loadModel('ConnectionClientAtCompanyWorkPosition');
            $this->ConnectionClientAtCompanyWorkPosition->query('SET NAMES UTF8');
            $this->ConnectionClientAtCompanyWorkPosition->bindModel(array(
                'belongsTo' => array('Client', 'AtProjectCentre', 'AtCompany')
            ));
            $output = $this->ConnectionClientAtCompanyWorkPosition->find('first', array(
                'conditions' => array(
                    'ConnectionClientAtCompanyWorkPosition.id' => $connection_id,
                    'Client.kos' => 0,
                    'Client.name !=' => ''
                )
            , 'fields' => array('AtCompany.id', 'AtCompany.name', 'AtProjectCentre.name', 'ConnectionClientAtCompanyWorkPosition.at_project_cinnost_id')
            ));
            die(json_encode(array('result' => false, 'AtCompany' => $output['AtCompany'], 'AtProjectCentre' => $output['AtProjectCentre']['name'], 'cinnost_id' => $output['ConnectionClientAtCompanyWorkPosition']['at_project_cinnost_id'])));
        }
        die(json_encode(array('result' => false, 'message' => 'Nebyla zvolená osoba')));
    }


    /**
     * nacteni seznamu stredisek
     */
    function load_at_project_centre_list($company_id = null)
    {
        if ($company_id != null) {
            $cetre_list = array();
            $this->loadModel('AtProjectCompany');
            $atpc = $this->AtProjectCompany->find('all', array(
                'conditions' => array(
                    'AtProjectCompany.kos' => 0,
                    'at_company_id' => $company_id
                )
            ));

            /**
             * Podminka, pokud ma videt jen sve - vyber firmy takove,
             * u kterych existuje stredisko ve kterem je jako spravce majetku
             */
            $con = array(
                'AtProjectCentre.at_project_id' => Set::extract('/AtProjectCompany/at_project_id', $atpc),
                'AtProjectCentre.kos' => 0
            );

            if ($this->logged_user['CmsGroup']['id'] != 1) {
                $con = am($con, array(
                    'OR' => array(
                        'AtProjectCentre.spravce_majetku_id' => $this->logged_user['CmsUser']['id'],
                        'AtProject.project_spravce_majetku_id' => $this->logged_user['CmsUser']['id'],
                    )
                ));
            }

            if ($atpc) {
                $this->loadModel('AtProjectCentre');
                $this->AtProjectCentre->bindModel(array('belongsTo' => array('AtProject')));
                $cetre_list = $this->AtProjectCentre->find('all', array(
                    'conditions' => $con
                ));
                $cetre_list = Set::combine($cetre_list, '/AtProjectCentre/id', '/AtProjectCentre/name');
            }

            die(json_encode($cetre_list));
        }
        die(json_encode(array('result' => false, 'message' => 'Nebyla zvolená firma')));
    }


    /**
     * zobrazeni historie prideleni majetku
     */
    function history($audit_estate_id = null)
    {
        if ($audit_estate_id != null) {
            $this->loadModel('ConnectionAuditEstate');
            $this->ConnectionAuditEstate->bindModel(array('belongsTo' => array('CompanyAuditEstate' => array('foreignKey' => 'company_id'), 'CompanyPerson')));
            $history_list = $this->ConnectionAuditEstate->find('all', array(
                'conditions' => array(
                    'ConnectionAuditEstate.kos' => 0,
                    'audit_estate_id' => $audit_estate_id
                ),
                'order' => 'ConnectionAuditEstate.id DESC'
            ));

            $this->set('history_list', $history_list);

            //render
            $this->render('history');
        }
        else
            die('Chybi ID majetku ke kterému chcete zobrazit historii');

    }


    function add_repair($audit_estate_id = null)
    {
        if (empty($this->data)) {
            $this->data['AuditEstateRepair']['audit_estate_id'] = $audit_estate_id;
        }
        else {
            $this->loadModel('AuditEstateRepair');
            if ($this->AuditEstateRepair->save($this->data)) {
                die(json_encode(array('result' => true)));
            } else
                die(json_encode(array('result' => false)));
        }
    }

    function add_stav($audit_estate_id = null, $in_return_estate = false)
    {
        if (empty($this->data)) {
            $this->set('in_return_estate', $in_return_estate);
            $this->data['AuditEstateStav']['audit_estate_id'] = $audit_estate_id;
        }
        else {
            $this->loadModel('AuditEstateStav');
            if ($this->AuditEstateStav->save($this->data)) {
                die(json_encode(array('result' => true)));
            } else
                die(json_encode(array('result' => false)));
        }
    }

    /**
     * Zobrazeni dostupnych formularu pro majetek
     */
    function form_template($audit_estate_id = null)
    {
        if (empty($this->data)) {
            $this->set('id', $audit_estate_id);
            //$this->set('form_list',$this->get_list('FormTemplate',array('type'=>1)));

            $this->loadModel('ConnectionAuditEstate');
            $this->ConnectionAuditEstate->bindModel(array(
                'belongsTo' => array('Client', 'AuditEstate', 'ClientForeign')
            ), false);
            $connections = $this->ConnectionAuditEstate->find('all', array(
                'conditions' => array(
                    'ConnectionAuditEstate.kos' => 0,
                    'ConnectionAuditEstate.to_date' => '0000-00-00',
                    'audit_estate_id' => $audit_estate_id
                ),
                'fields' => array(
                    '(IF(ConnectionAuditEstate.type = 1,Client.name,IFNULL(ClientForeign.name,"neuvedeno"))) as klient',
                    'ConnectionAuditEstate.*'
                )
            ));
            $this->set('connections', Set::combine($connections, '/ConnectionAuditEstate/id', '/0/klient'));


            $connections2 = $this->ConnectionAuditEstate->find('all', array(
                'conditions' => array(
                    'ConnectionAuditEstate.kos' => 0,
                    'ConnectionAuditEstate.to_date !=' => '0000-00-00',
                    'audit_estate_id' => $audit_estate_id
                ),
                'fields' => array(
                    'IF(ConnectionAuditEstate.type = 1,Client.name,IFNULL(ClientForeign.name,"neuvedeno")) as klient',
                    'ConnectionAuditEstate.*'
                )
            ));
            $this->set('connections2', Set::combine($connections2, '/ConnectionAuditEstate/id', '/0/klient'));

            $this->render('form_template/index');
        }
    }

    function form_template_items($connection_audit_estate_id = null, $typ = 1)
    {
        if ($connection_audit_estate_id == null)
            die('Zadne ID');


        $con_for_join = array(
            'ConnectionClientAtCompanyWorkPosition.at_company_id = ConnectionAuditEstate.at_company_id'
        );

        //pro ty aktualni, hledame pouze aktivni zamestnani,
        //ne uplne stastne reseni, protoze muze figurovat v obou
        if ($typ == 1) {
            $con_for_join = am($con_for_join, array('ConnectionClientAtCompanyWorkPosition.datum_to' => '0000-00-00'));
        }

        $this->loadModel('ConnectionAuditEstate');
        $this->ConnectionAuditEstate->bindModel(array(
            'belongsTo' => array('Client', 'AuditEstate'),
            'joinSpec' => array(
                'ConnectionClientAtCompanyWorkPosition' => array(
                    'className' => 'ConnectionClientAtCompanyWorkPosition',
                    'primaryKey' => 'Client.id',
                    'foreignKey' => 'ConnectionClientAtCompanyWorkPosition.client_id',
                    'conditions' => $con_for_join
                )
            )
        ));
        $connection = $this->ConnectionAuditEstate->find('first', array(
            'conditions' => array(
                'ConnectionAuditEstate.id' => $connection_audit_estate_id
            ),
            'fields' => array('Client.id', 'AuditEstate.at_company_id', 'AuditEstate.id',
                'ConnectionClientAtCompanyWorkPosition.at_company_money_item_id', 'ConnectionAuditEstate.*')
        ));


        if (!$connection || ($connection['ConnectionClientAtCompanyWorkPosition']['at_company_money_item_id'] == '' && $connection['ConnectionAuditEstate']['type'] == 1))
            die('Chyba, nenalezeno žádné aktivní zaměstnání.');

        else if ($connection['ConnectionAuditEstate']['type'] == 2) {
            $client = array();
            if ($connection['ConnectionAuditEstate']['client_foreign_id'] != '') {
                $this->loadModel('ClientForeign');
                $fi = $this->ClientForeign->read(null, $connection['ConnectionAuditEstate']['client_foreign_id']);
                $client = $fi['ClientForeign'];
                $soukromy = 1;
            }
            else if ($connection['ConnectionAuditEstate']['company_foreign_id'] != '') {
                $this->loadModel('CompanyForeign');
                $fi = $this->CompanyForeign->read(null, $connection['ConnectionAuditEstate']['company_foreign_id']);
                $client = $fi['CompanyForeign'];
                $soukromy = 2;
            }
            $con_for_forms = array(
                'FormTemplate.kos' => 0,
                'FormTemplate.typ' => 3
            );
            $this->loadModel('FormTemplate');
            $forms = $this->FormTemplate->find('all', array('conditions' => array(
                'FormTemplate.kos' => 0,
                'FormTemplate.type' => 3
            )));

            $connection['Client'] = $client;

        } else {
            $soukromy = 0;
            $this->loadModel('ConnectionFormTemplate');
            $this->ConnectionFormTemplate->bindModel(array(
                'belongsTo' => array('FormTemplate')
            ));

            //pr($connection);
            $forms = $this->ConnectionFormTemplate->find('all', array('conditions' => array(
                'at_company_id' => $connection['AuditEstate']['at_company_id'],
                'at_company_money_item_id' => $connection['ConnectionClientAtCompanyWorkPosition']['at_company_money_item_id'],
                'ConnectionFormTemplate.kos' => 0,
                'ConnectionFormTemplate.typ' => $typ,
                'FormTemplate.kos' => 0
            )));

            //pr($forms);
        }


        //pr($forms);
        $this->set('form_list', $forms);
        $this->set('soukromy', $soukromy);
        $this->set('client_id', $connection['Client']['id']);
        $this->set('audit_estate_id', $connection['AuditEstate']['id']);
        $this->set('connection_audit_estate_id', $connection_audit_estate_id);

        $this->render('form_template/items');
    }

    /**
     * Funkce pro generovani formulare
     * Tisk
     */
    function print_form($form_id, $audit_estate_id, $client_id, $connection_audit_estate_id, $soukromy = false)
    {
        //function generate_form($id = null,$client_id, $data_id = null, $company_table = false){
        $this->layout = 'print';

        $this->loadModel('ConnectionAuditEstate');
        $this->ConnectionAuditEstate->bindModel(array(
            'belongsTo' => array('CmsUser')
        ));
        $con = array(
            'ConnectionAuditEstate.kos' => 0,
            'ConnectionAuditEstate.id' => $connection_audit_estate_id,
            'audit_estate_id' => $audit_estate_id,
        );
        if ($soukromy == false) {
            $con['client_id'] = $client_id;
        }

        $find = $this->ConnectionAuditEstate->find('first', array(
            'conditions' => $con
        ));
        if (!$find)
            die('K majeteku neni nikdo pridelen, nelze vyhledavat prislusne data o klientovi');

        /**
         * v pripade soukromych ucelu
         */
        if ($soukromy != false) {
            if ($soukromy == 1) { //klient
                $this->loadModel('ClientForeign');
                $cf = $this->ClientForeign->read(null, $client_id);
                $_client = $cf['ClientForeign'];
            }
            else if ($soukromy == 2) { //firma
                $this->loadModel('CompanyForeign');
                $cf = $this->CompanyForeign->read(null, $client_id);
                $_client = $cf['CompanyForeign'];
            }

            $data['Client'] = $_client;
        }
        else {
            $this->loadModel('Client');
            $data = $this->Client->read(null, $client_id);
        }
        $data = am($data, $find);

        $this->loadModel('AuditEstate');
        $this->AuditEstate->bindModel(array(
            'belongsTo' => array(
                'AtCompany2' => array('className' => 'AtCompany', 'foreignKey' => 'at_company_id'),
                'AtProject2' => array('className' => 'AtProject', 'foreignKey' => 'at_project_id'),
                'AtProjectCentre2' => array('className' => 'AtProjectCentre', 'foreignKey' => 'at_project_centre_id'),
            )
        ));
        $ae = $this->AuditEstate->read(array(
            'AuditEstate.*',
            'AtCompany2.name', 'AtCompany2.ulice', 'AtCompany2.psc', 'AtCompany2.mesto',
            'AtCompany2.ico', 'AtCompany2.registration_in',
            'AtProject2.name', 'AtProjectCentre2.name',
        ), $find['ConnectionAuditEstate']['audit_estate_id']);

        //stav majetku
        $this->loadModel('AuditEstateStav');
        $stav = $this->AuditEstateStav->find('all', array(
            'conditions' => array('audit_estate_id' => $audit_estate_id, 'kos' => 0),
            'order' => 'date_stav DESC',
            'fields' => array('type')
        ));
        if (!$stav)
            $data = am($data, array('AuditEstateStav' => array('name' => null), '0' => array('all_states' => null)));
        else {
            $last_stav = $all_states = null;
            foreach ($stav as $i => $s_item) {
                $all_states .= $this->audit_estate_type_list[$s_item['AuditEstateStav']['type']] . "<br />";
                if ($i == 0)
                    $last_stav = $this->audit_estate_type_list[$s_item['AuditEstateStav']['type']];
            }
            $data = am($data, array('AuditEstateStav' => array('name' => $last_stav), '0' => array('all_states' => $all_states)));
        }
        $data = am($data, $ae);
        //pr($data);

        if ($soukromy == false) {
            $this->loadModel('ConnectionClientAtCompanyWorkPosition');
            $this->ConnectionClientAtCompanyWorkPosition->bindModel(array(
                'belongsTo' => array('AtProject', 'AtCompany', 'AtProjectCentre')
            ));
            $connection = $this->ConnectionClientAtCompanyWorkPosition->find('first', array(
                'conditions' => array(
                    'client_id' => $find['ConnectionAuditEstate']['client_id'],
                    'at_company_id' => $find['ConnectionAuditEstate']['at_company_id'],
                    //'datum_to' => '0000-00-00'
                )
            ));
            if (!$connection)
                die('Nenalezeno příslušné zaměstnání');
            $data = am($connection, $data);
        }

        $this->loadModel('FormTemplate');
        $datas = $this->FormTemplate->read(null, $form_id);

        preg_match_all('@##([a-zA-z0-9\.\_\|]*)##@', $datas['FormTemplate']['text'], $text2);
        foreach ($text2[1] as $key => $item) {
            $text2[1][$key] = $this->transfer_to_html($item, $data);
        }

        $data_ = str_replace($text2[0], $text2[1], $datas['FormTemplate']['text']);
        $this->set('data', $data_);

        //$this->render('generate');
    }

    private function transfer_to_html($value, $data)
    {
        if (strpos($value, '|') != false)
            list($value, $arg) = explode('|', $value);


        if (strpos($value, '.') != false) {
            list($model, $col) = explode('.', $value);
            //pr($data);
            $value = (isset($data[$model][$col]) ? $data[$model][$col] : '');
        }

        if (isset($arg))
            switch ($arg) {
                case 'date':
                    $value = date('d.m.Y', strtotime($value));
                    break;
                case 'today_date':
                    $value = date('d.m.Y');
                    break;
                case 'mena':
                    $arr = array(0 => ',- EUR', 1 => ',- CZK');
                    $value = (isset($arr[$value]) ? $arr[$value] : '');
                    break;
                case 'typ_smlouvy':
                    $arr = array(1 => 'Smlouva na dobu neurčitou', 2 => 'Smlouva na dobu určitou');
                    $value = (isset($arr[$value]) ? $arr[$value] : $arr[1]);

                    if ($value == 2 && isset($data['ConnectionClientAtCompanyWorkPosition']['expirace_smlouvy'])) {
                        $value .= ' (' . date('d.m.Y', strtotime($data['ConnectionClientAtCompanyWorkPosition']['expirace_smlouvy'])) . ')';
                    }
                    break;
            }


        return $value;
    }

    function get_stredisko($id = null)
    {
        $conditions = array(
            'kos' => 0,
            //'status' => 1
        );
        if ($id != null) {
            $conditions['at_project_id'] = $id;
        }
        die(json_encode($this->get_list('AtProjectCentre', $conditions)));
    }


    /**
     * Funkce pro správu historie majetku
     */
    function manage_estate_admin($audit_estate_id = null)
    {
        if ($audit_estate_id != null) {
            $this->set('id', $audit_estate_id);

            $this->loadModel('ConnectionAuditEstate');
            $this->ConnectionAuditEstate->bindModel(array(
                'belongsTo' => am($this->ConnectionAuditEstate->standart_bind, array('AuditEstate'))
            ));
            $connections = $this->ConnectionAuditEstate->find('all', array(
                'conditions' => array(
                    'ConnectionAuditEstate.audit_estate_id' => $audit_estate_id,
                    'ConnectionAuditEstate.kos' => 0,
                    //'ConnectionAuditEstate.to_date'=>'0000-00-00'
                ),
                'fields' => am($this->ConnectionAuditEstate->spolecnost_klient_fields,
                    array('ConnectionAuditEstate.*',
                        'AuditEstate.hire_price',
                        'IF(ConnectionAuditEstate.created = ConnectionAuditEstate.to_date,0,
                                sum_finally_hire_price(ConnectionAuditEstate.created,IF(ConnectionAuditEstate.to_date = "0000-00-00",NOW(),ConnectionAuditEstate.to_date),AuditEstate.hire_price)
                             ) as finally_price')
                )
            ));
            $this->set('connections', $connections);
            //pr($connections);

            $this->render('manage_estate_admin/index');
        }
        else
            die('Nenalezneo ID');
    }

    /**
     * /prava datumu přidělneí a odebrani
     */
    function edit_estate_datum($connection_audit_estate_id = null)
    {
        $this->loadModel('ConnectionAuditEstate');
        if (empty($this->data)) {
            if ($connection_audit_estate_id == null)
                die('<br/><br/><br/><br/><p align="center"><em>Spojeni nebylo nalezeno.</em></p>');
            $this->data = $this->ConnectionAuditEstate->read(array('id', 'description', 'client_id', 'at_company_id', 'audit_estate_id', 'to_date', 'created', 'private_use'), $connection_audit_estate_id);
            $this->data['ConnectionAuditEstate']['old_to_date'] = $this->data['ConnectionAuditEstate']['to_date'];

            $this->render('manage_estate_admin/edit');
        }
        else {
            $audit_estate_id = $this->data['ConnectionAuditEstate']['audit_estate_id'];

            if ($this->ConnectionAuditEstate->save($this->data)) {
                $exist = $this->ConnectionAuditEstate->find('all', array(
                    'conditions' => array(
                        'audit_estate_id' => $audit_estate_id,
                        'kos' => 0,
                        'ConnectionAuditEstate.to_date' => '0000-00-00'
                    )
                ));
                $this->loadModel('AuditEstate');
                $this->AuditEstate->id = $audit_estate_id;

                /**
                 * Melo by se ukladat pouze pokud je zmena stavu
                 * dodelani az po update
                 */
                $stav = 1;
                if (!$exist) {
                    $stav = 0;
                }
                $this->AuditEstate->saveField('stav', $stav);

                if ($this->data['ConnectionAuditEstate']['old_to_date'] == '0000-00-00' && $this->data['ConnectionAuditEstate']['to_date'] != '') {
                    $connection = $this->ConnectionAuditEstate->read(array('client_foreign_id', 'company_foreign_id', 'cinnost_id', 'type', 'client_id', 'at_company_id', 'centre'), $this->data['ConnectionAuditEstate']['id']);
                    $majetek = $this->AuditEstate->read(null, $this->data['ConnectionAuditEstate']['audit_estate_id']);
                    $cinnost = self::load_for_email($connection['ConnectionAuditEstate']['cinnost_id'], 'AtProjectCentre', array('name'));
                    $comp = self::load_for_email($majetek['AuditEstate']['at_company_id'], 'AtCompany', array('name'));
                    $st = self::load_for_email($majetek['AuditEstate']['at_project_centre_id'], 'AtProjectCentre', array('name', 'spravce_majetku_id', 'reditel_strediska_id'));
                    $project = self::load_for_email($majetek['AuditEstate']['at_project_id'], 'AtProject', array('name', 'manager_id', 'project_spravce_majetku_id'));

                    $typ = $connection['ConnectionAuditEstate']['type'];
                    $cms_user_mail = array();
                    if ($typ == 1) {
                        $client = self::load_for_email($connection['ConnectionAuditEstate']['client_id'], 'Client', array('name', 'email'));
                        $comp_komu = self::load_for_email($connection['ConnectionAuditEstate']['at_company_id'], 'AtCompany', array('name'));

                        /**
                         * Pokud client_id je prirazeno nejakemu cms_user, tak vyhledej jeho mail a zasli notifikaci
                         */
                        $this->loadModel('CmsUser');
                        $f_cms = $this->CmsUser->find('first', array('conditions' => array('at_employee_id' => $connection['ConnectionAuditEstate']['client_id'])));
                        if ($f_cms) {
                            $cms_user_mail[] = $f_cms['CmsUser']['email'];
                        }
                        else if ($client['Client']['email'] != '') { //pokud ne tak ho zkus vytahnout z interniho zamestance
                            $cms_user_mail[] = $client['Client']['email'];
                        }
                    }
                    else { //soukrome ucely
                        //udaje o soukrome spolecnosti
                        if ($connection['ConnectionAuditEstate']['company_foreign_id'] != '') {
                            $_comp_komu = self::load_for_email($connection['ConnectionAuditEstate']['company_foreign_id'], 'CompanyForeign', array('name'));
                            $comp_komu['AtCompany'] = $_comp_komu['CompanyForeign'];
                        }
                        else
                            $comp_komu['AtCompany']['name'] = '<neuvedeno>';

                        //udaje o soukrome osobe
                        if ($connection['ConnectionAuditEstate']['client_foreign_id'] != '') {
                            $_client = self::load_for_email($connection['ConnectionAuditEstate']['client_foreign_id'], 'ClientForeign', array('name', 'email'));
                            $client['Client'] = $_client['ClientForeign'];
                            if (isset($client['Client']['email']) && $client['Client']['email'] == '')
                                $cms_user_mail[] = $client['Client']['email'];
                        }
                        else
                            $client['Client']['name'] = '<neuvedeno>';
                    }

                    $ucely = null;
                    if ($this->data['ConnectionAuditEstate']['private_use'] == 1 || $this->data['ConnectionAuditEstate']['private_use'] == 'on') {
                        $ucely = '<br />Majetek je použit pro soukromé účely.<br />';
                    }

                    //stav majetku
                    $this->loadModel('AuditEstateStav');
                    $stav = $this->AuditEstateStav->find('first', array(
                        'conditions' => array('audit_estate_id' => $this->data['ConnectionAuditEstate']['audit_estate_id'], 'kos' => 0),
                        'order' => 'created DESC', //posledni pridany
                        'fields' => array('type', 'description')
                    ));


                    $this->AuditEstate->bindModel(array(
                        'belongsTo' => array(
                            'AtProject' => array(
                                'foreignKey' => 'at_project_id'
                            )
                        )
                    ));
                    $majetek = $this->AuditEstate->read(null, $this->data['ConnectionAuditEstate']['audit_estate_id']);

                    $this->loadModel('CmsUser');
                    $usr = $this->CmsUser->read('email', $majetek['AtProject']['manager_id']);
                    $cms_user_mail[] = $usr['CmsUser']['email'];

                    $replace_list = array(
                        '##Komu.cinnost##' => $cinnost['AtProjectCentre']['name'],
                        '##AtCompany.name##' => $comp['AtCompany']['name'],
                        '##AtProjectCentre.name##' => $st['AtProjectCentre']['name'],
                        '##odpisovatelny##' => $this->ano_ne_checkbox[$majetek['AuditEstate']['odpisovatelny'] == 'on' || $majetek['AuditEstate']['odpisovatelny'] == 1 ? 1 : 0],
                        '##mena##' => $this->currency_list[$majetek['AuditEstate']['currency']],
                        '##porizeni##' => self::mail_date($majetek['AuditEstate']['payment_date']),
                        '##elimination_date##' => self::mail_date($majetek['AuditEstate']['elimination_date']),
                        '##vracen##' => self::mail_date($this->data['ConnectionAuditEstate']['to_date']),
                        '##Komu.name##' => $client['Client']['name'],
                        '##Komu.firma##' => $comp_komu['AtCompany']['name'],
                        '##Komu.stredisko##' => $connection['ConnectionAuditEstate']['centre'],
                        '##CmsUser.name##' => $this->logged_user['CmsUser']['name'],
                        '##soukrome_ucely##' => $ucely,
                        '##stav_majetku##' => $this->audit_estate_type_list[$stav['AuditEstateStav']['type']],
                        '##AuditEstateStav.description##' => $stav['AuditEstateStav']['description']
                    );
                    foreach ($majetek['AuditEstate'] as $item => $value) {
                        $replace_list['##AuditEstate.' . $item . '##'] = $value;
                    }

                    if ($typ == 1) {
                        $this->Email->set_company_data(array(
                            'project_manager_id' => $project['AtProject']['manager_id'],
                            'spravce_majetku_id' => $st['AtProjectCentre']['spravce_majetku_id'],
                            'project_spravce_majetku_id' => $project['AtProject']['project_spravce_majetku_id'],
                            'reditel_strediska_id' => $st['AtProjectCentre']['reditel_strediska_id']
                        ));
                    }

                    //ucetni
                    if ($this->data['ConnectionAuditEstate']['private_use'] == 1 || $this->data['ConnectionAuditEstate']['private_use'] == 'on')
                        $this->Email->send_from_template_new(46, $cms_user_mail, $replace_list);
                    else { //stara sablona
                        $this->Email->send_from_template_new(32, $cms_user_mail, $replace_list);
                    }
                }

                die(json_encode(array('result' => true)));
            } else
                die(json_encode(array('result' => false)));
        }
    }

    /**
     * Overeni zda datum vraceni majetku, je v dobe kdy byl dany klient zamestnan.
     */
    function check_return_date($date, $client_id, $at_company_id)
    {
        /*if($_SERVER['REMOTE_ADDR'] == '90.176.43.89'){
              pr($date);    pr($client_id);    pr($at_company_id);
        } */
        
        $this->loadModel('ConnectionClientAtCompanyWorkPosition');
        $connection = $this->ConnectionClientAtCompanyWorkPosition->find('first', array(
            'conditions' => array(
                'client_id' => $client_id,
                'at_company_id' => $at_company_id,
                '(datum_to = "0000-00-00" OR datum_to >= "' . $date . '")',
                'kos' => 0
            )
        ));
        if ($connection) {
            die(json_encode(array('result' => true)));
        }
        else
            die(json_encode(array('result' => false, 'message' => 'V tomto datumu nebyl klient zaměstnán. Prosím zadejte korektní datum, které bude v době zaměstnání klienta.')));
    }


    /**
     * smazani connection
     */
    function delete_connection_estate($connection_audit_estate_id = null, $audit_estate_id = null)
    {
        $this->loadModel('ConnectionAuditEstate');
        if ($connection_audit_estate_id == null)
            die(json_encode(array('result' => false)));

        $this->ConnectionAuditEstate->id = $connection_audit_estate_id;
        if ($this->ConnectionAuditEstate->saveField('kos', 1)) {
            $exist = $this->ConnectionAuditEstate->find('all', array(
                'conditions' => array(
                    'audit_estate_id' => $audit_estate_id,
                    'kos' => 0,
                    'ConnectionAuditEstate.to_date' => '0000-00-00'
                )
            ));

            if (!$exist) { //v pripade ze neni zadny dalsi clovke aktivni, musime zmenit stav majetku
                $this->loadModel('AuditEstate');
                $this->AuditEstate->id = $audit_estate_id;
                $this->AuditEstate->saveField('stav', 0);
            }

            die(json_encode(array('result' => true)));
        } else
            die(json_encode(array('result' => false)));

    }

    /**
     * smazani connection
     */
    function delete_stav($stav_id = null)
    {
        $this->loadModel('AuditEstateStav');
        if ($stav_id == null)
            die(json_encode(array('result' => false)));

        $this->AuditEstateStav->id = $stav_id;
        if ($this->AuditEstateStav->saveField('kos', 1)) {
            die(json_encode(array('result' => true)));
        } else
            die(json_encode(array('result' => false)));

    }


    /**
     * Zmena fa pro jednotlive
     */
    function change_fa($val, $id)
    {
        $this->loadModel('ConnectionAuditEstate');
        $this->ConnectionAuditEstate->bindModel(array(
            'belongsTo' => array(
                'AuditEstate'
            )
        ));

        if ($val == 1) {
            $audit_estate = $this->ConnectionAuditEstate->read(array(
                'AuditEstate.hire_price',
                'ConnectionAuditEstate.to_date'
            ), $id);
        } else if ($val == 0) {
            $audit_estate = $this->ConnectionAuditEstate->read(array(
                'IF(ConnectionAuditEstate.created = ConnectionAuditEstate.to_date,0,
                                            sum_finally_hire_price(ConnectionAuditEstate.created,IF(ConnectionAuditEstate.to_date = "0000-00-00",NOW(),ConnectionAuditEstate.to_date),AuditEstate.hire_price)
                                         ) as finally_price'
            ), $id);
        }

        if ($this->ConnectionAuditEstate->save(array(
            'id' => $id,
            'fa' => $val
        ))
        ) {
            die(json_encode(array('result' => true, 'price' => ($val == 1)?round($audit_estate['AuditEstate']['hire_price']/date('t',strtotime($audit_estate['ConnectionAuditEstate']['to_date'])),2):$audit_estate[0]['finally_price'])));
        } else {
            die(json_encode(array('result' => false, 'message' => 'Nepodařilo se uložit záznam do DB')));
        }
    }

    /**
     * funkce pro vytvoreni exportu do excelu - CSV
     * podle filtrace vyber dane klienty a vygeneruj je do csv
     */
    function export_excel()
    {
        Configure::write('debug', 0);

        $start = microtime();
        $start = explode(" ", $start);
        $start = $start [1] + $start [0];

        $fields_sql = array(fields);
        $criteria = $this->ViewIndex->filtration();


        header("Pragma: public"); // požadováno
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false); // požadováno u některých prohlížečů
        header("Content-Transfer-Encoding: binary");
        Header('Content-Type: application/octet-stream');
        Header('Content-Disposition: attachment; filename="' . date('Ymd_His') . '.csv"');


        /**
         * Celkovy pocet zaznamu
         */
        $limit = 100;
        $page = 1;
        $this->AuditEstate->bindModel(array(
            'belongsTo' => array('AtCompany', 'AtProjectCentre'),
            'hasOne' => array(
                'ConnectionAuditEstate' => array(
                    'conditions' => array(
                        'ConnectionAuditEstate.kos' => 0,
                        'ConnectionAuditEstate.to_date' => '0000-00-00'
                    )
                )
            ),
            'joinSpec' => array(
                'ClientZam' => array(
                    'className' => 'ConnectionClientAtCompanyWorkPosition',
                    'primaryKey' => 'ClientZam.client_id',
                    'foreignKey' => 'ConnectionAuditEstate.client_id',
                    'conditions' => array(
                        'ClientZam.kos' => 0,
                        'ClientZam.datum_to' => '0000-00-00'
                    )
                ),
                'Client' => array(
                    'className' => 'Client',
                    'primaryKey' => 'Client.id',
                    'foreignKey' => 'ConnectionAuditEstate.client_id'
                ),
                'ClientForeign' => array(
                    'className' => 'ClientForeign',
                    'primaryKey' => 'ClientForeign.id',
                    'foreignKey' => 'ConnectionAuditEstate.client_foreign_id'
                ),
                'SpravceMajetku' => array(
                    'className' => 'CmsUser',
                    'primaryKey' => 'SpravceMajetku.id',
                    'foreignKey' => 'AtProjectCentre.spravce_majetku_id'
                )
            )
        ), false);
        $count = $this->AuditEstate->find('first',
            array(
                'fields' => array("COUNT(DISTINCT AuditEstate.id) as count"),
                'conditions' => am($criteria, array('AuditEstate.kos' => 0)),
                'recursive' => 1
            )
        );
        $count = $count[0]['count'];

        // hlavicka
        foreach ($this->renderSetting['items'] as &$item_setting) {
            list($caption, $model, $col, $type, $fnc) = explode('|', $item_setting);
            $item_setting = compact(array("caption", "model", "col", "type", "fnc"));
            if ($type != 'hidden') echo '"' . iconv('UTF-8', 'Windows-1250', $caption) . '";';
        }
        echo "\n";
        unset($item_setting, $caption, $model, $col, $type, $fnc);

        $str_array = array("<br/>" => ', ', ':' => ',', ';' => ',', '?' => '', '#' => ' ');
        /*
         * Cyklicky vypis dat po $limit zaznamu
         */
        for ($exported = 0; $exported < $count; $exported += $limit) {
            $this->AuditEstate->bindModel(array(
                'belongsTo' => array('AtCompany', 'AtProjectCentre'),
                'hasOne' => array(
                    'ConnectionAuditEstate' => array(
                        'conditions' => array(
                            'ConnectionAuditEstate.kos' => 0,
                            'ConnectionAuditEstate.to_date' => '0000-00-00'
                        )
                    )
                ),
                'joinSpec' => array(
                    'ClientZam' => array(
                        'className' => 'ConnectionClientAtCompanyWorkPosition',
                        'primaryKey' => 'ClientZam.client_id',
                        'foreignKey' => 'ConnectionAuditEstate.client_id',
                        'conditions' => array(
                            'ClientZam.kos' => 0,
                            'ClientZam.datum_to' => '0000-00-00'
                        )
                    ),
                    'Client' => array(
                        'className' => 'Client',
                        'primaryKey' => 'Client.id',
                        'foreignKey' => 'ConnectionAuditEstate.client_id'
                    ),
                    'ClientForeign' => array(
                        'className' => 'ClientForeign',
                        'primaryKey' => 'ClientForeign.id',
                        'foreignKey' => 'ConnectionAuditEstate.client_foreign_id'
                    ),
                    'SpravceMajetku' => array(
                        'className' => 'CmsUser',
                        'primaryKey' => 'SpravceMajetku.id',
                        'foreignKey' => 'AtProjectCentre.spravce_majetku_id'
                    )
                )
            ), false);
            foreach ($this->AuditEstate->find('all', array(
                'fields' => $fields_sql,
                'conditions' => am($criteria, array('AuditEstate.kos' => 0)),
                'group' => 'AuditEstate.id',
                'limit' => $limit,
                'page' => $page,
                'recursive' => 1,
                'order' => 'AuditEstate.id DESC'
            )) as $item) {
                foreach ($this->renderSetting['items'] as $key => $td) {
                    if ($td['type'] != 'hidden') echo '"' . iconv('UTF-8', 'Windows-1250', strtr($this->ViewIndex->generate_td($item, $td, $this->viewVars), $str_array)) . '";';
                }
                echo "\n";
            }
            $page++;
        }

        //time
        $end = microtime();
        $end = explode(" ", $end);
        $end = $end [1] + $end [0];
        echo 'Generate in ' . ($end - $start) . 's';
        echo "\n";

        die();
    }

}

?>