<?php
Configure::write('debug',1);
class AtEmployeesController extends AppController
{
    var $name = 'AtEmployees';
    var $helpers = array('htmlExt', 'Pagination', 'ViewIndex', 'FileInput');
    var $components = array('ViewIndex', 'RequestHandler', 'Email', 'IntEmployee', 'Upload');
    var $uses = array('ConnectionClientAtCompanyWorkPosition');
    var $renderSetting = array(
        'bindModel' => array(
            'belongsTo' => array('AtCompany', 'Client', 'AtProjectCentreWorkPosition', 'AtProjectCentre', 'AtCompanyMoneyItem', 'AtProject'),
            'joinSpec' => array(
                'AtProjectCinnost' => array('className' => 'AtProjectCentre', 'primaryKey' => 'AtProjectCinnost.id', 'foreignKey' => 'ConnectionClientAtCompanyWorkPosition.at_project_cinnost_id')
            )
        ),
        'SQLfields' => 'AtProjectCinnost.name, ConnectionClientAtCompanyWorkPosition.*,AtCompany.name,Client.id,Client.prijmeni,Client.jmeno,
                        AtProjectCentreWorkPosition.name,AtProject.name,AtProjectCentre.name,AtCompanyMoneyItem.name,
                        ConnectionClientAtCompanyWorkPosition.datum_nastupu,
                        IF(typ_smlouvy = 2,ConnectionClientAtCompanyWorkPosition.expirace_smlouvy,"") as exp_smlouvy
                        ',
        'SQLcondition' => array('datum_to' => '0000-00-00'),
        'controller' => 'at_employees',
        'page_caption' => 'interní zaměstnanci',
        'sortBy' => 'ConnectionClientAtCompanyWorkPosition.datum_nastupu.DESC',
        'top_action' => array(
            'export_excel' => 'Export Excel|export_excel|Export Excel|export_excel',
        ),
        'filtration' => array(
            'Client-name|' => 'text|Jméno|',
            'ConnectionClientAtCompanyWorkPosition-at_project_id' => 'select|Projekt|project_list',
            'ConnectionClientAtCompanyWorkPosition-at_company_id' => 'select|Firma|company_list',
            'AtProjectCentreWorkPosition-name|' => 'text|Profese|',
        ),
        'items' => array(
            'id' => 'ID|Client|id|text|',
            'surname' => 'Přijmení|Client|prijmeni|text|',
            'name' => 'Jméno|Client|jmeno|text|',
            'profese' => 'Profese|AtProjectCentreWorkPosition|name|text|',
            'projekt' => 'Projekt|AtProject|name|text|',
            'firma' => 'Firma|AtCompany|name|text|',
            'centre' => 'Sředisko|AtProjectCentre|name|text|',
            'cinnost' => 'Činnost|AtProjectCinnost|name|text|',
            'forma' => 'Forma zaměstnání|AtCompanyMoneyItem|name|text|',
            'created' => 'Datum nástupu|ConnectionClientAtCompanyWorkPosition|datum_nastupu|date|',
            'expiration' => 'Expirace smlouvy|0|exp_smlouvy|date|',
        ),
        'posibility' => array(
            'edit' => 'edit|Editace položky|edit',
            'attach' => 'attachs|Přílohy|attach',
            'vyhodit' => 'rozvazat_prac_pomer_int|Rozvázat pracovní poměr|rozvazat_prac_pomer_int',
            'edit_datum_zamestnani' => 'edit_datum_zamestnani|Úprava datumu zaměstnání|edit_datum_zamestnani',
            'edit_pp' => 'edit_prac_pomer|Úprava zaměstnání|edit_prac_pomer',
            'zmena_pp_int' => 'zmena_pp_int|Změna pracovního poměru|zmena_pp_int',
            'cinnost' => 'cinnost|Nastavení činnosti|cinnost',
        ),
        'posibility_link' => array(
            'edit' => 'Client.id/ConnectionClientAtCompanyWorkPosition.id',
            'rozvazat_prac_pomer_int' => 'Client.id/ConnectionClientAtCompanyWorkPosition.id',
            'edit_datum_zamestnani' => 'ConnectionClientAtCompanyWorkPosition.id',
            'edit_prac_pomer' => 'ConnectionClientAtCompanyWorkPosition.id',
            'cinnost' => 'ConnectionClientAtCompanyWorkPosition.id',
            'zmena_pp_int' => 'Client.id/ConnectionClientAtCompanyWorkPosition.id',
        ),
        'domwin_setting' => array(
            'sizes' => '[800,900]',
            'scrollbars' => true,
            'languages' => true,
        ),
        'only_his' => array(
            'all_set' => true,
            'in_col' => 'at_project_id',
            'col' => 'id',
            'table' => 'wapis__at_projects',
            'where_col' => array('manager_id', 'office_manager_id'),
            'where_join' => 'OR'
        ),
        'only_his2' => array(
            'select_connection' => true,
            'where_model' => 'ConnectionClientAtCompanyWorkPosition',
            'where_col' => 'at_project_id'
        )
    );


    function index()
    {
        $this->set('fastlinks', array('ATEP' => '/', 'Klienti - interní nábor' => '#'));

        $this->set('project_list', $this->get_list('AtProject'));
        $this->set('company_list', $this->get_list('AtCompany'));

        if ($this->RequestHandler->isAjax()) {
            $this->render('../system/items');
        } else {
            $this->set('scripts', array('uploader/uploader'));
            $this->set('styles', array('../js/uploader/uploader'));
            $this->render('../system/index');
        }
    }


    function edit($id = null, $connection_id = null)
    {
        $domwin = $show = null;
        echo $this->requestAction('clients/edit/' . $id . '/' . $domwin . '/' . $show, array('at_interni' => true, 'con_id' => $connection_id));
        die();
    }

    /**
     * Seznam priloh
     *
     * @param $client_id
     * @return view
     * @access public
     **/
    function attachs($client_id)
    {
        echo $this->requestAction('clients/attachs/' . $client_id);
        die();
    }

    function cinnost($connection_id = null)
    {
        $this->loadModel('ConnectionClientAtCompanyWorkPosition');
        if (empty($this->data)) {
            if ($connection_id == null)
                die('Nenalezeno ID spojeni!');

            $this->ConnectionClientAtCompanyWorkPosition->bindModel(array('belongsTo' => array('AtProject')));
            $this->data = $this->ConnectionClientAtCompanyWorkPosition->read(array('ConnectionClientAtCompanyWorkPosition.id','at_project_centre_id','at_project_cinnost_id','AtProject.name','at_project_id'), $connection_id);
            $this->set('centre_list', $this->get_list('AtProjectCentre', array('at_project_id' => $this->data['ConnectionClientAtCompanyWorkPosition']['at_project_id'],'parent_id'=>0)));
            $this->set('cinnost_list', $this->get_list('AtProjectCentre', array('parent_id' => $this->data['ConnectionClientAtCompanyWorkPosition']['at_project_centre_id'])));
        }
        else {
            $this->ConnectionClientAtCompanyWorkPosition->save($this->data);
            die(json_encode(array('result' => true)));
        }
    }


    function rozvazat_prac_pomer_int($client_id, $connection_id)
    {
        if (!isset($connection_id) || $connection_id == null)
            die('Špatně nastavené parametry spojení - connection_id!');

        if (empty($this->data)) {
            // nacteni formularu k danemu klientovi
            $this->loadModel('SettingReasonAtSacking');
            $this->set('duvod_propusteni_list', $this->SettingReasonAtSacking->find('list', array(
                'conditions' => array(
                    'SettingReasonAtSacking.kos' => 0
                )
            )));
            unset($this->SettingReasonAtSacking);

            $this->set('client_id', $client_id);
            $this->set('connection_id', $connection_id);

            $this->loadModel('ConnectionAuditEstate');
            $this->ConnectionAuditEstate->bindModel(array(
                'belongsTo' => array('AuditEstate')
            ));
            $ma_majetek = $this->ConnectionAuditEstate->find('all', array(
                'conditions' => array(
                    'ConnectionAuditEstate.to_date' => '0000-00-00',
                    'ConnectionAuditEstate.client_id' => $client_id,
                    'ConnectionAuditEstate.kos' => 0,
                    'AuditEstate.kos' => 0
                )
            ));

            if ($ma_majetek) {
                $text = '<br/><br/><br/><br/><p align="center"><em>Uživatel má stále přiřazen nějaký majetek!!!</em></p>';
                $i = 1;
                foreach ($ma_majetek as $item) {
                    $text .= $i . '. <strong>' . $item['AuditEstate']['name'] . '</strong>, interni číslo: ' . $item['AuditEstate']['internal_number'] . '<br />';
                    $i++;
                }
                die($text);
            }

            $this->ConnectionAuditEstate->bindModel(array(
                'belongsTo' => array('AuditEstate'),
                'joinSpec' => array(
                    'Renting' => array(
                        'className' => 'ConnectionAuditEstate',
                        'primaryKey' => 'Renting.id',
                        'foreignKey' => 'ConnectionAuditEstate.renting_connection'
                    ),
                    'Client' => array(
                        'className' => 'Client',
                        'primaryKey' => 'Client.id',
                        'foreignKey' => 'Renting.client_id'
                    )
                )
            ));
            $pujcil_majetek_leasing = $this->ConnectionAuditEstate->find('all', array(
                'conditions' => array(
                    'ConnectionAuditEstate.to_date != ' => '0000-00-00',
                    'Renting.to_date' => '0000-00-00',
                    'ConnectionAuditEstate.renting_connection IS NOT NULL',
                    'ConnectionAuditEstate.client_id' => $client_id,
                    'ConnectionAuditEstate.kos' => 0,
                    'ConnectionAuditEstate.kos'
                ),
                'fields' => array('*')//array('Client.name', 'AuditEstate.name', 'AuditEstate.internal_number'),
            ));
            if ($pujcil_majetek_leasing) {
                $text = '<br/><br/><br/><br/><p align="center"><em>Uživatel má pujčené majetky leasingovým zaměstnancům!!!</em></p>';
                $i = 1;
                foreach ($pujcil_majetek_leasing as $item) {
                    $text .= $i . '. <strong>' . $item['AuditEstate']['name'] . '</strong>, interni číslo: ' . $item['AuditEstate']['internal_number'] . ', osobě: ' . $item['Client']['name'] . '<br />';
                    $i++;
                }
                die($text);
            }

            $this->render('rozvazat_prac_pomer');
        } else
        {

            $this->loadModel('ConnectionClientAtCompanyWorkPosition');
            $conection = $this->ConnectionClientAtCompanyWorkPosition->find('first', array('conditions' =>
            array('client_id' => $client_id, 'datum_to' => '0000-00-00')));
            if (!$conection)
                die(json_encode(array('result' => false, 'message' =>
                'Chyba aplikace, nenalezeno spojeni mezi klientem a pracovní pozici')));

            if ($this->data['to'] < $conection['ConnectionClientAtCompanyWorkPosition']['datum_nastupu'])
                die(json_encode(array('result' => false, 'message' =>
                'Vámi zadané datum propuštění je menší než dautm nástupu!!! Datum nástumu je ' .
                    $conection['ConnectionClientAtCompanyWorkPosition']['datum_nastupu'])));

            /**
             * Zjisteni zda ma int. zam. nejake OPP nasklade
             * Pokud ano musime je vratit rovnou na sklad opp
             */
            $this->loadModel('CmsUser');
            $cu = $this->CmsUser->find('first', array('conditions' => array('at_employee_id' => $client_id, 'kos' => 0)));
            if ($cu) {
                $this->loadModel('OppItem');
                $this->loadModel('OppOrderItem');

                $items = $this->OppOrderItem->find('all', array(
                    'conditions' => array(
                        'cms_user_id' => $cu['CmsUser']['id'],
                        'count > 0',
                        'kos' => 0
                    ),
                    'fields' => array('id', 'name', 'type_id', 'size', 'count')
                ));

                $opp_item_ids = array();
                foreach ($items as $item) {
                    $exist = $this->OppItem->find('first', array('conditions' => array('name' => $item['OppOrderItem']['name'], 'size' => $item['OppOrderItem']['size'], 'type' => $item['OppOrderItem']['type_id'], 'kos' => 0)));
                    if ($exist) {
                        $opp_item_ids[] = array(
                            'id' => $exist['OppItem']['id'],
                            'count' => $item['OppOrderItem']['count']
                        );
                    }
                    //vyresetujeme na polozce objednace pocet
                    $this->OppOrderItem->id = $item['OppOrderItem']['id'];
                    $this->OppOrderItem->saveField('count', 0);
                }

                /**
                 * vratime opp na sklad
                 */
                foreach ($opp_item_ids as $opp_item) {
                    $this->OppItem->query('UPDATE wapis__opp_items SET count = count + ' . $opp_item['count'] . ' WHERE id = ' . $opp_item['id']);
                }
            }


            //company
            $this->loadModel('AtCompany');
            $comp = $this->AtCompany->read('name', $conection['ConnectionClientAtCompanyWorkPosition']['at_company_id']);
            unset($this->AtCompany);


            //ulozeni data
            $this->ConnectionClientAtCompanyWorkPosition->id = $conection['ConnectionClientAtCompanyWorkPosition']['id'];
            $this->ConnectionClientAtCompanyWorkPosition->saveField('datum_to', $this->data["to"]);
            $this->ConnectionClientAtCompanyWorkPosition->saveField('reason_at_sacking', $this->data["reason_at_sacking"]);


            //
            $this->loadModel('AtProjectCentreWorkPosition');
            $cwp = $this->AtProjectCentreWorkPosition->read('name', $conection['ConnectionClientAtCompanyWorkPosition']['at_project_centre_work_position_id']);
            unset($this->AtProjectCentreWorkPosition);

            $this->loadModel('AtCompanyMoneyItem');
            $money = $this->AtCompanyMoneyItem->read('name', $conection['ConnectionClientAtCompanyWorkPosition']['at_company_money_item_id']);
            unset($this->AtCompanyMoneyItem);

            $this->loadModel('AtProject');
            $project = $this->AtProject->read(array('name', 'manager_id', 'office_manager_id'), $conection['ConnectionClientAtCompanyWorkPosition']['at_project_id']);
            unset($this->AtProject);

            $this->loadModel('AtProjectCentre');
            $centre = $this->AtProjectCentre->read('name', $conection['ConnectionClientAtCompanyWorkPosition']['at_project_centre_id']);
            $centre2 = $this->AtProjectCentre->read('name', $conection['ConnectionClientAtCompanyWorkPosition']['at_project_cinnost_id']);
            unset($this->AtProjectCentre);

            $this->loadModel('SettingReasonAtSacking');
            $sras = $this->SettingReasonAtSacking->read('name', $this->data['reason_at_sacking']);
            unset($this->SettingReasonAtSacking);

            //client
            $this->loadModel('Client');
            $cl = $this->Client->read(null, $client_id);

            $replace_list = array(
                '##Company.name##' => $comp['AtCompany']['name'],
                '##cinnost_name##' =>$centre2['AtProjectCentre']["name"],
                '##CompanyWorkPosition.name##' => $cwp["AtProjectCentreWorkPosition"]["name"],
                '##AtCompanyMoneyItem.name##' => $money["AtCompanyMoneyItem"]["name"],
                '##AtProject.name##' => $project['AtProject']["name"],
                '##AtProjectCentre.name##' => $centre['AtProjectCentre']["name"],
                '##To##' => $this->data["to"],
                '##Client.name##' => $cl["Client"]["name"],
                '##CmsUser.name##' => $this->logged_user["CmsUser"]["name"],
                '##ReasonAtSacking##' => $sras["SettingReasonAtSacking"]['name'],
                '##Kvalita.prace##' => $this->hodnoceni_list2[$this->data['ClientRating']["kvalita_prace"]],
                '##Spolehlivost##' => $this->hodnoceni_list2[$this->data['ClientRating']["spolehlivost"]],
                '##Alkohol##' => $this->hodnoceni_list3[$this->data['ClientRating']["alkohol"]],
                '##Blacklist##' => $this->doporuceni_pro_nabor[$this->data['ClientRating']["blacklist"]],
                '##Zamestnan##' => $this->ano_ne_list[$this->data['ClientRating']["zamestnan"]],
                '##Message.text##' => $this->data['ClientRating']["text"]
            );
            $this->Email->set_company_data(array('project_manager_id' => $project['AtProject']['manager_id'], 'office_manager_id' => $project['AtProject']['office_manager_id']));
            $this->Email->send_from_template_new(25, array(), $replace_list);

            // ulozeni stavu clienta
            $this->Client->saveField('stav', 0);

            if ($cl['Client']['parent_id'] != 0)
                $this->Client->saveField('kos', 1);

            unset($this->Client);

            //ulozeni hodnoceni
            $this->loadModel('ClientRating');
            $this->data['ClientRating']['client_id'] = $client_id;
            $this->data['ClientRating']['cms_user_id'] = $this->logged_user["CmsUser"]["id"];
            $this->ClientRating->save($this->data['ClientRating']);
            unset($this->ClientRating);

            /**
             * Zrusit pristup do ATEPu!!!
             */
            $this->IntEmployee->delete_account($client_id);

            die(json_encode(array('result' => true)));
        }

    }

    /**
     * Kontrola zda klient by nevlastnil nejaky majetek po datumu propusteni
     * coz nesmi za zadnych okolnosti nastat
     */
    function check_return_date_with_estates($client_id, $date = null)
    {
        $this->loadModel('ConnectionAuditEstate');
        $this->ConnectionAuditEstate->bindModel(array(
            'belongsTo' => array('AuditEstate')
        ));
        $ma_majetek = $this->ConnectionAuditEstate->find('all', array(
            'conditions' => array(
                'ConnectionAuditEstate.to_date > "' . $date . '"',
                'ConnectionAuditEstate.client_id' => $client_id,
                'ConnectionAuditEstate.kos' => 0,
                'AuditEstate.kos' => 0
            )
        ));
        if ($ma_majetek) {
            $message = "Toto datum nelze použít, zaměstnanec by vlastnil po propuštění tyto majetky:\n";
            $i = 1;
            foreach ($ma_majetek as $item) {
                $message .= $i . '. ' . $item['AuditEstate']['name'] . ', interni číslo: ' . $item['AuditEstate']['internal_number'] . ",datum vrácení: " . $item['ConnectionAuditEstate']['to_date'] . "\n";
                $i++;
            }

            die(json_encode(array('result' => false, 'message' => $message)));
        }
        else {
            die(json_encode(array('result' => true)));
        }
    }


    function edit_prac_pomer($connection_id = null)
    {
        $this->loadModel('ConnectionClientAtCompanyWorkPosition');
        if (empty($this->data)) {
            if ($connection_id == null)
                die('Nenalezeno ID spojeni!');
            $this->ConnectionClientAtCompanyWorkPosition->bindModel(array('belongsTo' => array('AtProject', 'AtCompany')));
            $this->data = $this->ConnectionClientAtCompanyWorkPosition->read(null, $connection_id);

            $con = array(
                'kos' => 0
            );

            $this->loadModel('Client');
            $cl = $this->Client->read(array('email'), $this->data['ConnectionClientAtCompanyWorkPosition']['client_id']);
            $this->data['Client'] = $cl['Client'];

            $this->set('centre_list', $this->get_list('AtProjectCentre', array('at_project_id' => $this->data['ConnectionClientAtCompanyWorkPosition']['at_project_id'], 'parent_id'=>0)));
            $this->set('cinnost_list', $this->get_list('AtProjectCentre', array('parent_id' => $this->data['ConnectionClientAtCompanyWorkPosition']['at_project_centre_id'])));
            $this->set('work_position_list', $this->get_list('AtProjectCentreWorkPosition', array('at_project_centre_id' => $this->data['ConnectionClientAtCompanyWorkPosition']['at_project_centre_id'])));

            $this->set('money_item_list', $this->get_list('AtCompanyMoneyItem'));
        }
        else {
            $old_ids = $this->ConnectionClientAtCompanyWorkPosition->find(
                'first',
                array(
                    'fields' => array(
                        'at_company_id',
                        'at_project_id',
                        'at_project_centre_id',
                        'at_project_cinnost_id',
                        'at_project_centre_work_position_id',
                        'at_company_money_item_id'
                    ),
                    'conditions' => array(
                        'id' => $this->data['ConnectionClientAtCompanyWorkPosition']['id']
                    )
                )
            );

            if ($this->ConnectionClientAtCompanyWorkPosition->save($this->data)) {
                $this->loadModel('ConnectionAuditEstate');
                $connections = $this->ConnectionAuditEstate->find('all', array(
                    'conditions' => array(
                        'connection_client_at_company_work_position_id IS NULL',
                        'client_id' => $this->data['ConnectionClientAtCompanyWorkPosition']['client_id'],
                        'created >= "' . $this->data['ConnectionClientAtCompanyWorkPosition']['datum_nastupu'] . '"',
                        '(to_date <= "' . $this->data['ConnectionClientAtCompanyWorkPosition']['datum_to'] . '" || to_date = "0000-00-00")',
                        'kos' => 0
                    )
                ));

                if (count($connections) > 0) {
                    $this->ConnectionAuditEstate->updateAll(
                        array(
                            'ConnectionAuditEstate.connection_client_at_company_work_position_id' => $this->ConnectionClientAtCompanyWorkPosition->id
                        ),
                        array(
                            'connection_client_at_company_work_position_id IS NULL',
                            'client_id' => $user['ConnectionClientAtCompanyWorkPosition']['client_id'],
                            'created >= "' . $user['ConnectionClientAtCompanyWorkPosition']['datum_nastupu'] . '"',
                            '(to_date <= "' . $user['ConnectionClientAtCompanyWorkPosition']['datum_to'] . '" || to_date = "0000-00-00")',
                            'kos' => 0
                        )
                    );
                }


                $this->loadModel('Client');
                $client = $this->Client->read(array('id', 'email', 'name', 'jmeno', 'prijmeni', 'mesto'), $this->data['ConnectionClientAtCompanyWorkPosition']['client_id']);
                unset($this->Client);

                $this->loadModel('AtProjectCentre');
                $at_centre = $this->AtProjectCentre->read(array('name', 'reditel_strediska_id'), $this->data['ConnectionClientAtCompanyWorkPosition']['at_project_centre_id']);
                $at_centre_old = $this->AtProjectCentre->read(array('name'), $old_ids['ConnectionClientAtCompanyWorkPosition']['at_project_centre_id']);

                if (empty($this->data['ConnectionClientAtCompanyWorkPosition']['at_project_cinnost_id'])){
                    $cinnost = array('AtProjectCentre'=>array('name'=>'Nespecifikováno'));
                } else {
                    $cinnost = $this->AtProjectCentre->read(array('name'), $this->data['ConnectionClientAtCompanyWorkPosition']['at_project_cinnost_id']);
                }

                if (empty($old_ids['ConnectionClientAtCompanyWorkPosition']['at_project_cinnost_id'])){
                    $cinnost_old = array('AtProjectCentre'=>array('name'=>'Nespecifikováno'));
                } else {
                    $cinnost_old = $this->AtProjectCentre->read(array('name'), $old_ids['ConnectionClientAtCompanyWorkPosition']['at_project_cinnost_id']);
                }


                unset($this->AtProjectCentre);

                $this->loadModel('AtProjectCentreWorkPosition');
                $at_wp = $this->AtProjectCentreWorkPosition->read(array('name'), $this->data['ConnectionClientAtCompanyWorkPosition']['at_project_centre_work_position_id']);
                $at_wp_old = $this->AtProjectCentreWorkPosition->read(array('name'), $old_ids['ConnectionClientAtCompanyWorkPosition']['at_project_centre_work_position_id']);
                unset($this->AtProjectCentreWorkPosition);

                $this->loadModel('AtCompanyMoneyItem');
                $at_mi = $this->AtCompanyMoneyItem->read(array('name'), $this->data['ConnectionClientAtCompanyWorkPosition']['at_company_money_item_id']);
                $at_mi_old = $this->AtCompanyMoneyItem->read(array('name'), $old_ids['ConnectionClientAtCompanyWorkPosition']['at_company_money_item_id']);
                unset($this->AtCompanyMoneyItem);

                $this->loadModel('AtProject');
                $project_old = $this->AtProject->read(array('name'), $old_ids['ConnectionClientAtCompanyWorkPosition']['at_project_id']);
                unset($this->AtProject);

                $this->loadModel('AtCompany');
                $comp_old = $this->AtCompany->read('name', $old_ids['ConnectionClientAtCompanyWorkPosition']['at_company_id']);
                unset($this->AtCompany);

                $replace_list = array(
                    '##datum##' => date('d.m.Y', strtotime($this->data['datum'])),
                    '##Client.name##' => $client['Client']['name'],

                    '##AtProject.name##' => $project_old['AtProject']['name'],
                    '##AtCompany.name##' => $comp_old['AtCompany']['name'],

                    '##cinnost_name_old##' =>$cinnost_old['AtProjectCentre']["name"],
                    '##cinnost_name##' =>$cinnost['AtProjectCentre']["name"],

                    '##AtProjectCentre.name_old##' => $at_centre_old['AtProjectCentre']['name'],
                    '##AtProjectCentreWorkPosition.name_old##' => $at_wp_old['AtProjectCentreWorkPosition']['name'],
                    '##AtCompanyMoneyItem.name_old##' => $at_mi_old['AtCompanyMoneyItem']['name'],

                    '##AtProjectCentre.name##' => $at_centre['AtProjectCentre']['name'],
                    '##AtProjectCentreWorkPosition.name##' => $at_wp['AtProjectCentreWorkPosition']['name'],
                    '##AtCompanyMoneyItem.name##' => $at_mi['AtCompanyMoneyItem']['name'],
                );
                $this->Email->set_company_data(array(
                    'project_manager_id' => $project['AtProject']['manager_id'],
                    'office_manager_id' => $project['AtProject']['office_manager_id'],
                    'reditel_strediska_id' => $at_centre['AtProjectCentre']['reditel_strediska_id']
                ));
                $this->Email->send_from_template_new(24, array(), $replace_list);

                /**
                 * Pokud se zmenila profese je treba osetrit uzivatelske konto v ATEPu
                 */
                if ($old_ids['ConnectionClientAtCompanyWorkPosition']['at_project_centre_work_position_id'] != $this->data['ConnectionClientAtCompanyWorkPosition']['at_project_centre_work_position_id']) {

                    $acc = $this->IntEmployee->client_have_cms_account($this->data['ConnectionClientAtCompanyWorkPosition']['client_id']);

                    $this->loadModel('CmsUser');
                    $this->CmsUser->id = $acc['CmsUser']['id'];
                    if ($this->data['generate_login'] == 1) { //potrebuje login
                        if ($acc !== false) {
                            $this->CmsUser->save(array('status' => 1, 'cms_group_id' => $this->data['group_id']));
                        }
                        else {
                            $heslo = $this->IntEmployee->generate_int_zam_login($client, $this->data['group_id']);
                            $replace_list = array(
                                '##Client.email##' => $client['Client']['email'],
                                '##heslo##' => $heslo,
                            );
                            $this->Email->clear_company_data();
                            $this->Email->send_from_template_new(33, array($client['Client']['email']), $replace_list);
                        }
                    } else {
                        /**
                         * Nepotrebuje tato profese login
                         * musime zjisit zda nahodou nema pristup a pokud ano tak ho smazat!
                         */
                        if ($acc !== false) {
                            $this->CmsUser->saveField('status', 0);
                        }

                    }
                }

                die(json_encode(array('result' => true)));
            }
        }
    }


    function edit_datum_zamestnani($connection_id = null)
    {
        $this->loadModel('ConnectionClientAtCompanyWorkPosition');
        if (empty($this->data)) {
            if ($connection_id == null)
                die('Nenalezeno ID spojeni!');
            $this->data = $this->ConnectionClientAtCompanyWorkPosition->read(null, $connection_id);
        }
        else {
            if ($this->ConnectionClientAtCompanyWorkPosition->save($this->data)) {
                die(json_encode(array('result' => true)));
            }
        }
    }

    function check_expirate()
    {
        $this->loadModel('ConnectionClientAtCompanyWorkPosition');

        $date = new DateTime();
        $date->modify('+1 month');

        $smlouvy = $this->ConnectionClientAtCompanyWorkPosition->find('all', array(
            'conditions' => array(
                'typ_smlouvy' => 2,
                'expirace_smlouvy' => $date->format('Y-m-d')
            )
        ));

        $this->loadModel('AtProjectCentreWorkPosition');
        $this->loadModel('AtProjectCentreWorkPosition');
        $this->loadModel('AtCompanyMoneyItem');
        $this->loadModel('AtProject');
        $this->loadModel('AtProjectCentre');
        $this->loadModel('AtCompany');
        $this->loadModel('Client');

        foreach ($smlouvy as $smlouva) {

            $cwp = $this->AtProjectCentreWorkPosition->read('name', $smlouva['ConnectionClientAtCompanyWorkPosition']['at_project_centre_work_position_id']);
            $money = $this->AtCompanyMoneyItem->read('name', $smlouva['ConnectionClientAtCompanyWorkPosition']['at_company_money_item_id']);
            $project = $this->AtProject->read(array('name', 'manager_id', 'office_manager_id'), $smlouva['ConnectionClientAtCompanyWorkPosition']['at_project_id']);
            $centre = $this->AtProjectCentre->read('name', $smlouva['ConnectionClientAtCompanyWorkPosition']['at_project_centre_id']);
            $comp = $this->AtCompany->read('name', $smlouva['ConnectionClientAtCompanyWorkPosition']['at_company_id']);
            $cl = $this->Client->read(null, $smlouva['ConnectionClientAtCompanyWorkPosition']['client_id']);

            $replace_list = array(
                '##Company.name##' => $comp['AtCompany']['name'],
                '##CompanyWorkPosition.name##' => $cwp["AtProjectCentreWorkPosition"]["name"],
                '##AtCompanyMoneyItem.name##' => $money["AtCompanyMoneyItem"]["name"],
                '##AtProject.name##' => $project['AtProject']["name"],
                '##AtProjectCentre.name##' => $centre['AtProjectCentre']["name"],
                '##expirace##' => $date->format('d.m.Y'),
                '##Client.name##' => $cl["Client"]["name"]
            );
            $this->Email->set_company_data(array('project_manager_id' => $project['AtProject']['manager_id'], 'office_manager_id' => $project['AtProject']['office_manager_id']));
            $this->Email->send_from_template_new(26, array(), $replace_list);
        }
    }

    /**
     * funkce pro vytvoreni exportu do excelu - CSV
     * podle filtrace vyber dane klienty a vygeneruj je do csv
     */
    function export_excel()
    {
        Configure::write('debug', 1);

        $start = microtime();
        $start = explode(" ", $start);
        $start = $start [1] + $start [0];

        $fields_sql = array(
            "Client.id",
            "Client.jmeno",
            "Client.prijmeni",
            "Client.email",
            "Client.private_email",
            "AtProjectCentreWorkPosition.name",
            "AtProject.name",
            "AtCompany.name",
            "AtProjectCentre.name",
            "AtCompanyMoneyItem.name",
            "ConnectionClientAtCompanyWorkPosition.datum_nastupu",
            "IF(typ_smlouvy = 2,ConnectionClientAtCompanyWorkPosition.expirace_smlouvy,'') as exp_smlouvy"
        );

        $criteria = $this->ViewIndex->filtration();


        header("Pragma: public"); // požadováno
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false); // požadováno u některých prohlížečů
        header("Content-Transfer-Encoding: binary");
        Header('Content-Type: application/octet-stream');
        Header('Content-Disposition: attachment; filename="' . date('Ymd_His') . '.csv"');


        /*
        * Celkovy pocet zaznamu
        */

        $limit = 100;
        $page = 1;
        $this->ConnectionClientAtCompanyWorkPosition->bindModel(array(
            'belongsTo' => array('AtCompany', 'Client', 'AtProjectCentreWorkPosition', 'AtProjectCentre', 'AtCompanyMoneyItem', 'AtProject')
        ), false);
        $count = $this->ConnectionClientAtCompanyWorkPosition->find('first',
            array(
                'fields' => array("COUNT(DISTINCT ConnectionClientAtCompanyWorkPosition.id) as count"),
                'conditions' => am($criteria, array('datum_to' => '0000-00-00')),
                'recursive' => 1
            )
        );
        $count = $count[0]['count'];
        $this->renderSetting['items'] = am($this->renderSetting['items'], array(
            'email' => 'Email|Client|email|text|',
            'private_email' => 'Soukromý Email|Client|private_email|text|'
        ));
        // hlavicka
        foreach ($this->renderSetting['items'] as &$item_setting) {
            list($caption, $model, $col, $type, $fnc) = explode('|', $item_setting);
            $item_setting = compact(array("caption", "model", "col", "type", "fnc"));
            if ($type != 'hidden') echo '"' . iconv('UTF-8', 'Windows-1250', $caption) . '";';
        }
        echo "\n";
        unset($item_setting, $caption, $model, $col, $type, $fnc);

        $str_array = array("<br/>" => ', ', ':' => ',', ';' => ',', '?' => '', '#' => ' ');
        /*
         * Cyklicky vypis dat po $limit zaznamu
         */

        for ($exported = 0; $exported < $count; $exported += $limit) {
            foreach ($this->ConnectionClientAtCompanyWorkPosition->find('all', array(
                'fields' => $fields_sql,
                'conditions' => am($criteria, array('datum_to' => '0000-00-00')),
                //'group'=>'ClientView2.id',
                'limit' => $limit,
                'page' => $page,
                'recursive' => 1,
                'order' => 'datum_nastupu DESC'
            )) as $item) {
                foreach ($this->renderSetting['items'] as $key => $td) {
                    if ($td['type'] != 'hidden') echo '"' . iconv('UTF-8', 'Windows-1250', strtr($this->ViewIndex->generate_td($item, $td, $this->viewVars), $str_array)) . '";';
                }
                echo "\n";
            }
            $page++;
        }

        //time
        $end = microtime();
        $end = explode(" ", $end);
        $end = $end [1] + $end [0];
        echo 'Generate in ' . ($end - $start) . 's';
        echo "\n";

        die();
    }


    function test()
    {
        die('nefunkcni');
        if (!isset($this->ConnectionClientAtCompanyWorkPosition))
            $this->loadModel('ConnectionClientAtCompanyWorkPosition');

        $this->ConnectionClientAtCompanyWorkPosition->bindModel(array(
            'belongsTo' => array('AtProjectCentreWorkPosition', 'Client'),
            'joinSpec' => array(
                'CmsUser' => array(
                    'primaryKey' => 'CmsUser.at_employee_id',
                    'foreignKey' => 'ConnectionClientAtCompanyWorkPosition.client_id',
                    'conditions' => array('CmsUser.status' => 1, 'CmsUser.kos' => 0)
                )
            )
        ));

        $con = array(
            'AtProjectCentreWorkPosition.cms_group_id != ""',
            'ConnectionClientAtCompanyWorkPosition.datum_to' => '0000-00-00',
            'ConnectionClientAtCompanyWorkPosition.kos' => 0
        );
        $users = $this->ConnectionClientAtCompanyWorkPosition->find('all', array(
            'conditions' => $con,
            'fields' => array('CmsUser.id', 'Client.id', 'Client.email', 'Client.jmeno', 'Client.prijmeni', 'Client.name', 'AtProjectCentreWorkPosition.cms_group_id')
        ));
        if ($users) {
            $this->loadModel('CmsUser');
            $a = $b = 0;
            foreach ($users as $user) {
                if (!isset($user['CmsUser']['id']) && $user['CmsUser']['id'] == '') {

                    $client = null;
                    $client['Client'] = $user['Client'];
                    $heslo = $this->IntEmployee->generate_int_zam_login($client, $user['AtProjectCentreWorkPosition']['cms_group_id']);
                    $replace_list = array(
                        '##Client.email##' => $client['Client']['email'],
                        '##heslo##' => $heslo,
                    );
                    $this->Email->send_from_template_new(33, array($client['Client']['email']), $replace_list);
                } else {
                    $acc = $this->CmsUser->read('cms_group_id', $user['CmsUser']['id']);
                    if ($acc['CmsUser']['cms_group_id'] != $user['AtProjectCentreWorkPosition']['cms_group_id']) {

                        //echo $user['Client']['name'].' má '.$acc['CmsUser']['cms_group_id'].', ale má mít('.$user['AtProjectCentreWorkPosition']['cms_group_id'].')<br />';

                    }

                }
            }

            echo $a . ' / ' . $b;
        }

        die();
    }

    function actualizate_connection_id_after_2012()
    {
        //die('vypnuto');
        Configure::write('debug', 1);

        if (!isset($this->ConnectionClientAtCompanyWorkPosition))
            $this->loadModel('ConnectionClientAtCompanyWorkPosition');

        $con = array(
            'ConnectionClientAtCompanyWorkPosition.updated > "2012-01-01 00:00:00"',
            '(ConnectionClientAtCompanyWorkPosition.datum_to = "0000-00-00" || ConnectionClientAtCompanyWorkPosition.datum_to > "2012-01-01")',
            'ConnectionClientAtCompanyWorkPosition.kos' => 0,
            'ConnectionClientAtCompanyWorkPosition.client_id' => 22692
        );
        $users = $this->ConnectionClientAtCompanyWorkPosition->find('all', array(
            'conditions' => $con,
        ));
        if ($users) {
            $this->loadModel('ConnectionAuditEstate');
            foreach ($users as $user) {
                $connections = $this->ConnectionAuditEstate->find('all', array(
                    'conditions' => array(
                        'connection_client_at_company_work_position_id IS NULL',
                        'client_id' => $user['ConnectionClientAtCompanyWorkPosition']['client_id'],
                        //'created >= "'.$user['ConnectionClientAtCompanyWorkPosition']['datum_nastupu'].'"',
                        //'(to_date <= "'.$user['ConnectionClientAtCompanyWorkPosition']['datum_to'].'" || to_date = "0000-00-00")',
                        'kos' => 0
                    )
                ));
                pr($user);
                if (count($connections) > 0) {
                    pr($connections);
                    /*
                      $this->ConnectionAuditEstate->updateAll(
                          array(
                              'ConnectionAuditEstate.connection_client_at_company_work_position_id'=>$user['ConnectionClientAtCompanyWorkPosition']['id']
                          ),
                          array(
                              'connection_client_at_company_work_position_id IS NULL',
                              'client_id'=>$user['ConnectionClientAtCompanyWorkPosition']['client_id'],
                              'created >= "'.$user['ConnectionClientAtCompanyWorkPosition']['datum_nastupu'].'"',
                              '(to_date <= "'.$user['ConnectionClientAtCompanyWorkPosition']['datum_to'].'" || to_date = "0000-00-00")',
                              'kos'=>0
                          )
                     );
                    */
                }
            }
        }

        die();
    }


    /**
     * ZMENA PP - Int. zaměstnanci
     * Nova forma zmeny pracovniho procesu, kdy ukoncuje stare zamestnani a vytvari nove
     * Tedy ukoncuje jedno connection(povinny -> [to]) a vytvari nove s -> [from]
     * @author Sol
     * @created 18.4.12
     */
    function zmena_pp_int($client_id = null, $connection_id = null)
    {
        Configure::write('debug', 1);
        if (empty($this->data)) {
            if ($connection_id == null || $client_id == null)
                die('chyba vstupu');

            $detail = $this->ConnectionClientAtCompanyWorkPosition->read(null, $connection_id);
            $this->data['Old'] = $detail['ConnectionClientAtCompanyWorkPosition'];

            $con = array(
                'kos' => 0
            );

            $allow_superior_id = array(1, 6);
            if (!in_array($this->logged_user['CmsGroup']['cms_group_superior_id'], $allow_superior_id)) {
                $con = array(
                    'kos' => 0,
                    'OR' => array(
                        'manager_id' => $this->logged_user['CmsUser']['id'],
                        'office_manager_id' => $this->logged_user['CmsUser']['id']
                    )
                );
            }

            $this->loadModel('Client');
            $cl = $this->Client->read(array('email'), $client_id);
            $this->data['Client'] = $cl['Client'];

            $this->loadModel('AtProject');
            //$this->set('company_list',$this->AtCompany->find('list',array('conditions'=>array('kos'=>0))));
            $this->set('project_list', $this->AtProject->find('list', array('conditions' => $con)));
            $this->set('money_item_list', $this->get_list('AtCompanyMoneyItem'));
            $this->set('cinnost_list', array());
        }
        else { //save

            //ukonceni stareho prac. pomeru
            $this->ConnectionClientAtCompanyWorkPosition->id = $this->data['Old']['id'];
            $this->ConnectionClientAtCompanyWorkPosition->saveField('datum_to', $this->data['do']);

            //vytovreni noveho pracovniho pomeru
            $this->ConnectionClientAtCompanyWorkPosition->id = null;
            $this->data['ConnectionClientAtCompanyWorkPosition']['client_id'] = $this->data['Old']['client_id'];
            $this->data['ConnectionClientAtCompanyWorkPosition']['cms_user_id'] = $this->logged_user['CmsUser']['id'];
            $this->ConnectionClientAtCompanyWorkPosition->save($this->data);

            /**
             * prevod majetku
             * stare connection musime ukoncit a vytvorit nove s novym conneciton a datumem od
             */
            $this->loadModel('ConnectionAuditEstate');
            $majetky = $this->ConnectionAuditEstate->find('all', array(
                'conditions' => array(
                    'client_id' => $this->data['Old']['client_id'],
                    'to_date' => "0000-00-00",
                    'kos' => 0
                )
            ));
            if ($majetky) {
                foreach ($majetky as $majetek) {
                    //ulozeni stare connection majektu
                    $this->ConnectionAuditEstate->id = $majetek['ConnectionAuditEstate']['id'];
                    $this->ConnectionAuditEstate->saveField('to_date', $this->data['do']);

                    //ulozeni nove connection
                    $this->ConnectionAuditEstate->id = null;
                    $new_majetek = array(
                        'cms_user_id' => $this->logged_user['CmsUser']['id'],
                        'connection_client_at_company_work_position_id' => $this->ConnectionClientAtCompanyWorkPosition->id,
                        'created' => $this->data['ConnectionClientAtCompanyWorkPosition']['datum_nastupu'],
                        'at_company_id' => $this->data['ConnectionClientAtCompanyWorkPosition']['at_company_id'],
                        'audit_estate_id' => $majetek['ConnectionAuditEstate']['audit_estate_id'],
                        'client_id' => $majetek['ConnectionAuditEstate']['client_id']
                    );
                    $this->ConnectionAuditEstate->save($new_majetek);
                }
            }


            /**
             * Pristup do ATEPu
             */
            if (isset($this->data['generate_login']) && $this->data['generate_login'] == 1) {
                $this->IntEmployee->change_int_zam_account($this->data['Old']['client_id'], $this->data['group_id']);
            }
            else {
                //zruseni - v novem ho neportrebuje
                $this->IntEmployee->delete_account($this->data['Old']['client_id']);
            }

            die(json_encode(array('result' => true, 'message' => 'Pracovní poměr byl změněn.')));

        }
    }
}

?>