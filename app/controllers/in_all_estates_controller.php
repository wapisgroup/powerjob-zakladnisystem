<?php
	Configure::write('debug',1);
	define('fields','AtCompany.name,
        ( 
		  SUM(if(AuditEstate.currency = 1,AuditEstate.first_cost,0))
		) as sum_czk,
        ( 
          SUM(if(AuditEstate.currency = 0,AuditEstate.first_cost,0))  
		) as sum_eur,
        ( 
          SUM(if(AuditEstate.elimination_request = 1 AND AuditEstate.elimination_date = "0000-00-00",1,0))  
		) as zadosti,
        ( 
          SUM(if(AuditEstate.elimination_request = 1 AND AuditEstate.elimination_date != "0000-00-00",1,0))  
		) as vyrazeno
        	
	');
class InAllEstatesController extends AppController {
	var $name = 'InAllEstates';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('AuditEstate');
	var $renderSetting = array(
		'bindModel'	=> array('belongsTo'=>array('AtCompany')),
		'SQLfields' => array(fields),
        'SQLcondition' => array('AuditEstate.at_company_id != ""','AuditEstate.kos'=>0),
		'controller'=> 'in_all_estates',
		'page_caption'=>'Celkem za AT',
		'sortBy'=>'AtCompany.name.ASC',
        'count_col'=>'AuditEstate.at_company_id',
        'group_by'=>'AuditEstate.at_company_id',
		'top_action' => array(
			'add_item'		=>	'Přidat|edit|Pridat majetek|add',
		),
		'filtration' => array(
			//'Company-name'				=>	'text|Jméno|',		 	
		),
		'items' => array(
            'comp'       =>	'Firma|AtCompany|name|text|',
			'sum_czk'    =>	'Hodnota majetku v CZK|0|sum_czk|money2|',
            'sum_eur'    =>	'Hodnota majetku v EUR|0|sum_eur|money2|',
            'zadosti'    =>	'Počet žádosti o vyřazení|0|zadosti|text|',
            'vyrazeno'   =>	'Počet vyřazeného majetku|0|vyrazeno|text|',
		),
        'sum_row'=>'Celkem za AT',
		'posibility' => array(
			//'show'		        =>	'show|Detail položky|show'
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Evidence majetku'=>'#',$this->renderSetting['page_caption']=>'#'));

		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
  
    
   
}
?>