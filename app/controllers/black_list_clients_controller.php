<?php
Configure::write('debug',1);
class BlackListClientsController extends AppController {
	var $name = 'BlackListClients';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('ClientRating');
	var $renderSetting = array(
		'bindModel' => array(
            'belongsTo'=>array('Client'),
        ),
		'SQLfields' => '*',
		'SQLcondition' => array(
            'blacklist'=>array(2,3)
        ),
        'no_trash'=>1,
		'controller'=> 'black_list_clients',
		'page_caption'=>'Klienti - černá listina',
		'sortBy'=>'ClientRating.created.DESC',
		'top_action' => array(
           //'export_excel' => 'Export Excel|export_excel|Export Excel|export_excel',
            'add_item'		=>	'Přidat|edit|Pridat popis|add',
        ),
		'filtration' => array(
 			'Client-name|'		=>	'text|Jméno|',
 			'Client-datum_narozeni'		=>	'text|Datum narození|',
		),
		'items' => array(
			'id'		=>	'ID|Client|id|text|',
			'surname'	=>	'Přijmení|Client|prijmeni|text|',
			'name'		=>	'Jméno|Client|jmeno|text|', 
			'blacklist'=>'Doporučení pro nábor|ClientRating|blacklist|var|doporuceni_pro_nabor', 
            'text'		=>	'Poznámka|ClientRating|text|text|', 
			'created'	=>	'Vytvořeno|ClientRating|created|date|',   	
		),
		'posibility' => array(
			'edit'	=> 'edit|Karta klienta|edit',
		),
        'posibility_link' => array(
            'edit'	=>	'Client.id',
        ),    
		'domwin_setting' => array(
			'sizes' 		=> '[800,900]',
			'scrollbars'	=> true,
			'languages'		=> true,
		)
	);
    
    function beforeFilter(){
        parent::beforeFilter();
        
        if(isset($this->params['request_data']))
            $this->data = $this->params['request_data'];
    }
	
    
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Klienti - interní nábor'=>'#'));

		//$this->set('project_list', $this->get_list('AtProject'));
        //$this->set('company_list', $this->get_list('AtCompany'));
				
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	
	
	//zobrazeni karty klienta
	function edit($id = null){
		echo $this->requestAction('clients/edit/'.$id.'/domwin/only_show');
		die();
	}
    
    
    function add_to_blacklist($client_id){
        if(isset($this->params['request_data'])){
            $this->data = $this->params['request_data'];
               
            //ulozeni hodnoceni
            $this->loadModel('ClientRating');
            $this->data['ClientRating']['client_id'] = $client_id;
            $this->data['ClientRating']['cms_user_id'] = $this->logged_user["CmsUser"]["id"];
            $this->ClientRating->save($this->data['ClientRating']);
            unset($this->ClientRating);
        }    
    }


}
?>