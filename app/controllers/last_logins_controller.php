<?php
class LastLoginsController extends AppController {
	var $name = 'LastLogins';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('LastLogin');
	var $renderSetting = array(
		'controller'=>'last_logins',
		'bindModel'	=> array('belongsTo'=>array('CmsUser')),
		'SQLfields' => array('LastLogin.id','CmsUser.name','LastLogin.ip','LastLogin.created'),
		'page_caption'=>'Poslední přihlášení',
		'sortBy'=>'LastLogin.created.DESC',
		'top_action' => array(

		),
		'filtration' => array(
		//	'SettingAccommodationType-status'	=>	'select|Stav|select_stav_zadosti',
			'LastLogin-ip|'		=>	'text|IP|',
			'LastLogin-cms_user_id'	=>	'select|Uživatel|cms_user_list',
		),
		'items' => array(
			'id'		=>	'ID|LastLogin|id|text|',
			'name'		=>	'Uživatel|CmsUser|name|text|',
			'ip'		=>	'IP|LastLogin|ip|text|',
			'created'	=>	'Vytvořeno|LastLogin|created|datetime|'
		),
		'posibility' => array(
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Poslední přihlášení'=>'#'));

		$this->loadModel('CmsUser');
		$this->set('cms_user_list', $this->CmsUser->find('list', array('conditions'=>array('kos'=>0))));
		unset($this->CmsUser);

		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
}
?>