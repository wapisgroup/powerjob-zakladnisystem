<?php
//Configure::write('debug',1);
class FaqsController extends AppController {
	var $name = 'Faqs';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload');
	var $uses = array('Faq');
	var $renderSetting = array(
		'bindModel'	=> array('belongsTo'=>array('FaqGroup')),
		'SQLfields' => '*',
        'controller'=> 'faqs',
		'page_caption'=>'Často kladené otázky',
		'sortBy'=>'Faq.id.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
			'group'		    =>	'Kategorie|group|Kategorie|group'
			//'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
			//'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
			//'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
			'Faq-question'			=>	'text|Otázka|',
			'Faq-faq_group_id'		=>	'select|Kategorie|faq_group_list'	
		),
		'items' => array(
			'id'		=>	'ID|Faq|id|text|',
            'group'		=>	'Kategorie|FaqGroup|name|text|',
			'question'	=>	'Otázka|Faq|question|text|orez#100',
			'answer'	=>	'Odpověď|Faq|answer|text|orez#100',
			'contact'	=>	'Kontaktná osoba|Faq|contact|text|orez#100',
			//'created'	=>	'Vytvořeno|Faq|created|datetime|',
			//'updated'	=>	'Změněno|Faq|updated|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
            'access'	=>	'access|Přístupy|access',
            'attach'	=>	'attachs|Přílohy|edit',	
			'delete'	=>	'trash|Odstranit položku|delete'			
		),
        /**
         * nove zadavani jen sve, tzv. pristupy
         * nutne je zadat tabulku kde jsou pristupy ulozeny
         * a id pro jake se ma filtrovat prirazene zaznamy
         * group -> zda plati jen sve pro cms_group_id nebo pro cms_user_id
         */
        'only_his'=>array(
            'table'=>'wapis__faq_access',
            'col'=>'faq_id',
            'group'=>true
        )
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#',$this->renderSetting['page_caption']=>'#'));
		$this->loadModel('FaqGroup'); 
		$this->set('faq_group_list',$this->FaqGroup->find('list',array(
            'conditions'=>array('kos'=>0),
            'group'=>'name')
        ));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->set('scripts',array('uploader/uploader'));
			$this->set('styles',array('../js/uploader/uploader'));
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			// load faq group list
			$this->loadModel('FaqGroup');
			$this->set('faq_group_list',$this->FaqGroup->find('list',array('conditions'=>array('FaqGroup.kos'=>0))));

			
			if ($id != null){
				$this->data = $this->Faq->read(null,$id);
			}

			$this->render('edit');
		} else {
			$this->Faq->save($this->data['Faq']);
			die();
		}
	}
    
    
    /**
     * sekce skupiny
     */
    function group(){
        
        $this->loadModel('FaqGroup');
        $group_list = $this->FaqGroup->find('all',array(
            'conditions'=>array(
                'kos'=>0
            )
        ));       
        
        $this->set('group_list',$group_list);
        
        //render
        $this->render('group/index');
    }
    
        //EDIT
        function group_edit($id = null){
            if(empty($this->data)){
                if($id != null){
                    $this->loadModel('FaqGroup');
                    $this->data = $this->FaqGroup->read(null,$id);
                }
                
                //render
                $this->render('group/edit');
            }
            else{
                $this->loadModel('FaqGroup');
                $this->FaqGroup->save($this->data);
            }
        }
        
        //TRASH
        function group_trash($id){
            $this->loadModel('FaqGroup');
            $this->FaqGroup->id = $id;
            $this->FaqGroup->saveField('kos',1);
            unset($this->FaqGroup);
            die();
        }



    /**
     * přístupy k danému faq 
     * CMSGroup
     */
    function access($faq_id = null){
        $this->set('faq_id',$faq_id);
        
        $this->loadModel('FaqAccess');
        $this->FaqAccess->bindModel(array('belongsTo'=>array('CmsGroup')));
        $access_list = $this->FaqAccess->find('all',array(
            'conditions'=>array(
                'FaqAccess.kos'=>0,
                'FaqAccess.faq_id'=>$faq_id
            )
        ));       
        
        $this->set('access_list',$access_list);
        
        //render
        $this->render('access/index');
    }
    
        //EDIT
        function access_edit($faq_id = null,$id = null){
            if(empty($this->data)){
                if($id != null){
                    $this->loadModel('FaqAccess');
                    $this->data = $this->FaqAccess->read(null,$id);
                }
                else                
                    $this->data['FaqAccess']['faq_id'] = $faq_id;
                $this->loadModel('CmsGroup');
                $this->set('cms_group_list',$this->CmsGroup->find('list',array('conditions'=>array('kos'=>0,'status'=>1,'id NOT IN(SELECT cms_group_id FROM wapis__faq_access Where kos = 0 and faq_id = '.$faq_id.')'))));
                
                //render
                $this->render('access/edit');
            }
            else{
                $this->loadModel('FaqAccess');
                $this->FaqAccess->save($this->data);
            }
        }
        
        //TRASH
        function access_trash($id){
            $this->loadModel('FaqAccess');
            $this->FaqAccess->id = $id;
            $this->FaqAccess->saveField('kos',1);
            unset($this->FaqAccess);
            die();
        } 
        
   
   /**
 	* Seznam priloh
 	*
	* @param $faq_id
 	* @return view
 	* @access public
	**/
	function attachs($faq_id){
		$this->autoLayout = false;
		$this->loadModel('FaqAttachment'); 
		$this->FaqAttachment->bindModel(array('belongsTo'=>array('SettingAttachmentType')));
		$this->set('attachment_list', $this->FaqAttachment->findAll(array('FaqAttachment.faq_id'=>$faq_id,'FaqAttachment.kos'=>0)));
		$this->set('faq_id',$faq_id);
		unset($this->FaqAttachment);
		$this->render('attachs/index');
	}
	
	/**
 	* Editace priloh
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function attachs_edit($faq_id = null, $id = null){
		$this->autoLayout = false;
		$this->loadModel('FaqAttachment'); 
		if (empty($this->data)){
			$this->loadModel('SettingAttachmentType');
			$this->set('setting_attachment_type_list',$this->SettingAttachmentType->find('list',array('conditions'=>array('kos'=>0),'order'=>'poradi ASC')));
			unset($this->SettingAttachmentType);
            
			$this->data['FaqAttachment']['faq_id'] = $faq_id;
			if ($id != null){
				$this->data = $this->FaqAttachment->read(null,$id);
			}
			$this->render('attachs/edit');
		} else {
			$this->FaqAttachment->save($this->data);
			$this->attachs($this->data['FaqAttachment']['faq_id']);
		}
		unset($this->FaqAttachment);
	}
	/**
 	* Presun prilohy do kose
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function attachs_trash($faq_id, $id){
		$this->loadModel('FaqAttachment');
		$this->FaqAttachment->save(array('FaqAttachment'=>array('kos'=>1,'id'=>$id)));
		$this->attachs($faq_id);
		unset($this->FaqAttachment);
	}
	
	/**
 	* Nahrani prilohy na ftp
 	*
 	* @return view
 	* @access public
	**/
	function upload_attach() {
		$this->Upload->set('data_upload',$_FILES['upload_file']);
		if ($this->Upload->doit(json_decode($this->data['upload']['setting'],true))){
			echo json_encode(array('upload_file'=>array('name'=>$this->Upload->get('outputFilename')),'return'=>true));
		} else 
			echo json_encode(array('return'=>false,'message'=>$this->Upload->get('error_message')));		
		die();
	} 
	
	/**
 	* Stazeni prilohy
 	*
	* @param $file
	* @param $file_name
 	* @return download file
 	* @access public
	**/
	function  attachs_download($file,$file_name){
		$pripona = strtolower(end(Explode(".", $file)));
		$file = strtr($file,array("|"=>"/"));
		$filesize = filesize('./uploaded/'.$file);
		$cesta = "http://".$_SERVER['SERVER_NAME']."/uploaded/".$file;
				 
		header("Pragma: public"); // požadováno
	    header("Expires: 0");
	    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	    header("Cache-Control: private",false); // požadováno u některých prohlížečů
	    header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . $filesize);
		Header('Content-Type: application/octet-stream');
		Header('Content-Disposition: attachment; filename="'.$file_name.'.'.$pripona.'"');
		readfile($cesta);
		die();

	}     
    
    
    
    function import(){
        $file = './uploaded/faqs/faq.csv';
        
        
        $handle = @fopen($file, "r");
        if ($handle) {
            $i = 0;
            while (!feof($handle)) {
                $buffer = fgets($handle, 4096);
                $data = explode(';',$buffer);

                if($i > 0  && $data[0] != ''){ //prvni radek je hlavicka
                    $to_save = array(
                        'question'=>iconv('CP1250','UTF8',$data[0]),
                        'answer'=>iconv('CP1250','UTF8',$data[1]),
                        'contact'=>iconv('CP1250','UTF8',$data[2]),
                    );
                
                    $this->Faq->id = null;
                    $this->Faq->save($to_save);
                }
            
                $i++;
                
            }
            fclose($handle);
        }

       die('done'); 
    }
    
}
?>