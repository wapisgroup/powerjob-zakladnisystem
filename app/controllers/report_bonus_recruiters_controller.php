<?php
$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:date('m');

define('CURRENT_YEAR', $_GET['current_year']);
define('CURRENT_MONTH', (strlen($_GET['current_month']) == 1)?'0'.$_GET['current_month']:$_GET['current_month']);
	
class ReportBonusRecruitersController  extends AppController {
	var $name = 'ReportBonusRecruiters';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('BonusRecruiterView');
	var $renderSetting = array(
		'controller'=>'report_bonus_recruiters',
		'bindModel' => array('hasOne' => array(
				'BonusRecruiterPayment' => array(
					'className' => 'BonusRecruiterPayment', 
					'foreignKey' => 'recruiter_id',
					'conditions' => array('BonusRecruiterPayment.year'=>CURRENT_YEAR,'BonusRecruiterPayment.month'=>CURRENT_MONTH)
				)
		)),
		'SQLfields' => 'BonusRecruiterView.*,Sum(odmena) as odmena,IF(IFNULL(BonusRecruiterPayment.id,0)=0,MIN(stav), 5) as stav',
		'page_caption'=>'Odměny recruiterů pro fin. účetní',
		'sortBy'=>'BonusRecruiterView.name.ASC',
		'groupBy'=>'BonusRecruiterView.name',
		'SQLcondition'=>array('BonusRecruiterView.year =#CURRENT_YEAR# AND BonusRecruiterView.MONTH =#CURRENT_MONTH# AND celkem_hodin <> 0  GROUP BY BonusRecruiterView.id'),
		'top_action' => array(
			'multi_edit'		=>	'Vyplatit hromadně|money_all|Vyplatit hromadně|multi_edit',
		),
		'checkbox_setting' => array(
			'model'			=>	'BonusRecruiterView',
			'model2'			=>	'BonusRecruiterPayment',
			'col'			=>	'stav',
			'stav_array'	=>	array(3,4),
			'month_year'	=>	true
		),
		'filtration' => array(
			'GET-current_year'							=>	'select|Rok|actual_years_list',
			'GET-current_month'							=>	'select|Měsíc|mesice_list',
		),
		'items' => array(
			'id'	=>	'Recruiter|BonusRecruiterView|id|hidden|',				
			'rec_id'	=>	'Recruiter|BonusRecruiterView|name|text|',			
			'month'		=>	'Měsíc|BonusRecruiterView|month|text|',
			'year'		=>	'Rok|BonusRecruiterView|year|text|',	
			'stav'		=>	'Stav|BonusRecruiterView|stav|var|stav_rec_payment_list',	
			'odmena'	=>	'Odměna|BonusRecruiterView|odmena|text|',	
		),
		'posibility' => array(
			'money2' => 'money2|Vyplatit|money2'
		)
	);
	
	function index(){

		// set JS and Style
		$this->set('scripts',array('uploader/uploader','clearbox/clearbox'));
		$this->set('styles',array('../js/uploader/uploader','../js/clearbox/clearbox'));
		// set FastLinks
		$this->set('fastlinks',array('ATEP'=>'/','Nábor'=>'#','Odměny recruiterů pro fin. účetní'=>'#'));
		$this->set('stav_rec_payment_list',array(
			'' => 'Čeká na autorizaci',
			1 => 'Čeká na autorizaci',
			2 => 'Čeká na autorizaci',
			3 => 'K vyplacení',
			4 => 'K vyplacení',
			5 => 'Provize vyplacena'
		));


		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function money2($rec_id,$year,$month){
		$this->loadModel('BonusRecruiterPayment');
		$save_data['recruiter_id']=$rec_id;
		$save_data['year']=$year;
		$save_data['month']=$month;
		$this->BonusRecruiterPayment->save($save_data);
		unset($this->BonusRecruiterPayment);
	}

	function money_all(){
		$model = $this->renderSetting["checkbox_setting"]['model'];
		$saveModel = $this->renderSetting["checkbox_setting"]['model2'];
		$this->loadModel($saveModel);

		foreach($this->params['url']['data'][$model]['id'] as $key => $on){
			$save_data = array(
				'year' => $year=av(array_keys($on['year']),'0'),
				'recruiter_id'=>$key,
				'month' => av(array_keys($on['year'][$year]['month']),'0')

			);
			$this->$saveModel->id;
			$this->$saveModel->save($save_data);
		}
		unset($this->$saveModel);
		die();
	}
	
	
	
}
?>