<?php
Configure::write('debug',1);
$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');
@define('CURRENT_YEAR', $_GET['current_year']); 
@define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);
@define('CURRENT_YM',CURRENT_YEAR.'-'.CURRENT_MONTH);
	define('fields','AuditEstate.*,SpravceMajetku.name,CarMonthlyStatement.*,
        ( 
		  if(AuditEstate.stav = 1,
            (GROUP_CONCAT(Client.name SEPARATOR "<br />")),
            SpravceMajetku.name
          )  
		) as osoba,	
        ( 
		  if(AuditEstate.stav = 1,
            (GROUP_CONCAT(ConnectionAuditEstate.centre SEPARATOR "<br />")),
            (AtProjectCentre.name)
          )  
		) as stredisko,
        ( 
          CONCAT_WS(" ",IFNULL(AuditEstate.hire_price,0),if(AuditEstate.currency = 0,",- EUR",",- Kč"))  
		) as hire_price,
        IFNULL(ROUND(SUM( 
            (hire_price/'.date('t',strtotime(CURRENT_YEAR."-".CURRENT_MONTH."-01")).') * 
            (
                DATEDIFF(
                    IF(
                        DATE_FORMAT(ConnectionAuditEstate.to_date,"%Y-%m") = "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'",
                        ConnectionAuditEstate.to_date,
                        "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'-'.date('t',strtotime(CURRENT_YEAR."-".CURRENT_MONTH."-01")).'"
                    ),
                    IF(
                        DATE_FORMAT(ConnectionAuditEstate.created,"%Y-%m") = "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'",
                        ConnectionAuditEstate.created,
                        "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'-01"
                    )
                )+1
            )
        ),2),0) as sum_hire_price,
        IFNULL(ROUND(SUM(AuditEstateLossEvent.fa_price),2),0) as sum_loss_events
	');
class CarMonthlyStatementsController extends AppController {
	var $name = 'CarMonthlyStatements';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('AuditEstate');
    var $mesic = CURRENT_MONTH;
	var $rok = CURRENT_YEAR;
	var $renderSetting = array(
		'bindModel'	=> array(
			'belongsTo'=>array('AtProjectCentre'),
			'hasOne'=>array(
				'ConnectionAuditEstate'=>array(
					'conditions'=>array(
						'ConnectionAuditEstate.kos'=>0,
						'ConnectionAuditEstate.to_date'=>'0000-00-00'
					)
				),
                'CarMonthlyStatement'=>array(
					'conditions' => array(
						'CarMonthlyStatement.mesic'=> CURRENT_MONTH,
						'CarMonthlyStatement.rok'=> CURRENT_YEAR
					)
				)
			),
			'joinSpec'=>array(
			    'Client'=>array(
    				'className'=>'Client',
    				'primaryKey'=>'Client.id',
    				'foreignKey'=>'ConnectionAuditEstate.client_id'
			    ),
                 'SpravceMajetku'=>array(
    				'className'=>'CmsUser',
    				'primaryKey'=>'SpravceMajetku.id',
    				'foreignKey'=>'AtProjectCentre.spravce_majetku_id'
			    ),
                'AuditEstateLossEvent'=>array(
    				'className'=>'AuditEstateLossEvent',
    				'primaryKey'=>'AuditEstateLossEvent.audit_estate_id',
    				'foreignKey'=>'AuditEstate.id',
                    'conditions'=>array(
                        'AuditEstateLossEvent.kos'=>0,
                        'DATE_FORMAT(AuditEstateLossEvent.date_event,"%Y-%m")'=> CURRENT_YM
                    )
			    )
			)
		),
		'SQLfields' =>array(fields),
		'SQLcondition' => array(
			'AuditEstate.elimination_date IS NULL',
            'AuditEstate.kos'=>0,
            'AuditEstate.audit_estate_type_id'=>1//auta
		),
		'controller'=> 'car_monthly_statements',
		'page_caption'=>'Měsíční výkaz automobilů',
		'sortBy'=>'AuditEstate.id.DESC',
		'group_by'=>'AuditEstate.id',
		'top_action' => array(
			'add_item'		=>	'Přidat|edit|Pridat výkaz|add',
            //'export_excel' => 'Excel|export_excel|Export Excel|export_excel', 
		),
		'filtration' => array(
			'GET-current_month'							=>	'select|Měsíc|mesice_list',
            'GET-current_year'							=>	'select|Rok|actual_years_list',
		),
		'items' => array(
			'id'		=>	'ID|AuditEstate|id|text|',
			'name'		=>	'Název|AuditEstate|name|text|',
            'company_person'	=>	'Osoba|0|osoba|text|',
			'company_centre'	=>	'Středisko|0|stredisko|text|',
			'hire_price'	=>	'Měs. pronájem|0|hire_price|text|',
            'sum_hire_price'	=>	'Fakt.nájem|0|sum_hire_price|text|',
            'sum_loss_events'	=>	'Pojist.plnění|0|sum_loss_events|text|',
            'leasing'		=>	'Leasing|CarMonthlyStatement|leasing|text|',
            'insurance'		=>	'Pojištění|CarMonthlyStatement|insurance|text|',
            'cest_dan'		=>	'Cest.dan|CarMonthlyStatement|cest_dan|text|',
            'gps_data'		=>	'Gps/data|CarMonthlyStatement|gps_data|text|',
            'repairs'		=>	'Opravy|CarMonthlyStatement|repairs|text|',
            'gears_km'		=>	'Pneu/km|CarMonthlyStatement|gears_km|text|',
            'other_costs'		=>	'Ost/výd|CarMonthlyStatement|other_costs|text|',
            'yields_costs'		=>	'Výnosy/Náklady|CarMonthlyStatement|yields_costs|text|',
            'km_monthly'		=>	'Km/měs|CarMonthlyStatement|km_monthly|text|',
            'phm_monthly'		=>	'PHM/L/měs|CarMonthlyStatement|phm_monthly|text|',
            'average_consumption'		=>	'PrůmSpot|CarMonthlyStatement|average_consumption|text|',
            'normal_consumption'		=>	'Nor.Spotřeba|CarMonthlyStatement|normal_consumption|text|',
			'difference'		=>	'Rozdíl/L|CarMonthlyStatement|difference|text|'//'updated'	=>	'Změněno|AuditEstate|updated|datetime|'
		),
		'posibility' => array(
			'edit'		        =>	'edit|Editace položky|edit'		
		),
        'posibility_link'=>array(
            'edit'=>'AuditEstate.id/#CURRENT_MONTH#/#CURRENT_YEAR#'
        )       
	);
    
    function beforeRender(){
		parent::beforeRender();

		// úprava nadpisu reportu a přidání do něj datumu
        if(isset($this->viewVars['renderSetting']))
		  $this->set('spec_h1', $this->viewVars['renderSetting']['page_caption'].' za měsíc '.$this->mesice_list[ltrim($this->mesic,'0')].' a rok '.$this->rok);
	}

	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Evidence majetku'=>'#',$this->renderSetting['page_caption']=>'#'));
		          
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
    
    function edit($id = null,$month = null,$year = null){
        if(empty($this->data)){
             $this->AuditEstate->bindModel($this->renderSetting['bindModel']);
             $this->data = $this->AuditEstate->find('first',array(
                'conditions'=>array(
                    'AuditEstate.id'=>$id
                ),
                'fields'=>array(fields)
            ));
            if($this->data['CarMonthlyStatement']['id'] == ''){
                $this->data['CarMonthlyStatement']['mesic'] = $month;
                $this->data['CarMonthlyStatement']['rok'] = $year;
                $this->data['CarMonthlyStatement']['audit_estate_id'] = $id;
                $this->data['CarMonthlyStatement']['fakt_najem'] = $this->data[0]['sum_hire_price'];
                $this->data['CarMonthlyStatement']['pojistne_plneni'] = $this->data[0]['sum_loss_events'];
            }
            
        }
        else{//save
            $this->data['CarMonthlyStatement']['cms_user_id'] = $this->logged_user['CmsUser']['id'];
            $this->loadModel('CarMonthlyStatement');
            $this->CarMonthlyStatement->save($this->data);
        }
    }
    
}	
?>