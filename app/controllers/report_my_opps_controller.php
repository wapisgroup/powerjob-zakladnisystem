<?php
//Configure::write('debug',2);
class ReportMyOppsController extends AppController {
    var $name = 'ReportMyOpps';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('OppOrderItem');
	var $renderSetting = array(
		'SQLfields' => '*',
		'bindModel' => array(
            'belongsTo' => array(
                'CmsUser'=>array('className'=>'CmsUser','foreignKey'=>'cms_user_id'),
                'OppOrder',
            )
        ),
		'controller'=> 'report_my_opps',
		'page_caption'=>'Report moje OPP',
		'sortBy'=>'OppOrderItem.created.DESC',
		'top_action' => array(),
        'SQLcondition'=>array(
            'OppOrderItem.kos'=>0,
            'OppOrder.kos'=>0,
            'OppOrderItem.count >'=>0,
            'OppOrder.datum_potvrzeni != '=>'0000-00-00 00:00:00'
        ),	
        'filtration' => array(
            'OppOrder-cms_user_id' => 'select|Uživatel|cms_user_list',
            //'GET-stav'		          =>	'select|Stav|returned_opp_stav_list',
           //'GET-coo'                 => 'select|CM/COO|coo_list',
            //'GET-current_month'		  =>	'select|Měsíc|mesice_list',
            //'GET-current_year'		  =>	'select|Rok|actual_years_list',
            
            //'CompanyOrderItem-nabor' => 'select|Je nábor|select_ano',
            //'CompanyOrderItem-order_status' => 'select|Nábor status|select_order_status',
            //'Company-id' => 'select|Společnost|company_list',
        ),
		'items' => array(
			'id'				=>	'ID|OppOrderItem|id|text|',
	        'typ'				=>	'Typ|OppOrderItem|type_id|text|',
            'name'				=>	'Položka|OppOrderItem|name|text|',
            'size'				=>	'Velikost|OppOrderItem|size|text|',	
            'stav'		        =>	'Dostupných kusů|OppOrderItem|count|text|',
            'price_cz'			=>	'Cena CZ|OppOrderItem|price_cz|money2|',
            'price_eu'			=>	'Cena EU|OppOrderItem|price_eu|money2|',
            'datum'		        =>	'Datum naskladneni|OppOrderItem|created|date|',
		),
		'posibility' => array(),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'		=> true,
			'languages'	=> 'false'
		)
	);
    
    function index(){
        $this->set('cms_user_list',$this->get_list('CmsUser'));
        
        if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {			
			// set FastLinks
			$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#',$this->renderSetting['page_caption']=>'#'));
			$this->render('../system/index');
           
		}
	}
   
	
}
?>