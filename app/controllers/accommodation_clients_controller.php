<?php
//Configure::write('debug',1);

$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');
define('CURRENT_YEAR', $_GET['current_year']); 
define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);

class AccommodationClientsController  extends AppController {
	var $name = 'AccommodationClients';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('ClientWorkingHour');
	var $mesic = CURRENT_MONTH;
	var $rok = CURRENT_YEAR;
	var $renderSetting = array(
		'bindModel'	=> array(
			'belongsTo'	=>	array('Accommodation','Company','Client')
		),
		'controller'	=>	'accommodation_clients',
		'SQLfields' 	=>	'*',
		'SQLcondition'	=>  array(
			'ClientWorkingHour.kos'=>0,
            'ClientWorkingHour.year'=> CURRENT_YEAR,
			'ClientWorkingHour.month'=> CURRENT_MONTH,
			'ClientWorkingHour.accommodation_id !='=> 0
		),
		'page_caption'=>'Report ubytovaných klientů',
		'sortBy'=>'Accommodation.name.ASC',
		'top_action' => array(
             'export_excel' => 'Export Excel|export_excel|Export Excel|export_excel', 
        ),
		'filtration' => array(
            'Client-name'		                    =>	'text|Klient|',
			'ClientWorkingHour-company_id'	        =>	'select|Společnost|company_list',
		    'GET-current_month'						=>	'select|Měsíc|mesice_list',
			'ClientWorkingHour-accommodation_id'	=>	'select|Ubytovna|accommodation_list',   
            'Company-client_manager_id|coordinator_id|coordinator_id2-cmkoo'		=>	'select|CM/KOO|cm_koo_list',
            'GET-current_year'						=>	'select|Rok|actual_years_list'
        ),
		'items' => array(
			'id'				=>	'ID|ClientWorkingHour|id|hidden|',
			'accommodation'		=>	'Ubytovna|Accommodation|name|text|',
			'company'			=>	'Společnost|Company|name|text|',
			'client'			=>	'Klient|Client|name|text|',
			'accommodation_days_count'			=>	'Počet ubytovácích dnů|ClientWorkingHour|accommodation_days_count|text|',
			'year'			    =>	'Rok|ClientWorkingHour|year|text|',
			'month'			    =>	'Měsíc|ClientWorkingHour|month|var|mesice_list',
		),
		'posibility' => array(
            'show'		=>	'show|Detail položky|show',
		),
		'domwin_setting' => array(
			'sizes' 		=> '[500,150]',
			'scrollbars'	=> true,
			'languages'		=> true,
		)
	);

	

	function beforeRender(){
		parent::beforeRender();

		// úprava nadpisu reportu a přidání do něj datumu
        if(isset($this->viewVars['renderSetting']))
		   $this->set('spec_h1', $this->viewVars['renderSetting']['page_caption'].' za měsíc '.$this->mesice_list[ltrim($this->mesic,'0')].' a rok '.$this->rok);
	}
	
	function index(){
		// set FastLinks
		$this->set('fastlinks',array('ATEP'=>'/','Ubytování'=>'#',$this->viewVars['renderSetting']['page_caption']=>'#'));
		
		$this->loadModel('CmsUser');
		$this->set('cm_koo_list',$cm_koo_list = $this->CmsUser->find('list',array(
			'conditions'=>array(
				'kos'=>0,
				'status'=>1,
				'cms_group_id IN(3,4)'
			)
		)));
		
		$this->loadModel('Accommodation');
		$accommodation_list = $this->Accommodation->find('list',array(
			 'conditions'=> array('kos'=>0),
			 'order'=>'name ASC'
		 ));
		 $this->set('accommodation_list',$accommodation_list);
		 
      
  		$this->loadModel('Company');
		$company_list = $this->Company->find('list',array(
			 'conditions'=> array('kos'=>0,'status'=>1),
			 'order'=>'name ASC'
		 ));
		 $this->set('company_list',$company_list);
		 
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
		
	}
	
    
    function show($id){
        $data = $this->ClientWorkingHour->read(array('year','month','accommodation_days','company_id'),$id);
        $this->set('data',$data);
        
         // load free days for state, month, year
		$this->loadModel('SettingStatSvatek');
		$this->set('svatky',$this->SettingStatSvatek->find('list', array(
            'conditions'=>array( 
                'setting_stat_id'	=> av(av($data['ClientWorkingHour']['company_id'],'Company'),'stat_id'), 
                'mesic'=> $data['ClientWorkingHour']['month'], 
                'rok'=> $data['ClientWorkingHour']['year'] 
            ),
            'fields'=>array('id','den'),
            'order'=>'den ASC'
        ))); 				
		
        
        $this->render('show');
    }
    
     /**
     * funkce pro vytvoreni exportu do excelu - CSV
     * podle filtrace vyber dane klienty a vygeneruj je do csv
     */
    function export_excel(){
       //Configure::write('debug',1);
         
       $start = microtime ();
       $start = explode ( " " , $start );
       $start = $start [ 1 ]+ $start [ 0 ];  
        
       $fields_sql = $this->renderSetting['SQLfields'];     
       $criteria = $this->ViewIndex->filtration();        
       $this->loadModel('ClientWorkingHour');
      

         header("Pragma: public"); // požadováno
         header("Expires: 0");
         header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
         header("Cache-Control: private",false); // požadováno u některých prohlížečů
         header("Content-Transfer-Encoding: binary");
         Header('Content-Type: application/octet-stream');
         Header('Content-Disposition: attachment; filename="'.date('Ymd_His').'.csv"');

      
        /*
         * Celkovy pocet zaznamu
         */
         
        $limit = 100;   
        $page = 1;
        $this->ClientWorkingHour->bindModel($this->renderSetting['bindModel']);        
        $count = $this->ClientWorkingHour->find('first',
        	array(
        		'fields' =>  array("COUNT(DISTINCT ClientWorkingHour.id) as count"),
        		'conditions'=>am($criteria,array(
        			'ClientWorkingHour.kos'=>0,
                    'ClientWorkingHour.year'=> CURRENT_YEAR,
        			'ClientWorkingHour.month'=> CURRENT_MONTH,
        			'ClientWorkingHour.accommodation_id !='=> 0
        		)),
        		'recursive'	=>1
        	)
        );    
        $count = $count[0]['count'];

        foreach($this->renderSetting['items'] as &$item_setting){
            list($caption, $model, $col, $type, $fnc) = explode('|',$item_setting);
        	$item_setting = compact(array("caption", "model","col","type","fnc"));	
            if($type != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',$caption).'";'; 
        }
        echo "\n";   
        unset($item_setting, $caption, $model, $col, $type, $fnc);
        
        $str_array=array("<br/>"=>', ',':'=>',',';'=>',','?'=>'', '#'=>' ');
        /*
         * Cyklicky vypis dat po $limit zaznamu
         */
        for ($exported = 0; $exported < $count; $exported += $limit){
            $this->ClientWorkingHour->bindModel($this->renderSetting['bindModel']);     
            foreach($this->ClientWorkingHour->find('all',array(
                   'fields'=>$fields_sql,
                    'conditions'=>am($criteria,array(
            			'ClientWorkingHour.kos'=>0,
                        'ClientWorkingHour.year'=> CURRENT_YEAR,
            			'ClientWorkingHour.month'=> CURRENT_MONTH,
            			'ClientWorkingHour.accommodation_id !='=> 0
            		)),
                    'group'=>'ClientWorkingHour.id',
                    'limit'=>$limit,
                    'page'=>$page,
                    'recursive'=>1
            )) as $item){
                foreach($this->renderSetting['items'] as $key => $td){
                    if($td['type'] != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',strtr($this->ViewIndex->generate_td($item,$td,$this->viewVars),$str_array)).'";';     
                }
                echo "\n";  
            }
            $page++;
        }
       
        //time
         $end = microtime ();
         $end = explode ( " " , $end );
         $end = $end [ 1 ]+ $end [ 0 ]; 
         echo 'Generate in '.($end - $start).'s';
         echo "\n"; 
        
        die();
    }
}    
?>