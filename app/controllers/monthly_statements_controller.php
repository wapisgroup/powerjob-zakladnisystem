<?php
Configure::write('debug',1);
$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');
@define('CURRENT_YEAR', $_GET['current_year']); 
@define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);
/*
$pocet_dnu = date_dif(mydate(0,$item['ConnectionAuditEstate']['created'],$year.'-'.$month), mydate(1,$item['ConnectionAuditEstate']['to_date'],$year.'-'.$month));
$pocet_dnu_v_danem_mesici = date('t',strtotime("$year-$month-01"));
$price = $item['AuditEstate']['hire_price'] / $pocet_dnu_v_danem_mesici * $pocet_dnu;
*/
define('YM', CURRENT_YEAR."-".CURRENT_MONTH);

define('fields','*,
        IF(ConnectionAuditEstate.type = 1,AtCompany.name,"soukromý nájem") as spolecnost,
        IF(ConnectionAuditEstate.type = 1,AtCompany.id,0) as spol_id,
        CONCAT_WS(" ",IFNULL(ROUND(SUM(
            IF(ConnectionAuditEstate.created = ConnectionAuditEstate.to_date,
                IF (ConnectionAuditEstate.fa = 1,AuditEstate.hire_price/'.date('t',strtotime(CURRENT_YEAR."-".CURRENT_MONTH."-01")).',0),
                (hire_price/'.date('t',strtotime(CURRENT_YEAR."-".CURRENT_MONTH."-01")).') *
                (
                    DATEDIFF(
                        IF(
                            DATE_FORMAT(ConnectionAuditEstate.to_date,"%Y-%m") = "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'",
                            ConnectionAuditEstate.to_date,
                            "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'-'.date('t',strtotime(CURRENT_YEAR."-".CURRENT_MONTH."-01")).'"
                        ),
                        IF(
                            DATE_FORMAT(ConnectionAuditEstate.created,"%Y-%m") = "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'",
                            ConnectionAuditEstate.created,
                            "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'-01"
                        )
                    )+1
                )
            )
        ),2),0),if(AuditEstate.currency = 0,",- EUR",",- Kč")) as sum_hire_price, 
        ConnectionFaAuditEstate.id,
        if(ConnectionFaAuditEstate.id != "","Fakturováno","Nefakturováno") as fakturovano,
        GROUP_CONCAT(ConnectionClientAtCompanyWorkPosition.id) as cc
');
class MonthlyStatementsController extends AppController {
	var $name = 'MonthlyStatements';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('ConnectionAuditEstate');
	var $renderSetting = array(
		'bindModel'	=> array(
            'belongsTo'=>array('AuditEstate','AtCompany','CompanyForeign'),
            'joinSpec'=>array(
                'CompanyE'=>array(
                    'className'=>'AtCompany',
                    'primaryKey'=>'CompanyE.id',
                    'foreignKey'=>'AuditEstate.at_company_id'
                ),    
                'AtProjectCentre'=>array(
                    'className'=>'AtProjectCentre',
                    'primaryKey'=>'AtProjectCentre.id',
                    'foreignKey'=>'AuditEstate.at_project_centre_id',
                    'conditions' => array(
                        'type_id' => 1,
                    )
                ),
                'ConnectionFaAuditEstate'=>array(
                    'className'=>'ConnectionFaAuditEstate',
                    'foreignKey'=>'ConnectionFaAuditEstate.at_company_e_id',
                    'primaryKey'=>'AuditEstate.at_company_id',
                    'conditions'=>array(
                        'ConnectionFaAuditEstate.at_company_u_id = ConnectionAuditEstate.at_company_id',
                        'ConnectionFaAuditEstate.year'=>CURRENT_YEAR,
                        'ConnectionFaAuditEstate.month'=>CURRENT_MONTH,
                        'ConnectionFaAuditEstate.kos'=>0,
                        'ConnectionFaAuditEstate.private_use'=>0
                    )
                ),
                'ConnectionClientAtCompanyWorkPosition'=>array(
                    'foreignKey' => 'ConnectionClientAtCompanyWorkPosition.id',
                    'primaryKey' => 'ConnectionAuditEstate.connection_client_at_company_work_position_id',
                )
            )
        ),
		'SQLfields' => array(fields),
        'SQLcondition'=>array(
            'AuditEstate.kos'=>0,
            'AuditEstate.at_company_id != ""',
            '(AuditEstate.at_company_id != ConnectionAuditEstate.at_company_id OR (AuditEstate.at_company_id = ConnectionAuditEstate.at_company_id AND AtProjectCentre.name != ConnectionAuditEstate.centre))',
            'ConnectionAuditEstate.kos'=>0,
            '(ConnectionAuditEstate.private_use = 0 OR ConnectionAuditEstate.private_use IS NULL)',
            "((DATE_FORMAT(ConnectionAuditEstate.created,'%Y-%m')<= '#CURRENT_YEAR#-#CURRENT_MONTH#'))",
			'((DATE_FORMAT(ConnectionAuditEstate.to_date,"%Y-%m") >= "#CURRENT_YEAR#-#CURRENT_MONTH#") OR (ConnectionAuditEstate.to_date = "0000-00-00"))'
         ),
		'controller'=> 'monthly_statements',
		'page_caption'=>'Měsíční fakturace pronájmu',
		'sortBy'=>'CompanyE.name.ASC',
		//'group_by'=>'ConnectionAuditEstate.at_company_id,AuditEstate.at_company_id,AuditEstate.at_project_centre_id',
		'group_by'=>'ConnectionAuditEstate.at_company_id,AuditEstate.at_company_id,AuditEstate.at_project_centre_id',
        //'group_by' => 'ConnectionClientAtCompanyWorkPosition.at_company_id, ConnectionClientAtCompanyWorkPosition.at_project_centre_id, ConnectionClientAtCompanyWorkPosition.at_project_cinnost_id,AuditEstate.at_company_id,AuditEstate.at_project_centre_id',
		'top_action' => array(
			//'add_item'		=>	'Přidat|edit|Pridat majetek|add',
		),
		'filtration' => array(
			//'CompanyPerson-name'				=>	'text|Jméno|',
            'GET-current_month'							=>	'select|Měsíc|mesice_list',
			'AuditEstate-at_company_id'	    =>	'select|Firma evidence|company_list',
            'ConnectionAuditEstate-at_company_id'	    =>	'select|Firma uzivatel|company_list',
		    'GET-current_year'							=>	'select|Rok|actual_years_list',

		),
		'items' => array(
			'id'		=>	'ID|ConnectionAuditEstate|id|hidden|',
			'companye'	=>	'Firma evidence|CompanyE|name|text|',
            'centre'	=>	'Středisko(dle majetku)|AtProjectCentre|name|text|',
          //  'centre2'	=>	'Středisko uživatel|AtProjectCentre2|name|text|',
            //'cinnost'	=>	'Činnost|AtProjectCentre3|name|text|',
			'companyu'	=>	'Firma uživatel|0|spolecnost|text|',
			'sum_hire_price'=>	'Mesíční pronájem|0|sum_hire_price|text|',
            'stav'      =>	'Stav|0|fakturovano|text|'
		),
		'posibility' => array(
			'show'		        =>	'show|Detail položky|show',
            'fakturovano'		=>	'fakturovano|Fakturováno|fakturovano',
		),
        'posibility_link' => array(
            'show' => 'ConnectionAuditEstate.type/CompanyE.id/0.spol_id/AuditEstate.at_project_centre_id/#CURRENT_YEAR#/#CURRENT_MONTH#',
             'fakturovano' => 'ConnectionAuditEstate.type/CompanyE.id/0.spol_id/AuditEstate.at_project_centre_id/#CURRENT_YEAR#/#CURRENT_MONTH#'

        ),
        'only_his'=>array(
			'in_col'=>'at_project_centre_id',
            'in_table'=>'AuditEstate',
			'subQuery'=>'(SELECT id FROM wapis__at_project_centres Where spravce_majetku_id = #CmsUser.id# AND kos = 0)'
		),
        'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'	=> true,
			'languages'		=> true,
		)
	);
	function index(){
	  
		$this->set('fastlinks',array('ATEP'=>'/','Evidence majetku'=>'#',$this->renderSetting['page_caption']=>'#'));

        $this->set('company_list',$this->get_list('AtCompany'));

		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
    
    /**
     * detail majetku za fakturovanou castku
     */
	function show($type = 1,$e_company_id = null,$u_company_id = null,$at_project_centre_id = null,$year = null,$month = null,$print = false,$order_by = false,$filtr_spolecnost = false,$filtr_stredisko = false, $filtr_cinnost = false){
    	   if($year > date('Y') || ($year == date('Y') && $month > date('m')))
            die('Toto datum je mimo aktuální, mužete pouze do minulosti nebo v stávájicím měsicí.');

           if($e_company_id != null && $u_company_id != null){
    	        if($print == true){
    	           $this->layout = 'print';
    	        }
               
                $this->loadModel('AtCompany');
                $this->loadModel('AtProjectCentre');
                $atc_e = $this->AtCompany->read('name',$e_company_id);
                $info = $this->AtProjectCentre->read('name',$at_project_centre_id);
                if($u_company_id > 0){ 
                    $atc_u = $this->AtCompany->read('name',$u_company_id); 
                    $info['AtCompanyU'] = $atc_u['AtCompany'];
                }
                else { $info['AtCompanyU']['name'] = 'soukromé účely';}
                $info['AtCompanyE'] = $atc_e['AtCompany'];
                
                $this->set('info',$info);
                
                $this->loadModel('ConnectionAuditEstate');
                $this->ConnectionAuditEstate->bindModel(array(
                    'belongsTo'=>array('AuditEstate','Client','CompanyForeign','ClientForeign'),
                    'joinSpec'=>array(
                        'ConnectionClientRequirement'=>array(
                            'primaryKey'=>'ConnectionClientRequirement.client_id',
                            'foreignKey'=>'ConnectionAuditEstate.client_id',
                            'conditions'=>array(
                                '((ConnectionAuditEstate.to_date = "0000-00-00" AND ConnectionClientRequirement.to = "0000-00-00") OR (ConnectionAuditEstate.to_date != "0000-00-00" && ConnectionClientRequirement.to >= ConnectionAuditEstate.to_date))','ConnectionClientRequirement.type'=>2)
                        ),
                        'Company'=>array(
                            'primaryKey'=>'Company.id',
                            'foreignKey'=>'ConnectionClientRequirement.company_id',
                        ),

                    )
                ));
                
                $spolecnost = null;
                if($filtr_spolecnost != false){
                    $filtr_spolecnost = base64_decode($filtr_spolecnost);
                    $spolecnost = 'IF(ConnectionAuditEstate.type = 1,IFNULL(Company.name,"nezvoleno"),IFNULL(CompanyForeign.name,"neuvedeno")) = "'.$filtr_spolecnost.'"';
                }
                
                if($filtr_stredisko != false){
                    $filtr_stredisko = str_replace(' ','+',$filtr_stredisko);
                    $filtr_stredisko = base64_decode($filtr_stredisko);
                    $spolecnost .= 'IF(ConnectionAuditEstate.connection_client_at_company_work_position_id IS NULL,
                          IF(ConnectionAuditEstate.type = 1,ConnectionAuditEstate.centre,-1),
                          (SELECT ccat_c.name FROM wapis__connection_client_at_company_work_positions as ccat
                            LEFT JOIN wapis__at_project_centres ccat_c ON (ccat_c.id = ccat.at_project_centre_id)
                            WHERE ccat.id=ConnectionAuditEstate.connection_client_at_company_work_position_id AND ccat.kos=0
                          )
                         ) = "'.$filtr_stredisko.'"';
                }

               if ($filtr_cinnost != false){
                   if ($filtr_cinnost == -1)
                        $spolecnost .= 'ConnectionAuditEstate.cinnost_id = 0';
                   else
                        $spolecnost .= 'ConnectionAuditEstate.cinnost_id = '.$filtr_cinnost;
               }
                
                $estate_list = $this->ConnectionAuditEstate->find('all',array(
                    'conditions'=>array(
                        'AuditEstate.kos'=>0,
                        'ConnectionAuditEstate.kos'=>0,
                        '(ConnectionAuditEstate.private_use = 0 OR ConnectionAuditEstate.private_use IS NULL)',
                        '(AuditEstate.at_company_id != ConnectionAuditEstate.at_company_id OR (AuditEstate.at_company_id = ConnectionAuditEstate.at_company_id AND ConnectionAuditEstate.centre != "'.$info['AtProjectCentre']['name'].'"))',
                         "((DATE_FORMAT(ConnectionAuditEstate.created,'%Y-%m')<= '".$year."-".$month."'))",
			             '((DATE_FORMAT(ConnectionAuditEstate.to_date,"%Y-%m") >= "'.$year.'-'.$month.'") OR (ConnectionAuditEstate.to_date = "0000-00-00"))',
	                    'ConnectionAuditEstate.at_company_id'=>$u_company_id,


                        'AuditEstate.at_company_id'=>$e_company_id,
                        'AuditEstate.at_project_centre_id'=>$at_project_centre_id,
                        $spolecnost
                    ),
                    'fields'=>array('AuditEstate.*','ConnectionAuditEstate.*','CompanyForeign.ulice',
                        'CompanyForeign.mesto','ClientForeign.ulice','ClientForeign.mesto',
                        'IF(ConnectionAuditEstate.type = 1,IFNULL(Company.name,"nezvoleno"),IFNULL(CompanyForeign.name,"neuvedeno")) as spolecnost',
                        'IF(ConnectionAuditEstate.type = 1,Client.name,IFNULL(ClientForeign.name,"neuvedeno")) as klient',
                        'IF(ConnectionAuditEstate.connection_client_at_company_work_position_id IS NULL,
                          IF(ConnectionAuditEstate.type = 1,ConnectionAuditEstate.centre,-1),
                          (SELECT ccat_c.name FROM wapis__connection_client_at_company_work_positions as ccat
                            LEFT JOIN wapis__at_project_centres ccat_c ON (ccat_c.id = ccat.at_project_centre_id)
                            WHERE ccat.id=ConnectionAuditEstate.connection_client_at_company_work_position_id AND ccat.kos=0
                          )
                         ) as stredisko,
                         (SELECT name FROM wapis__at_project_centres WHERE id = ConnectionAuditEstate.cinnost_id LIMIT 1) as cinnost

                         '
                    ),
                    'order'=>($order_by == false?null:$order_by),
                    'group'=>'ConnectionAuditEstate.id'
		
                ));
                //pr($estate_list);
                
                $order_list = array(
                    'AuditEstate.name ASC'=>'Majetek - vzestupně',
                    'AuditEstate.name DESC'=>'Majetek - sestupně',
                    'AuditEstate.imei_sn_vin ASC'=>'IMEI - vzestupně',
                    'AuditEstate.imei_sn_vin DESC'=>'IMEI - sestupně',
                    'Client.name ASC'=>'Osoba - vzestupně',
                    'Client.name DESC'=>'Osoba - sestupně',
                    'ConnectionAuditEstate.centre ASC'=>'Středisko - vzestupně',
                    'ConnectionAuditEstate.centre DESC'=>'Středisko - sestupně',
                    'AuditEstate.hire_price ASC'=>'Měsíčně - vzestupně',
                    'AuditEstate.hire_price DESC'=>'Měsíčně - sestupně',
                    'ConnectionAuditEstate.created ASC'=>'Pronájem od - vzestupně',
                    'ConnectionAuditEstate.created DESC'=>'Pronájem od - sestupně',
                    'ConnectionAuditEstate.to_date ASC'=>'Pronájem do - vzestupně',
                    'ConnectionAuditEstate.to_date DESC'=>'Pronájem do - sestupně',
                );
                
                $spol_list = array();
                $temp_spol_list = array_unique(Set::combine($estate_list,'/0/spolecnost','/0/spolecnost'));
                
                foreach($temp_spol_list as $id_spol=>$i_spol){
                   $spol_list[base64_encode($id_spol)] = $i_spol;
                }
                unset($temp_spol_list);
                
                $stredisko_list = array();
                $temp_stredisko_list = array_unique(Set::combine($estate_list,'/0/stredisko','/0/stredisko'));
                foreach($temp_stredisko_list as $id_spol=>$i_spol){
                   $stredisko_list[base64_encode($id_spol)] = $i_spol;
                }
                unset($temp_stredisko_list);


                $cinnost_ids = array_unique(Set::combine($estate_list,'/ConnectionAuditEstate/cinnost_id','/ConnectionAuditEstate/cinnost_id'));
                $this->loadModel('AtProjectCentre');
                $this->set('cinnost_list', $this->AtProjectCentre->find('list', array('conditions'=>array('id'=>$cinnost_ids))));

                $this->set('estate_list',$estate_list);
                $this->set('year',$year);
                $this->set('month',$month);
                $this->set('print',$print);
                $this->set('order_list',$order_list);
                $this->set('spol_list',$spol_list);
                $this->set('stredisko_list',$stredisko_list);
                $this->set('refresh_url','/monthly_statements/show/'.$type.'/'.$e_company_id.'/'.$u_company_id.'/'.$at_project_centre_id.'/'.$year.'/'.$month);
           
                if($print == false && ($order_by != false || $filtr_spolecnost != false || $filtr_stredisko != false || $filtr_cinnost != false)){
                    $this->render('items');
                }
                else{
                    $this->render('show');
                }
           }     
           else
                die('Chyba: Bez id?'); 
	}
       
   
    function fakturovano($type = 1,$e_company_id = null,$u_company_id = null,$at_project_centre_id = null,$year = null,$month = null){
        if($year > date('Y') || ($year == date('Y') && $month > date('m')))
            die(json_encode(array('result'=>false,'message'=>'Toto datum je mimo aktuální, mužete pouze do minulosti nebo v stávájicím měsicí.')));
        
        $this->loadModel('ConnectionFaAuditEstate');
        $data = array(
            'at_company_e_id'=>$e_company_id,
            'at_project_centre_id'=>$at_project_centre_id,
            'at_company_u_id'=>$u_company_id,
            'private_use'=>0,
            'year'=>$year,
            'month'=>$month,
        );
        $this->ConnectionFaAuditEstate->save($data);
        die(json_encode(array('result'=>true)));
    }
}
?>