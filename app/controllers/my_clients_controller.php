<?php
//Configure::write('debug', 2);

class MyClientsController extends AppController
{
    var $name = 'MyClients';
    var $helpers = array('htmlExt', 'Pagination', 'ViewIndex');
    var $components = array('ViewIndex', 'RequestHandler');
    var $uses = array('MyClientsView');
    var $renderSetting = array(
        'SQLfields' => array(
            'MyClientsView.*',
            'IF( MyClientsView.pay_date IS NOT NULL, "ano"  ,"ne" ) as payed',
            'IF( MyClientsView.price IS NOT NULL, MyClientsView.price  ,MyClientsView.price_2 ) as price',
        ),
        'SQLcondition' => array(
            'IF( ISNULL(to_cms_user_id) , 0 , to_cms_user_id = #CmsUserId#  AND to_date = "0000-00-00" )',
        ),
        'controller' => 'my_clients',
        'page_caption' => 'Klienti v mém vlastnictví',
        'top_action' => array(
            //'add_item'		=>	'Přidat|edit|Pridat majetek|add',
        ),
        'no_trash'=> false,
        'filtration' => array(

            'GET-current_month' => 'select|Měsíc|mesice_list',
            'GET-current_year' => 'select|Rok|actual_years_list',

        ),
        'items' => array(
            'id' => 'ID|MyClientsView|id|hidden|',
            'client_name' => 'Pracovník|MyClientsView|client_name|text',
            'position' => 'Způsob|MyClientsView|position_name|text',
            'created' => 'Objednán|MyClientsView|from_date|text',
            'price' => 'Cena|0|price|text|',
            'payed' => 'Zaplacen|0|payed|text|'
        ),
        'sortBy'=>'MyClientsView.client_name.ASC',
        'domwin_setting' => array(
            'sizes' => '[1000,1000]',
            'scrollbars' => true,
            'languages' => true,
        )
    );

    function index()
    {

        $this->set('fastlinks', array('ATEP' => '/', 'Evidence majetku' => '#', $this->renderSetting['page_caption'] => '#'));

        /*   $this->set('company_list', $this->get_list('AtCompany'));
       $this->set('project_list', $this->get_list('AtProject'));
       $this->set('center_list', $this->get_list('AtProjectCentre'));*/
        if ($this->RequestHandler->isAjax()) {
            $this->render('../system/items');
        } else {
            $this->render('../system/index');
        }
    }

}

?>