<?php
//	Configure::write('debug',2);
class ReportDmsDocsController extends AppController {
	var $name = 'ReportDmsDocs';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload');
	var $uses = array('ReportDmsDoc');
	var $renderSetting = array(
        'bindModel'=>array(
            'belongsTo'=>array('ReportDmsType','ReportDmsTypeDelivery','AtProject')
        ),
		'controller'=>'report_dms_docs',
		'SQLfields' => '*',//array('id','name','updated','created'),
		'page_caption'=>'DMS - správa dokumentů',
		'sortBy'=>'ReportDmsDoc.name.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
            'groups'		=>	'Čísleniky|items|Čísleniky|groups',
		),
		'filtration' => array(
            'ReportDmsDoc-name|'	=>	'text|Název|',
            'ReportDmsDoc-sender|'	=>	'text|Odesílatel|',
            'ReportDmsDoc-addressee_name|'	=>	'text|Adresát|',
			
			'ReportDmsDoc-report_dms_type_id'		=>	'select|Typ|type_list',
            
            
            'ReportDmsDoc-at_project_id|'	=>	'select|Projekt|project_list',
            'ReportDmsDoc-delivery'	=>	'checkbox|Doručeno|',
            'ReportDmsDoc-created|'	=>	'date|Datum|',
            
		),
		'items' => array(
			'id'		=>	'ID|ReportDmsDoc|id|text|',
            'typ'		=>	'Typ|ReportDmsType|name|text|',
            'name'		=>	'Název|ReportDmsDoc|name|text|',
            'sender'	=>	'Odesílatel|ReportDmsDoc|sender|text|',
            'addressee_name'	=>	'Adresát|ReportDmsDoc|addressee_name|text|',
            'projekt'	=>	'Adresát - projekt|AtProject|name|text|',
            'delivery'	=>	'Fyzicky doručeno|ReportDmsDoc|delivery|text|value_to_yes_no#long',
			//'updated'	=>	'Upraveno|ReportDmsDoc|updated|datetime|',
			'created'	=>	'Vytvořeno|ReportDmsDoc|created|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
            'attach'	=>	'attachs|Přílohy|attach',
			'trash'	=>	'trash|Do košiku|trash'			
		),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'	=> true,
			'languages'		=> 'false'
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Nastavení dokumentů'=>'#'));
        // set JS and Style
		$this->set('scripts',array('uploader/uploader'));
		$this->set('styles',array('../js/uploader/uploader'));
        
        $this->set('project_list',$this->get_list('AtProject'));
        $this->set('type_list',$this->get_list('ReportDmsType'));
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
            $cetre_list = array();		  
			if ($id != null){
				$this->data = $this->ReportDmsDoc->read(null,$id);
                if($this->data['ReportDmsDoc']['at_project_id'] != '')
                    $cetre_list = $this->get_list('AtProjectCentre',array('at_project_id'=>$this->data['ReportDmsDoc']['at_project_id'],'kos'=>0));
            }
              
            $this->set('project_list',$this->get_list('AtProject'));
            $this->set('centre_list',$cetre_list);
            $this->set('type_list',$this->get_list('ReportDmsType'));
            $this->set('type_delivery_list',$this->get_list('ReportDmsTypeDelivery'));    
			$this->render('edit');
		} else {
		    if($this->data['ReportDmsDoc']['id'] == ''){$this->data['ReportDmsDoc']['cms_user_id'] = $this->logged_user['CmsUser']['id'];}  
			
            $this->ReportDmsDoc->save($this->data);
			die();
		}
	}
    
    /**
     * Administrace čísleníku pro DMS
     */
    function items(){
        $this->loadModel('ReportDmsType');
        $types = $this->ReportDmsType->find('all',array('conditions'=>array('kos'=>0)));
        $this->set('type_list',$types);
        
        $this->loadModel('ReportDmsTypeDelivery');
        $types_del = $this->ReportDmsTypeDelivery->find('all',array('conditions'=>array('kos'=>0)));
        $this->set('type_delivery_list',$types_del);
        
        $this->render('items/index');
    }
    
    function item_edit($model,$id = null){
        $this->loadModel($model);
        if(empty($this->data)){
            if($id != null){
                $this->data = $this->{$model}->read(null,$id);
            }
            
            $this->set('model',$model);
            $this->render('items/edit');
        }
        else{
            $this->{$model}->save($this->data);
            die(json_encode(array('result'=>true)));
        }
    }
    
    function item_trash($model,$id){
        $this->loadModel($model);
        $this->{$model}->id = $id;
        $this->{$model}->saveField('kos',1);
        die(json_encode(array('result'=>true)));
    }
    
    /**
     * nacteni seznamu stredisek
     */    
    function load_at_project_centre_list($project_id = null){
        if ($project_id != null){

           $this->loadModel('AtProjectCentre');
           //$this->AtProjectCentre->bindModel(array('belongsTo'=>array('AtProject')));
           $cetre_list = $this->AtProjectCentre->find('list',array(
               'conditions'=>array('at_project_id'=>$project_id,'kos'=>0)
           ));
           //$cetre_list = Set::combine($cetre_list,'/AtProjectCentre/id','/AtProjectCentre/name');

		   die(json_encode($cetre_list));
		}
		die(json_encode(array('result'=>false,'message'=>'Nebyl zvolený projekt')));    
    }
    
    
    /**
 	* Seznam priloh
 	*
	* @param $company_id
 	* @return view
 	* @access public
	**/
	function attachs($report_dms_doc_id){
		$this->autoLayout = false;
		$this->loadModel('ReportDmsDocAttachment'); 

		if($this->logged_user["CmsGroup"]["permission"]['report_dms_docs']["attach"]==2) 
			$pristup =  "ReportDmsDocAttachment.cms_user_id=".$this->logged_user['CmsUser']['id'];
		else 
			$pristup="";

		$this->ReportDmsDocAttachment->bindModel(array('belongsTo'=>array('ReportDmsType')));
		$this->set('attachment_list', $this->ReportDmsDocAttachment->findAll(array(
        'ReportDmsDocAttachment.report_dms_doc_id'=>$report_dms_doc_id,
        'ReportDmsDocAttachment.kos'=>0 ,$pristup)));
        
		$this->set('id',$report_dms_doc_id);
		unset($this->ReportDmsDocAttachment);
		$this->render('attachs/index');
		
	}
	/**
 	* Editace priloh
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function attachs_edit($report_dms_doc_id = null, $id = null){
		$this->autoLayout = false;
		$this->loadModel('ReportDmsDocAttachment');
		if (empty($this->data)){
			$this->set('type_list',$this->get_list('ReportDmsType'));
            
			$this->data['ReportDmsDocAttachment']['report_dms_doc_id'] = $report_dms_doc_id;
			if ($id != null){
				$this->data = $this->ReportDmsDocAttachment->read(null,$id);
			}
			$this->render('attachs/edit');
		} else {
			$this->data["ReportDmsDocAttachment"]["cms_user_id"]=$this->logged_user['CmsUser']['id'];
			$this->ReportDmsDocAttachment->save($this->data);
			$this->attachs($this->data['ReportDmsDocAttachment']['report_dms_doc_id']);
		}
		unset($this->ReportDmsDocAttachment);
	}
	/**
 	* Presun prilohy do kose
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function attachs_trash($report_dms_doc_id, $id){
		$this->loadModel('ReportDmsDocAttachment');
		$this->ReportDmsDocAttachment->save(array('ReportDmsDocAttachment'=>array('kos'=>1,'id'=>$id)));
		$this->attachs($report_dms_doc_id);
		unset($this->ReportDmsDocAttachment);
	}
	/**
 	* Nahrani prilohy na ftp
 	*
 	* @return view
 	* @access public
	**/
	function upload_attach() {
		$this->Upload->set('data_upload',$_FILES['upload_file']);
		if ($this->Upload->doit(json_decode($this->data['upload']['setting'],true))){
			echo json_encode(array('upload_file'=>array('name'=>$this->Upload->get('outputFilename')),'return'=>true));
		} else 
			echo json_encode(array('return'=>false,'message'=>$this->Upload->get('error_message')));		
		die();
	} 
	/**
 	* Stazeni prilohy
 	*
	* @param $file
	* @param $file_name
 	* @return download file
 	* @access public
	**/
	function  attachs_download($file,$file_name){
		$pripona = strtolower(end(Explode(".", $file)));
		$file = strtr($file,array("|"=>"/"));
		$filesize = filesize('../atep/uploaded/'.$file);
		$cesta = "http://administrace.power-job.cz/uploaded/".$file;
				 
		header("Pragma: public"); // požadováno
	    header("Expires: 0");
	    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	    header("Cache-Control: private",false); // požadováno u některých prohlížečů
	    header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . $filesize);
		Header('Content-Type: application/octet-stream');
		Header('Content-Disposition: attachment; filename="'.$file_name.'.'.$pripona.'"');
		readfile($cesta);
		die();

	}
    
}
?>