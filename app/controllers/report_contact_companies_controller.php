<?php
//Configure::write('debug',2);
class ReportContactCompaniesController extends AppController {
	var $name = 'ReportContactCompanies';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('CompanyContact');
	var $renderSetting = array(
		'bindModel'	=> array(
            'belongsTo'=>array('Company'),
            'joinSpec'=>array(
                'SettingParentCompany'=>array(
                    'className'=>'SettingParentCompany',
                    'foreignKey'=>'Company.parent_id',
                    'primaryKey'=>'SettingParentCompany.id'
                )
            )
        ),
		'controller'=>'report_contact_companies',
		'SQLfields' => array('CompanyContact.id','CompanyContact.name','Company.self_manager_id',
                             'Company.client_manager_id','Company.coordinator_id','CompanyContact.email',
                             'CompanyContact.telefon1','CompanyContact.telefon2','CompanyContact.position',
                             'CompanyContact.comment',
                             'Company.name','CompanyContact.company_id','SettingParentCompany.name'),
		'page_caption'=>'Kontakty podniku',
		'sortBy'=>'CompanyContact.id.DESC',
		'permModel'=>'Company',
		'TypUserCompany'=>true,
		'permGroupId'=>true,
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat kontakt|add',
            'export_excel' => 'Export Excel|export_excel|Export Excel|export_excel', 
		),
		'filtration' => array(
			'CompanyContact-company_id'	=>	'select|Firma|company_list',
            'CompanyContact-name'	=>	'text|Jméno|',
			'Company-parent_id'		=>	'select|Nadřazená spol.|nad_spol_list',
		),
		'items' => array(
			'id'		=>	'ID|CompanyContact|id|text|',
			'company'	=>	'Firma|Company|name|text|',
            'nad_spol'	=>	'Nadřazená spol.|SettingParentCompany|name|text|',
			'name'		=>	'Název|CompanyContact|name|text|',
            'pozice'	=>	'Pozice|CompanyContact|position|text|',
			'email'		=>	'Email|CompanyContact|email|text|',
			'telefon1'		=>	'Telefon|CompanyContact|telefon1|text|',
            'telefon2'		=>	'Telefon2|CompanyContact|telefon2|text|',
            'comment'		=>	'Komentář|CompanyContact|comment|text|orez#50',
		),
		'posibility' => array(
		//	'show'		=>	'show|Zobrazit aktivitu|show',			
			'edit'		=>	'edit|Editovat kontakt|edit',
			'delete'	=>	'trash|Odstranit kontakt|trash'				
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#','Kontakty firem'=>'#'));
		$this->loadModel('Company');
		$company_conditions =  array('Company.kos'=>0);
		if (isset($this->filtration_company_condition))
			$company_conditions = am($company_conditions, $this->filtration_company_condition);
				
		$this->set('company_list',$this->Company->find('list',array('order'=>'name','conditions'=>$company_conditions)));
		unset($this->Company);
        
        $this->set('nad_spol_list',$this->get_list('SettingParentCompany'));
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id=null){
		$this->autoLayout = false;
		
		if (empty($this->data)){
			$this->loadModel('Company');

			if($this->logged_user["CmsGroup"]["permission"]["report_contact_companies"]["add"]==2)
				$podminka=$this->logged_user["CmsGroup"]["permission"]["companies"]["group_id"]."=".$this->logged_user["CmsUser"]["id"];
			else 
				$podminka="";

			$this->set('company_list',$this->Company->find('list',array('order'=>array('name'),'conditions'=>array('Company.kos'=>0,$podminka))));
			if ($id != null){
				$this->data = $this->CompanyContact->read(null,$id);
			}
			$this->render('edit');
			unset($this->Company);
		} else {
			$this->CompanyContact->save($this->data);
			die();
		}
		
	}

	function trash($id){
		if($id!=null){	
			$this->CompanyContact->save(array('CompanyContact'=>array('kos'=>1,'id'=>$id)));
			die();
		}
	}
	
    
    /**
     * funkce pro vytvoreni exportu do excelu - CSV
     * podle filtrace vyber dane klienty a vygeneruj je do csv
     */
    function export_excel(){
        //Configure::write('debug',1);

         
       $start = microtime ();
       $start = explode ( " " , $start );
       $start = $start [ 1 ]+ $start [ 0 ];  
        
        
       $fields_sql = $this->renderSetting['SQLfields'];
               
        $criteria = $this->ViewIndex->filtration();
        /*
        foreach($criteria as $key => $val){
            if (strpos($key,'ClientView.') !== false){
                $new_key = substr($key,0,strpos($key,'ClientView.')).'ClientView2.'.substr($key,strpos($key,'ClientView.')+11);
                unset($criteria[$key]);
                $criteria[$new_key] = $val;
            }   
        }
        */
        
        
       
        $this->loadModel('CompanyContact');
      

         header("Pragma: public"); // požadováno
         header("Expires: 0");
         header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
         header("Cache-Control: private",false); // požadováno u některých prohlížečů
         header("Content-Transfer-Encoding: binary");
         Header('Content-Type: application/octet-stream');
         Header('Content-Disposition: attachment; filename="'.date('Ymd_His').'.csv"');

      
        /*
         * Celkovy pocet zaznamu
         */
         
        $limit = 100;   
        $page = 1;
        $this->CompanyContact->bindModel($this->renderSetting['bindModel']);        
        $count = $this->CompanyContact->find('first',
        	array(
        		'fields' =>  array("COUNT(DISTINCT CompanyContact.id) as count"),
        		'conditions'=>am($criteria,array('CompanyContact.kos'=>0)),
        		'recursive'	=>1
        	)
        );    
        $count = $count[0]['count'];
       //  echo $count;
        //  echo "\n"; 
        // hlavicka
        foreach($this->renderSetting['items'] as &$item_setting){
            list($caption, $model, $col, $type, $fnc) = explode('|',$item_setting);
        	$item_setting = compact(array("caption", "model","col","type","fnc"));	
            if($type != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',$caption).'";'; 
        }
        echo "\n";   
        unset($item_setting, $caption, $model, $col, $type, $fnc);
        
        $str_array=array("<br/>"=>', ',':'=>',',';'=>',','?'=>'', '#'=>' ');
        /*
         * Cyklicky vypis dat po $limit zaznamu
         */
        for ($exported = 0; $exported < $count; $exported += $limit){
            $this->CompanyContact->bindModel($this->renderSetting['bindModel']);     
            foreach($this->CompanyContact->find('all',array(
                   'fields'=>$fields_sql,
                    'conditions'=>am($criteria,array('CompanyContact.kos'=>0)),
                    'group'=>'CompanyContact.id',
                    'limit'=>$limit,
                    'page'=>$page,
                    'recursive'=>1
            )) as $item){
                foreach($this->renderSetting['items'] as $key => $td){
                    if($td['type'] != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',strtr($this->ViewIndex->generate_td($item,$td,$this->viewVars),$str_array)).'";';     
                }
                echo "\n";  
            }
            $page++;
        }
       
        //time
         $end = microtime ();
         $end = explode ( " " , $end );
         $end = $end [ 1 ]+ $end [ 0 ]; 
         echo 'Generate in '.($end - $start).'s';
         echo "\n"; 
        
        die();
    }
}
?>