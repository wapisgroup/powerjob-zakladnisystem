<?php

$_GET['start_year'] 	= (isset($_GET['start_year']) && !empty($_GET['start_year']))?$_GET['start_year']:(int)date('Y');
$_GET['end_year'] 	= (isset($_GET['end_year']) && !empty($_GET['end_year']))?$_GET['end_year']:(int)date('Y');
@define('START_YEAR', $_GET['start_year']); 
@define('END_YEAR', $_GET['end_year']); 

$_GET['start_month'] 	= (isset($_GET['start_month']))?$_GET['start_month']:(int)date('m');
$_GET['end_month'] 	= (isset($_GET['end_month']))?$_GET['end_month']:(int)date('m');
@define('START_MONTH', ($_GET['start_month'] < 10)?'0'.$_GET['start_month']:$_GET['start_month']); 
@define('END_MONTH', ($_GET['end_month'] < 10)?'0'.$_GET['end_month']:$_GET['end_month']); 

Configure::write('debug',1);
class ReportWorkingSummaryController extends AppController {
    var $name = 'ReportWorkingSummary';
    var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput','Excel');
    var $components = array('ViewIndex','RequestHandler','Upload','Email');
    var $uses = array('ConnectionClientRequirement');
    var $renderSetting = array(
        'SQLfields' => array(
            'ConnectionClientRequirement.id',
            'Client.name',
            'Client.id',
            'Company.name',
            'Company.id',
            'DATE_FORMAT(ConnectionClientRequirement.from,"%d. %m. %Y") as date_from',
            'ConnectionClientRequirement.updated',
            'ConnectionClientRequirement.created',
            'ConnectionClientRequirement.from',
            'ConnectionClientRequirement.from_when',
            'ConnectionClientRequirement.from_cms_user_id',
            'DATEDIFF(ConnectionClientRequirement.from_when,ConnectionClientRequirement.from) as rozdil_from'
            
        ),
        'bindModel' => array(
            'belongsTo' => array(
                'Company'=>array('foreignKey'=>'company_id'),
                'Client'=>array('foreignKey'=>'client_id')
            ),
        ),
        
        'controller'=> 'report_working_summary',
        'page_caption'=>'Evidencia datumov nastup',
        'sortBy'=>'ConnectionClientRequirement.from.DESC',
        'top_action' => array(
            'excel'			=>	'Export|export_excel|Vyexportovat do excelu|index',                      
        ),
        'filtration' => array(
            'Client-name|'      => 'text|Klient|',
            'Company-id'	=> 'select|Společnost|company_list',
            'Rozdil-value'	=> 'text|Rozdíl|',
            'ConnectionClientRequirement-from'	=> 'date_f_t|Datum|',
        ),
        'SQLcondition' => array(
            'ConnectionClientRequirement.from != 0000-00-00',                     
        ),
        'items' => array(
           'id'		=>	'ID|ConnectionClientRequirement|id|text|',
            'client'		=>	'Klient|Client|name|text|',
            'company'	        =>	'Společnost|Company|name|text|',	
            'date_to'		=>	'Datum nástupu|ConnectionClientRequirement|from|date|',	
            'date_to_when'	=>	'Datum nástupu (Kdy)|ConnectionClientRequirement|from_when|date|',	
            'date_to_diff'	=>	'Rozdil|0|rozdil_from|text|',	
            'koo2'	        =>	'KOO|ConnectionClientRequirement|from_cms_user_id|var|user_list',		
        ),
        'posibility' => array(
            //'show'		=>	'show|Detail|show',
        ),
        'domwin_setting' => array(
            'sizes' 		=> '[1000,1000]',
            'scrollbars'		=> true,
            'languages'	=> 'false'
        )
    );
    
    function beforeFilter(){
        parent::beforeFilter();
        
        if (isset($_GET['excel'])){
            $this->renderSetting['no_limit'] = true;
        }
        
        if (isset($this->params['url']['filtration_Rozdil-value']) && $this->params['url']['filtration_Rozdil-value'] != ''){
            $this->renderSetting['SQLcondition'][] = 'DATEDIFF(ConnectionClientRequirement.from_when,ConnectionClientRequirement.from) >= '.$this->params['url']['filtration_Rozdil-value'];
            unset($this->params['url']['filtration_Rozdil-value']);
        }
        
    }
    
    
    function export_excel(){
		$link = '/'.$this->renderSetting['controller'].'/?';
		$sub = array();
		foreach($this->params['url'] as $key=>$get){
			if ($key != 'url'){
				$sub[] = "{$key}={$get}";
			}
		}
		$link .= implode('&',$sub) . '&excel=true';
		$this->redirect($link);
		exit;
	}
    
    function index(){
        $this->set('company_list',$this->get_list('Company'));
        /**
         * Natahnuti seznamu uzivatelu pro doplneni informaci o koo z historie,
         * je mozne posleze omezit na pouzite users z history
         */
        $this->set('user_list',$this->get_list('CmsUser'));
        
        if (isset($_GET['excel'])){
			$this->autoLayout = false;
			echo $this->render('../system/excel');
			die();
		}
        
        if ($this->RequestHandler->isAjax()){
            $this->render('../system/items');
        } else {
            $this->render('../system/index');
        }
    }
    
    
    /**
     * Funcke vytáhne z historie
     */
    function convert_from_history($page = 0){
        
        $items = $this->ConnectionClientRequirement->find('all',array(
            'conditions'=>array(
                //'company_id'=>16,
                'type'=>2,
                '(from_cms_user_id IS NULL OR to_cms_user_id IS NULL)',
            ),
            'page'=>$page,
            'limit'=>1000
        ));
        
        $this->set('company_list',$this->get_list('Company'));
        /**
         * Natahnuti seznamu typu akci historie
         */
        $this->set('history_type_list',$this->get_list('HistoryType'));
        /**
         * Natahnuti seznamu uzivatelu pro doplneni informaci o koo z historie,
         * je mozne posleze omezit na pouzite users z history
         */
        $this->set('user_list',$this->get_list('CmsUser'));
        
        /**
         * Natahnuti informaci z historie
         */
        $this->loadModel('HistoryItem');
        $acct = 0;
        foreach($items as $row){
            /**
             * najiti id kooordinatora, ktery propustil zamestnance,
             * najiti dane udalosti v historii items
             * s type id akce 13 (ukonceni zamestnani)
             */
            $data = $this->HistoryItem->find(
                'all',
                array(
                    'conditions' => array(
                        'client_id' => $row['ConnectionClientRequirement']['client_id'],
                        'action_id' => array(10,13),
                        'DATE_FORMAT(created,"%Y-%m-%d %k:%i") = DATE_FORMAT("'.$row['ConnectionClientRequirement']['updated'].'","%Y-%m-%d %k:%i")'
                    ),
                    'fields' => array(
                        'cms_user_id',
                        'action_id',
                        'created'
                    )
                )
            );
            
            if (count($data) == 1){
                $row[0]['cms_user_id2'] = $data[0]['HistoryItem']['cms_user_id'];
                $row[0]['akce2'] = $data[0]['HistoryItem']['action_id'];
                $row[0]['when_to'] = date('Y-m-d',strtotime($data[0]['HistoryItem']['created']));
                
            } else {
                $row[0]['cms_user_id2'] = -111;
                $row[0]['akce2'] = -111;
            }
            unset($data);
            /**
             * najiti datumu, kdy byla aktivita zamestnani pridana
             * jde o to, ze jelikoz nevime datum, pujde o najiti nejblizsiho mensiho terminu
             * teto aktivity, zda toto bude fungovat nikdo nevi, ani buh ne
             */
            
            $this->HistoryItem->bindModel(array('hasOne'=>array(
                'HistoryData' => array(
                    'conditions' => array(
                        'caption' => array('Od','Datum nástupu')
                    )
                )
            )));
            $data = $this->HistoryItem->find(
                'all',
                array(
                    'conditions' => array(
                        'client_id' => $row['ConnectionClientRequirement']['client_id'],
                        'action_id' => array(10,9,33,35),
                        //'DATE_FORMAT(HistoryItem.created,"%Y-%m-%d") <= DATE_FORMAT("'.$row['ConnectionClientRequirement']['to'].'","%Y-%m-%d")',
                        //'DATE_FORMAT(HistoryItem.created,"%Y-%m-%d %k") >= DATE_FORMAT("'.$row['ConnectionClientRequirement']['created'].'","%Y-%m-%d %k")'
                        'HistoryData.value = DATE_FORMAT("'.$row['ConnectionClientRequirement']['from'].'","%d.%m.%Y")'
                        
                    ),
                    'fields' => array(
                        'HistoryItem.cms_user_id',
                        'HistoryItem.action_id',
                        'HistoryItem.id',
                        'HistoryItem.created',
                        'HistoryData.value'
                    ),
                    'limit' => 1
                )
            );
            if (count($data) == 1){
                $row[0]['cms_user_id1'] = $data[0]['HistoryItem']['cms_user_id'];
                $row[0]['akce1'] = $data[0]['HistoryItem']['action_id'];
                $row[0]['when_from'] = date('Y-m-d',strtotime($data[0]['HistoryItem']['created']));
            } else {
                $row[0]['cms_user_id1'] = -111;
                $row[0]['akce1'] = -111;
            }
          
            $save_connection = array();
            if($row[0]['akce1'] != -111 && $row['ConnectionClientRequirement']['from_cms_user_id'] == ''){
                $save_connection = array(
                    'from_when'=>$row[0]['when_from'],
                    'from_cms_user_id'=>$row[0]['cms_user_id1']
                );
            }
            if($row[0]['akce2'] != -111 && $row['ConnectionClientRequirement']['to_cms_user_id'] == ''){
                $_save_connection = array(
                    'to_when'=>$row[0]['when_to'],
                    'to_cms_user_id'=>$row[0]['cms_user_id2']
                );
                $save_connection = am($save_connection,$_save_connection);
            }
            if(!empty($save_connection)){
                $acct++;
                $this->ConnectionClientRequirement->id = $row['ConnectionClientRequirement']['id'];
                $this->ConnectionClientRequirement->save($save_connection);
            }
            
        }
        echo $acct.'<br />';
        die('done');

    }
    
    
}
?>