<?php
Configure::write('debug',1);

$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');

define('CURRENT_YEAR', $_GET['current_year']);
define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);

class ReportForMuPaymentIntClientsController extends AppController {
	var $name = 'ReportForMuPaymentIntClients';
	var $helpers = array('htmlExt','Pagination','ViewIndex','Excel');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('ConnectionClientAtCompanyWorkPosition');
	var $renderSetting = array(
		'bindModel'	=> array(
			'belongsTo'	=>	array('Client','AtCompany','AtCompanyMoneyItem'
            ),
			'hasOne'	=>	array(
				'ClientWorkingHoursInt'=>array(
					'conditions' => array(
						'ClientWorkingHoursInt.year'=> CURRENT_YEAR,
						'ClientWorkingHoursInt.month'=> CURRENT_MONTH
					)
				)
			)
		),
		'controller'	=>	'report_for_mu_payment_int_clients',
		'SQLfields' 	=>	'*,(drawback+drawback_close_cw+drawback_activity+phm_consumption+mobile_consumption+data_consumption) as drawbacks, (odmena_1+odmena_2+odmena_3) as bonus',
		'SQLcondition'	=>  array(
			"((DATE_FORMAT(ConnectionClientAtCompanyWorkPosition.datum_nastupu,'%Y-%m')<= '#CURRENT_YEAR#-#CURRENT_MONTH#'))",
			'((DATE_FORMAT(ConnectionClientAtCompanyWorkPosition.datum_to,"%Y-%m") >= "#CURRENT_YEAR#-#CURRENT_MONTH#") OR (ConnectionClientAtCompanyWorkPosition.datum_to = "0000-00-00 00:00"))',
            'ConnectionClientAtCompanyWorkPosition.kos'=> 0,
            'ConnectionClientAtCompanyWorkPosition.at_company_money_item_id'=>array(1,3),//dohoda, ps
			'ClientWorkingHoursInt.stav IN (3,4)'
		),
		'page_caption'=>'Výplaty pro mzdovou účetní - Int. zam',
		'checkbox_setting' => array(
			'model'			=>	'ClientWorkingHoursInt',
			'col'			=>	'stav',
			'col2'			=>	'stop_payment',
			'stav_array'	=>	array(3),
			'month_year'	=>	false
		),
		'sortBy'=>'Client.name.ASC',
		'top_action' => array(
			'multi_edit'		=>	'Vyplatit hromadně|money_all|Vyplatit hromadně|multi_edit',
			'export_excel' 		=> 	'Export Excel|export_excel|Export Excel|multi_edit',
		),
		'filtration' => array(
            'Client-name'		=>	'text|Klient|',
			'ConnectionClientAtCompanyWorkPosition-company_id'	=>	'select|Společnost|at_company_list',
			'GET-current_year'							=>	'select|Rok|actual_years_list',
			'GET-current_month'							=>	'select|Měsíc|mesice_list',
		),
		'items' => array(
			'id'				=>	'ID|ClientWorkingHoursInt|id|hidden|',
			'company'			=>	'Společnost|AtCompany|name|text|',
			'client'			=>	'Klient|Client|name|text|',	
            'forma_mzdy'		=>	'Forma|AtCompanyMoneyItem|name|text|',
			'nastup'			=>	'Nástup|ConnectionClientAtCompanyWorkPosition|datum_nastupu|date|',
			'ukonceni'			=>	'Ukončení|ConnectionClientAtCompanyWorkPosition|datum_to|date|',
			'norhod'			=>	'FPD|ClientWorkingHoursInt|standard_hours|text|',
			'svatky'			=>	'Sv|ClientWorkingHoursInt|svatky|text|',
			'vikendy'			=>	'Ví|ClientWorkingHoursInt|vikendy|text|',
			'pn'				=>	'PN|ClientWorkingHoursInt|pracovni_neschopnost|text|',
			'dovolena'			=>	'Do|ClientWorkingHoursInt|dovolena|text|',
			'prescasy'			=>	'Př|ClientWorkingHoursInt|prescasy|text|',
			'hour_s1'			=>	'S1|ClientWorkingHoursInt|hour_s1|text|',
			'hour_s2'			=>	'S2|ClientWorkingHoursInt|hour_s2|text|',
			'hour_s3'			=>	'S3|ClientWorkingHoursInt|hour_s3|text|',
			'celkem'			=>	'Celkem|ClientWorkingHoursInt|celkem_hodin|text|',
			'fix'				=>	'Fix|ClientWorkingHoursInt|salary_fix|text|',
			'total'				=>	'Total|ClientWorkingHoursInt|salary_total|text|',
            'srazka'			=>	'Srážky celkem|0|drawbacks|text|',
			'odmena1'			=>	'Odměny celkem|0|bonus|text|',
			//'stravne'			=>	'Stravné|CompanyMoneyItem|vlozeny_prispevek_na_stravne|text|',
			//'cestovne'			=>	'Cestovné|CompanyMoneyItem|vlozeny_prispevek_na_cestovne|text|',
			'stav'				=>	'Stav|ClientWorkingHoursInt|stav|var|kalkulace_stav_list',
			'stop_payment'		=>	'Výp.|ClientWorkingHoursInt|stop_payment|var|ano_ne'	
		),
		'posibility' => array(
			'stop_status'			=>	'stop|Pozastavit výplatu|stop_status',
			'show'			=>	'odpracovane_hodiny|Karta docházky|show',
			'edit'			=>	'money|Vyplatit|edit'
		)
	);
    
	function export_excel(){
		$link = '/'.$this->renderSetting['controller'].'/?';
		$sub = array();
		foreach($this->params['url'] as $key=>$get){
			if ($key != 'url'){
				$sub[] = "{$key}={$get}";
			}
		}
		$link .= implode('&',$sub) . '&excel=true';
		$this->redirect($link);
		exit;
	}
	
	function index(){
		//pr($this->viewVars['items']);
		$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#','Výplaty pro mzdovou účetní'=>'#'));	
		$this->set('at_company_list',$this->get_list('AtCompany'));


		if (isset($_GET['excel'])){
			$this->autoLayout = false;
			echo $this->render('../system/excel');
			die();
		}
	
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}

	
	function money($id){
		$this->loadModel('ClientWorkingHoursInt');
		$this->ClientWorkingHoursInt->id = $id;
		$this->ClientWorkingHoursInt->saveField('stav',4);
		unset($this->ClientWorkingHoursInt);
		die();
	}

	function stop($id,$status){
		$this->loadModel('ClientWorkingHoursInt');
		$this->ClientWorkingHoursInt->id = $id;
		$this->ClientWorkingHoursInt->saveField('stop_payment',$status==1 ? 0 :1);
		unset($this->ClientWorkingHoursInt);
		die();
	}

	function odpracovane_hodiny($id = null, $year = null, $month = null){
		echo $this->requestAction('internal_employees/odpracovane_hodiny/'.$id.'/'.$year.'/'.$month.'/');
		die();
	}

	function money_all(){
		$model = $this->renderSetting["checkbox_setting"]['model'];
		$this->loadModel($model);
		foreach($this->params['url']['data'][$model]['id'] as $key => $on){
			$this->$model->id = $key;
			$this->$model->saveField('stav',4);
		}
		unset($this->$model);	
		die();
	}

	
	
}
?>