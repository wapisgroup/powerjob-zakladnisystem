<?php
if($_SERVER['REMOTE_ADDR'] == '2a00:1028:96c0:ad66:a14e:aa87:a1b9:ff63' || $_SERVER['REMOTE_ADDR'] == '90.176.43.89') {
 //Configure::write('debug',1);

}
//Configure::write('debug',1);
// if (isset($_GET['current_month']) && $_GET['current_month'] < 10) $_GET['current_month'] = '0'.$_GET['current_month'];
// define('CURRENT_YEAR', (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y'));
// define('CURRENT_MONTH', (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:date('m'));
//Configure::write('debug',2);
$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');
@define('CURRENT_YEAR', $_GET['current_year']); 
@define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);


class EmployeesController  extends AppController {
	var $name = 'Employees';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email','Employees');
	var $uses = array('ConnectionClientRequirement');
	var $mesic = CURRENT_MONTH;
	var $rok = CURRENT_YEAR;
	var $renderSetting = array(
		'bindModel'	=> array(
			'belongsTo'	=>	array('Client','Company','CompanyWorkPosition','CompanyMoneyItem'),
			'hasOne'	=>	array(
				'ClientWorkingHour'=>array(
					'conditions' => array(
						'ClientWorkingHour.year'=> CURRENT_YEAR,
						'ClientWorkingHour.month'=> CURRENT_MONTH
					)
				),
                'ClientUcetniDokumentace'=>array(
                    'conditions' => array(
                        'ClientUcetniDokumentace.kos'=> 0,
                        'ClientUcetniDokumentace.forma = CompanyMoneyItem.name'
                    )
                )
			)
		),
		'controller'	=>	'employees',
		'SQLfields' 	=>	'*',
		'SQLcondition'	=>  array(
			"((DATE_FORMAT(ConnectionClientRequirement.from,'%Y-%m')<= '#CURRENT_YEAR#-#CURRENT_MONTH#'))",
			'((DATE_FORMAT(ConnectionClientRequirement.to,"%Y-%m") >= "#CURRENT_YEAR#-#CURRENT_MONTH#") OR (ConnectionClientRequirement.to = "0000-00-00 00:00"))',
			'ConnectionClientRequirement.type'=> 2,
           // 'ConnectionClientRequirement.new'=> 0
		),
		'page_caption'=>'Docházky zaměstnanců',
		'sortBy'=>'Company.name.ASC',
		'top_action' => array(
		//	'add_item'		=>	'Přidat|edit|Přidat novou položku|add',
		),
		'filtration' => array(
            'Client-name'		=>	'text|Klient|',
			'ConnectionClientRequirement-company_id'	=>	'select|Společnost|company_list',
            'GET-current_month'							=>	'select|Měsíc|mesice_list',
             'ClientWorkingHour-change_max_salary'		=>	'checkbox|Změn. max.|',
			'CompanyWorkPosition-name|'			=>	'text|Pozice|',	
			'Company-client_manager_id|coordinator_id|coordinator_id2-cmkoo'		=>	'select|CM/KOO|cm_koo_list',
            'GET-current_year'							=>	'select|Rok|actual_years_list',
           
        ),
		'items' => array(
			'id'				=>	'ID|ConnectionClientRequirement|id|hidden|',
			'company'			=>	'Společnost|Company|name|text|',
			'req'				=>	'Profese|CompanyWorkPosition|name|text|',
			
			'client_id'			=>	'Klient ID|Client|id|text|',
            //'obj'			=>	'Obj.|ConnectionClientRequirement|requirements_for_recruitment_id|text|',
			'client'			=>	'Klient|Client|name|text|',
			//'cc'				=>	'Client č.|ClientWorkingHour|client_number|text|',
			'stav'				=>	'Stav|ClientWorkingHour|stav|var|kalkulace_stav_list',
			'nastup'			=>	'Datum nástupu|ConnectionClientRequirement|from|date|',
			'ukonceni'			=>	'Datum ukončení|ConnectionClientRequirement|to|date|',
            'cuctu'			    =>	'Číslo účtu|Client|cislo_uctu|text|',
			'formi'				=>	'CID|CompanyMoneyItem|id|text|',
			'forma'				=>	'Forma|CompanyMoneyItem|name|text|',
			//'norhod'			=>	'Normo hodina|ClientWorkingHour|standard_hours|text|',
			//'svatky'			=>	'Svátky|ClientWorkingHour|svatky|text|',
			//'vikendy'			=>	'Víkendy|ClientWorkingHour|vikendy|text|',
			//'pn'				=>	'PN|ClientWorkingHour|pracovni_neschopnost|text|',
			//'dovolena'			=>	'Dovolená|ClientWorkingHour|dovolena|text|',
			//'prescasem'			=>	'Přesčas|ClientWorkingHour|prescasy|text|',
			'celkem'			=>	'Celkem|ClientWorkingHour|celkem_hodin|text|',
			'change_max_salary'	=>	'#|ClientWorkingHour|change_max_salary|text|status_to_ico2#zmena_maximalky-Změna maximálky-1',
			'stav_dok'			=>	'#|ClientUcetniDokumentace|stav|text|status_to_ico2#chybna_dokumentace-Neuplná účetní dokumentace'
			//'marze'				=>	'Marže|ClientWorkingHour|marze|text|'
		),
		'posibility' => array(
			'edit'			=>	'odpracovane_hodiny|Editovat položku|edit',
			'uzavrit_odpracovane_hodiny'	=>	'uzavrit_odpracovane_hodiny|Uzavřít docházku|uzavrit_odpracovane_hodiny',
			'ucetni_dokumentace'	=>	'ucetni_dokumentace|Účetní dokumentace|ucetni_dokumentace',
			'client_info'	=>	'client_info|Karta klienta|client_info',
			//'autorizace'			=>	'autorizace|Autorizovat|autorizace',
            'modify_from_date'	=>	'modify_from_date|Editace zamestanani klienta|modify_from_date',	
		),
        'posibility_link' => array(
            'modify_from_date'=>'ConnectionClientRequirement.id/Client.id'
        ),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'	=> true,
			'languages'		=> true,
		)
	);

	

	function beforeRender(){
		parent::beforeRender();

		// úprava nadpisu reportu a přidání do něj datumu
        if(isset($this->viewVars['renderSetting']))
		  $this->set('spec_h1', $this->viewVars['renderSetting']['page_caption'].' za měsíc '.$this->mesice_list[ltrim($this->mesic,'0')].' a rok '.$this->rok);
	}
	
	function index(){
		// set FastLinks
		$this->set('fastlinks',array('ATEP'=>'/','Klienti'=>'#','Docházky zaměstnanců'=>'#'));
		
		$this->loadModel('CmsUser');
		$this->set('cm_koo_list',$cm_koo_list = $this->CmsUser->find('list',array(
			'conditions'=>array(
				'kos'=>0,
				'status'=>1,
				'cms_group_id IN(3,4)'
			)
		)));
		
		$this->loadModel('CompanyWorkPosition');
		$cwp_list = $this->CompanyWorkPosition->find('list',array(
				'conditions'=>array(
					'CompanyWorkPosition.kos'=>0,
					'CompanyWorkPosition.test'=>0
					// 'OR'=>array(
						// 'CompanyWorkPosition.id'=>$this->data["ClientWorkingHour"]["company_work_position_id"],
						// 'CompanyWorkPosition.parent_id'=>$this->data["ClientWorkingHour"]["company_work_position_id"]
					// )
				)
		));
		$this->loadModel('CompanyMoneyItem');
		$cmi_list_query = $this->CompanyMoneyItem->find('all',array(
				'conditions'=>array('CompanyMoneyItem.kos'=>0),
				'fields'=>array('id','name','doprava','cena_ubytovani_na_mesic')
		));
		foreach($cmi_list_query as $mi){
			$cmi_list[$mi['CompanyMoneyItem']['id']] = $mi['CompanyMoneyItem']['name'].'|'.$mi['CompanyMoneyItem']['doprava'].'|'.$mi['CompanyMoneyItem']['cena_ubytovani_na_mesic'];
		}
	//var_dump($cmi_list_query);
		//změna profese podle nastaveneho company_work_position_id
		foreach($this->viewVars['items'] as &$item){
            
			if(isset($item['ClientWorkingHour']['company_work_position_id']) && $item['ClientWorkingHour']['company_work_position_id'] != $item['ConnectionClientRequirement']['company_work_position_id'])
				$item['CompanyWorkPosition']['name'] = $cwp_list[$item['ClientWorkingHour']['company_work_position_id']];
		
			//nastaveni forem odmeny a zobrazeni ubytovani a dopravy
			if(isset($item['CompanyMoneyItem'])){
				// pokud jsou rozdilna v ulozene dochazce nez v connection
				if($item['ClientWorkingHour']['company_money_item_id'] != $item['ConnectionClientRequirement']['company_money_item_id'] && $item['ClientWorkingHour']['company_money_item_id']<>0){
					$pole = explode('|',$cmi_list[$item['ClientWorkingHour']['company_money_item_id']]);
					$doprava = 'Doprava '.($pole[1] != 0 ? 'Ano' : 'Ne');
					$ubytovani = 'Ubytování '.($pole[2] != 0 ? 'Ano' : 'Ne');
					$name = $pole[0];
					$item['CompanyMoneyItem']['forma'] = $name;
					$item['CompanyMoneyItem']['name'] = $name.' - '.$ubytovani.' / '.$doprava;;
				}
				else { //jinak vem z conection
					$doprava = 'Doprava '.($item['CompanyMoneyItem']['doprava'] != 0 ? 'Ano' : 'Ne');
					$ubytovani = 'Ubytování '.($item['CompanyMoneyItem']['cena_ubytovani_na_mesic'] != 0 ? 'Ano' : 'Ne');
	                $item['CompanyMoneyItem']['forma'] = $item['CompanyMoneyItem']['name'];
					$item['CompanyMoneyItem']['name'] = $item['CompanyMoneyItem']['name'].' - '.$ubytovani.' / '.$doprava;;
				}
                
			}
     
		}
	
		
		$company_conditions = array(
			 "((DATE_FORMAT(ConnectionClientRequirement.from,'%Y-%m')<= '".CURRENT_YEAR."-".CURRENT_MONTH."'))",
			 '((DATE_FORMAT(ConnectionClientRequirement.to,"%Y-%m") >= "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'") OR (ConnectionClientRequirement.to = "0000-00-00 00:00"))',
			 'ConnectionClientRequirement.type'=> 2
		);
		
		if (isset($this->filtration_company_condition))
			$company_conditions = am($company_conditions, $this->filtration_company_condition);
		
      
        $this->ConnectionClientRequirement->unbindModel(array('hasOne'=>array('ClientUcetniDokumentace')));
		$this->ConnectionClientRequirement->bindModel(array('belongsTo'=>array('Company')));
        
		$company_list = $this->ConnectionClientRequirement->find('list',array(
			 'conditions'=> $company_conditions,
			 'fields'=>array('Company.id','Company.name'),
			 'recursive'=>1
		 ));
		 $this->set('company_list',$company_list);
		 
		
		if ($this->RequestHandler->isAjax()){
			$this->set('change_list_js', json_encode(array('filtr_ConnectionClientRequirement-company_id'=>$company_list)));
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
		
	}


    function cancel_auth($id){
        $this->loadModel('ClientWorkingHour');
        if ($this->ClientWorkingHour->save(array(
            'id' => $id,
            'stav' => 1
        ))) {
            die(json_encode(array('result' => true)));
        } else {
            die(json_encode(array('result' => false,'message'=>'Chyba během ukládaní do DB')));
        }

    }

	
	function zam_refresh($requirements_for_recruitment_id, $year =null, $month=null){
   	if ($year == null || $month == null) die(); 
		$detail = $this->RequirementsForRecruitment->read(null,$requirements_for_recruitment_id);

		$this->loadModel('ConnectionClientRequirement');
		$this->ConnectionClientRequirement->bindModel(array(
			'belongsTo'=>array('Client'),
			'hasOne'	=>	array(
            'ClientWorkingHour' => array( 'conditions' => 
                    array('ClientWorkingHour.year'=>$year,'ClientWorkingHour.month'=>$month)))
		    ));
		 if($month<10) $month="0".$month;
			$from=	'((DATE_FORMAT(ConnectionClientRequirement.from,"%Y-%m")<= "'.$year.'-'.$month.'"))';
			$to=	'((DATE_FORMAT(ConnectionClientRequirement.to,"%Y-%m") >= "'.$year.'-'.$month.'") OR (ConnectionClientRequirement.to = "0000-00-00 00:00"))';
  	
		$zamestnanci_list = $this->ConnectionClientRequirement->find(
			'all', 
			array(
				'conditions'=>array('ConnectionClientRequirement.type'=>2,'ConnectionClientRequirement.requirements_for_recruitment_id'=>$requirements_for_recruitment_id,$from,$to),
				'fields'=>array(
					'ConnectionClientRequirement.id',
					'ClientWorkingHour.svatky',
					'ClientWorkingHour.pracovni_neschopnost',
					'ClientWorkingHour.prescasy',
					'ClientWorkingHour.vikendy',
					'ClientWorkingHour.dovolena',
					'ClientWorkingHour.celkem_hodin',
					'Client.id',
					'Client.name',
					'ConnectionClientRequirement.from'
				),
				'order'=>'ConnectionClientRequirement.id ASC'
			)
		);
		$this->set('zamestnanci_list',$zamestnanci_list);
		$this->set('detail',$detail);
		$this->set('rok',$year);
		$this->set('mesic',$month);
		
		$this->render('zamestnanci_list_refresh');
		//pr($zamestnanci_list);

	}
	
	private function read_connection($connection_client_requirement_id = null, $year = null, $month = null){
		// loadmodel && bindModel
			$this->ConnectionClientRequirement->bindModel(array(
				'belongsTo'	=>	array('RequirementsForRecruitment','Client',
                                'CompanyOrderItem'=>array(
                                    'foreignKey'=>'requirements_for_recruitment_id'                                   
                                )
                ),
				'hasOne'	=>	array('ClientWorkingHour' => array('conditions' =>array('ClientWorkingHour.year'=>$year,'ClientWorkingHour.month'=>$month)))
			));

			// read from db
			return  $this->ConnectionClientRequirement->read(null,$connection_client_requirement_id);
	}
	
    //funkce na prepocet prescasu
	private function prepocet_prescasy($setting){
		$pd = 0;
		$this->loadModel('SettingStatPd');
		$pd_list = $this->SettingStatPd->find('list',array(
				'conditions'=>array(
					'kos'=>0,
					'mesic'=>CURRENT_MONTH,
					'rok'=>CURRENT_YEAR
				),
				'fields'=>array('setting_stat_id','pocet')
		));
		$nh = $setting['normo_hodina'];
		//pokud exituji nastaveny pracovni fond pro tento mesic a rok
		if(array_key_exists($setting['stat_id'],$pd_list))
			$pd = $pd_list[$setting['stat_id']];
		if($pd <> 0 && $nh <> 0){
			return  ($nh * $pd);
		}
		else 
			return 0;
	}
    
    
    /**
     * Funkce na kontrolu prenesenenych hodinovek z minuleho mesice,
     * pokud byly hodinovky vetsi nez stavajici maximalky formy odmeny vrat je na maximalni hdonotu
     * @since 12.11.09
     * @author Sol
     */
    private function check_salary_hour_of_value($data){
      
         $testing_array = array(
            'salary_part_1',
            'salary_part_2',
            'salary_part_3',
            'salary_part_4',
            'odmena_1',
            'odmena_2'
         );
         //$data['ClientWorkingHour']['salary_per_hour'] = 0;
         foreach($testing_array as $item){
            if($data['ClientWorkingHour'][$item] > $data['ClientWorkingHour'][$item.'_max']){
                //$data['ClientWorkingHour'][$item] = $data['ClientWorkingHour'][$item.'_max'];
                if(substr($item,0,6) == 'salary'){
                    //preneseme i maximalku
                    $data['ClientWorkingHour'][$item.'_max'] = $data['ClientWorkingHour'][$item];
                    //upozornujeme ze se prenasi zmena maximalky
                    $data['ClientWorkingHour']['change_salary_max'] = 1;
                }
                //$data['ClientWorkingHour']['salary_per_hour'] +=  $data['ClientWorkingHour'][$item];      
            }
         }

         return $data;
    }
    
    
	
	/**
 	* Returns a view of "odpracovane hodiny"
 	*
	* @param $connection_client_requirement_id (INT)
	* @param $year (DATE(Y))
	* @param $month DATE(m))
	* @param $inner (STRING)
 	* @return view
 	* @access public
	**/
	public function odpracovane_hodiny($connection_client_requirement_id = null, $year = null, $month = null, $inner = null){
            $requirements_table = 'RequirementsForRecruitment';


            if (empty($this->data)){
                if ($connection_client_requirement_id == null || $year == null || $month== null){
				    die('Chyba, neznami cinitele');
			}
            
           /**
            * nastaveni predchoziho mesice se spravnym rokem 
            */    
            $prev_month_date = new DateTime();
            $prev_month_date->setDate($year, $month, 01);
            $prev_month_date->modify("-1 month");
		

			$this->data = $this->read_connection($connection_client_requirement_id, $year, $month);
			
            /**
             * kontrola, zda v connection nejsou chybne zadane data propusteni
             * Nekdy se jedna o umysl administratora, 
             * takhle zakryt chybu koordinatora aby nemohl takovou dochazku jiz editovat 
             */
            if($this->data['ConnectionClientRequirement']['to'] != '0000-00-00' && $this->data['ConnectionClientRequirement']['from'] > $this->data['ConnectionClientRequirement']['to'])	
                die('Datum ukončení je měnší než datum nástupu, tato docházka, nelze editovat!');

            if($this->data['ConnectionClientRequirement']['objednavka'] == 1)
                $requirements_table = 'CompanyOrderItem';   
          
            // pokud je ulozena dochazka a jeji foram odemny neulozena nastav globalni
			// if($this->data['ClientWorkingHour']['company_money_item_id'] == 0)
				// $this->data['ClientWorkingHour']['company_money_item_id'] = $this->data['ConnectionClientRequirement']['company_money_item_id'];
	
   			$this->loadModel('Company');
			$company_stat_id = $this->Company->read(array('stat_id','payment_cash','payment_amount','payment_once_in_month','manazer_realizace_id'),$this->data['ConnectionClientRequirement']['company_id']);
            $this->set('company_detail',$company_stat_id);

            $transfer_from_last_month = false;
            
			if (empty($this->data['ClientWorkingHour']['id'])){
				//company_work_position_id se nyni bere z connection_client_requirements
				$this->data['ClientWorkingHour']['company_work_position_id'] = $this->data['ConnectionClientRequirement']['company_work_position_id'];


				$prev_month =  $this->read_connection($connection_client_requirement_id, $prev_month_date->format('Y'),$prev_month_date->format('m'));
				if (!empty($prev_month['ClientWorkingHour']['id'])){
				    $tran_prev_array = array('client_number','setting_shift_working_id','cena_za_ubytovani','company_work_position_id','company_money_item_id','accommodation_id','including_of_accommodation','including_of_meals_ticket','salary_per_hour','salary_part_1','salary_part_2','salary_part_3','salary_part_4','accommodation_cost','recruitment_cost','clothes_cost','transfer_cost','food_cost','including_of_transport','including_suit','company_cw_template_id');
					foreach($tran_prev_array as $tran)
						$this->data['ClientWorkingHour'][$tran] = $prev_month['ClientWorkingHour'][$tran];
                    
                    /**
                     * Prenaseni sablony dochazku hodinovek
                     * pokud je sablona zadana, vyhledavame novou verzi pro novy mesic v db
                     * pokud pro tuto sablonu ktera byla v minulem mesici existuje nadefinovany i tento - novy mesic, nacti id
                     */
                    if($this->data['ClientWorkingHour']['company_cw_template_id'] != ''){
                         $this->loadModel('CompanyClientWorkingTemplateItem');
                         $ex = $this->CompanyClientWorkingTemplateItem->read('company_client_working_template_id',$this->data['ClientWorkingHour']['company_cw_template_id']);
                         if($ex){
                             $for_new_month = $this->CompanyClientWorkingTemplateItem->find('first',array(
                                'conditions'=>array(
                                    'company_client_working_template_id'=>$ex['CompanyClientWorkingTemplateItem']['company_client_working_template_id'],
                                    'rok'=>$year,
                                    'mesic'=>$month,
                                    'kos'=>0
                                )
                             ));
                             
                             if($for_new_month){
                                $this->data['ClientWorkingHour']['company_cw_template_id'] = $for_new_month['CompanyClientWorkingTemplateItem']['id'];
                             }
                         }
                    }
                        
					$transfer_from_last_month = true;
					$this->set('transfer_from_last_month',$transfer_from_last_month);
				
					//novinka - 13.10.09 -> pokud bylo zmeneno v connection profese a 
					// 						odmena vem pro nasledujici mesic jejich ne z predesleho
					if($prev_month['ClientWorkingHour']['company_work_position_id'] <> $this->data['ConnectionClientRequirement']['company_work_position_id'])
						$this->data['ClientWorkingHour']['company_work_position_id'] = $this->data['ConnectionClientRequirement']['company_work_position_id'] ;
					if($prev_month['ClientWorkingHour']['company_money_item_id'] <> $this->data['ConnectionClientRequirement']['company_money_item_id'])
						$this->data['ClientWorkingHour']['company_money_item_id'] = $this->data['ConnectionClientRequirement']['company_money_item_id'] ;
				}				
			}
			else {
				if($this->logged_user['CmsGroup']['id']==1 && $this->data['ClientWorkingHour']['company_work_position_id'] == 0){
					$this->data['ClientWorkingHour']['company_work_position_id']= $this->data['ConnectionClientRequirement']['company_work_position_id'];
					echo "Profese byla špatně nastavena pracujete s přenesenou profesi která je pro neuložené docházky.";
				}
			}
	

			$this->data['ClientWorkingHour']['connection_client_requirement_id'] = $connection_client_requirement_id;
			$this->data['ClientWorkingHour']['company_id'] = $this->data['ConnectionClientRequirement']['company_id'] ;
			$this->data['ClientWorkingHour']['client_id'] = $this->data['ConnectionClientRequirement']['client_id'] ;
			$this->data['ClientWorkingHour']['requirements_for_recruitment_id'] = $this->data['ConnectionClientRequirement']['requirements_for_recruitment_id'] ;
			
			

			// if not exist ClientWorkingHour load dota from RequirementsForRecruitment
			if (empty($this->data['ClientWorkingHour']['id']) && empty($prev_month['ClientWorkingHour']['id'])){
				$transfer_arrray_con_rec = array('company_id','client_id', 'company_money_item_id', 'requirements_for_recruitment_id', 'company_work_position_id'); 		
				//zruseno 4.1.10
                //$transfer_arrray_rec = array( 'setting_shift_working_id', 'including_of_accommodation', 'including_of_meals_ticket','including_of_transport' ); 		
				$transfer_arrray_rec = array( 'setting_shift_working_id' ); 		
				foreach($transfer_arrray_con_rec as $transform) $this->data['ClientWorkingHour'][$transform] = $this->data['ConnectionClientRequirement'][$transform];
				foreach($transfer_arrray_rec as $transform)		$this->data['ClientWorkingHour'][$transform] = $this->data[$requirements_table][$transform];
			}

			if($this->data['ClientWorkingHour']['company_work_position_id']==0 )
				die('Chybně nastavena profese v náboru, při zaměstnání!!! Většinou se jedná o staré umístění klientů.');
			
            
			// load calculation for work_position
			$money_items = $this->load_calculation_for_prefese($this->data['ClientWorkingHour']['company_work_position_id'],$year,$month, false);
			
         //  var_dump($money_items);


            
            $money_item_list = array();
			$money_item_title = array();
			$prispevky_z_kalkulace = array();
			$nacti_def = false;
			$this->set('ubytovani_select',false);
            $this->set('odmeny_typ',1);  //normalni zobrazeni - jinak je zobrazeni pro pausal, meni se view
                
			if ($money_items){
                //	echo Count($money_items);
				foreach($money_items as $money_item){
					$doprava = 'Doprava '.($money_item['doprava'] != 0 ? 'Ano' : 'Ne');
					$ubytovani = 'Ubytování '.($money_item['cena_ubytovani_na_mesic'] != 0 ? 'Ano' : 'Ne');
	
					$money_item_list[$money_item['id']] = $money_item['name'].' - '.$money_item['cista_mzda_z_pracovni_smlouvy_na_hodinu'].'/'.$money_item['cista_mzda_dohoda_na_hodinu'].'/'.$money_item['cista_mzda_faktura_zivnostnika_na_hodinu'].'/'.$money_item['cista_mzda_cash_na_hodinu'].' - '.$ubytovani.' / '.$doprava;
					$money_item_title[$money_item['id']] = $money_item['name'].'|'.$money_item['stravenka'].'|'.$money_item['normo_hodina'].'|'.$money_item['doprava'].'|'.$money_item['fakturacni_sazba_na_hodinu'];
					
					if($money_item['name'] == '1001') { $nacti_def = true;}

					if($money_item['id']==$this->data['ClientWorkingHour']['company_money_item_id']){
					   $this->data['money_item_name'] = $money_item['name'];
                       /**
                        * prideleni hodnot pro PS 
                        * prispevky stravne a cestovne
                        */
                        $prispevky_z_kalkulace = array(
                            'cestovne' => $money_item['vlozeny_prispevek_na_cestovne'],
                            'stravne' => $money_item['vlozeny_prispevek_na_stravne']
                        );  
				
						$this->set('max_salary_hours',$money_item);
						$this->data['ClientWorkingHour']['standard_hours']=$money_item['normo_hodina'];
						$this->Employees->set_new_odmeny($money_item['odmena_max_1'],1,'max');
						$this->Employees->set_new_odmeny($money_item['odmena_max_2'],2,'max');
                        
                        //pokud je pausal
                        if($money_item['pausal'] == 1)
							$this->set('odmeny_typ',2);
                        
                        //pokud je h1000
                        if($money_item['h1000'] == 1){
							$this->set('odmeny_typ',3);                           
                            $odmeny_data = array(
                                1 => array(
                                    'label'=>$money_item['ps_hodina_odmena_1'],
                                    'max'=> $money_item['ps_hodina_max_1']
                                ),
                                2 => array(
                                    'label'=>$money_item['ps_hodina_odmena_2'],
                                    'max'=>$money_item['ps_hodina_max_2']
                                )
                            );
                            
                            $this->Employees->set_new_odmeny($odmeny_data);
                        }    
                            
                        $this->data['ClientWorkingHour']['ps_pausal_hmm']=$money_item['ps_pausal_hmm'];
                        $this->data['ClientWorkingHour']['ps_hodina_hmm']=$money_item['ps_hodina_hmm'];
                        $this->data['ClientWorkingHour']['ps_hodina_hmm_monthly']=$money_item['ps_hodina_hmm_monthly'];
                      
						
						if($money_item['cena_ubytovani_na_mesic'] != 0){//povol zobrazeni selectu ubytovani
							$this->set('ubytovani_select',true);
                            $this->set('cena_ubytovani_na_mesic', $money_item['cena_ubytovani_na_mesic']);
                        }    

						// nove nastavnei maximalek, preneseni z money item
						if (empty($this->data['ClientWorkingHour']['id'])){
						$this->data['ClientWorkingHour']['salary_part_1_max']=$money_item['cista_mzda_z_pracovni_smlouvy_na_hodinu'];
						$this->data['ClientWorkingHour']['salary_part_2_max']=$money_item['cista_mzda_dohoda_na_hodinu'];
						$this->data['ClientWorkingHour']['salary_part_3_max']=$money_item['cista_mzda_faktura_zivnostnika_na_hodinu'];
						$this->data['ClientWorkingHour']['salary_part_4_max']=$money_item['cista_mzda_cash_na_hodinu'];
                            
                            //otestovani zda nepresahuji prevedene hodinovky maximalky 
                            if($transfer_from_last_month){
                               $this->data =  $this->check_salary_hour_of_value($this->data);
                            }    
                            
                            //DPC
                            $this->data['ClientWorkingHour']['fa_dpc'] = $money_item['fa_dpc'];
						}
						else {
							if($this->data['ClientWorkingHour']['salary_part_1_max'] == 0)
								$this->data['ClientWorkingHour']['salary_part_1_max']=$money_item['cista_mzda_z_pracovni_smlouvy_na_hodinu'];
							if($this->data['ClientWorkingHour']['salary_part_2_max'] == 0)
								$this->data['ClientWorkingHour']['salary_part_2_max']=$money_item['cista_mzda_dohoda_na_hodinu'];
							if($this->data['ClientWorkingHour']['salary_part_3_max'] == 0)
								$this->data['ClientWorkingHour']['salary_part_3_max']=$money_item['cista_mzda_faktura_zivnostnika_na_hodinu'];
							if($this->data['ClientWorkingHour']['salary_part_4_max'] == 0)
								$this->data['ClientWorkingHour']['salary_part_4_max']=$money_item['cista_mzda_cash_na_hodinu'];
						
                            if($this->data['ClientWorkingHour']['fa_dpc'] == 0){
                                $this->data['ClientWorkingHour']['fa_dpc'] = $money_item['fa_dpc'];
                            }    
                        }
					}
				}
			}
                /*echo Count($money_item_list);
                var_dump($money_item_list);*/
			$this->set('prispevky_z_kalkulace', $prispevky_z_kalkulace);
			$this->set('money_item_list', $money_item_list);
			$this->set('money_item_title', $money_item_title);
            
            $this->loadModel('Setting'); 
			$setting_def= array();
			$setting_def = $this->Setting->read(null,($company_stat_id['Company']['stat_id'] == 1 ? 3 : 4));

			//nacteni default pro 1001 kombinaci
			if($nacti_def){
				$this->set('cista_mzda_1001', $setting_def['Setting']['value']['cista_mzda_1001']);
			}
            
            //limit pro 0101 a 0100
            if(isset($setting_def['Setting']['value']['dohoda_limit']) && $setting_def['Setting']['value']['dohoda_limit'] > 0)
                $this->set('dohoda_limit', $setting_def['Setting']['value']['dohoda_limit']);

			//informace o skupine prihlaseneho pro tl. ulozeni
			$this->set('logged_group', $this->logged_user['CmsGroup']['id']);
			$this->set('logged_user', $this->logged_user['CmsUser']['id']);
			
            
            
			// set disabled || enabled sub_salary
			$disabled_arr = array();
            if (!empty($this->data['ClientWorkingHour']['company_money_item_id'])){
				for ($i = 0; $i < 4; $i++)  $disabled_arr[$i+1] = ($money_item_title[$this->data['ClientWorkingHour']['company_money_item_id']][$i] == 0)?'disabled':null;
				$this->data['ClientWorkingHour']['stravenka'] = av(explode('|',$money_item_title[$this->data['ClientWorkingHour']['company_money_item_id']]),1);
			} else {
				for ($i = 0; $i < 4; $i++) $disabled_arr[$i+1] = 'disabled';
				$this->data['ClientWorkingHour']['stravenka'] = 0;
			}

			$this->set('sub_salary_disabled',$disabled_arr);
				
			// load list of accomodations, in future will be  add condition for cms_user_id
				 //podminka pro jednotliveho usera jen k nemu prirazene ubytovny
                $acc_con = array(
                    'ConnectionAccommodationUser.cms_user_id'=>array(-2,$this->logged_user['CmsUser']['id'])
                );
                
                //pokud se jedna o adminy nebo DIR tato podminak pro ne neplati
                if(in_array($this->logged_user['CmsGroup']['id'],array(1,5)) || in_array($this->logged_user['CmsUser']['id'],array(209)))
                    $acc_con = array();
                
                $this->loadModel('Accommodation');
				$this->Accommodation->bindModel(array(
					'hasOne'=>array('ConnectionAccommodationUser'=>array('foreignKey'=>'accommodation_id'))
				));                 
				$this->set('accommodation_list',$x = $this->Accommodation->find('list',array(
					'fields'=>array('Accommodation.id','Accommodation.name'),
					'conditions'=>am(array('Accommodation.kos'=>0),$acc_con),
                    'recursive'=>1
				)));
                $this->Accommodation->bindModel(array(
					'hasOne'=>array('ConnectionAccommodationUser'=>array('foreignKey'=>'accommodation_id'))
				)); 
				$this->set('accommodation_title_list',$atl = $this->Accommodation->find('superlist',array(
					'conditions'=>am(array('Accommodation.kos'=>0),$acc_con),
					'fields'=>array('Accommodation.id','type','price'),
					'separator'=>'|',
                    'recursive'=>1
				)));
				unset($this->Accommodation);

			// load list of clients for subdodavka in company
			$this->ConnectionClientRequirement->bindModel(array(
				'belongsTo'	=>	array('Client')
			));	
			$subdodavka_list = array();
			$k = $this->ConnectionClientRequirement->find('all',array(
				'fields'=>array('client_id','Client.name'),
				'conditions'=>array(
					'ConnectionClientRequirement.company_id'=>$this->data['ConnectionClientRequirement']['company_id'],
					'type'=>2,
					'client_id <>'=>$this->data["ConnectionClientRequirement"]["client_id"]
				)
			));
			foreach($k as $item){
				$subdodavka_list[$item['ConnectionClientRequirement']['client_id']] = $item['Client']['name'] ;
			}
			$this->set('subdodavka_list', $subdodavka_list);  

    
			// load list of work position
			$this->loadModel('CompanyWorkPosition');
			$cwp = $this->CompanyWorkPosition->find('all',array(
				'conditions'=>array(
					'CompanyWorkPosition.kos'=>0,
					'CompanyWorkPosition.test'=>0,
					'CompanyWorkPosition.company_id'=>$this->data['ClientWorkingHour']['company_id']
					// 'OR'=>array(
						// 'CompanyWorkPosition.id'=>$this->data["ClientWorkingHour"]["company_work_position_id"],
						// 'CompanyWorkPosition.parent_id'=>$this->data["ClientWorkingHour"]["company_work_position_id"]
					// )
				)
			));
			//if(!array_ key_ exists($this->data["ClientWorkingHour"]["company_work_position_id"],$cwp));

			// vytvoreni listu profesi
			foreach($cwp as $item){
				// pokud je nastavena hlavni normo hodina nastav NH z profese - jiank se deafultne bere z formy odmeny
				if($item['CompanyWorkPosition']['id']==$this->data['ClientWorkingHour']['company_work_position_id'] && $item['CompanyWorkPosition']['standard_hours']<>0)
					$this->data["ClientWorkingHour"]["standard_hours"] = $item['CompanyWorkPosition']['standard_hours'];
					
				$company_work_position_list[$item['CompanyWorkPosition']['id']] = $item['CompanyWorkPosition']['name'];
			}
            
			$this->set('company_work_position_list',$company_work_position_list);
			unset($this->CompanyWorkPosition);


            /**
             * nacteni zaloh k danemu connection
             */
			$this->loadModel('ReportDownPayment');
            $this->ReportDownPayment->bindModel(array(
                'belongsTo'=>array('CmsUser')
            ));
			$rdp = $this->ReportDownPayment->find('all',array(
				'conditions'=>array(
					'ReportDownPayment.kos'=>0,
                    'OR'=>array(
                        array(
        					'(DATE_FORMAT(ReportDownPayment.created,"%Y-%m"))' => $year.'-'.$month,
        					'ReportDownPayment.month' =>'0',
        					'ReportDownPayment.year' =>'0'
                        ),
                        array(
                            'ReportDownPayment.month' =>$month,
        					'ReportDownPayment.year' =>$year
                        )    
					),
                    'ReportDownPayment.client_id'=>$this->data['ClientWorkingHour']['client_id'],
					'ReportDownPayment.connection_client_requirement_id'=>$connection_client_requirement_id
				)
			));
			$this->set('payment_list',$rdp);
			$this->set('payment_sum',0);
			unset($this->ReportDownPayment);
            

						
			// default variable na index
			$this->set('stat_id', $company_stat_id['Company']['stat_id']); 
			$this->set('connection_client_requirement_id', $connection_client_requirement_id); 
			$this->set('year', $year); 
			$this->set('month', $month); 
			
			// kill rendering, if client dont work in this year and month, or you select future;
			list($start_y, $start_m, $start_d) 	= explode('-', $this->data['ConnectionClientRequirement']['from']);
			list($end_y, $end_m, $end_d) 		= explode('-', $this->data['ConnectionClientRequirement']['to']);
			
			// for Test Only = $end_y = '2009'; 	$end_m = '06';
			// condition for kill rendering
			$render_element = 0;

			if (( $year <= date('Y') && $month <= date('m') ) || ($month > date('m') && $year < date('Y'))){

				if (($end_y == '0000' && $start_y <= $year && $start_m <= $month) || ($end_y == '0000' && $start_y < $year && $start_m > $month)) {
					$render_element = 1;	
				} else if ($end_y != '0000' && $start_y <= $year && $start_m <= $month && $end_y >= $year && $end_m >= $month){
					$render_element = 1;
				}
			} else {
				$render_element = -1;
			}
			// load free days for state, month, year
			$this->loadModel('SettingStatSvatek');
			$this->set('svatky',$this->SettingStatSvatek->find('list', array('conditions'=>array( 'setting_stat_id'	=> av(av($company_stat_id,'Company'),'stat_id'), 'mesic'=> $month, 'rok'=> $year ),'fields'=>array('id','den'),'order'=>'den ASC'))); 				
			unset($this->Company);
			unset($this->SettingStatSvatek);
					
			//prescasy
			$this->data['ClientWorkingHour']['max_hodin_v_mesici'] = $this->prepocet_prescasy(array(
				'normo_hodina'=> $this->data["ClientWorkingHour"]["standard_hours"],
				'stat_id'=> $company_stat_id['Company']['stat_id']
			));
			//prescasy end
            
            
            /**
             * VYPNUTO od 8.3.2012
             * osetreni podminky c.1 pro zalohy
             * pokud existuje dochazka v minulem mesici  = zakaz vlozit pozadavek na zalohu 
             * v terminu 15. – 25. dne v mesici
             * 
             * dotazy usetrime tim ze to budeme provadet az 15-25 dnu
             */
             $allow_payment_0 = $company_stat_id['Company']['payment_amount'];
             $allow_payment_1 = $company_stat_id['Company']['payment_cash'];
             /**
              * VYPNUTO od 8.3.2012
             if(self::is_in_interval(date('d'),15,25) && (isset($company_stat_id['Company']['payment_once_in_month']) && $company_stat_id['Company']['payment_once_in_month'] == 0)){
                $this->loadModel('ClientWorkingHour');
                $previous_client_working = $this->ClientWorkingHour->find('first',array(
                    'conditions'=>array(
                        'connection_client_requirement_id'=>$connection_client_requirement_id,
                        'year'=> $prev_month_date->format('Y'),
                        'month'=> $prev_month_date->format('m')
                    )
                ));
                
                
                /**
                 * pokud je ve vyjimce tak tuto podminku vynech
                
                $this->loadModel('CompanyExceptionPayment');
                $exception_company = $this->CompanyExceptionPayment->findByCompanyId($this->data['ClientWorkingHour']['company_id']);  
             */
            
                /**
                 * jestlize ma v predchozim mesici dochazku tak zamez vystaveni zalohy
                 
                 if($previous_client_working && !$exception_company)
                   $allow_payment_0 = false;
                   
             }
              */
              
             /**
               * V intervalu 9. - 15. v aktualni mesici nelze zadavat zalohy!!!
               */ 
             $this->loadModel('CompanyExceptionPayment');
             $exception_company = $this->CompanyExceptionPayment->find('first',array('conditions'=>array('company_id'=>$this->data['ClientWorkingHour']['company_id'],'kos'=>0)));  
                
            /* if(self::is_in_interval(date('d'),9,15) && !$exception_company){
                $allow_payment_0 = false;
             }*/
             if(isset($company_stat_id['Company']['payment_once_in_month']) && $company_stat_id['Company']['payment_once_in_month'] == 1){
                if(!isset($this->ReportDownPayment))
                    $this->loadModel('ReportDownPayment');
                
                 /**
                  * Nova podminka pokud je zaskrtnuta nova podminka u firem
                  * pouze jednou mesicne
                  * pokud najdeme zalohu v terminu od 16 v danem mesici tak zamez vlozeni
                  * pokud najdeme zalohu z predchazejiciho obdobi (16 minuleho - 8 toho mesice) - zamez vlozeni
                  */
                  $have_payment = false;
                  if(self::is_in_interval(date('d'),1,8)){
                        $have_payment = $this->ReportDownPayment->find('first',array(
                            'conditions'=>array(
                                'kos'=>0,
                                'client_id'=>$this->data['ClientWorkingHour']['client_id'],
                                'company_id'=>$this->data['ClientWorkingHour']['company_id'],
                                'connection_client_requirement_id'=>$this->data['ClientWorkingHour']['connection_client_requirement_id'],
                                'DATE_FORMAT(created,"%Y-%m-%d") >= DATE_SUB(DATE_FORMAT(NOW(),"%Y-%m-16"), INTERVAL 1 MONTH) AND DATE_FORMAT(NOW(),"%Y-%m-%d") <= DATE_FORMAT(NOW(),"%Y-%m-8")'
                            ),
                            'order'=>'id DESC'
                         ));  
                  }
                  else if(self::is_in_interval(date('d'),16,31)){
                        $have_payment = $this->ReportDownPayment->find('first',array(
                            'conditions'=>array(
                                'kos'=>0,
                                'client_id'=>$this->data['ClientWorkingHour']['client_id'],
                                'company_id'=>$this->data['ClientWorkingHour']['company_id'],
                                'connection_client_requirement_id'=>$this->data['ClientWorkingHour']['connection_client_requirement_id'],
                                'DATE_FORMAT(created,"%Y-%m-%d") >= DATE_FORMAT(NOW(),"%Y-%m-16") AND  DATE_FORMAT(created,"%Y-%m-%d") <= DATE_FORMAT(NOW(),"%Y-%m-31")'
                            ),
                            'order'=>'id DESC'
                         ));  
                  }
                  
                  /*if($have_payment){
                       $allow_payment_0 = false;     
                  }*/
             }
             
             /**
              * Permision na zalohy dle uz. skupiny 
              */
             if(isset($this->controller_permission['allow_uses_payment_amount']) && $this->controller_permission['allow_uses_payment_amount'] == false){
                $allow_payment_0 = false;
             }
                //$allow_payment_0 = true;
             /**
              * Poslední pravidlo pro zálohy před rendrem
              * Pokud má stale povolené zalohy je nutne zjistit zda uz odpracoval 80hodin!
              * tedy projit ulozene dochazky tohoto zamestani a spocitat sumu odprac. hodin
              */
            /* if($allow_payment_0 == true){
                $this->loadModel('ClientWorkingHour');
                $odpracovanych_hodin = $this->ClientWorkingHour->find('first',array(
                    'conditions'=>array(
                        'kos'=>0,
                        'client_id'=>$this->data['ClientWorkingHour']['client_id'],
                        'connection_client_requirement_id'=>$this->data['ClientWorkingHour']['connection_client_requirement_id']                    
                     ),
                    'fields'=>array('SUM(celkem_hodin) as odpracovanych_hodin'),
                    'group'=>'connection_client_requirement_id'
                 ));

                 //pokud je ve vyjimce tak tuto podminku vynech

                 if($odpracovanych_hodin[0]['odpracovanych_hodin'] < 80 && !$exception_company)
                    $allow_payment_0 = false;
             }*/

             $this->set('allow_payment_0', $allow_payment_0);
             $this->set('allow_payment_1', $allow_payment_1);
			 // END podminka 1
             
             
             /**
              * nastaveni promennych s kteryma pracuje komponenta
              * a zavolani setu pro view
              */
             $this->Employees->permission = $this->logged_user['CmsGroup']['permission']['employees'];
             $this->Employees->logged_user = $this->logged_user;
             $this->Employees->set('year',$year);
             $this->Employees->set('month',$month);
             $this->Employees->set('this_data',$this->data);
             $this->Employees->set('connection_id',$connection_client_requirement_id);
             $this->Employees->set_variables_for_view();
            
            
            /**
             * nastavení naroku na dovolenou
             * zde pouze odlisujeme zda se jedna o klienta s os. cislem a tedy jeho nula znamena
             * zaden narok na dovolenou
             * a klienty kteri nemaji vyplnene os. cislo a tedy nevime jaky je narok a vlozime otaznik
             * a dovolenou muzeme zadat
             */
             if($this->data['Client']['os_cislo'] == ''){
                $this->data['Client']['zbyva_dovolene'] = '?';
             }
             
             
            		
			// render view
			if ($inner != null)
				if ($render_element === 1)
					$this->render('element');
				else if ($render_element === -1)
				    die('<p>Umíte věštit? Jak to víte, kolik toho udělá?</p>');
				else 
					die('<p>Tady vubec nic nedělal!!</p>');
			else
				$this->render('index');
		} else {		
			$this->loadModel('ClientWorkingHour');

            if($this->data['ClientWorkingHour']['id'] == ''){
                 $this->Employees->set('year',$this->data['ClientWorkingHour']['year']);
                 $this->Employees->set('month',$this->data['ClientWorkingHour']['month']);
                 $this->Employees->set('connection_id',$this->data['ClientWorkingHour']['connection_client_requirement_id']);
                
                 if($this->Employees->if_exist_client_working_hour() === true)
                    die(json_encode(array('result'=>false, 'message' => 'Chyba: Snažíte se uložit duplicitní docházku!!! Obnovte okno, nebo zavřete okno a načtěte docházku znova.')));
            }

            //zmena money item
            if($this->data['transfer_money_item_id'] == 1)
                $this->company_money_item_id_to_connection($this->data['ClientWorkingHour']['connection_client_requirement_id'],$this->data['ClientWorkingHour']['company_money_item_id'],$this->data['ClientWorkingHour']['company_work_position_id']);

            /**
             * Pokud uklada nekdo jiz autorizovanou dochazku zasli emailovou notifikaci
             */
            if($this->data['ClientWorkingHour']['stav'] == 3){
                $this->loadModel('Company');
    			$company = $this->Company->read($this->Company->company_data_for_email_notification,$this->data['ClientWorkingHour']['company_id']);
                unset($this->Company);
                
                $this->loadModel('Client');
    			$cl = $this->Client->read(array('name'),$this->data['ClientWorkingHour']['client_id']);
                unset($this->Client);
                
                            
    			$replace_list = array(
    					'##CmsUser.name##' 	=>$this->logged_user['CmsUser']['name'],
    					'##Company.name##' 	=>$company['Company']['name'],
    					'##Client.name##' 	=>$cl['Client']['name'],
    					'##Rok##' 	=>$this->data['ClientWorkingHour']['year'],
    					'##Mesic##' 	=>$this->data['ClientWorkingHour']['month'],
    					'##Text##' 	=>$this->Employees->check_changes_in_wh($this->data),
    					'##Status##' 	=>($this->data['money_item_name'] == '0010' ? 'faktura' : 'výplatní páska')
    			);
                
                $this->Email->set_company_data($company['Company']);
    			$this->Email->send_from_template_new(18,array(),$replace_list);
            }
                
            // osetreni prazdeho stavu - defaultne 1, nepochopitelna chyba, 
            // nenastavuje db automaticky defaultni hodnotu    
            if($this->data['ClientWorkingHour']['stav'] == '')
                $this->data['ClientWorkingHour']['stav'] = 1;

			if($this->ClientWorkingHour->save($this->data)){
					die(json_encode(array('result'=>true, 'id'=>$this->ClientWorkingHour->id, 'message' => 'Úspěšně uloženo do databáze.')));
			} else {
				die(json_encode(array('result'=>false, 'message' => 'Chyba: Nepodařilo se uložit do DB.')));
			}
		}
	}
	
    /**
     * @since 12.11.09
     * @author Sol
     * @abstract privatni funkce pro zpracovani preneseni nove zvolene company_money_item_id,company_work_position_id do connection 
     */
     private function company_money_item_id_to_connection($connection_id = null,$company_money_item_id = null,$company_work_position_id){
        if($connection_id == null || $company_money_item_id == null)
            die(json_encode(array('result'=>false, 'message' => 'Chyba: Nepodařilo se uložit do DB a tabulky connection.')));
        else{    
            $this->ConnectionClientRequirement->id = $connection_id;
            $this->ConnectionClientRequirement->saveField('company_money_item_id',$company_money_item_id);        
            $this->ConnectionClientRequirement->saveField('company_work_position_id',$company_work_position_id);        
        }    
     }
    
    
	function uzavrit_odpracovane_hodiny($client_id = null, $company_id = null, $client_working_hour_id = null, $month = null, $year = null){
		if ($company_id == null || $client_working_hour_id == null){
			LogError('Controller: employees_controller; Function: uzavrit_odpracovane_hodiny; Chyba! Nejsou známé všechny parametry.');
			die(json_encode(array('result'=>false,'message'=>'Chyba! Nejsou známé všechny parametry.')));
		}
		
		$this->loadModel('ClientWorkingHour');
		$this->ClientWorkingHour->id = $client_working_hour_id;
		$this->ClientWorkingHour->saveField('stav',2);
		
		$this->ConnectionClientRequirement->bindModel(array(
			'belongsTo'	=>	array('Client','Company','RequirementsForRecruitment'),
			'hasOne'	=>	array('ClientWorkingHour'=>array(
				'conditions' => array(
					'ClientWorkingHour.year'=> $year,
					'ClientWorkingHour.month'=> $month
				)
			))
		));
		
		$conditions	= array(
			"((DATE_FORMAT(ConnectionClientRequirement.from,'%Y-%m')<= '$year-$month'))",
			"((DATE_FORMAT(ConnectionClientRequirement.to,'%Y-%m') >= '$year-$month') OR (ConnectionClientRequirement.to = '0000-00-00 00:00'))",
			'ConnectionClientRequirement.type'=> 2,
			'ClientWorkingHour.stav' => 1,
			'ClientWorkingHour.company_id' => $company_id
		);
		
		$count = $this->ConnectionClientRequirement->find('count', array('conditions'=>$conditions));
		
		if ($count == 0){
			$this->loadModel('Company');
			$company = $this->Company->read($this->Company->company_data_for_email_notification,$company_id);
			
			$termin = new DateTime();
			$termin->modify("+2 day");
			
			$task_to_save = array('CompanyTask'=>array(
				'cms_user_id' 	=> 	14, // Putorik Robert
				'name'			=> 	'Autorizace odpracovaných hodin',
				'termin'		=>	$termin->format("Y-m-d"),
				'company_id'	=>	$company_id,
				'setting_task_type_id' => 9,
				'creator_id'	=> 	$this->logged_user['CmsUser']['id'],
				'text'			=> 'Autorizujte odpracované hodiny u firmy '.$company['Company']['name'].'.'
			));
			$this->loadModel('CompanyTask');
			$this->CompanyTask->save($task_to_save);

			//vytvoreni notifikacniho emailu pro CM,KOO,SM dane firmy a DIR = id->14 
			

			$replace_list = array(
					'##Company.name##' 	=>$company['Company']['name'],
					'##Rok##' 	=>$year,
					'##Mesic##' 	=>$month
			);
            $this->Email->set_company_data($company['Company']);
			$this->Email->send_from_template_new(10,array(),$replace_list);
			
			//vytoreni connection  firmy a jejiho stavu pro dany rok a mesic
			$conditions	= array(
				'ConnectionCompanyToAuthorization.year' => $year,
				'ConnectionCompanyToAuthorization.month' => $month,
				'ConnectionCompanyToAuthorization.company_id' => $company_id
			);
			$this->loadModel('ConnectionCompanyToAuthorization');
			$finCon = $this->ConnectionCompanyToAuthorization->find('all', array(
				'conditions'=>$conditions,
				'fields'=>array('id')
			));
			if(!$finCon){ // jedna se o nove connection,pro dany mesic a rok
				$to_save = array('ConnectionCompanyToAuthorization'=>array(
					'company_id'	=>	$company_id,
					'year'			=> 	$year,
					'month'		=>	$month,
					'closed_user'	=>	$this->logged_user['CmsUser']['id'], // at vime kdo uzavrel odpracovane hodiny
					'closed' => date("Y-m-d H:i:s")
				));
				$this->ConnectionCompanyToAuthorization->save($to_save);
			}
			else { // connection je jiz vytvoreno, pouze obnov stav firmy, pro dany mesic a rok
				$this->ConnectionCompanyToAuthorization->id = $finCon[0]['ConnectionCompanyToAuthorization']['id'];
				$this->ConnectionCompanyToAuthorization->saveField('stav',1);
				$this->ConnectionCompanyToAuthorization->saveField('closed',date("Y-m-d H:i:s"));
				$this->ConnectionCompanyToAuthorization->saveField('closed_user',$this->logged_user['CmsUser']['id']);
			}

		}
		
		die(json_encode(array('result'=>true,'pocet'=>$count)));
	}
	
	/**
 	* Returns a JSON list of calculation for "profese"
 	*
	* @param $company_work_position_id (INT)
	* @param $year (DATE(Y))
	* @param $month DATE(m))
	* @param $json Boolean
 	* @return JSON
 	* @access public
	**/
	//moemntalne se nepouziva prootze byli zmeneny load profesi!
	public function load_calculation_for_prefese($company_work_position_id = '', $year = null, $month = null, $json = true){
		if ($company_work_position_id == null || $year == null || $month == null) 
			die(json_encode(array('result'=>false,'message'=>'Nelze zvolit prázdnou profesi.')));
		
		
		// load model of validity and calculation
		$this->loadModel('CompanyMoneyValidity');
		$this->CompanyMoneyValidity->bindModel(array('hasMany'=>array('CompanyMoneyItem'=>array(
            'fields'=>array(
                'id','name','vypocitana_celkova_cista_maximalni_mzda_na_hodinu',
                'cista_mzda_z_pracovni_smlouvy_na_hodinu','cista_mzda_faktura_zivnostnika_na_hodinu',
                'cista_mzda_dohoda_na_hodinu','cista_mzda_cash_na_hodinu','stravenka',
                'fakturacni_sazba_na_hodinu','normo_hodina','doprava','cena_ubytovani_na_mesic',
                'odmena_max_1','odmena_max_2','pausal','ps_pausal_hmm',
                'vlozeny_prispevek_na_stravne','vlozeny_prispevek_na_cestovne',
                'h1000','ps_hodina_hmm_monthly','ps_hodina_hmm',
                'ps_hodina_odmena_1','ps_hodina_max_1','ps_hodina_max_1p','ps_hodina_odmena_1_type',
                'ps_hodina_odmena_2','ps_hodina_max_2','ps_hodina_max_2p','ps_hodina_odmena_2_type',
                'fa_dpc'
                
            )
        ))));

		$money_items = $this->CompanyMoneyValidity->find(
				'all', 
				array(
					'conditions'=>array(
						'company_work_position_id' => $company_work_position_id,
                        "((DATE_FORMAT(CompanyMoneyValidity.platnost_od,'%Y-%m')<= '".$year."-".$month."'))",
						"((DATE_FORMAT(CompanyMoneyValidity.platnost_do,'%Y-%m')>= '".$year."-".$month."') OR (DATE_FORMAT(CompanyMoneyValidity.platnost_do,'%Y-%m') = '0000-00'))",
                           
                        //'MONTH(platnost_od) <=' => $month,
						//'YEAR(platnost_od) <=' => $year,
					//	array('AND'=> array('OR' => array(
                             //'MONTH(platnost_do) >=' => $month,
							//'MONTH(platnost_do)' => '00'
					//	))),
					//	array('AND'=> array('OR' => array(
					//		'YEAR(platnost_do) >=' => $year,
					//		'YEAR(platnost_do)' => '0000'
					//	)))
					)
				)
			);
		if ($money_items == false){
			if ($json == true)
				die(json_encode(array('result'=>false,'message'=>"Nebyla nalezena kalkulace s odpovídající platností pro $year/$month.")));
			else
				return false;
		} else {
			if (count($money_items) > 1){
				if ($json == true)
					die(json_encode(array('result'=>false,'message'=>"Pro tuto profesi byly naleznuty 2 nebo více odpovidajících platností pro $year/$month. Pravděpodobně se jedná o chybu v zadání nové platnosti fakturační sazby.")));
				else
					return false;
			} else {
				if ($json == true)
					die(json_encode(array('result'=>true,'data'=>$money_items[0]['CompanyMoneyItem'])));
				else
					return $money_items[0]['CompanyMoneyItem'];
			}
		}
		if ($json == true)
			die(json_encode(array('result'=>false,'message'=>"Chyba: Sem to vubec nemelo projit. Neco je spatne s podminkou.")));
		else
			return false;
	}
	
	
	public function load_calculation_for_prefese_second($company_work_position_id = null, $json = true){
		if ($company_work_position_id == null) 
			die(json_encode(array('result'=>false,'message'=>'Nelze zvolit prázdnou profesi.')));
	
		// load model of validity and calculation
		$this->loadModel('CompanyMoneyValidity');
		$this->CompanyMoneyValidity->bindModel(array('hasMany'=>array('CompanyMoneyItem'=>array('fields'=>array('id','name','vypocitana_celkova_cista_maximalni_mzda_na_hodinu','cista_mzda_z_pracovni_smlouvy_na_hodinu','cista_mzda_faktura_zivnostnika_na_hodinu','cista_mzda_dohoda_na_hodinu','cista_mzda_cash_na_hodinu','stravenka','fakturacni_sazba_na_hodinu','doprava','cena_ubytovani_na_mesic','odmena_max_1','odmena_max_2','normo_hodina')))));
		$money_items = $this->CompanyMoneyValidity->find(
				'all', 
				array(
					'conditions'=>array(
						'company_work_position_id' => $company_work_position_id,
						'platnost_od !='=>'0000-00-00', 
						'platnost_do '=>'0000-00-00'
					)
				)
			);
		if ($money_items == false){
			if ($json == true)
				die(json_encode(array('result'=>false,'message'=>"Nebyla nalezena kalkulace s odpovídající platností.")));
			else
				return false;
		} else {
			if (count($money_items) > 1){
				if ($json == true)
					die(json_encode(array('result'=>false,'message'=>"Pro tuto profesi byly naleznuty 2 nebo více odpovidajících platností pro $year/$month. Pravděpodobně se jedná o chybu v zadání nové platnosti fakturační sazby.")));
				else
					return false;
			} else {
				if ($json == true)
					die(json_encode(array('result'=>true,'data'=>$money_items[0]['CompanyMoneyItem'])));
				else
					return $money_items[0]['CompanyMoneyItem'];
			}
		}
		if ($json == true)
			die(json_encode(array('result'=>false,'message'=>"Chyba: Sem to vubec nemelo projit. Neco je spatne s podminkou.")));
		else
			return false;
	}
	
	
	
	function load_client($kvalifikace_id=null, $company_id = null, $requirement_id = null){
		if ($kvalifikace_id == null){
			die ('Prosím vyberte kvalifikaci');
		}
		$this->loadModel('ConnectionClientCareerItem');
		$this->ConnectionClientCareerItem->bindModel(array('belongsTo'=>array('Client')));
		$this->set('client_list',$this->ConnectionClientCareerItem->find('list',array('conditions'=>array('ConnectionClientCareerItem.setting_career_item_id'=>$kvalifikace_id),'fields'=>array('Client.id','Client.name'),'recursive'=>2,'order'=>'Client.name')));
		unset($this->ConnectionClientCareerItem);
		
		$this->set('requirement_id', $requirement_id);
		$this->set('company_id', $company_id);
		
		$this->render('nabor_pozice_client_list');
	}
	
    
    /**
     * @name ucetni dokumentace
     * @abstract na odpracovane hodiny id
     * @author Sol
     * @since 2.11.2009
     */
    function ucetni_dokumentace ($connection_client_requirement_id = null, $forma = null){
    //Configure::write('debug',1);
        if(empty($this->data)){
            if($connection_client_requirement_id == null)
                die('Chyba špatné spojení mezi klientem a náborem!!!');
            
            $this->loadModel('ClientUcetniDokumentace');
            $this->data = $this->ClientUcetniDokumentace->find('first',array(
                'conditions'=>array(
                    'connection_client_requirement_id'=>$connection_client_requirement_id,
                    'forma'=>$forma,
                    'kos'=>0
                )
            ));

   
            if(empty($this->data)){
               $this->data['ClientUcetniDokumentace'] = array(
                    'connection_client_requirement_id'=>$connection_client_requirement_id,
                    'forma'=>$forma,
               );
            }

            
           $this->set('forma',$forma);
            
            /**
             * nastavenií prav zobrazeni
             * nyni editovat pouze ucetni a admin
             */
             
             $only_show = false;
             if(!in_array($this->logged_user['CmsGroup']['id'],array(1,6,7)))
                $only_show = true;
                
             $this->set('only_show',$only_show);   
             
            //render
            $this->render('ucetni_dokumentace');
        }
        else {
            if($this->data['ClientUcetniDokumentace']['id'] == null)
                $this->data['ClientUcetniDokumentace']['cms_user_id'] = $this->logged_user['CmsUser']['id'];
                
            $this->loadModel('ClientUcetniDokumentace');    
            $this->ClientUcetniDokumentace->save($this->data);    
            
        }

    }
	
	
	//zobrazeni karty klienta
	function client_info($id = null){
		echo $this->requestAction('clients/edit/'.$id.'/domwin/only_show');
		die();
	}
    
    /**
     * funkce ktera nam bude vracet nove mozne company_money_item_id
     * @author Sol
     * @since 11.11.09
     */
    public function load_new_posssible_money_item_id($client_connection_requirement = null){
		if ($client_connection_requirement == null) 
			die(json_encode(array('result'=>false,'message'=>'Nelze zvolit prázdnou profesi.')));
	
        $this->ConnectionClientRequirement->bindModel(array(
            'belongsTo'=>array(
                'CompanyMoneyItem'=>array(
                    'fields'=>array('id','name','doprava','cena_ubytovani_na_mesic')
                )
            )
        ));
	    $cmi_id = $this->ConnectionClientRequirement->read(
            array(
                'CompanyMoneyItem.id',
                'CompanyMoneyItem.name',
                'CompanyMoneyItem.doprava',
                'CompanyMoneyItem.pausal',
                'CompanyMoneyItem.cena_ubytovani_na_mesic',
                'company_money_item_id',
                'company_work_position_id'
            ),$client_connection_requirement);
        
        
        if(!$cmi_id)
            die(json_encode(array('result'=>false,'message'=>'V dochzáce nenalezena žádna forma odměny, kontaktujte administrátora.')));
        else{
            
            $forma_odmeny_name = $cmi_id['CompanyMoneyItem']['name'];
            $ubytovani = ($cmi_id['CompanyMoneyItem']['cena_ubytovani_na_mesic'] > 0 ? 1 : 0);
            $doprava = ($cmi_id['CompanyMoneyItem']['doprava'] > 0 ? 1 : 0);
            $pausal = (($cmi_id['CompanyMoneyItem']['pausal'] == 1) ? 1 : 0);
           
            $this->loadModel('CompanyMoneyItem');
            $search_unique = $this->CompanyMoneyItem->query('
                 SELECT id,name,IF(doprava > 0,1,0) as dop, IF(cena_ubytovani_na_mesic> 0,1,0) as ub 
                    FROM `wapis__company_money_items` AS `CompanyMoneyItem`
                 WHERE `company_work_position_id` = '.$cmi_id['ConnectionClientRequirement']['company_work_position_id'].'
                        AND `name` = '.$forma_odmeny_name.' and pausal='.$pausal.' AND `kos` = 0 AND id != '.$cmi_id['CompanyMoneyItem']['id'].'
                 HAVING dop = '.$doprava.' AND ub= '.$ubytovani.' ORDER BY id DESC LIMIT 1   
	       '); 
           
            if(count($search_unique) == 1)
               die(json_encode(array('result'=>true,'id'=>$search_unique[0]['CompanyMoneyItem']['id'])));
            else   
               die(json_encode(array('result'=>false,'message'=>'Nenalezena žádna možná nová forma odměny.')));
        }
		die(json_encode(array('result'=>false,'message'=>"Chyba: Sem to vubec nemelo projit. Neco je spatne s podminkou.")));
	}
    
    /**
     * funkce pro zobrazeni domwinu a posleze
     */
     function add_down_payments($type = 0){
         if(!isset($this->data))
            $this->data = $_POST; 
         
         if($this->data['save'] == 0){
            
             /**
              * Druha podminka pro vydani zaloh
              * maximalni limit pro zalohu je 3000 Kc nebo 100€ podle statu
              * s vyjimkou u ktere je zachovan soucasny maximalni limit: pocet hodin x hodinova sazba 
              * Tyto firmy jsou pridavany v Zadosti o zalohy
              * 
              * UPDATE 9.9.2010
              * limit se prenasi z nastaveni firmy
               */
              //$limit = ($this->data['stat_id']==1 ? 3000 : 100);
              
              $exception = false;
              $this->loadModel('CompanyExceptionPayment');
              $exception_company = $this->CompanyExceptionPayment->find('first',array(
                'conditions'=>array(
                    'kos'=>0,
                    'company_id'=>$this->data['company_id']
                )
              ));  
            
              if($exception_company){      
                $exception = true;  
              }
              $this->data['exception'] = $exception;
              
              
              /**
               * Limit berem z nastaveni firmy
               */
               $this->loadModel('Company');
               $comp = $this->Company->read(array('payment_cash_limit','payment_amount_limit'),$this->data['company_id']);
               $this->data['limit'] = ($type == 0 ? $comp['Company']['payment_amount_limit'] : $comp['Company']['payment_cash_limit']);  
               unset($this->Company); 
              //END druha podminka
            
              $this->data['ReportDownPayment'] = array(
                  'year' =>$this->data['year'],
                  'month' =>$this->data['month'],
                  'client_id' =>$this->data['client_id'],
                  'company_id' =>$this->data['company_id'],
                  'celkem_hodin' =>$this->data['celkem_hodin'],
                  'connection_client_requirement_id' =>$this->data['connection_client_requirement_id'],
                  'cms_user_id' =>$this->logged_user['CmsUser']['id']
              );
              
              $this->set('type',$type);

              //render              
              $this->render('add_down_payments');
         }
         else {
            $this->loadModel('ReportDownPayment');
            
            
            /**
             * Novinka od 17.3.10
             * UPRAVA od 13.3.12 - pouze od 1-9 den do predchazejiciho
             * pokud jsme do 15dne a klient ma v predchozim mesici dochazku
             * ulozime zalohu k predeslemu mesici
             */
            /* if(self::is_in_interval(date('d'),1,9)){
                $prev_month_date = new DateTime();
                $prev_month_date->setDate($this->data['ReportDownPayment']['year'], $this->data['ReportDownPayment']['month'], 01);
                $prev_month_date->modify("-1 month");

                $this->loadModel('ClientWorkingHour');
                $previous_client_working = $this->ClientWorkingHour->find('first',array(
                    'conditions'=>array(
                        'connection_client_requirement_id'=>$this->data['ReportDownPayment']['connection_client_requirement_id'],
                        'year'=> $prev_month_date->format('Y'),
                        'month'=> $prev_month_date->format('m')
                    )
                ));
                
                if($previous_client_working){
                    $this->data['ReportDownPayment']['year'] = $prev_month_date->format('Y');
                    $this->data['ReportDownPayment']['month'] = $prev_month_date->format('m');
                }
                
             } */
            
            
            /**
             * Podminka c.3
             * zalohy jednou za 4 dny
             * vyjimka opet tuto podminku rusi
             */
          /*  $have_payment = $this->ReportDownPayment->find('first',array(
                'conditions'=>array(
                    'kos'=>0,
                    'client_id'=>$this->data['ReportDownPayment']['client_id'],
                    'company_id'=>$this->data['ReportDownPayment']['company_id'],
                    'connection_client_requirement_id'=>$this->data['ReportDownPayment']['connection_client_requirement_id'],
                    'DATE_FORMAT(created,"%Y-%m-%d") > DATE_SUB(DATE_FORMAT(NOW(),"%Y-%m-%d"), INTERVAL 4 DAY)'
                ),
                'order'=>'id DESC'
             ));
             //pr($have_payment);
             
             if($have_payment && $this->data['exception'] != 1){
                die(json_encode(array('result'=>false,'message'=>'Záloha muže být zadána pouze jednou za 4 dny.')));  
             }
             else{  */
                 if($this->ReportDownPayment->save($this->data))
                	die(json_encode(array('result'=>true)));
    	         else
            		die(json_encode(array('result'=>false,'message'=>'Chyba během ukládání zálohy.')));  
            // }
         }
     }
     
     
     /**
      * funkce napsana pro zjisteni cisla zda spada do intervalu
      */
     private function is_in_interval($value, $start, $end){
        if($value >= $start && $value <= $end)
            return true;
        else
            return false;    
     }
     
     
    /**
     * nova moznost editace connection from / to datumu 
     */   
	function modify_from_date($connection_id = null,$client_id = null){
		if(empty($this->data)){
		    $this->set('client_id',$client_id);
          
		    $this->loadModel('ConnectionClientRequirement');  
            
            /**
             * nacteni seznamu connection, ostatnich zamestanni
             * aby pri editaci byl prehled kde a kdy pracoval..
             */
             $this->ConnectionClientRequirement->bindModel(array(
                'belongsTo'=>array('Company')
             )
             );
            $conection_list = $this->ConnectionClientRequirement->find('all',array(
                'conditions'=>array(
                    'client_id'=>$client_id,
                    'type'=>2,
                    'ConnectionClientRequirement.id !='=>$connection_id
                )
            ));
            $this->set('conection_list',$conection_list);
          
            
            $this->data = $this->ConnectionClientRequirement->read(array('id','from','to'),$connection_id);
            if(empty($this->data) || $this->data == null)
                die('Takové zaměstnání nebylo nalezeno');
		}
        else{
            if(empty($this->data['ConnectionClientRequirement']['from']) && $this->data['ConnectionClientRequirement']['id'] != '')
                die(json_encode(array('result'=>false)));  
            
            $this->loadModel('ConnectionClientRequirement');
            if($this->ConnectionClientRequirement->save($this->data)){  
                unset($this->ConnectionClientRequirement);
                die(json_encode(array('result'=>true)));
            }
            else
                die(json_encode(array('result'=>false)));    
        }
	}
    
    /**
     * nacteni detailu sablony pro dochazku
     */
    function load_cw_template_item($template_item_id = null){
        if($template_item_id == null)
             die(json_encode(array('result'=>false))); 
 
        $this->loadModel('CompanyClientWorkingTemplateItem');
        $days = $this->CompanyClientWorkingTemplateItem->read('days',$template_item_id);
        die(json_encode(array('result'=>true,'data'=>$days['CompanyClientWorkingTemplateItem']['days'])));
    }
    
    
    function transfer_comp_to_comp(){
        Configure::write('debug',1);
        die('neaktivní');
        /* trnsfer zamestanni*/
        $this->loadModel('ConnectionClientRequirement');
        $this->ConnectionClientRequirement->debug =  true;
        $cons = $this->ConnectionClientRequirement->find('all',array(
            'conditions'=>array(
                'company_id'=>2278,
                'from >'=>"2011-07-01",
                'type'=>2
            )
        ));
        echo count($cons);
        foreach($cons as $conn){
            //ukonceni stareho
            $connection_new = $this->ConnectionClientRequirement->find('first',array(
                'conditions'=>array(
                    'company_id'=>'2388',
                    'client_id'=>$conn['ConnectionClientRequirement']['client_id']
                )
            ));
            if($connection_new){
                $this->ConnectionClientRequirement->id = $connection_new['ConnectionClientRequirement']['id'];
                $this->ConnectionClientRequirement->saveField('from',$conn['ConnectionClientRequirement']['from']);
            }
        }
        
        /* transfer dochazek 
        $this->loadModel('ClientWorkingHour');
        $dochazky = $this->ClientWorkingHour->find('all',array(
            'conditions'=>array(
                'company_id'=>2278,
                'year'=>'2011',
                'month'=>'07',
                //'type'=>2
            )
        ));
        echo count($dochazky);
        //pr($dochazky);
        foreach($dochazky as $conn){
            $save = $conn;
            $connection_new = $this->ConnectionClientRequirement->find('first',array(
                'conditions'=>array(
                    'company_id'=>'2388',
                    'client_id'=>$save['ClientWorkingHour']['client_id']
                )
            ));
            //nova connection pro firmu lowipro
            $save['ClientWorkingHour']['company_id'] = 2388;
            $save['ClientWorkingHour']['connection_client_requirement_id'] = $connection_new['ConnectionClientRequirement']['id'];
            $save['ClientWorkingHour']['requirements_for_recruitment_id'] = 2469;
            $save['ClientWorkingHour']['company_money_item_id'] = 3791;
            $save['ClientWorkingHour']['company_work_position_id'] = 1233;
            $this->ClientWorkingHour->id = $save['ClientWorkingHour']['id'];
            $this->ClientWorkingHour->save($save);
        }
        */
        
        die();
    }
    
    
    function new_dohoda_recount(){
        die('vypnuto');
        Configure::write('debug',1);
        $this->loadModel('Setting');
        $setting_def= array();
        $dohoda_limit_cz = $dohoda_limit_sk = 0;
		$setting_def = $this->Setting->read(null,3);
        //limit pro 0101 a 0100
        if(isset($setting_def['Setting']['value']['dohoda_limit']) && $setting_def['Setting']['value']['dohoda_limit'] > 0)
            $dohoda_limit_cz = $setting_def['Setting']['value']['dohoda_limit'];
            
        $setting_def = $this->Setting->read(null,4);
        //limit pro 0101 a 0100
        if(isset($setting_def['Setting']['value']['dohoda_limit']) && $setting_def['Setting']['value']['dohoda_limit'] > 0)
            $dohoda_limit_sk = $setting_def['Setting']['value']['dohoda_limit'];     
  
        /* trnsfer zamestanni*/
        $this->loadModel('ClientWorkingHour');
        //$this->ConnectionClientRequirement->debug =  true;
        $this->ClientWorkingHour->bindModel(array('belongsTo'=>array('Client','Company','CompanyMoneyItem')));
        $cons = $this->ClientWorkingHour->find('all',array(
            'conditions'=>array(
                'year'=>'2012',
                'celkem_hodin > '=>0,
                'salary_per_hour > '=>0,
                'CompanyMoneyItem.name'=>array('0100','0101')
            )
        ));
        echo count($cons);
  
        foreach($cons as $conn){
            $dohoda_limit = ($conn['Company']['stat_id'] == 1?$dohoda_limit_cz:$dohoda_limit_sk);
            
            if($dohoda_limit > 0){
                $pocet_hodin = $conn['ClientWorkingHour']['celkem_hodin'];
        		
        		$salary_part_1 = $conn['ClientWorkingHour']['salary_part_1'];
        		$salary_part_2 = $conn['ClientWorkingHour']['salary_part_2'];
        		$salary_part_3 = $conn['ClientWorkingHour']['salary_part_3'];
        		$salary_part_4 = $conn['ClientWorkingHour']['salary_part_4'];
        		
        		$drawback = $conn['ClientWorkingHour']['drawback'];
        
                if($conn['ClientWorkingHour']['drawback_accommodation_add']){
                    $drawback_accommodation_add = $conn['ClientWorkingHour']['drawback_accommodation_add'];
                    $drawback_accommodation = ($drawback_accommodation_add == true && $conn['ClientWorkingHour']['drawback_accommodation'] < 0 ? $conn['ClientWorkingHour']['drawback_accommodation'] : 0);
                }
                else{
                    $drawback_accommodation_add = $drawback_accommodation = 0;
                }
                
                if($conn['ClientWorkingHour']['drawback_opp_add']){
                    $drawback_opp_add = $conn['ClientWorkingHour']['drawback_opp_add'];
                    $drawback_opp = ($drawback_opp_add == true ? $conn['ClientWorkingHour']['drawback_opp'] : 0);
                }
                else{
                    $drawback_opp_add = $drawback_opp = 0;
                }
                
        		$odmena_1 = $conn['ClientWorkingHour']['odmena_1'];
        		$odmena_2 = $conn['ClientWorkingHour']['odmena_2'];
        
        		$odmeny = $odmena_1 + $odmena_2;
                $srazky = 0-$drawback-$drawback_opp+$drawback_accommodation;
        
                $s1 = $pocet_hodin*$conn['ClientWorkingHour']['salary_part_1'];
        		$s2 = $pocet_hodin*$conn['ClientWorkingHour']['salary_part_2'];
                $s3 = $pocet_hodin*$conn['ClientWorkingHour']['salary_part_3'];
        		$s4 = $pocet_hodin*$conn['ClientWorkingHour']['salary_part_4'];
                
                $celkem_mzda = (($s1 + $s2 + $s3 + $s4)+$srazky+$odmeny);
                
                if($celkem_mzda <= $dohoda_limit){
                    /**
                     * abc se vyplaci do limitu pouze ve forme dohody
                     * vse nad limit pak jde do abc
                     */
                    $to_save['ClientWorkingHour']['salary_part_2_p'] = $celkem_mzda;
                    $to_save['ClientWorkingHour']['salary_part_4_p'] = 0;
                }
                else{
                    $five_percent = ($dohoda_limit/100)*5;
                    //5% castka a ted randomem od 1-5% odečtem e z dohody a hodime to do abc at neni dohoda porad 9500
                    $random_number = rand(1,$five_percent);
                    $random_number = ($conn['Company']['stat_id'] == 1 ?round($random_number):round($random_number,2));

                    $to_save['ClientWorkingHour']['salary_part_4_p'] = ($celkem_mzda) - $dohoda_limit+$random_number;
                    $to_save['ClientWorkingHour']['salary_part_2_p'] = $dohoda_limit-$random_number;
                }
                
                $this->ClientWorkingHour->id = $conn['ClientWorkingHour']['id'];
                $this->ClientWorkingHour->save($to_save);   
            }
        }
        
        /* transfer dochazek 
        $this->loadModel('ClientWorkingHour');
        $dochazky = $this->ClientWorkingHour->find('all',array(
            'conditions'=>array(
                'company_id'=>2278,
                'year'=>'2011',
                'month'=>'07',
                //'type'=>2
            )
        ));
        echo count($dochazky);
        //pr($dochazky);
        foreach($dochazky as $conn){
            $save = $conn;
            $connection_new = $this->ConnectionClientRequirement->find('first',array(
                'conditions'=>array(
                    'company_id'=>'2388',
                    'client_id'=>$save['ClientWorkingHour']['client_id']
                )
            ));
            //nova connection pro firmu lowipro
            $save['ClientWorkingHour']['company_id'] = 2388;
            $save['ClientWorkingHour']['connection_client_requirement_id'] = $connection_new['ConnectionClientRequirement']['id'];
            $save['ClientWorkingHour']['requirements_for_recruitment_id'] = 2469;
            $save['ClientWorkingHour']['company_money_item_id'] = 3791;
            $save['ClientWorkingHour']['company_work_position_id'] = 1233;
            $this->ClientWorkingHour->id = $save['ClientWorkingHour']['id'];
            $this->ClientWorkingHour->save($save);
        }
        */
        
        die();
    }
    
    
    function new_dpc_recount(){
        die('vypnuto');
        Configure::write('debug',1);
        $rozmezi = 250;
        $max = 2500;       
        
        $this->loadModel('ClientWorkingHour');
        $this->ClientWorkingHour->bindModel(array('belongsTo'=>array('Client','Company','CompanyMoneyItem','ConnectionClientRequirement')));
        $cons = $this->ClientWorkingHour->find('all',array(
            'conditions'=>array(
                'Company.stat_id'=>1,
                'ClientWorkingHour.year'=>'2012',
                'ClientWorkingHour.month'=>'02',
                "((DATE_FORMAT(ConnectionClientRequirement.from,'%Y-%m')<= '2012-02'))",
    			'((DATE_FORMAT(ConnectionClientRequirement.to,"%Y-%m") >= "2012-02") OR (ConnectionClientRequirement.to = "0000-00-00 00:00"))',
    			'ConnectionClientRequirement.type'=> 2,
                'ConnectionClientRequirement.new'=> 0,
                '(ClientWorkingHour.fa_dpc = 0 OR ClientWorkingHour.fa_dpc)',
                'celkem_hodin > '=>0,
                'salary_per_hour > '=>0,
                'CompanyMoneyItem.name'=>array('0010')
            )
        ));
        echo count($cons);

        foreach($cons as $conn){
                $pocet_hodin = $conn['ClientWorkingHour']['celkem_hodin'];
        		
        		$salary_part_1 = $conn['ClientWorkingHour']['salary_part_1'];
        		$salary_part_2 = $conn['ClientWorkingHour']['salary_part_2'];
        		$salary_part_3 = $conn['ClientWorkingHour']['salary_part_3'];
        		$salary_part_4 = $conn['ClientWorkingHour']['salary_part_4'];
                
        		$odmena_1 = $conn['ClientWorkingHour']['odmena_1'];
        		$odmena_2 = $conn['ClientWorkingHour']['odmena_2'];
                
                $drawback = $conn['ClientWorkingHour']['drawback'];
        
                if($conn['ClientWorkingHour']['drawback_accommodation_add']){
                    $drawback_accommodation_add = $conn['ClientWorkingHour']['drawback_accommodation_add'];
                    $drawback_accommodation = ($drawback_accommodation_add == true && $conn['ClientWorkingHour']['drawback_accommodation'] < 0 ? $conn['ClientWorkingHour']['drawback_accommodation'] : 0);
                }
                else{
                    $drawback_accommodation_add = $drawback_accommodation = 0;
                }
                
                if($conn['ClientWorkingHour']['drawback_opp_add']){
                    $drawback_opp_add = $conn['ClientWorkingHour']['drawback_opp_add'];
                    $drawback_opp = ($drawback_opp_add == true ? $conn['ClientWorkingHour']['drawback_opp'] : 0);
                }
                else{
                    $drawback_opp_add = $drawback_opp = 0;
                }
        
        		$odmeny = $odmena_1 + $odmena_2;
                $srazky = 0-$drawback-$drawback_opp+$drawback_accommodation;
        
                $s1 = $pocet_hodin*$conn['ClientWorkingHour']['salary_part_1'];
        		$s2 = $pocet_hodin*$conn['ClientWorkingHour']['salary_part_2'];
                $s3 = $pocet_hodin*$conn['ClientWorkingHour']['salary_part_3'];
        		$s4 = $pocet_hodin*$conn['ClientWorkingHour']['salary_part_4'];
                
              
                //1-250
                $random_number = round(rand(1,$rozmezi));
                $dpc = $conn['CompanyMoneyItem']['fa_dpc'];
                if($dpc < 1){
                  $dpc = 100;    
                }
                $dpc_value = $dpc*15;
                
                if($dpc_value > $max){$dpc_value = $max;}
                
                if($s3 > 0){
                    $s3 = $s3 - ($dpc_value - $random_number);
                }
                else{
                    continue;
                }
               
                //pr($conn['ClientWorkingHour']);
                //pr($conn['ClientWorkingHour']['salary_part_3_p']);
                //pr($conn['ClientWorkingHour']['salary_per_hour_p']);
                    
                $celkem_mzda = (($s1 + $s2 + $s3 + $s4)+$srazky+$odmeny);
                  
                $to_save['ClientWorkingHour']['fa_dpc'] = $dpc;    
                $to_save['ClientWorkingHour']['fa_dpc_calculation'] = ($dpc_value - $random_number);  
                $to_save['ClientWorkingHour']['salary_part_3_p'] = round($s3);
                $to_save['ClientWorkingHour']['salary_per_hour_p'] = round($celkem_mzda);
                //pr($conn['Client']['name']);
                //pr($to_save);
                //echo round($to_save['ClientWorkingHour']['fa_dpc_value']+$to_save['ClientWorkingHour']['salary_part_3_p']);
                
                //die();
                $this->ClientWorkingHour->id = $conn['ClientWorkingHour']['id'];
                $this->ClientWorkingHour->save($to_save);   

        }
        
        die('done');
    }
}
?>