<?php

$_GET['start_year'] 	= (isset($_GET['start_year']) && !empty($_GET['start_year']))?$_GET['start_year']:(int)date('Y');
$_GET['end_year'] 	= (isset($_GET['end_year']) && !empty($_GET['end_year']))?$_GET['end_year']:(int)date('Y');
@define('START_YEAR', $_GET['start_year']); 
@define('END_YEAR', $_GET['end_year']); 

$_GET['start_month'] 	= (isset($_GET['start_month']))?$_GET['start_month']:(int)date('m');
$_GET['end_month'] 	= (isset($_GET['end_month']))?$_GET['end_month']:(int)date('m');
@define('START_MONTH', ($_GET['start_month'] < 10)?'0'.$_GET['start_month']:$_GET['start_month']); 
@define('END_MONTH', ($_GET['end_month'] < 10)?'0'.$_GET['end_month']:$_GET['end_month']); 

Configure::write('debug',1);
class ReportWorkingSummary2Controller extends AppController {
    var $name = 'ReportWorkingSummary2';
    var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput','Excel');
    var $components = array('ViewIndex','RequestHandler','Upload','Email');
    var $uses = array('ConnectionClientRequirement');
    var $renderSetting = array(
        'SQLfields' => array(
            'ConnectionClientRequirement.id',
            'Client.name',
            'Client.id',
            'Company.name',
            'Company.id',
            'ConnectionClientRequirement.updated',
            'ConnectionClientRequirement.created',
            'ConnectionClientRequirement.from',
            'ConnectionClientRequirement.to',
            'ConnectionClientRequirement.to_when',
            'ConnectionClientRequirement.to_cms_user_id',
            'DATEDIFF(ConnectionClientRequirement.to_when,ConnectionClientRequirement.to) as rozdil_to'
        ),
        'bindModel' => array(
            'belongsTo' => array(
                'Company'=>array('foreignKey'=>'company_id'),
                'Client'=>array('foreignKey'=>'client_id')
            ),
        ),
        
        'controller'=> 'report_working_summary2',
        'page_caption'=>'Evidencia datumov vystup',
        'sortBy'=>'ConnectionClientRequirement.to.DESC',
        'top_action' => array(
            'excel'			=>	'Export|export_excel|Vyexportovat do excelu|index',                      
        ),
        'filtration' => array(
            'Client-name|'      => 'text|Klient|',
            'Company-id'	=> 'select|Společnost|company_list',
            'Rozdil-value'	=> 'text|Rozdíl|',
            'ConnectionClientRequirement-to'	=> 'date_f_t|Datum|',
        ),
        'SQLcondition' => array(
            'ConnectionClientRequirement.to != 0000-00-00',                       
        ),
        'items' => array(
            'id'		=>	'ID|ConnectionClientRequirement|id|text|',
            'client'		=>	'Klient|Client|name|text|',
            'company'	        =>	'Společnost|Company|name|text|',	
            'date_to'		=>	'Datum výstupu|ConnectionClientRequirement|to|date|',	
            'date_to_when'	=>	'Datum výstupu (Kdy)|ConnectionClientRequirement|to_when|date|',	
            'date_to_diff'	=>	'Rozdil|0|rozdil_to|text|',	
            'koo2'	        =>	'KOO|ConnectionClientRequirement|to_cms_user_id|var|user_list',	
            //'pozn2'	        =>	'Pozn|0|akce2|var|history_type_list'	
        ),
        'posibility' => array(
            //'show'		=>	'show|Detail|show',
        ),
        'domwin_setting' => array(
            'sizes' 		=> '[1000,1000]',
            'scrollbars'		=> true,
            'languages'	=> 'false'
        )
    );
    
    function beforeFilter(){
        parent::beforeFilter();
        
        if (isset($_GET['excel'])){
            $this->renderSetting['no_limit'] = true;
        }
        
        if (isset($this->params['url']['filtration_Rozdil-value']) && $this->params['url']['filtration_Rozdil-value'] != ''){
            $this->renderSetting['SQLcondition'][] = 'DATEDIFF(ConnectionClientRequirement.to_when,ConnectionClientRequirement.to) >= '.$this->params['url']['filtration_Rozdil-value'];
            unset($this->params['url']['filtration_Rozdil-value']);
        }
        
    }
    
    function export_excel(){
		$link = '/'.$this->renderSetting['controller'].'/?';
		$sub = array();
		foreach($this->params['url'] as $key=>$get){
			if ($key != 'url'){
				$sub[] = "{$key}={$get}";
			}
		}
		$link .= implode('&',$sub) . '&excel=true';
		$this->redirect($link);
		exit;
	}
    
    function index(){
        $this->set('company_list',$this->get_list('Company'));
        /**
         * Natahnuti seznamu typu akci historie
         */
        $this->set('history_type_list',$this->get_list('HistoryType'));
        /**
         * Natahnuti seznamu uzivatelu pro doplneni informaci o koo z historie,
         * je mozne posleze omezit na pouzite users z history
         */
        $this->set('user_list',$this->get_list('CmsUser'));
        
                

        if (isset($_GET['excel'])){
			$this->autoLayout = false;
			echo $this->render('../system/excel');
			die();
		}
        
        if ($this->RequestHandler->isAjax()){
            $this->render('../system/items');
        } else {
            $this->render('../system/index');
        }
    }   
    
}
?>