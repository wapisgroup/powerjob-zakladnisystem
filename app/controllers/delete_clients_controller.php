<?php
//Configure::write('debug',1);
class DeleteClientsController extends AppController
{
  var $name = 'DeleteClients';
  var $helpers = array('htmlExt', 'Pagination', 'ViewIndex');
  var $components = array('ViewIndex', 'RequestHandler');
  var $uses = array('Client');
  var $renderSetting = array(
    'bindModel' => array(),
        //'SQLfields' => '*,GROUP_CONCAT(profese_name SEPARATOR "<br/>") as Profese ',
    'SQLfields' => '*', 
    'SQLcondition' => array(
        'kos'=>1
    ), 
    'controller' => 'delete_clients', 
    'page_caption' => 'Klienti - koš',
    //'count_condition' => '(SELECT COUNT(c.id) as pocet FROM wapis__clients as c Where c.name=ClientView.name GROUP BY c.name)  > 1',
    'sortBy' => 'Client.id.ASC', 
    'no_trash' => true, 
    'top_action' =>array( 
        // caption|url|description|permission
    ), 
    'filtration' => array(
        'Client-id|' => 'text|Klient ID|',
        'Client-name|' => 'text|Jméno|'
    ), 
    'items' => array(
        'id' => 'ID|Client|id|text|',
        'client_id' => 'ID|Client|id|hidden|',
        'name' => 'Jméno|Client|name|text|', 
        'updated' => 'Poslední úprava/datum smazání|Client|updated|datetime|', 
    ),     
    'posibility' => array(
        'refresh'       => 'refresh|Obnovit klienta|refresh',
        'client_info'	=> 'client_info|Karta klienta|client_info',
        'stats'         => 'stats|Statistiky spojení|stats' 
    ), 
    'domwin_setting' => array(
        'sizes' => '[1000,1000]', 
        'scrollbars' => true, 
        'languages' => true 
    )
  );
    

    function index()
    {
        $this->set('fastlinks', array('ATEP' => '/', 'Smazání klienti' => '#'));


        if ($this->RequestHandler->isAjax())
        {
            $this->render('../system/items');
        } else
        {
            $this->render('../system/index');
        }
    }
    
    
    /**
     * funkce ktera vraci klienta mezi aktivni
     * kos = 0
     * @created 9.12.09
     * @author Sol
     */
    function refresh($client_id){
        
        $this->Client->id = $client_id;
        $this->Client->saveField('kos',0);
        die();
    }
    
    
    //zobrazeni karty klienta
	function client_info($id = null){
		echo $this->requestAction('clients/edit/'.$id.'/domwin/only_show');
		die();
	}
    
    //statistiky spojeni klienta
	function stats($id = null){
		echo $this->requestAction('clients/stats/'.$id);
		die();
	}

}