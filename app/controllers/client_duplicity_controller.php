<?php
//Configure::write('debug',1);
class ClientDuplicityController extends AppController
{
  var $name = 'ClientDuplicity';
  var $helpers = array('htmlExt', 'Pagination', 'ViewIndex', 'FileInput');
  var $components = array('ViewIndex', 'RequestHandler', 'Upload', 'Email');
  var $uses = array('ClientView', 'Client');
  var $renderSetting = array(
    'bindModel' => array(   ),
        //'SQLfields' => '*,GROUP_CONCAT(profese_name SEPARATOR "<br/>") as Profese ',
    'SQLfields' => '*,COUNT(ClientView.id) as pocet,SUM(ClientView.kos) as smazanych', 
    'controller' => 'client_duplicity', 
    'page_caption' => 'Klienti',
    //'count_condition' => '(SELECT COUNT(c.id) as pocet FROM wapis__clients as c Where c.name=ClientView.name GROUP BY c.name)  > 1',
    'group_by' => 'ClientView.name HAVING pocet > 1', 
    'sortBy' => 'NULL.pocet.DESC', 
    'no_trash' => true, 
    'top_action' =>array( 
        // caption|url|description|permission
    ), 
    'filtration' => array(
        'ClientView-name|' => 'text|Jméno|'
    ), 
    'items' => array(
        'id' => 'ID|ClientView|id|text|',
        'name' => 'Jméno|ClientView|name|text|', 
        'pocet' => 'Počet|0|pocet|text|', 
        'smazanych' => 'Počet smazaných|0|smazanych|text|'
    ),     
    'posibility' => array(
        'edit' => 'edit|Editace položky|edit', 
       // 'delete' => 'trash|Odstranit položku|trash'
    ), 
    'domwin_setting' => array(
        'sizes' => '[1000,1000]', 
        'scrollbars' => true, 
        'languages' => true 
    )
    );
    

    function index()
    {
        $this->set('fastlinks', array('ATEP' => '/', 'Duplicitní klienti' => '#'));

        

        if ($this->RequestHandler->isAjax())
        {
            $this->render('../system/items');
        } else
        {
            $this->set('scripts', array('uploader/uploader'));
            $this->set('styles', array('../js/uploader/uploader'));
            $this->render('../system/index');
        }
    }
    
    function edit($id = null,$domwin = null, $show = null){
		echo $this->requestAction('clients/edit/'.$id.'/'.$domwin.'/'.$show,array('request_data'=>array(
            'duplicity_domwin'=>true
        )));
		die();
	}

}