<?php
Configure::write('debug',1);
class MoneyItemDrawbacksController extends AppController {
	var $name = 'MoneyItemDrawbacks';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('MoneyItemDrawback');
	var $renderSetting = array(
		'controller'=>'money_item_drawbacks',
		'SQLfields' => array('id','name','updated','created'),
		'page_caption'=>'Kalkulace - nastavení srážek',
		'sortBy'=>'MoneyItemDrawback.created.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat důvod|add',
		//	'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
		//	'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
		//	'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
		//	'SettingAccommodationType-status'	=>	'select|Stav|select_stav_zadosti',
		//	'SettingAccommodationType-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|MoneyItemDrawback|id|text|',
			'name'		=>	'Název|MoneyItemDrawback|name|text|',
			'updated'	=>	'Upraveno|MoneyItemDrawback|updated|datetime|',
			'created'	=>	'Vytvořeno|MoneyItemDrawback|created|datetime|'
		),
		'posibility' => array(
			'status'	=> 	'status|Změna stavu|status',
			'edit'		=>	'edit|Editace položky|edit',
			'delete'	=>	'trash|Do košiku|trash'			
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#',$this->renderSetting['page_caption']=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null)
				$this->data = $this->MoneyItemDrawback->read(null,$id);
                
                $this->set('connection_tools',$this->get_list('MoneyItemConnectionTool'));   
			$this->render('edit');
		} else {
			$this->MoneyItemDrawback->save($this->data);
			die();
		}
	}
}
?>