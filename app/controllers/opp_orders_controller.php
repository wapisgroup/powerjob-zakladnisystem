<?php
Configure::write('debug',1);
class OppOrdersController extends AppController {
	var $name = 'OppOrder';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload','Email');
	var $uses = array('OppOrder');
	var $renderSetting = array(
		'bindModel'	=> array('belongsTo'=>array('CmsUser')),
		'SQLfields' => '*',
        'controller'=> 'opp_orders',
		'page_caption'=>'Objednavky na OPP',
		'sortBy'=>'OppOrder.id.DESC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add'
		),
		'filtration' => array(
		
		),
		'items' => array(
			'id'		=>	'ID|OppOrder|id|text|',
			'user'		=>  'Vytvořil|CmsUser|name|text|',
			'created'	=>	'Vytvořeno|OppOrder|created|datetime|',
			'updated'	=>	'Změněno|OppOrder|updated|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
			'delete'	=>	'trash|Odstranit položku|delete'			
		),
        'domwin_setting'=>array(
            'sizes'=>'[900,900]'
        )
	);
	function index(){
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#',$this->renderSetting['page_caption']=>'#'));
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			$this->loadModel('OppItem');
			$type_list = $this->OppItem->find('list', array('conditions'=>array('kos'=>0,'type !='=>'-1'),'group'=>'type','fields'=>array('type','type')));
		
            $this->set('template_list',$this->get_list('OppTemplate'));
                
			if ($id != null){
				$this->OppOrder->bindModel(array('hasMany'=>array('OppOrderItem')));
				$this->data = $this->OppOrder->read(null,$id);
                
                $kos = true;
                //potvrzena objednavka nepotrebuje podminku na aktualni zbozi/historie
                if($this->data['OppOrder']['status'] == 1){
                    $kos = false;
                }
                
                if($this->data['OppOrder']['opp_template_id'] != ''){
                    $type_list = self::load_kind($this->data['OppOrder']['opp_template_id'],true);
                }
				
				$names = array();
				foreach(Set::extract('/OppOrderItem/type_id',$this->data) as $t){
					if($this->data['OppOrder']['opp_template_id'] != '')
                        $names[$t] = self::load_typ2($this->data['OppOrder']['opp_template_id'],$t,true);
                    else
                        $names[$t] = self::load_typ($t,true,$kos);
                }    
				$this->set('names',$names);
				
				$sizes = array();
				foreach(Set::extract('/OppOrderItem/name',$this->data) as $t)
					$sizes[$t] = self::load_size($t,true,$kos);
				$this->set('sizes',$sizes);
			}	
            
            $this->set('type_list',$type_list);

            $con = array('kos'=>0);
            if(!in_array($this->logged_user['CmsGroup']['id'],array(1,56,57,18))){
                    $con = array(
                        'kos'=>0,
                        'OR'=>array(
                            'coordinator_id'=>$this->logged_user['CmsUser']['id'],
                            'coordinator_id2'=>$this->logged_user['CmsUser']['id'],
                            'client_manager_id'=>$this->logged_user['CmsUser']['id'],
                        )
                    );
            }
            else if($this->logged_user['CmsGroup']['id'] == 18){
                $con = array(
                        'kos'=>0,
                        'manazer_realizace_id'=>$this->logged_user['CmsUser']['id'],
                    );
            }
            $this->set('company_list',$this->get_list('Company',$con));
			
            if($this->data['OppOrder']['status'] == 1){
                $this->render('../opp_orders/confirm');
            }
            else
                $this->render('../opp_orders/edit');
		} else {
	        /**
       	     * Nova objednávka, zašleme notifikaci
	         */ 
            $send_notification = false; 	  
		    if($this->data['OppOrder']['id'] == ''){ $send_notification = true;}          
		
    		if($this->OppOrder->save(array(
    				'id'			=> $this->data['OppOrder']['id'],
                    'opp_template_id'=> $this->data['OppOrder']['opp_template_id'],
    				'cms_user_id' 	=> $this->logged_user['CmsUser']['id']
    			))){
		 

				$this->loadModel('OppOrderItem');
				$this->OppOrderItem->deleteAll(array('opp_order_id'=>$this->OppOrder->id));
			
                $company_list = array();
				foreach($this->data['OppOrderItem'] as $itm){
				    $company_list[] = $itm['company_id'];
                    $itm['opp_order_id'] = $this->OppOrder->id;
					$itm['cms_user_id']	= $this->logged_user['CmsUser']['id'];
                    $itm['count_order'] = $itm['count'];
					$this->OppOrderItem->save($itm);
					$this->OppOrderItem->id = null;
				}              
                 
			    if($send_notification === true){
			        //prp koordinatory
                    $recipient_list = array();
			        if($this->logged_user['CmsGroup']['cms_group_superior_id'] == 2){
			             //COO jako samotny prijemce
			             $recipient_list[] = $this->logged_user['CmsUser']['email'];
                         
                         //a vsichni MR firem z OPP kterym prirazuje
                         $company_list = array_unique($company_list);
                         $this->loadModel('Company');
                         $this->Company->bindModel(array(
                            'belongsTo'=>array('CmsUser'=>array('foreignKey'=>'manazer_realizace_id'))
                         ));
                         $mr_list = $this->Company->find('all',array(
                            'conditions'=>array('Company.id'=>$company_list),
                            'fields'=>array('CmsUser.email')
                         ));
                         foreach($mr_list as $mr){ $recipient_list[] = $mr['CmsUser']['email']; }
			        }

			        $replace_list = array(
            			'##id##' 		=>$this->OppOrder->id,
                        '##vytvoril##'  =>$this->logged_user['CmsUser']['name']
            	    );
		            $this->Email->send_from_template_new(22,$recipient_list,$replace_list);
			    }
				
				die(json_encode(array('result'=>true)));
			} else 
				die(json_encode(array('result'=>false,'message'=>'Nepodarilo se ulozit do DB')));
		}
	}
	
    
    function load_kind($template_id = null, $return = false){
        if($template_id == null)
            die(json_encode(false));
            
        $this->loadModel('OppTemplateItem');  
        $allowed_kind = $this->OppTemplateItem->find('list', array(
            'conditions'=>array('opp_template_id'=>$template_id,'kos'=>0),
            'fields'=>array('type_id','type_id'),
            'group'=>array('type_id')
        ));
          
		$this->loadModel('OppItem');
		$list = $this->OppItem->find('list', array('conditions'=>array('kos'=>0,'type'=>array_keys($allowed_kind)),'group'=>'type','fields'=>array('type','type')));
		
        //$this->set('type_list',$type_list);
        if ($return === false)
			die(json_encode($list));
		else
			return $list;
	}
    
	function load_typ($type = null, $return = false,$kos = true){
	    $conditions = array('type'=>$type);
        if($kos == true){$conditions['kos'] = 0;}
        
		$this->loadModel('OppItem');
		$list = $this->OppItem->find('list',array('conditions'=>$conditions,'fields'=>array('name','name'),'group'=>'name'));
		if ($return === false)
			die(json_encode($list));
		else
			return $list;
	}
    
    function load_typ2($template_id,$type = null, $return = false){
        $this->loadModel('OppTemplateItem');  
        $allowed_typ = $this->OppTemplateItem->find('list', array(
            'conditions'=>array('opp_template_id'=>$template_id,'kos'=>0),
            'fields'=>array('name','name'),
            'group'=>array('name')
        ));
        
		$this->loadModel('OppItem');
		$list = $this->OppItem->find('list', array('conditions'=>array('kos'=>0,'type'=>$type,'name'=>array_keys($allowed_typ)),'group'=>'name','fields'=>array('name','name')));
		if ($return === false)
			die(json_encode($list));
		else
			return $list;
	}
	
	function load_size($type = null, $return = false,$kos = true){
	    $conditions = array('name'=>$type);
        if($kos == true){$conditions['kos'] = 0;}
        
		$this->loadModel('OppItem');
		$list = $this->OppItem->find('all',array('conditions'=>$conditions,'fields'=>array('id','size')));    
        $list = Set::combine($list, '{n}.OppItem.size',array('%s|%s','{n}.OppItem.size','{n}.OppItem.id'));
        
		if ($return === false)
			die(json_encode($list));
		else
			return $list;
	}
	
	function load_count_price($id = null){
		$this->loadModel('OppItem');
		$data = $this->OppItem->read(array('price_euro','price_cz','count'),$id);
		if ($data){
			die(json_encode(array('result'=>true,'data'=>$data)));
		} else {
			die(json_encode(array('result'=>false,'message'=>'Zaznam nebyl nalezen')));
		}
	}
}
?>