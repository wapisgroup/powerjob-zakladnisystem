<?php
//Configure::write('debug',2);
$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');
@define('CURRENT_YEAR', $_GET['current_year']); 
@define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);

class ReportDownPaymentsController extends AppController {
	var $name = 'ReportDownPayments';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email','Best','Hb','Tatra');
	var $uses = array('ReportDownPayment');
	var $renderSetting = array(
		'bindModel'	=> array('belongsTo'=>array('Company','CmsUser','Client'),
            'joinSpec'=>array(
                'SettingParentCompany'=>array(
                    'className'=>'SettingParentCompany',
                    'foreignKey'=>'Company.parent_id',
                    'primaryKey'=>'SettingParentCompany.id'
                ),
                'ConnectionClientRequirement'=>array(
                    'className'=>'ConnectionClientRequirement',
                    'foreignKey'=>'ReportDownPayment.connection_client_requirement_id',
                    'primaryKey'=>'ConnectionClientRequirement.id'
                ),
                'CompanyWorkPosition'=>array(
                    'className'=>'CompanyWorkPosition',
                    'foreignKey'=>'ConnectionClientRequirement.company_work_position_id',
                    'primaryKey'=>'CompanyWorkPosition.id'
                ),
                'CompanyMoneyItem'=>array(
                    'className'=>'CompanyMoneyItem',
                    'foreignKey'=>'ConnectionClientRequirement.company_money_item_id',
                    'primaryKey'=>'CompanyMoneyItem.id'
                ),
               'ParentClient'=>array(
                    'className'=>'Client',
                    'foreignKey'=>'Client.parent_id',
                    'primaryKey'=>'ParentClient.id',
                    'conditions'=>array('Client.parent_id IS NOT NULL')
                )
            )
        ),
		'controller'=>'report_down_payments',
		'SQLfields' => '*,(IF(ReportDownPayment.date_of_paid != "0000-00-00" && (ReportDownPayment.account <> "" OR ReportDownPayment.account <> 0),ReportDownPayment.account,IF(Client.parent_id IS NOT NULL,ParentClient.cislo_uctu,Client.cislo_uctu))) as cislo_uctu',
		'page_caption'=>'Seznam požadavků na zálohy',
		'sortBy'=>'ReportDownPayment.created.DESC',
		'SQLcondition'=>array('ReportDownPayment.type'=>0,'ReportDownPayment.kos'=>0,"ReportDownPayment.year = '#CURRENT_YEAR#'", "ReportDownPayment.month = '#CURRENT_MONTH#'"),
		'top_action' => array(
			// caption|url|description|permission
		//	'add_item'		=>	'Přidat|edit|Přidat nový požadavek|add',
            'multi_edit'		=>	'Uhradit hromadně|paid_all|Uhradit hromadně|multi_edit',
            'kb'		        =>	'Bankovní Export|kb|Bankovní Export|kb',
            'company_exception'	=>	'Firmy vyjimky|company_exception|Firmy vyjimky|company_exception',
		),
		'filtration' => array(
			'ReportDownPayment-cms_user_id'		=>	'select|CM/KOO|cm_list',
			'ReportDownPayment-company_id'		=>	'select|Společnost|company_list',
			'Client-name'		=>	'text|Zaměstnanec|',
			'ReportDownPayment-date_of_paid'	=>	'select|Neuhrazeno|filtr_uhrazeno',
			//'ReportDownPayment-type'	=>	'select|Typ|down_payments_type',
            
            'GET-current_year'							=>	'select|Rok|actual_years_list',
            'GET-current_month'							=>	'select|Měsíc|mesice_list',
		),
		'items' => array(
			'id'				=>	'ID|ReportDownPayment|id|hidden|',
			'cms_user_id'		=>	'CM/KOO|CmsUser|name|text|',
			'company'			=>	'Firma|Company|name|text|',
            'nad_spol'			=>	'Nadřazená spol.|SettingParentCompany|name|text|',
			'zamestnanec'		=>	'Zaměstnanec|Client|name|text|',
            'forma'				=>	'Forma|CompanyMoneyItem|name|text|',
			'cislo_uctu'		=>	'Č. účtu|0|cislo_uctu|cislo_uctu|',
			'castka'			=>	'Částka|ReportDownPayment|amount|text|',
			'date_of_paid'		=>	'Uhrazeno|ReportDownPayment|date_of_paid|date|',
			'type'		        =>	'Typ|ReportDownPayment|type|var|down_payments_type|',
			'created'			=>	'Vytvořeno|ReportDownPayment|created|date|'
		),
		'posibility' => array(
		//	'show'		=>	'show|Zobrazit aktivitu|show',			
			//'edit'		=>	'edit|Editovat položku|edit',
            'uhrazeno'	=>	'uhrazeno|Uhrazeno|uhrazeno',			
			'trash'		=>	'trash|Smazat položku|trash',			
		),
		'domwin_setting' => array(
			'sizes' 		=> '[900,900]',
			'scrollbars'	=> true,
			'languages'		=> 'false',
			'defined_lang'	=> 'false'
		),
        'checkbox_setting' => array(
			'model'			=>	'ReportDownPayment',
			'col'			=>	'date_of_paid',
			'stav_array'	=>	array('0000-00-00'),
			'month_year'	=>	false
		),
	);
	
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#','Vystavené zálohy'=>'#'));
		
		/*
		 * Spolecnost List pro filtraci
		 */
		$this->loadModel('Company'); 
		$company_conditions =  array('Company.kos'=>0);
		if (isset($this->filtration_company_condition))
			$company_conditions = am($company_conditions, $this->filtration_company_condition);
		$this->set('company_list',		$this->Company->find('list',array('conditions'=>$company_conditions,'order'=>'Company.name ASC')));
		unset($this->Company);
		
		/*
		 * Seznam CM a KOO List pro filtraci
		 */
		$this->loadModel('CmsUser');
		$this->set('cm_list',			$this->CmsUser->find('list',array('conditions'=>array('CmsUser.cms_group_id'=>array(3,4)))));
		unset($this->CmsUser);
		
        //změna profese podle nastaveneho company_work_position_id
		foreach($this->viewVars['items'] as &$item){
			//nastaveni forem odmeny a zobrazeni ubytovani a dopravy
			if(isset($item['CompanyMoneyItem'])){
					$doprava = 'Doprava '.($item['CompanyMoneyItem']['doprava'] != 0 ? 'Ano' : 'Ne');
					$ubytovani = 'Ubytování '.($item['CompanyMoneyItem']['cena_ubytovani_na_mesic'] != 0 ? 'Ano' : 'Ne');
	                $item['CompanyMoneyItem']['forma'] = $item['CompanyMoneyItem']['name'];
					$item['CompanyMoneyItem']['name'] = $item['CompanyMoneyItem']['name'].' - '.$ubytovani.' / '.$doprava;; 
			}
		}
        
		/*
		 * Start Render
		 */
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		if (empty($this->data)){
			$this->loadModel('Company');
			
			if ($this->logged_user['CmsGroup']['id'] == 3 || $this->logged_user['CmsGroup']['id']==4){ 
        		$company_conditions =  array('Company.kos'=>0);
        		if (isset($this->filtration_company_condition))
        			$company_conditions = am($company_conditions, $this->filtration_company_condition);
        		$this->set('company_list',		$this->Company->find('list',array('conditions'=>$company_conditions,'order'=>'Company.name ASC')));
        		unset($this->Company);
            
            } else if (in_array($this->logged_user['CmsGroup']['id'],array(1,5,6,7))){
				$this->set('company_list', $this->Company->find('list',array('conditions'=>array('Company.kos'=>0),'order'=>'Company.name')));
			}	

			unset($this->Company);
						
			if ($id != null){
				$this->ReportDownPayment->bindModel(array('belongsTo'=>array('Company','Client')));
				$this->data = $this->ReportDownPayment->read(null,$id);
                
				$this->loadModel('ConnectionClientRequirement'); 
				$this->ConnectionClientRequirement->bindModel(array(
					'belongsTo'=>array('Client')
				));
                
               $output = null;
			   $output = $this->ConnectionClientRequirement->find('list', array(
                    'fields'=>array('Client.id','Client.name'),
                    'conditions'=>array(
                        'OR'=>array(
                            'ConnectionClientRequirement.client_id'=>$this->data['ReportDownPayment']['client_id'],
                            'AND'=>array(
                                'ConnectionClientRequirement.company_id'=>$this->data['ReportDownPayment']['company_id'],
                                'ConnectionClientRequirement.type'=>2, 
                                'ConnectionClientRequirement.to'=>'0000-00-00', 
                                'Client.kos'=>0
                            )
                       )       
                    ), 
                    'order'=>'Client.name ASC',
                    'recursive'=>1                    
                ));	
				$this->set('client_list',$output);
			
                
                $output = null;
                $this->ConnectionClientRequirement->bindModel(array(
					'belongsTo'=>array('Client')
				));
				$output = $this->ConnectionClientRequirement->find('list', array(
                    'fields'=>array('Client.id','Client.cislo_uctu'),
                    'conditions'=>array(
                        'OR'=>array(
                            'ConnectionClientRequirement.client_id'=>$this->data['ReportDownPayment']['client_id'],
                            'AND'=>array(
                                'ConnectionClientRequirement.company_id'=>$this->data['ReportDownPayment']['company_id'],
                                'ConnectionClientRequirement.type'=>2, 
                                'ConnectionClientRequirement.to'=>'0000-00-00', 
                                'Client.kos'=>0
                            )
                       )       
                    ), 
                    'order'=>'Client.name ASC',
                    'recursive'=>1                    
                ));	         
				$this->set('client_title_list',$output);
				
				
				
			} else {
				$this->set('client_list',array());
				$this->set('client_title_list',array());
				$this->data = array('ReportDownPayment'=>array('cms_user_id'=>$this->logged_user['CmsUser']['id']));
			}
		} else {
			$this->ReportDownPayment->save($this->data);
			die(json_encode(array('result'=>true)));
		}
	}

	function load_ajax_zamestnanci($company_id){
		if ($company_id != null){
			$this->loadModel('ConnectionClientRequirement'); 
			$this->ConnectionClientRequirement->bindModel(array(
				'belongsTo'=>array(
					'Client'=>array(
						'fields'=>array('id','name','cislo_uctu')
					)
				)
			));
			$output = $this->ConnectionClientRequirement->find('all',array(							
					'fields'=>array('DISTINCT Client.id','Client.name','Client.cislo_uctu'),
					'conditions'=>array(
						'ConnectionClientRequirement.company_id'=>$company_id,
						'ConnectionClientRequirement.type'=>2,
                        'ConnectionClientRequirement.to'=>'0000-00-00', 
						'Client.kos'=>0
					), 
					'order'=>'Client.name ASC'
			));	

			die(json_encode(array('result'=>true,'data'=>$output)));
		}
		die(json_encode(array('result'=>false,'message'=>'Nebyla zvolena firma')));
	}
    
    
    /**
     * Funkce ktera vyplnuje dle aktualniho datumu nebo zvoleneho sloupce uhrazeno
     * 
     * @param $id 
     * @author Sol
     * @created 7.12.2009
     */
    function uhrazeno($id){
   	    if (empty($this->data)){
            $this->data['ReportDownPayment']['id'] = $id;
            
            //render
            $this->render('uhrazeno');
		}
        else {
        
            if ($this->data['ReportDownPayment']['date_of_paid'] != null){
                $this->ReportDownPayment->save($this->data);
                
                $detail = $this->ReportDownPayment->read(null,$this->data['ReportDownPayment']['id']);
                
                //ulozeni uctu k uhrade na pevno pro historii
                $this->loadModel('Client');
                $cl = $this->Client->read(array('cislo_uctu','parent_id'),$detail['ReportDownPayment']['client_id']);
                if($cl['Client']['parent_id'] != ''){
                  $cl = $this->Client->read(array('cislo_uctu'),$cl['Client']['parent_id']);
                }
                $this->ReportDownPayment->id = $this->data['ReportDownPayment']['id'];
                $this->ReportDownPayment->saveField('account',$cl['Client']['cislo_uctu']);
                unset($this->Client);
                
                //data pro historii
                $data = array(
                    'date_of_paid'=>$this->data['ReportDownPayment']['date_of_paid'],
                    'company_id'=>$detail['ReportDownPayment']['company_id'],
                    'amount'=>$detail['ReportDownPayment']['amount']
                );
                //odeslani akce pro zachyceni historie
                $this->requestAction('report_down_payments/record_paid_client/'.$detail['ReportDownPayment']['client_id'],array('data'=>$data));
             
    
    			die(json_encode(array('result'=>true)));
    		}
    		die(json_encode(array('result'=>false,'message'=>'Nebylo zvoleno datum')));
       }     
	}
    
    
    /**
     * funkce dava datum pro vsechny zaskrtnute
     */
    function paid_all(){
         		
        if(isset($this->data['ReportDownPayment']['save']) && $this->data['ReportDownPayment']['save'] == 1){
    		$this->loadModel('ReportDownPayment');
            $ids = json_decode($this->data['ReportDownPayment']['ids'],true);
                
           	foreach(array_shift($ids) as $key => $on){
    			$this->ReportDownPayment->id = $key;
    			$this->ReportDownPayment->saveField('date_of_paid',$this->data['ReportDownPayment']['date_of_paid']);
    		
            
            
                $detail = $this->ReportDownPayment->read(null,$key);
                
                //ulozeni uctu k uhrade na pevno pro historii
                $this->loadModel('Client');
                $cl = $this->Client->read(array('cislo_uctu','parent_id'),$detail['ReportDownPayment']['client_id']);
                if($cl['Client']['parent_id'] != ''){
                  $cl = $this->Client->read(array('cislo_uctu'),$cl['Client']['parent_id']);
                }
                $this->ReportDownPayment->id = $key;
                $this->ReportDownPayment->saveField('account',$cl['Client']['cislo_uctu']);
                unset($this->Client);
                
                //data pro historii
                $data = array(
                    'date_of_paid'=>$this->data['ReportDownPayment']['date_of_paid'],
                    'company_id'=>$detail['ReportDownPayment']['company_id'],
                    'amount'=>$detail['ReportDownPayment']['amount']
                );
           
                //odeslani akce pro zachyceni historie
                $this->requestAction('report_down_payments/record_paid_client/'.$detail['ReportDownPayment']['client_id'],array('data'=>$data));
             
            }
    		unset($this->ReportDownPayment);	
    		die(json_encode(array('result'=>true)));
       }  
       else {
            $this->data['ReportDownPayment']['ids'] = json_encode(array_shift(Set::extract('/ReportDownPayment/id',$this->data)));
         
            //render
            $this->render('paid_all');
        
       }
       
          
	}
    
    /**
     * slepa funkce pro historii
     * @param $client_id
     * @data $date_of_paid
     * @data $company_id
     * @data $amount
     */
    function record_paid_client($client_id){
       return true;      
    }
    
    
    function kb(){
        if(empty($this->data)){
            $this->ReportDownPayment->bindModel(array(
				'belongsTo'=>array('Company','CmsUser','Client'),
                'joinSpec'=>array(
                   'ParentClient'=>array(
                        'className'=>'Client',
                        'foreignKey'=>'Client.parent_id',
                        'primaryKey'=>'ParentClient.id'
                    )
                )
			));
			$output = $this->ReportDownPayment->find('all',array(							
					'conditions'=>array(
						'(
                          (Client.parent_id IS NULL && Client.cislo_uctu <> 0 &&  Client.cislo_uctu <> "")
                            ||
                          (Client.parent_id IS NOT NULL && ParentClient.cislo_uctu <> 0 &&  ParentClient.cislo_uctu <> "")
                         )  
                        ',
						'ReportDownPayment.type'=>0,
						'ReportDownPayment.kos'=>0,
						'Company.stat_id'=>1,
						'Company.parent_id'=>1,
						'ReportDownPayment.date_of_paid'=>"0000-00-00"
					),
                    'fields'=>array('ReportDownPayment.*','Company.name','Client.*',
                        'ParentClient.id','ParentClient.cislo_uctu','CmsUser.*'),
                    'order'=>'ReportDownPayment.created DESC'
			));	
            $this->set('zamestnanci_list',$output);
            
            /**
             * seznam nadrazenych spolecnosti
             */
            $this->loadModel('SettingParentCompany'); 
            $this->set('parent_company_list',$this->SettingParentCompany->find('list',array(	
                'conditions'=>array(
                    'kos'=>0
                )
            )));
            
            /**
             * nacteni historie ze slozek bank
             * kb,ss,tatra
             */
            $this->set('file_list_kb',self::get_history('kb'));
            $this->set('file_list_ss',self::get_history('ss'));
            $this->set('file_list_tatra',self::get_history('tatra'));
            
            //render
            $this->render('kb');
        }
        else{
            /**
             * jde o KB nebo SS
             */

                if(isset($this->data['type_export'])){
                    $type_of = $this->data['type_export'];
                } else {
                    $type_of = 'ss';
                }

                            
            /**
             * jen ty zaskrtnute
             */           
             $ids = array_keys($this->data['id'],1);
             //$ids = array(12486);
             
             /**
              * vytahnuti dat z db 
              */
             $this->ReportDownPayment->bindModel(array(
				'belongsTo'=>array('Client'),
                'joinSpec'=>array(
                   'ParentClient'=>array(
                        'className'=>'Client',
                        'foreignKey'=>'Client.parent_id',
                        'primaryKey'=>'ParentClient.id'
                    )
                )
			 ));
             $data_items = $this->ReportDownPayment->find('all',array(
                    'fields'=>array(
                        'Client.name',
                        'IF(Client.parent_id IS NOT NULL,ParentClient.cislo_uctu,Client.cislo_uctu) as cislo_uctu',
                        'ReportDownPayment.amount'
                    ),							
					'conditions'=>array(
						'ReportDownPayment.id'=>$ids
					)
			 ));

             
            /**
             * vytvoreni sablony pro jednotlive prikazy
             * pridani uctu prikazce z nadrazenych spolecnosti
             * a VS - 01 2010 (mesic a rok, vzdy do 15. dne v mesici mesic predchazejici, jinak aktualni, nebo muze byt zadavano rucne pri definici exportu)
             */
             $this->loadModel('SettingParentCompany');
             $parent_company = $this->SettingParentCompany->read(array('name','account'),$this->data['parent_company']);
            
            
             $prevdate = new DateTime();
             $prevdate->modify("-1 month");
             
             if(date('d') <= 15){
                $vs = $prevdate->format('mY'); 
                $ms_vs = $prevdate->format('m'); 
             }
             else{
                $vs = date('mY');
                $ms_vs = date('m');
             }
             
             
             /**
              * KB
              */
             if($type_of == 'kb'){
                   
                    /**
                     * inicializace kontruktoru
                     * vytvari se hlavicka a sablona pro data
                     */
                     $this->Best->Best();
                     
                     /**
                      * vytvoreni sablony pro jednotlive prikazy
                      */
                     $this->Best->make_data_template(trim(strtr($parent_company['SettingParentCompany']['account'],array('-'=>''))),$vs);
                    
                    /**
                     * nastaveni ciselne rady jednotlivych prikazu
                     * load z settingu
                     */
                     $this->loadModel('Setting');
                     $no_id = $this->Setting->read('value',6);
                     $this->Best->__set('no',$no_id['Setting']['value']['id_count_payment']);
             }  
             /**
              * Tatra 
              */      
             else if($type_of == 'tatra'){
                    /**
                     * inicializace kontruktoru
                     * vytvari se hlavicka a sablona pro data
                     */
                     $this->Tatra->Tatra();
                     $acccount = $prefix = null;
                     $acc = trim(strtr($parent_company['SettingParentCompany']['account'],array('-'=>'',' '=>'')));
                     $acccount = substr($acc,-10);
                     $prefix = substr($acc, 0, -10);
                     $this->Tatra->make_data_template($prefix,$acccount,$vs,$parent_company['SettingParentCompany']['name']);
             }
             /**
              * SS 
              */      
             else{
                    /**
                     * inicializace kontruktoru
                     * vytvari se hlavicka a sablona pro data
                     */
                     $this->Hb->Hb();
                     $acccount = $prefix = null;
                     $acc = trim(strtr($parent_company['SettingParentCompany']['account'],array('-'=>'',' '=>'')));
                     $acccount = substr($acc,-10);
                     $prefix = substr($acc, 0, -10);
                     $this->Hb->make_data_template($prefix,$acccount,$vs);
             }
            
            
            
            /**
             * pridani jednotlivych prikazu
             */
             foreach($data_items as $item){
                
                list($acc,$code_bank) = explode('/',$item['0']['cislo_uctu']);
                $castka = $item['ReportDownPayment']['amount'];
                
                //je zvoleny prevod
                if(isset($this->data['transfer_money']) && $this->data['transfer_money'] == 1){
                    if($this->data['tm_type'] == 1){// EUR -> CZK
                        $castka = round($castka * $this->data['tm_rate'],2);
                    }
                    else{ // CZK -> EUR
                        $castka = round($castka / $this->data['tm_rate'],2);
                    }
                }
                
                if($type_of == 'kb'){
                    $to_add = array(
                        'amount'=>$castka,
                        'code_bank'=>trim($code_bank),
                        'account'=>trim(strtr($acc,array('-'=>''))),
                        'date_to_pay'=>strtr($this->data['date_to_pay'],array('-'=>'')),
                        'message_commiter'=>"zaloha".$ms_vs." ".trim(iconv('UTF-8','Windows-1250',strtr($item['Client']['name'],array('-'=>' '))))
                    );
                
                    $this->Best->add_data($to_add);
                }
                 /**
                  * Tatra 
                  */      
                else if($type_of == 'tatra'){
                    $date = new DateTime($this->data['date_to_pay']);
                    
                    if(strlen($acc) < 10)
                        $acc = str_pad($acc,10,0,STR_PAD_LEFT);
                    
                    $to_add = array(
                        'amount'=>$castka,
                        'code_bank'=>trim($code_bank),
                        'prefix_account'=>substr(trim(strtr($acc,array('-'=>'',' '=>''))),0,-10),
                        'account'=>substr(trim(strtr($acc,array('-'=>'',' '=>''))),-10),
                        'date_to_pay'=>$date->format('ymd'),
                        'message_commiter'=>"zaloha".$ms_vs." ".trim(iconv('UTF-8','Windows-1250',strtr($item['Client']['name'],array('-'=>' ')))),
                        'name_contra'=>trim(iconv('UTF-8','Windows-1250',strtr($item['Client']['name'],array('-'=>' '))))
                    );
                
                    $this->Tatra->add_data($to_add);
                }
                else{
                    $date = new DateTime($this->data['date_to_pay']);
                    
                    if(strlen($acc) < 10)
                        $acc = str_pad($acc,10,0,STR_PAD_LEFT);
                    
                    $to_add = array(
                        'amount'=>$castka,
                        'code_bank'=>trim($code_bank),
                        'prefix_account'=>substr(trim(strtr($acc,array('-'=>'',' '=>''))),0,-10),
                        'account'=>substr(trim(strtr($acc,array('-'=>'',' '=>''))),-10),
                        'date_to_pay'=>$date->format('d.m.Y'),
                        'message_commiter'=>"zaloha".$ms_vs." ".trim(iconv('UTF-8','Windows-1250',strtr($item['Client']['name'],array('-'=>' '))))
                    );
                
                    $this->Hb->add_data($to_add);
                }    
             }
             
             
             if($type_of == 'kb'){
                /**
                 * nakonec vytvoreni paticky
                 */
                 $this->Best->make_footer(); 
             }
            
             if($type_of == 'kb')
                $componenta = 'Best';
             else if($type_of == 'tatra')  
                $componenta = 'Tatra'; 
             else
                $componenta = 'Hb';   
            
             /**
              * vytovreni souboru a zavolani force dl
              */
             if($this->$componenta->get_error() === true){
                /**
                 * pokud byl proveden vice jak jeden prikaz tak aktualizuj id v db pro priste 
                 */
                 if($type_of == 'kb'){
                     if($this->Best->__get('count_payments') > 0){                
                        $data_setting = array(
                            'Setting'=>array(
                                'id'=>6,
                                'value'=>array(
                                    'id_count_payment'=>$no_id['Setting']['value']['id_count_payment'] + $this->Best->__get('count_payments')
                                )
                            )
                        );
                        $this->Setting->save($data_setting);
                     }
                 }
                
                 $filename = $this->$componenta->create_file();
                 if($filename)
                    die(json_encode(array('result'=>true,'file'=>$filename,'type'=>$type_of)));
                 else if($this->$componenta->testing === true){
                    die(json_encode(array('result'=>false,'message'=>'Testování...')));
                 }   
                 else
                    die(json_encode(array('result'=>false,'message'=>'Soubor nebyl vytvořen.')));
             }
             else{
                 die(json_encode(array('result'=>false,'message'=>implode("\n",$this->$componenta->get_error()))));
             }
     
        } 
    }
    
    /**
     * funkce pro nacteni zamestnancu podle nadrazene spolecnosti
     */
    public function load_parent_company_employees($parent_company_id){
 
         	$this->ReportDownPayment->bindModel(array(
				'belongsTo'=>array('Company','CmsUser','Client'),
                'joinSpec'=>array(
                   'ParentClient'=>array(
                        'className'=>'Client',
                        'foreignKey'=>'Client.parent_id',
                        'primaryKey'=>'ParentClient.id'
                    )
                )
			));
			$output = $this->ReportDownPayment->find('all',array(							
					'conditions'=>array(
						'(
                          (Client.parent_id IS NULL && Client.cislo_uctu <> 0 &&  Client.cislo_uctu <> "")
                            ||
                          (Client.parent_id IS NOT NULL && ParentClient.cislo_uctu <> 0 &&  ParentClient.cislo_uctu <> "")
                         )  
                        ',
						'ReportDownPayment.type'=>0,
						'ReportDownPayment.kos'=>0,
						//'Company.stat_id'=>1,
						'Company.parent_id'=>$parent_company_id,
						'ReportDownPayment.date_of_paid'=>"0000-00-00"
					),
                    'fields'=>array('ReportDownPayment.*','Company.name','Client.*',
                        'ParentClient.id','ParentClient.cislo_uctu','CmsUser.*'),
                    'order'=>'ReportDownPayment.created DESC'
			));	
            //pr($output);
            $this->set('zamestnanci_list',$output);
            
         unset($this->ReportDownPayment);
         echo $this->render('kb_items');
         die();
    }
    
    /**
     * FORCE DL KB
     */
    public function download($file_path,$file,$file_name = null){
       
          if($file_name == null){ 
            $file_name = explode('.',$file);  
            $file_name = $file_name[0];  
          }
          
          $pripona = strtolower(end(explode(".", $file)));
  
          if (file_exists('./'.$file_path.'/'.$file)){  
               $filesize = filesize('./'.$file_path.'/'.$file);               
               $cesta = "http://".$_SERVER['SERVER_NAME']."/".$file_path."/".$file;
               header("Pragma: public"); // požadováno
                  header("Expires: 0");
                  header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                  header("Cache-Control: private",false); // požadováno u některých prohlížečů
                  header("Content-Transfer-Encoding: binary");
               header("Content-Length: " . $filesize);
               Header('Content-Type: application/octet-stream');
               Header('Content-Disposition: attachment; filename="'.$file_name.'.'.$pripona.'"');
               readfile($cesta);
               die();
          } 
          else 
            die('Požadovaný soubor nenalezen');
      
    }
    
    
    /**
     * nastaveni firem, ktere budou mit vyjimku
     */
    public function company_exception(){
        
        $this->loadModel('CompanyExceptionPayment');
        $this->CompanyExceptionPayment->bindModel(array('belongsTo'=>array('CmsUser','Company')));
        $this->set('company_exception_list',$this->CompanyExceptionPayment->find('all',array(
            'conditions'=>array(
                'CompanyExceptionPayment.kos'=>0
            )
        )));
        
        //render
        $this->render('company_exception/index');
    }
    
    /**
     * pridani vyjimky
     */
    public function exception_add(){
        if(empty($this->data)){
            $this->loadModel('Company');
            $this->set('company_list',$this->Company->company_list());
            
            //render
            $this->render('company_exception/add');
        }
        else{
            $this->loadModel('CompanyExceptionPayment');
            $this->data['CompanyExceptionPayment']['cms_user_id'] = $this->logged_user['CmsUser']['id'];
            if($this->CompanyExceptionPayment->save($this->data))
                die(json_encode(array('result'=>true)));
            else
                die(json_encode(array('result'=>false)));
                
        }
    } 
    
    /**
     * smazani vyjimky
     */
    public function exception_trash($id){
         $this->loadModel('CompanyExceptionPayment');
         $this->CompanyExceptionPayment->id = $id;  
         $this->CompanyExceptionPayment->saveField('kos',1);  
         die();
    }
    
    private function get_history($dir){
        $file_list = array(); 
        if ($handle = opendir('./'.$dir)) {
            /* This is the correct way to loop over the directory. */
            while (false !== ($file = readdir($handle))) {
                if($file != '.' && $file != '..' )
                    $file_list[] = $file;
            }
            closedir($handle);
        }
        rsort($file_list);
        return $file_list;
    }
}
?>