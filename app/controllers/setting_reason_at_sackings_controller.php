<?php
//Configure::write('debug',1);
class SettingReasonAtSackingsController extends AppController {
	var $name = 'SettingReasonAtSackings';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('SettingReasonAtSacking');
	var $renderSetting = array(
		'controller'=>'setting_reason_at_sackings',
		'SQLfields' => array('id','name','updated','created'),
		'page_caption'=>'Nastavení důvodu propuštení',
		'sortBy'=>'SettingReasonAtSacking.created.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat důvod|add',
		//	'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
		//	'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
		//	'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
		//	'SettingAccommodationType-status'	=>	'select|Stav|select_stav_zadosti',
		//	'SettingAccommodationType-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|SettingReasonAtSacking|id|text|',
			'name'		=>	'Název|SettingReasonAtSacking|name|text|',
			'updated'	=>	'Upraveno|SettingReasonAtSacking|updated|datetime|',
			'created'	=>	'Vytvořeno|SettingReasonAtSacking|created|datetime|'
		),
		'posibility' => array(
			'status'	=> 	'status|Změna stavu|status',
			'edit'		=>	'edit|Editace položky|edit',
			'trash'	=>	'trash|Do košiku|trash'
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Nastavení důvodu propuštení'=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null)
				$this->data = $this->SettingReasonAtSacking->read(null,$id);
			$this->render('edit');
		} else {
			$this->SettingReasonAtSacking->save($this->data);
			die();
		}
	}
}
?>