<?php
	Configure::write('debug',1);
	define('fields','AuditEstateLossEvent.*,Client.*, AuditEstate.*,AtCompany.*,AuditEstateLossEventCmsUser.name,
        ClientSpravceMajetku.name,ClientSpravceMajetku.mobil1,
        ( 
		  if(AuditEstateLossEvent.type = 1,
            AuditEstate.name,
            AuditEstateLossEvent.audit_estate_name
          )  
		) as majetek,
        ( 
		  if(AuditEstateLossEvent.type = 1,
            AuditEstate.imei_sn_vin,
            AuditEstateLossEvent.imei_sn_vin
          )  
		) as imei_sn_vin,	
        (
		  if(AuditEstateLossEvent.type = 1,
            AuditEstate.internal_number,
            AuditEstateLossEvent.internal_number
          )  
		) as internal_number,
		(
          if(AuditEstateLossEvent.type = 1,
            AtCompany.name,
            AuditEstateLossEvent.at_company_name
          )  
		) as firma,
        (
          if(AuditEstateLossEvent.type = 1,
            Client.name,
            AuditEstateLossEvent.osoba
          )  
		) as vlastnik_majetku,
        ( 
          CONCAT_WS(" ",IFNULL(AuditEstateLossEvent.expected_price,0),if(AuditEstateLossEvent.currency = 0,",- EUR",",- Kč"))  
		) as expected_price	,
        ( 
          CONCAT_WS(" ",IFNULL(AuditEstateLossEvent.fa_price,0),if(AuditEstateLossEvent.currency = 0,",- EUR",",- Kč"))  
		) as fa_price,	
        ( 
          SELECT COUNT(id) FROM wapis__audit_estate_loss_event_attachments WHERE audit_estate_loss_event_id = AuditEstateLossEvent.id and kos = 0
        ) as count_attachment
	');
class ReportMaintenancesController extends AppController {
	var $name = 'AuditEstatesLossEvents';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('AuditEstateLossEvent');
	var $renderSetting = array(
		'bindModel'	=> array(
			'belongsTo'=>array('AuditEstate','Client'),
			'joinSpec'=>array(
                'AtCompany'=>array(
    				'className'=>'AtCompany',
    				'primaryKey'=>'AtCompany.id',
    				'foreignKey'=>'AuditEstate.at_company_id'
			    ),
                'AtProjectCentre'=>array(
    				'className'=>'AtProjectCentre',
    				'primaryKey'=>'AtProjectCentre.id',
    				'foreignKey'=>'AuditEstate.at_project_centre_id'
			    ),
                'SpravceMajetku'=>array(
    				'className'=>'CmsUser',
    				'primaryKey'=>'SpravceMajetku.id',
    				'foreignKey'=>'AtProjectCentre.spravce_majetku_id'
			    ),
                'AuditEstateLossEventCmsUser'=>array(
                    'className'=>'CmsUser',
                    'primaryKey'=>'AuditEstateLossEventCmsUser.id',
    				'foreignKey'=>'AuditEstateLossEvent.cms_user_id'
                ),
                'ClientSpravceMajetku'=>array(
                    'className'=>'Client',
                    'primaryKey'=>'ClientSpravceMajetku.id',
    				'foreignKey'=>'SpravceMajetku.at_employee_id'
                )
			)
		),
		'SQLfields' =>array(fields),
		'SQLcondition' => array(
			'AuditEstateLossEvent.send_to_maintenance'=>1,
		),
		'controller'=> 'report_maintenances',
		'page_caption'=>'Report údržby',
		'sortBy'=>'AuditEstateLossEvent.id.DESC',
		//'group_by'=>'AuditEstate.id',
		'top_action' => array(
			'add_item'		=>	'Přidat|edit|Pridat událost|add',
            'export_excel' => 'Excel|export_excel|Export Excel|export_excel', 
		),
		'filtration' => array(
			'AuditEstate-name|AuditEstateLossEvent-audit_estate_name'			=>	'text|Název majetku|',
			//'AuditEstate-imei_sn_vin|AuditEstateLossEvent-imei_sn_vin'		=>	'text|IMEI/SN/VIN|',
			//'AuditEstateLossEvent-osoba|Client-name'	=>	'text|Osoba|',
			'AuditEstate-at_company_id'	    	=>	'select|Firma|company_list',
            //'AuditEstateLossEvent-snp_stav'	    	=>	'select|Stav|snp_stav_list',	
            //'AuditEstateLossEvent-currency'	    	=>	'select|Měna|currency_list'				
		),
		'items' => array(
			'id'		=>	'ID|AuditEstateLossEvent|id|text|',
			'name'		=>	'Název|AuditEstateLossEvent|audit_estate_name|text|',
			'imei'	      =>	'IMEI/SN/VIN|0|imei_sn_vin|text|',
			'faults_found'	      =>	'Zjištěné závady|AuditEstateLossEvent|faults_found|text|',
            'vlastnik_majetku'	=>	'Osoba v době škodové události|0|vlastnik_majetku|text|',
		    'date_event'	=>	'Datum škodové události|AuditEstateLossEvent|date_event|date|', 
            'date_to_repair'	=>	'Datum odeslání do opravy|AuditEstateLossEvent|date_to_repair|date|',
            'service_center'    =>	'Servisní středisko|AuditEstateLossEvent|service_center|text|',   
            'date_from_repair'	=>	'Datum přijetí z opravy|AuditEstateLossEvent|date_from_repair|date|',   
        	'expected_price'	=>	'Předpokládáná cena|0|expected_price|text|', 
            'fa_price'	=>	'Fakturovaná cena|0|fa_price|text|',
            'stav_km'	=>	'Stav Km|AuditEstateLossEvent|stav_km|text|',
            'repair_info'	=>	'Oprava - informace|AuditEstateLossEvent|repair_info|text|orez3#30',
		),
		'posibility' => array(
		)     
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Evidence majetku'=>'#',$this->renderSetting['page_caption']=>'#'));
        $this->set('company_list',$this->get_list('AtCompany'));           
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
}
?>