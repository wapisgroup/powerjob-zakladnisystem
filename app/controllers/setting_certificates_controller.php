<?php
class SettingCertificatesController extends AppController {
	var $name = 'SettingCertificates';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('SettingCertificate');
	var $renderSetting = array(
		'controller'=>'setting_certificates',
		'SQLfields' => array('id','name','updated','created'),
		'page_caption'=>'Nastavení certifikátů',
		'sortBy'=>'SettingCertificate.name.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
		//	'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
		//	'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
		//	'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
		//	'SettingCertificate-status'	=>	'select|Stav|select_stav_zadosti',
		//	'SettingCertificate-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|SettingCertificate|id|text|',
			'name'		=>	'Název|SettingCertificate|name|text|',
			'updated'	=>	'Upraveno|SettingCertificate|updated|datetime|',
			'created'	=>	'Vytvořeno|SettingCertificate|created|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
			'trash'	=>	'trash|Do košiku|trash'
		),
		'domwin_setting' => array(
			'sizes' 		=> '[550,900]',
			'scrollbars'	=> true,
			'languages'		=> 'false'
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Nastavení certifikátů'=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null)
				$this->data = $this->SettingCertificate->read(null,$id);
			$this->render('edit');
		} else {
			$this->SettingCertificate->save($this->data);
			die();
		}
	}
}
?>