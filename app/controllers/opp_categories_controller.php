<?php
//Configure::write('debug',1);
class OppCategoriesController extends AppController {
	var $name = 'OppCategories';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('OppCategory');
	var $renderSetting = array(
		'controller'=>'opp_categories',
		'SQLfields' => array('id','name','parent_id','updated','created'),
		'page_caption'=>'OPP číselník',
		'sortBy'=>'OppCategory.created.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat důvod|add',
		//	'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
		//	'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
		//	'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
		//	'SettingAccommodationType-status'	=>	'select|Stav|select_stav_zadosti',
		//	'SettingAccommodationType-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|OppCategory|id|text|',
            'name'		=>	'Název|OppCategory|name|text|',
			'parent_id'		=>	'Kód kategorie v importu|OppCategory|parent_id|text|',
			'updated'	=>	'Upraveno|OppCategory|updated|datetime|',
			'created'	=>	'Vytvořeno|OppCategory|created|datetime|'
		),
		'posibility' => array(
			'status'	=> 	'status|Změna stavu|status',
			'edit'		=>	'edit|Editace položky|edit',
			'delete'	=>	'trash|Do košiku|trash'			
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#',$this->renderSetting['page_caption']=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null)
				$this->data = $this->OppCategory->read(null,$id);
			$this->render('edit');
		} else {
			$this->OppCategory->save($this->data);
			die();
		}
	}
}
?>