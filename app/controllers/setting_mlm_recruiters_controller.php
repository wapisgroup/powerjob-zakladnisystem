<?php
class SettingMlmRecruitersController extends AppController {
	var $name = 'SettingMlmRecruiters';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('ConnectionMlmRecruiter');
	var $master_MLM_id = 1;
	var $master_id = 7;
	var $renderSetting = array(
		'controller'=>'setting_mlm_recruiters',
		'bindModel'	=> array('belongsTo'=>array('Recruiter'=>array('className'=>'CmsUser','foreignKey'=>'cms_user_id'),'ParentUser'=>array('className'=>'CmsUser','foreignKey'=>'parent_id'))),
		'SQLfields' => array('Recruiter.name','Recruiter.mlm_code','ParentUser.name','ConnectionMlmRecruiter.id','ConnectionMlmRecruiter.closed','ConnectionMlmRecruiter.cms_user_id','ConnectionMlmRecruiter.parent_id','ConnectionMlmRecruiter.mlm_group_id','ConnectionMlmRecruiter.updated','ConnectionMlmRecruiter.created','ConnectionMlmRecruiter.rok_od','ConnectionMlmRecruiter.mesic_od','ConnectionMlmRecruiter.rok_do','ConnectionMlmRecruiter.mesic_do'),
		'page_caption'=>'MLM',
		'sortBy'=>'ConnectionMlmRecruiter.closed.ASC',
		//'SQLcondition'=>array('ConnectionMlmRecruiter.closed'=>"0000-00-00"),
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat MLM|add',
		//	'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
		//	'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
		//	'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
			//'ConnectionMlmRecruiter-created'	=>	'select|Stav|select_stav_zadosti',
			'ConnectionMlmRecruiter-created|mlm'		=>	'date|Datum|',
		//	'SettingAccommodationType-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|ConnectionMlmRecruiter|id|text|',
			'mlm_group_id'		=>	'Úroveň|ConnectionMlmRecruiter|mlm_group_id|var|mlm_group_list',
			'recruiter_name'		=>	'Recruiter|Recruiter|name|text|',
			'recruiter_code'		=>	'MLM code|Recruiter|mlm_code|text|',
			'parent_id'		=>	'Parent|ParentUser|name|text|',
			'rok_od'	=>	'Platnost od - rok|ConnectionMlmRecruiter|rok_od|text|',
			'mesic_od'	=>	'Platnost od - mesic|ConnectionMlmRecruiter|mesic_od|text|',
			'rok_do'	=>	'Platnost do - rok|ConnectionMlmRecruiter|rok_do|text|',
			'mesic_do'	=>	'Platnost do - mesic|ConnectionMlmRecruiter|mesic_do|text|',
			//'created'	=>	'Platnost od|ConnectionMlmRecruiter|created|date|',
			//'closed'	=>	'Platnost do|ConnectionMlmRecruiter|closed|date|',
			//'updated'	=>	'Upraveno|ConnectionMlmRecruiter|updated|datetime|',
			
		),
		'posibility' => array(
			'status'	=> 	'status|Změna stavu|status',
			'edit'		=>	'edit|Editace položky|edit',
			'trash'	=>	'trash_re|Do košiku|trash'
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','MLM'=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		
		if (empty($this->data)){

			//editace
			if ($id != null) 
			{
				// bindModel
    			$this->ConnectionMlmRecruiter->bindModel(array(
    			'belongsTo'	=>	array('CmsUser' => array('fields' =>array('name')))
				));
				$this->data = $this->ConnectionMlmRecruiter->read(null,$id);
				$rok_zalozeni = $this->data['ConnectionMlmRecruiter']['rok_od'];
				$mesic_zalozeni = $this->data['ConnectionMlmRecruiter']['mesic_od'];
				// pokud je zalozen ve stejnem mesici a roce pouze delame update
				if($rok_zalozeni == date('Y') && $mesic_zalozeni == date('m'))
					$this->set('onlyUpdate',true);	
				else
					$this->set('onlyUpdate',false);	
			}	
			
			// nacteni moznych novych recruiteru do mlm
			$this->loadModel('CmsUser');
			$query=$this->CmsUser->query("
				SELECT 
					CmsUser.id,CmsUser.name, MLM.created,rok_do,mesic_do, MLM.id, MLM.kos  
				FROM `wapis__cms_users` AS CmsUser
				LEFT JOIN 
					wapis__connection_mlm_recruiters as MLM
					ON (CmsUser.id = MLM.cms_user_id) 
				group by CmsUser.id 
				having  (
					(
						(rok_do != '0000' and mesic_do!='00')
					AND(
						rok_do = YEAR(NOW()) AND 
						mesic_do!=MONTH(NOW())
					)) 
					or MLM.id is NULL
				) or MLM.kos=1 
				Order by CmsUser.name");
      
			$CmsUser = array();
			foreach($query as $key => $val){
				$CmsUser[$val['CmsUser']['id']] = $val['CmsUser']['name'];
			}
		
			$this->set('recruiters_list',$CmsUser);
					
			$this->set('id',$id);	
			$this->render('edit');
		} else {
		//save	
			if (($this->data["ConnectionMlmRecruiter"]["id"] != $this->master_MLM_id)){  //master_MLM_id je Kulla
				//minuly mesic
			   $dnes=date('Y-m');
		       $date = new DateTime($dnes);
		       $date->modify("-1 month");
		       $mm = implode('-',$date->format("Y-m"));
			   $m_rok = $mm[0];
			   $m_mesic = $mm[1];

			   //promenne 
			   $NewLevel=$this->data["ConnectionMlmRecruiter"]["mlm_group_id"];
			   $OldLevel=$this->data["OldData"]["mlm_group_id"];
			   $onlyUpdate = ($this->data["OldData"]["onlyUpdate"]==1 ? true : false);
			  	
				if ($this->data["ConnectionMlmRecruiter"]["id"] != null){
					if(
						($NewLevel!=$OldLevel)||
						($this->data["ConnectionMlmRecruiter"]["parent_id"]!=$this->data["OldData"]["parent_id"])     
					){
						if($onlyUpdate){ //pouze update stavjcicho  a nevytvareni historie 
							$this->ConnectionMlmRecruiter->id=$this->data["ConnectionMlmRecruiter"]["id"];
							$this->ConnectionMlmRecruiter->save($this->data);
						}
						else{ //uzavreni predesleho a udelat novy zaznam
				            $this->ConnectionMlmRecruiter->id=$this->data["ConnectionMlmRecruiter"]["id"];
				            $this->ConnectionMlmRecruiter->saveField('rok_do',$m_rok);
				            $this->ConnectionMlmRecruiter->saveField('mesic_do',$m_mesic);
			                    
							$this->ConnectionMlmRecruiter->id = null; 
							$this->data['ConnectionMlmRecruiter']['rok_od']=date('Y');
							$this->data['ConnectionMlmRecruiter']['mesic_od']=date('m');
							$this->ConnectionMlmRecruiter->save($this->data);  
						}

						//zmeny s childy
						if($NewLevel > $OldLevel)
						{
		                    if($NewLevel==3 && $OldLevel==2)
		                    {                  

		                      $child_list=$this->ConnectionMlmRecruiter->find('all',
		                      array(
		                        'conditions'=>array('ConnectionMlmRecruiter.kos'=>0,
		                            'ConnectionMlmRecruiter.parent_id '=>$this->data["ConnectionMlmRecruiter"]["cms_user_id"],
		                            'ConnectionMlmRecruiter.mlm_group_id'=>3,
		                            'ConnectionMlmRecruiter.rok_do'=>'0000',
									'ConnectionMlmRecruiter.mesic_do'=>'00'
								),
		                        'fields'=>array('ConnectionMlmRecruiter.id','ConnectionMlmRecruiter.cms_user_id','ConnectionMlmRecruiter.mlm_group_id','ConnectionMlmRecruiter.rok_od','ConnectionMlmRecruiter.mesic_od')
		                          )
		                       
		                     ); 
		                    }
		                    else if($NewLevel==2 && $OldLevel==1)
		                    {   
		                  
								$child_list=$this->ConnectionMlmRecruiter->find('all',
		                        array(
		                        'conditions'=>array('ConnectionMlmRecruiter.kos'=>0,
		                            'ConnectionMlmRecruiter.parent_id '=>$this->data["ConnectionMlmRecruiter"]["cms_user_id"],
		                            'ConnectionMlmRecruiter.mlm_group_id IN (2)',
		                            'ConnectionMlmRecruiter.rok_do'=>'0000',
									'ConnectionMlmRecruiter.mesic_do'=>'00'),
		                        'fields'=>array('ConnectionMlmRecruiter.id','ConnectionMlmRecruiter.cms_user_id','ConnectionMlmRecruiter.mlm_group_id','ConnectionMlmRecruiter.rok_od','ConnectionMlmRecruiter.mesic_od')
		                        )
		                       
								); 
		                     
		                    }
		                    else if($NewLevel==3 && $OldLevel==1)
		                    {                 
								$child_list=$this->ConnectionMlmRecruiter->find('all',
								array(
		                        'conditions'=>array('ConnectionMlmRecruiter.kos'=>0,
		                            'ConnectionMlmRecruiter.parent_id '=>$this->data["ConnectionMlmRecruiter"]["cms_user_id"],
		                            'ConnectionMlmRecruiter.mlm_group_id IN (2,3)',
		                            'ConnectionMlmRecruiter.rok_do'=>'0000',
									'ConnectionMlmRecruiter.mesic_do'=>'00'),
		                        'fields'=>array('ConnectionMlmRecruiter.id','ConnectionMlmRecruiter.cms_user_id','ConnectionMlmRecruiter.mlm_group_id','ConnectionMlmRecruiter.rok_od','ConnectionMlmRecruiter.mesic_od')
		                          )
		                       
								); 
		                     
							}

							// cyklus na prochazeni child_listu a jejich upravy
		                    foreach($child_list as $val => $value)
		                    {
								$child_rok_od =  $value["ConnectionMlmRecruiter"]["rok_od"];
								$child_mesic_od =  $value["ConnectionMlmRecruiter"]["mesic_od"];

								//pokud je child ze stejneho data tak ho jen updateni
								if($child_rok_od == date('Y') && $child_mesic_od == date('m')){
									$this->ConnectionMlmRecruiter->id = $value["ConnectionMlmRecruiter"]["id"]; 
		                            $data["ConnectionMlmRecruiter"]["cms_user_id"]=$value["ConnectionMlmRecruiter"]["cms_user_id"];
		                            $data["ConnectionMlmRecruiter"]["parent_id"]=$this->master_id;
		                            $data["ConnectionMlmRecruiter"]["mlm_group_id"]=$value["ConnectionMlmRecruiter"]["mlm_group_id"];
		                            $this->ConnectionMlmRecruiter->save($data);	
								}
								else {

									//ulozeni starych spojeni
		          			        $this->ConnectionMlmRecruiter->id = $value["ConnectionMlmRecruiter"]["id"];
									$this->ConnectionMlmRecruiter->saveField('rok_do',$m_rok);
									$this->ConnectionMlmRecruiter->saveField('mesic_do',$m_mesic);
		    
		                            //vytvoreni novyho spojeni              
		                            $this->ConnectionMlmRecruiter->id= null; 
		                            $data["ConnectionMlmRecruiter"]["cms_user_id"]=$value["ConnectionMlmRecruiter"]["cms_user_id"];
		                            $data["ConnectionMlmRecruiter"]["parent_id"]=$this->master_id;
		                            $data["ConnectionMlmRecruiter"]["mlm_group_id"]=$value["ConnectionMlmRecruiter"]["mlm_group_id"];
		                            $data['ConnectionMlmRecruiter']['rok_od']=date('Y');
									$data['ConnectionMlmRecruiter']['mesic_od']=date('m');
		                            $this->ConnectionMlmRecruiter->save($data);	                   
								}
		                    } 

						}

					}
				}
				else {
					$this->data['ConnectionMlmRecruiter']['rok_od']=date('Y');
					$this->data['ConnectionMlmRecruiter']['mesic_od']=date('m');
					$this->ConnectionMlmRecruiter->save($this->data);	 
				}
			}
        die();   
		}
		
	}
	
	function load_recruiters($level = null,$parent_id = null,$id_cms_user=null){
  		$this->loadModel('CmsUser');
  		
  		// render view
  		if ($level != null){
	  			
	        // bindModel
	    	$this->ConnectionMlmRecruiter->bindModel(array(
	    	'belongsTo'	=>	array('CmsUser' => array('fields' =>array('name')))
	    	));
	  		$this->set('parent_list',$this->ConnectionMlmRecruiter->find('list',array(
	            'conditions'=>array(
					'ConnectionMlmRecruiter.kos'=>0,
					'ConnectionMlmRecruiter.cms_user_id !='=>$id_cms_user,
					'ConnectionMlmRecruiter.mlm_group_id <'=>$level,
					'ConnectionMlmRecruiter.rok_do'=>'0000',
					'ConnectionMlmRecruiter.mesic_do'=>'00'
				),
	            'fields'=>array(
					'ConnectionMlmRecruiter.cms_user_id',
					'CmsUser.name'
				),
	            'recursive' =>2
	          )
	       
			));
	      
			// selected pro editaci
		    if($parent_id!=null) 
	         $this->data["ConnectionMlmRecruiter"]["parent_id"]=$parent_id;
				
			$this->render('connection');
		}
	}
	
	function trash_re($id){
		
		if (($id != null) && ($id != $this->master_MLM_id)){  //master_MLM_id je Kulla
		  
    		$this->data = $this->ConnectionMlmRecruiter->read(null,$id);
    		$this_rok_od = $this->data['ConnectionMlmRecruiter']['rok_od']; 
    		$this_mesic_od = $this->data['ConnectionMlmRecruiter']['mesic_od']; 
    		$level = $this->data['ConnectionMlmRecruiter']['mlm_group_id']; 
    		$parent_id = $this->data['ConnectionMlmRecruiter']['cms_user_id'];
    			
			//minuly mesic
		    $dnes=date('Y-m');
	        $date = new DateTime($dnes);
	        $date->modify("-1 month");
	        $mm=split('-',$date->format("Y-m"));
		    $m_rok = $mm[0];
		    $m_mesic = $mm[1];
    			
    		switch($level){
				case '1' : 
	                $child_list=$this->ConnectionMlmRecruiter->find('all',
						array(
	                    'conditions'=>array('ConnectionMlmRecruiter.kos'=>0,
	                        'ConnectionMlmRecruiter.parent_id '=>$this->data["ConnectionMlmRecruiter"]["cms_user_id"],
	                        'ConnectionMlmRecruiter.mlm_group_id IN (2,3)',
	                        'ConnectionMlmRecruiter.rok_do'=>'0000',
							'ConnectionMlmRecruiter.mesic_do'=>'00'),
	                    'fields'=>array('ConnectionMlmRecruiter.id','ConnectionMlmRecruiter.cms_user_id','ConnectionMlmRecruiter.mlm_group_id','ConnectionMlmRecruiter.rok_od','ConnectionMlmRecruiter.mesic_od')
	                          )
	                       
					); 
	                 //ulozeni stareho
	    			$this->ConnectionMlmRecruiter->id = $id;
	                $this->ConnectionMlmRecruiter->saveField('rok_do',date('Y'));
					$this->ConnectionMlmRecruiter->saveField('mesic_do', date('m'));
	                unset($id);
	                  
	                // cyklus na prochazeni child_listu a jejich upravy
	                    foreach($child_list as $val => $value)
	                    {
							$child_rok_od =  $value["ConnectionMlmRecruiter"]["rok_od"];
							$child_mesic_od =  $value["ConnectionMlmRecruiter"]["mesic_od"];

							//pokud je child ze stejneho data tak ho jen updateni
							if($child_rok_od == date('Y') && $child_mesic_od == date('m')){
								$this->ConnectionMlmRecruiter->id = $value["ConnectionMlmRecruiter"]["id"]; 
	                            $data["ConnectionMlmRecruiter"]["cms_user_id"]=$value["ConnectionMlmRecruiter"]["cms_user_id"];
	                            $data["ConnectionMlmRecruiter"]["parent_id"]=$this->master_id;
	                            $data["ConnectionMlmRecruiter"]["mlm_group_id"]=$value["ConnectionMlmRecruiter"]["mlm_group_id"];
	                            $this->ConnectionMlmRecruiter->save($data);	
							}
							else {

								//ulozeni starych spojeni
	          			        $this->ConnectionMlmRecruiter->id = $value["ConnectionMlmRecruiter"]["id"];
								$this->ConnectionMlmRecruiter->saveField('rok_do',$m_rok);
								$this->ConnectionMlmRecruiter->saveField('mesic_do',$m_mesic);
	    
	                            //vytvoreni novyho spojeni              
	                            $this->ConnectionMlmRecruiter->id= null; 
	                            $data["ConnectionMlmRecruiter"]["cms_user_id"]=$value["ConnectionMlmRecruiter"]["cms_user_id"];
	                            $data["ConnectionMlmRecruiter"]["parent_id"]=$this->master_id;
	                            $data["ConnectionMlmRecruiter"]["mlm_group_id"]=$value["ConnectionMlmRecruiter"]["mlm_group_id"];
	                            $data['ConnectionMlmRecruiter']['rok_od']=date('Y');
								$data['ConnectionMlmRecruiter']['mesic_od']=date('m');
	                            $this->ConnectionMlmRecruiter->save($data);	                   
							}
	                    } 
				break;
				case '2' :      
	                $child_list=$this->ConnectionMlmRecruiter->find('all',
						array(
	                    'conditions'=>array('ConnectionMlmRecruiter.kos'=>0,
	                        'ConnectionMlmRecruiter.parent_id '=>$this->data["ConnectionMlmRecruiter"]["cms_user_id"],
	                        'ConnectionMlmRecruiter.mlm_group_id IN (3)',
	                        'ConnectionMlmRecruiter.rok_do'=>'0000',
							'ConnectionMlmRecruiter.mesic_do'=>'00'),
	                    'fields'=>array('ConnectionMlmRecruiter.id','ConnectionMlmRecruiter.cms_user_id','ConnectionMlmRecruiter.mlm_group_id','ConnectionMlmRecruiter.rok_od','ConnectionMlmRecruiter.mesic_od')
	                          )
	                       
					); 
	                 //ulozeni stareho
	    			$this->ConnectionMlmRecruiter->id = $id;
	                $this->ConnectionMlmRecruiter->saveField('rok_do',date('Y'));
					$this->ConnectionMlmRecruiter->saveField('mesic_do', date('m'));
	                unset($id);
	                   
	
						// cyklus na prochazeni child_listu a jejich upravy
	                    foreach($child_list as $val => $value)
	                    {
							$child_rok_od =  $value["ConnectionMlmRecruiter"]["rok_od"];
							$child_mesic_od =  $value["ConnectionMlmRecruiter"]["mesic_od"];

							//pokud je child ze stejneho data tak ho jen updateni
							if($child_rok_od == date('Y') && $child_mesic_od == date('m')){
								$this->ConnectionMlmRecruiter->id = $value["ConnectionMlmRecruiter"]["id"]; 
	                            $data["ConnectionMlmRecruiter"]["cms_user_id"]=$value["ConnectionMlmRecruiter"]["cms_user_id"];
	                            $data["ConnectionMlmRecruiter"]["parent_id"]=$this->master_id;
	                            $data["ConnectionMlmRecruiter"]["mlm_group_id"]=3;
	                            $this->ConnectionMlmRecruiter->save($data);	
							}
							else {

								//ulozeni starych spojeni
	          			        $this->ConnectionMlmRecruiter->id = $value["ConnectionMlmRecruiter"]["id"];
								$this->ConnectionMlmRecruiter->saveField('rok_do',$m_rok);
								$this->ConnectionMlmRecruiter->saveField('mesic_do',$m_mesic);
	    
	                            //vytvoreni novyho spojeni              
	                            $this->ConnectionMlmRecruiter->id= null; 
	                            $data["ConnectionMlmRecruiter"]["cms_user_id"]=$value["ConnectionMlmRecruiter"]["cms_user_id"];
	                            $data["ConnectionMlmRecruiter"]["parent_id"]=$this->master_id;
	                            $data["ConnectionMlmRecruiter"]["mlm_group_id"]=3;
	                            $data['ConnectionMlmRecruiter']['rok_od']=date('Y');
								$data['ConnectionMlmRecruiter']['mesic_od']=date('m');
	                            $this->ConnectionMlmRecruiter->save($data);	                   
							}
	                    } 
               
				break;
				case '3' : 
					$this->ConnectionMlmRecruiter->id = $id;
					$this->ConnectionMlmRecruiter->saveField('rok_do',date('Y'));
					$this->ConnectionMlmRecruiter->saveField('mesic_do', date('m'));
				break;
          }

		} 	
		die();
	}
		
	// pro testovani 
	function kresli(){
		  
      $pole=$this->ConnectionMlmRecruiter->query("SELECT `Recruiter`.`name`, `ParentUser`.`name`, `ConnectionMlmRecruiter`.`id`, `ConnectionMlmRecruiter`.`closed`, `ConnectionMlmRecruiter`.`cms_user_id`, `ConnectionMlmRecruiter`.`parent_id`, `ConnectionMlmRecruiter`.`mlm_group_id`, `ConnectionMlmRecruiter`.`updated`, `ConnectionMlmRecruiter`.`created` FROM `wapis__connection_mlm_recruiters` AS `ConnectionMlmRecruiter` LEFT JOIN `wapis__cms_users` AS `Recruiter` ON (`ConnectionMlmRecruiter`.`cms_user_id` = `Recruiter`.`id`) LEFT JOIN `wapis__cms_users` AS `ParentUser` ON (`ConnectionMlmRecruiter`.`parent_id` = `ParentUser`.`id`) WHERE `ConnectionMlmRecruiter`.`kos` = 0 ORDER BY `ConnectionMlmRecruiter`.`mlm_group_id` ASC,`ConnectionMlmRecruiter`.`parent_id` ASC LIMIT 20");
      $neco = array();
      foreach($pole as $item){
        $neco[]=array(
           'id'=>$item["ConnectionMlmRecruiter"]["cms_user_id"],
           'conId'=>$item["ConnectionMlmRecruiter"]["id"],
           'parent_id'=>$item["ConnectionMlmRecruiter"]["parent_id"],
           'name'=>$item["Recruiter"]["name"],
           'level'=>$item["ConnectionMlmRecruiter"]["mlm_group_id"]
        );
      }
      //pr($neco);
      $this->set('neco',json_encode($neco));
      
    }
}
?>