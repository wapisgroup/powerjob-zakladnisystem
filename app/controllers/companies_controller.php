<?php
if($_SERVER['REMOTE_ADDR'] == '90.176.43.89'){
    Configure::write('debug',2);
}
//Configure::write('debug',1);
	define('fields','CompanyView.*,
			( 
			  SELECT 
			   COUNT(client_id)
			  FROM wapis__connection_client_requirements
			  where type=2 and company_id=CompanyView.id and(
				`from` <= "'. date('Y-m-d').'" AND (`to`>="'. date('Y-m-d').'" OR `to`="0000-00-00")
			  ) 
			) as poc_zamestnancu
		');
class CompaniesController extends AppController {
	var $name = 'Companies';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput','Wysiwyg');
	var $components = array('ViewIndex','RequestHandler','Upload','Email');
	var $uses = array('CompanyView','Company');
	var $renderSetting = array(
		//'SQLfields' => array('id','Company.name','updated','created','status','SalesManager.name','ClientManager.name','Coordinator.name','mesto','SettingStav.namre'),
		'SQLfields' => array(fields),
		// 'bindModel' => array('belongsTo' => array(
				// 'SalesManager' => array('className' => 'CmsUser', 'foreignKey' => 'self_manager_id'),
				// 'ClientManager' => array('className' => 'CmsUser', 'foreignKey' => 'client_manager_id'),
				// 'Coordinator' => array('className' => 'CmsUser', 'foreignKey' => 'coordinator_id'),
				// 'SettingParentCompany' => array('className' => 'SettingParentCompany', 'foreignKey' => 'parent_id'),
		// )),
		'controller'=> 'companies',
		'page_caption'=>'Seznam zakázek - personální leasing',
		'sortBy'=>'CompanyView.name.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat firmu|add',
            'convert_users'		=>	'Převod uživatelů|convert_users|Převod uživatelů ve firmách|convert_users',
            'export_excel' => 'Export Excel|export_excel|Export Excel|export_excel',  
		),
		'filtration' => array(
			'CompanyView-name'				=>	'text|Název firmy|',
			'CompanyView-mesto|'			=>	'text|Město|',
			'CompanyView-setting_stav_id'	=>	'select|Stav|setting_stav_list',
			'CompanyView-self_manager_id'	=>	'select|SM|self_manager_list',
			'CompanyView-client_manager_id|coordinator_id|coordinator_id2-cmkoo'		=>	'select|CM/KOO|cm_koo_list',
            'CompanyView-ico|'				=>	'text|IČO|',
            'CompanyView-parent_id'	=>	'select|Nadřazená f.|parent_company_list',		 	
            'CompanyView-manazer_realizace_id'	=>	'select|Manažer real.|manager_realizace_list',
            'CompanyView-vip'				=>	'checkbox|VIP|',		 	
		),
		'items' => array(
			'id'					=>	'ID|CompanyView|id|text|',
			'ico'					=>	'IČO|CompanyView|ico|text|',
			'name'					=>	'Podniky(zakazky)|CompanyView|name|text|',
			'mesto'					=>	'Město|CompanyView|mesto|text|',
			'sm_id' 		        =>	'SalesManager|CompanyView|sales_manager|text|',
			'cam_id' 		        =>	'Manažér realizace|CompanyView|company_manager|text|',
			'cm_id'		            =>	'Koordinátor|CompanyView|client_manager|text|',
			'coordinator'		    =>	'Koordinátor2|CompanyView|coordinator|text|',
			'coordinator2'		    =>	'Koordinátor3|CompanyView|coordinator2|text|',
			'poc_zamestnancu'		=>	'Počet zaměstanců|0|poc_zamestnancu|text|',
            'int_zam'		=>	'Int. zaměstanců|CompanyView|internal_employee|text|',
            'nad_firma'		        =>	'Nadr.Firma|CompanyView|nadrazena_spol|text|',
			'stav'					=>	'Stav|CompanyView|stav|text|'
			//'pocet_zamestnancu'		=>	'Zaměst.|CompanyView|pocet_zamestnancu|text|'
		),
		'posibility' => array(
			//'status'	=> 	'status|Změna stavu|status',
			'edit'		=>	'edit|Editace položky|edit',			
			'kontakty'	=>	'kontakty|Kontakty|kontakty',		
			'reports'	=>	'aktivity|Aktivity|aktivity',
			'tasks'		=>	'tasks|Úkoly|tasks',
			'attach'	=>	'attachs|Přílohy|attach',
			'pozice'	=>	'pozice|Definice profesí a kalkulace|definice',
			'fakturace'	=>	'fakturacka|Vystavené faktury a úhrady|faktury',
			'template'	=>	'template|Šablony požadavků pro nábor|template',
			'order'		=>	'order|Objednávky|order',
			'atw_order'		=>	'atw_order|ATW - Volné pozice|atw_order',
			'form_template'		=>	'form_template|Šablony formulářu|form_template',
            'client_working_template'		=>	'client_working_template|Šablony docházek|client_working_template',
			'delete'	=>	'trash|Odstranit položku|trash',
		),
		'domwin_setting' => array(
			'sizes' 		=> '[800,900]',
			'scrollbars'	=> true,
			'languages'		=> true,
			'defined_lang'	=> "['cz','en','de']",
		),
        'group_condition'=>array(
           array('id'=>33,'condition'=>'dy_prilezitost = 1'),
           array('id'=>array(66,21,65,67,34,69),'condition'=>'CompanyView.id = 359')
        )
	);
	var  $shift_type_list = array(1=>'Ranní', 2=>'Odpolední', 3=>'Noční');



	/**
 	* Returns a view, if not AJAX, load data for basic view. 
 	*
	* @param none
 	* @return view
 	* @access public
	**/
	function index(){
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			// set JS and Style
			$this->set('scripts',array('uploader/uploader','clearbox/clearbox','wysiwyg_old/wswg'));
			$this->set('styles',array('../js/uploader/uploader','../js/clearbox/clearbox','../js/wysiwyg_old/wswg'));
			// set FastLinks
			$this->set('fastlinks',array('ATEP'=>'/','Firmy'=>'#','Seznam firem'=>'#'));
			// load stavy zadosti for filter
			$this->loadModel('SettingStav'); $this->SettingStav = new SettingStav();
			$this->set('setting_stav_list',$this->SettingStav->find('list',array('conditions'=>array('SettingStav.status'=>1,'SettingStav.kos'=>0))));
			unset($this->SettingStav);

					
			// load list for filter SM, KO, CM
			$this->loadModel('CmsUser');
			
			//$this->set('self_manager_list', $this->CmsUser->find('list', array('conditions'=>array('cms_group_id'=>array(2,52,84),'kos'=>0))));
			$this->set('self_manager_list', $this->get_sm());

			$this->set('manager_realizace_list', $this->CmsUser->find('list', array('conditions'=>array('cms_group_id'=>18,'kos'=>0))));
			
    		$this->set('cm_koo_list',$this->get_list_of_superrior(2));
            
            //list nadrazených spolecnosti
            $this->set('parent_company_list',$this->get_list('SettingParentCompany'));
			
			
			$this->render('../system/index');
		}
	}
	
	/**
 	* Returns a view if empty this->data, ales JSON result of save
 	*
	* @param $id The number of ID column
 	* @return view / JSON
 	* @access public
	**/
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			
			// load cms user
			$this->loadModel('CmsUser');
			$this->set('permission',$permision = $this->logged_user['CmsGroup']['permission']['companies']);
			
			$group = $this->logged_user['CmsGroup']['id'];
			
			
			if (($group == 2 || $group == 52)) { // sales manager
				$this->set('coordinator_list',$this->get_list_of_superrior(2));
				$this->set('client_manager_list',$this->get_list_of_superrior(2));
            }
            			
			if ($group == 4) { // coordinator
				$this->set('self_manager_list',$this->get_sm());
				$this->set('client_manager_list',$this->get_list_of_superrior(2));
			}
                
			if ($group == 1 || $group == 5 || $group == 18) { // admin
				$this->set('coordinator_list',$this->get_list_of_superrior(2));
                $this->set('self_manager_list',$this->get_sm());
				$this->set('client_manager_list',$this->get_list_of_superrior(2));
                $this->set('manazer_realizace_list',$this->CmsUser->find('list',array('conditions'=>array('CmsUser.status'=>1,'CmsUser.kos'=>0,'CmsUser.cms_group_id'=>18))));
        	    $this->set(
                    'asistent_obchodu_list',
                    $this->CmsUser->find(
                        'list',
                        array(
                            'conditions'=>array(
                                'CmsUser.status'=>1,
                                'CmsUser.kos'=>0,
                                'OR' => array(
                                    'CmsUser.cms_group_id'=>26,
                                    'CmsUser.id' => 553
                                )
                            )
                        )
                    )
                );
  
            }
            else if($permision['choose_coordinator'] == true  || $permision['choose_client_manager'] == true ){
                $this->set('coordinator_list',$this->get_list_of_superrior(2));
                $this->set('client_manager_list',$this->get_list_of_superrior(2));
            } 
            if($permision['choose_self_manager'] == true)
             	$this->set('self_manager_list',$this->get_sm());
            
            if(isset($permision['choose_asistent_obchodu']) && $permision['choose_asistent_obchodu'] == true)
                $this->set(
                    'asistent_obchodu_list',
                    $this->CmsUser->find(
                        'list',
                        array(
                            'conditions'=>array(
                                'CmsUser.status'=>1,
                                'CmsUser.kos'=>0,
                                'OR' => array(
                                    'CmsUser.cms_group_id'=>26,
                                    'CmsUser.id' => 553
                                )
                            )
                        )
                    )
                );
            if(isset($permision['choose_manazer_realizace']) && $permision['choose_manazer_realizace'] == true)
           	    $this->set('manazer_realizace_list',$this->CmsUser->find('list',array('conditions'=>array('CmsUser.status'=>1,'CmsUser.kos'=>0,'CmsUser.cms_group_id'=>18))));
        	  
            
			/**
			 * konzultanti z W db
			 */
            $this->set('konzultant_list',$this->CmsUser->konzultant_list());

            /**
             * Konuzltanti DY ze skupiny atepu id - 33
             */
            $this->set('konzultant_dy_list',$this->get_list('CmsUser',array('cms_group_id'=>33))); 
            
            //A&T firmy
            $this->set('at_company_list',$this->get_list('AtCompany')); 
            //Průmysl
            $this->set('industry_list',$this->get_list('IndustryList')); 
            
			// load stat list
			$this->loadModel('SettingStat');
			$this->set('company_stat_list',$this->SettingStat->find('list',array('conditions'=>array('SettingStat.status'=>1,'SettingStat.kos'=>0))));
			unset($this->SettingStat);
						
			// load parent company
			$this->loadModel('SettingParentCompany'); $this->SettingParentCompany = new SettingParentCompany();
			$this->set('setting_parent_company_list',$this->SettingParentCompany->find('list',array('conditions'=>array('SettingParentCompany.status'=>1,'SettingParentCompany.kos'=>0))));
			unset($this->SettingParentCompany );
			
			// load stavy zadosti
			$this->loadModel('SettingStav'); $this->SettingStav = new SettingStav();
			$this->set('setting_stav_list',$this->SettingStav->find('list',array('conditions'=>array('SettingStav.status'=>1,'SettingStav.kos'=>0))));
			unset($this->SettingStav);
			
			
			if ($id != null){
				$this->Company->bindModel(array('belongsTo' => array(
					'ClientManager' => array('className' 	=> 'CmsUser', 'foreignKey' 	=> 'client_manager_id'),
					'Coordinator' 	=> array('className' 	=> 'CmsUser', 'foreignKey' 	=> 'coordinator_id'),
					'SalesManager' 	=> array('className' 	=> 'CmsUser', 'foreignKey' 	=> 'self_manager_id'),
                    'AsistentObchodu' 	=> array('className' 	=> 'CmsUser', 'foreignKey' 	=> 'asistent_obchodu_id'),
                    'ManzarRealizace' 	=> array('className' 	=> 'CmsUser', 'foreignKey' 	=> 'manazer_realizace_id')
				)));
				$this->data = $this->Company->read(null,$id);
				
				// load province list 
                $this->set('province_list',self::load_province($this->data['Company']['stat_id'],true));
                
               	// load country list
                if($this->data['Company']['stat_id'] == 3 || $this->data['Company']['province_id'] == -1){
                    $this->set('country_list',array(-1=>'Nezadáno'));
                }
                else if (isset($this->data['Company']['province_id']) && !empty($this->data['Company']['province_id'])){
				    $this->loadModel('Countrie');
                    $country_list = $this->Countrie->find('list',array('conditions'=>array('province_id'=>$this->data['Company']['province_id'])));
                    $country_list[-1] = 'Nezadáno';
				    $this->set('country_list', $country_list);
				    unset($this->Province );
                }
                
                $this->loadModel('CompanyCoordinator');
                $this->CompanyCoordinator->bindModel(array(
                    'joinSpec'=>array('CmsUser'=>array(
                        'className'=>'CmsUser',
                        'primaryKey'=>'CmsUser.id',
                        'foreignKey'=>'CompanyCoordinator.cms_user_id'
                    ))
                ));
			    $this->set('coordinators_list',$rrr= $this->CompanyCoordinator->find('all',array(
                    'conditions'=>array('CompanyCoordinator.kos'=>0,'CompanyCoordinator.company_id'=>$id),
                    'fields'=>array('CompanyCoordinator.*','CmsUser.name')
                )));
                unset($this->CompanyCoordinator);
		
				
			} else {
				$this->loadModel('Province');
				$this->set('province_list',$this->Province->find('list',array('conditions'=>array('stat_id'=>1))));
				if (($group == 2 || $group == 52)) { // sales manager
					$this->data['SalesManager']['name'] = $this->logged_user['CmsUser']['name'];
				}
				if ($group == 4) { // coordinator
					$this->data['Coordinator']['name'] = $this->logged_user['CmsUser']['name'];
				}
			
			}
            
        
            
			unset($this->CmsUser );
			$this->render('edit');
		} else {
			$group = $this->logged_user['CmsGroup']['id'];
			
			if (empty($this->data['Company']['id']) && ($group == 2 || $group == 52))
				$this->data['Company']['self_manager_id'] = $this->logged_user['CmsUser']['id'];

			if (empty($this->data['Company']['id']) && $group == 4)
				$this->data['Company']['coordinator_id'] = $this->logged_user['CmsUser']['id'];

            if (empty($this->data['Company']['id']) && empty($this->data['Company']['asistent_obchodu_id'])){
                $this->data['Company']['asistent_obchodu_id'] = 553;
            }
            $koo = array(
                   'client_manager_id'  => 'Koordinátor 1',
                   'coordinator_id'     => 'Koordinátor 2',
                   'coordinator_id2'    => 'Koordinátor 3',
            );

            if (!empty($this->data['Company']['id'])){
                $old_data = $this->Company->read(array_keys($koo),$this->data['Company']['id']);

                foreach($koo as $ki => $kcap){
                    if (empty($old_data['Company'][$ki]) && !empty($this->data['Company'][$ki])){
                        $this->koordinator_change_activity($this->data['Company']['id'],$kcap, 11,$this->data['Company'][$ki]);
                    } else if (!empty($old_data['Company'][$ki]) && empty($this->data['Company'][$ki])){
                        $this->koordinator_change_activity($this->data['Company']['id'],$kcap, 12,$old_data['Company'][$ki]);
                    } else if (!empty($old_data['Company'][$ki]) && !empty($this->data['Company'][$ki]) && $this->data['Company'][$ki] != $old_data['Company'][$ki]){
                        $this->koordinator_change_activity($this->data['Company']['id'],$kcap, 11,$this->data['Company'][$ki]);
                        $this->koordinator_change_activity($this->data['Company']['id'],$kcap, 12,$old_data['Company'][$ki]);
                    }
                }
            }

			$this->Company->save($this->data);
			
			// Odeslani emailu
			if (empty($this->data['Company']['id'])){
				
				$replace_list = array(
					'##Company.name##' 	=>$this->data['Company']['name'],
					'##CmsUser.name##' 	=>$this->logged_user['CmsUser']['name']
				);
				
				$this->Email->send_from_template_new(2,array(),$replace_list);
			}
			die();
		}
	}

    /**
     * Zaznam o zmene koordinatora, vytvori se nova aktivita
     * @param $company_id INT
     * @param $col string, [client_manager_id, coordinator_id, coordinator_id2]
     * @param $type INT; 11 = add, 12 = remove
     */
    function koordinator_change_activity($company_id, $col, $type = null, $coo_id){
        $this->loadModel('CmsUser');
        $cmsuser = $this->CmsUser->read('name',$coo_id);
        if ($type !== null){
            $to_save = array(
                'name' => (($type==11)?'Přidán':'Odebrán').' '.($col).' '.$cmsuser['CmsUser']['name'],
                'activity_datetime'=>date('Y-m-d H:i:s'),
                'company_activity_type_id'=>$type,
                'cms_user_id'=> $this->logged_user['CmsUser']['id'],
                'company_id'=>$company_id
            );
            $this->loadModel('CompanyActivity');
            $this->CompanyActivity->save($to_save);
            $this->CompanyActivity->id  = null;
            
            $this->Company->bindModel(array('belongsTo'=>array('IndustryList')));
		    $company = $this->Company->read(am($this->Company->company_data_for_email_notification,array('IndustryList.name')),$company_id);
	
    
            $this->loadModel('ConnectionClientRequirement');
            $counter_zam = 0;
            
            //leasing zam
            $pocet_zam = $this->ConnectionClientRequirement->find('first',array(
                'conditions'=>array(
                    'ConnectionClientRequirement.company_id'=>$company_id,
                    'ConnectionClientRequirement.kos'=>0,
                    'ConnectionClientRequirement.new'=>0,
                    'ConnectionClientRequirement.type'=>2,
                    'ConnectionClientRequirement.from <= '=>date('Y-m-d'),
                    'OR'=>array(
                        'ConnectionClientRequirement.to'=>'0000-00-00',
                        'ConnectionClientRequirement.to >= '=>date('Y-m-d')
                    )
                ),
                'fields'=>array(
                    'COUNT(ConnectionClientRequirement.id) as count'
                ),
                'group'=>'ConnectionClientRequirement.company_id'
            ));
            $counter_zam += $pocet_zam[0]['count'];
            
            //nove dochazky
            $pocet_zam_new = $this->ConnectionClientRequirement->find('first',array(
                'conditions'=>array(
                    'ConnectionClientRequirement.company_id'=>$company_id,
                    'ConnectionClientRequirement.kos'=>0,
                    'ConnectionClientRequirement.new'=>1,
                    'ConnectionClientRequirement.type'=>2,
                    'ConnectionClientRequirement.from <= '=>date('Y-m-d'),
                    'OR'=>array(
                        'ConnectionClientRequirement.to'=>'0000-00-00',
                        'ConnectionClientRequirement.to >= '=>date('Y-m-d')
                    )
                ),
                'fields'=>array(
                    'COUNT(ConnectionClientRequirement.id) as count',
                ),
                'group'=>'ConnectionClientRequirement.company_id'
            ));
            
           $counter_zam += $pocet_zam_new[0]['count'];
  
            
            $replace_list = array(
                '##Company.name##' 	=>$company['Company']['name'],
                '##Company.adresa##' =>$company['Company']['ulice'].' '.$company['Company']['mesto'].', '.$company['Company']['psc'],
                '##IndustryList.name##' =>$company['IndustryList']['name'],
                '##Company.pocet_zamestnancu##' =>$counter_zam,
                '##Company.internal_employee##' =>$company['Company']['internal_employee'],
                '##CmsUser.name##' 	=>$this->logged_user['CmsUser']['name'],
                '##akce##'=> 'byl '.(($type==11)?'přidán':'odebrán').' koordinátor '.$cmsuser['CmsUser']['name']
    		);
            $this->Email->set_company_data($company['Company']);
            $this->Email->send_from_template_new(52,array($this->logged_user['CmsUser']['email']),$replace_list);
            
            
            return true;
        } else
            return false;
    }
    
    function load_for_email($id,$model,$fields = null){
        if(!isset($this->{$model}))
            $this->loadModel($model);
        
        return $this->{$model}->read($fields,$id);   
    }
   
    
	/**
 	* Zobrazi seznam aktivit
 	*
	* @param $company_id
 	* @return view
 	* @access public
	**/

	function aktivity($company_id){
		$this->autoLayout = false;

        $this->set('logged_group',$this->logged_user['CmsGroup']['id']);
		$this->set('permission',$permision = $this->logged_user['CmsGroup']['permission'][$this->renderSetting['controller']]);
			
		$condition = array('CompanyActivity.company_id'=>$company_id,'CompanyActivity.kos'=>0);
		
		if ($this->logged_user['CmsGroup']['permission']['companies']['aktivity']== 2)
			$condition['cms_user_id'] = $this->logged_user['CmsUser']['id'];
		
		$this->loadModel('CompanyActivity');
		$this->CompanyActivity->bindModel(array('belongsTo'=>array('CompanyContact','CmsUser','Company','SettingActivityType'=>array('foreignKey'=>'company_activity_type_id'))));
        $this->set('contact_list',$c = $this->CompanyActivity->findAll($condition, null, 'CompanyActivity.id DESC',300));
		$this->set('company_id',$company_id);
		unset($this->CompanyActivity);
		$this->render('aktivity/index');
	}
	
	/**
 	* Editace aktivit
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function aktivity_edit($company_id = null, $id = null){
		$this->autoLayout = false;


		// START nacteni  zakladnich modelu pro editaci
		$this->loadModel('CompanyActivity'); $this->CompanyActivity = new CompanyActivity();
        $this->loadModel('CompanyExtendedActivity');
		$this->loadModel('CompanyTask'); $this->CompanyTask = new CompanyTask();
		// END nacteni  zakladnich modelu pro editaci
		if (empty($this->data)){			
			// START nacteni  zakladnich modelu a dat pro vyctove listy
			$this->loadModel('CompanyContact'); $this->CompanyContact = new CompanyContact();
			$this->loadModel('SettingActivityType'); $this->SettingActivityType = new SettingActivityType();
			$this->loadModel('SettingTaskType'); $this->SettingTaskType = new SettingTaskType();
            $this->loadModel('CmsUser');

            $cms_user = $this->CmsUser->find('first', array( 'fields'=>array('cms_group_id','id'),'conditions'=>array('CmsUser.id'=>$this->logged_user['CmsUser'])));
            $this->set('cms_user',$cms_user['CmsUser']['cms_group_id']);
/*
            $allowed_groups_mandatory = array(4);
            if( in_array($cms_user['CmsUser']['cms_group_id'], $allowed_groups_mandatory )){ $allow = true; }
            else{ $allow = false; }

            $this->set('allow_user_group_mandatory',$allow);*/

            $allowed_groupsR = array(1);
            if( in_array($cms_user['CmsUser']['cms_group_id'], $allowed_groupsR )){ $allowR = true; }
            else{ $allowR = false; }
            $this->set('allow_user_group_report',$allowR);


            $this->set('shift_type_list', $this->shift_type_list);

            $this->set('comapany_contact_list',$this->CompanyContact->find('list',array('conditions'=>array('CompanyContact.kos'=>0,'CompanyContact.company_id'=>$company_id))));

            if( $cms_user['CmsUser']['cms_group_id'] == 4 || $cms_user['CmsUser']['cms_group_id'] == 18){ // Koordinator 4, Manazer realizace 18
                $this->set('company_activity_type_list', $this->SettingActivityType->find('list',array('conditions'=>array('SettingActivityType.kos'=>0,'SettingActivityType.status'=>1, 'SettingActivityType.cms_user_group_id'=>$cms_user['CmsUser']['cms_group_id']), 'order'=>'poradi ASC')));
            }else if( $cms_user['CmsUser']['cms_group_id'] == 1 ){
                $this->set('company_activity_type_list', $this->SettingActivityType->find('list',array('conditions'=>array('SettingActivityType.kos'=>0,'SettingActivityType.status'=>1), 'order'=>'poradi ASC')));
            }else{
                $this->set('company_activity_type_list', $this->SettingActivityType->find('list',array('conditions'=>array('SettingActivityType.id'=>16), 'order'=>'poradi ASC')));
            }



			$this->set('company_task_type_list', $this->SettingTaskType->find('list',array('conditions'=>array('kos'=>0), 'order'=>'poradi ASC')));

            $extended = array();
            if($id){
               $ext = $this->CompanyExtendedActivity->find('all',array('conditions'=>array('CompanyExtendedActivity.company_activity_id'=>$id),'fields'=>array('id','atribut','value')));
               foreach($ext as $item){
                   $parseAtr = explode('|' ,$item['CompanyExtendedActivity']['atribut']);
                   if(Count($parseAtr) >1 ){
                       if($parseAtr[0] > 3){
                           $extended[$parseAtr[1]][$parseAtr[0]]['value'] = $item['CompanyExtendedActivity']['value'];
                           $extended[$parseAtr[1]][$parseAtr[0]]['id'] = $item['CompanyExtendedActivity']['id'];
                       }
                   }
                   $extended[$item['CompanyExtendedActivity']['atribut']]['value'] = $item['CompanyExtendedActivity']['value'];
                   $extended[$item['CompanyExtendedActivity']['atribut']]['id'] = $item['CompanyExtendedActivity']['id'];

               }

            }
            $this->set('extended',$extended);
            unset($this->CompanyContact);
			unset($this->SettingActivityType);
			unset($this->SettingTaskType);
			// END nacteni  zakladnich modelu a dat pro vyctove listy

			$this->data['CompanyActivity']['company_id'] = $company_id;
			$this->data['CompanyTask']['company_id'] = $company_id;

			if ($id != null){
				$this->CompanyActivity->bindModel(array('hasOne'=>array('CompanyTask')));
				$this->data = am($this->data, $this->CompanyActivity->read(null,$id));
			} else {
				$cmpn = $this->Company->read(array('mesto'),$company_id);
				$this->data['CompanyActivity']['mesto'] = $cmpn['Company']['mesto'];
			}
			$this->render('aktivity/edit');
		} else {
			// START pridani cms_user_id pokud se vytvari aktivita nebo ukol
			if (empty($this->data['CompanyActivity']['id'])) 	$this->data['CompanyActivity']['cms_user_id'] 	= $this->logged_user['CmsUser']['id'];
			if (empty($this->data['CompanyTask']['id'])) 		$this->data['CompanyTask']['cms_user_id'] 		= $this->logged_user['CmsUser']['id'];
			// END pridani cms_user_id pokud se vytvari aktivita nebo ukol
			// ulozeni aktivity
			$this->CompanyActivity->save($this->data);
			// START ulozeni ukolu, pokud je k dane aktivite nadefinovan
			if (!empty($this->data['CompanyTask']['name'])){
				$this->data['CompanyTask']['company_activity_id'] = $this->CompanyActivity->id;
				$this->CompanyTask->save($this->data);
			}
            $extended = array();
            if(Count($this->data['Extended']) > 0){
                foreach($this->data['Extended'] as $atribut=>$value){
                    if($value != ''){
                        if(is_array($value)){
                                foreach($value as $atr=>$val){
                                    if($val != '' ){
                                        if(!empty($this->data['Ids'][$atribut.'|'.$atr])){
                                            $extended['CompanyExtendedActivity']['id'] = $this->data['Ids'][$atribut.'|'.$atr];
                                        }
                                        $extended['CompanyExtendedActivity']['value'] = $val;
                                        $extended['CompanyExtendedActivity']['atribut'] = $atribut.'|'.$atr;
                                        $extended['CompanyExtendedActivity']['company_activity_id'] = $this->CompanyActivity->id;
                                        $this->CompanyExtendedActivity->save($extended);
                                        $this->CompanyExtendedActivity->id = null;
                                    }else{
                                        if(!empty($this->data['Ids'][$atribut.'|'.$atr])){
                                            $extended['CompanyExtendedActivity']['id'] = $this->data['Ids'][$atribut.'|'.$atr];
                                            $extended['CompanyExtendedActivity']['value'] = '';
                                            $extended['CompanyExtendedActivity']['atribut'] = $atribut.'|'.$atr;
                                            $extended['CompanyExtendedActivity']['company_activity_id'] = $this->CompanyActivity->id;
                                            $this->CompanyExtendedActivity->save($extended);
                                            $this->CompanyExtendedActivity->id = null;
                                        }
                                    }
                                }
                        }else{
                            if(!empty($this->data['Ids'][$atribut])){
                                $extended['CompanyExtendedActivity']['id'] = $this->data['Ids'][$atribut];
                            }
                            $extended['CompanyExtendedActivity']['value'] = $value;
                            $extended['CompanyExtendedActivity']['atribut'] = $atribut;
                            $extended['CompanyExtendedActivity']['company_activity_id'] = $this->CompanyActivity->id;
                            $this->CompanyExtendedActivity->save($extended);
                            $this->CompanyExtendedActivity->id = null;
                        }
                    }else{
                            if(!empty($this->data['Ids'][$atribut])){
                                $extended['CompanyExtendedActivity']['id'] = $this->data['Ids'][$atribut];
                                $extended['CompanyExtendedActivity']['value'] = '';
                                $extended['CompanyExtendedActivity']['atribut'] = $atribut;
                                $extended['CompanyExtendedActivity']['company_activity_id'] = $this->CompanyActivity->id;
                                $this->CompanyExtendedActivity->save($extended);
                                $this->CompanyExtendedActivity->id = null;
                            }
                    }
                }

            }
			// END ulozeni ukolu, pokud je k dane aktivite nadefinovan


            if(!empty($this->data['DeleteExtends'])){
                $ids = explode('|',$this->data['DeleteExtends']);
                foreach($ids as $id){
                    if(intval($id) > 0){
                        $this->CompanyExtendedActivity->delete(intval($id));
                    }
                }
            }
			// odeslani informacniho emailu ze je nova aktivita
			if (empty($this->data['CompanyActivity']['id'])){
				$this->send_email_to_directors_about_new_activity();
			}

			// reload seznamu aktivit
			$this->aktivity($this->data['CompanyActivity']['company_id']);

		}
		unset($this->CompanyActivity);
	}

	/**
 	* Odeslani direktorum mail o nove aktivite ze systemu
 	*
 	* @return true
 	* @access public
	**/
	function send_email_to_directors_about_new_activity(){

		$this->loadModel('Company');
        $this->Company->bindModel(array(
            'belongsTo'=>array('IndustryList')
        ));
		$company = $this->Company->read(am($this->Company->company_data_for_email_notification,array('IndustryList.name','Company.self_manager_id')),$this->data['CompanyActivity']['company_id']);
		unset($this->Company);

		$this->loadModel('CompanyContact');
		$company_contact = $this->CompanyContact->read(array('name','position'),$this->data['CompanyActivity']['company_contact_id']);
		unset($this->CompanyContact);


        $this->loadModel('ConnectionClientRequirement');
        $counter_zam = 0;
        $text_odmeny = null;

            $this->ConnectionClientRequirement->bindModel(array(
                'belongsTo'=>array('CompanyMoneyItem')
            ));

            //leasing zam
            $pocet_zam = $this->ConnectionClientRequirement->find('all',array(
                'conditions'=>array(
                    'ConnectionClientRequirement.company_id'=>$this->data['CompanyActivity']['company_id'],
                    'ConnectionClientRequirement.kos'=>0,
                    'ConnectionClientRequirement.new'=>0,
                    'ConnectionClientRequirement.type'=>2,
                    'ConnectionClientRequirement.from <= '=>date('Y-m-d'),
                    'OR'=>array(
                        'ConnectionClientRequirement.to'=>'0000-00-00',
                        'ConnectionClientRequirement.to >= '=>date('Y-m-d')
                    )
                ),
                'fields'=>array(
                    'COUNT(CompanyMoneyItem.id) as count',
                    'CompanyMoneyItem.id','CompanyMoneyItem.pausal','CompanyMoneyItem.checkbox_pracovni_smlouva',
                    'CompanyMoneyItem.checkbox_dohoda','CompanyMoneyItem.checkbox_faktura','CompanyMoneyItem.checkbox_cash',
                    'CompanyMoneyItem.doprava',
                    'CompanyMoneyItem.cena_ubytovani_na_mesic',
                ),
                'group'=>'CompanyMoneyItem.id'
            ));

            $_odmeny = array();
            foreach($pocet_zam as $zam){
                $name = self::generate_work_money_name($zam);
                $dop_ = ($zam['CompanyMoneyItem']['doprava'] == 0 || $zam['CompanyMoneyItem']['doprava'] == ''?0:1);
                $zam_ = ($zam['CompanyMoneyItem']['cena_ubytovani_na_mesic'] == 0 || $zam['CompanyMoneyItem']['cena_ubytovani_na_mesic'] == ''?0:1);
                $key = $name.'|'.$dop_.'|'.$zam_;

                if(!array_key_exists($key,$_odmeny))
                    $_odmeny[$key] = $zam[0]['count'];
                else
                    $_odmeny[$key] += $zam[0]['count'];

                $counter_zam += $zam[0]['count'];
            }

            //nove dochazky
            $this->ConnectionClientRequirement->bindModel(array(
                'belongsTo'=>array('CompanyWorkPosition')
            ));
            $pocet_zam_new = $this->ConnectionClientRequirement->find('all',array(
                'conditions'=>array(
                    'ConnectionClientRequirement.company_id'=>$this->data['CompanyActivity']['company_id'],
                    'ConnectionClientRequirement.kos'=>0,
                    'ConnectionClientRequirement.new'=>1,
                    'ConnectionClientRequirement.type'=>2,
                    'ConnectionClientRequirement.from <= '=>date('Y-m-d'),
                    'OR'=>array(
                        'ConnectionClientRequirement.to'=>'0000-00-00',
                        'ConnectionClientRequirement.to >= '=>date('Y-m-d')
                    )
                ),
                'fields'=>array(
                    'COUNT(ConnectionClientRequirement.company_work_position_id) as count',
                    'CompanyWorkPosition.name'
                ),
                'group'=>'ConnectionClientRequirement.company_work_position_id'
            ));

            $_odmeny_new = array();
            foreach($pocet_zam_new as $zam){
                $key = $zam['CompanyWorkPosition']['name'];

                if(!array_key_exists($key,$_odmeny))
                    $_odmeny_new[$key] = $zam[0]['count'];
                else
                    $_odmeny_new[$key] += $zam[0]['count'];

                $counter_zam += $zam[0]['count'];
            }

            App::import('Helper', 'Fastest');
            $loadHelper = 'FastestHelper';
            $fastest = new $loadHelper();
            foreach($_odmeny as $key=>$count){
                list($name,$doprava,$ubytovani) = explode('|',$key);
                $text_odmeny .= 'Z toho: '.$fastest->made_forma_odmeny($name,$doprava,$ubytovani).' : '.$count.'<br />';
            }

            foreach($_odmeny_new as $key=>$count){
                $text_odmeny .= 'Nové formy odměny, '.$key.' : '.$count.'<br />';
            }

            $pocet_zam = $counter_zam;

        //objednavky
        $this->loadModel('CompanyOrderItem');
        $text_objednavky = null;
        $objednavky_podniku = $this->CompanyOrderItem->find('all',array(
                'conditions'=>array(
                    'CompanyOrderItem.company_id'=>$this->data['CompanyActivity']['company_id'],
                    'CompanyOrderItem.order_status'=>1,
                    'CompanyOrderItem.kos'=>0,
                ),
                'fields'=>array(
                   'CompanyOrderItem.name,
                    CONCAT_WS(" + ",CompanyOrderItem.count_of_free_position,CompanyOrderItem.count_of_substitute) as objednavka,
                    ((SELECT COUNT(c.id)
                             FROM wapis__connection_client_requirements as c
                             Where
                                c.company_id = CompanyOrderItem.company_id AND
                                c.requirements_for_recruitment_id = CompanyOrderItem.id AND
                                objednavka=1 and type = 1 and c.kos = 0
                             )
                    ) as pocet_na_cl'
                ),
                'order'=>'CompanyOrderItem.order_date DESC'
            ));
         foreach($objednavky_podniku as $obj){
            $text_objednavky .= $obj['CompanyOrderItem']['name'].' -> '.$obj[0]['objednavka'].' ( na ČL '.$obj[0]['pocet_na_cl'].')<br />';
         }

		$replace_list = array(
			'##Company.name##' 			=>$company['Company']['name'],
            '##IndustryList.name##' 		=>(isset($company['IndustryList']['name'])?$company['IndustryList']['name']:''),
            '##Company.pocet_zamestnancu##' =>$pocet_zam,
            '##Company.internal_employee##' =>$company['Company']['internal_employee'],
            '##Company.mesto##' 			=>$company['Company']['mesto'],
            '##Company.adresa##' 			=>$company['Company']['ulice'].' '.$company['Company']['mesto'].', '.$company['Company']['psc'],
			'##CompanyActivity.name##' 	=>$this->data['CompanyActivity']['name'],
			'##CompanyActivity.text##' 	=>$this->data['CompanyActivity']['text'],
			'##CompanyActivity.activity_datetime##' 	=> date('d.m.Y',strtotime($this->data['CompanyActivity']['activity_datetime'])),
            '##shif_type##'=> $this->shift_type_list[$this->data['Extended']['shift_type']],
			'##CompanyContact.name##' 	=>$company_contact['CompanyContact']['name'],
			'##CompanyContact.position##' 	=>$company_contact['CompanyContact']['position'],
			'##CmsUser.name##' 			=>$this->logged_user['CmsUser']['name'],
            '##text_odmeny##'  => $text_odmeny,
            '##objednavky##'  => $text_objednavky,
            '##dodatecne_info##' => $this->gen_email_data()
		);

       /**
        * nove nastaveni componenty
        * nastaveni dat spolecnosti name, id jestlivych skupin CM, SM, COO, COO2
        */
	    $this->Email->set_company_data($company['Company']);
        $recipient_list = array($this->logged_user['CmsUser']['email']);
        if(in_array($this->data['CompanyActivity']['company_id'],array(852,884,826, 81))){
            $recipient_list = array('vlastimilkula@wincottpeople.com','Choi@chb-prague.com','Solisova@Jofex.eu','solisova.v@chb-prague.com',$this->logged_user['CmsUser']['email']);
        }

        if(in_array($this->data['CompanyActivity']['company_id'],array(2626,129, 207, 11,1772,4,2536,770,2279))){
            $recipient_list = array('adriansuchanek@atgroup.sk',$this->logged_user['CmsUser']['email']);
        }

       if(in_array($this->logged_user['CmsGroup']['id'], array(52,18))){ // Pokud aktivitu vytvari SalesManager nebo Manazer Realizace tak posli email na janamarkova@atgroup.sk
            $recipient_list[] = 'radekvlk@wincottpeople.com';
       }

        if(in_array( $this->logged_user['CmsGroup']['id'], array(4,18))){  //Pokud se jedna o aktivity ManazeraRealizace nebo Koordinatora tak posli email SalesManagerovi, zasilani pouze pro spolecnosti jejichz smlouva byla uzavrena pred mene nez pul rokem

            if(isset($company['Company']['datum_podpisu_smlouvy']) && $company['Company']['datum_podpisu_smlouvy'] != '0000-00-00'){
                $datum = date("Y-m-d", strtotime( $company['Company']['datum_podpisu_smlouvy'] . "+6 month"));
            }else{
                $datum = '0000-00-00';
            }
            if($datum > date('Y-m-d')){
                if($company['Company']['self_manager_id']){
                    $this->loadModel('CmsUser');
                    $sm = $this->CmsUser->read(null,$company['Company']['self_manager_id']);
                    $recipient_list[] = $sm['CmsUser']['email'];
                }
            }
        }


        //ID3
        $this->Email->send_from_template_new(0,$recipient_list,$replace_list);

    }


    function gen_email_data(){
        $string  = '';
        if($this->data['CompanyActivity']['company_activity_type_id'] == 14){
                $string .= '<br /><b>Náborová aktivita:</b><br /> '.$this->data['Extended']['hire_activity'].'<br />';
                $string .= '<br /><b>Mělo nastoupit:</b> '.$this->data['Extended']['suppose_come'].'<br />';
                $string .= '<b>Nastoupilo:</b> '.$this->data['Extended']['comes'].'<br />';
                $string .= '<b>Nenastoupil:</b> ';
                    foreach($this->data['Extended'] as $item){
                        if(is_array($item)){
                            foreach($item as $val){
                                if(!empty($val)){
                                    $string .= $val.', ';
                                }
                            }
                        }
                    }
                $string .= '<br /><b>Nahrazeno:</b> '.$this->data['Extended']['replaced'].'<br />';
                $string .= '<b>Sankce:</b> '.$this->data['Extended']['sanctions'].'<br />';
                $string .= '<b>Nové nástupy:</b> '.$this->data['Extended']['new_comes'].'<br />';
                $string .= '<b>Odchody:</b> '.$this->data['Extended']['leaves'].'<br />';
        }else if($this->data['CompanyActivity']['company_activity_type_id'] == 15){
                $string2 = '' ;
                $string .= '<br /><b>Fluktuace</b><br />';
                $string .= 'Shrnutí začátek týdne: '.$this->data['Extended']['start_week'].'<br />';
                $string .= 'Odchodů: '.$this->data['Extended']['leaves_report'].'<br />';
                $string .= 'Nahrazeno: '.$this->data['Extended']['replaced_report'].'<br />';
                $string .= '<b>Shrnutí týdenních objednávek</b><br />';
                $string .= 'Volných pozic: '.$this->data['Extended']['free_positions'].'<br />';
                $string .= 'Obsazených pozic: '.$this->data['Extended']['filled_positions'].'<br />';
                $string .= 'Odpracovaných hodin: '.$this->data['Extended']['worked_hours'].'<br />';
                $string .= 'Předběžná týdenní fakturace: '.$this->data['Extended']['week_billing'].'<br />';
                $string .= '<b>Profese</b><br />';
                $string .= '<table style="text-align:center;"><tr><th>Agentura</th><th>Profese</th><th>Zaměstnanců konkurence</th><th>Aktuální počet</th></tr>';
                foreach($this->data['Extended'] as $uid=>$item){
                    if(is_array($item)){
                        if(!empty($item['profession_report'])){
                            $string .= '<tr><td>'.$item['agency'].'</td><td>'.$item['profession_report'].'</td><td>'.$item['employe_count'].'</td><td>'.$item['current_count'].'</td></tr>';
                        }
                        if(!empty($item['company_meetings'])){
                            $string2 .= '<b>Schůzka: </b>'.$item['company_meetings'].' - '.$item['reason_of_meeting'].' - '.$item['solution'].'<br />';
                        }
                    }
                }
                $string .= '</table>';
                $string .= $string2;
        }

        return $string;
    }

	/**
 	* Prenos aktivity do kose
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function aktivity_trash($company_id, $id){
		$this->loadModel('CompanyActivity');
		$this->CompanyActivity->save(array('CompanyActivity'=>array(
            'kos'=>1,
            'id'=>$id,
            'kos_cms_user_id'=>$this->logged_user['CmsUser']['id']
        )));
		$this->aktivity($company_id);
		unset($this->CompanyActivity);
	}
	/**
 	* Zobrazeni kontaktu k dane firme
 	*
	* @param $company_id
 	* @return view
 	* @access public
	**/
	function kontakty($company_id){
		$this->autoLayout = false;
		$this->loadModel('CompanyContact'); $this->CompanyContact = new CompanyContact();
		$this->set('contact_list', $this->CompanyContact->findAll(array('CompanyContact.company_id'=>$company_id,'CompanyContact.kos'=>0)));
		$this->set('company_id',$company_id);
		unset($this->CompanyContact);
		$this->render('kontakty/index');

	}
	/**
 	* Editace kontaktu
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function kontakty_edit($company_id = null, $id = null){
		$this->autoLayout = false;
		$this->loadModel('CompanyContact'); $this->CompanyContact = new CompanyContact();
		if (empty($this->data)){
			$this->data['CompanyContact']['company_id'] = $company_id;
			if ($id != null){
				$this->data = $this->CompanyContact->read(null,$id);
			}
			$this->render('kontakty/edit');
		} else {
			$this->CompanyContact->save($this->data);
			$this->kontakty($this->data['CompanyContact']['company_id']);
		}
		unset($this->CompanyContact);
	}
	/**
 	*Presun kontaktu do kose
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function kontakty_trash($company_id, $id){
		$this->loadModel('CompanyContact'); $this->CompanyContact = new CompanyContact();
		$this->CompanyContact->save(array('CompanyContact'=>array('kos'=>1,'id'=>$id)));
		$this->kontakty($company_id);
		unset($this->CompanyContact);
	}

	/**
 	* Seznam ukolu
 	*
	* @param $company_id
 	* @return view
 	* @access public
	**/
	// TASKS
	function tasks($company_id){
		$this->autoLayout = false;
		$conditions = array('CompanyTask.company_id'=>$company_id,'CompanyTask.kos'=>0);

        $this->set('logged_group',$this->logged_user['CmsGroup']['id']);

		if ($this->logged_user['CmsGroup']['permission']['companies']['tasks'] == 2)
			$conditions['cms_user_id'] = $this->logged_user['CmsUser']['id'];

		$this->loadModel('CompanyTask'); $this->CompanyTask = new CompanyTask();
		$this->CompanyTask->bindModel(array('belongsTo'=>array('Company','CmsUserFrom'=>array('className'=>'CmsUser','foreignKey'=>'creator_id'),'CmsUserTo'=>array('className'=>'CmsUser','foreignKey'=>'cms_user_id'))));
		$this->set('contact_list', $this->CompanyTask->findAll($conditions,null,'CompanyTask.complate ASC, CompanyTask.complate_date DESC, CompanyTask.id DESC'));
		$this->set('company_id',$company_id);
		unset($this->CompanyTask);
		$this->render('tasks/index');

	}
	/**
 	* Editace ukolu
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function tasks_edit($company_id = null, $id = null){
		$this->autoLayout = false;
		$this->loadModel('CompanyTask'); $this->CompanyTask = new CompanyTask();
		if (empty($this->data)){
			switch($company_id){
				case -1:
					$this->set('domwin_name','domwin');
					break;
				default:
					$this->set('domwin_name','domwin_tasks_add');
					break;
			}


			$this->loadModel('SettingActivityType'); $this->SettingActivityType = new SettingActivityType();
			$this->loadModel('CmsUser'); $this->CmsUser = new CmsUser();

			$this->set('company_activity_type_list', $this->SettingActivityType->find('list',array('conditions'=>array('kos'=>0), 'order'=>'poradi ASC')));


			$this->set('cms_user_list',$this->CmsUser->find('list',array('conditions'=>array('CmsUser.kos'=>0))));
			unset($this->CmsUser);
			unset($this->SettingActivityType);

			$this->data['CompanyTask']['company_id'] = $company_id;
			if ($id != null){
				$this->CompanyTask->bindModel(array('belongsTo'=>array('Company','CmsUser','SettingActivityType'=>array('foreignKey'=>'setting_task_type_id'))));
				$this->data = $this->CompanyTask->read(null,$id);

				$this->loadModel('CompanyContact'); $this->CompanyContact = new CompanyContact();
				$this->set('comapany_contact_list',$this->CompanyContact->find('list',array('conditions'=>array('CompanyContact.kos'=>0,'CompanyContact.company_id'=>$this->data['CompanyTask']['company_id']))));
				unset($this->CompanyContact);

			} else {
				// START load company name pro var_input
				$this->data = am($this->data, $this->Company->read(array('name','mesto'),$company_id));
				// END load company name pro var_input
			}
			$this->render('tasks/edit');
		} else {
			// START pridani cms_user_id pokud se vytvari ukol
			if (empty($this->data['CompanyTask']['id'])) 		{
				$this->data['CompanyTask']['creator_id'] = $this->logged_user['CmsUser']['id'];
			}
			// END pridani cms_user_id pokud se vytvari ukol
			$this->CompanyTask->save($this->data);
			$this->tasks($this->data['CompanyTask']['company_id']);
		}
		unset($this->CompanyTask);
	}
	/**
 	* Zmena ukolu na hotovy a vytvoreni aktivity
 	*
 	* @return view
 	* @access public
	**/
	function tasks_complate(){
		if (!empty($this->data)){
			$this->loadModel('CompanyTask'); $this->CompanyTask = new CompanyTask();
			$this->data['CompanyTask']['complate'] = 1;
			$this->data['CompanyTask']['complate_date'] = $this->data['CompanyActivity']['activity_datetime'];
			$this->CompanyTask->save($this->data);

			$this->loadModel('CompanyActivity'); $this->CompanyActivity = new CompanyActivity();

			$this->data['CompanyActivity']['cms_user_id'] 	= $this->logged_user['CmsUser']['id'];
			$this->CompanyActivity->save($this->data);
			$this->send_email_to_directors_about_new_activity();
			unset($this->CompanyActivity);
			$this->tasks($this->data['CompanyTask']['company_id']);
		} else {
			die('Nepovolena operace');
		}
	}
	/**
 	* Presun ukolu do kose
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function tasks_trash($company_id, $id){
		$this->loadModel('CompanyTask'); $this->CompanyTask = new CompanyTask();
		$this->CompanyTask->save(array('CompanyTask'=>array('kos'=>1,'id'=>$id)));
		$this->tasks($company_id);
		unset($this->CompanyTask);
	}

	/**
 	* Seznam priloh
 	*
	* @param $company_id
 	* @return view
 	* @access public
	**/
	function attachs($company_id){
		$this->autoLayout = false;
		$this->loadModel('CompanyAttachment');

		if($this->logged_user["CmsGroup"]["permission"]["companies"]["attach"]==2)
			$pristup =  "CompanyAttachment.cms_user_id=".$this->logged_user['CmsUser']['id'];
		else
			$pristup="";

		$this->CompanyAttachment->bindModel(array('belongsTo'=>array('SettingAttachmentType')));
		$this->set('attachment_list', $this->CompanyAttachment->findAll(array('CompanyAttachment.company_id'=>$company_id,'CompanyAttachment.kos'=>0 ,$pristup)));
		$this->set('company_id',$company_id);
		unset($this->CompanyAttachment);
		$this->render('attachs/index');

	}
	/**
 	* Editace priloh
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function attachs_edit($company_id = null, $id = null){
		$this->autoLayout = false;
		$this->loadModel('CompanyAttachment'); $this->CompanyAttachment = new CompanyAttachment();
		if (empty($this->data)){
			$this->loadModel('SettingAttachmentType'); $this->SettingAttachmentType = new SettingAttachmentType();
			$this->set('setting_attachment_type_list',$this->SettingAttachmentType->find('list',array('conditions'=>array('kos'=>0),'order'=>'poradi ASC')));
			unset($this->SettingAttachmentType);
			$this->data['CompanyAttachment']['company_id'] = $company_id;
			if ($id != null){
				$this->data = $this->CompanyAttachment->read(null,$id);
			}
			$this->render('attachs/edit');
		} else {
			$this->data["CompanyAttachment"]["cms_user_id"]=$this->logged_user['CmsUser']['id'];
			$this->CompanyAttachment->save($this->data);
			$this->attachs($this->data['CompanyAttachment']['company_id']);
		}
		unset($this->CompanyAttachment);
	}
	/**
 	* Presun prilohy do kose
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function attachs_trash($company_id, $id){
		$this->loadModel('CompanyAttachment'); $this->CompanyAttachment = new CompanyAttachment();
		$this->CompanyAttachment->save(array('CompanyAttachment'=>array('kos'=>1,'id'=>$id)));
		$this->attachs($company_id);
		unset($this->CompanyAttachment);
	}
	/**
 	* Nahrani prilohy na ftp
 	*
 	* @return view
 	* @access public
	**/
	function upload_attach() {
		$this->Upload->set('data_upload',$_FILES['upload_file']);
		if ($this->Upload->doit(json_decode($this->data['upload']['setting'],true))){
			echo json_encode(array('upload_file'=>array('name'=>$this->Upload->get('outputFilename')),'return'=>true));
		} else
			echo json_encode(array('return'=>false,'message'=>$this->Upload->get('error_message')));
		die();
	}
	/**
 	* Stazeni prilohy
 	*
	* @param $file
	* @param $file_name
 	* @return download file
 	* @access public
	**/
	function  attachs_download($file,$file_name){
		$pripona = strtolower(end(Explode(".", $file)));
		$file = strtr($file,array("|"=>"/"));
		$filesize = filesize('../administrace/uploaded/'.$file);
		$cesta = "http://administrace.power-job.cz/uploaded/".$file;

		header("Pragma: public"); // požadováno
	    header("Expires: 0");
	    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	    header("Cache-Control: private",false); // požadováno u některých prohlížečů
	    header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . $filesize);
		Header('Content-Type: application/octet-stream');
		Header('Content-Disposition: attachment; filename="'.$file_name.'.'.$pripona.'"');
		readfile($cesta);

        echo readfile($cesta);
        die();

    }

	/**
 	* Nacteni province
 	*
	* @param $province_id
 	* @return view
 	* @access public
	**/
	function load_province($parent_id = null,$return = false){
		$this->loadModel('Province');
        $provinces = $this->Province->find('list',array('conditions'=>array('Province.stat_id'=>$parent_id), 'order'=>'name ASC'));
		if(empty($provinces)){
            $provinces = array(-1 => 'Nezadáno');
        }else{
            $provinces[-1] = 'Nezadáno';
        }
        if($return !== false)
            return $provinces;
        else
            die(json_encode($provinces));
	}



    function load_position_profesia($category_id = null,$return = false){
        $this->loadModel('ProfesiaCategoryPosition');
        $pos = $this->ProfesiaCategoryPosition->find('all',array('fields'=>array('position_id','name'),'conditions'=>array('ProfesiaCategoryPosition.category_id'=>$category_id)));
        foreach($pos as  $item){
            $positions[$item["ProfesiaCategoryPosition"]['position_id']]= $item ["ProfesiaCategoryPosition"]['name'];
        }
        if(empty($positions))
            $positions = array(-1 => 'Nezadáno');

        if($return !== false)
            return $positions;
        else
            die(json_encode($positions));
    }
	/**
 	* Nacteni country
 	*
	* @param $country_id
 	* @return view
 	* @access public
	**/
	function load_country($parent_id = null){
		$this->loadModel('Countrie');
        $this->loadModel('Province');
        $stat = $this->Province->read('stat_id',$parent_id);
        unset($this->Province);
        if($stat['Province']['stat_id'] == 3 || $parent_id == -1){
            $country_list = array('-1'=>'Nezadáno');
        }else{
		    $country_list = $this->Countrie->find('list',array('conditions'=>array('Countrie.province_id'=>$parent_id), 'order'=>'name ASC'));
            $country_list['-1'] = 'Nezadáno';
        }
        die(json_encode($country_list));
	}
	/**
	* Slouzi pro zobrazeni nadefinovanych pracovnich pozic k dane firme
	*
	* @param $company_id
 	* @return view  list of company position
 	* @access public
 	*/

	function pozice($company_id){
		// nacteni potrebnych DB modelu
		$this->loadModel('CompanyWorkPosition');
		$this->CompanyWorkPosition->bindModel(array('hasMany'=>array('CompanyMoneyValidity'=>array('fields'=>array('id','fakturacka','platnost_od','platnost_do'),'order'=>'platnost_od DESC','conditions'=>array('kos'=>0)))),false);
		$this->CompanyWorkPosition->CompanyMoneyValidity->bindModel(array('hasMany'=>array('CompanyMoneyItem'=>array('fields'=>array('id','doprava','cena_ubytovani_na_mesic','provinzni_marze','vypocitana_celkova_cista_maximalni_mzda_na_hodinu','name'),'conditions'=>array('kos'=>0)))));
		// nacteni pracovnich pozic k dane spolecnosti
		$this->set('work_position_list', $pp =  $this->CompanyWorkPosition->find('all',array(
				'fields'=>array(
					'if(CompanyWorkPosition.parent_id = -1, CompanyWorkPosition.id, CompanyWorkPosition.parent_id) as myOrder',
					'CompanyWorkPosition.*'
				),
				'conditions'=>array(
					'CompanyWorkPosition.company_id'=>$company_id,
					'CompanyWorkPosition.kos'=>0,
					'CompanyWorkPosition.test'=>0,
                    'CompanyWorkPosition.new'=>0
				),
				'order'=>array('myOrder ASC','parent_id ASC'),
				'recursive' =>2
		)));

        $this->CompanyWorkPosition->CompanyMoneyValidity->bindModel(array('hasOne'=>array('NewMoneyItem'=>array('fields'=>array('id'),'conditions'=>array('kos'=>0)))));
        // nacteni (nových) pracovnich pozic k dane spolecnosti
		$this->set('work_position_list_new', $this->CompanyWorkPosition->find('all',array(
				'fields'=>array(
					'if(CompanyWorkPosition.parent_id = -1, CompanyWorkPosition.id, CompanyWorkPosition.parent_id) as myOrder',
					'CompanyWorkPosition.*'
				),
				'conditions'=>array(
					'CompanyWorkPosition.company_id'=>$company_id,
					'CompanyWorkPosition.kos'=>0,
					'CompanyWorkPosition.test'=>0,
                    'CompanyWorkPosition.new'=>1
				),
				'order'=>array('myOrder ASC','parent_id ASC'),
				'recursive' =>2
		)));
		// uvolneni modelu z pameti
		unset($this->CompanyWorkPosition);

		$this->loadModel('CompanyWorkPosition');
		$this->CompanyWorkPosition->bindModel(array(
            'hasMany'=>array(
                'CompanyMoneyItem'=>array(
                    'fields'=>array('id','doprava','cena_ubytovani_na_mesic','provinzni_marze',
                            'vypocitana_celkova_cista_maximalni_mzda_na_hodinu','name',
                            'fakturacni_sazba_na_hodinu'),
                    'conditions'=>array('kos'=>0)
                )
            ),
            'hasOne'=>array(
                'NewMoneyItem'=>array(
                    'fields'=>array('NewMoneyItem.id'),
                    'conditions'=>array('NewMoneyItem.kos'=>0)
                )
            )
        ),false);
		// nacteni testovacich pracovnich pozic k dane spolecnosti
		$this->set('work_position_list_test', $pt =  $this->CompanyWorkPosition->findAll(array('CompanyWorkPosition.company_id'=>$company_id,'CompanyWorkPosition.kos'=>0,'CompanyWorkPosition.test'=>1,'CompanyWorkPosition.new'=>0),null,'name ASC',null,null,2));

        // testovacich ale novych
        $this->set('work_position_list_test_new', $this->CompanyWorkPosition->findAll(array('CompanyWorkPosition.company_id'=>$company_id,'CompanyWorkPosition.kos'=>0,'CompanyWorkPosition.test'=>1,'CompanyWorkPosition.new'=>1),null,'name ASC',null,null,2));


		// predani ID spolecnosti do View
		$this->set('company_id',$company_id);
		// zjisteni o jaky se jedna stat kvuli měně :-)
		$this->set('stat_id',$this->Company->read('stat_id',$company_id));


		// uvolneni modelu z pameti
		unset($this->CompanyWorkPosition);

       // pr($pt);
        /**
         * Nastaveni novych moeny item, do listu, dle workp_position_id
         */
        // $this->set('new_money_items',$aa = Set::combine($pt,'{n}.CompanyWorkPosition.id','{n}.NewMoneyItem.id'));
        //print_r($aa);
        /**
         * nastaveni prav
         */
        $this->set('permission',$this->logged_user['CmsGroup']['permission']['companies']);

		// vyrendrovani seznamu pozic
		$this->render('work_position/index');
	}
	/**
	* Slouzi pro zmenu nazvu pracovni pozice pomoci AJAX v seznamu pozic
	*
	* @param $id
	* @param $name
 	* @return JSON true | false
 	* @access public
 	*/
	function position_change_name($id, $name = 'CHYBA vyplňte jméno'){
		$this->loadModel('CompanyWorkPosition'); $this->CompanyWorkPosition = new CompanyWorkPosition();
		$this->CompanyWorkPosition->id = $id;
		$this->CompanyWorkPosition->saveField('name',$name);
		die(json_encode(array('result'=>'true')));
	}
	/**
	* odstraneni pracovni pozice
	*
	* @param $company_id
	* @param $id
 	* @return View
 	* @access public
 	*/
	function position_trash($company_id, $id){
		$this->loadModel('CompanyWorkPosition');
		$detail = array();
		$detail = $this->CompanyWorkPosition->find('first',array('fields'=>array('id'),
		'conditions'=>array(
			'parent_id'=>$id,
			'kos'=>0
		)));

		if($detail){
			die(json_encode(array('result'=>false)));
		}
		else{
			$this->CompanyWorkPosition->id = $id;
			$this->CompanyWorkPosition->saveField('kos',1);

			die(json_encode(array('result'=>true)));
		}
	}
	/**
	* odstraneni kalkulace
	*
	* @param $company_id
	* @param $id
 	* @return View
 	* @access public
 	*/
	function money_item_trash($company_id, $id){
		$this->loadModel('CompanyMoneyItem'); $this->CompanyMoneyItem = new CompanyMoneyItem();
		$this->CompanyMoneyItem->id = $id;
		$this->CompanyMoneyItem->saveField('kos',1);

		// vyrendrovani seznamu pozic
		$this->pozice($company_id);
	}
	/**
	* odstraneni platnosti kalkulaci
	*
	* @param $company_id
	* @param $id
 	* @return View
 	* @access public
 	*/
	function platnost_trash($company_id, $id){
		$this->loadModel('CompanyMoneyValidity'); $this->CompanyMoneyValidity = new CompanyMoneyValidity();
		$this->CompanyMoneyValidity->id = $id;
		$this->CompanyMoneyValidity->saveField('kos',1);

		// vyrendrovani seznamu pozic
		$this->pozice($company_id);
	}

	/**
	* Slouzi pro pridani nove pracovni pozice, pokud je empty $this->data, tak se vyrendruje formular,
	* pokud neni, dojde k ulozeni nove pozice a nove casove platnosti do DB
	*
	* @param $company_id
 	* @return view if empty this->data, list od company position if not empty
 	* @access public
 	*/

	function work_position_add($company_id = null){
		if (empty($this->data)){
			$this->data['CompanyWorkPosition']['company_id'] = $company_id;
			$this->set('isSm',($this->logged_user['CmsGroup']['id']==2 ? true : false ));
			$this->render('work_position/add');
		} else {
			// inicializace modelu
			$this->loadModel('CompanyWorkPosition');

			$this->data['CompanyWorkPosition']['test'] = 1;
            $this->data['CompanyWorkPosition']['cms_user_id'] = $this->logged_user['CmsUser']['id'];
			// ulozeni pracovni pozice
			$this->CompanyWorkPosition->save($this->data);


			// uvolneni modelu z pameti
			unset($this->CompanyWorkPosition);

			// vyrendrovani seznamu pozic
			$this->pozice($this->data['CompanyWorkPosition']['company_id']);
		}
	}

	/**
	* Slouzi pro zmenu fakturacky a s tim spojene platnosti, pri aplikaci zmen dojde k vytvoreni kopie money_validity
	* a k ni nadefinovanym kalkulacim, ktere se prepocitaji dle nove fakturovane castky
	*
	* @param $id
 	* @return view if empty this->data, list od company position if not empty
 	* @access public
 	*/
	function work_money_validity_edit($id = null){
		// inicializace modelu
		$this->loadModel('CompanyMoneyValidity');
		$this->loadModel('CompanyMoneyItem');
		if (empty($this->data)){
			$this->data = $this->CompanyMoneyValidity->read(null, $id);
			$this->data['CompanyMoneyValidity']['platnost_od_old'] = $this->data['CompanyMoneyValidity']['platnost_od'];
			$this->render('work_position/change_validity');
		} else {

			$child_save = $this->data;
			unset($child_save['CompanyMoneyValidity']['id']);

			// nastaveni platnosti do u parent polozky
			$this->data['CompanyMoneyValidity']['platnost_do'] = date("Y-m-d",strtotime($this->data['CompanyMoneyValidity']['platnost_od']) - (60*60*24));

			// preneseni id a fakturacky z parent polozky
			$parent_id = $this->data['CompanyMoneyValidity']['id'];

			// odebrani s this->data id a fakturacka
			unset($this->data['CompanyMoneyValidity']['fakturacka']);
			unset($this->data['CompanyMoneyValidity']['platnost_od']);

			// ulozeni platnost do pro parent polozku
			$this->CompanyMoneyValidity->save($this->data);
			$this->CompanyMoneyValidity->id = null;

			// ulozeni nove platnosti do DB
			$this->CompanyMoneyValidity->save($child_save);
			$new_parent_id = $this->CompanyMoneyValidity->id;

			// zjisteni o jaky se jedna stat kvuli kalkulacce
			$company_stat_info = $this->Company->read(array('stat_id'),$this->data['CompanyMoneyValidity']['company_id']);
			$company_stat_id = $company_stat_info['Company']['stat_id'];

			// nalezeni money_item pro kopirovani a prepocitani k nove platnosti
			$money_item_list = $this->CompanyMoneyItem->findAll(array('CompanyMoneyItem.company_money_validity_id' => $parent_id));

			foreach($money_item_list as $item){
				unset($item['CompanyMoneyItem']['id']);
				$item['CompanyMoneyItem']['fakturacni_sazba_na_hodinu'] = $child_save['CompanyMoneyValidity']['fakturacka'];
				switch ($company_stat_id){
					case 1:
						$to_save['CompanyMoneyItem'] = $this->kalkulacka_cz($item['CompanyMoneyItem']);
						break;
					case 2:
						$to_save['CompanyMoneyItem'] = $this->kalkulacka_sk($item['CompanyMoneyItem']);
						break;
					default:
						die('CHYBA VYBERU JAZYKA KALKULACKY');
				}

				$to_save['CompanyMoneyItem']['company_money_validity_id'] = $new_parent_id;
				$this->CompanyMoneyItem->save($to_save);
				$this->CompanyMoneyItem->id = null;
			}

			// uvolneni modelu z pameti
			unset($this->CompanyMoneyValidity);

			// vyrendrovani seznamu pozic
			$this->pozice($this->data['CompanyMoneyValidity']['company_id']);
		}
	}

    function work_money_edit_stat($position_id,$stat_id){
        echo $this->requestAction('companies/work_money_edit/'.$position_id.'/-1/',array('stat_id'=>$stat_id));
        die();
    }

	/**
	* Slouzi pro nadefinovani jednotlivych kalkulaci k dane spolecnosti / pozici / platnosti
	*
	* @param $position_id
	* @param $money_validity_id
	* @param $id
 	* @return view if empty this->data, list od company position if not empty
 	* @access public
 	*/
	function work_money_edit($position_id = null, $money_validity_id = null, $id = null, $show= null){
	//Configure::write('debug',2);
	//echo 'Probíhá testování v kalkulacích!!!';

    	// inicializace modelu
		$this->loadModel('CompanyMoneyItem');
		$this->loadModel('CompanyMoneyValidity');
		$this->loadModel('CompanyWorkPosition');
		$pausal = false;
		$h1000 = false;

        //prepnuti do rezimu pausalu view a zachovani cele logiky
        if($money_validity_id == -2){
            $money_validity_id = -1;
            $pausal = true;
        }

        /**
         * prepnuti na hmm hodinovou
         * forma odmeny h1000
         */
        if($money_validity_id == -3){
            $money_validity_id = -1;
            $h1000 = true;
        }

		if($show == -1){ //chceme jen nahled a zamezit editaci
			$this->set('only_show', true);

            //pro admina a directora povolit editaci jen par veci
            if(in_array($this->logged_user['CmsGroup']['id'],array(1,5))){
                $this->set('edit_small', true);
            }
            else
                $this->set('edit_small', false);
        }
		else
			$this->set('only_show', false);

        //print_r($this->data);

		if (empty($this->data)) {
			$company_stat_id = null;
			$position = $this->CompanyWorkPosition->read(array('company_id','test'),$position_id);
			// zjisteni o jaky se jedna stat kvuli kalkulacce
			$company_stat_info = $this->Company->read(array('stat_id'),$position['CompanyWorkPosition']['company_id']);
			$company_stat_id = $company_stat_info['Company']['stat_id'];
		    /** 30.11.11
		     * Nove pridano, kalkulace muze mit jiny stat nez firma
		     */
            if(isset($this->params['stat_id']) && $this->params['stat_id'] > 0){
		      $company_stat_id = $this->params['stat_id'];
		    }


            //nacteni zakladnich profesi k firme
            $this->set('position_list', $this->CompanyWorkPosition->find('list',array('order'=>'name ASC','conditions'=>array('company_id'=>$position['CompanyWorkPosition']['company_id'],'kos'=>0))));

			if ($id != null) {
				// pokud se nejedna o vytvoreni nove polozky nacti z DB a zakaz zmenu fakturacky
				$this->data = $this->CompanyMoneyItem->read(null,$id);
				// pokud pristupuji z reportu
				if ($money_validity_id == -1) {
					if($position['CompanyWorkPosition']['test']==0)
						$this->set('z_reportu', true);
					else{
						$this->set('save_test', true);
						// nacteni seznamu firem, pro moznost nacitani hodnot z db  pokud je id empty ???? WTF
						$this->set('company_list', $this->Company->find('list',array('order'=>'name ASC','conditions'=>array('id'=>$position['CompanyWorkPosition']['company_id']))));
								}
					// zjisteni o jaky se jedna stat kvuli kalkulacce
					$company_stat_info = $this->Company->read(array('stat_id'),$this->data['CompanyMoneyItem']['company_id']);
					/** 30.11.11
					 * Novinka
                     * pokud je stat_id nastaven u kalkulace tak berem tento
                     * jinak berem z firmy
					 */
                    $company_stat_id = (isset($this->data['CompanyMoneyItem']['stat_id']) && $this->data['CompanyMoneyItem']['stat_id']>0?$this->data['CompanyMoneyItem']['stat_id']:$company_stat_info['Company']['stat_id']);
				}


				//nastaveni hodnoty min. hrube hodinove mzdy pro view - k danemu statu
				$set_id = $company_stat_id==1 ? 3 : 4;
					$this->loadModel('Setting');
					$setting = array();
					$setting = $this->Setting->read(null,$set_id);
					$this->data['CompanyMoneyItem']['mhhd'] = $setting['Setting']['value']['mhhd'];
                    $this->data['CompanyMoneyItem']['pocet_dni_dovolene_svatku_zapocitanych_do_nakladu_default'] = $setting['Setting']['value']['pocet_dni_dovolene_svatku_zapocitanych_do_nakladu'];
					$this->data['CompanyMoneyItem']['vlozeny_prispevek_na_stravne_default'] = $setting['Setting']['value']['vlozeny_prispevek_na_stravne'];
					$this->data['CompanyMoneyItem']['vlozeny_prispevek_na_cestovne_default'] = $setting['Setting']['value']['vlozeny_prispevek_na_cestovne'];

				if($position['CompanyWorkPosition']['test']==0)
					$this->set('fa_disabled', true);


			} else {
			    if($pausal)
			         $this->data['CompanyMoneyItem']['pausal'] = 1;

                if($h1000)
			         $this->data['CompanyMoneyItem']['h1000'] = 1;
				// prozativni reseni, nacteni defaultnich hodnot z DB
//				$cols = $this->CompanyMoneyItem->query('DESCRIBE wapis__company_money_items');
//				foreach ($cols as $column) {$this->data['CompanyMoneyItem'][$column['COLUMNS']['Field']] = $column['COLUMNS']['Default'];}
				switch ($company_stat_id){
					case 1:
						$this->loadModel('Setting');
						$setting_cz = array();
						$setting_cz = $this->Setting->read(null,3);
						foreach($setting_cz['Setting']['value'] as $transform=>$key) {
							if($transform != 'vlozeny_prispevek_na_stravne' && $transform != 'vlozeny_prispevek_na_cestovne' )
								$this->data['CompanyMoneyItem'][$transform] = $key;
							else
								$this->data['CompanyMoneyItem'][$transform.'_default'] = $key;

						}
						break;
					case 2:
						$this->loadModel('Setting');
						$setting_sk = array();
						$setting_sk = $this->Setting->read(null,4);
						foreach($setting_sk['Setting']['value'] as $transform=>$key) {
							if($transform != 'pocet_dni_dovolene_svatku_zapocitanych_do_nakladu' && $transform != 'vlozeny_prispevek_na_stravne' && $transform != 'vlozeny_prispevek_na_cestovne' )
								$this->data['CompanyMoneyItem'][$transform] = $key;
                            else if($transform == 'pocet_dni_dovolene_svatku_zapocitanych_do_nakladu'){
                                $this->data['CompanyMoneyItem'][$transform.'_default'] = $key;
                                $this->data['CompanyMoneyItem'][$transform] = $key;
                            }
							else
								$this->data['CompanyMoneyItem'][$transform.'_default'] = $key;
						}
						break;
					default:
						die('nevi pro jaky stat ma vzit default hodnoty');
						break;
				}
				//pr($this->data['CompanyMoneyItem']);
				$this->data['CompanyMoneyItem']['company_work_position_id'] = $position_id;
				$this->data['CompanyMoneyItem']['company_money_validity_id'] = $money_validity_id;

				// jestlize se nejedna o prvni polozku, nacti fakturacku (prepis z default)  a nastav disabled
				$list = $this->CompanyMoneyItem->find(array('company_work_position_id'=>$position_id),array('fakturacni_sazba_na_hodinu'));
				if ($list && $position['CompanyWorkPosition']['test']==0){
					$this->data['CompanyMoneyItem']['fakturacni_sazba_na_hodinu'] = $list['CompanyMoneyItem']['fakturacni_sazba_na_hodinu'];
					$this->set('fa_disabled', true);
				}
				else
					$this->set('save_test', true);


				$this->data['CompanyMoneyItem']['company_id'] = $position['CompanyWorkPosition']['company_id'];

				$this->set('company_list', $this->Company->find('list',array('order'=>'name ASC','conditions'=>array('id'=>$position['CompanyWorkPosition']['company_id']))));

			}

            $this->set('company_stat_id',$company_stat_id);
			// vyrendrovani formulare
			switch ($company_stat_id){
				case 1:
                    if($pausal || (isset($this->data['CompanyMoneyItem']['pausal']) && $this->data['CompanyMoneyItem']['pausal']==1))
					   $this->render('work_money/edit_sk_pausal');
                    else if($h1000 || (isset($this->data['CompanyMoneyItem']['h1000']) && $this->data['CompanyMoneyItem']['h1000']==1))
				       $this->render('work_money/edit_h1000');
					else
                       $this->render('work_money/edit_cz');
					break;
				case 2:
                    if($pausal || (isset($this->data['CompanyMoneyItem']['pausal']) && $this->data['CompanyMoneyItem']['pausal']==1))
					   $this->render('work_money/edit_sk_pausal');
                    else if($h1000 || (isset($this->data['CompanyMoneyItem']['h1000']) && $this->data['CompanyMoneyItem']['h1000']==1))
				       $this->render('work_money/edit_h1000');
					else
					   $this->render('work_money/edit_sk');
			        break;
				default:
					die('nevi pro jaky stat renderovat');
					break;
			}
		} else {
			$search_unique = array();

            // poslat notfikaci mailem, pokud dojde ke zmene schvalene polozky
			if (isset($this->data['CompanyMoneyItem']['id']) && !isset($this->data['CompanyMoneyItem']['schvaleno'])){
				$schvaleni = $this->CompanyMoneyItem->read(array('schvaleno'), $this->data['CompanyMoneyItem']['id']);
				if ($schvaleni['CompanyMoneyItem']['schvaleno'] == 1){
					$this->data['CompanyMoneyItem']['schvaleno'] = 0;
					$send_mail = true;
				}
			}

	       if (!isset($this->data['CompanyMoneyItem']['id']) || $this->data['CompanyMoneyItem']['id'] == 0){
            //poskladani formy odmeny pro test unikatnosti jednotlive formy odmeny pro danou profesi
            $forma_odmeny_name = $this->generate_work_money_name($this->data);
            $ubytovani = ($this->data['CompanyMoneyItem']['cena_ubytovani_na_mesic'] > 0 ? 1 : 0);
            $doprava = ($this->data['CompanyMoneyItem']['doprava'] > 0 ? 1 : 0);
            $pausal = (isset($this->data['CompanyMoneyItem']['pausal']) ? 1 : 0);

           /** VYPNUTO 16.9.2010
            $this->loadModel('CompanyMoneyItem');
            $search_unique = $this->CompanyMoneyItem->query('
                 SELECT IF(doprava > 0,1,0) as dop, IF(cena_ubytovani_na_mesic> 0,1,0) as ub
                    FROM `wapis__company_money_items` AS `CompanyMoneyItem`
                 WHERE `company_work_position_id` = '.$this->data['CompanyMoneyItem']['company_work_position_id'].' AND `company_money_validity_id` = -1
                        AND `name` = '.$forma_odmeny_name.' and pausal='.$pausal.' AND `kos` = 0
                 HAVING dop = '.$doprava.' AND ub= '.$ubytovani.' LIMIT 1
				');
             */

            }

            //pokud nejaka takova foram existuje vyhod chybu
            if(count($search_unique) > 0)
                die(json_encode(array('result'=>false,'message'=>'Tato forma odměny se stejným nastavením ubytování a dopravy již existuje pro tuto profesi!')));
            else {

                pr($this->data);
                // jinak uloz do DB
                $this->CompanyMoneyItem->save($this->data);

    			if (isset($send_mail))
    				$this->send_email_to_directors_about_edit_money_item();

    			// pokud se jedna o prvni polozku, respektive vytvoreni, je otazkou, zda nedat podminku na to, zda je opravdu prvni, ulozi se do platnosti
    			if (empty($this->data['CompanyMoneyItem']['id']) && $this->data['CompanyMoneyItem']['company_money_validity_id']!=-1)
    				$this->CompanyMoneyValidity->save(array('CompanyMoneyValidity'=>array('fakturacka'=>$this->data['CompanyMoneyItem']['fakturacni_sazba_na_hodinu'], 'id'=>$this->data['CompanyMoneyItem']['company_money_validity_id'])));

    			// uvolneni modelu z pameti
    			unset($this->CompanyWorkPosition);
    			unset($this->CompanyMoneyValidity);
    			unset($this->CompanyMoneyItem);

    			// vyrendrovani seznamu pozic
    			$this->pozice($this->data['CompanyMoneyItem']['company_id']);
                die(json_encode(array('result'=>true)));

            }

		}
	}

    /**
     * Vytvoreni kodu formy odmeny
     * @since 10.11.09
     * @author Sol
     */
     private function generate_work_money_name($data){
        if(!isset($data['CompanyMoneyItem']['pausal']) || $data['CompanyMoneyItem']['pausal'] == 0)
            return $data['CompanyMoneyItem']['checkbox_pracovni_smlouva'].$data['CompanyMoneyItem']['checkbox_dohoda'].$data['CompanyMoneyItem']['checkbox_faktura'].$data['CompanyMoneyItem']['checkbox_cash'];
        else
            return '00001';
     }


	/**
	* Slouzi proautorizaci testovaci kalkulace
	*
	* @param $id
 	* @return view
 	* @access public
 	*/
	function work_money_auth($id = null, $form = null){
	//Configure::write('debug',2);
		if($id != null)
		{
			// zobraz autorizacni formular
			if($form == -1)
			{
				if (empty($this->data)) {
					$this->loadModel('CompanyWorkPosition');

					$detail= $this->CompanyWorkPosition->read(array('company_id','name'),$id);
					$position_list = array('-1'=>'Vytvořit novou');

					$this->CompanyWorkPosition->bindModel(array(
						'hasMany'=>array(
							'CompanyWorkPosition2'=>array(
								'order'=>'name ASC',
								'foreignKey' => 'parent_id',
								'conditions'=>array(
									'company_id  '=>$detail['CompanyWorkPosition']['company_id'],
									'kos'=>0,
									'test'=>0
								),
								//'recursive'=>2
							)
						)
					));

					$position_list_b = $this->CompanyWorkPosition->find('all',array(
						'conditions'=>array(
							'parent_id' => -1,
							'company_id  '=>$detail['CompanyWorkPosition']['company_id'],
							'kos'=>0,
							'test'=>0
						)
					));

					$output = array(''=>'Vytvořit novou');
					$output2 = array(''=>'-- Vyberte profesi --');
					foreach($position_list_b as $item){
						$output[$item['CompanyWorkPosition']['id']] = $item['CompanyWorkPosition']['name'].' ( přidat novou platnost )';
						$output2[$item['CompanyWorkPosition']['id']] = $item['CompanyWorkPosition']['name'];
						$output['p_'.$item['CompanyWorkPosition']['id']] = '-> Vytvořit novou sub';
						if (isset($item['CompanyWorkPosition2']) && count($item['CompanyWorkPosition2']) > 0){
							foreach($item['CompanyWorkPosition2'] as $sub_item){
								$output[$sub_item['id']] = '-> '.$sub_item['name'] .'  ( přidat novou platnost )';
								$output2[$sub_item['id']] = '-> '.$sub_item['name'];
							}
						}
					}


					$choose_list = array(
						''=>'--- Vyberte akci ---',
						'1'=>'Přidat pouze kalkulace k existujicí',
						'2'=>'Vytvořit novou kalkulaci, platnost nebo novou sub'
					);

					$this->set('choose_list',$choose_list);
					$this->set('pozice_name',$detail['CompanyWorkPosition']['name']);
					$this->set('id',$id);
					$this->data['CompanyWorkPosition']['company_id'] = $detail['CompanyWorkPosition']['company_id'];
					$this->set('company_work_position_list',$output);//$position_list+$position_list_b);
					$this->set('company_work_position_list2',$output2);// pro pridani pouze klakulaci k jiz vytvorene


                    //render
					$this->render('work_position/auth');
				}
				else {
				//save

					$choose_type = $this->data['CompanyMoneyValidity']['choose'];
					if($choose_type == 2){
					// stary typ autorizace, zde pridavem novou kalkulaci, nebo platnost nebo novou sub

						$typ = $this->data['CompanyMoneyValidity']['profese'];
						//vyhledani spolecne fakturacky
						$this->loadModel('CompanyMoneyItem');
						$one_item = $this->CompanyMoneyItem->find('first',array(
							'fields' => 'fakturacni_sazba_na_hodinu',
							'conditions'=>array(
								'company_work_position_id '=>$id,
								'kos  '=>0
							)
						));

						// vytvoreni uplne nove profese
						if($typ == "" || substr($typ, 0, 2) == 'p_'){
							//pokud pridavam novou sub pozici nastav parent_id
							$parent_id =  (substr($typ, 0, 2) == 'p_') ?  substr($typ, 2) : -1;

							//tvorba nove validity
							$this->loadModel('CompanyMoneyValidity');
							$to_save['CompanyMoneyValidity']['company_id'] = $this->data['CompanyWorkPosition']['company_id'];
							$to_save['CompanyMoneyValidity']['company_work_position_id'] = $id;
							$to_save['CompanyMoneyValidity']['fakturacka'] = $one_item['CompanyMoneyItem']['fakturacni_sazba_na_hodinu'];
							$to_save['CompanyMoneyValidity']['platnost_od'] = $this->data['CompanyMoneyValidity']['platnost_od'];

							//pr($to_save);
							$this->CompanyMoneyValidity->save($to_save);

							//u itemu zmeni validity id
							$this->CompanyMoneyItem->updateAll(
								array('company_money_validity_id' => $this->CompanyMoneyValidity->id),
								array('company_work_position_id' => $id,'kos'=>0)
							);

							//u position zmenit test na 0 a parent_id
							$this->loadModel('CompanyWorkPosition');
							$this->CompanyWorkPosition->id = $id;
							$this->CompanyWorkPosition->saveField('test',0);
							$this->CompanyWorkPosition->saveField('parent_id',$parent_id);

                            /**
                             * odeslani emailu o autorizaci
                             */
                             self::send_mail_about_auth_money_items($this->data['CompanyWorkPosition']['company_id'],$id);


							die(json_encode(true));
						}
						// verze kdy zadavam novou platnost k existujici pozici
						else if(substr($typ, 0, 2) != 'p_'){
							$pozice_id = $typ; // predani id
							$this->loadModel('CompanyMoneyValidity');
							//hledani aktualni platnosti
							$detail = $this->CompanyMoneyValidity->find('first',array(
								'fields' => array('id','platnost_od'),
								'conditions'=>array(
									'company_work_position_id '=>$pozice_id,
									'platnost_do  '=>'0000-00-00',
									'platnost_od  <>'=>'0000-00-00',
									'kos  '=>0
								)
							));

							//pokud je zadana platnost mensi nez stavajici tak vyhod chybu
							if($this->data['CompanyMoneyValidity']['platnost_od'] <= $detail['CompanyMoneyValidity']['platnost_od'])
								die(json_encode('Platnost je příliš malá, stará platnost je '.$detail['CompanyMoneyValidity']['platnost_od']));
							//pokud je zadana platnost mensi nez nynejsi mesic
							else if(($this->data['CompanyMoneyValidity']['platnost_od'] < (date('Y-m').'-01')) && $this->logged_user['CmsGroup']['id'] != 1)
								die(json_encode('Platnost je příliš malá, zadávate menší platnost než je nynější měsíc!!!'));
							// jinak uloz
							else{
								//nastaveni platnosti na nova platnost_od  - 1 den = minus jeden mesic a posledni den v nem
								$currentDate = new DateTime($this->data['CompanyMoneyValidity']['platnost_od']);
								$currentDate->modify('-1 day');
								$currentDate->format('Y-m-d');
								//ukonceni platnosti stare validity
								$this->CompanyMoneyValidity->id = $detail['CompanyMoneyValidity']['id'];
								$this->CompanyMoneyValidity->saveField('platnost_do',$currentDate->format('Y-m-d'));

								//ulozeni nove validity
								$this->CompanyMoneyValidity->id = null;
								$to_save_a['CompanyMoneyValidity']['company_id'] = $this->data['CompanyWorkPosition']['company_id'];
								$to_save_a['CompanyMoneyValidity']['company_work_position_id'] = $pozice_id;
								$to_save_a['CompanyMoneyValidity']['fakturacka'] = $one_item['CompanyMoneyItem']['fakturacni_sazba_na_hodinu'];
								$to_save_a['CompanyMoneyValidity']['platnost_od'] = $this->data['CompanyMoneyValidity']['platnost_od'];
								$this->CompanyMoneyValidity->save($to_save_a);

								//u stavajici profese priradit kos
								$this->loadModel('CompanyWorkPosition');
								$this->CompanyWorkPosition->id = $id;
								$this->CompanyWorkPosition->saveField('kos',1);

								//u itemu zmen validity id a profese_id
								$this->CompanyMoneyItem->updateAll(
									array('company_money_validity_id' => $this->CompanyMoneyValidity->id,'company_work_position_id' => $pozice_id),
									array('company_work_position_id' => $id,'kos'=>0)
								);

                                /**
                                 * odeslani emailu o autorizaci
                                 */
                                 self::send_mail_about_auth_money_items($this->data['CompanyWorkPosition']['company_id'],$id);

								 die(json_encode(true));
							}
						}
					}
					else if($choose_type==1) {
					// pridavame kalkulace k jiz vytvorene profesi a musi mit stejnou fakturacku
						if($this->data['CompanyMoneyValidity']['profese2'] != ''){
							$this->loadModel('CompanyMoneyValidity');
							//hledani aktualni platnosti
							$detail = $this->CompanyMoneyValidity->find('first',array(
								'fields' => array('id','fakturacka'),
								'conditions'=>array(
									'company_work_position_id '=>$this->data['CompanyMoneyValidity']['profese2'],
									'company_id '=>$this->data['CompanyWorkPosition']['company_id'],
									'platnost_do '=>'0000-00-00',
									'platnost_od  <>'=>'0000-00-00',
									'kos  '=>0
								)
							));

							$this->loadModel('CompanyMoneyItem');
							$fakturacka = $this->CompanyMoneyItem->find('first',array(
								'fields' => 'fakturacni_sazba_na_hodinu',
								'conditions'=>array(
									'company_work_position_id '=>$id,
									'company_id '=>$this->data['CompanyWorkPosition']['company_id'],
									'kos  '=>0
								)
							));

							if($fakturacka['CompanyMoneyItem']['fakturacni_sazba_na_hodinu'] != $detail['CompanyMoneyValidity']['fakturacka'])
								die(json_encode('Nesouhlasí Vám fakturačky, musíte přiřazovat kalkulace k profesi se stejnou fakturačkou. Vámi autorizována fakturace je '.$fakturacka['CompanyMoneyItem']['fakturacni_sazba_na_hodinu'].' a přidáváte k fakturaci '.$detail['CompanyMoneyValidity']['fakturacka']));

                            $cwp_list = array();
                            $cwp_list[] = $id;
                            $cwp_list[] = $this->data['CompanyMoneyValidity']['profese2'];
                            $compare_money_items = $this->CompanyMoneyItem->find('all',array(
								'fields' => array('company_work_position_id','name','cena_ubytovani_na_mesic','doprava'),
								'conditions'=>array(
								    'company_work_position_id'=>$cwp_list,
									'company_id'=>$this->data['CompanyWorkPosition']['company_id'],
									'(company_money_validity_id = '.$detail['CompanyMoneyValidity']['id'].' OR company_money_validity_id=-1)',
                                    'kos  '=>0
								)
							));
                            //print_r($compare_money_items);

                            foreach($compare_money_items as $item){

                                if($item['CompanyMoneyItem']['company_work_position_id'] == $id)
                                    $pole1[]=array(
                                            'name'=>$item['CompanyMoneyItem']['name'],
                                            'ubytovani'=>($item['CompanyMoneyItem']['cena_ubytovani_na_mesic']==0 ? 0 : 1),
                                            'doprava'=>($item['CompanyMoneyItem']['doprava']==0 ? 0 : 1),
                                    );
                                else if($item['CompanyMoneyItem']['company_work_position_id'] == $this->data['CompanyMoneyValidity']['profese2'])
                                   $pole2[]=array(
                                            'name'=>$item['CompanyMoneyItem']['name'],
                                            'ubytovani'=>($item['CompanyMoneyItem']['cena_ubytovani_na_mesic']==0 ? 0 : 1),
                                            'doprava'=>($item['CompanyMoneyItem']['doprava']==0 ? 0 : 1),
                                    );
                            }

                            $tmp=array();

                            /* nahrazeni hodnoty za text ano ne */
                        	function value_to_yes_no($value,$type = 'long'){
                                $tmp = array(
                                    'long'=>array('Ano','Ne'),
                                    'short'=>array('A','N')
                                );
                        		if($value != null && $value!= 0)
                        			return $tmp[$type][0];
                        		else
                        			return $tmp[$type][1];
                        	}

                            foreach($pole1 as $item){
                                if(in_array($item,$pole2))
                                    $tmp[] = $item['name'].' - Ubytování '.value_to_yes_no($item['ubytovani']).'/Doprava '.value_to_yes_no($item['doprava']);
                            }

                            if(count($tmp) > 0)
                                die(json_encode("Chyba: Tyto kalkulace jsou již obsaženy v této profesi:\n\n".implode("\n",$tmp)."\n\n Tyto kalkulace musíte smazat."));


							if($detail['CompanyMoneyValidity']['id'] != null){
								//u stavajici profese priradit kos
								$this->loadModel('CompanyWorkPosition');
								$this->CompanyWorkPosition->id = $id;
								$this->CompanyWorkPosition->saveField('kos',1);

								$now = date('Y-m-d H:i:s');// cas

								//u itemu zmen validity id a profese_id
								$this->CompanyMoneyItem->updateAll(
									array(
										'autorizace' => 1,
										'autorizace_date'=>'"'.$now.'"',
										'company_money_validity_id' => $detail['CompanyMoneyValidity']['id'],
										'company_work_position_id' => $this->data['CompanyMoneyValidity']['profese2']
									),
									array('company_work_position_id' => $id,'kos'=>0)
								);

                                /**
                                 * odeslani emailu o autorizaci
                                 */
                                 self::send_mail_about_auth_money_items($this->data['CompanyWorkPosition']['company_id'],$id);


								die(json_encode(true));
							}
							else
								die(json_encode('Chyba v hledani platnosti k Vámi vybrané profesi.'));
						}
					}


				}

			} // kontroluj zda muze byt autorizovana profese
			else {
				$this->loadModel('CompanyMoneyItem');
				$pocet = $this->CompanyMoneyItem->find('count',array(
					'fields' => 'DISTINCT fakturacni_sazba_na_hodinu',
					'conditions'=>array(
						'company_work_position_id '=>$id,
						'kos  '=>0
					)
				));

				if($pocet == 1) // existuje pouze jedna fakturacka pro vsechny items, muzeme autorizovat
					die(json_encode(true));
				else
					die(json_encode(false));
			}

		}


	}


    /**
     * funkce pro odeslani mailu pro kazdou kalkulaci ktera byla autorizovana
     * tato funkce
     */
    function send_mail_about_auth_money_items($company_id,$work_position_id){

            //load dat pro email
            $company = $this->Company->read($this->Company->company_data_for_email_notification,$company_id);
            $this->loadModel('CompanyMoneyItem');
            $this->CompanyMoneyItem->bindModel(array(
                'belongsTo'=>array('CompanyWorkPosition'=>array('fields'=>array('name')))
            ));
            $money_list = $this->CompanyMoneyItem->find('all',array(
                'conditions'=>array(
                    'CompanyMoneyItem.company_work_position_id '=>$work_position_id,
				    'CompanyMoneyItem.kos'=>0
                )
            ));
            unset($this->CompanyMoneyItem);

            /**
             * zjisteni vyskytu typu odmen
             * a po te zavolani funkce ktera zjistuje spravnost formularu
             */
            $checkbox = array(
                1=>array_sum(Set::extract('/CompanyMoneyItem/checkbox_pracovni_smlouva',$money_list)),
                2=>array_sum(Set::extract('/CompanyMoneyItem/checkbox_dohoda',$money_list)),
                3=>array_sum(Set::extract('/CompanyMoneyItem/checkbox_faktura',$money_list)),
                4=>array_sum(Set::extract('/CompanyMoneyItem/checkbox_cash',$money_list))
            );
            foreach($checkbox as $id_group=>$check){
                if($check > 0){
                    $this->check_right_form_for_company($id_group,$company_id);
                }
            }



            foreach($money_list as $item){
                $replace_list = array(
                            '##Company.name##' => $company['Company']['name'],
                            '##CompanyWorkPosition.name##' => $item["CompanyWorkPosition"]["name"],
                            '##CompanyMoneyItem.name##' => $item["CompanyMoneyItem"]["name"],
                            '##Ubytovani##' => ($item['CompanyMoneyItem']['cena_ubytovani_na_mesic'] > 0 ?'Ano':'Ne'),
                            '##Doprava##' => ($item['CompanyMoneyItem']['doprava'] > 0 ?'Ano':'Ne'),
                            '##Stravenky##' => ($item['CompanyMoneyItem']['stravenka'] > 0 ?'Ano':'Ne'),
                            '##CompanyMoneyItem.max##' => $item['CompanyMoneyItem']['vypocitana_celkova_cista_maximalni_mzda_na_hodinu']
                );
                /**
                 * odeslani emailu o autorizaci kalkulace
                 * nastaveni komu jiz dle noveho systemu
                 */
                 $this->Email->set_company_data($company['Company']);
                 $this->Email->send_from_template_new(17,array(),$replace_list);
            }
    }

    /**
     * funkce ktera hlida zda pro danou kalkulaci a jeji typ jsou nahrane spravne formulare
     * pro spolecnost
     */
    private function check_right_form_for_company($group_id,$company_id){
        $this->loadModel('FormTemplate');
        $this->loadModel('CompanyFormTemplate');
        $find = $this->CompanyFormTemplate->find('first',array(
            'conditions'=>array(
                'kos'=>0,
                'history'=>0,
                'company_id'=>$company_id,
                'form_template_group_id'=>$group_id
            )
        ));

        /**
         * pokud nema firma prirazenou alespon jednu aktivni
         * sablonu teto skupiny tak je nahraj
         */
        if(!$find){
            $forms = $this->FormTemplate->find('all',array(
               'fields'=>array('id','name','text'),
               'conditions'=>array(
                    'form_template_group_id'=>$group_id,
                    'kos'=>0
               )
            ));

            //nejake takove sablony existuji tak je pridej
            if($forms){
                foreach($forms as $item){
                    $this->CompanyFormTemplate->id = null;
                    $to_save = array(
                        'company_id'=>$company_id,
                        'parent_id'=>$item['FormTemplate']['id'],
                        'form_template_group_id'=>$group_id,
                        'name'=>$item['FormTemplate']['name'],
                        'text'=>$item['FormTemplate']['text']
                    );
                    $this->CompanyFormTemplate->save($to_save);
                }
            }

        }
        unset($this->CompanyFormTemplate);
        unset($this->FormTemplate);
    }



	function send_email_to_directors_about_edit_money_item(){

		// nacteni nazvu spolecnosti
		$this->loadModel('Company');
		$company = $this->Company->read($this->Company->company_data_for_email_notification,$this->data['CompanyMoneyItem']['company_id']);
		unset($this->Company);

		// nacteni nazvu pozice
		$this->loadModel('CompanyWorkPosition'); $this->CompanyWorkPosition = new CompanyWorkPosition();
		$company_work_position = $this->CompanyWorkPosition->read(array('name'),$this->data['CompanyMoneyItem']['company_work_position_id']);
		unset($this->CompanyWorkPosition);

		// nacteni nazvu kalkulace
		$this->loadModel('CompanyMoneyItem'); $this->CompanyMoneyItem = new CompanyMoneyItem();
		$company_money_item = $this->CompanyMoneyItem->read(array('name'),$this->data['CompanyMoneyItem']['id']);
		unset($this->CompanyMoneyItem);

		$replace_list = array(
			'##Company.name##' 				=> $company['Company']['name'],
			'##CompanyMoneyItem.name##' 	=> $company_money_item['CompanyMoneyItem']['name'],
			'##CompanyWorkPosition.name##' 	=> $company_work_position['CompanyWorkPosition']['name'],
			'##CmsUser.name##' 				=> $this->logged_user['CmsUser']['name']
		);

		$this->Email->send_from_template_new(4,array(),$replace_list);
	}

	/**
	* Slouzi pro vytvoreni sub-polozky
	*
	* @param $id
 	* @return view if empty this->data, list od company position if not empty
 	* @access public
 	*/
	function money_add_subitem($id = null){
		$this->autoLayout = false;
		$this->loadModel('CompanyWorkPosition'); $this->CompanyWorkPosition = new CompanyWorkPosition();
		$this->loadModel('CompanyMoneyValidity'); $this->CompanyMoneyValidity = new CompanyMoneyValidity();
		$this->loadModel('CompanyMoneyItem'); $this->CompanyMoneyItem = new CompanyMoneyItem();
		if (empty($this->data)){
			$this->CompanyWorkPosition->bindModel(array('hasOne'=>array('CompanyMoneyValidity'=>array('order'=>'CompanyMoneyValidity.platnost_od DESC','conditions'=>array('CompanyMoneyValidity.kos'=>0)))));
			$this->data = $this->CompanyWorkPosition->read(array('CompanyWorkPosition.id','CompanyWorkPosition.company_id','CompanyWorkPosition.name','CompanyMoneyValidity.id','CompanyMoneyValidity.company_id','CompanyMoneyValidity.fakturacka','CompanyMoneyValidity.platnost_od','CompanyMoneyValidity.platnost_do'), $id);
			$this->render('work_position/add_subitem');
		} else {
			// ulozeni nove pracovni pozice
			$this->CompanyWorkPosition->save($this->data);
			$work_position_id = $this->CompanyWorkPosition->id;

			// ulozeni platnosti
			$this->data['CompanyMoneyValidity']['company_work_position_id'] = $work_position_id;
			$this->CompanyMoneyValidity->save($this->data);
			$money_validity_id = $this->CompanyMoneyValidity->id;

			// zjisteni o jaky se jedna stat kvuli kalkulacce
			$company_stat_info = $this->Company->read(array('stat_id'),$this->data['CompanyMoneyValidity']['company_id']);
			$company_stat_id = $company_stat_info['Company']['stat_id'];

			// nacteni seznamku kalkulaci pro danou platnost
			$parent_money_items = $this->CompanyMoneyItem->findAll(array('CompanyMoneyItem.company_money_validity_id'=>$this->data['CompanyMoneyValidity']['old_money_validity_id']));

			foreach($parent_money_items as $item){
				unset($item['CompanyMoneyItem']['id']);
				$item['CompanyMoneyItem']['fakturacni_sazba_na_hodinu'] = $this->data['CompanyMoneyValidity']['fakturacka'];
				switch ($company_stat_id){
					case 1:
						$to_save['CompanyMoneyItem'] = $this->kalkulacka_cz($item['CompanyMoneyItem']);
						break;
					case 2:
						$to_save['CompanyMoneyItem'] = $this->kalkulacka_sk($item['CompanyMoneyItem']);
						break;
					default:
						die('CHYBA VYBERU JAZYKA KALKULACKY');
				}

				$to_save['CompanyMoneyItem']['company_work_position_id'] = $work_position_id;
				$to_save['CompanyMoneyItem']['company_money_validity_id'] = $money_validity_id;
				$this->CompanyMoneyItem->save($to_save);
				$this->CompanyMoneyItem->id = null;
			}
			$this->pozice($this->data['CompanyWorkPosition']['company_id']);
		}
	}

	/**
 	* AJAX nacteni seznamu pracovnich pozic
 	*
	* @param $company_id
 	* @return view
 	* @access public
	**/
	function load_ajax_position($company_id){
		$this->loadModel('CompanyWorkPosition');
		die(json_encode($this->CompanyWorkPosition->find('list', array(
            'conditions'=>array(
                'company_id'=>$company_id,
                'kos'=>0
            ),
            'order'=>'name ASC'
        ))));
	}

	// /**
 	// * AJAX nacteni seznamu  sub profesi
 	// *
	// * @param $profese
 	// * @return view
 	// * @access public
	// **/
	// function load_sub_position($profese_id){
		// if($profese_id != -1){
			// $this->loadModel('CompanyWorkPosition');
			// $p = array( '-1' => 'Vytvořit novou');
			// die(json_encode($p + $this->CompanyWorkPosition->find('list', array('conditions'=>array('parent_id'=>$profese_id, 'kos'=>0,'test'=>0), 'order'=>'name ASC'))));
		// }
		// die();
	// }

	/**
 	* AJAX nacteni seznamu kalkulaci
 	*
	* @param $position_id
 	* @return view
 	* @access public
	**/
	function load_ajax_money($position_id,$pausal = 0, $h1000 = 0){

		$this->loadModel('CompanyWorkPosition');
        $con = array(
                    'CompanyMoneyItem.kos'=>0
        );
        if($pausal == 1 || $pausal == true)
            $con = am($con , array('pausal'=>1));
        else if($h1000 == 1 || $h1000 == true)
            $con = am($con , array('h1000'=>1));
        else
            $con = am($con , array('pausal'=>0,'h1000'=>0));


		$this->CompanyWorkPosition->bindModel(array(

			'hasOne'=>array(
                'CompanyMoneyValidity'=>array(
                    'fields'=>array('id','platnost_do'),
                    'conditions'=>array('CompanyMoneyValidity.kos'=>0)
                ),
                'CompanyMoneyItem'=>array(
                    'fields'=>array('id','name','company_money_validity_id'),
                    'conditions'=> $con
            ))
		));
		$detail = $this->CompanyWorkPosition->find('all',array(
			'conditions'=>array(
				'and'=>array(
					'OR'=>array(
						'test'=>1,
						array('AND' =>array(
							'test'=>0,
                            'platnost_do'=>'0000-00-00',
                            	'CompanyMoneyItem.company_money_validity_id = CompanyMoneyValidity.id'
						))),
					'CompanyWorkPosition.id'=>$position_id,
					'CompanyWorkPosition.kos'=>0
				)
			)
		));

        $out = array();
        $out = Set::combine($detail,'{n}.CompanyMoneyItem.id','{n}.CompanyMoneyItem.name');


        //$out = array(''=>'Žádne kalkulace');

		die(json_encode($out));

	}
	/**
 	* AJAX nacteni detailu kalkulace
 	*
	* @param $money_id
 	* @return view
 	* @access public
	**/
	function load_ajax_money_detail($money_id = null){
		$this->loadModel('CompanyMoneyItem');
		if ($money_id != null){
			$detail = $this->CompanyMoneyItem->read(null,$money_id);
			unset($detail['CompanyMoneyItem']['id']);
			unset($detail['CompanyMoneyItem']['name']);
			unset($detail['CompanyMoneyItem']['company_work_position_id']);
			die(json_encode($detail['CompanyMoneyItem']));
		} else {
			$cols = $this->CompanyMoneyItem->query('DESCRIBE wapis__company_money_items');
			$detail = array();
			foreach ($cols as $column) {
				$detail[$column['COLUMNS']['Field']] = $column['COLUMNS']['Default'];
			}
			unset($detail['id']);
			unset($detail['name']);
			unset($detail['company_work_position_id']);
			die(json_encode($detail));
		}
	}

	/**
 	* Kalkulacka pro firmy v CZ
 	*
	* @param $data
 	* @return view
 	* @access public
	**/
	function kalkulacka_cz($data){
		extract($data);

		$pocet_hodin_zakladniho_pracovniho_fondu = $normo_hodina * $pocet_dni_v_mesici;
		$celkovy_pocet_hodin_mesicne = $pocet_hodin_zakladniho_pracovniho_fondu + $pocet_hodin_prescasu_mesicne;
		$celkove_naklady_firmy_na_zamestnance_bez_mzdy_mesicne = $cena_ubytovani_na_mesic + $cena_pracovnich_odevu_mesicne + $dalsi_naklady_mesicne + $doprava + $stravenka * $pocet_dni_v_mesici;

		$cista_mzda_zamestnance_mesicne  = round($cista_mzda_z_pracovni_smlouvy_na_hodinu * $celkovy_pocet_hodin_mesicne,2);
		$cista_mzda_mesicne_bez_prispevku  = $cista_mzda_zamestnance_mesicne - $vlozeny_prispevek_na_stravne - $vlozeny_prispevek_na_cestovne;

		$pracovni_smlouva_celkova_hruba_mzda_mesicne = ($cista_mzda_mesicne_bez_prispevku > 10223)? round(($cista_mzda_mesicne_bez_prispevku - 2070)/ (1 - $procentuelni_odvod_zamestnance / 100 - $procenta_dane_z_prijmu / 100 - ($procenta_dane_z_prijmu / 100 * $procentuelni_odvod_zamestnavatele/100)),2):round($cista_mzda_mesicne_bez_prispevku / (1-$procentuelni_odvod_zamestnance/100),2);
		$naklady_mesicne_na_dovolene_svatky = round($pracovni_smlouva_celkova_hruba_mzda_mesicne * (1 + ($procentuelni_odvod_zamestnavatele / 100)) / 20 * $pocet_dni_dovolene_svatku_zapocitanych_do_nakladu / 12,2);
		$celkove_mzdove_naklady_firmy_mesicne_bez_prac_smlouvy  = round($pracovni_smlouva_celkova_hruba_mzda_mesicne * (1 + $procentuelni_odvod_zamestnavatele / 100) +  $vlozeny_prispevek_na_stravne + $vlozeny_prispevek_na_cestovne + $naklady_mesicne_na_dovolene_svatky,2);
		$celkove_mzdove_naklady_firmy_mesicne_bez_zivnostnika = round($cista_mzda_faktura_zivnostnika_na_hodinu * (1 + $vlozena_procenta_nakladu_na_zivnostnika / 100) * $celkovy_pocet_hodin_mesicne,2);

		$celkove_mzdove_naklady_firmy_mesicne_bez_dohody = round( $cista_mzda_dohoda_na_hodinu * (1 + $procenta_dane_z_prijmu_dohoda / 100) * (1 + $odvody_dohoda / 100) * $celkovy_pocet_hodin_mesicne ,2);

		$celkove_mzdove_naklady_firmy_mesicne_bez_cash = round($cista_mzda_cash_na_hodinu*(1 + $vlozena_procenta_nakladu_na_cash / 100) * $celkovy_pocet_hodin_mesicne,2);
		$celkove_naklady_firmy_na_zamestnance_mesicne = $celkove_naklady_firmy_na_zamestnance_bez_mzdy_mesicne + $celkove_mzdove_naklady_firmy_mesicne_bez_prac_smlouvy + $celkove_mzdove_naklady_firmy_mesicne_bez_zivnostnika + $celkove_mzdove_naklady_firmy_mesicne_bez_dohody + $celkove_mzdove_naklady_firmy_mesicne_bez_cash;
		$celkova_fakturace_mesicne = round($fakturacni_sazba_na_hodinu * $celkovy_pocet_hodin_mesicne);
		$provinzni_marze = round(($celkova_fakturace_mesicne - $celkove_naklady_firmy_na_zamestnance_mesicne) / $celkova_fakturace_mesicne * 100,2);
		$vypocitana_celkova_cista_maximalni_mzda_na_hodinu = $cista_mzda_z_pracovni_smlouvy_na_hodinu + $cista_mzda_faktura_zivnostnika_na_hodinu + $cista_mzda_dohoda_na_hodinu + $cista_mzda_cash_na_hodinu;

		return compact(array_keys($data));
	}

	/**
 	* Kalkulacka pro firmy v SK
 	*
	* @param $data
 	* @return view
 	* @access public
	**/
	function kalkulacka_sk($data){
		extract($data);
		$danovy_bonus_celkem = $danovy_bonus_za_kazde_dite * $pocet_deti;
		$pocet_hodin_zakladniho_pracovniho_fondu = $normo_hodina * $pocet_dni_v_mesici;
		$celkovy_pocet_hodin_mesicne = $pocet_hodin_zakladniho_pracovniho_fondu + $pocet_hodin_prescasu_mesicne;
		$celkove_naklady_firmy_na_zamestnance_bez_mzdy_mesicne = $cena_ubytovani_na_mesic + $cena_pracovnich_odevu_mesicne + $dalsi_naklady_mesicne + $doprava + $stravenka * $pocet_dni_v_mesici;
		$cista_mzda_zamestnance_mesicne  = round($cista_mzda_z_pracovni_smlouvy_na_hodinu * $celkovy_pocet_hodin_mesicne,2);
		$cista_mzda_mesicne_bez_prispevku  = $cista_mzda_zamestnance_mesicne - $vlozeny_prispevek_na_stravne - $vlozeny_prispevek_na_cestovne;
		$pracovni_smlouva_celkova_hruba_mzda_mesicne = ($cista_mzda_mesicne_bez_prispevku > 335.5)?round(($cista_mzda_mesicne_bez_prispevku - ($odpocitatelna_polozka / 100 * $procenta_dane_z_prijmu) - $danovy_bonus_celkem)/ (1 - $procentuelni_odvod_zamestnance / 100 - $procenta_dane_z_prijmu / 100 + ($procenta_dane_z_prijmu / 100 * $procentuelni_odvod_zamestnance/100)),2):round(($cista_mzda_mesicne_bez_prispevku - $danovy_bonus_celkem)/ (1-$procentuelni_odvod_zamestnance/100),2);
		$naklady_mesicne_na_dovolene_svatky = round( $pracovni_smlouva_celkova_hruba_mzda_mesicne * (1 - ($procentuelni_odvod_zamestnance / 100)) / 20 * $pocet_dni_dovolene_svatku_zapocitanych_do_nakladu / 12 ,2);
		$celkove_mzdove_naklady_firmy_mesicne_bez_prac_smlouvy  = round( $pracovni_smlouva_celkova_hruba_mzda_mesicne * (1 + $procentuelni_odvod_zamestnavatele / 100) +  $vlozeny_prispevek_na_stravne + $vlozeny_prispevek_na_cestovne + $naklady_mesicne_na_dovolene_svatky ,2);
		$celkove_mzdove_naklady_firmy_mesicne_bez_zivnostnika = round($cista_mzda_faktura_zivnostnika_na_hodinu * (1 + $vlozena_procenta_nakladu_na_zivnostnika / 100) * $celkovy_pocet_hodin_mesicne,2);
		$temp_dohoda = (($cista_mzda_dohoda_na_hodinu * $celkovy_pocet_hodin_mesicne) > $odpocitatelna_polozka)? (($cista_mzda_dohoda_na_hodinu * $celkovy_pocet_hodin_mesicne) - ($odpocitatelna_polozka * ($procenta_dane_z_prijmu_dohoda/100)))/ (1 - $procenta_dane_z_prijmu_dohoda / 100): ($cista_mzda_dohoda_na_hodinu * $celkovy_pocet_hodin_mesicne);
		$celkove_mzdove_naklady_firmy_mesicne_bez_dohody =  round($temp_dohoda * (1 + $odvody_dohoda /100),2);
		$celkove_mzdove_naklady_firmy_mesicne_bez_cash = round($cista_mzda_cash_na_hodinu*(1 + $vlozena_procenta_nakladu_na_cash / 100) * $celkovy_pocet_hodin_mesicne,2);
		$celkove_naklady_firmy_na_zamestnance_mesicne = $celkove_naklady_firmy_na_zamestnance_bez_mzdy_mesicne + $celkove_mzdove_naklady_firmy_mesicne_bez_prac_smlouvy + $celkove_mzdove_naklady_firmy_mesicne_bez_zivnostnika + $celkove_mzdove_naklady_firmy_mesicne_bez_dohody + $celkove_mzdove_naklady_firmy_mesicne_bez_cash;
		$celkova_fakturace_mesicne = round($fakturacni_sazba_na_hodinu *$celkovy_pocet_hodin_mesicne,2);
		$provinzni_marze = round(($celkova_fakturace_mesicne - $celkove_naklady_firmy_na_zamestnance_mesicne) / $celkova_fakturace_mesicne * 100,2);
		$vypocitana_celkova_cista_maximalni_mzda_na_hodinu = $cista_mzda_z_pracovni_smlouvy_na_hodinu + $cista_mzda_faktura_zivnostnika_na_hodinu + $cista_mzda_dohoda_na_hodinu + $cista_mzda_cash_na_hodinu;
		return compact(array_keys($data));
	}

	/**
	* Slouzi pro zobrazeni nadefinovanych fakturaci k dane firme
	*
	* @param $company_id
 	* @return view  list of company position
 	* @access public
 	*/

	function fakturacka($company_id){
		$comp = $this->Company->read('stat_id',$company_id);
		$this->set('stat_id',$comp['Company']['stat_id']);

		// nacteni potrebnych DB modelu
		$this->loadModel('CompanyInvoiceItem');
		// nacteni fakturaci k dane spolecnosti
		$this->set('invoice_item_list', $pp =  $this->CompanyInvoiceItem->findAll(array('CompanyInvoiceItem.company_id'=>$company_id,'CompanyInvoiceItem.kos'=>0)));

		// predani ID spolecnosti do View
		$this->set('company_id',$company_id);

		// uvolneni modelu z pameti
		unset($this->CompanyInvoiceItem);

		// vyrendrovani seznamu pozic
		$this->render('invoice_item/index');
	}

	function invoice_item_edit($company_id = null, $id = null) {

		$this->loadModel('CompanyInvoiceItem');
		if (empty($this->data)){
			if($this->logged_user["CmsGroup"]["permission"]["report_invoice_items"]["add"]==2){
				$podminka = array(
					$this->logged_user["CmsGroup"]["permission"]["companies"]["group_id"]=>$this->logged_user["CmsUser"]["id"],
					'kos'=>0
				);
			} else
				$podminka = array('kos'=>0);

			if ($company_id == -1){
				$this->set('company_list', $this->Company->find('list', array('order'=>'name ASC','conditions'=>array($podminka))));
				$this->set('from_report', true);
			}

			if ($id != null) {
				$this->data =  $this->CompanyInvoiceItem->read(null, $id);
				if ($company_id == -1){
					$this->Company->bindModel(array('belongsTo' => array(
						'SalesManager' 	=> array('className' => 'CmsUser', 'foreignKey' => 'self_manager_id'),
						'ClientManager' => array('className' => 'CmsUser', 'foreignKey' => 'client_manager_id')
					)));
					$c = $this->Company->read(array('Company.ico','SalesManager.name', 'ClientManager.name'),$this->data['CompanyInvoiceItem']['company_id']);
					$this->data['Company']['ico'] = $c['Company']['ico'];
					$this->data['Company']['cm'] = $c['ClientManager']['name'];
					$this->data['Company']['sm'] = $c['SalesManager']['name'];
				}
			} else
				$this->data['CompanyInvoiceItem']['company_id'] = $company_id;
			$this->render('invoice_item/edit');
		} else {
			$this->CompanyInvoiceItem->save($this->data);
			$this->fakturacka($this->data['CompanyInvoiceItem']['company_id']);
		}
	}

	function nabor($company_id){
		// nacteni potrebnych DB modelu
		$this->loadModel('RequirementsForRecruitment'); $this->RequirementsForRecruitment = new RequirementsForRecruitment();

		// natahnuti polozek z DB
		$this->set('items',$this->RequirementsForRecruitment->findAll(array('company_id'=>$company_id)));

		// predani ID spolecnosti do View
		$this->set('company_id',$company_id);

		// vyrendrovani seznamu naboru
		$this->render('requirements_for_recruitment/index');
	}

	function nabor_edit($company_id = null, $id =null,$print = null){
	//Configure::write('debug',2);

		$this->set('print',false);

		// nacteni potrebnych DB modelu
		$this->loadModel('RequirementsForRecruitment');
		$this->set('CmsGroupId',$this->logged_user['CmsGroup']['id']);

		//nastaveni layoutu pro tiskove sestavy
		if($print != null){
			$this->autoLayout = true;
			$this->layout = 'print';
			$this->set('print',true);
		}


		if (empty($this->data)){

			$this->loadModel('SettingCareerItem');
			$this->set('setting_career_list', $this->SettingCareerItem->find('list',array('order'=>'name ASC')));
			unset($this->SettingCareerItem);

			// load kategorii a jejich prize
			$this->loadModel('SettingCareerCat');
			$career_cats = $this->SettingCareerCat->find('all',array(
					'fields'=>array(
						'id',
						'name',
						'prize'
					),
					'order'=>'name ASC'
			));

			$carrer_cat_list = array();
			$carrer_cat_title = array();
			if ($career_cats){
				foreach($career_cats as $cat_id=>$item){
					$carrer_cat_list[$item['SettingCareerCat']['id']] = $item['SettingCareerCat']['name'];
					$carrer_cat_title[$item['SettingCareerCat']['id']] = $item['SettingCareerCat']['name'].'|'.$item['SettingCareerCat']['prize'];
				}
			}
			$this->set('carrer_cat_list', $carrer_cat_list);
			$this->set('carrer_cat_title', $carrer_cat_title);

			//zobraz firmy jen ktere patri pod nej a je CM,SM,COO
			if(!in_array($this->logged_user["CmsGroup"]["id"] ,array(1,5,8,9))){
				$this->set('company_list', $this->Company->find('list',array('conditions'=>array('AND'=>array('kos'=>0,'OR'=>array('client_manager_id'=>$this->logged_user["CmsUser"]["id"],'self_manager_id'=>$this->logged_user["CmsUser"]["id"],'coordinator_id'=>$this->logged_user["CmsUser"]["id"],'coordinator_id2'=>$this->logged_user["CmsUser"]["id"]))),'order'=>'name ASC')));
			// adminum vsechny
			} else
				$this->set('company_list', $this->Company->find('list',array('order'=>'name ASC','conditions'=>array('kos'=>0))));

			if ($id != null){
				$this->data = $this->RequirementsForRecruitment->read(null,$id);
				// pokud je uz odsouhlasen externi nabor, zamez COO,SM,CM v editaci
				$externi = $this->data['RequirementsForRecruitment']['checkbox_interni_nabor'];
				if(in_array($this->logged_user["CmsGroup"]["id"],array(2,3,4)) && $externi==1)
					$this->set('disabledSave',true);

				// load CM kontaktu
				$this->Company->bindModel(array('belongsTo' => array(
				'ClientManager' => array('className' => 'CmsUser', 'foreignKey' => 'client_manager_id'))));
				$cm_info = $this->Company->read(array('ClientManager.email','ClientManager.telefon','ClientManager.name','ClientManager.id'),$this->data['RequirementsForRecruitment']['company_id']);
				
					//predani promenych CM kontaktu
					$this->data['RequirementsForRecruitment']['company_contact_id'] = $cm_info['ClientManager']['id'];
					$this->data['RequirementsForRecruitment']['cm_name'] = $cm_info['ClientManager']['name'];
					$this->data['RequirementsForRecruitment']['cm_tel'] = $cm_info['ClientManager']['telefon'];
					$this->data['RequirementsForRecruitment']['cm_email'] = $cm_info['ClientManager']['email'];

						
				$this->loadModel('CompanyWorkPosition'); 
				$this->set('company_position_list', $this->CompanyWorkPosition->find('list',array('order'=>'name ASC','conditions'=>array('company_id'=>$this->data['RequirementsForRecruitment']['company_id'], 'parent_id'=>-1, 'kos'=>0 , 'test'=>0))));
				unset($this->CompanyWorkPosition);
			} else {
				$this->data['RequirementsForRecruitment']['company_id'] = $company_id;
				$this->set('company_position_list',array());
			}

			if ($company_id == -1){
				$this->set('z_reportu', true);		
			}

			//render
			if ($company_id == -2 && $print != 1)
				$this->render('requirements_for_recruitment/show');	
			else if($print == 1)
				$this->render('requirements_for_recruitment/souhrn');	
			else
				$this->render('requirements_for_recruitment/edit');


		} else { 
        //Save
			if($this->data['RequirementsForRecruitment']['id']=="")
				$this->data['RequirementsForRecruitment']['cms_user_id'] = $this->logged_user['CmsUser']['id'];
			if($this->data['RequirementsForRecruitment']['publikovani_typ']==1){
				$this->data['RequirementsForRecruitment']['checkbox_interni_nabor'] = 1;
				$this->data['RequirementsForRecruitment']['datum_interni'] = date('Y-m-d H:i:s');
			}
			if($this->data['RequirementsForRecruitment']['publikovani_typ']==2){
				$this->data['RequirementsForRecruitment']['checkbox_interni_nabor'] = 1;
				$this->data['RequirementsForRecruitment']['checkbox_externi_nabor'] = 1;
				$this->data['RequirementsForRecruitment']['datum_externi'] = date('Y-m-d H:i:s');
			}


			if ($this->RequirementsForRecruitment->save($this->data))
				die(json_encode(array('result'=>true)));
			else
				die(json_encode(array('result'=>false)));
			//$this->nabor($this->data['RequirementsForRecruitment']['company_id']);
			die();
		}
	}
	
	function load_cms_user_kontakt($id){
		$this->loadModel('CmsUser'); 
		die(json_encode($this->CmsUser->read(array('CmsUser.telefon', 'CmsUser.email'),$id)));
		
	}
	
	function load_position_group($position_id){
		$this->loadModel('SettingCareerItem');
		die(json_encode(array('result'=>true,'selected'=> av(av($this->SettingCareerItem->read(array('setting_career_cat_id'),$position_id),'SettingCareerItem'),'setting_career_cat_id'))));
	}
	
	
	function load_profesion_by_company($company_id){
		$this->Company->bindModel(array('belongsTo' => array(
				'ClientManager' => array('className' => 'CmsUser', 'foreignKey' => 'client_manager_id'))));
		$cm_info = $this->Company->read(array('ClientManager.email','ClientManager.telefon','ClientManager.name','ClientManager.id','Company.mesto'),$company_id);

		$this->loadModel('CompanyWorkPosition'); 
		die(json_encode(array(
			'result'=>true,
			'data'	=>array(
				'kalkulace'	=> $this->CompanyWorkPosition->find('list',array('order'=>'name ASC','conditions'=>array('company_id'=>$company_id, 'parent_id'=>-1, 'kos'=>0))),
				'mesto' 	=> $cm_info['Company']['mesto'],
				'company_contact_id' 	=> $cm_info['ClientManager']['id'],
				'cm_name' 	=> $cm_info['ClientManager']['name'],
				'cm_tel' 	=> $cm_info['ClientManager']['telefon'],
				'cm_email' 	=> $cm_info['ClientManager']['email']
			)
		)));
	}
	
	/**
 	*upload fotografie do nabor
 	*
 	* @return view
 	* @access public
	**/
	function requirement_upload_foto(){
		$this->Upload->set('data_upload',$_FILES['upload_file']);
		if ($this->Upload->doit(json_decode($this->data['upload']['setting'],true))){
			echo json_encode(array('upload_file'=>array('name'=>$this->Upload->get('outputFilename')),'return'=>true));
		} else 
			echo json_encode(array('return'=>false,'message'=>$this->Upload->get('error_message')));		
		die();
	}

	/**
	 * Seznam šablon
	* @param $company_id
 	* @return view list of company template
 	* @access public
	 */
	
	function template($company_id = null){
		if ($company_id != null){
			$this->loadModel('CompanyOrderTemplate');
			$this->set('template_list', $this->CompanyOrderTemplate->find(
				'all',
				array(
					'conditions'=>array(
						'company_id' 	=> $company_id,
						'kos'			=> 0
					)
				)
			));
			$this->set('company_id',$company_id);
			$this->render('templates/index');
		}
	}
	
	/**
	 * editace nebo pridani template
	* @param $company_id
	* @param $id
 	* @return view edit of company template
 	* @access public
	 */
	function template_edit($company_id = null, $id=null, $show = false){
		$this->loadModel('CompanyOrderTemplate');
		if (empty($this->data)){
            /**
		     * nastaveni show pro js a edit
             * v pripdate true, vse zneaktivni a da disabled
		     */
             if($show)
                $this->set('show',$show);
    
		
			/*
			 * Nacteni seznamu profesi ze settingu
			 */
			$this->loadModel('SettingCareerItem'); 
			$this->set('setting_career_list', $this->SettingCareerItem->find('list',array('order'=>'name ASC')));
			unset($this->SettingCareerItem);
						
			/*
			 * Nacteni pracovnich pozic nadefinovanych v kalkulacich pro danou spolecnost
			 * serazene dle nazvu profese, parent_id - ty co jsou hlavni, ne varianty, test - a ty, ktere nejsou testovaci
			 */
			$this->loadModel('CompanyWorkPosition'); 
			//$this->set('company_position_list', $this->CompanyWorkPosition->find('list',array('order'=>'name ASC','conditions'=>array('company_id'=>$company_id, 'parent_id'=>-1, 'kos'=>0 , 'test'=>0))));
			$this->CompanyWorkPosition->bindModel(array('hasOne'=>array('CompanyMoneyValidity')));
			$this->set(
				'company_position_list', 
				$this->CompanyWorkPosition->find(
					'list',
					array(
						'conditions'=>array(
							'CompanyWorkPosition.company_id' => $company_id,
							//'CompanyWorkPosition.parent_id'=> -1, // ?? DOTAZ, ZDA TO TAK JESTE JE
							'CompanyWorkPosition.test'=>0,
							'CompanyWorkPosition.kos' => 0,
							'CompanyMoneyValidity.platnost_do' => '0000-00-00'
						),
						'order'=>'CompanyWorkPosition.name ASC',
						'recursive'=>1
					)
				)
			);
            unset($this->CompanyWorkPosition);	
            
            

			/*
			 * natahnuti informaci o firme
			 * nazev spolecnosti, mesto, kontakt na Cm a Koo1, Koo2
			 */ 
			$this->Company->bindModel(array('belongsTo'=>array(
				'ClientManager' => array('className'	=>'CmsUser','foreignKey'	=> 'client_manager_id'),
				'Koordinator1' => array('className'	=>'CmsUser','foreignKey'	=> 'coordinator_id'),
				'Koordinator2' => array('className'	=>'CmsUser','foreignKey'	=> 'coordinator_id2'),
			)));
			$this->set(
                'company_info',
                $company_info = $this->Company->read(
                    array(
                         'Company.name',
                         'Company.mesto',
                         'ClientManager.at_employee_id',
                         'ClientManager.name',
                         'ClientManager.email',
                         'ClientManager.telefon',
                         'Koordinator1.at_employee_id',
                         'Koordinator1.name',
                         'Koordinator1.email',
                         'Koordinator1.telefon',
                         'Koordinator2.at_employee_id',
                         'Koordinator2.name',
                         'Koordinator2.email',
                         'Koordinator2.telefon'
                    ),
                    $company_id
            ));
			$this->set('company_id', $company_id);

            $this->loadModel('Client');
            $tels = $this->Client->find(
                'all',
                array(
                    'conditions' => array(
                        'id' => array(
                            $company_info['ClientManager']['at_employee_id'],
                            $company_info['Koordinator1']['at_employee_id'],
                            $company_info['Koordinator2']['at_employee_id']
                        )
                    ),
                    'fields' => array(
                        'id',
                        'mobil2'
                    )
                )
            );
            $this->set('phone_list',Set::Combine($tels,"{n}.Client.id","{n}.Client.mobil2"));
			/*
			 * narvani info o mestu do this->data, pokud se pote jedna o editaci
			 * tak to stejne read prevali
			 */
			$this->data['CompanyOrderTemplate']['job_city'] = $company_info['Company']['mesto'];
			
			/*
			 * pokud se jedna o editaci, natahni data 
			 */
			if ($id != null){
				$this->data = $this->CompanyOrderTemplate->read(null, $id); 
				self::load_kalkulace($this->data['CompanyOrderTemplate']['company_work_position_id'], false);
			} 
			
			/*
			 * render view
			 */
			$this->render('templates/edit');
		} else {
			if ($this->CompanyOrderTemplate->save($this->data)){
				die(json_encode(array('result'=>true)));
			} else {
				die(json_encode(array('result'=>false,'message'=>'Chyba behem ukladani')));			
			}
			
		}
	}
    
   /**
 	* Presun sablony do kose
 	*/
	function template_trash($company_id, $id){
		$this->loadModel('CompanyOrderTemplate'); 
		$this->CompanyOrderTemplate->save(array('CompanyOrderTemplate'=>array('kos'=>1,'id'=>$id)));
		unset($this->CompanyOrderTemplate);
        die(json_encode(array('result'=>true)));
	}
	
	/**
	 * nacteni kalkulaci (money items) pro danou pozici do 
	 * editace template
	 * @param $company_id
	 * @param $render, if render is true, render element
 	 * @return view list of company money items
 	 * @access public
	 */
	function load_kalkulace($position_id, $render = true){
		$this->loadModel('CompanyMoneyItem');
        $this->CompanyMoneyItem->bindModel(array('belongsTo'=>array('CompanyMoneyValidity')));
		$this->set('kalkulace_list', $this->CompanyMoneyItem->find(
			'all',
			array(
				'conditions'=>array(
					'CompanyMoneyItem.company_work_position_id' => $position_id,
                    'CompanyMoneyItem.kos'=>0,
                    'CompanyMoneyValidity.platnost_do' => '0000-00-00'
				)
			)
		));
		if ($render == true)
			$this->render('templates/kalkulace');
	}           

	/**
	 * Seznam objednavek
	* @param $company_id
 	* @return view list of company order
 	* @access public
	 */
	
	function order($company_id = null){
		if ($company_id != null){
			$this->loadModel('CompanyOrderItem');
			$this->set('order_list', $this->CompanyOrderItem->find(
				'all',
				array(
                    'fields'=>array(
                        'CompanyOrderItem.*',
                        'CONCAT_WS(" + ",CompanyOrderItem.count_of_free_position,CompanyOrderItem.count_of_substitute) as objednavka,
                         CONCAT_WS(" / ",CompanyOrderItem.count_of_free_position,
                            IF(
                             CompanyOrderItem.order_status = 1,
                                (SELECT 
                                    COUNT(c.id) 
                                 FROM wapis__connection_client_requirements as c
                                 Where 
                                    c.company_id = CompanyOrderItem.company_id AND 
                                    c.requirements_for_recruitment_id = CompanyOrderItem.id AND
                                    type = 2
                                 ),
                             CompanyOrderItem.statistika
                            )
                         ) as statistika,
                         IF((CompanyOrderItem.start_datetime < NOW() && CompanyOrderItem.order_status IN (-1,1)), 1 , 0) as expire'
                    ),
					'conditions'=>array(
						'company_id' 	=> $company_id,
						'kos'			=> 0
					)
				)
			));
			$this->set('company_id',$company_id);
			$this->render('orders/index');
		}
	}
	
	/**
	 * editace nebo pridani template
	* @param $company_id
	* @param $id
	* @param $show
	* @param $generate
 	* @return view edit of company template
 	* @access public
	 */
	function order_edit($company_id = null, $id = null,$show = false,$generate = false) {
	  //Configure::write('debug',1);
		$this->loadModel('CompanyOrderItem');
		if (empty($this->data)){
		    /**
		     * nastaveni show pro js a edit
             * v pripdate true, vse zneaktivni a da disabled
		     */
             if($show == true && $show != -1)
                $this->set('show',$show);
            
            $this->set('permission',$permision = $this->logged_user['CmsGroup']['permission']['companies']);


            $this->set('country_list',$this->get_list('Countrie'));

			/*
			 * Nacteni seznamu profesi ze settingu
			 */
			$this->loadModel('SettingCareerItem'); 
			$this->set('setting_career_list', $this->SettingCareerItem->find('list',array('order'=>'name ASC')));
			unset($this->SettingCareerItem);
						
			/*
			 * Nacteni pracovnich pozic nadefinovanych v kalkulacich pro danou spolecnost
			 * serazene dle nazvu profese, parent_id - ty co jsou hlavni, ne varianty, test - a ty, ktere nejsou testovaci
			 */
			$this->loadModel('CompanyWorkPosition'); 
			$this->set('company_position_list', $this->CompanyWorkPosition->find('list',array('order'=>'name ASC','conditions'=>array('company_id'=>$company_id, 'kos'=>0 , 'test'=>0))));
			unset($this->CompanyWorkPosition);	

			/*
			 * natahnuti informaci o firme
			 * nazev spolecnosti, mesto, kontakt na Cm a Koo1, Koo2
			 */ 
			$this->Company->bindModel(array('belongsTo'=>array(
				'ClientManager' => array('className'	=>'CmsUser','foreignKey'	=> 'client_manager_id'),
				'Koordinator1' => array('className'	=>'CmsUser','foreignKey'	=> 'coordinator_id'),
				'Koordinator2' => array('className'	=>'CmsUser','foreignKey'	=> 'coordinator_id2'),
			)));
			$this->set('company_info', $company_info = $this->Company->read(
                array(
                         'Company.name',
                         'Company.mesto',
                         'ClientManager.at_employee_id',
                         'ClientManager.name',
                         'ClientManager.email',
                         'ClientManager.telefon',
                         'Koordinator1.at_employee_id',
                         'Koordinator1.name',
                         'Koordinator1.email',
                         'Koordinator1.telefon',
                         'Koordinator2.at_employee_id',
                         'Koordinator2.name',
                         'Koordinator2.email',
                         'Koordinator2.telefon'
                 ),$company_id));
			$this->set('company_id', $company_id);
			
            $this->loadModel('Client');
            $tels = $this->Client->find(
                'all',
                array(
                    'conditions' => array(
                        'id' => array(
                            $company_info['ClientManager']['at_employee_id'],
                            $company_info['Koordinator1']['at_employee_id'],
                            $company_info['Koordinator2']['at_employee_id']
                        )
                    ),
                    'fields' => array(
                        'id',
                        'mobil2'//firemni
                    )
                )
            );
            //pr($tels);
            $this->set('phone_list',Set::Combine($tels,"{n}.Client.id","{n}.Client.mobil2"));
            
			/*
			 * narvani info o mestu do this->data, pokud se pote jedna o editaci
			 * tak to stejne read prevali
			 */
			$this->data['CompanyOrderTemplate']['job_city'] = $company_info['Company']['mesto'];
			
			/*
			 * pokud se jedna o editaci, natahni data 
			 */
			if ($id != null){
				$this->data = $this->CompanyOrderItem->read(null, $id); 
				self::load_kalkulace($this->data['CompanyOrderItem']['company_work_position_id'], false);
			} 
			
			/*
			 * Natahnuti seznamu sablon 
			 */
			$this->loadModel('CompanyOrderTemplate');
			$this->set('template_list',$this->CompanyOrderTemplate->find(
				'list',
				array(
					'conditions'=>array(
						'company_id'=>$company_id,
						'kos'=>0
					)
				)
			));

            $this->set('cm_koo_list',$this->get_list_of_superrior(2));

            /**
             * pokud se jedna o generovani tak se odstrani ID po vsech nacteni
             */   
            if($generate){   
               unset($this->data['CompanyOrderItem']['id']);
               $this->set('generate',$id);
            }
            else
                $this->set('generate',false); 
            
			/*
			 * render view
			 */
			$this->render('orders/edit');
		} else {
		    if($this->data['CompanyOrderItem']['id'] == '')
                $this->data['CompanyOrderItem']['cms_user_id'] = $this->logged_user['CmsUser']['id'];
            
			if ($this->CompanyOrderItem->save($this->data)){
                if(isset($this->data['CompanyOrderItem']['generate'])){
                    if(!self::prenes_lidi_z_objednavek($this->data['CompanyOrderItem']['generate'],$this->CompanyOrderItem->id))
                        die(json_encode(array('result'=>false,'message'=>'Chyba behem prenaseni klientu')));	
                } 
                
                if($this->data['CompanyOrderItem']['id'] == ''){
                    $this->loadModel('Company');
            		$company = $this->Company->read($this->Company->company_data_for_email_notification,$this->data['CompanyOrderItem']['company_id']);
            		unset($this->Company);
                    
                    $this->loadModel('CompanyOrderTemplate');
            		$template = $this->CompanyOrderTemplate->read(array('name'),$this->data['CompanyOrderItem']['order_template_id']);
            		unset($this->CompanyOrderTemplate);
            
            		$replace_list = array(
            			'##Company.name##' 			=>$company['Company']['name'],
                        '##CmsUser.name##' 			=>$this->logged_user['CmsUser']['name'],
            			'##Order.name##' 	=>$this->data['CompanyOrderItem']['name'],
            			'##Template.name##' 	=>$template['CompanyOrderTemplate']['name'],
            			'##Order.type##' 	=>$this->order_type_list[$this->data['CompanyOrderItem']['order_type']],
            			'##Order.order_date##' 	=>date('d.m.Y',strtotime($this->data['CompanyOrderItem']['order_date'])),
                        '##Order.stav##' 	=>$this->order_stav_list[$this->data['CompanyOrderItem']['order_status']],
                        '##Order.count_of_free_position##' 	=>$this->data['CompanyOrderItem']['count_of_free_position'],
                        '##Order.count_of_substitute##' 	=>$this->data['CompanyOrderItem']['count_of_substitute'],
                        '##Order.start_datetime##' 	=>date('d.m.Y H:i:s',strtotime($this->data['CompanyOrderItem']['start_datetime'])),
                        '##Order.start_place##' 	=>$this->data['CompanyOrderItem']['start_place'],
                        '##Order.start_comment##' 	=>$this->data['CompanyOrderItem']['start_comment']
            		);
            
                   /**
                    * nove nastaveni componenty
                    * nastaveni dat spolecnosti name, id jestlivych skupin CM, SM, COO, COO2
                    */
            	    $this->Email->set_company_data($company['Company']);
                    $this->Email->send_from_template_new(9,array(),$replace_list);
                    
                   // $this->Email->send_from_template_new(34,array(),$replace_list);
                }
                
				die(json_encode(array('result'=>true)));
			} else {
				die(json_encode(array('result'=>false,'message'=>'Chyba behem ukladani')));			
			}
			
		}
	}
    
    /**
     * funkce pro vymazani objednavky
     */
     function order_trash($id){
        $this->loadModel('CompanyOrderItem');
        $this->CompanyOrderItem->id = $id;
        if($this->CompanyOrderItem->saveField('kos',1))       
        	die(json_encode(array('result'=>true)));
        else
            die(json_encode(array('result'=>false)));
     }
    
    
    
    /**
     * funkce ktera kopiruje lidi z generovane objednavky na novou
     * @created 27.11.09
     * @author Sol
     */
    function prenes_lidi_z_objednavek($order_id, $new_order_id){
            $this->loadModel('ConnectionClientRequirement');
           
            /**
             * hledame seznam vsech zaznamu z nominacnich listin
             * z generovane sablony
             */
            $nominacni_listina = $this->ConnectionClientRequirement->find('all',array(
                'fields'=>array('requirements_for_recruitment_id','client_id','company_id','cms_user_id','type'),
                'conditions'=>array(
                   'requirements_for_recruitment_id'=>$order_id,
                   'type'=>3
                )
            ));
            
            /**
             * pokud byl nekdo nalezen
             * projdi tento seznam a zamen id objednavky za nove id objednavky kterou jsme ulozili
             */
            if($nominacni_listina){
                foreach($nominacni_listina as $id=>$item)
                    $nominacni_listina[$id]['ConnectionClientRequirement']['requirements_for_recruitment_id'] = $new_order_id;
                
                /**
                 * CakePhp funkce SaveAll
                 * ktera uklada i pole prvku najednou.
                 * Musi mit vsechny modely ktere jsou v danem poli pouzity
                 */
                if(!$this->ConnectionClientRequirement->saveAll($nominacni_listina))
                    return false;
            }    
            
            unset($this->ConnectionClientRequirement);
        return true;
    }
    
    
    /**
     * funkce ktera uzavira objednavku, meni order_status na 2
     * @param $order_id
     * @return result json_encode
     * @created 26.11.09
     * @author Sol
     */
    function order_close($order_id = null, $type = 2){
        if($order_id != null){
            $this->loadModel('CompanyOrderItem');
            $this->loadModel('ConnectionClientRequirement');
            
            /**
             * SQL dotaz na vyhledani zamestannych a tech na cekaci listine
             * Usetrime si dva dotazy, pri pouziti groupu a prislusne navoleneho GROUP_CONCAT :-)
             */
            $this->ConnectionClientRequirement->bindModel(array(
                'belongsTo'=>array('Client')
            ));
            $count = $this->ConnectionClientRequirement->find('all',array(
                'fields'=>array('type','Count(type) as pocet','GROUP_CONCAT(Client.name SEPARATOR "|") as clients'),
                'conditions'=>array(
                    'requirements_for_recruitment_id'=>$order_id,
					'objednavka' => 1,
                    'type'=>array(1,2)
                ),
                'group'=>'type'
            ));
            
            
            /**
             * osetreni zda nekdo neni na cekaci listine, pokud neni vytvor count kolik je zamestanych
             * pokud existuje type == 1 tak vypis kdo je na cekaci listine 
             */
            if($count) 
                if($count[0]['ConnectionClientRequirement']['type'] == 2)
                    $count = $count[0][0]['pocet'];
                else if($count[0]['ConnectionClientRequirement']['type'] == 1){    
                    $tmp = explode('|',$count[0][0]['clients']);
                    die(json_encode(array('result'=>false,'message'=>"Chyba : Čekací listina není prázdna! Čekací listina obsahuje:\n\n".implode($tmp,"\n"))));		
                }
                else
                    die(json_encode(array('result'=>false,'message'=>"Chyba : Špatně nastavené connection")));
                  
                  
            /**
             * konecny save objednavky 
             */      
            $this->CompanyOrderItem->id = $order_id;
            $this->CompanyOrderItem->saveField('order_status',$type);
            $this->CompanyOrderItem->saveField('closed_comment',$this->data['text']);
            $this->CompanyOrderItem->saveField('closed_date',date("Y-m-d H:i:s"));
            $this->CompanyOrderItem->saveField('statistika',(isset($count) ? $count : 0));

           	/*
 			 * Odstranit klienty ze vsech nominacnich listin, portoze pro uzavrenou jiz nejsou potreba
 			 */
 			$this->ConnectionClientRequirement->updateAll(
				array('ConnectionClientRequirement.type' => '-1'),
		    	array(
                    'ConnectionClientRequirement.requirements_for_recruitment_id'=>$order_id,
                    'ConnectionClientRequirement.objednavka'=>1,
                    'ConnectionClientRequirement.type' => 3
                )
		 	);
            
            /**
             * notifikace o uzavrene objednavce
             */
            if($type == 3){
                $detail_order = $this->CompanyOrderItem->read(array('company_id','name'),$order_id);
                $company = $this->Company->read($this->Company->company_data_for_email_notification,$detail_order['CompanyOrderItem']['company_id']);
                
                $replace_list = array(
                    '##CompanyOrderItem.name##'=>$detail_order['CompanyOrderItem']['name'],
                    '##Company.name##'=>$company['Company']['name'],
                    '##CmsUser.name##'=>$this->logged_user['CmsUser']['name'],
                    '##Comment##'=>$this->data['text']
                );
                
               /**
                * nove nastaveni componenty
                * nastaveni dat spolecnosti name, id jestlivych skupin CM, SM, COO, COO2
                */
        	    $this->Email->set_company_data($company['Company']);
                $this->Email->send_from_template_new(20,array(),$replace_list);
                
            }
            
            
            unset($this->CompanyOrderItem);
            unset($this->ConnectionClientRequirement);
    		die(json_encode(array('result'=>true)));
        }
        else
            die(json_encode(array('result'=>false,'message'=>'Chybi id!!!')));		
		  
    }
	/**
	 * Natahnuti informaci sablony objednavky pro JS nacuc dat
	 * do objednavky
	 * @param integer $template_id 
	 * @return 
	 */
	function load_template($template_id = null){
		$this->loadModel('CompanyOrderTemplate');
		$data = $this->CompanyOrderTemplate->read(null, $template_id);
		if ($data){
			unset(
				$data['CompanyOrderTemplate']['id'],
				$data['CompanyOrderTemplate']['company_id'],
				$data['CompanyOrderTemplate']['cms_user_id'],
				$data['CompanyOrderTemplate']['name']
			);
		
			die(json_encode(array('result'=>true,'data'=>$data['CompanyOrderTemplate'])));
		} else {
			die(json_encode(array('result'=>false,'message'=>'Nepodarilo se nacist template')));
		}
		
	}
	
	/**
	 * 
	 * @param object $order_id [optional]
	 * @return 
	 */
	function close_order_domwin($order_id=null){
		if (empty($this->data)){
			$this->set('order_id', $order_id);
			$this->render('orders/close_order_domwin');
		}
	}
    
    
    /**
	 * funkce ktera vytvari kopii objednavky a prenasi CL
	 * @param object $order_id [optional]
	 * @return 
	 */
	function order_definite($order_id=null){
    //Configure::write('debug',1);
		if (empty($this->data)){
			$this->set('order_id', $order_id);     
            
            /**
             * natahnuti CL
             */
            $this->loadModel('ConnectionClientRequirement');
            $this->ConnectionClientRequirement->bindModel(array(
                'belongsTo'=>array('Client')
            ));
            $this->set('cekaci_listina',$cl = $this->ConnectionClientRequirement->find('all',array(
                'conditions'=>array(
                    'requirements_for_recruitment_id'=>$order_id,
                    'objednavka'=>1,
                    'type'=>1
                )
            ))); 
            
            //render
			$this->render('orders/order_definite');
		}
        else{
            if($this->data['CompanyOrderItem']['start_datetime'] == '')
                 die(json_encode(array('result'=>false,'message'=>'Nebylo vyplněno datum nástupu!')));
            
            //zaskrtnuti klienti
            $ids = array_keys($this->data['id'],1);
            
            /**
             * vytvoreni kopie stavajici a zmena na zavaznou s datumem
             */
             $this->loadModel('CompanyOrderItem');
             $data = $this->CompanyOrderItem->read(null,$order_id);
             $data['CompanyOrderItem']['id'] = null;
             $data['CompanyOrderItem']['order_status'] = 1;
             // pocet pozadovanych jen na ty ktere prenasime
             $data['CompanyOrderItem']['count_of_free_position'] = count($ids); 
             //nahradniky nulujeme
             $data['CompanyOrderItem']['count_of_substitute'] = 0; 
             $data['CompanyOrderItem']['start_datetime'] = $this->data['CompanyOrderItem']['start_datetime'];
             $this->CompanyOrderItem->save($data);
            
             /**
              * preneseni lidi ktere jsou na CL zaskrtnuti
              */         
             $this->loadModel('ConnectionClientRequirement');
             foreach($ids as $connection_id){
                $this->ConnectionClientRequirement->id = $connection_id;
                $this->ConnectionClientRequirement->saveField('requirements_for_recruitment_id',$this->CompanyOrderItem->id);
             }
            
            
            die(json_encode(array('result'=>true)));
        }
	}
    
    
   /**
 	* Zobrazi seznam formularovych sablon
 	*
	* @param $company_id
 	* @return view
 	* @access public
	**/
	
	function form_template($company_id){
		$this->autoLayout = false;
       
        	
		$this->loadModel('CompanyFormTemplate'); 
        $this->CompanyFormTemplate->bindModel(array(
            'belongsTo' => array('FormTemplateGroup')
        ));
		$this->set('form_list', $this->CompanyFormTemplate->find('all',array(
            'conditions'=>array(
                'CompanyFormTemplate.kos'=>0,
                'CompanyFormTemplate.company_id'=>$company_id
            ),
            'order'=>array(
                'history ASC'
            )
        )));
		
        
        $this->set('company_id',$company_id);
		unset($this->CompanyFormTemplate);
		
        
        //render
        $this->render('form_template/index');
	}
    
    /**
 	* Pridani formularove sablony
 	*
	* @param $company_id
 	* @return view
 	* @access public
	**/
	function form_template_add($company_id){
		$this->autoLayout = false;
        if (empty($this->data)){
        	$this->data['CompanyFormTemplate']['company_id'] = $company_id;
            
    		$this->loadModel('FormTemplateGroup');
        	$this->set('group_list', $this->FormTemplateGroup->find(
        		'list',
        		array(
        			'conditions' => array(
        				'FormTemplateGroup.kos' => 0
        			),
        			'order'=>'FormTemplateGroup.created DESC'
        		)
        	));
            
            //render
            $this->render('form_template/add');
        }
        else{
           $this->loadModel('CompanyFormTemplate');
           
           /**
            * nacteni textu a nazvu globalni sablony a ulozeni k firme
            */
           $this->loadModel('FormTemplate');
           $detail_form_template = $this->FormTemplate->read(array('name','text'), $this->data['CompanyFormTemplate']['parent_id']);
           unset($this->FormTemplate);
           
           $this->data['CompanyFormTemplate']['text'] = $detail_form_template['FormTemplate']['text'];
           $this->data['CompanyFormTemplate']['name'] = $detail_form_template['FormTemplate']['name'];
           
           if($this->CompanyFormTemplate->save($this->data))
                die(json_encode(array('result'=>true)));
           else     
                die(json_encode(array('result'=>false)));
        }    
	}
    
    
    /**
 	 * Editace firemni formularove sablony
     *
	 * @param $company_id
 	 * @return view
 	 * @access public
	 **/
	function form_template_edit($company_id,$id){
	    $this->loadModel('CompanyFormTemplate');
       	$this->autoLayout = false;
		if (empty($this->data)){
          
			if ($id != null){
				$this->data = $this->CompanyFormTemplate->read(null,$id);
			}
			$this->render('form_template/edit');
		} else {
			$this->CompanyFormTemplate->save($this->data);
			die();
		}
    
    }
    
    /**
     * AJAX nacteni nove aktualni sablony
     * stara sablona bude ulozena do historie a nacte se nova
     */
    function form_template_refresh($id){
        $this->loadModel('CompanyFormTemplate');
        $detail = $this->CompanyFormTemplate->read(null,$id);

        /**
         * stara sablona se posila do historie
         */
        $this->CompanyFormTemplate->id = $id;
        $this->CompanyFormTemplate->saveField('change',0);
        $this->CompanyFormTemplate->saveField('history',1);
       
        /**
         * nacteni aktualni sablony s gloablnich sablon
         */
        $this->loadModel('FormTemplate');
        
        $new_form_template = $this->FormTemplate->read(null,$detail['CompanyFormTemplate']['parent_id']); 
        
        $to_save['name'] = $new_form_template['FormTemplate']['name'];
        $to_save['text'] = $new_form_template['FormTemplate']['text'];
        $to_save['company_id'] = $detail['CompanyFormTemplate']['company_id'];
        $to_save['parent_id'] = $detail['CompanyFormTemplate']['parent_id'];
        $to_save['form_template_group_id'] = $detail['CompanyFormTemplate']['form_template_group_id'];
        
        /**
         * ulozeni nove sablony
         */
        $this->CompanyFormTemplate->id = null;
        if($this->CompanyFormTemplate->save($to_save))
           die(json_encode(array('result'=>true)));
        else     
           die(json_encode(array('result'=>false)));
    } 
    
    
    /**
     * AJAX nacteni seznamu jednotlivych globalnich sablon podle skupiny
     */
    function ajax_load_form_template_group($group_id){
        
		$this->loadModel('FormTemplate'); 
		die(json_encode($this->FormTemplate->find('list', array(
            'conditions'=>array(
                'form_template_group_id'=>$group_id,
                'kos'=>0
            ), 
            'order'=>'name ASC'
        ))));	
        
    }
    
    
    
    
    /**
     * FUNKCE pro ATW
     */
     
     
    /**
      * funkce objednavek pro danou firmu
      */ 
    function atw_order($company_id = null){
        if($company_id == null)
            die('Bez id?');
        
        $this->set('company_id',$company_id);   
        
        
        /**
         * nacteni seznamu objednávek pro danou firmu 
         */ 
         $this->loadModel('AtwCompanyOrder');
         $this->AtwCompanyOrder->query("SET NAMES 'utf8'");
         $order_list = $this->AtwCompanyOrder->find('all',array(
            'conditions'=>array(
                'kos'=>0
            ),
			'order'=>'name',
         ));
         $this->set('order_list',$order_list);
        
        //render
        $this->render('atw_order/index');    
            
    }
    
    /**
     * editace
     */
    function atw_order_edit($company_id = null,$order_id = null, $show = false){
        if(empty($this->data)){
            $this->set('company_id',$company_id);    
            
             $this->loadModel('Province');
            $this->Province->query("SET NAMES 'utf8'");
            $this->set('province_list',$province_list = $this->Province->find('list',array('fields'=>array(),'conditions'=>array('stat_id'=>1),'order'=>'id ASC')));
        
            
            if($order_id != null){
                $this->loadModel('AtwCompanyOrder');
                 $this->AtwCompanyOrder->query("SET NAMES 'utf8'");
                $this->data = $this->AtwCompanyOrder->read(null,$order_id);
                unset($this->AtwCompanyOrder);
                
                if($this->data['AtwCompanyOrder']['order_status'] == 1)
                    $show = true;
                    $this->set('show_from',true);
            
                $this->loadModel('Country');
                $this->Country->query("SET NAMES 'utf8'");
                $this->set('country_list',$country_list = $this->Country->find('list',array('fields'=>array(),'conditions'=>array('province_id'=>$this->data['AtwCompanyOrder']['province_id']),'order'=>'id ASC')));
    
            
            
            }
            
            /**
             * pridavame objednavku a firmu vybirame v editu
             */
            if($company_id == -1){
                $this->Company->query("SET NAMES 'utf8'");
                $this->set('company_list',$this->get_list('Company',array('kos'=>0,'status'=>1)));
            }
            
            //set show
            if($show && $show != -2)
                $this->set('show',$show);
            else
                $this->set('_domwin',true);
            //render
            $this->render('atw_order/edit');    
        }
        else{
            $this->data['AtwCompanyOrder']['cms_user_id'] = $this->logged_user['CmsUser']['id'];
            
            $this->loadModel('AtwCompanyOrder');
             $this->AtwCompanyOrder->query("SET NAMES 'utf8'");
            if($this->AtwCompanyOrder->save($this->data))
               die(json_encode(array('result'=>true)));
            else     
               die(json_encode(array('result'=>false)));
        }    
    }
    
    
    /**
     * zmena stavu objednavky 
     *  uzavreni objednavky
     *      nebo
     *  prirazeni do kose
     */
	function atw_change_status($order_id = null,$col = null){
	   if($order_id == null || $col == null)
         die(json_encode(array('result'=>false)));
       
         $this->loadModel('AtwCompanyOrder');
          $this->AtwCompanyOrder->query("SET NAMES 'utf8'");
         $this->AtwCompanyOrder->id = $order_id;
         if($this->AtwCompanyOrder->saveField($col,1))
            die(json_encode(array('result'=>true)));
         else
            die(json_encode(array('result'=>false)));
           
	}
    

    
    /**
     * funkce pro odeslani zadosti na email
     * konkretni pracovni pozice
     */
    function require_work_money_auth($work_money_id){
         if($work_money_id == null)
            die(json_encode(array('result'=>false)));
       
         $this->loadModel('CompanyWorkPosition');
         $cwp = $this->CompanyWorkPosition->read(array('name','company_id'),$work_money_id);

         $company = $this->Company->read($this->Company->company_data_for_email_notification,$cwp['CompanyWorkPosition']['company_id']);

         $replace_list = array(
             '##CmsUser.name##' => $this->logged_user['CmsUser']['name'],
             '##Company.name##' => $company['Company']['name'],
             '##CompanyWorkPosition.name##' => $cwp["CompanyWorkPosition"]["name"],
         );

         $this->Email->set_company_data($company['Company']);
         
         if($this->Email->send_from_template_new(19,array(),$replace_list))
            die(json_encode(array('result'=>true)));
         else
            die(json_encode(array('result'=>false)));
    } 
    
    function position_opp($company_id = null, $position_id = null){
    	$this->loadModel('OppItem');
    	if (empty($this->data)){
    		$this->set('company_id', $company_id);
    		$this->set('position_id',$position_id);
            
            $this->loadModel('OppCategory');
    		$this->set('types',$this->OppCategory->find('list',array('conditions'=>array('status'=>1,'kos'=>0),'fields'=>array('name','name'))));
    		
    		
    		$this->loadModel('ConnectionPositionOpp');
    		$list = $this->ConnectionPositionOpp->find('all',array('conditions'=>array('position_id'=>$position_id)));
    		
    		$this->data['ConnectionPositionOpp'] = Set::combine($list,'{n}.ConnectionPositionOpp.type','{n}.ConnectionPositionOpp');
    		
    		$this->render('work_position/opp');
    	} else {
    		$this->loadModel('ConnectionPositionOpp');
    		$this->ConnectionPositionOpp->deleteAll(array('position_id'=>$this->data['position_id']));
    		foreach($this->data['ConnectionPositionOpp'] as $type=>$data){
    			$data['type'] = $type;
    			$this->ConnectionPositionOpp->save($data);
    			$this->ConnectionPositionOpp->id = null;
    		}
    		die(json_encode(array('result'=>true)));	
    	}
    }
    
    
    
    /**
     * Nove zobrazeni karty pro editaci forem odmeny pro jednotlivou profesi
     */
    function work_money_edit_new($position_id = null, $money_validity_id = null, $id = null, $show= null){
	//Configure::write('debug',1);
        if(empty($this->data)){
            if($show == -1 && !in_array($this->logged_user['CmsGroup']['id'],array(1,5))){ //chceme jen nahled a zamezit editaci
    			$this->set('only_show', true);
                
                //pro admina a directora povolit editaci jen par veci
                if(in_array($this->logged_user['CmsGroup']['id'],array(1,5))){
                    $this->set('edit_small', true);
                }
                else
                    $this->set('edit_small', false);
            }    
    		else
    			$this->set('only_show', false);
            
             /**
             * Nacteni jiz prirazenych forem mzdy
             */
            $this->loadModel('NewMoneyItemForm');
            $is_set_forms = $this->NewMoneyItemForm->find('all',array('conditions'=>array('company_work_position_id'=>$position_id,'kos'=>0)));
            $this->set('is_set_forms',$is_set_forms);
            
            $this->loadModel('CompanyWorkPosition');
            $position = $this->CompanyWorkPosition->read(array('company_id','test'),$position_id);
			$this->data['NewMoneyItem'] = array(
                'company_id'=>$position['CompanyWorkPosition']['company_id'],
                'company_work_position_id'=>$position_id,
                'company_money_validity_id'=>$money_validity_id,
            );
            
            $this->loadModel('NewMoneyItem');
            if($id != null){
                $this->NewMoneyItem->bindModel(array(
                    'hasMany'=>array('ConnectionMoneyItemSetting'=>array('conditions'=>array('kos'=>0)))
                ));
                
                $this->data = $this->NewMoneyItem->read(null,$id);
                if(isset($this->data['ConnectionMoneyItemSetting']))
                    $this->data['ConnectionMoneyItemSetting'] = Set::combine($this->data['ConnectionMoneyItemSetting'],'{n}.id','{n}','{n}.type_id');    
            }
              
            $this->data['CompanyMoneyItem']['company_work_position_id'] = $position_id;
        
            
            /**
             * Nastaveni priplatku
             */
            $this->loadModel('MoneyItemExtraPay');
            $extra_pays = $this->MoneyItemExtraPay->find('all',array('conditions'=>array('kos'=>0)));
            $extra_pay_list = array();
            foreach($extra_pays as $ep){
                $extra_pay_list[$ep['MoneyItemExtraPay']['id']] = $ep['MoneyItemExtraPay']['name'].' '.($ep['MoneyItemExtraPay']['money_item_connection_tool_id'] > 0 ? '[DB]' :'');
            }
            
            /**
             * Nastaveni srážek
             */
            $this->loadModel('MoneyItemDrawback');
            $drawbacks = $this->MoneyItemDrawback->find('all',array('conditions'=>array('kos'=>0)));
            $drawbacks_list = array();
            foreach($drawbacks as $dw){
                $drawbacks_list[$dw['MoneyItemDrawback']['id']] = $dw['MoneyItemDrawback']['name'].' '.($dw['MoneyItemDrawback']['money_item_connection_tool_id'] > 0 ? '[DB]' :'');
            }
            
            /**
             * Nastaveni bonusu
             */
            $this->loadModel('MoneyItemBonus');
            $bonuses = $this->MoneyItemBonus->find('all',array('conditions'=>array('kos'=>0)));
            $bonuses_list = array();
            foreach($bonuses as $dw){
                $bonuses_list[$dw['MoneyItemBonus']['id']] = $dw['MoneyItemBonus']['name'].' '.($dw['MoneyItemBonus']['money_item_connection_tool_id'] > 0 ? '[DB]' :'');
            }
         
           
            $this->set('extra_pay_list',$extra_pay_list);
            $this->set('drawback_list',$drawbacks_list);
    		$this->set('bonus_list',$bonuses_list);
    		$this->render('work_money_new/edit');  
					
		} else {
		/**
		 * Save 
		 */		
            $this->loadModel('NewMoneyItem');
            if($this->NewMoneyItem->save($this->data)){


                if(isset($this->data['ConnectionMoneyItemSetting'])){
                    $this->loadModel('ConnectionMoneyItemSetting');
                	$finds_co = $this->ConnectionMoneyItemSetting->find('list',array('conditions'=>array('new_money_item_id'=>$this->NewMoneyItem->id),'fields'=>array('id','id')));	
                	//vsechny pridame do kose
                    $this->ConnectionMoneyItemSetting->query('UPDATE wapis__connection_money_item_settings SET kos=1 WHERE new_money_item_id = '.$this->NewMoneyItem->id);
                    
                    foreach($this->data['ConnectionMoneyItemSetting'] as $type_id=>$type_data){
    	            	foreach($type_data as $connection_id=>$data){
    	            	        if($connection_id == 'new_0')
                                    continue;   
                          
    	            	        if(in_array($connection_id,$finds_co)){
    	            	           $this->ConnectionMoneyItemSetting->id = $connection_id;
    	            	        }
                          
                                $data['kos'] = 0; // stavjici a nove prirazujeme s kosem 0
    	            	        $data['type_id'] = $type_id;
    	            			$data['new_money_item_id'] = $this->NewMoneyItem->id;
                         
                        		if (!$this->ConnectionMoneyItemSetting->save($data)){
    		            			die(json_encode(array('result'=>false, 'message'=>'Chyba behem ukladani modelu ')));
    		            		}
    		            		$this->ConnectionMoneyItemSetting->id = null; 
    		            }
                	}
                    unset($this->ConnectionMoneyItemSetting);
                }
                
                die(json_encode(array('result'=>true)));
            }
            else
                die(json_encode(array('result'=>false)));
		}
	}
    
    
    
    /**
     * NActeni informaci o zvolenych srazkach nebo priplatcich z preddefinovaneho seznamu do kalkulace
     */
    function load_detail_for_money_item($typ,$id = null){     
        $typ_table = array(0=>'MoneyItemBonus',1=>'MoneyItemExtraPay',2=>'MoneyItemDrawback');
        
        if($id != null){
            $this->loadModel($typ_table[$typ]);
            $miep = $this->{$typ_table[$typ]}->find('first',array('conditions'=>array(
                'id'=>$id,
                'kos'=>0
            )));
            
            if(!$miep)
                die(json_encode(array('result'=>false,'message'=>'Nenalezeno ID!')));
            
            /**
             * Uprava vystupu pro JS abych vratil, jiz primo IDcka ktera se objevuji ve formulari kalkulace
             */    
            $output = array();
            foreach($miep[$typ_table[$typ]] as $key=>$value){
                $output['ConnectionMoneyItemSetting'.$typ.'New0'.Inflector::camelize($key)] = $value;
            }     
            
            die(json_encode(array('result'=>true,'data'=>$output)));
        }
        else
            die(json_encode(array('result'=>false)));
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * Pridani noveho nastaveni k pracovni profesi
     * zarazeni dle typu
     * 0 - bonusy
     * 1 - priplatek
     * 2 - srazka
     */
    function make_money_item_setting(){                
        if(isset($_POST))
            $this->data = $_POST;
   
        if(!empty($this->data)){
            $this->loadModel('ConnectionMoneyItemSetting');
            $is_set_settings = $this->ConnectionMoneyItemSetting->find('first',array('conditions'=>array(
                'company_work_position_id'=>$this->data['company_work_position_id'],
                'typ'=>$this->data['typ'],
                'money_item_type_id'=>$this->data['money_item_type_id'],
                'kos'=>0
            )));
            
            if($is_set_settings)
                die(json_encode(array('result'=>false,'message'=>'Již existuje!')));
            
            $this->ConnectionMoneyItemSetting->save($this->data); 
            die(json_encode(array('result'=>true,'id'=>$this->ConnectionMoneyItemSetting->id)));
        }
        else
            die(json_encode(array('result'=>false)));
    }
    
    /**
     * Smazeni nastaveni k dane profesi
     */
    function delete_money_item_setting($connection_id = null){
        if($connection_id == null)
            die(json_encode(array('result'=>false)));
            
        $this->loadModel('ConnectionMoneyItemSetting');
        $this->ConnectionMoneyItemSetting->id = $connection_id;
        if($this->ConnectionMoneyItemSetting->saveField('kos',1)){
            die(json_encode(array('result'=>true)));
        }
        else
             die(json_encode(array('result'=>false,'message'=>'Chyba při ukládání')));
    }
    
    /**
     * Pridani k dane profesi typ formy odmeny XXXX
     * PP,DO,ZL,Abc as  nataveni stravenek a dopravy
     */
    function add_new_money_item_form($position_id = null){ 
        if(isset($_POST))
            $this->data = $_POST;
   
        if(!empty($this->data)){
            $this->loadModel('NewMoneyItemForm');
            $is_set_settings = $this->NewMoneyItemForm->find('first',array('conditions'=>array(
                'company_work_position_id'=>$this->data['company_work_position_id'],
                'stravenky'=>$this->data['stravenky'],
                'doprava'=>$this->data['doprava'],
                'pp'=>$this->data['pp'],
                'do'=>$this->data['do'],
                'zl'=>$this->data['zl'],
                'cash'=>$this->data['cash'],
                'kos'=>0
            )));
            
            if($is_set_settings)
                die(json_encode(array('result'=>false,'message'=>'Již existuje!')));
                      
            $this->data['name'] = $name = $this->NewMoneyItemForm->generate_name($this->data);
            $this->NewMoneyItemForm->save($this->data); 
            die(json_encode(array('result'=>true,'name'=>$name,'id'=>$this->NewMoneyItemForm->id)));
        }
        else
            die(json_encode(array('result'=>false)));
    }
    
    function delete_money_item_form($id = null){
         if($id == null)
            die(json_encode(array('result'=>false)));
            
        $this->loadModel('NewMoneyItemForm');
        $this->NewMoneyItemForm->id = $id;
        if($this->NewMoneyItemForm->saveField('kos',1)){
            die(json_encode(array('result'=>true)));
        }
        else
             die(json_encode(array('result'=>false,'message'=>'Chyba při ukládání')));
    }
    
    /**
     * Kontrola jiz expirovanych firem a jejich smluv
     * zaslani atep robotem email
     */
    function check_expirate(){
        $date = new DateTime();
        $date->modify('+1 month');

        $date2 = new DateTime();
        $date2->modify('+2 month');
        
        $smlouvy = $this->Company->query('SELECT * FROM `wapis__companies` where kos = 0 and (expirace_smlouvy = "'.$date->format('Y-m-d').'" OR expirace_smlouvy = "'.$date2->format('Y-m-d').'")');
   
        foreach($smlouvy as $smlouva){
            $smlouva = $smlouva['wapis__companies'];
            $replace_list = array(
                '##Company.name##' => $smlouva['name'],
                '##expirace##' =>date('d.m.Y',strtotime($smlouva['expirace_smlouvy']))
            );
            $this->Email->set_company_data($smlouva);
            $this->Email->send_from_template_new(27,array(),$replace_list);
        }
    }
    
    /**
     * Zjistuji jaky stat je dana firma
     */
    function check_stat_id($company_id = null){
        if($company_id != null){
            $comp = $this->Company->read('stat_id',$company_id);
            die(json_encode(array('result'=>true,'stat_id'=>$comp['Company']['stat_id'])));
        }
        else
            die(json_encode(array('result'=>false,'message'=>'Chyba, ID firma je prazdne')));
    }
    
    
    
    /**
     * Funkce pro správu šablon docházek (hodinovek, pro konkretni firmu)
     */
    function client_working_template($company_id = null){
		$this->autoLayout = false;
		$this->loadModel('CompanyClientWorkingTemplate'); 
		$this->set('template_list', $this->CompanyClientWorkingTemplate->find('all',array(
            'conditions'=>array(
                'CompanyClientWorkingTemplate.kos'=>0,
                'CompanyClientWorkingTemplate.company_id'=>$company_id
            )
        )));
		
        
        $this->set('company_id',$company_id);
		unset($this->CompanyFormTemplate);
		
        
        //render
        $this->render('client_working_template/index');
	
    }
    
    /**
     * editace / přídání šablon
     */
    function cw_template_edit($company_id = null, $id = null){
	    $this->loadModel('CompanyClientWorkingTemplate');
		if (empty($this->data)){
		    $this->data['CompanyClientWorkingTemplate']['company_id'] = $company_id;
			if ($id != null){
				$this->data = $this->CompanyClientWorkingTemplate->read(null,$id);
			}
			$this->render('client_working_template/edit');
		} else {
			$this->CompanyClientWorkingTemplate->save($this->data);
			die();
		}
    
    }   
    
    /**
     * Položky šablon
     */
    function cw_template_items($company_id = null,$template_id){
		$this->autoLayout = false;
		$this->loadModel('CompanyClientWorkingTemplateItem'); 
		$this->set('template_items_list', $this->CompanyClientWorkingTemplateItem->find('all',array(
            'conditions'=>array(
                'CompanyClientWorkingTemplateItem.kos'=>0,
                'CompanyClientWorkingTemplateItem.company_id'=>$company_id,
                'CompanyClientWorkingTemplateItem.company_client_working_template_id'=>$template_id
            ),'order'=>'rok desc, mesic asc'
        )));
        $this->set('company_id',$company_id);
        $this->set('template_id',$template_id);
		unset($this->CompanyFormTemplate);
		
        //render
        $this->render('client_working_template/items/index');
    } 
    
    /**
     * Přídání editace šablon
     */
    function cw_template_item_edit($company_id = null, $template_id = null, $template_item_id = null){
        
	    $this->loadModel('CompanyClientWorkingTemplateItem');
		if (empty($this->data)){
		    $month = date('m');
            $year = date('Y');
            $shift = 1;
		    $this->data['CompanyClientWorkingTemplateItem']['company_id'] = $company_id;
            $this->data['CompanyClientWorkingTemplateItem']['company_client_working_template_id'] = $template_id;
			if ($template_item_id != null){
				$this->data = $this->CompanyClientWorkingTemplateItem->read(null,$template_item_id);
                $month = $this->data['CompanyClientWorkingTemplateItem']['mesic'];
                $year = $this->data['CompanyClientWorkingTemplateItem']['rok'];
                $shift = $this->data['CompanyClientWorkingTemplateItem']['setting_shift_working_id'];
            }
            $this->set('month',$month);
            $this->set('year',$year);
            $this->set('setting_shift_working_id',$shift);
            
            $comp = $this->Company->read('stat_id',$company_id);
            $this->set('stat_id',$comp['Company']['stat_id']);
            // load free days for state, month, year
			$this->loadModel('SettingStatSvatek');
			$this->set('svatky',$this->SettingStatSvatek->find('list', array('conditions'=>array( 'setting_stat_id'	=> $comp['Company']['stat_id'], 'mesic'=> $month, 'rok'=> $year ),'fields'=>array('id','den'),'order'=>'den ASC'))); 				
			unset($this->SettingStatSvatek);
        
		    $this->render('client_working_template/items/edit');
		} else {
		  
		   $exist = $this->CompanyClientWorkingTemplateItem->find('first',array(
            'conditions'=>array(
                'company_id'=>$this->data['CompanyClientWorkingTemplateItem']['company_id'],
                'company_client_working_template_id'=>$this->data['CompanyClientWorkingTemplateItem']['company_client_working_template_id'],
                'rok'=>$this->data['CompanyClientWorkingTemplateItem']['rok'],
                'mesic'=>$this->data['CompanyClientWorkingTemplateItem']['mesic'],
              
            )
           ));
           
           if($exist && $this->data['CompanyClientWorkingTemplateItem']['id'] == '')
             die(json_encode(array('result'=>false,'message'=>'Šablona pro tento rok a měsíc již existuje.')));
           else{
			 $this->CompanyClientWorkingTemplateItem->save($this->data);
			
             die(json_encode(array('result'=>true)));
		   }
        }
    
    }   
    
    /**
     * generovani tabulky hodinovek, podle nastaveneho datumu
     */
    function cw_template_item_render_element($year = null, $month = null,$smennost = null,$stat_id = null){
	    $this->set('year',$year);
        $this->set('month',$month);
        $this->set('setting_shift_working_id',$smennost);
        
        // load free days for state, month, year
		$this->loadModel('SettingStatSvatek');
		$this->set('svatky',$this->SettingStatSvatek->find('list', array('conditions'=>array( 'setting_stat_id'	=> $stat_id, 'mesic'=> $month, 'rok'=> $year ),'fields'=>array('id','den'),'order'=>'den ASC'))); 				
		unset($this->SettingStatSvatek);
        
        $this->render('client_working_template/items/element');
    } 
    
    
    /**
	* Slouzi proautorizaci testovaci kalkulace
	*
	* @param $id
 	* @return view 
 	* @access public
 	*/
	function new_work_money_auth($id = null){
	//Configure::write('debug',2);
		if($id != null)
		{
				if (empty($this->data)) {
					$this->loadModel('CompanyWorkPosition');

					$detail= $this->CompanyWorkPosition->read(array('company_id','name'),$id);
					$this->CompanyWorkPosition->bindModel(array(
						'hasMany'=>array(
							'CompanyWorkPosition2'=>array(
								'order'=>'name ASC',
								'foreignKey' => 'parent_id',
								'conditions'=>array(
									'company_id  '=>$detail['CompanyWorkPosition']['company_id'],
									'kos'=>0,
									'test'=>0,
                                    'new'=>1
								)
							)
						)
					));
				
					$position_list_b = $this->CompanyWorkPosition->find('all',array(
						'conditions'=>array(
							'parent_id' => -1,
							'company_id  '=>$detail['CompanyWorkPosition']['company_id'],
							'kos'=>0,
							'test'=>0,
                            'new'=>1
						)
					));

					$output = array(''=>'Vytvořit novou');
					foreach($position_list_b as $item){
						$output[$item['CompanyWorkPosition']['id']] = $item['CompanyWorkPosition']['name'].' ( přidat novou platnost )';
						$output['p_'.$item['CompanyWorkPosition']['id']] = '-> Vytvořit novou sub';
						if (isset($item['CompanyWorkPosition2']) && count($item['CompanyWorkPosition2']) > 0){
							foreach($item['CompanyWorkPosition2'] as $sub_item){
								$output[$sub_item['id']] = '-> '.$sub_item['name'] .'  ( přidat novou platnost )';
							}
						}
					}
						
				
					$this->set('pozice_name',$detail['CompanyWorkPosition']['name']);
					$this->set('id',$id);
					$this->data['CompanyWorkPosition']['company_id'] = $detail['CompanyWorkPosition']['company_id'];
					$this->set('company_work_position_list',$output);//$position_list+$position_list_b);
					                
                    //render
					$this->render('work_position/new_auth');	
				}
				else {
				//save
                
						$typ = $this->data['CompanyMoneyValidity']['profese'];
						//vyhledani spolecne fakturacky
						$this->loadModel('NewMoneyItem');
						$one_item = $this->NewMoneyItem->find('first',array(
							'fields' => 'fakturacka',
							'conditions'=>array(
								'company_work_position_id '=>$id,
								'kos  '=>0
							)
						));

						// vytvoreni uplne nove profese
						if($typ == "" || substr($typ, 0, 2) == 'p_'){ 
							//pokud pridavam novou sub pozici nastav parent_id
							$parent_id =  (substr($typ, 0, 2) == 'p_') ?  substr($typ, 2) : -1;

							//tvorba nove validity
							$this->loadModel('CompanyMoneyValidity');
							$to_save['CompanyMoneyValidity']['company_id'] = $this->data['CompanyWorkPosition']['company_id'];
							$to_save['CompanyMoneyValidity']['company_work_position_id'] = $id;
							$to_save['CompanyMoneyValidity']['fakturacka'] = $one_item['NewMoneyItem']['fakturacka'];
							$to_save['CompanyMoneyValidity']['platnost_od'] = $this->data['CompanyMoneyValidity']['platnost_od'];

							//pr($to_save);
							$this->CompanyMoneyValidity->save($to_save);
							
							//u itemu zmeni validity id
							$this->NewMoneyItem->updateAll(
								array('company_money_validity_id' => $this->CompanyMoneyValidity->id),
								array('company_work_position_id' => $id,'kos'=>0)
							);
				
							//u position zmenit test na 0 a parent_id
							$this->loadModel('CompanyWorkPosition');
							$this->CompanyWorkPosition->id = $id;
							$this->CompanyWorkPosition->saveField('test',0);
							$this->CompanyWorkPosition->saveField('parent_id',$parent_id);
                            
                            /**
                             * odeslani emailu o autorizaci
                             */
                             self::send_mail_about_auth_money_items($this->data['CompanyWorkPosition']['company_id'],$id);
                       
			
							die(json_encode(true));						
						}
						// verze kdy zadavam novou platnost k existujici pozici
						else if(substr($typ, 0, 2) != 'p_'){
							$pozice_id = $typ; // predani id 
							$this->loadModel('CompanyMoneyValidity');
							//hledani aktualni platnosti
							$detail = $this->CompanyMoneyValidity->find('first',array(
								'fields' => array('id','platnost_od'),
								'conditions'=>array(
									'company_work_position_id '=>$pozice_id,
									'platnost_do  '=>'0000-00-00',
									'platnost_od  <>'=>'0000-00-00',
									'kos  '=>0
								)
							));

							//pokud je zadana platnost mensi nez stavajici tak vyhod chybu
							if($this->data['CompanyMoneyValidity']['platnost_od'] <= $detail['CompanyMoneyValidity']['platnost_od'])
								die(json_encode('Platnost je příliš malá, stará platnost je '.$detail['CompanyMoneyValidity']['platnost_od']));
							//pokud je zadana platnost mensi nez nynejsi mesic
							else if(($this->data['CompanyMoneyValidity']['platnost_od'] < (date('Y-m').'-01')) && $this->logged_user['CmsGroup']['id'] != 1)
								die(json_encode('Platnost je příliš malá, zadávate menší platnost než je nynější měsíc!!!'));
							// jinak uloz
							else{
								//nastaveni platnosti na nova platnost_od  - 1 den = minus jeden mesic a posledni den v nem  
								$currentDate = new DateTime($this->data['CompanyMoneyValidity']['platnost_od']);
								$currentDate->modify('-1 day');
								$currentDate->format('Y-m-d');
								//ukonceni platnosti stare validity
								$this->CompanyMoneyValidity->id = $detail['CompanyMoneyValidity']['id'];
								$this->CompanyMoneyValidity->saveField('platnost_do',$currentDate->format('Y-m-d'));

								//ulozeni nove validity
								$this->CompanyMoneyValidity->id = null;
								$to_save_a['CompanyMoneyValidity']['company_id'] = $this->data['CompanyWorkPosition']['company_id'];
								$to_save_a['CompanyMoneyValidity']['company_work_position_id'] = $pozice_id;
								$to_save_a['CompanyMoneyValidity']['fakturacka'] = $one_item['NewMoneyItem']['fakturacka'];
								$to_save_a['CompanyMoneyValidity']['platnost_od'] = $this->data['CompanyMoneyValidity']['platnost_od'];
								$this->CompanyMoneyValidity->save($to_save_a);

								//u stavajici profese priradit kos
								$this->loadModel('CompanyWorkPosition');
								$this->CompanyWorkPosition->id = $id;
								$this->CompanyWorkPosition->saveField('kos',1);

								//u itemu zmen validity id a profese_id 
								$this->NewMoneyItem->updateAll(
									array('company_money_validity_id' => $this->CompanyMoneyValidity->id,'company_work_position_id' => $pozice_id),
									array('company_work_position_id' => $id,'kos'=>0)
								);
                                
                                /**
                                 * odeslani emailu o autorizaci
                                 */
                                 self::send_mail_about_auth_money_items($this->data['CompanyWorkPosition']['company_id'],$id);
                           
								 die(json_encode(true));
							}
						}
					           
				}
		}
	}
    
    function company_template_status($id,$status){
	    $this->loadModel('CompanyFormTemplate');
        $this->CompanyFormTemplate->id = $id;
		$this->CompanyFormTemplate->saveField('status',($status==0)?1:0);
		die();
	}
    
    function add_provinces(){
        die('nepouzivat');
        $provinces = 'Berlín, Hlavní město NDR, Drážďany, Karl-Marx-Stadt, Lipsko, Gera, Erfurt, Suhl, Halle, Magdeburg, Chotěbuz, Postupim, Frankfurt nad Odrou, Neubrandenburg, Schwerin, Rostock';
        $this->loadModel('Province');
        $_provinces = explode(',',$provinces);
        foreach($_provinces as $pr){
            $this->Province->id = null;    
            $this->Province->save(array('name'=>trim($pr),'stat_id'=>3));
        }
    }
    
    /*
    function recreate_money_items(){
        $this->loadModel('CompanyMoneyItem');
        $data = $this->CompanyMoneyItem->read(null,3088);
        unset($data['CompanyMoneyItem']['id']);
        $data['CompanyMoneyItem']['company_id'] = 2388;
        $data['CompanyMoneyItem']['company_work_position_id'] = 1233;
        $data['CompanyMoneyItem']['company_money_validity_id'] = 657;
        $this->CompanyMoneyItem->create();
        $this->CompanyMoneyItem->save($data);
    }*/
    
    function coordinators_edit($company_id = null, $id = null){
        $this->loadModel('CompanyCoordinator');
        if(empty($this->data)){
            if($id != null){
                $this->data = $this->CompanyCoordinator->read(null,$id);
            }
            else{
                $this->data['CompanyCoordinator']['company_id'] = $company_id;
            }
            
            $this->set('coo_list',$this->get_list_of_superrior(2));
            $this->render('coordinators/edit');
        }        
        else{       
            if($this->data['CompanyCoordinator']['id'] == '')
                $this->data['CompanyCoordinator']['add_cms_user_id'] = $this->logged_user['CmsUser']['id'];
            
            $this->loadModel('CompanyCoordinator');
            if($this->CompanyCoordinator->save($this->data)){
                die(json_encode(array('result'=>true)));
            }else
                die(json_encode(array('result'=>false)));
        }
    }
    
    function coordinators_trash($id){
        $this->loadModel('CompanyCoordinator');
        $this->CompanyCoordinator->id = $id;
        $this->CompanyCoordinator->saveField('kos',1);
    }
    
    /**
     * Funkce slouzi pro prevod/predani mezi uzivateli u jednotlivych firem
     * Napr. hromadna zmena MR,SM,KOO atd.
     */
    function convert_users(){
        $allow_convert_fields = array(
            'manazer_realizace_id'=>'Manažer realizace',
            'asistent_obchodu_id'=>'Asistent obchodu',
            'self_manager_id'=>'Sales Manager',
            'client_manager_id'=>'Koordinátor',
            'coordinator_id'=>'Koordinátor 2',
            'coordinator_id2'=>'Koordinátor 3',
        );
        if(empty($this->data)){
            $this->set('fields',$allow_convert_fields);
        }
        else{//save
            $this->Company->updateAll(
                array($this->data['field']=>$this->data['user_to']),
                array($this->data['field']=>$this->data['user_from'])
            );
            die(json_encode(array('result'=>true)));
        }
        
    }
    
    function ajax_load_users($fields){
        $return = array();
        $this->loadModel('CmsUser');
        switch($fields){
            case 'manazer_realizace_id':
			   $return = $this->CmsUser->find('list', array('conditions'=>array('cms_group_id'=>18,'kos'=>0)));
            break;
            case 'asistent_obchodu_id':
               $return = $this->CmsUser->find('list',array('conditions'=>array('CmsUser.status'=>1,'CmsUser.kos'=>0,'CmsUser.cms_group_id'=>26)));
            break;
            case 'self_manager_id':
                $return =  $this->get_sm();
            break;
            case 'client_manager_id':
            case 'coordinator_id':
            case 'coordinator_id2':
                $return = $this->get_list_of_superrior(2);
            break;
        }
        
        die(json_encode($return));
    }
    
    
     /**
     * funkce pro vytvoreni exportu do excelu - CSV
     * podle filtrace vyber dane klienty a vygeneruj je do csv
     */
    function export_excel(){
        //Configure::write('debug',1);
        
       $start = microtime ();
       $start = explode ( " " , $start );
       $start = $start [ 1 ]+ $start [ 0 ];  
        
         $fields_sql = array(
            'CompanyView.*,
			( 
			  SELECT 
			   COUNT(client_id)
			  FROM wapis__connection_client_requirements
			  where type=2 and company_id=CompanyView.id and(
				`from` <= "'. date('Y-m-d').'" AND (`to`>="'. date('Y-m-d').'" OR `to`="0000-00-00")
			  ) 
			) as poc_zamestnancu'
         );
               
        $criteria = $this->ViewIndex->filtration();        
      
      
         header("Pragma: public"); // požadováno
         header("Expires: 0");
         header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
         header("Cache-Control: private",false); // požadováno u některých prohlížečů
         header("Content-Transfer-Encoding: binary");
         Header('Content-Type: application/octet-stream');
         Header('Content-Disposition: attachment; filename="'.date('Ymd_His').'.csv"');
     
      
         /*
         * Celkovy pocet zaznamu
         */
         
        $limit = 100;   
        $page = 1;     
        $count = $this->CompanyView->find('first',
        	array(
        		'fields' =>  array("COUNT(DISTINCT CompanyView.id) as count"),
        		'conditions'=>$criteria,
        		'recursive'	=>1
        	)
        );    
        $count = $count[0]['count'];
        // hlavicka
        foreach($this->renderSetting['items'] as &$item_setting){
            list($caption, $model, $col, $type, $fnc) = explode('|',$item_setting);
        	$item_setting = compact(array("caption", "model","col","type","fnc"));	
            if($type != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',$caption).'";'; 
        }
        echo "\n";   
        unset($item_setting, $caption, $model, $col, $type, $fnc);
        
        $str_array=array("<br/>"=>', ',':'=>',',';'=>',','?'=>'', '#'=>' ');
        /*
         * Cyklicky vypis dat po $limit zaznamu
         */
        
        $this->loadModel('CompanyContact');
        for ($exported = 0; $exported < $count; $exported += $limit){
            foreach($this->CompanyView->find('all',array(
                   'fields'=>$fields_sql,
                    'conditions'=>$criteria,
                    'limit'=>$limit,
                    'page'=>$page,
                    'recursive'=>1,
                    'order'=>'CompanyView.name ASC'
            )) as $item){
                foreach($this->renderSetting['items'] as $key => $td){
                    if($td['type'] != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',strtr($this->ViewIndex->generate_td($item,$td,$this->viewVars),$str_array)).'";';     
                }

                /**
                 * kontakty k danym firmam
                 */
                $contacts = $this->CompanyContact->find('all',array(
                'conditions'=>array(
                    'company_id'=>$item['CompanyView']['id'],
                    'kos'=>0
                )));
                foreach($contacts as $contact){
                    echo '"'.iconv('UTF-8','Windows-1250',strtr($contact['CompanyContact']['name'].', '.$contact['CompanyContact']['email'].', '.$contact['CompanyContact']['telefon1'],$str_array)).'";';
                }
                
                echo "\n";  
            }
            $page++;
        }
       
        //time
         $end = microtime ();
         $end = explode ( " " , $end );
         $end = $end [ 1 ]+ $end [ 0 ]; 
         echo 'Generate in '.($end - $start).'s';
         echo "\n"; 
        
        die();
    }
}
?>
