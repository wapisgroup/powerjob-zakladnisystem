<?php
Configure::write('debug',1);

$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');
@define('CURRENT_YEAR', $_GET['current_year']); 
@define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);

class InternalEmployeesController  extends AppController {
	var $name = 'InternalEmployees';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email','Employees');
	var $uses = array('ConnectionClientAtCompanyWorkPosition');
	var $mesic = CURRENT_MONTH;
	var $rok = CURRENT_YEAR;
	var $renderSetting = array(
		'bindModel'	=> array(
			'belongsTo'=>array('AtCompany','Client','AtProjectCentreWorkPosition','AtProjectCentre','AtCompanyMoneyItem'),
            'hasOne'	=>	array(
                'ClientWorkingHoursInt'=>array(
					'conditions' => array(
						'ClientWorkingHoursInt.year'=> CURRENT_YEAR,
						'ClientWorkingHoursInt.month'=> CURRENT_MONTH
					)
				)	
			)
		),
		'controller'	=>	'internal_employees',
		'SQLfields' 	=>	'*',
		'SQLcondition'	=>  array(
			"((DATE_FORMAT(ConnectionClientAtCompanyWorkPosition.datum_nastupu,'%Y-%m')<= '#CURRENT_YEAR#-#CURRENT_MONTH#'))",
			'((DATE_FORMAT(ConnectionClientAtCompanyWorkPosition.datum_to,"%Y-%m") >= "#CURRENT_YEAR#-#CURRENT_MONTH#") OR (ConnectionClientAtCompanyWorkPosition.datum_to = "0000-00-00 00:00"))',
            'ConnectionClientAtCompanyWorkPosition.kos'=> 0
		),
		'page_caption'=>'Docházky interních zaměstnanců',
		'sortBy'=>'AtCompany.name.ASC',
		'top_action' => array(
		//	'add_item'		=>	'Přidat|edit|Přidat novou položku|add',
		),
		'filtration' => array(
            'Client-name'		=>	'text|Klient|',
			'ConnectionClientAtCompanyWorkPosition-at_at_company_id'	=>	'select|Společnost|at_company_list',
            'GET-current_month'							=>	'select|Měsíc|mesice_list',
            'GET-current_year'							=>	'select|Rok|actual_years_list',
        ),
		'items' => array(
			'id'				=>	'ID|ConnectionClientAtCompanyWorkPosition|id|hidden|',
			'company'			=>	'Společnost|AtCompany|name|text|',
            'centre'	        =>	'Sředisko|AtProjectCentre|name|text|',
			'profese'	        =>	'Profese|AtProjectCentreWorkPosition|name|text|',
            'client_id'			=>	'ID|Client|id|text|',
     		'client'			=>	'Zaměstnanec|Client|name|text|',
			'stav'				=>	'Stav|ClientWorkingHoursInt|stav|var|kalkulace_stav_list',
			'nastup'			=>	'Datum nástupu|ConnectionClientAtCompanyWorkPosition|datum_nastupu|date|',
			'ukonceni'			=>	'Datum ukončení|ConnectionClientAtCompanyWorkPosition|datum_to|date|',
			'forma'				=>	'Forma|AtCompanyMoneyItem|name|text|',
			'celkem'			=>	'Celkem|ClientWorkingHoursInt|celkem_hodin|text|',
			//'change_max_salary'	=>	'#|ClientWorkingHoursInt|change_max_salary|text|status_to_ico2#zmena_maximalky-Změna maximálky-1',		
        ),
		'posibility' => array(
			'edit'			=>	'odpracovane_hodiny|Editovat položku|edit',
			'uzavrit_odpracovane_hodiny'	=>	'uzavrit_odpracovane_hodiny|Uzavřít docházku|uzavrit_odpracovane_hodiny',
			'autorizace_dochazky'	=>	'autorizace_dochazky|Autorizovat docházku|autorizace_dochazky',
			'client_info'	=>	'client_info|Karta klienta|client_info',
		    //'modify_from_date'	=>	'modify_from_date|Editace zamestanani klienta|modify_from_date',	
		),
        'posibility_link' => array(
            'uzavrit_odpracovane_hodiny'=>'ClientWorkingHoursInt.id'                
        ),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'	=> true,
			'languages'		=> true,
		)
	);

	

	function beforeRender(){
		parent::beforeRender();

		// úprava nadpisu reportu a přidání do něj datumu
        if(isset($this->viewVars['renderSetting']))
		  $this->set('spec_h1', $this->viewVars['renderSetting']['page_caption'].' za měsíc '.$this->mesice_list[ltrim($this->mesic,'0')].' a rok '.$this->rok);
	}
	
	function index(){
		// set FastLinks
		$this->set('fastlinks',array('ATEP'=>'/','Klienti'=>'#','Docházky zaměstnanců'=>'#'));
		$this->set('at_company_list',$this->get_list('AtCompany'));
				
		if ($this->RequestHandler->isAjax()){
			//$this->set('change_list_js', json_encode(array('filtr_ConnectionClientAtCompanyWorkPosition-at_company_id'=>$company_list)));
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
		
	}

	
	private function read_connection($connection_client_at_company_work_position_id = null, $year = null, $month = null){
		// loadmodel && bindModel
			$this->ConnectionClientAtCompanyWorkPosition->bindModel(array(
				'belongsTo'=>array('AtCompany','Client','AtProjectCentreWorkPosition','AtProjectCentre','AtCompanyMoneyItem'),
				'hasOne'	=>	array('ClientWorkingHoursInt' => array('conditions' =>array('ClientWorkingHoursInt.year'=>$year,'ClientWorkingHoursInt.month'=>$month)))
			));

			// read from db
			return  $this->ConnectionClientAtCompanyWorkPosition->read(null,$connection_client_at_company_work_position_id);
	}
	
    //funkce na prepocet prescasu
	private function prepocet_prescasy($setting){
		$pd = 0;
		$this->loadModel('SettingStatPd');
		$pd_list = $this->SettingStatPd->find('list',array(
				'conditions'=>array(
					'kos'=>0,
					'mesic'=>CURRENT_MONTH,
					'rok'=>CURRENT_YEAR
				),
				'fields'=>array('setting_stat_id','pocet')
		));
		$nh = $setting['normo_hodina'];
	
				
		//pokud exituji nastaveny pracovni fond pro tento mesic a rok
		if(array_key_exists($setting['stat_id'],$pd_list))
			$pd = $pd_list[$setting['stat_id']];
		if($pd <> 0 && $nh <> 0){
			return  ($nh * $pd);
		}
		else 
			return 0;
	}
    
    
    /**
     * Funkce na kontrolu prenesenenych hodinovek z minuleho mesice,
     * pokud byly hodinovky vetsi nez stavajici maximalky formy odmeny vrat je na maximalni hdonotu
     * @since 12.11.09
     * @author Sol
     */
    private function check_salary_hour_of_value($data){
      
         $testing_array = array(
            'salary_part_1',
            'salary_part_2',
            'salary_part_3',
            'salary_part_4',
            'odmena_1',
            'odmena_2'
         );
         $data['ClientWorkingHoursInt']['salary_per_hour'] = 0;
         foreach($testing_array as $item){
            if($data['ClientWorkingHoursInt'][$item] > $data['ClientWorkingHoursInt'][$item.'_max'])
                $data['ClientWorkingHoursInt'][$item] = $data['ClientWorkingHoursInt'][$item.'_max'];
            if(substr($item,0,6) == 'salary')
                $data['ClientWorkingHoursInt']['salary_per_hour'] +=  $data['ClientWorkingHoursInt'][$item];      
         }

         return $data;
    }
    
    
	
	/**
 	* Returns a view of "odpracovane hodiny"
 	*
	* @param $connection_client_at_company_work_position_id (INT)
	* @param $year (DATE(Y))
	* @param $month DATE(m))
	* @param $inner (STRING)
 	* @return view
 	* @access public
	**/
	public function odpracovane_hodiny($connection_client_at_company_work_position_id = null, $year = null, $month = null, $inner = null){
         if (empty($this->data)){
               if ($connection_client_at_company_work_position_id == null || $year == null || $month== null){
				  die('Chyba, neznami cinitele');
			   }
            
           /**
            * nastaveni predchoziho mesice se spravnym rokem 
            */    
            $prev_month_date = new DateTime();
            $prev_month_date->setDate($year, $month, 01);
            $prev_month_date->modify("-1 month");
		

			$this->data = $this->read_connection($connection_client_at_company_work_position_id, $year, $month);
    
     		$this->loadModel('AtCompany');
			$company_stat_id = $this->AtCompany->read(null,$this->data['ConnectionClientAtCompanyWorkPosition']['at_company_id']);
            $this->set('company_detail',$company_stat_id);

            $transfer_from_last_month = false;
            
			if (empty($this->data['ClientWorkingHoursInt']['id'])){
				//vyresit transfer z predesleho mesice ?
			}
	

			$this->data['ClientWorkingHoursInt']['connection_client_at_company_work_position_id'] = $connection_client_at_company_work_position_id;
			$this->data['ClientWorkingHoursInt']['at_company_id'] = $this->data['ConnectionClientAtCompanyWorkPosition']['at_company_id'] ;
			$this->data['ClientWorkingHoursInt']['client_id'] = $this->data['ConnectionClientAtCompanyWorkPosition']['client_id'] ;
			

			$this->set('ubytovani_select',false);
            $this->set('odmeny_typ',1);  //normalni zobrazeni - jinak je zobrazeni pro pausal, meni se view
                

			//informace o skupine prihlaseneho pro tl. ulozeni
			$this->set('logged_group', $this->logged_user['CmsGroup']['id']);
			$this->set('logged_user', $this->logged_user['CmsUser']['id']);
			

            /**
             * nacteni zaloh k danemu connection
             */
			$this->loadModel('ReportDownPayment');
            $this->ReportDownPayment->bindModel(array(
                'belongsTo'=>array('CmsUser')
            ));
			$rdp = $this->ReportDownPayment->find('all',array(
				'conditions'=>array(
					'ReportDownPayment.kos'=>0,
                    'ReportDownPayment.dochazka_type'=>2,
                    'OR'=>array(
                        array(
        					'(DATE_FORMAT(ReportDownPayment.created,"%Y-%m"))' => $year.'-'.$month,
        					'ReportDownPayment.month' =>'0',
        					'ReportDownPayment.year' =>'0'
                        ),
                        array(
                            'ReportDownPayment.month' =>$month,
        					'ReportDownPayment.year' =>$year
                        )    
					),
                    'ReportDownPayment.client_id'=>$this->data['ClientWorkingHoursInt']['client_id'],
					'ReportDownPayment.connection_client_requirement_id'=>$connection_client_at_company_work_position_id
				)
			));
			$this->set('payment_list',$rdp);
			$this->set('payment_sum',0);
			unset($this->ReportDownPayment);
            

						
			// default variable na index
			$this->set('stat_id', $company_stat_id['AtCompany']['stat_id']); 
			$this->set('connection_id', $connection_client_at_company_work_position_id); 
			$this->set('year', $year); 
			$this->set('month', $month); 
			
			// kill rendering, if client dont work in this year and month, or you select future;
			list($start_y, $start_m, $start_d) 	= explode('-', $this->data['ConnectionClientAtCompanyWorkPosition']['datum_nastupu']);
			list($end_y, $end_m, $end_d) 		= explode('-', $this->data['ConnectionClientAtCompanyWorkPosition']['datum_to']);
			
			// for Test Only = $end_y = '2009'; 	$end_m = '06';
			// condition for kill rendering
			$render_element = 0;

			if (( $year <= date('Y') && $month <= date('m') ) || ($month > date('m') && $year < date('Y'))){

				if (($end_y == '0000' && $start_y <= $year && $start_m <= $month) || ($end_y == '0000' && $start_y < $year && $start_m > $month)) {
					$render_element = 1;	
				} else if ($end_y != '0000' && $start_y <= $year && $start_m <= $month && $end_y >= $year && $end_m >= $month){
					$render_element = 1;
				}
			} else {
				$render_element = -1;
			}
			// load free days for state, month, year
			$this->loadModel('SettingStatSvatek');
			$this->set('svatky',$this->SettingStatSvatek->find('list', array('conditions'=>array( 'setting_stat_id'	=> av(av($company_stat_id,'AtCompany'),'stat_id'), 'mesic'=> $month, 'rok'=> $year ),'fields'=>array('id','den'),'order'=>'den ASC'))); 				
			unset($this->AtCompany);
			unset($this->SettingStatSvatek);
					
		            
            /**
             * osetreni podminky c.1 pro zalohy
             * pokud existuje dochazka v minulem mesici  = zakaz vlozit pozadavek na zalohu 
             * v terminu 15. – 25. dne v mesici
             * 
             * dotazy usetrime tim ze to budeme provadet az 15-25 dnu
             */
             $allow_payment_0 = $company_stat_id['AtCompany']['payment_amount'];
             $allow_payment_1 = $company_stat_id['AtCompany']['payment_cash'];
             if(self::is_in_interval(date('d'),15,25) && (isset($company_stat_id['AtCompany']['payment_once_in_month']) && $company_stat_id['AtCompany']['payment_once_in_month'] == 0)){
                $this->loadModel('ClientWorkingHoursInt');
                $previous_client_working = $this->ClientWorkingHoursInt->find('first',array(
                    'conditions'=>array(
                        'connection_client_at_company_work_position_id'=>$connection_client_at_company_work_position_id,
                        'year'=> $prev_month_date->format('Y'),
                        'month'=> $prev_month_date->format('m')
                    )
                ));
                

                /**
                 * pokud je ve vyjimce tak tuto podminku vynech
                 */
                $this->loadModel('CompanyExceptionPayment');
                $exception_company = $this->CompanyExceptionPayment->findByAtCompanyId($this->data['ClientWorkingHoursInt']['at_company_id']);  
            
            
                /**
                 * jestlize ma v predchozim mesici dochazku tak zamez vystaveni zalohy
                 */
                 if($previous_client_working && !$exception_company)
                   $allow_payment_0 = false;
             }
             else if(isset($company_stat_id['AtCompany']['payment_once_in_month']) && $company_stat_id['AtCompany']['payment_once_in_month'] == 1){
                if(!isset($this->ReportDownPayment))
                    $this->loadModel('ReportDownPayment');
                
                 /**
                  * Nova podminka pokud je zaskrtnuta nova podminka u firem
                  * pouze jednou mesicne
                  * pokud najdeme zalohu v terminu od 15 v danem mesici tak zamez vlozeni
                  * pokud najdeme zalohu z predchazejiciho obdobi (15 minuleho - 14 toho mesice) - zamez vlozeni
                  */
                  $have_payment = false;
                  if(self::is_in_interval(date('d'),1,14)){
                        $have_payment = $this->ReportDownPayment->find('first',array(
                            'conditions'=>array(
                                'kos'=>0,
                                'ReportDownPayment.dochazka_type'=>2,
                                'client_id'=>$this->data['ClientWorkingHoursInt']['client_id'],
                                'company_id'=>$this->data['ClientWorkingHoursInt']['at_company_id'],
                                'connection_client_requirement_id'=>$this->data['ClientWorkingHoursInt']['connection_client_at_company_work_position_id'],
                                'DATE_FORMAT(created,"%Y-%m-%d") >= DATE_SUB(DATE_FORMAT(NOW(),"%Y-%m-15"), INTERVAL 1 MONTH) AND DATE_FORMAT(NOW(),"%Y-%m-%d") <= DATE_FORMAT(NOW(),"%Y-%m-14")'
                            ),
                            'order'=>'id DESC'
                         ));  
                  }
                  else if(self::is_in_interval(date('d'),15,31)){
                        $have_payment = $this->ReportDownPayment->find('first',array(
                            'conditions'=>array(
                                'kos'=>0,
                                'ReportDownPayment.dochazka_type'=>2, 
                                'client_id'=>$this->data['ClientWorkingHoursInt']['client_id'],
                                'company_id'=>$this->data['ClientWorkingHoursInt']['at_company_id'],
                                'connection_client_requirement_id'=>$this->data['ClientWorkingHoursInt']['connection_client_at_company_work_position_id'],
                                'DATE_FORMAT(created,"%Y-%m-%d") >= DATE_FORMAT(NOW(),"%Y-%m-15") AND  DATE_FORMAT(created,"%Y-%m-%d") <= DATE_FORMAT(NOW(),"%Y-%m-31")'
                            ),
                            'order'=>'id DESC'
                         ));  
                  }
                  
                  if($have_payment){
                       $allow_payment_0 = false;     
                  }
             }
             
             $this->set('allow_payment_0', $allow_payment_0);
             $this->set('allow_payment_1', $allow_payment_1);
			 // END podminka 1
             
             
             /**
              * defaultni nastaveni /jinak to prebije nastaveni z komponenty dle formy odmeny
              */
             $this->data['ClientWorkingHoursInt']['setting_shift_working_id'] = 1;
             $this->data['ClientWorkingHoursInt']['stravenky'] = 1;
             $this->data['ClientWorkingHoursInt']['standard_hours'] = 7.5;
            
             
             /**
              * nastaveni promennych s kteryma pracuje komponenta
              * a zavolani setu pro view
              */
             $this->Employees->permission = $this->logged_user['CmsGroup']['permission'][$this->renderSetting['controller']];
             $this->Employees->logged_user = $this->logged_user;
             $this->Employees->set('year',$year);
             $this->Employees->set('month',$month);
             $this->Employees->set('this_data',$this->data);
             $this->Employees->set('dochazka_model','ClientWorkingHoursInt');
             $this->Employees->set('connection_column','connection_client_at_company_work_position_id');
             $this->Employees->set('connection_id',$connection_client_at_company_work_position_id);
             $this->Employees->set_variables_for_view(2);
            
            
            /**
             * nastavení naroku na dovolenou
             * zde pouze odlisujeme zda se jedna o klienta s os. cislem a tedy jeho nula znamena
             * zaden narok na dovolenou
             * a klienty kteri nemaji vyplnene os. cislo a tedy nevime jaky je narok a vlozime otaznik
             * a dovolenou muzeme zadat
             */
             if($this->data['Client']['os_cislo'] == ''){
                $this->data['Client']['zbyva_dovolene'] = '?';
             }
             
             
             		
			// render view
			if ($inner != null)
				if ($render_element === 1)
					$this->render('element');
				else if ($render_element === -1)
					die('<p>Umíte věštit? Jak to víte, kolik toho udělá?</p>');
				else 
					die('<p>Tady vubec nic nedělal!!</p>');
			else
				$this->render('index');
		
        } else {//SAVE		
			$this->loadModel('ClientWorkingHoursInt');

            if($this->data['ClientWorkingHoursInt']['id'] == ''){
                 $this->Employees->set('year',$this->data['ClientWorkingHoursInt']['year']);
                 $this->Employees->set('month',$this->data['ClientWorkingHoursInt']['month']);
                 $this->Employees->set('dochazka_model','ClientWorkingHoursInt');
                 $this->Employees->set('connection_column','connection_client_at_company_work_position_id');
                 $this->Employees->set('connection_id',$this->data['ClientWorkingHoursInt']['connection_client_at_company_work_position_id']);
                
                 if($this->Employees->if_exist_client_working_hour() === true)
                    die(json_encode(array('result'=>false, 'message' => 'Chyba: Snažíte se uložit duplicitní docházku!!! Obnovte okno, nebo zavřete okno a načtěte docházku znova.')));
            }

            // osetreni prazdeho stavu - defaultne 1, nepochopitelna chyba, 
            // nenastavuje db automaticky defaultni hodnotu    
            if($this->data['ClientWorkingHoursInt']['stav'] == '')
                $this->data['ClientWorkingHoursInt']['stav'] = 1;

			if($this->ClientWorkingHoursInt->save($this->data)){
					die(json_encode(array('result'=>true, 'id'=>$this->ClientWorkingHoursInt->id, 'message' => 'Úspěšně uloženo do databáze.')));
			} else {
				die(json_encode(array('result'=>false, 'message' => 'Chyba: Nepodařilo se uložit do DB.')));
			}
		}
	}
	
    
	function uzavrit_odpracovane_hodiny($client_working_hour_id = null){
		if ($client_working_hour_id == null){
			LogError('Controller: employees_controller; Function: uzavrit_odpracovane_hodiny; Chyba! Nejsou známé všechny parametry.');
			die(json_encode(array('result'=>false,'message'=>'Chyba! Nejsou známé všechny parametry.')));
		}
		
		$this->loadModel('ClientWorkingHoursInt');
		$this->ClientWorkingHoursInt->id = $client_working_hour_id;
		$this->ClientWorkingHoursInt->saveField('stav',2);
		
		die(json_encode(array('result'=>true)));
	}
    
    function autorizace_dochazky($client_working_hour_id = null){
		if ($client_working_hour_id == null){
			LogError('Controller: employees_controller; Function: uzavrit_odpracovane_hodiny; Chyba! Nejsou známé všechny parametry.');
			die(json_encode(array('result'=>false,'message'=>'Chyba! Nejsou známé všechny parametry.')));
		}
		
		$this->loadModel('ClientWorkingHoursInt');
		$this->ClientWorkingHoursInt->id = $client_working_hour_id;
		$this->ClientWorkingHoursInt->saveField('stav',3);
		
		die(json_encode(array('result'=>true)));
	}
	
	
	//zobrazeni karty klienta
	function client_info($id = null){
		echo $this->requestAction('clients/edit/'.$id.'/domwin/only_show');
		die();
	}
    
    /**
     * funkce pro zobrazeni domwinu a posleze
     */
     function add_down_payments($type = 0){
         if(!isset($this->data))
            $this->data = $_POST; 
         
         if($this->data['save'] == 0){
            
             /**
              * Druha podminka pro vydani zaloh
              * maximalni limit pro zalohu je 3000 Kc nebo 100€ podle statu
              * s vyjimkou u ktere je zachovan soucasny maximalni limit: pocet hodin x hodinova sazba 
              * Tyto firmy jsou pridavany v Zadosti o zalohy
              * 
              * UPDATE 9.9.2010
              * limit se prenasi z nastaveni firmy
               */
              //$limit = ($this->data['stat_id']==1 ? 3000 : 100);
              
              $exception = false;
              $this->loadModel('CompanyExceptionPayment');
              $exception_company = $this->CompanyExceptionPayment->find('first',array(
                'conditions'=>array(
                    'kos'=>0,
                    'at_company_id'=>$this->data['at_company_id']
                )
              ));  
            
              if($exception_company){      
                $exception = true;  
              }
              $this->data['exception'] = $exception;
              
              
              /**
               * Limit berem z nastaveni firmy
               */
               $this->loadModel('AtCompany');
               $comp = $this->AtCompany->read(array('payment_cash_limit','payment_amount_limit'),$this->data['at_company_id']);
               $this->data['limit'] = ($type == 0 ? $comp['AtCompany']['payment_amount_limit'] : $comp['AtCompany']['payment_cash_limit']);  
               unset($this->AtCompany); 
              //END druha podminka
            
              $this->data['ReportDownPayment'] = array(
                  'year' =>$this->data['year'],
                  'month' =>$this->data['month'],
                  'client_id' =>$this->data['client_id'],
                  'at_company_id' =>$this->data['at_company_id'],
                  'celkem_hodin' =>$this->data['celkem_hodin'],
                  'connection_client_requirement_id' =>$this->data['connection_client_requirement_id'],
                  'cms_user_id' =>$this->logged_user['CmsUser']['id']
              );
              
              $this->set('type',$type);
              
              //render              
              $this->render('add_down_payments');
         }
         else {
            $this->loadModel('ReportDownPayment');
            
            
            /**
             * Novinka od 17.3.10
             * pokud jsme do 15dne a klient ma v predchozim mesici dochazku
             * ulozime zalohu k predeslemu mesici
             */
             if(self::is_in_interval(date('d'),1,15)){
                $prev_month_date = new DateTime();
                $prev_month_date->setDate($this->data['ReportDownPayment']['year'], $this->data['ReportDownPayment']['month'], 01);
                $prev_month_date->modify("-1 month");

                $this->loadModel('ClientWorkingHoursInt');
                $previous_client_working = $this->ClientWorkingHoursInt->find('first',array(
                    'conditions'=>array(
                        'connection_client_requirement_id'=>$this->data['ReportDownPayment']['connection_client_requirement_id'],
                        'year'=> $prev_month_date->format('Y'),
                        'month'=> $prev_month_date->format('m')
                    )
                ));
                
                if($previous_client_working){
                    $this->data['ReportDownPayment']['year'] = $prev_month_date->format('Y');
                    $this->data['ReportDownPayment']['month'] = $prev_month_date->format('m');
                }
                
             } 
            
            
            /**
             * Podminka c.3
             * zalohy jednou za 4 dny
             * vyjimka opet tuto podminku rusi
             */
            $have_payment = $this->ReportDownPayment->find('first',array(
                'conditions'=>array(
                    'kos'=>0,
                    'client_id'=>$this->data['ReportDownPayment']['client_id'],
                    'at_company_id'=>$this->data['ReportDownPayment']['at_company_id'],
                    'connection_client_requirement_id'=>$this->data['ReportDownPayment']['connection_client_requirement_id'],
                    'DATE_FORMAT(created,"%Y-%m-%d") > DATE_SUB(DATE_FORMAT(NOW(),"%Y-%m-%d"), INTERVAL 4 DAY)'
                ),
                'order'=>'id DESC'
             ));
             //pr($have_payment);
             
             if($have_payment && $this->data['exception'] != 1){
                die(json_encode(array('result'=>false,'message'=>'Záloha muže být zadána pouze jednou za 4 dny.')));  
             }
             else{  
                 $this->data['ReportDownPayment']['dochazka_type'] = 2;//typ intr. zam
                 
                 if($this->ReportDownPayment->save($this->data))
                	die(json_encode(array('result'=>true)));
    	         else
            		die(json_encode(array('result'=>false,'message'=>'Chyba během ukládání zálohy.')));  
             }   
         }
     }
     
     
     /**
      * funkce napsana pro zjisteni cisla zda spada do intervalu
      */
     private function is_in_interval($value, $start, $end){
        if($value >= $start && $value <= $end)
            return true;
        else
            return false;    
     }
      
    
    
      
}
?>