<?php
Configure::write('debug',1);
$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');
define('CURRENT_YEAR', $_GET['current_year']);
define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);

define('ccr_con_1',"((DATE_FORMAT(ConnectionClientRequirement.from,'%Y-%m')<= '".CURRENT_YEAR."-".CURRENT_MONTH."'))");
define('ccr_con_2',	'((DATE_FORMAT(ConnectionClientRequirement.to,"%Y-%m") >= "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'") OR (ConnectionClientRequirement.to = "0000-00-00"))');

class ReportDetailToInvoiceItemsController extends AppController {
	var $name = 'ReportDetailToInvoiceItems';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('ClientWorkingHour');
    var $mesic = CURRENT_MONTH;
	var $rok = CURRENT_YEAR;
	var $renderSetting = array(
		'bindModel'	=> array(
            'belongsTo'=>array(
                'Company',
                'ConnectionClientRequirement'=>array(
                        'conditions'=>array(
                            ccr_con_1,
                            ccr_con_2,
                           	'type'=> 2
                        ),
                        'fields'=>array(
                            'ConnectionClientRequirement.id'
                        )
                )
            ),
            'joinSpec'=>array(
                'ConnectionCompanyToAuthorization'=>array(
                    'foreignKey'=>'ConnectionCompanyToAuthorization.company_id',
                    'primaryKey'=>'ClientWorkingHour.company_id',
                    'conditions'=>array(
                         'ConnectionCompanyToAuthorization.year'=> CURRENT_YEAR,
    			         'ConnectionCompanyToAuthorization.month'=> CURRENT_MONTH
                    )
                )
            )
        ),
		'controller'=>'report_detail_to_invoice_items',
		//'SQLfields' => array('CompanyMoneyItem.fakturacni_sazba_na_hodinu','CompanyMoneyItem.provinzni_marze','CompanyMoneyItem.vypocitana_celkova_cista_maximalni_mzda_na_hodinu','CompanyMoneyItem.cena_ubytovani_na_mesic','Company.name','Company.self_manager_id','Company.client_manager_id','Company.coordinator_id','CompanyWorkPosition.name','CompanyMoneyValidity.platnost_od','CompanyMoneyItem.id','CompanyMoneyItem.name','CompanyMoneyItem.created','Company.client_manager_id','CompanyMoneyItem.id'),
		'SQLfields' => array('*,
                SUM(salary_per_hour_p) as total,
                ROUND(SUM(celkem_hodin),1) as sum_celkem_hodin,
                COUNT(DISTINCT ClientWorkingHour.client_id) as pocet_zamestnancu
        '),
        'page_caption'=>'Podklady pro fakturace za odpracovane hodiny',
        'count_col'=>'ClientWorkingHour.company_id',
        'group_by' => 'ClientWorkingHour.company_id,ClientWorkingHour.year, ClientWorkingHour.month',
		'sortBy'=>'NULL.total.DESC',
		'SQLcondition'=>array(
			'ClientWorkingHour.kos'=>0,
            'ClientWorkingHour.year'=> CURRENT_YEAR,
			'ClientWorkingHour.month'=> CURRENT_MONTH,
            'ConnectionClientRequirement.id <>'=>0
		),
		'top_action' => array(
			// caption|url|description|permission
			//'add_item'		=>	'Přidat|edit|Pridat popis|add',
		),
		'filtration' => array(
			'ClientWorkingHour-company_id'		=>	'select|Firma|company_list',
			'GET-current_month'					=>	'select|Měsíc|mesice_list',
            'GET-current_year'					=>	'select|Rok|actual_years_list',
            'Company-client_manager_id|coordinator_id|coordinator_id2'					=>	'select|Koordinátor|coo_list',
		    'Company-parent_id'	=>	'select|Nadřazená f.|parent_company_list',
        ),
		'items' => array(
			'id'		=>	'ID|ClientWorkingHour|id|hidden|',
			'company'	=>	'Firma|Company|name|text|',
            'stav'				=>	'Stav|ConnectionCompanyToAuthorization|stav|var|stav_company_auth_list|',
		
			'year'	=>	'Rok|ClientWorkingHour|year|text|',
			'month'	=>	'Měsíc|ClientWorkingHour|month|text|',
			'sum_celkem_hodin'	=>	'Celkem hodin|0|sum_celkem_hodin|text|',
			'pocet_zamestnancu'	=>	'Počet zaměstnanců|0|pocet_zamestnancu|text|',
			'total'	=>	'Celkem fakturace|0|total|text|',
		),
		'posibility' => array(
			'show'		=>	'show_podklady|Zobrazit detail|show',			
		//	'edit'		=>	'edit|Editovat položku|edit',			
		), 
        'domwin_setting' => array(
            'sizes' => '[1000,1000]', 
            'scrollbars' => true, 
            'languages' => true 
        ),
        'sum_variables_on_top'=>array(
            'path'=>'../report_detail_to_invoice_items/filtration_top'
        )
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#',$this->renderSetting['page_caption']=>'#'));
		
			$this->loadModel('CmsUser'); 
			$this->loadModel('Company'); 
			$this->set('coo_list',		$this->CmsUser->find('list',array('conditions'=>array('CmsUser.status'=>1,'CmsUser.kos'=>0,'CmsUser.cms_group_id'=>4))));
			$this->set('company_list',		$this->Company->find('list',array(
				'conditions'=>array(
					'Company.kos'=>0
				),
				'order'=>array('Company.name ASC')
			)));
			unset($this->Company);
			unset($this->CmsUser);
            
            //list nadrazených spolecnosti
            $this->set('parent_company_list',$this->get_list('SettingParentCompany'));
			
            
            //top pocty
            $this->ClientWorkingHour->bindModel($this->renderSetting['bindModel']);
    		$rr = $this->ClientWorkingHour->find('all',array(
                'conditions'=>$this->ViewIndex->criteria,
                'fields'=>array('
                        IFNULL(SUM(salary_per_hour_p),0) as total,
                        IFNULL(ROUND(SUM(celkem_hodin),1),0) as sum_celkem_hodin,
                        IFNULL(COUNT(DISTINCT ClientWorkingHour.client_id),0) as pocet_zamestnancu
                '),
                'group'=>'ClientWorkingHour.year, ClientWorkingHour.month',
            ));
            $this->set('filtration_sum_variables',$rr);  
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
    
    
    function show_podklady($company_id,$year,$month){
		if ($company_id != null){
		    $month = ($month<10) ? '0'.$month : $month;  
			$this->ClientWorkingHour->bindModel(array(
			   'belongsTo'=>array(
                    'Client'=>array('fields'=>array('name')),    
                    'CompanyWorkPosition',
                    'ConnectionClientRequirement'=>array(
                        'conditions'=>array(
                            "((DATE_FORMAT(ConnectionClientRequirement.from,'%Y-%m')<= '".$year."-".$month."'))",
		                  	'((DATE_FORMAT(ConnectionClientRequirement.to,"%Y-%m") >= "'.$year.'-'.$month.'") OR (ConnectionClientRequirement.to = "0000-00-00"))',
		                  	'type'=> 2
                        ),
                        'fields'=>array(
                            'ConnectionClientRequirement.id'
                        )
                    )
               )
		    ),false);
	
			$this->set('show_detail',$this->ClientWorkingHour->find('all',array(
				'conditions'=>array(
					'ClientWorkingHour.company_id'=>$company_id,
					'ClientWorkingHour.year'=>$year,
					'ClientWorkingHour.month'=>$month,
                    'ConnectionClientRequirement.id <>'=>0
				),
				'fields'=>array(
					'CompanyWorkPosition.name',
					'ROUND(SUM(celkem_hodin),1) as celkem_hodin'
				),
				'order'=>'celkem_hodin DESC',
				'group'=>'ClientWorkingHour.company_work_position_id'
			)));
            
            $this->set('zamestnanci_list',$this->ClientWorkingHour->find('all',array(
                'conditions'=>array(
                     'ClientWorkingHour.company_id'=>$company_id,
                     'ClientWorkingHour.year'=>$year,
                     'ClientWorkingHour.month'=>$month,
                     'ClientWorkingHour.celkem_hodin <>'=>0,
                     'ConnectionClientRequirement.id <>'=>0
                ),
                'fields'=>array('Client.name','celkem_hodin'),
                'order'=>'Client.name ASC'
            )));

			unset($this->ClientWorkingHour);

			//render
			$this->render('show');	
	    }
		else 
			die('Bez id ? :-)');
		
		
	}	
	
	
}
?>
