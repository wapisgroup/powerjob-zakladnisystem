<?php
Configure::write('debug',1);
	define('fields','AuditEstate.*,AtCompany.name, AtProjectCentre.spravce_majetku_id, SpravceMajetku.name,
        ConnectionAuditEstate.id,ConnectionAuditEstate.renting_connection,
        IF((
           (AuditEstate.first_cost/(SELECT SUM(price) FROM wapis__audit_estate_repairs Where audit_estate_id=AuditEstate.id AND kos=0)) <= 5
        ),1,0) as repair_high, 
        ( 
		  if(AuditEstate.stav = 1,
            Client.name,
            SpravceMajetku.name
          )  
		) as osoba,	
        ( 
		  if(AuditEstate.stav = 1,
            ConnectionAuditEstate.centre,
            AtProjectCentre.name
          )  
		) as stredisko,
        ( 
          CONCAT_WS(" ",AuditEstate.first_cost,if(AuditEstate.currency = 0,",- EUR",",- Kč"))  
		) as first_cost	,
        ( 
          CONCAT_WS(" ",IFNULL(AuditEstate.hire_price,0),if(AuditEstate.currency = 0,",- EUR",",- Kč"))  
		) as hire_price	,
        IF(ConnectionAuditEstate.renting_connection IS NULL,"u sebe",CONCAT_WS(" ","u leasing. zam.",(SELECT cl.name FROM wapis__connection_audit_estates as cae LEFT JOIN wapis__clients as cl ON (cl.id = cae.client_id) WHERE cae.id = ConnectionAuditEstate.renting_connection))) as stav
	');
class MyAuditEstatesController extends AppController {
	var $name = 'MyAuditEstates';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('AuditEstate');
	var $renderSetting = array(
        'bindModel'	=> array(
			'belongsTo'=>array('AtCompany','AtProjectCentre'),
			'hasOne'=>array(
				'ConnectionAuditEstate'=>array(
					'conditions'=>array(
						'ConnectionAuditEstate.kos'=>0,
						//'ConnectionAuditEstate.to_date'=>'0000-00-00'
					)
				)
			),
			'joinSpec'=>array(
			    'Client'=>array(
    				'className'=>'Client',
    				'primaryKey'=>'Client.id',
    				'foreignKey'=>'ConnectionAuditEstate.client_id'
			    ),
                'SpravceMajetku'=>array(
    				'className'=>'CmsUser',
    				'primaryKey'=>'SpravceMajetku.id',
    				'foreignKey'=>'AtProjectCentre.spravce_majetku_id'
			    )
			)
		),
		'SQLfields' =>array(fields),
        'SQLcondition' => array(
			'AuditEstate.elimination_date IS NULL',
            'ConnectionAuditEstate.client_id'=>"#CmsUserAtEmployeeId#",
            '(ConnectionAuditEstate.to_date = "0000-00-00" 
                OR 
              (ConnectionAuditEstate.renting_connection IS NOT NULL AND (SELECT to_date FROM wapis__connection_audit_estates WHERE id = ConnectionAuditEstate.renting_connection) = "0000-00-00"))'
		),
		'controller'=> 'my_audit_estates',
		'page_caption'=>'Můj majetek',
		'sortBy'=>'AuditEstate.name.ASC',
        'group_by'=>'AuditEstate.id',
		'top_action' => array(),
		'filtration' => array(
		    //'AuditEstateWithdrawalType-form_template_group_id'		=>	'select|Skupina|group_list'
		),
		'items' => array(
			'id'		=>	'ID|AuditEstate|id|text|',
			'name'		=>	'Název|AuditEstate|name|text|',
			'imei'	      =>	'IMEI/SN/VIN|AuditEstate|imei_sn_vin|text|',
			'int_numb'	      =>	'Int. číslo|AuditEstate|internal_number|text|',
			'company'	=>	'Firma|AtCompany|name|text|',
			'company_centre'	=>	'Středisko|0|stredisko|text|',
			'company_person'	=>	'Osoba|0|osoba|text|',
			'first_cost'	=>	'Pořizovací cena|0|first_cost|text|',
			'payment_date'	=>	'Datum pořízení|AuditEstate|payment_date|date|',
			'hire_price'	=>	'Měs. pronájem|0|hire_price|text|',	
            'stav'	=>	'Stav|0|stav|text|',	
		),
		'posibility' => array(
			'manage_estate'		=>	'manage_estate|Předání majetku|manage_estate',	
		),
        'posibility_link' => array(
			'manage_estate'		=>	'AuditEstate.id/ConnectionAuditEstate.id/ConnectionAuditEstate.renting_connection',	
		),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'	=> 'true',
			'languages'		=> 'false'
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Můj majetek'=>'#'));
        
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function manage_estate($audit_estate_id = null, $connection_coo_id = null,$renting_connection = null){

		$this->autoLayout = false;
		if (empty($this->data)){
			    $this->data['audit_estate_id'] = $audit_estate_id;
                $this->data['old_connection_id'] = $connection_coo_id;
                $this->data['renting_connection'] = $renting_connection;
            
            $this->loadModel('ConnectionAuditEstate'); 
            
            
            if($renting_connection == null){ //coo predava na leasingoveho zamestnance 
                $this->set('old',$this->ConnectionAuditEstate->read('created',$connection_coo_id));
              
                $this->loadModel('Company');
                $this->set('at_company_list',$this->Company->find('list',array(
                    'conditions'=>array('at_company_id IS NOT NULL','kos'=>0),
                    'order'=>'name ASC'
                )));
                $this->render('edit');
            }
            else{ // coo si bere majetek od leasingoveho zpet
                $this->set('old',$this->ConnectionAuditEstate->read('created',$renting_connection));
            
                $this->render('edit2');
            }
			
		} else {
		    
            $this->loadModel('ConnectionAuditEstate'); 
            if(!isset($this->data['renting_connection']) || $this->data['renting_connection'] == ''){
                $this->loadModel('Company'); 
                $comp = $this->Company->read('at_company_id',$this->data['company_id']);
                /**
                 * Vytvorime nove connection pro leasingoveho zamestnance
                 */
                $this->ConnectionAuditEstate->id = null;
                $new_data = array(
                    'audit_estate_id'=>$this->data['audit_estate_id'],
                    'at_company_id'=>$comp['Company']['at_company_id'],
                    'client_id'=>$this->data['client_id'],
                    'centre'=>'Realizácia',
                    'created'=>$this->data['od'],
                    'cms_user_id'=>$this->logged_user['CmsUser']['id']
                );
                $this->ConnectionAuditEstate->save($new_data);
                $new_id = $this->ConnectionAuditEstate->id;
                /**
                 * vraceni majetku od COO 
                 */          
                $this->ConnectionAuditEstate->id = $this->data['old_connection_id'];
                $this->ConnectionAuditEstate->save(array('to_date'=>$this->data['do'],'renting_connection'=>$new_id));
            }
            else{
                //ukoncime pomer pro leasingoveho zam.
                $this->ConnectionAuditEstate->id = $this->data['renting_connection'];
                $this->ConnectionAuditEstate->saveField('to_date',$this->data['do']);

                $old_conection = $this->ConnectionAuditEstate->read(array('audit_estate_id','at_company_id','client_id','centre'),$this->data['old_connection_id']); 
                //ulozeni noveho prebrani COO
                $old_conection['ConnectionAuditEstate']['created'] = $this->data['od'];
                $old_conection['ConnectionAuditEstate']['cms_user_id'] = $this->logged_user['CmsUser']['id'];
                $this->ConnectionAuditEstate->id = null;
                $this->ConnectionAuditEstate->save($old_conection);
            }

            die(json_encode(array('result'=>true)));
		}
	}
    
   	/**
 	* AJAX nacteni zamestancu, na ktere muzem predat majetek
	**/
	function load_ajax_employees($company_id = null){
		
		if ($company_id != null){
		    $this->loadModel('ConnectionClientRequirement'); 
            $this->ConnectionClientRequirement->bindModel(array('belongsTo'=>array('Client')));
			$zams = $this->ConnectionClientRequirement->find('all',array(
                'conditions'=>array(
                    'ConnectionClientRequirement.company_id'=>$company_id,
                    'ConnectionClientRequirement.kos'=>0,
                    'ConnectionClientRequirement.new'=>0,
                    'ConnectionClientRequirement.type'=>2,
                    'ConnectionClientRequirement.to'=>'0000-00-00'
                ),
                'fields'=>array('Client.id','Client.name')
            ));
		
            
			$out = array(); 
            $out = Set::combine($zams,'{n}.Client.id','{n}.Client.name');    
    		die(json_encode($out));

		} 
        else
            die(json_encode(false));
	}

}
?>