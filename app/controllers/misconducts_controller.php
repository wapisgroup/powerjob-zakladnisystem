<?php
Configure::write('debug',1);
class MisconductsController extends AppController {
	var $name = 'Misconducts';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('Misconduct');
	var $renderSetting = array(
		'controller'=>'misconducts',
		'SQLfields' => '*',
		'page_caption'=>'Nastavení prohřešků',
		'sortBy'=>'Misconduct.created.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat důvod|add',
		//	'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
		//	'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
		//	'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
		//	'SettingAccommodationType-status'	=>	'select|Stav|select_stav_zadosti',
		//	'SettingAccommodationType-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|Misconduct|id|text|',
			'name'		=>	'Název|Misconduct|name|text|',
			'frequency'		=>	'Četnost|Misconduct|frequency|text|',
			'go_to_rank_1'		=>	'Posun o hodnoceni 1|Misconduct|go_to_rank_1|text|',
			'go_to_rank_2'		=>	'Posun o hodnoceni 2|Misconduct|go_to_rank_2|text|',
			'updated'	=>	'Upraveno|Misconduct|updated|datetime|',
			'created'	=>	'Vytvořeno|Misconduct|created|datetime|'
		),
		'posibility' => array(
			'status'	=> 	'status|Změna stavu|status',
			'edit'		=>	'edit|Editace položky|edit',
			'delete'	=>	'trash|Do košiku|trash'			
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Nastavení prohřešků'=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null)
				$this->data = $this->Misconduct->read(null,$id);
			$this->render('edit');
		} else {
			$this->Misconduct->save($this->data);
			die();
		}
	}
    
    
    function add($type = 0){
		if (empty($this->data)){
		    if(!isset($_POST['client_id']))
                die('Spatny parametr ID clienta!');  
          
            $this->loadModel('Client');
			$cl = $this->Client->read('hodnoceni',$_POST['client_id']);
            $this->set('client_id',$_POST['client_id']);
            $this->set('hodnoceni',$cl['Client']['hodnoceni']);
            $this->set('problem_list',$this->get_list('Misconduct'));
            
            if(isset($_POST['year']) && isset($_POST['month'])){
               $this->set('year',$_POST['year']);
               $this->set('month',$_POST['month']);
            }
            
            $this->set('type',$type);
			$this->render('add');
		} else {
			self::save();
			die();
		}
	}
    
    function load_problem_list(){
        die(json_encode($this->Misconduct->find('list', array('conditions' => array('Misconduct.kos' => 0),'order' => 'name ASC'))));  
    }
    
    
    function save(){
         if(isset($this->params['request_data']) || (isset($this->data) && $this->data['action'] == 'add_misconduct' )){
            if(isset($this->params['request_data']))
               $data = $this->params['request_data'];
         
            /**
             * Pokud posilame do save this->data a neposilame je pres request action
             */
            if(isset($this->data['action']))
                $data = $this->data;
            
            $data['misconduct_id'] = $data['problem'];
            
            $this->loadModel('ConnectionClientMisconduct');
            $count_problemu = $this->ConnectionClientMisconduct->find('count',array(
                'conditions'=>array(
                    'client_id'=>$data['client_id'],
                    'misconduct_id'=>$data['misconduct_id']
                )
            ));
            
            
            $dusledky = $this->Misconduct->read(array('frequency','go_to_rank_1','go_to_rank_2','ignore_freq'),$data['problem']);
            
            if($dusledky['Misconduct']['ignore_freq'] == 0){//pouze ktere se maji propisovat do hodnoceni
                unset($this->Misconduct);
                $this->loadModel('Client');
                $this->Client->id = $data['client_id'];
                
                if($count_problemu == 0 && $data['hodnoceni'] > $dusledky['Misconduct']['go_to_rank_1']){
                    /**
                     * Problem se stal poprve
                     */ 
                    $this->Client->saveField('hodnoceni',$dusledky['Misconduct']['go_to_rank_1']);
                }
                else if($count_problemu == $dusledky['Misconduct']['frequency'] && $data['hodnoceni'] > $dusledky['Misconduct']['go_to_rank_2']){
                    /**
                     * Opakovany problem
                     */
                    $this->Client->saveField('hodnoceni',$dusledky['Misconduct']['go_to_rank_2']); 
                }
                unset($this->Client);
            }
            
            /**
             * Finalni ulozeni connection
             */
            $this->ConnectionClientMisconduct->save($data); 
            unset($this->ClConnectionClientMisconductient);
            
         }
    }
}
?>