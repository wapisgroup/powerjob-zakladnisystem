<?php
Configure::write('debug',0);
/**
 * 
 * @author Zbyněk Strnad - Fastest Solution s.r.o.
 * @created 12.11.2009
 */

class HistoryItemsController extends AppController {
	var $name = 'HistoryItems';
	var $helpers = array('htmlExt','Pagination','ViewIndex','Excel');
	var $components = array('ViewIndex','RequestHandler','SmsClient');
	var $uses = array('HistoryItem');
	var $renderSetting = array(
		'bindModel'=>array(
			'belongsTo'=>array(
				'Client'
			)
		),
		'SQLfields' => array('*'),
		'controller'=> 'history_items',
		'page_caption'=>'Historie - itms',
		'sortBy'=>'HistoryItem.created.DESC',
		'top_action' => array(
            'export_excel' 		=> 	'Export Excel|export_excel|Export Excel|export_excel',
		),
		'filtration' => array(
			'HistoryItem-cms_user_id'		=>	'select|Uživatel|cms_user_list',
            'HistoryItem-action_id'		=>	'select|Akce|history_type_list',
		),
		'items' => array(
			'id'			=>	'ID|HistoryItem|id|hidden|',
			'client'		=>	'Klient|HistoryItem|client_id|var|client_list',
			'uzivatel'		=>	'Uživatel|HistoryItem|cms_user_id|var|cms_user_list',
			'controller'	=>	'Kategorie|HistoryItem|category_id|var|history_category',
			'akce'			=>	'Akce|HistoryItem|action_id|var|history_type_list',
			'datum'			=>	'Datum|HistoryItem|created|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Změny|edit'		
		)
	);
	
	function beforeFilter(){
		parent::beforeFilter();
		$this->loadModel('Client');
		$this->Client->query("SET NAMES 'utf8'");
	}
	
	function export_excel(){
	   $start = microtime ();
       $start = explode ( " " , $start );
       $start = $start [ 1 ]+ $start [ 0 ];  
       

         $fields_sql = array(
            'HistoryItem.id',
            'HistoryItem.client_id',
            'HistoryItem.cms_user_id',
            'HistoryItem.category_id',
            'HistoryItem.action_id',
            'HistoryItem.created',
        );
         
        //vars 
        $this->set('client_list',$this->get_list('Client'));
		$this->loadModel('CmsUser');
		$this->CmsUser->query("SET NAMES 'utf8'");
		$this->set('cms_user_list',$this->CmsUser->find('list',array('order'=>'name ASC','conditions'=>array('kos'=>0,'status'=>1))));
		unset($this->CmsUser);
		$this->set('client_list',$this->get_list('Client'));
		$this->loadModel('HistoryType');
		$this->set('history_type_list',$this->HistoryType->find('list',array('order'=>'name ASC','conditions'=>array('kos'=>0))));
		unset($this->HistoryType);
               
        $criteria = $this->ViewIndex->filtration(); 
        if(empty($criteria)){
            die('Nelze exportovat bez filtace uživatele nebo akce!');
        }   
        $render_condition = $this->ViewIndex->foreach_SQLcondition($this->renderSetting['SQLcondition']);    
          
         header("Pragma: public"); // požadováno
         header("Expires: 0");
         header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
         header("Cache-Control: private",false); // požadováno u některých prohlížečů
         header("Content-Transfer-Encoding: binary");
         Header('Content-Type: application/octet-stream');
         Header('Content-Disposition: attachment; filename="'.date('Ymd_His').'.csv"');
     
      
        /*
         * Celkovy pocet zaznamu
         */
         
        $limit = 100;   
        $page = 1;     
        $this->HistoryItem->bindModel($this->renderSetting['bindModel'],false);   
        $count = $this->HistoryItem->find('first',
        	array(
        		'fields' =>  array("COUNT(DISTINCT HistoryItem.id) as count"),
        		'conditions'=>am($criteria,$render_condition),
        		'recursive'	=>1
        	)
        );    
        $count = $count[0]['count'];
        // hlavicka
        foreach($this->renderSetting['items'] as &$item_setting){
            list($caption, $model, $col, $type, $fnc) = explode('|',$item_setting);
        	$item_setting = compact(array("caption", "model","col","type","fnc"));	
            if($type != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',$caption).'";'; 
        }
        echo "\n";   
        unset($item_setting, $caption, $model, $col, $type, $fnc);
        
        $str_array=array("<br/>"=>', ',':'=>',',';'=>',','?'=>'', '#'=>' ');
        /*
         * Cyklicky vypis dat po $limit zaznamu
         */
        
        for ($exported = 0; $exported < $count; $exported += $limit){
            $this->HistoryItem->bindModel($this->renderSetting['bindModel'],false);
            foreach($items = $this->HistoryItem->find('all',array(
                   'fields'=>$fields_sql,
                    'conditions'=>am($criteria,$render_condition),
                    'limit'=>$limit,
                    'page'=>$page,
                    'recursive'=>1,
                    'order'=>'HistoryItem.created DESC'
            )) as $item){
                foreach($this->renderSetting['items'] as $key => $td){
                    if($td['type'] != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',strtr($this->ViewIndex->generate_td($item,$td,$this->viewVars),$str_array)).'";';     
                }
                echo "\n";  
            }
            $page++;
        }
       
        //time
         $end = microtime ();
         $end = explode ( " " , $end );
         $end = $end [ 1 ]+ $end [ 0 ]; 
         echo 'Generate in '.($end - $start).'s';
         echo "\n"; 
         die();
	}
	
	/**
	 * 
	 * @return view
	 */
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/',$this->renderSetting['page_caption']=>'#'));
	
		$this->loadModel('CmsUser');
		$this->CmsUser->query("SET NAMES 'utf8'");
		$this->set('cms_user_list',$this->CmsUser->find('list',array('order'=>'name ASC','conditions'=>array('kos'=>0,'status'=>1))));
		unset($this->CmsUser);
				
		$this->loadModel('HistoryType');
		$this->set('history_type_list',$this->HistoryType->find('list',array('order'=>'name ASC','conditions'=>array('kos'=>0))));
		unset($this->HistoryType);
        
        $this->set('client_list',$this->get_list('Client'));
		        
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		
		$this->HistoryItem->bindModel(array(
			'hasMany'=>array('HistoryData'),
			'belongsTo'=>array('Client','CmsUser','HistoryType'=>array('foreignKey'=>'action_id'))
		),false);
		
		$this->data = $this->HistoryItem->read(null,$id);
		
		$last = $this->HistoryItem->find(
			'first',
			array(
				'conditions' => array(
					'HistoryItem.action_id' =>$this->data['HistoryItem']['action_id'],
					'HistoryItem.category_id' => $this->data['HistoryItem']['category_id'],
					'HistoryItem.client_id' => $this->data['HistoryItem']['client_id'],
					'HistoryItem.id < ' => $id
				),
				'order' => 'HistoryItem.id DESC'
			)
		);

		$this->set('last_item',$last); 
	}
	
	
	
}
?>