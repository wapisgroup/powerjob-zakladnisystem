<?php
Configure::write('debug',1);
class OppItemsController extends AppController {
	var $name = 'OppItems';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload');
	var $uses = array('OppItem');
	var $renderSetting = array(
		'controller'=>'opp_items',
		'SQLfields' => array('*'),
		'page_caption'=>'Evidence pracovních pomůcek',
		'sortBy'=>'OppItem.name.ASC',
		'top_action' => array(
			'import_csv' 	=> 'Import CSV|import_csv|Import CSV|import_csv', 
			'setting' 		=> 'Nastavení kurzu|setting|Nastavení kurzu|setting', 
            'add' 			=> 'Přidat Opp|edit|Přidat Opp|add', 
		),
		'filtration' => array(
		),
		'items' => array(
			'id'		=>	'ID|OppItem|id|text|',
			'name'		=>	'Typ|OppItem|type|text|',
			'type'		=>	'Název|OppItem|name|text|',
			'kod'		=>	'Kód|OppItem|code|text|',
			'size'		=>	'Velikost|OppItem|size|text|',
			'count'		=>	'Počet|OppItem|count|text|',
			'cenaCZK'	=>	'Cena CZK|OppItem|price_cz|text|',
			'cenaEURO'	=>	'Cena EURO|OppItem|price_euro|text|',
			'rate'		=>	'Kurz|OppItem|rate|text|',
//			'updated'	=>	'Upraveno|OppItem|updated|datetime|',
//			'created'	=>	'Vytvořeno|OppItem|created|datetime|'
		),
		'posibility' => array(
			'status'	=> 	'status|Změna stavu|status',
			'edit'		=>	'edit|Editace položky|edit',
			'trash'	=>	'trash|Do kosiku|trash'			
		)
	);
	function index(){
	 
		$this->set('fastlinks',array('ATEP'=>'/','Pomocník'=>'#','Seznam OPP'=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->set('scripts',array('uploader/uploader'));
			$this->set('styles',array('../js/uploader/uploader'));
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		//if (empty($this->data) && $id === null){
		//	self::setting();
		//	exit();
		//}
	
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null){
				$this->data = $this->OppItem->read(null,$id);
			}else{
                $this->loadModel('OppItem');
                $sett = $this->OppItem->find('first',array('fields'=>array('rate')));
                unset($this->OppItem);
                $this->data['OppItem']['rate'] = $sett['OppItem']['rate'] ;
            }
            
            $this->loadModel('OppCategory');
            $this->set('type_list',$this->OppCategory->find('list',array('conditions'=>array('kos'=>0,'status'=>1),'fields'=>array('name','name'))));
			$this->render('edit');
		} else {
			if ($this->OppItem->save($this->data))
				die(json_encode(array('result'=>true)));
			else
				die(json_encode(array('result'=>false,'message'=>'Chyba behem ukladani do DB')));
		}
	}
	
	function setting(){
		if (empty($this->data)){
			$this->data = $this->OppItem->find('first',array('fields'=>array('rate')));
			$this->render('setting');
		} else {
			foreach($this->OppItem->find('all',array('fields'=>array('id','price_cz'))) as $item){
				$this->OppItem->save(array(
					'id' => $item['OppItem']['id'],
					'rate' => $this->data['OppItem']['rate'],
					'price_euro' => $item['OppItem']['price_cz'] / $this->data['OppItem']['rate']
				));
				$this->OppItem->id = null;
			}
			die(json_encode(array('result'=>true)));
		}
	}
	
	function import_csv(){
	   if(empty($this->data)){
    	    $wt = $this->OppItem->find('all',array(
                'conditions'=>array('type'=>'-1','kos'=>0),
                //'fields'=>array('rate')
            ));
            
            $this->loadModel('OppCategory');
            $this->set('type_list',$this->OppCategory->find('list',array('conditions'=>array('kos'=>0,'status'=>1),'fields'=>array('name','name'))));
    	
            $this->set('without_type_list',$wt);
            $this->render('import_csv');
       }
       else{
            /**
             * Ulozeni nastavenych OPP a jejich kategorii
             */
            if(isset($this->data['id'])){
                $type = $this->data['type'];
                foreach($this->data['id'] as $id=>$bool){
                    if($bool == 1 ||$bool == 'on' && isset($type[$id]) && $type[$id] != ''){
                        $this->OppItem->id = $id;
                        $this->OppItem->saveField('type',$type[$id]);
                    }
                }
           } 
       }
    }

	
    function upload_file(){
        $this->Upload->set('data_upload',$_FILES['upload_file']);
		if ($this->Upload->doit(json_decode($this->data['upload']['setting'],true))){
			echo json_encode(array('upload_file'=>array('name'=>$this->Upload->get('outputFilename')),'return'=>true));
		} else 
			echo json_encode(array('return'=>false,'message'=>$this->Upload->get('error_message')));		
		die();
    }
    
    function import_validate($file = null, $pohoda = false){
        /**
         * Validujeme soubor, a kontrolujeme zda vsechny zadane kategorii mame v DB
         */
        if($pohoda == false){ 
            $this->loadModel('OppCategory');
            $cat_list = $this->OppCategory->find('list',array(
                'conditions'=>array('kos'=>0,'status'=>1),
                'fields'=>array('parent_id','name')
            ));
        }
        
        $errors_cat = array();
        
        if (($handle = fopen('./uploaded/imports/'.$file, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                if($pohoda == true){
                    if(count($data) == 8){
                        die(json_encode(array('result'=>true)));
                    }
                    else{
                         die(json_encode(array('result'=>false,'message'=>'Soubor z Pohody, musí mít 8 sloupců!')));
                    }
                }
                
                if(isset($data[0])){
                	if(!array_key_exists($data[0],$cat_list)){
                	   $errors_cat[] = $data[0];
                	}
                } 
            }
            fclose($handle);
            
            if(empty($errors_cat)){
                die(json_encode(array('result'=>true)));
            }
            else{
                $errors_cat = array_unique($errors_cat);
                die(json_encode(array('result'=>false,'message'=>'Kategorie ['.join(",",$errors_cat).'] nejsou definovana v cisleniku OPP')));
            }
		}
        else{
            die(json_encode(array('result'=>false,'message'=>'Soubor nenalezen')));
        }
    }
    
	function import($file, $pohoda = false){
	    $d_rate = $this->OppItem->find('first',array('fields'=>array('rate')));
        $rate = $d_rate['OppItem']['rate'];
		if (($handle = fopen('./uploaded/imports/'.$file, "r")) !== FALSE) {
		  $i = 0;
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                if(isset($data[0])){
                    if($pohoda == true){
                        if($i > 0){ self::import_pohoda($data,$rate); }
                    }
                    else
                        self::import_default($data,$rate);
                } 
                $i++;
            }
            die(json_encode(array('result'=>true)));
		}
        else
         die(json_encode(array('result'=>false,'message'=>'Soubor nebyl nalezen.')));	
	}
    
    function import_pohoda($data,$rate){
        	list($bool,$kod,$nazev,$price_eu,$pocet,$rec,$karta,$cena2) = $data;
        	$vel = self::get_size_from_code($kod);
          
          /**
           * zname cneu v eurech - prepocitame i koruny
           */
            $price_eu = (float)trim(strtr(substr($price_eu,0,-2),array(','=>'.')));
            $price_cz = (float)($price_eu * $rate);
             
        	$to_save = array(
        		'name'	=> iconv('cp1250','utf8',$nazev),
        		'size'	=> $vel,
        		'type'	=> '-1',//musíme ještě dodefinovat
        		'code'	=> $kod,
        		'count'	=> $pocet,
                'price_cz'=>$price_cz,
                'rate'=>$rate,
                'price_euro'=>$price_eu	
        	);
        	
        	$row_exist = $this->OppItem->find('first',array(
                'conditions'=>array('code'=>$kod,'kos'=>0)
            ));
        	if ($row_exist){              	   
        		$sav = array(
        			'id'	=> $row_exist['OppItem']['id'],
        			'count' => $pocet,
                    'price_cz'=>$price_cz,
                    'rate'=>$rate,
                    'price_euro'=>$price_eu	
        		);
                $this->OppItem->save($sav);
        		$this->OppItem->id = null;
                echo 'akt<br />';
        	} else {
        		$this->OppItem->save($to_save);
        		$this->OppItem->id = null;
                echo 'new<br />';
                pr($to_save);
            die();
        	}
            
    }
    
    function import_default($data,$rate){
            $this->loadModel('OppCategory');
            $cat_list = $this->OppCategory->find('list',array(
                'conditions'=>array('kos'=>0,'status'=>1),
                'fields'=>array('parent_id','name')
            ));
            
        	list($kategorie,$nazev, $vel, $pocet) = $data;
        	list($kod, $nazev) = explode(':',$nazev);
        	list($prefix, $_vel) = explode('-', $kod);
        	if ($vel == ''){
        	   self::get_size_from_code($_vel);
        	}   
            
            /**
             * Cena CZK 
             */  
            if(isset($data[4])){ $price_cz = $data[4]; } 
            else{ $price_cz = 0; }
             
        	$to_save = array(
        		'name'	=> iconv('cp1250','utf8',$nazev),
        		'size'	=> $vel,
        		'type'	=> $cat_list[$kategorie],
        		'code'	=> $kod,
        		'count'	=> $pocet,
                'price_cz'=>$price_cz	
        	);
        	
        	$row_exist = $this->OppItem->find('first',array(
                'conditions'=>array('code'=>$kod,'kos'=>0)
            ));
        	if ($row_exist){              	   
        		$sav = array(
        			'id'	=> $row_exist['OppItem']['id'],
        			'count' => (integer)$pocet
        		);
                $this->OppItem->save($sav);
        		$this->OppItem->id = null;
        	} else {
        		$this->OppItem->save($to_save);
        		$this->OppItem->id = null;
        	}
    }
    
    function get_size_from_code($code){
            $code = substr($code,-2);
            if(is_integer($code))
                return (int)$code;
            else{    
            	switch($code){
        			case '0S':	$vel = 'S'; 	break;
    				case '0M':	
                    case '4M':
                        $vel = 'M'; 	break;
    				case '0L':
                    case 'ZL':
                    case '4L':
                    	$vel = 'L'; 	break;
    				case '1X':	$vel = 'XL';	break;
    				case '2X':	$vel = 'XXL';	break;
    				case '3X':	$vel = 'XXXL';	break;
        			default: 	$vel = '??';	break;
               }
           }
           
           return $vel;
    }
}
?>