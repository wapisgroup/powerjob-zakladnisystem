<?php
Configure::write('debug',1);
class DashboardController extends AppController {
	var $name = 'Dashboard';
	var $helpers = array('htmlExt');
	var $uses = array('Company');
    var $modul_load_view = false; //defaultne modul nenacita
	 
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#','Dashboard'=>'#'));

        $this->fakturace(date('Y-m').'-01');        
        $this->dochazky(date('Y-m').'-01');        
        $this->firmy(date('Y-m-d'));        
        $this->objednavky(date('Y-m-d'));        
        $this->oddeleni_naboru(date('Y-m').'-01');        
        $this->oddeleni_realizace(date('Y-m').'-01');        
        $this->oddeleni_obchodu(date('Y-m').'-01');       
        
        $this->int_zam(date('Y-m').'-01');    
	}


    /**
     * modul Fakturace
     */
    function fakturace($date = null){
        if($date == null)
            $date = date('Y-m').'-01';

        /**
         * navratove pole hodnot
         * parametry 
         *  nazev vypoctu (popisek)
         *  jejich hodnotu
         *  type - money (foramt na price z fastest helperu)
         * defaultni nastaveni 
         */
        $return = array(
            'celkem_at'=>array(
                'label'=>'Celkem AT',
                'value'=>0,
                'type'=>'money'
            ),
            'celkem_atc'=>array(
                'label'=>'z toho ATC',
                'value'=>0,
                'type'=>'money'
            ),
            'celkem_wdh'=>array(
                'label'=>'z toho WDH',
                'value'=>0,
                'type'=>'money'
            ),
            'celkem_atsk'=>array(
                'label'=>'z toho ATSK',
                'value'=>0,
                'type'=>'money'
            ),
            'celkem_top5'=>array(
                'label'=>'z toho TOP5',
                'value'=>0
            ),
            'pocet_faktur'=>array(
                'label'=>'Počet faktur',
                'value'=>0
            ),
            'neuhrazenych_faktur'=>array(
                'label'=>'z toho neuhrazenych',
                'value'=>0
            )
        );
        
        
        
        $this->loadModel('CompanyInvoiceItem');
        $this->CompanyInvoiceItem->bindModel(array(
            'belongsTo'=>array(
                'Company'
            )
        ));
        $invoice = $this->CompanyInvoiceItem->find('all',array(
            'conditions'=>array(
                'za_mesic' => $date,
                'cena_bez_dph >'=>0.00
            ),
            'fields'=>array(
                'Company.parent_id','cena_bez_dph',
                'COUNT(CompanyInvoiceItem.id) as pocet_faktur',
                'SUM(IF(datum_uhrady = "0000-00-00",1,0)) as neuhrazeno'
            ),
            'order'=>'cena_bez_dph DESC',
            'group'=>'CompanyInvoiceItem.id'
        ));
        
        $i = $top5 = 0;
        foreach($invoice as $item){
            /**
             * Celkem za ATC
             */
            if($item['Company']['parent_id'] == 1 ){
                $return['celkem_atc']['value'] += (double)$item['CompanyInvoiceItem']['cena_bez_dph'];
            } 
            
            /**
             * Celkem za WDH
             */
            if($item['Company']['parent_id'] == 2 ){
                $return['celkem_wdh']['value'] += (double)$item['CompanyInvoiceItem']['cena_bez_dph'];
            }
            
            /**
             * Celkem za WDH
             */
            if($item['Company']['parent_id'] == 3 ){
                $return['celkem_atsk']['value'] += (double)$item['CompanyInvoiceItem']['cena_bez_dph'];
            }
            
            /**
             * globalni soucty
             */
            $return['celkem_at']['value'] += (double)$item['CompanyInvoiceItem']['cena_bez_dph'];   // castka za cele AT 
            $return['pocet_faktur']['value'] += $item[0]['pocet_faktur'];    //pocet faktur
            $return['neuhrazenych_faktur']['value'] += $item[0]['neuhrazeno'];   // z toho neuhrazenych  
            
            if($i < 5)
                $top5 += $item['CompanyInvoiceItem']['cena_bez_dph'];
            
            $i++;
        }
        
        //nastaveni TOP5
        if($return['celkem_at']['value'] != 0)
            $return['celkem_top5']['value'] = round((($top5/$return['celkem_at']['value']) * 100),2).'%'; 
        else
            $return['celkem_top5']['value'] = '0%'; 
        
    
        
        //data
        $this->set('fakturace_data',$return);
        //datum
        $this->set('mesic',$date);
    
        /**
         * pokud je povoleno vykresli view
         */
        if($this->modul_load_view === true)
            $this->render('moduls/fakturace');
    }
    
    /**
     * modul dochazky
     * @param $date - datum k kteremu je pocitana dochazka
     * @param $coo_id - pouze za daneho coordinatora
     */
    function dochazky($date = null, $coo_id = null){
        if($date == null)
            $date = date('Y-m').'-01';

        $dates = explode('-',$date);
        $year = $dates[0];
        $month = $dates[1];

        /**
         * navratove pole hodnot
         * parametry 
         *  nazev vypoctu (popisek)
         *  jejich hodnotu
         *  type - money (foramt na price z fastest helperu)
         * defaultni nastaveni 
         */
        $return = array(
            'pocet_firem'=>array(
                'label'=>'Počet firem',
                'value'=>0
            ),
            'pocet_zam'=>array(
                'label'=>'Počet zam.',
                'value'=>0
            ),
            'pocet_hodin'=>array(
                'label'=>'Počet hodin',
                'value'=>0
            ),
            'celkem_castky_mezd'=>array(
                'label'=>'Částky mezd',
                'value'=>''
            ),
            'ps'=>array(
                'label'=>'1xxx',
                'value'=>0,
                'type'=>'money'
            ),
            'd'=>array(
                'label'=>'x1xx',
                'value'=>0,
                'type'=>'money'
            ),
            'f'=>array(
                'label'=>'xx1x',
                'value'=>0,
                'type'=>'money'
            ),
            'c'=>array(
                'label'=>'xxx1',
                'value'=>0,
                'type'=>'money'
            )
            
        );
        
        
        
        $this->loadModel('ClientWorkingHour');
        
        $conditions = array(
                'year' => $year,
                'month' => $month,
                'ClientWorkingHour.kos' => 0
        );
        
        
        if($coo_id != null){
            
            $this->ClientWorkingHour->bindModel(array(
                'belongsTo'=>array(
                    'Company'=>array(
                        'conditions'=>array(
                             $coo_id.' IN (Company.client_manager_id,Company.coordinator_id,Company.coordinator_id2)'
                        )
                    )
                )
            ));
            
            $conditions = am($conditions, array('Company.id <> ""'));
        }
        
        $details = $this->ClientWorkingHour->find('all',array(
            'conditions'=>$conditions,
            'fields'=>array(
                'COUNT(DISTINCT ClientWorkingHour.company_id) as pocet_firem',
                'COUNT(DISTINCT ClientWorkingHour.client_id) as pocet_zam',
                'ROUND(SUM(ClientWorkingHour.celkem_hodin),2) as pocet_hodin',
                'SUM(ClientWorkingHour.salary_part_1_p) as ps',
                'SUM(ClientWorkingHour.salary_part_2_p) as d',
                'SUM(ClientWorkingHour.salary_part_3_p) as f',
                'SUM(ClientWorkingHour.salary_part_4_p) as c',
  
            ),
            'group'=>'ClientWorkingHour.year,ClientWorkingHour.month'
        ));
        
        $details = array_shift($details);
        
        
        /**
         * nastaveni return values
         */
        $return['pocet_firem']['value'] = $details[0]['pocet_firem'];   
        $return['pocet_zam']['value'] = $details[0]['pocet_zam'];
        $return['pocet_hodin']['value'] = $details[0]['pocet_hodin'];
        $return['ps']['value'] = $details[0]['ps'];
        $return['d']['value'] = $details[0]['d'];
        $return['f']['value'] = $details[0]['f'];
        $return['c']['value'] = $details[0]['c'];
        
        $this->set('coo_list',$this->get_list('CmsUser',array('cms_group_id'=>4,'status'=>1)));

        //data
        $this->set('dochazky_data',$return);
        //datum
        $this->set('mesic',$date);
        //coo
        $this->set('coo_id',$coo_id);
        
        /**
         * pokud je povoleno vykresli view
         */
        if($this->modul_load_view === true)
            $this->render('moduls/dochazky');
    }
    
    
    
    /**
     * modul objednavky
     * @param $date - datum k kteremu jsou pocitany objednavky
     */
    function objednavky($date = null){
        if($date == null)
            $date = date('Y-m-d');

        /**
         * navratove pole hodnot
         * parametry 
         *  label - popisek
         *  value - pokud je array jedna se o vice smloupcovy value
         *  type - money (foramt na price z fastest helperu)
         * defaultni nastaveni 
         */
        $return = array(
            'pocet_obj'=>array(
                'label'=>'Počet objednávek',
                'value'=>0
            ),
            'obj_objednano'=>array(
                'label'=>'z toho objednáných',
                'value'=>0
            ),
            'obj_zavaznych'=>array(
                'label'=>'z toho závazných',
                'value'=>0
            ),
            'obj_predbeznych'=>array(
                'label'=>'z toho předběžných',
                'value'=>0
            ),
            'obj_zrusenych'=>array(
                'label'=>'z toho zrušených',
                'value'=>0
            )  
        );
        
        
        
        $this->loadModel('CompanyOrderItem');
        $details = $this->CompanyOrderItem->find('all',array(
            'conditions'=>array(
                'kos'=>0,
                'order_date <= '=>$date
            ),
            'fields'=>array(
                'SUM(IF(order_status = 1,1,0)) as obj_objednano',
                'SUM(IF(order_status = -1,1,0)) as obj_predbeznych',
                'SUM(IF(order_status = 2,1,0)) as obj_zavaznych',
                'SUM(IF(order_status = 3,1,0)) as obj_zrusenych',
                'COUNT(CompanyOrderItem.id) as pocet_obj'
            ),
            'group'=>'order_date <= '.$date
        ));
             
        $details = array_shift($details); 
        
        /**
         * nastaveni return values
         */       
        $return['pocet_obj']['value'] = $details[0]['pocet_obj'];
        $return['obj_objednano']['value'] = $details[0]['obj_objednano'];
        $return['obj_zavaznych']['value'] = $details[0]['obj_zavaznych'];
        $return['obj_predbeznych']['value'] = $details[0]['obj_predbeznych'];
        $return['obj_zrusenych']['value'] = $details[0]['obj_zrusenych'];

        //data 1
        $this->set('objednavky_data',$return);


        
        /**
         * navratove pole hodnot2
         */
        $return = array(); 
        $return = array(
            'celkem_obj'=>array(
                'label'=>'otevřených objednávek',
                'value'=>0
            ),
            'celkem_mist'=>array(
                'label'=>'požadovaných míst',
                'value'=>0
            ),
            'nominace_coo'=>array(
                'label'=>'nominace COO',
                'value'=>0
            ),
            'nominace_in'=>array(
                'label'=>'nominace IN',
                'value'=>0
            ),
            'nominace_en'=>array(
                'label'=>'nominace EN',
                'value'=>0
            ),
            'cl'=>array(
                'label'=>'čekací listina',
                'value'=>0
            ),
            'zam'=>array(
                'label'=>'zaměstnání',
                'value'=>0
            )    
        );
        
        
        $details = $this->CompanyOrderItem->find('all',array(
            'conditions'=>array(
                'kos'=>0,
                'order_status'=>1
            ),
            'fields'=>array(
                'COUNT(id) as celkem_obj',
                'SUM(count_of_free_position) as celkem_mist',
                'SUM(cl_count) as cl',
                'SUM(zam_count) as zam',
                '(( SELECT COUNT(cc.id) FROM wapis__connection_client_requirements as cc WHERE type=3 and cc.objednavka = 1 and cc.kos = 0 and cc.cms_user_id IN(SELECT id FROM wapis__cms_users Where kos=0 and cms_group_id = 4))) as nom_koo',
                '(( SELECT COUNT(cc.id) FROM wapis__connection_client_requirements as cc WHERE type=3 and cc.objednavka = 1 and cc.kos = 0 and cc.cms_user_id IN(SELECT id FROM wapis__cms_users Where kos=0 and cms_group_id = 9))) as nom_in',
                '(( SELECT COUNT(cc.id) FROM wapis__connection_client_requirements as cc LEFT JOIN wapis__clients as cl ON(cl.id = cc.client_id and cl.externi_nabor = 1) WHERE cl.id <> 0 and type=3 and cc.objednavka = 1 and cc.kos = 0 )) as nom_en',

            )
        ));
             
        $details = array_shift($details); 
        
        /** 
         * nastaveni return values
         */        
        $return['celkem_obj']['value'] = $details[0]['celkem_obj'];
        $return['celkem_mist']['value'] = $details[0]['celkem_mist'];
        $return['cl']['value'] = $details[0]['cl'];
        $return['zam']['value'] = $details[0]['zam'];
        $return['nominace_coo']['value'] = $details[0]['nom_koo'];
        $return['nominace_in']['value'] = $details[0]['nom_in'];
        $return['nominace_en']['value'] = $details[0]['nom_en'];


        //data 2
        $this->set('objednavky_data2',$return);

        /**
         * navratove pole hodnot3
         */
        $return = array(); 
        $return = array(
            'celkem'=>array(
                'label'=>'celkem',
                'value'=>0
            ),
            'cl'=>array(
                'label'=>'čekací listina celkem',
                'value'=>0
            ),
            'cl_in'=>array(
                'label'=>'čekací listina uživatelem IN',
                'value'=>0
            ),
            'cl_coo'=>array(
                'label'=>'čekací listina uživatelem KOO',
                'value'=>0
            ),
            'nominace_en'=>array(
                'label'=>'nominace uživatelem EN',
                'value'=>0
            ),
            'celkem_zam'=>array(
                'label'=>'celkem zaměstnanců',
                'value'=>0
            ),
            'zam_in'=>array(
                'label'=>'zaměstnanci uživatelem IN',
                'value'=>0
            ),
            'zam_koo'=>array(
                'label'=>'zaměstnanci uživatelem KOO',
                'value'=>0
            ),
            'zam_en'=>array(
                'label'=>'zaměstnanci uživatelem EN',
                'value'=>0
            )    
        );
        
        $this->loadModel('ConnectionClientRequirement');
        $this->ConnectionClientRequirement->bindModel(array(
            'belongsTo'=>array(
                'CompanyOrderItem'=>array(
                    'foreignKey'=>'requirements_for_recruitment_id',
                    'conditions'=>array(
                        'CompanyOrderItem.kos'=>0,
                        'DATE_FORMAT(order_date,"%Y-%m-%d") >= '=>'"2010-01-01"'
                    )
                ),
                'CmsUser'=>array(
                    'fields'=>array('cms_group_id')
                ),
                'Client'=>array(
                    'fields'=>array('externi_nabor')
                )
            )
        ));
        $details = $this->ConnectionClientRequirement->find('all',array(
            'conditions'=>array(
                'ConnectionClientRequirement.kos'=>0,
                'CompanyOrderItem.id <> '=>0,
                'type'=>2,
                'objednavka'=>1,
                'ConnectionClientRequirement.kos'=>0
            ),
            'fields'=>array(
                '(SELECT SUM(count_of_free_position) as celkem FROM wapis__company_order_items Where kos = 0 AND DATE_FORMAT(order_date,"%Y-%m-%d") >= "2010-01-01" ) as celkem',
                'COUNT(ConnectionClientRequirement.client_id) as celkem_zam',
                'SUM(IF(CmsUser.cms_group_id = 9,1,0)) as zam_in',
                'SUM(IF(CmsUser.cms_group_id = 4,1,0)) as zam_koo',
                'SUM(IF(Client.externi_nabor = 1,1,0)) as zam_en',
            ),
            //'group'=>'CompanyOrderItem.id'
        ));
        
        $IN_ids = array_keys($this->get_list('CmsUser',array('cms_group_id'=>9)));
        $KOO_ids = array_keys($this->get_list('CmsUser',array('cms_group_id'=>4)));
        
        $this->loadModel('HistoryItem');
        //cekaci listina od IN
        $history = $this->HistoryItem->find('all',array(
            'conditions'=>array(
                'HistoryItem.category_id' =>7,             
                'HistoryItem.action_id' =>31,             
                'HistoryItem.created >='=>'2010-01-01',
                'HistoryItem.cms_user_id'=>$IN_ids            
            ),
            'fields'=>array(
                'COUNT(DISTINCT client_id) as pocet_cl',
                'HistoryItem.*'
            ),
            'group'=>'cms_user_id'
        ));
        
        //cekaci listina od COO
        $history2 = $this->HistoryItem->find('all',array(
            'conditions'=>array(
                'HistoryItem.category_id' =>7,             
                'HistoryItem.action_id' =>31,             
                'HistoryItem.created >='=>'2010-01-01',
                'HistoryItem.cms_user_id'=>$KOO_ids            
            ),
            'fields'=>array(
                'COUNT(DISTINCT client_id) as pocet_cl',
                'HistoryItem.*'
            ),
            'group'=>'cms_user_id'
        ));
        
        $this->loadModel('EnNabor');
        $en_nabor = $this->EnNabor->find('list',array(
            'conditions'=>array('kos'=>0,'status'=>1),
            'fields'=>array('id','id')
        ));
        //nominacni listina od EN
        $history3 = $this->HistoryItem->find('all',array(
            'conditions'=>array(
                'HistoryItem.category_id' =>7,             
                'HistoryItem.action_id' =>29,             
                'HistoryItem.created >='=>'2010-01-01',
                'HistoryItem.cms_user_id'=>$en_nabor            
            ),
            'fields'=>array(
                'COUNT(DISTINCT client_id) as pocet_cl',
                'HistoryItem.*'
            ),
            'group'=>'cms_user_id'
        ));
        unset($this->HistoryItem);
       
        /**
         * rozdelime pole z history na tyto dve action
         * 29 - pridani na NL
         * 31 - pridani na CL
         */    
        $cl_by_in = Set::combine($history,'{n}.HistoryItem.cms_user_id','{n}.0.pocet_cl');       
        $cl_by_koo = Set::combine($history2,'{n}.HistoryItem.cms_user_id','{n}.0.pocet_cl');       
        $nom_by_en = Set::combine($history3,'{n}.HistoryItem.cms_user_id','{n}.0.pocet_cl');       

        $details = array_shift($details); 
        
        /** 
         * nastaveni return values
         */        
        $return['celkem']['value'] = $details[0]['celkem'];
        $return['celkem_zam']['value'] = $details[0]['celkem_zam'];
        $return['cl_in']['value'] = array_sum($cl_by_in);
        $return['cl_coo']['value'] = array_sum($cl_by_koo);
        $return['nominace_en']['value'] = array_sum($nom_by_en);
        $return['cl']['value'] = (array_sum($cl_by_koo)+array_sum($cl_by_in));
         
        //celkem zam
        $celkem_zam = $details[0]['celkem_zam'];
           
        if($celkem_zam != 0){
            $return['zam_in']['value'] = round((($details[0]['zam_in']/$celkem_zam) * 100),2).'%'; 
            $return['zam_koo']['value'] = round((($details[0]['zam_koo']/$celkem_zam) * 100),2).'%'; 
            $return['zam_en']['value'] = round((($details[0]['zam_en']/$celkem_zam) * 100),2).'%'; 
        }
        else {
            $return['zam_in']['value'] = '0%'; 
            $return['zam_koo']['value'] = '0%'; 
            $return['zam_en']['value'] = '0%'; 
        }    
  

        //data 3
        $this->set('objednavky_data3',$return);



        //datum
        $this->set('date',$date);
        
        /**
         * pokud je povoleno vykresli view
         */
        if($this->modul_load_view === true)
            $this->render('moduls/objednavky');
    }
    
    
    /**
     * modul firmy
     * @param $date - datum k kteremu je pocitana dochazka
     */
    function firmy($date = null){
        if($date == null)
            $date = date('Y-m-d');

        /**
         * navratove pole hodnot
         * parametry 
         *  label - popisek
         *  value - pokud je array jedna se o vice smloupcovy value
         *  type - money (foramt na price z fastest helperu)
         * defaultni nastaveni 
         */
        $return = array(
            'pocet_firem'=>array(
                'label'=>'Počet firem v DB',
                'value'=>array(0,0)
            ),
            'pocet_firem_zam'=>array(
                'label'=>'z toho co mají zaměstnance',
                'value'=>array(0,0)
            ),
            'pocet_zam'=>array(
                'label'=>'Počet zaměstnanců celkem',
                'value'=>array(0,0)
            ),
            'status'=>array(
                'label'=>'Smlouvy',
                'value'=>array('','')
            ) 
        );
        
        
        
        $this->loadModel('Company');
        $details = $this->Company->find('all',array(
            'conditions'=>array(
                'kos'=>0,
                'stat_id'=>array(1,2),
                'DATE_FORMAT(created,"%Y-%m-%d") <= '=>$date
            ),
            'fields'=>array(
                'stat_id',
                ('SUM(( SELECT COUNT(DISTINCT client_id) 
                    FROM wapis__connection_client_requirements
                    where type=2 and company_id=Company.id and(`from` <= "'. $date.'" AND (`to`>="'. $date.'" OR `to`="0000-00-00"))GROUP BY company_id
                   )) as pocet_zam'),
                'COUNT(Company.id) as pocet_firem',
                ('SUM(IF(( SELECT COUNT(DISTINCT client_id) 
                    FROM wapis__connection_client_requirements
                    where type=2 and company_id=Company.id and(`from` <= "'. $date.'" AND (`to`>="'. $date.'" OR `to`="0000-00-00"))GROUP BY company_id
                   ) > 0,1,0)) as pocet_firem_zam')
            ),
            'group'=>'stat_id'
        ));
        
        //find pro smlouvy
        $this->Company->bindModel(array('belongsTo'=>array('SettingStav'=>array('conditions'=>array('SettingStav.kos'=>0)))));
        $smlouvy = $this->Company->find('all',array(
            'conditions'=>array(
                'Company.kos'=>0,
                'stat_id'=>array(1,2),
                'SettingStav.id <> '=>0,
                'DATE_FORMAT(Company.created,"%Y-%m-%d") <= '=>$date
             ),
            'fields'=>array(
                'stat_id',
                'SettingStav.id',
                'SettingStav.name',
                 'COUNT(Company.id) as pocet',
             ),
            'group'=>'stat_id,setting_stav_id'
        ));      
          //pr($smlouvy);
        
        /**
         * nastaveni return values
         */       
        foreach($details as $id=>$item){
            $return['pocet_firem']['value'][$id] = $item[0]['pocet_firem'];
            $return['pocet_firem_zam']['value'][$id] = $item[0]['pocet_firem_zam'];
            $return['pocet_zam']['value'][$id] = $item[0]['pocet_zam'];
            
        }
        //pr($smlouvy);
        /**
         * nastaveni smluv
         */
        foreach($smlouvy as $id=>$item){
            $return[$item['SettingStav']['id']]['label'] = $item['SettingStav']['name'];
            
            //CR = 0 a SR = 1, protoze se vyzuiva key jako indexy pro view
            $stat = ($item['Company']['stat_id'] == 1 ? 0 : 1);
            
            $return[$item['SettingStav']['id']]['value'][$stat] = $item[0]['pocet'];  
            
            
            /**
             * nastav druhy stat na nulu pokud poprve nstavuji tuto smlouvu 
             * aby pro druhy stat nebyla prazdna value
             */
            if(!isset($return[$item['SettingStav']['id']]['value'][($stat == 1 ? 0 : 1)])){
                $return[$item['SettingStav']['id']]['value'][($stat == 1 ? 0 : 1)] = 0;
                
                //serad klice, aby bylo vzdy CR prvni, vkuli view
                ksort($return[$item['SettingStav']['id']]['value']);
            }           
        }
        //pr($return);

        //data
        $this->set('firmy_data',$return);
        //datum
        $this->set('date',$date);
        
        /**
         * pokud je povoleno vykresli view
         */
        if($this->modul_load_view === true)
            $this->render('moduls/firmy');
    }
    
    
    /**
     * modul interni zamestnanci
     * @param $date - datum filtrace
     */
    function int_zam($date = null){
        if($date == null)
            $date = date('Y-m').'-01';

        /**
         * navratove pole hodnot
         * parametry 
         *  label - popisek
         *  value - pokud je array jedna se o vice smloupcovy value
         *  type - money (foramt na price z fastest helperu)
         * defaultni nastaveni 
         */
        $return = array(
            'pocet_zam'=>array(
                'label'=>'Celkem AT',
                'value'=>0
            )
        );

        
        
        $this->loadModel('ConnectionClientAtCompanyWorkPosition');
        $this->ConnectionClientAtCompanyWorkPosition->bindModel(array(
            'belongsTo'=>array('AtCompany')
        ));
        $zam_company = $this->ConnectionClientAtCompanyWorkPosition->find('all',array(
            'conditions'=>array(
                'ConnectionClientAtCompanyWorkPosition.kos'=>0,
                '(datum_to = "0000-00-00" OR DATE_FORMAT(datum_to,"%Y-%m") >= "'.$date.'")',
                'DATE_FORMAT(datum_nastupu,"%Y-%m") <= '=>$date
            ),
            'fields'=>array(
                'AtCompany.id',
                'AtCompany.name',
                'COUNT(client_id) as pocet',
            ),
            'group'=>'at_company_id'
        ));
        
        //find pro smlouvy
        $this->ConnectionClientAtCompanyWorkPosition->bindModel(array(
            'belongsTo'=>array('AtProject')
        ));
        $zam_project = $this->ConnectionClientAtCompanyWorkPosition->find('all',array(
            'conditions'=>array(
                'ConnectionClientAtCompanyWorkPosition.kos'=>0,
                '(datum_to = "0000-00-00" OR DATE_FORMAT(datum_to,"%Y-%m") >= "'.$date.'")',
                'DATE_FORMAT(datum_nastupu,"%Y-%m") <= '=>$date,
                'ConnectionClientAtCompanyWorkPosition.at_project_id != '=>0
            ),
            'fields'=>array(
                'AtProject.id',
                'AtProject.name',
                'COUNT(client_id) as pocet',
            ),
            'group'=>'at_project_id'
        ));

        
        /**
         * nastaveni return
         */
        $celkem = 0; 
        //mezi radek    
        $return['mezi1'] = array('label'=>'Dle firem','value'=>''); 
        foreach($zam_company as $item){
            $return['company_'.$item['AtCompany']['id']]['label'] = $item['AtCompany']['name'];
            $return['company_'.$item['AtCompany']['id']]['value'] = $item[0]['pocet'];  
            
            $celkem += $item[0]['pocet'];          
        }
        //CELKEM ZA AT
        $return['pocet_zam']['value'] = $celkem;
        
            
        //mezi radek    
        $return['mezi2'] = array('label'=>'Dle projektu','value'=>''); 
        foreach($zam_project as $item){
            $return['project_'.$item['AtProject']['id']]['label'] = $item['AtProject']['name'];
            $return['project_'.$item['AtProject']['id']]['value'] = $item[0]['pocet'];      
        }
        
        //data
        $this->set('int_zam_data',$return);
   
        //datum
        $this->set('date',$date);
        
        /**
         * pokud je povoleno vykresli view
         */
        if($this->modul_load_view === true)
            $this->render('moduls/int_zam');
    }
    
    
    
    /**
     * modul oddeleni naboru
     * @param $date - datum
     */
    function oddeleni_naboru($date = null){
        if($date == null)
            $date = date('Y-m').'-01';

        /**
         * navratove pole hodnot
         * parametry 
         *  label - popisek
         *  value - pokud je array jedna se o vice smloupcovy value
         *  type - money (foramt na price z fastest helperu)
         * defaultni nastaveni 
         */
        $return = array();
               
        
        $this->loadModel('ConnectionClientRequirement');
        $this->ConnectionClientRequirement->query('SET NAMES "UTF8"');
        $this->ConnectionClientRequirement->bindModel(array(
            'belongsTo'=>array(
                'CmsUser'=>array(
                    'conditions'=>array('cms_group_id'=>9,'CmsUser.kos'=>0,'CmsUser.status'=>1)
                )
            )
        ));
        $details = $this->ConnectionClientRequirement->find('all',array(
            'conditions'=>array(
                'ConnectionClientRequirement.kos'=>0,
                'CmsUser.id <> '=>0,
                'DATE_FORMAT(ConnectionClientRequirement.from,"%Y-%m-01") = '=>$date
            ),
            'fields'=>array(
                'CmsUser.id',
                'CmsUser.name',
                'COUNT(client_id) as pocet_zam',
            ),
            'group'=>'cms_user_id'
        ));
        
        

        $this->loadModel('HistoryItem');
        $history = $this->HistoryItem->find('all',array(
            'conditions'=>array(
                'HistoryItem.category_id' =>7,             
                'HistoryItem.action_id' =>array(29,31),             
                'DATE_FORMAT(HistoryItem.created,"%Y-%m-01")'=>$date,
                'HistoryItem.cms_user_id'=>Set::extract($details,'/CmsUser/id')             
            ),
            'fields'=>array(
                'COUNT(DISTINCT client_id) as pocet_cl',
                'HistoryItem.*'
            ),
            'group'=>'cms_user_id,action_id'
        ));
        unset($this->HistoryItem);
            
        /**
         * rozdelime pole z history na tyto dve action
         * 29 - pridani na NL
         * 31 - pridani na CL
         */    
        $history_nom_cl_list = Set::combine($history,'{n}.HistoryItem.cms_user_id','{n}.0.pocet_cl','{n}.HistoryItem.action_id');
        
        /**
         * nastaveni return values
         */       
        foreach($details as $item){
            $return[$item['CmsUser']['id']]['label'] = $item['CmsUser']['name'];
            $return[$item['CmsUser']['id']]['value'][0] = $history_nom_cl_list[29][$item['CmsUser']['id']];
            $return[$item['CmsUser']['id']]['value'][1] = $history_nom_cl_list[31][$item['CmsUser']['id']];
            $return[$item['CmsUser']['id']]['value'][2] = $item[0]['pocet_zam']; 
        }

        $this->set('pocet_in',count($details));


        //data
        $this->set('oddeleni_naboru_data',$return);
        //datum
        $this->set('mesic',$date);
        
        /**
         * pokud je povoleno vykresli view
         */
        if($this->modul_load_view === true)
            $this->render('moduls/oddeleni_naboru');
    }
    
    
    /**
     * modul oddeleni realizace
     * @param $date - datum
     */
    function oddeleni_realizace($date = null){
        if($date == null)
            $date = date('Y-m').'-01';

        /**
         * navratove pole hodnot
         * parametry 
         *  label - popisek
         *  value - pokud je array jedna se o vice smloupcovy value
         *  type - money (foramt na price z fastest helperu)
         * defaultni nastaveni 
         */
        $return = array();
               
        
        $this->loadModel('CmsUser');
        $this->CmsUser->query('SET NAMES "UTF8"');
        $this->CmsUser->bindModel(array(
            'joinSpec'=>array(
                'CompanyActivity'=>array(
                    'className'=>'CompanyActivity',
                    'foreignKey'=>'CompanyActivity.cms_user_id',
                    'primaryKey'=>'CmsUser.id',
                    'conditions'=>array(
                        'DATE_FORMAT(CompanyActivity.created,"%Y-%m-01") <= '=>$date,
                        'CompanyActivity.kos'=>0
                    )
                )
            )
        ));
        
        $details = $this->CmsUser->find('all',array(
            'conditions'=>array(
                'CmsUser.cms_group_id'=>4,
                'CmsUser.kos'=>0,
                'CmsUser.status'=>1
            ),
            'fields'=>array(
                'CmsUser.id',
                'CmsUser.name',
                'COUNT(CompanyActivity.id) as pocet_aktivit',
                '(SELECT COUNT(id) FROM wapis__companies WHERE CmsUser.id IN (client_manager_id,coordinator_id,coordinator_id2) AND kos = 0) as pocet_firem',
            ),
            'group'=>'CmsUser.id'
        ));
         
        
        /**
         * nastaveni return values
         */       
        foreach($details as $item){
            $return[$item['CmsUser']['id']]['label'] = $item['CmsUser']['name'];
            $return[$item['CmsUser']['id']]['value'][0] = $item[0]['pocet_firem'];
            $return[$item['CmsUser']['id']]['value'][1] = $item[0]['pocet_aktivit'];
            
        }
        

        $this->set('pocet_koo',count($details));


        //data
        $this->set('oddeleni_realizace_data',$return);
        //datum
        $this->set('mesic',$date);
        
        /**
         * pokud je povoleno vykresli view
         */
        if($this->modul_load_view === true)
            $this->render('moduls/oddeleni_realizace');
    }
    
    
    /**
     * modul oddeleni obchodu
     * @param $date - datum
     */
    function oddeleni_obchodu($date = null){
        if($date == null)
            $date = date('Y-m').'-01';

        /**
         * navratove pole hodnot
         * parametry 
         *  label - popisek
         *  value - pokud je array jedna se o vice smloupcovy value
         *  type - money (foramt na price z fastest helperu)
         * defaultni nastaveni 
         */
        $return = array();
               
        
        $this->loadModel('CmsUser');
        $this->CmsUser->query('SET NAMES "UTF8"');
        $this->CmsUser->bindModel(array(
            'joinSpec'=>array(
                'CompanyActivity'=>array(
                    'className'=>'CompanyActivity',
                    'foreignKey'=>'CompanyActivity.cms_user_id',
                    'primaryKey'=>'CmsUser.id',
                    'conditions'=>array(
                        'DATE_FORMAT(CompanyActivity.created,"%Y-%m-01") <= '=>$date,
                        'CompanyActivity.kos'=>0
                    )
                ),
                'Company'=>array(
                    'className'=>'Company',
                    'foreignKey'=>'Company.self_manager_id',
                    'primaryKey'=>'CmsUser.id',
                    'conditions'=>array(
                        'Company.kos'=>0
                    )
                )
            )
        ));
        $details = $this->CmsUser->find('all',array(
            'conditions'=>array(
                'CmsUser.cms_group_id'=>2,
                'CmsUser.kos'=>0,
                'CmsUser.status'=>1
            ),
            'fields'=>array(
                'CmsUser.id',
                'CmsUser.name',
                'COUNT(DISTINCT CompanyActivity.id) as pocet_aktivit',
                'COUNT(DISTINCT Company.id) as pocet_firem',
            ),
            'group'=>'CmsUser.id'
        ));
         
        
        /**
         * nastaveni return values
         */       
        foreach($details as $item){
            $return[$item['CmsUser']['id']]['label'] = $item['CmsUser']['name'];
            $return[$item['CmsUser']['id']]['value'][0] = $item[0]['pocet_firem'];
            $return[$item['CmsUser']['id']]['value'][1] = $item[0]['pocet_aktivit'];
            
        }
        

        $this->set('pocet_sm',count($details));


        //data
        $this->set('oddeleni_obchodu_data',$return);
        //datum
        $this->set('mesic',$date);
        
        /**
         * pokud je povoleno vykresli view
         */
        if($this->modul_load_view === true)
            $this->render('moduls/oddeleni_obchodu');
    }
    
    
    
    /**
     * ajax load Modul
     * funkce ktera provede vypocet a vrati view ktere se obnovi
     * @param $name - jmeno funkce ktera se ma nacist
     * @param $date - datum pro ktere se ma nacist
     * @param $coo_id - filtrace na coordinatora, pouze u dochazek
     */
    function ajax_load_module($name = null, $date = null, $coo_id = null){
        if($date == null)
            $date = date('Y-m').'-01';
            
        /**
         * pri vyvolani funkce se vyvola jeho view, tzn vracime html
         */    
        $this->modul_load_view = true;
 
        if(method_exists($this,$name)){
            if($coo_id != null && $coo_id > 0)
                $this->$name($date, $coo_id);
            else    
                $this->$name($date);
        }    
        else
           die(json_encode(array('return'=>false)));    
    }
    
    
	
}
?>