<?php
//Configure::write('debug',1);

$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');
define('CURRENT_YEAR', $_GET['current_year']); 
define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);

class ReportFoodTicketsController  extends AppController {
	var $name = 'ReportFoodTickets';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('ClientWorkingHour');
	var $mesic = CURRENT_MONTH;
	var $rok = CURRENT_YEAR;
	var $renderSetting = array(
		'bindModel'	=> array(
			'belongsTo'	=>	array('Company')
		),
		'controller'	=>	'report_food_tickets',
		'SQLfields' 	=>	'*,count(client_id) as pocet_stravovacich,
                            SUM(food_days_count) as pocet_stravenek,
                            ',
		'SQLcondition'	=>  array(
			'ClientWorkingHour.kos'=>0,
            'ClientWorkingHour.year'=> CURRENT_YEAR,
			'ClientWorkingHour.month'=> CURRENT_MONTH,
			'ClientWorkingHour.food_days_count >'=> 0
		),
        'count_col'=>'ClientWorkingHour.company_id',
		'page_caption'=>'Report stravných lístků',
		'sortBy'=>'Company.name.ASC',
		'group_by'=>'ClientWorkingHour.company_id',
		'top_action' => array(),
		'filtration' => array(
			'ClientWorkingHour-company_id'	        =>	'select|Společnost|company_list',
		    'GET-current_month'						=>	'select|Měsíc|mesice_list',   
            'Company-client_manager_id|coordinator_id|coordinator_id2-cmkoo'		=>	'select|CM/KOO|cm_koo_list',
            'GET-current_year'						=>	'select|Rok|actual_years_list'
        ),
		'items' => array(
			'id'				=>	'ID|ClientWorkingHour|id|hidden|',
			'comapany'		=>	'Firma|Company|name|text|',
			'year'			    =>	'Rok|ClientWorkingHour|year|text|',
			'month'			    =>	'Měsíc|ClientWorkingHour|month|var|mesice_list',
            'pocet_stravovacich'	=>	'Počet stravovacích|0|pocet_stravovacich|text|',
			'pocet_stravenek'    	=>	'Počet stravenek|0|pocet_stravenek|text|',
		),
		'posibility' => array(
            'show'		=>	'show|Detail položky|show',
            'print_detail'		=>	'print_detail|Tisk položky|print_detail',
		),
        'posibility_link' => array(
            'show'		=>	'ClientWorkingHour.company_id/ClientWorkingHour.year/ClientWorkingHour.month',
            'print_detail'		=>	'ClientWorkingHour.company_id/ClientWorkingHour.year/ClientWorkingHour.month',
		),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'	=> true,
			'languages'		=> true,
		)
	);

	

	function beforeRender(){
		parent::beforeRender();

		// úprava nadpisu reportu a přidání do něj datumu
        if(isset($this->viewVars['renderSetting']))
		   $this->set('spec_h1', $this->viewVars['renderSetting']['page_caption'].' za měsíc '.$this->mesice_list[ltrim($this->mesic,'0')].' a rok '.$this->rok);
	}
	
	function index(){
		// set FastLinks
		$this->set('fastlinks',array('ATEP'=>'/','Ubytování'=>'#',$this->viewVars['renderSetting']['page_caption']=>'#'));
		
		$this->loadModel('CmsUser');
		$this->set('cm_koo_list',$cm_koo_list = $this->CmsUser->find('list',array(
			'conditions'=>array(
				'kos'=>0,
				'status'=>1,
				'cms_group_id IN(3,4)'
			)
		)));
        
	
      
  		$this->loadModel('Company');
		$company_list = $this->Company->find('list',array(
			 'conditions'=> array('kos'=>0,'status'=>1),
			 'order'=>'name ASC'
		 ));
		 $this->set('company_list',$company_list);
		 
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
		
	}
	
    /**
     * prehled stravnych
     */
    function show($company_id,$year,$month){
		if ($company_id != null){
			
			$this->ClientWorkingHour->bindModel(array(
			'belongsTo'=>array('Client')
		    ));
	
			$this->set('zamestnanci_list',$this->ClientWorkingHour->find('all',array(
				'conditions'=>array(
					'ClientWorkingHour.company_id'=>$company_id,
					'ClientWorkingHour.year'=>$year,
					'ClientWorkingHour.month'=>$month
				),
				'fields'=>array(
					'Client.name','ClientWorkingHour.id','food_days_count'
				),
				'order'=>'Client.name ASC'
			)));

			unset($this->ClientWorkingHour);

			//render
			$this->render('show');	
	    }
		else 
			die('Bez id ? :-)');
		
		
	}
    
    /**
     * tisk prehledu zamestancu
     */
    function print_detail($company_id,$year,$month){
        $this->layout = 'print';
        
        if ($company_id != null){
            $this->loadModel('Company');
			$this->set('detail',$this->Company->read(array('name','mesto'),$company_id));
			$this->set('year',$year);
			$this->set('month',$month);
            unset($this->Company);
            
            
			$this->ClientWorkingHour->bindModel(array(
			'belongsTo'=>array('Client','ConnectionClientRequirement')
		    ));
	
			$this->set('zamestnanci_list',$zl = $this->ClientWorkingHour->find('all',array(
				'conditions'=>array(
					'ClientWorkingHour.company_id'=>$company_id,
					'ClientWorkingHour.year'=>$year,
					'ClientWorkingHour.month'=>$month,
                    'ClientWorkingHour.food_days_count >'=> 0
				),
				'fields'=>array(
					'Client.name',
					'ClientWorkingHour.id',
					'ClientWorkingHour.year','ClientWorkingHour.month',
                    'food_days_count',
                    'ConnectionClientRequirement.from','ConnectionClientRequirement.to'
				),
				'order'=>'Client.name ASC'
			)));
            $ps = Set::extract('/ClientWorkingHour/food_days_count',$zl);
            $this->set('pocet_stravenek',array_sum($ps));

			unset($this->ClientWorkingHour);

			//render
			$this->render('print');	
	    }
		else 
			die('Bez id ? :-)');
		
		
	}    	
    
    /**
     * detail jednotlivych dni kdy se stravoval
     */
    function detail($id){
        $data = $this->ClientWorkingHour->read(array('year','month','food_days','company_id'),$id);
        $this->set('data',$data);
        
         // load free days for state, month, year
		$this->loadModel('SettingStatSvatek');
		$this->set('svatky',$this->SettingStatSvatek->find('list', array(
            'conditions'=>array( 
                'setting_stat_id'	=> av(av($data['ClientWorkingHour']['company_id'],'Company'),'stat_id'), 
                'mesic'=> $data['ClientWorkingHour']['month'], 
                'rok'=> $data['ClientWorkingHour']['year'] 
            ),
            'fields'=>array('id','den'),
            'order'=>'den ASC'
        ))); 				
		
        //render
        $this->render('detail');
    }
}    
?>