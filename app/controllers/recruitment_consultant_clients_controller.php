<?php
//Configure::write('debug',1);

$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');
@define('CURRENT_YEAR', $_GET['current_year']); 
@define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);

define('fields','CmsUser.id,CmsUser.name,   
    (SELECT COUNT(DISTINCT client_id) FROM `wapis__connection_client_recruiters` as ConnectionClientRecruiter
        WHERE
         ConnectionClientRecruiter.cms_user_id = CmsUser.id AND
         ((DATE_FORMAT(ConnectionClientRecruiter.created,"%Y-%m") = "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'"))
    ) as pocet_uchazecu,
    ('.CURRENT_YEAR.') as year,
    ('.CURRENT_MONTH.') as month,
    CmsUser.cms_group_id
');  // RIGHT JOIN wapis__clients AS Client ON ( Client.id = ConnectionClientRecruiter.client_id AND Client.kos = 0)
class RecruitmentConsultantClientsController extends AppController {
	var $name = 'RecruitmentConsultantClients';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('CmsUser');
	var $renderSetting = array(
		'bindModel' => array('belongsTo'=>array('CmsGroup')),
		'SQLfields' => array(fields),
		'SQLcondition' => array(
			'CmsGroup.cms_group_superior_id'=>array(7,2),
            //'CmsUser.status'=>1,
			'CmsUser.kos'=>0,
			"(CmsUser.status = 1 OR (CmsUser.status = 0 AND DATE_FORMAT(CmsUser.updated,'%Y-%m') >= '#CURRENT_YEAR#-#CURRENT_MONTH#'))",
		),
		//'no_trash'=>1,
		'controller'=> 'recruitment_consultant_clients',
		'page_caption'=>'Nábor - počet vložených uchazečů',
		'sortBy'=>'CmsUser.name.ASC',
		//'group'=>'CmsUser.id',
		'top_action' => array(
			'export_excel' => 'Export Excel|export_excel|Export Excel|export_excel',
		),
		'filtration' => array(
			'GET-current_year'							=>	'select|Rok|actual_years_list',
 			'GET-current_month'							=>	'select|Měsíc|mesice_list',
		),
		'items' => array(
			'id'		=>	'ID|CmsUser|id|text|',
			'name'	    	=>	'Náborář|CmsUser|name|text|',
			'pocet_uchazecu'=>	'Počet uchazečů|0|pocet_uchazecu|text|', 
			'rok'		=>	'Rok|0|year|text|',
			'za_mesic'	=>	'Měsíc|0|month|viewVars|mesice_list',
			'test'		=>	'test|CmsUser|cms_group_id|text|',
		),
		'posibility' => array(
			'edit'	=> 'edit|Přehled klientů|edit',
		),
        'posibility_link' => array(
            'edit' => '/CmsUser.id/#CURRENT_YEAR#/#CURRENT_MONTH#',
        ),
	);

    function beforeFilter(){
        parent::beforeFilter();
        if (isset($_GET['excel'])){
            $this->renderSetting['no_limit'] = true;
        }
    }

	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Klienti - interní nábor'=>'#'));

		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
    function edit($id = null, $year = '', $month = ''){
        $this->autoLayout = false;
        $this->set('year', $year);
        $this->set('month', $month);


        $this->loadModel('CmsUser');
        $recruiter = $this->CmsUser->read(null, $id);
        $this->set('recruiter', $recruiter);


        $conditions = array(
            'ConnectionClientRecruiter.cms_user_id'=>$id,
			'DATE_FORMAT(ConnectionClientRecruiter.created,"%Y-%m") = "'.$year.'-'.$month.'"',
            'ConnectionClientRecruiter.cms_user_id != -1',
            'Client.kos'=>0
        );
        $this->loadModel('ConnectionClientRecruiter');

        $this->ConnectionClientRecruiter->bindModel( array(
            'joinSpec' => array(
                'Client' => array(
                    'primaryKey' => 'Client.id',
                    'foreignKey' => 'ConnectionClientRecruiter.client_id',
                ),
                'Recruiter' => array(
                    'className' => 'CmsUser',
                    'primaryKey' => 'Recruiter.id',
                    'foreignKey' => 'ConnectionClientRecruiter.cms_user_id'
                ),
                'ClientRequirements' => array(
                    'className' => 'ConnectionClientRequirement',
                    'primaryKey' => 'ClientRequirements.client_id',
                    'foreignKey' => 'ConnectionClientRecruiter.client_id',
                ),
                'Company' => array(
                    'primaryKey' => 'Company.id',
                    'foreignKey' => 'ClientRequirements.company_id',
                ),
                'CompanyWorkPosition' => array(
                    'primaryKey' => 'CompanyWorkPosition.id',
                    'foreignKey' => 'ClientRequirements.company_work_position_id',
                ),
            ),
        ));
        $clients  = $this->ConnectionClientRecruiter->find('all', array(
            'fields'=>array(
                            'ConnectionClientRecruiter.client_id',
                            //'ConnectionClientRecruiter.cms_user_id',
                           // 'Recruiter.name',
                            'Client.name',
                           // 'ClientRequirements.*',
                            'IF(GROUP_CONCAT(Company.name SEPARATOR "<br/>") != "", GROUP_CONCAT(Company.name SEPARATOR "<br/>"), "-") as company',
                            'IF(GROUP_CONCAT(CompanyWorkPosition.name SEPARATOR "<br/>") != "", GROUP_CONCAT(CompanyWorkPosition.name SEPARATOR "<br/>"), "-") as position'
            ),
            'conditions'=>$conditions,
            'group'=>'ConnectionClientRecruiter.client_id',
            'order'=> 'ConnectionClientRecruiter.created ASC'//'Client.name ASC'
        ));

        $this->loadModel('ConnectionClientRecruiter');
        $this->ConnectionClientRecruiter->bindModel( array(
            'joinSpec' => array(
                'Client' => array(
                    'primaryKey' => 'Client.id',
                    'foreignKey' => 'ConnectionClientRecruiter.client_id'
                ),
                'ClientRequirements' => array(
                    'className' => 'ConnectionClientRequirement',
                    'primaryKey' => 'ClientRequirements.client_id',
                    'foreignKey' => 'ConnectionClientRecruiter.client_id',

                )
            ),
        ));
        $counts = $this->ConnectionClientRecruiter->find('first',array('conditions'=>$conditions,
            'fields'=>array('Count(DISTINCT ConnectionClientRecruiter.client_id) as celkem','Count(ClientRequirements.company_id) as zam')));
        $this->set('counts', $counts);
        //pr($clients);
        $this->set('clients', $clients);
    }

    function export_excel(){
        $start = microtime ();
        $start = explode ( " " , $start );
        $start = $start [ 1 ]+ $start [ 0 ];

                $fields_sql = array(
                    fields
                );


                $criteria = $this->ViewIndex->filtration();
                $render_condition = $this->ViewIndex->foreach_SQLcondition($this->renderSetting['SQLcondition']);

                header("Pragma: public"); // požadováno
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Cache-Control: private",false); // požadováno u některých prohlížečů
                header("Content-Transfer-Encoding: binary");
                Header('Content-Type: application/octet-stream');
                Header('Content-Disposition: attachment; filename="'.date('Ymd_His').'.csv"');


                $limit = 100;
                $page = 1;
                $this->loadModel('CmsUser');
                $this->CmsUser->bindModel($this->renderSetting['bindModel'],false);
                $count = $this->CmsUser->find('first',
                    array(
                        'fields' =>  array("COUNT(DISTINCT CmsUser.id) as count"),
                        'conditions'=>am($criteria,$render_condition),
                        'recursive'	=>1
                    )
                );
                $count = $count[0]['count'];
                // hlavicka
                foreach($this->renderSetting['items'] as &$item_setting){
                    list($caption, $model, $col, $type, $fnc) = explode('|',$item_setting);
                    $item_setting = compact(array("caption", "model","col","type","fnc"));
                    if($type != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',$caption).'";';
                }
                echo "\n";
                unset($item_setting, $caption, $model, $col, $type, $fnc);

                $str_array=array("<br/>"=>', ',':'=>',',';'=>',','?'=>'', '#'=>' ');

                for ($exported = 0; $exported < $count; $exported += $limit){
                    $this->CmsUser->bindModel($this->renderSetting['bindModel'],false);
                    foreach($this->CmsUser->find('all',array(
                        'fields'=>$fields_sql,
                        'conditions'=>am($criteria,$render_condition),
                        'limit'=>$limit,
                        'page'=>$page,
                        'recursive'=>1,
                        'order'=>'CmsUser.name ASC'
                    )) as $item){
                        foreach($this->renderSetting['items'] as $key => $td){
                            if($td['type'] != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',strtr($this->ViewIndex->generate_td($item,$td,$this->viewVars),$str_array)).'";';
                        }
                        echo "\n";
                    }
                    $page++;
                }

                //time
                $end = microtime ();
                $end = explode ( " " , $end );
                $end = $end [ 1 ]+ $end [ 0 ];
                //echo 'Generate in '.($end - $start).'s';
                echo "\n";
                die();
            }


}
?>