<?php
Configure::write('debug',1);
    $_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
	$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');
	define('CURRENT_YEAR', $_GET['current_year']);
	define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);

	define('fields','CompanyView.*,
			( 
			  SELECT 
			   COUNT(client_id)
			  FROM wapis__connection_client_requirements
			  where type=2 and company_id=CompanyView.id and(
				`from` <= "'. date('Y-m-d').'" AND (`to`>="'. date('Y-m-d').'" OR `to`="0000-00-00")
			  ) 
			) as poc_zamestnancu,
            (IFNULL((SELECT hodnota_vh FROM wapis__company_invoice_items WHERE company_id = CompanyView.id and kos = 0 and za_mesic = "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'-01"),"Neuvedeno")) as company_hv
		');
class ReportManagerRealizaceController extends AppController {
	var $name = 'ReportManagerRealizace';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('CompanyView');
	var $renderSetting = array(
		//'SQLfields' => array('id','Company.name','updated','created','status','SalesManager.name','ClientManager.name','Coordinator.name','mesto','SettingStav.namre'),
		'SQLfields' => array(fields),
		// 'bindModel' => array('belongsTo' => array(
				// 'SalesManager' => array('className' => 'CmsUser', 'foreignKey' => 'self_manager_id'),
				// 'ClientManager' => array('className' => 'CmsUser', 'foreignKey' => 'client_manager_id'),
				// 'Coordinator' => array('className' => 'CmsUser', 'foreignKey' => 'coordinator_id'),
				// 'SettingParentCompany' => array('className' => 'SettingParentCompany', 'foreignKey' => 'parent_id'),
		// )),
		'controller'=> 'report_manager_realizace',
		'page_caption'=>'Report - manažeři realizace',
		'sortBy'=>'CompanyView.name.ASC',
		'top_action' => array(
			// caption|url|description|permission
			//'add_item'		=>	'Přidat|edit|Pridat popis|add'
		),
		'filtration' => array(	 	
            'CompanyView-manazer_realizace_id'	=>	'select|Manažer real.|manager_realizace_list',
            'GET-current_year'					=>	'select|Rok|actual_years_list',
			'GET-current_month'					=>	'select|Měsíc|mesice_list',		 	
		),
		'items' => array(
			'id'					=>	'ID|CompanyView|id|text|',
			'name'					=>	'Podniky(zakazky)|CompanyView|name|text|',
			'poc_zamestnancu'		=>	'Počet zaměstanců|0|poc_zamestnancu|text|',
            'company_hv'		    =>	'Hodnota podniku HV|0|company_hv|text|',
            'cm_id'		            =>	'Koordinátor|CompanyView|client_manager|text|',
			'coordinator'		    =>	'Koordinátor2|CompanyView|coordinator|text|',
			'coordinator2'		    =>	'Koordinátor3|CompanyView|coordinator2|text|'
		),
		'posibility' => array(
			//'status'	=> 	'status|Změna stavu|status',
		),
        'sum_variables_on_top'=>array(
            'path'=>'../report_manager_realizace/filtration_top'
        )
	);
	
	/**
 	* Returns a view, if not AJAX, load data for basic view. 
 	*
	* @param none
 	* @return view
 	* @access public
	**/
	function index(){
		if ($this->RequestHandler->isAjax()){
		    
		    if(isset($this->params['url']['filtration_CompanyView-manazer_realizace_id'])){
               $mr_id =$this->params['url']['filtration_CompanyView-manazer_realizace_id'];
               if($mr_id != ''){
            		$rr = $this->CompanyView->find('all',array(
                        'conditions'=>$this->ViewIndex->criteria,
                        'fields'=>array('CompanyView.*,
                        			( 
                        			  SELECT 
                        			   COUNT(client_id)
                        			  FROM wapis__connection_client_requirements
                        			  where type=2 and company_id=CompanyView.id and(
                        				`from` <= "'. date('Y-m-d').'" AND (`to`>="'. date('Y-m-d').'" OR `to`="0000-00-00")
                        			  ) 
                        			) as poc_zamestnancu,
                                    (IFNULL((SELECT hodnota_vh FROM wapis__company_invoice_items WHERE company_id = CompanyView.id and kos = 0 and za_mesic = "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'-01"),"Neuvedeno")) as company_hv
                        '),
                        //'group'=>'CompanyView.manazer_realizace_id'
                    ));
                    $this->set('filtration_sum_variables2',$rr);   
               }
            }
          
			$this->render('../system/items');
		} else {
			// set FastLinks
			$this->set('fastlinks',array('ATEP'=>'/','Firmy'=>'#','Seznam firem'=>'#'));
					
			// load list for filter SM, KO, CM
			$this->loadModel('CmsUser');
			$this->set('manager_realizace_list', $this->CmsUser->find('list', array('conditions'=>array('cms_group_id'=>18),'kos'=>0)));            
            
			$this->render('../system/index');
		}
	}
	
}    
?>