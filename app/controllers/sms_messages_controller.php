<?php
//Configure::write('debug',2);
/**
 * 
 * @author Zbyněk Strnad - Fastest Solution s.r.o.
 * @created 16.9.2009
 */

class SmsMessagesController extends AppController {
	var $name = 'SmsMessages';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','SmsClient');
	var $uses = array('SmsMessage');
	var $renderSetting = array(
		'bindModel' => array(
			'belongsTo' => array(
				'Sms'=>array(
					'foreignKey'=>'group_id'
				)
			)
		),
		'SQLfields' => array('*,(SELECT name FROM wapis__cms_users as CU Where Sms.cms_user_id=CU.id) as odeslal,
								MONTH(SmsMessage.created) as mesic, 
								YEAR(SmsMessage.created) as rok'
		),
		'controller'=> 'sms_messages',
		'page_caption'=>'SMS - Zprávy',
		'sortBy'=>'SmsMessage.created.DESC',
		'top_action' => array(
			// caption|url|description|permission
			//'add_item'		=>	'Přidat|edit|Přidat novou SMS kampaň|edit',
		),
		'filtration' => array(
			'Sms-cms_user_id'		=>	'select|Odesílatel|odesilatel_list',
			'SmsMessage-status_sms'		=>	'select|Status|status_sms_list',
			'SmsMessage-group_id'		=>	'select|Kampaň|kampane_list',
			'SmsMessage-created|year'							=>	'select|Rok|actual_years_list',
			'SmsMessage-created|month'							=>	'select|Měsíc|mesice_list',
		),
		'items' => array(
			'id'			=>	'ID|SmsMessage|id|text|',
			'vytvoril'		=>	'Odeslal|0|odeslal|text|',
			'kampan'		=>	'Kampaň|Sms|name|text|',
			'name'			=>	'Komu|SmsMessage|client_name|text|',
			'text'			=>	'Text|SmsMessage|text|text|',
			'mesic'		=>	'Mesic|0|mesic|text|',
			'rok'		=>	'Rok|0|rok|text|',
			'created'		=>	'Vytvořeno|Sms|created|datetime|'
		),
		'posibility' => array(
			//'edit'		=>	'edit|Editace SMS kampaně|edit'		
		)
	);
	
	function beforeFilter(){
		parent::beforeFilter();

		// defaultne zobrazovat pouze smsky se statusem 200 - odeslány
		if (!isset($_GET['filtration_SmsMessage-status_sms']))
			$_GET['filtration_SmsMessage-status_sms'] = $this->params['url']['filtration_SmsMessage-status_sms'] = 200;
		
	}

	/**
	 * 
	 * @return view
	 */
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','SMS - Zprávy'=>'#'));
	

		$this->loadModel('CmsUser');
		$this->set('odesilatel_list',$this->CmsUser->find('list',array('order'=>'name ASC','conditions'=>array('kos'=>0,'status'=>1))));
		
		$this->loadModel('Sms');
		$this->set('kampane_list',$this->Sms->find('list',array('order'=>'name ASC','conditions'=>array('kos'=>0,'status'=>1))));
		

		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	
	
}
?>