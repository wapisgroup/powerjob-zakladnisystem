<?php
Configure::write('debug',1);
class RouteListsController extends AppController {
	var $name = 'RouteLists';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload','Email');
	var $uses = array('RouteList');
	var $renderSetting = array(
        //'bindModel'=>array('belongsTo'=>array('CmsUser','AtProject','AtProjectCentre','AtCompany')),
		'SQLfields' => '*',
		'controller'=> 'route_lists',
		'page_caption'=>'Seznam tras',
		'sortBy'=>'RouteList.name.ASC',
		'top_action' => array(
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
		),
		'filtration' => array(
			'RouteList-name|'				=>	'text|Název|',
		),
		'items' => array(
			'id'			=>	'ID|RouteList|id|text|',
			'name'			=>	'Název|RouteList|name|text|',
			'updated'		=>	'Změněno|RouteList|updated|datetime|',
			'created'		=>	'Vytvořeno|RouteList|created|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
			'trash'	=>	'trash|Odstranit položku|trash'			
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Klienti'=>'#'));
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null){
				$this->data = $this->RouteList->read(null,$id);
   			}

			$this->render('edit');
		} else {
			$this->RouteList->save($this->data);
			die();
		}
	}

}
?>