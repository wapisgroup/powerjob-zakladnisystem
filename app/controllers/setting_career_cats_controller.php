<?php
class SettingCareerCatsController extends AppController {
	var $name = 'SettingCareerCats';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('SettingCareerCat');
	var $renderSetting = array(
		'controller'=>'setting_career_cats',
		'SQLfields' => array('id','name','prize','updated','created'),
		'page_caption'=>'Nastavení kategorií pracovních pozic',
		'sortBy'=>'SettingCareerCat.created.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat kategorii|add',
		//	'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
		//	'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
		//	'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
		//	'SettingAccommodationType-status'	=>	'select|Stav|select_stav_zadosti',
		//	'SettingAccommodationType-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|SettingCareerCat|id|text|',
			'name'		=>	'Název|SettingCareerCat|name|text|',
			'cena'		=>	'Cena|SettingCareerCat|prize|text',
			'updated'	=>	'Upraveno|SettingCareerCat|updated|datetime|',
			'created'	=>	'Vytvořeno|SettingCareerCat|created|datetime|'
		),
		'posibility' => array(
			'status'	=> 	'status|Změna stavu|status',
			'edit'		=>	'edit|Editace položky|edit',
			'trash'	=>	'trash|Do košiku|trash'
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Nastavení kategorií pracovních pozic'=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null)
				$this->data = $this->SettingCareerCat->read(null,$id);
			$this->render('edit');
		} else {
			$this->SettingCareerCat->save($this->data);
			die();
		}
	}
}
?>