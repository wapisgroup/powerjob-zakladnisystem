<?php
//Configure::write('debug',2);
/**
 * 
 * @author Zbyněk Strnad - Fastest Solution s.r.o.
 * @created 16.9.2009
 */

class CampaignEmailMessagesController extends AppController {
	var $name = 'CampaignEmailMessages';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('CampaignEmailMessage');
	var $renderSetting = array(
		'bindModel' => array(
			'belongsTo' => array(
				'CampaignEmail'=>array(
					'foreignKey'=>'group_id'
				)
			)
		),
		'SQLfields' => array('*,(SELECT name FROM wapis__cms_users as CU Where CampaignEmail.cms_user_id=CU.id) as odeslal,
								MONTH(CampaignEmailMessage.created) as mesic, 
								YEAR(CampaignEmailMessage.created) as rok'
		),
		'controller'=> 'campaign_email_messages',
		'page_caption'=>'Email - Zprávy',
		'sortBy'=>'CampaignEmailMessage.created.DESC',
		'top_action' => array(
			// caption|url|description|permission
			//'add_item'		=>	'Přidat|edit|Přidat novou SMS kampaň|edit',
		),
		'filtration' => array(
			'CampaignEmail-cms_user_id'		=>	'select|Odesílatel|odesilatel_list',
			'CampaignEmailMessage-status_sms'		=>	'select|Status|status_sms_list',
			'CampaignEmailMessage-group_id'		=>	'select|Kampaň|kampane_list',
			'CampaignEmailMessage-created|year'							=>	'select|Rok|actual_years_list',
			'CampaignEmailMessage-created|month'							=>	'select|Měsíc|mesice_list',
		),
		'items' => array(
			'id'			=>	'ID|CampaignEmailMessage|id|text|',
			'vytvoril'		=>	'Odeslal|0|odeslal|text|',
			'kampan'		=>	'Kampaň|CampaignEmail|name|text|',
			'name'			=>	'Komu|CampaignEmailMessage|client_name|text|',
			'text'			=>	'Text|CampaignEmailMessage|text|text|',
			'mesic'		=>	'Mesic|0|mesic|text|',
			'rok'		=>	'Rok|0|rok|text|',
			'created'		=>	'Vytvořeno|CampaignEmail|created|datetime|'
		),
		'posibility' => array(
			//'edit'		=>	'edit|Editace SMS kampaně|edit'	
            'trash'		=>	'trash|Smazat|trash'	
		)
	);
	

	/**
	 * 
	 * @return view
	 */
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','SMS - Zprávy'=>'#'));
	

		$this->loadModel('CmsUser');
		$this->set('odesilatel_list',$this->CmsUser->find('list',array('order'=>'name ASC','conditions'=>array('kos'=>0,'status'=>1))));
		
		$this->loadModel('CampaignEmail');
		$this->set('kampane_list',$this->CampaignEmail->find('list',array('order'=>'name ASC','conditions'=>array('kos'=>0,'status'=>1))));
		

		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	
	
}
?>