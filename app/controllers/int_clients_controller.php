<?php


//$ch	= (isset($_GET['filtration_ConnectionClientRecruiter-cms_user_idint_ccr']) && !empty($_GET['filtration_ConnectionClientRecruiter-cms_user_idint_ccr']))?2:1;
$group = 'ClientView.id';// HAVING COUNT(DISTINCT ConnectionClientRecruiter.cms_user_id) = '.$ch;
@define('GROUP', $group);

//Configure::write('debug',2);
class IntClientsController extends AppController {
	var $name = 'IntClients';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload','Email');
	var $uses = array('ClientView','Client');
	var $renderSetting = array(
		'permModel'=>'ConnectionClientRecruiter',
		'bindModel' => array(
            'hasOne' => array(
                'ConnectionClientCareerItem' => array('foreignKey' => 'client_id'),
                'ConnectionClientRecruiter' => array('className' => 'ConnectionClientRecruiter', 'foreignKey' => 'client_id'),
                'ConnectionClientRequirement' => array(
					'className' => 'ConnectionClientRequirement', 
					'foreignKey' => 'client_id',
					'conditions' => array(
						'ConnectionClientRequirement.kos'=>0,
						'ConnectionClientRequirement.to'=>'0000-00-00',
						'(ConnectionClientRequirement.type = 2 OR ConnectionClientRequirement.type = 1)'
						
					)
				),
                'ClientRating' => array(
                    'className' => 'ClientRating', 
                    'foreignKey' => 'client_id',
                )
		    ),
            'joinSpec'=>array(
				'HistoryCompany' => array(
					'className' => 'ConnectionClientRequirement',
                    'primaryKey' => 'ClientView.id', 
					'foreignKey' => 'HistoryCompany.client_id',
					'conditions' => array(
						'HistoryCompany.kos'=>0,
						'HistoryCompany.to !='=>'0000-00-00',
						'HistoryCompany.type'=>2
					)
				),
                'ConnectionEmployed' => array(
                    'className' => 'ConnectionClientRequirement',
                    'foreignKey' => 'ClientView.id',
                    'primaryKey' => 'ConnectionEmployed.client_id',
                    'conditions' => array(
                        'ConnectionEmployed.kos'=>0,
                        'ConnectionEmployed.from >'=>'0000-00-00',
                        'ConnectionEmployed.to !='=>'0000-00-00',
                        '(ConnectionEmployed.type = 2 OR ConnectionEmployed.type = 1)'
                    )
                ),
                'CompanyEmployed' => array(
                    'className' => 'Company',
                    'foreignKey' => 'CompanyEmployed.id',
                    'primaryKey' => 'ConnectionEmployed.company_id',
                ),
              /*  'Client' => array(
                    'className' => 'Client',
                    'foreignKey' => 'Client.id',
                    'primaryKey' => 'ClientView.id',
                ),*/
            )    
        ),
		//'SQLfields' => '*,GROUP_CONCAT(profese_name SEPARATOR "<br/>") as Profese ',
		'SQLfields' =>'*,
            Round(AVG(ClientRating.kvalita_prace),1) as kp,
            Round(AVG(ClientRating.spolehlivost),1) as do,
            Round(AVG(ClientRating.alkohol),1) as al,
            MAX(ClientRating.blacklist) as bl,
            IF(ConnectionEmployed.id IS NOT NULL, CompanyEmployed.name, "Ne") as zamestnan,
            HistoryCompany.company_id
        ',
		'SQLcondition' => array(

		),
		'controller'=> 'int_clients',
		'page_caption'=>'Klienti - interní nábor',
		//'count_group_by' => GROUP,
		'group_by' => GROUP,
		//'group_by' => 'ClientView.id',
		'sortBy'=>'ClientView.id.ASC',
	
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
            'add_express_item' => 'Přidat Express|edit_express|Pridat popis|add_express',
            'export_excel' => 'Export Excel|export_excel|Export Excel|export_excel',   
		),
		'filtration' => array(
			'ClientView-name|'		=>	'text|Jméno|',
			//'ClientView-stav'		=>	'select|Status|stav_client_list',
			'ClientView-status'		=>	'select|Status|stav_client_list',
			'ConnectionClientCareerItem-setting_career_item_id'		=>	'select|Profese|profese_list',
			'ConnectionClientRecruiter-cms_user_id#admin_ctrl'		=>	'select|Recruiter|cms_user_list',

			'ConnectionClientRequirement-company_id||HistoryCompany-company_id'		=>	'select|Firma|company_list',
			'ClientView-next|'		=>	'select|Další|next_list',

            'ClientView-mesto|'	=>	'text|Město|',
            'ConnectionClientRecruiter-created|'		=>	'text|Recr. Mesic|',
			'ClientView-countries_id'	=>	'select|Okres|countries_list',
            'ClientView-mobil1|ClientView-mobil2|ClientView-mobil3|'		=>	'text|Telefon|',
            'ClientView-location_work|'	=>	'text|Lokalita|',

            'ClientView-vek|'		=>	'text|Věk|',
            'ClientView-pohlavi_list|'		=>	'select|Pohlavi|pohlavi_list',
            'ClientView-os_pohovor|'		=>	'select|Pohovor|os_pohovor_list',
            'ClientView-info_about_job|'		=>	'select|Informace o prac.místě|info_about_job_list',
            'ClientView-import_stav'		=>	'checkbox|z Www',
		),
		'items' => array(
			'client_manager_id'		=>	'cmid|ClientView|client_manager_id|hidden|',
			'id'		=>	'ID|ClientView|id|text|',
            'name'		=>	'Jméno|ClientView|name|text|',
           // 'namea'		=>	'.|ConnectionClientRecruiter|cms_user_id|text|',
            'profese'	=>	'Profese|ClientView|Profese|text|',
            'mobil'		=>	'Telefon|ClientView|mobil|text|',
            'vek'		=>	'Věk|ClientView|vek|text|',
            'mesto'     => 'Město|ClientView|mesto|text|',
            'okres'     => 'Okres|ClientView|okres|text|',
			//'status'	=>	'Status|ClientView|status|var|stav_client_list',
		//	'employed'	=>	'již Zaměstnán|0|zamestnan|text',
			//'status1'	=>	'Status|ClientView|status|text',//var|stav_client_list
			'status'	=>	'Status|ClientView|status|var|stav_client_list',

			'datum_nastupu'	=>	'Datum umistění / nástupu|ConnectionClientRequirement|from|date|',
			'firma'		=>	'Firma|ClientView|company|text|',
			'kp'		=>	'KP|0|kp|text|',
			'do'		=>	'DO|0|do|text|',
			'al'		=>	'A|0|al|text|',
			'bl'		=>	'DPN|0|bl|var|doporuceni_pro_nabor',

			'status_imp'=>	'StatusImport|ClientView|import_stav|var|stav_importu_list',
		//	'updated'	=>	'Změněno|ClientView|updated|datetime|',
			'created'	=>	'Vytvořeno|ClientView|created|datetime|',
            'datum_vytvoreni'	=>	'Datum nabor|ConnectionClientRecruiter|created|date|',
            'express'	=>	'#|ClientView|express|text|status_to_ico#express'
		
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
			'attach'	=>	'attachs|Přílohy|attach',
			'message'	=>	'messages|Zprávy|message',
			'duplicity_client'	=>	'duplicity_client|Vícenásobné zaměstnání|duplicity_client',
			'vyhodit'	=>	'rozvazat_prac_pomer|Rozvázat pracovní poměr|rozvazat_prac_pomer',
			'zmena_pp'	=>	'zmena_pp|Změna pracovního poměru|zmena_pp',
			'add_activity'	=>	'add_activity|Přidání aktivity|add_activity',
            //'buy'       => 'client_buy|Objednat klienta|client_buy',
            'employ_on_project'=>'employ_on_project|Zaměstnat do projektu|employ_on_project',
			'delete'	=>	'trash|Odstranit položku|trash'			
		),
        'posibility_link' => array(
            'rozvazat_prac_pomer'	=>	'ClientView.id/ConnectionClientRequirement.id',
        ),
		'domwin_setting' => array(
			'sizes' 		=> '[800,900]',
			'scrollbars'	=> true,
			'languages'		=> true,
		)
	);
	
    function beforeFilter(){
        parent::beforeFilter();
        
        if(isset($_GET['filtration_ClientView-next|']) && !empty($_GET['filtration_ClientView-next|'])){
            list($col,$value) = explode('-',$_GET['filtration_ClientView-next|']);

            $this->params['url']['filtration_ClientView-'.$col] = $value;
            
            unset($this->params['url']['filtration_ClientView-next|']); 
            unset($col);
            unset($value);
        }

        if(isset($_GET['filtration_ClientView-import_stav']) && !empty($_GET['filtration_ClientView-import_stav'])){
            unset($this->params['url']['filtration_ClientView-import_stav']);
            $this->params['url']['filtration_ClientView-import_stav'] = -2;

        }


        //pr($_GET);    
    }

    
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Klienti - interní nábor'=>'#'));
			
		$this->loadModel('CmsUser');
		$this->set('cms_user_list', $this->CmsUser->find('list',
			array('conditions'=>array('kos'=>0), 'order'=>'name ASC')
		));

		$this->set('cm_coo_list', $this->CmsUser->find('list',
			array('conditions'=>array('cms_group_id IN (3,4)'),'fields'=>array('name','name'), 'order'=>'name ASC')
		));

		$this->loadModel('Company'); 	
		$company_conditions = array('Company.kos'=>0);
		
		if (isset($this->filtration_company_condition))
			$company_conditions = am($company_conditions, $this->filtration_company_condition);
		 
		$this->set('company_list',		$this->Company->find('list',array(
			'conditions'=>$company_conditions,
			'order'=>array('Company.name ASC')
		)));
		unset($this->Company);
        
        //okresy
        $this->set('countries_list',$this->get_list('Countrie'));
        $this->set('info_about_job_list',$this->get_list('InfoAboutJob'));
        
        /**
        * dalsi moznosit filtrace
        * moznsosti zadavame sloupec-stav => nazev moznosti
        */
        $this->set('next_list',array(
            'express-1'=> 'Expresní klienti',
            'import_adresa-NOTNULL'=> 'Neprázdna importní adresa',
            'email-NOTNULL'=> 'Vyplněný email'
        ));


		$this->loadModel('SettingCareerItem');
		$this->set('profese_list', $this->SettingCareerItem->find('list',
			array('conditions'=>array('kos'=>0), 'order'=>'name ASC')
		));

		//změna pro umístění datum na created misto from
		foreach($this->viewVars['items'] as &$item){
			if($item['ConnectionClientRequirement']['type'] == 1)
				$item['ConnectionClientRequirement']['from'] = $item['ConnectionClientRequirement']['created'];
		}
				


		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->set('scripts',array('uploader/uploader'));
			$this->set('styles',array('../js/uploader/uploader'));
			$this->render('../system/index');
		}
    }
	
	
	
	function edit($id = 0,$domwin = 0, $show = 0){


		echo $this->requestAction('clients/edit/'.$id.'/'.$domwin.'/'.$show.'/1/');
		die();
	}

	/**
 	* Seznam priloh
 	*
	* @param $client_id
 	* @return view
 	* @access public
	**/
	function attachs($client_id){
		echo $this->requestAction('clients/attachs/'.$client_id);
		die();
	}
	
	function rozvazat_prac_pomer($client_id,$connection_id){
		echo $this->requestAction('clients/rozvazat_prac_pomer/'.$client_id.'/'.$connection_id);
		die();
	}	

	// funkce na zmenu pracovniho pomeru
	function zmena_pp($client_id = null,$company_id = null,$company_work_position_id = null){
		echo $this->requestAction('clients/zmena_pp/'.$client_id.'/'.$company_id.'/'.$company_work_position_id);
		die();
	}
    
    function edit_express($id = null,$domwin = null, $show = null){
		echo $this->requestAction('clients/edit_express/'.$id.'/'.$domwin.'/'.$show);
		die();
	}
    
    
    function add_activity($id = null){
		echo $this->requestAction('clients/domwin_add_activity/'.$id.'/');
		die();
	}




    function duplicity_client($client_id = null){
        if($client_id == null)
            die(json_encode(array('result'=>false,'message'=>'Nebylo zadáno id klienta!')));
    
        $data = $this->Client->read(null,$client_id);
        $data['Client']['id'] = $data['Client']['created'] = $data['Client']['updated'] = null;

        $data['Client']['stav'] = 0;
        $data['Client']['parent_id'] = $client_id;
        $data['Client']['parent_creater'] = $this->logged_user['CmsUser']['id'];

        if($this->Client->save($data)){
            /**
             * nacteni zaznamu o profesi
             */
            $this->loadModel('ConnectionClientCareerItem');
            $cci = $this->ConnectionClientCareerItem->find('all',array(
                'conditions'=>array(
                    'client_id'=>$client_id
                )
            ));
            foreach($cci as $item){
                $item['ConnectionClientCareerItem']['id'] = null;
                $item['ConnectionClientCareerItem']['client_id'] = $this->Client->id;
                $this->ConnectionClientCareerItem->id = null;
                $this->ConnectionClientCareerItem->save($item);
            }
            
            
            /**
             * nacteni zaznamu o recruiterech
             */
            $this->loadModel('ConnectionClientRecruiter');
            $ccr = $this->ConnectionClientRecruiter->find('all',array(
                'conditions'=>array(
                    'client_id'=>$client_id
                )
            ));
            foreach($ccr as $item){
                $item['ConnectionClientRecruiter']['id'] = null;
                $item['ConnectionClientRecruiter']['client_id'] = $this->Client->id;
                $this->ConnectionClientRecruiter->id = null;
                $this->ConnectionClientRecruiter->save($item);
            }
         
            die(json_encode(array('result'=>true, 'id'=>$this->Client->id)));
        }    
        else
            die(json_encode(array('result'=>false,'message'=>'Nepodařilo se uložit nového klienta!')));    
    }
    
    function employ_on_project($client_id = null){
        $this->loadModel('ConnectionClientAtCompanyWorkPosition');
        if(empty($this->data)){
            $this->data['ConnectionClientAtCompanyWorkPosition']['client_id'] = $client_id;
            
            $con = array(
                'kos'=>0
            );
            
            $allow_superior_id = array(1,6);
            if(!in_array($this->logged_user['CmsGroup']['cms_group_superior_id'],$allow_superior_id)){
                $con = array(
                    'kos'=>0,
                    'OR'=>array(
                        'manager_id'=>$this->logged_user['CmsUser']['id'],
                        'office_manager_id'=>$this->logged_user['CmsUser']['id']
                    )
                );
            }
            
            $this->loadModel('Client'); 
            $cl = $this->Client->read(array('email'),$client_id);
            $this->data['Client'] = $cl['Client'];
                        
            $this->loadModel('AtProject');
            //$this->set('company_list',$this->AtCompany->find('list',array('conditions'=>array('kos'=>0))));
            $this->set('project_list',$this->AtProject->find('list',array('conditions'=>$con)));
            $this->set('money_item_list',$this->get_list('AtCompanyMoneyItem'));
            	
        }else{
            $this->data['ConnectionClientAtCompanyWorkPosition']['cms_user_id'] = $this->logged_user['CmsUser']['id'];            
            $this->ConnectionClientAtCompanyWorkPosition->save($this->data);
            
            $this->loadModel('Client'); 
            $client = $this->Client->read(array('id','email','name','jmeno','prijmeni','mesto'),$this->data['ConnectionClientAtCompanyWorkPosition']['client_id']);
            unset($this->Client);
            
            $this->loadModel('AtProject'); 
            $project = $this->AtProject->read(array('name','manager_id','office_manager_id'),$this->data['ConnectionClientAtCompanyWorkPosition']['at_project_id']);
            unset($this->AtProject);
            
            $this->loadModel('AtCompany'); 
            $at_comp = $this->AtCompany->read(array('name'),$this->data['ConnectionClientAtCompanyWorkPosition']['at_company_id']);
            unset($this->AtCompany);
            
            $this->loadModel('AtProjectCentre'); 
            $at_centre = $this->AtProjectCentre->read(array('name','reditel_strediska_id'),$this->data['ConnectionClientAtCompanyWorkPosition']['at_project_centre_id']);
            $at_centre2 = $this->AtProjectCentre->read(array('name'),$this->data['ConnectionClientAtCompanyWorkPosition']['at_project_cinnost_id']);
            unset($this->AtProjectCentre);
            
            $this->loadModel('AtProjectCentreWorkPosition'); 
            $at_wp = $this->AtProjectCentreWorkPosition->read(array('name'),$this->data['ConnectionClientAtCompanyWorkPosition']['at_project_centre_work_position_id']);
            unset($this->AtProjectCentreWorkPosition);
            
            $this->loadModel('AtCompanyMoneyItem'); 
            $at_mi = $this->AtCompanyMoneyItem->read(array('name'),$this->data['ConnectionClientAtCompanyWorkPosition']['at_company_money_item_id']);
            unset($this->AtCompanyMoneyItem);
            
            $replace_list = array(
            	'##Client.name##' 		=>$client['Client']['name'],
                '##Client.mesto##'=> $client['Client']['mesto'],
                '##AtProject.name##'=>$project['AtProject']['name'],
                '##AtCompany.name##'=>$at_comp['AtCompany']['name'],
                '##cinnost_name##' =>$at_centre2['AtProjectCentre']["name"],
                '##AtProjectCentre.name##'=>$at_centre['AtProjectCentre']['name'],
                '##AtProjectCentreWorkPosition.name##'=>$at_wp['AtProjectCentreWorkPosition']['name'],
                '##AtCompanyMoneyItem.name##'=>$at_mi['AtCompanyMoneyItem']['name'],
                '##datum_nastupu##'=>date('d.m.Y',strtotime($this->data['ConnectionClientAtCompanyWorkPosition']['datum_nastupu']))
            );
            $this->Email->set_company_data(array(
                'project_manager_id'=>$project['AtProject']['manager_id'],
                'office_manager_id'=>$project['AtProject']['office_manager_id'],
                'reditel_strediska_id'=>$at_centre['AtProjectCentre']['reditel_strediska_id']
            ));
		    $this->Email->send_from_template_new(23,array(),$replace_list);
            
            if(isset($this->data['generate_login']) && $this->data['generate_login'] == 1){
                self::generate_int_zam_login($client,$this->data['group_id']);
            }
            
            die();
        }
    }
    
    
    function ajax_load_at_company_work_position_list($at_centre_id = null){
        if ($at_centre_id == null) 
			die(json_encode(array('result'=>false,'message'=>'Nelze zvolit prázdne středisko.')));
	   
        $this->loadModel('AtProjectCentreWorkPosition');
		die(json_encode($this->AtProjectCentreWorkPosition->find('list', array(
            'conditions'=>array(
                'at_project_centre_id'=>$at_centre_id,
                'kos'=>0
            ), 
            'order'=>'name ASC'
        ))));	

    } 
    
    function ajax_load_at_company_centre_list($at_project_id = null){
        if ($at_project_id == null) 
			die(json_encode(array('result'=>false,'message'=>'Nelze zvolit prázdny projekt.')));
	   
        $this->loadModel('AtProjectCentre');
		die(json_encode($this->AtProjectCentre->find('list', array(
            'conditions'=>array(
                'at_project_id'=>$at_project_id,
                'kos'=>0,
                'type_id' => 1
            ), 
            'order'=>'name ASC'
        ))));	

    }

    function ajax_load_at_company_cinnost_list($at_stredisko_id = null){
            if ($at_stredisko_id == null)
    			die(json_encode(array('result'=>false,'message'=>'Nelze zvolit prázdne stredisko.')));

            $this->loadModel('AtProjectCentre');
    		die(json_encode($this->AtProjectCentre->find('list', array(
                'conditions'=>array(
                    'parent_id'=>$at_stredisko_id,
                    'kos'=>0,
                    'type_id' => 2
                ),
                'order'=>'name ASC'
            ))));

        }
    
    function ajax_load_at_company_list($at_project_id = null){
        if ($at_project_id == null) 
			die(json_encode(array('result'=>false,'message'=>'Nelze zvolit prázdny projekt.')));
	   
       
        $this->loadModel('AtCompany');
        $this->loadModel('AtProjectCompany');
		die(json_encode($this->AtCompany->find('list', array(
            'conditions'=>array(
                'id IN(SELECT at_company_id FROM wapis__at_project_companies WHERE at_project_id = '.$at_project_id.' AND kos=0)',
                'kos'=>0
            ), 
            'order'=>'name ASC'
        ))));	

    } 
    
    
    /**
     * Funkce kontroluje, zda profese kterou se zvolila při zamestnani potrebuje vygenerovat login.
     * Tzn. profese ma prirazenou uzivatelskou skupinou
     */
    function need_genereate_login($centre_work_position_id = null){
        if ($centre_work_position_id == null)
			die(json_encode(array('result'=>false,'message'=>'Nelze zvolit prázdnou profesi.')));
	   
       
        $this->loadModel('AtProjectCentreWorkPosition');
        $position = $this->AtProjectCentreWorkPosition->read('cms_group_id',$centre_work_position_id);
	    
        if($position){
            $res = ($position['AtProjectCentreWorkPosition']['cms_group_id'] == ''?0:1);
            die(json_encode(array('result'=>true, 'need'=>$res, 'group_id'=>$position['AtProjectCentreWorkPosition']['cms_group_id'])));
        }    
        else
            die(json_encode(array('result'=>false,'message'=>'Nepodařilo se najít danou profesi!')));   

    }
    
    /**
     * Zjistujeme, zda email u zamestnavaneho clienta je v internich zam. unikatni
     * kvuli loginu do ATEPu
     */
    function check_mail_in_int_zam($email,$connection_id = null){
        $this->loadModel('ConnectionClientAtCompanyWorkPosition');
        $this->ConnectionClientAtCompanyWorkPosition->bindModel(array(
            'belongsTo'=>array('Client')
        ));
        
        $con = array(
                'Client.email'=>$email,
                'ConnectionClientAtCompanyWorkPosition.datum_to'=>'0000-00-00',
                'ConnectionClientAtCompanyWorkPosition.kos'=>0
            );
        if($connection_id != null){
            $con = am($con,array('ConnectionClientAtCompanyWorkPosition.id != '=>$connection_id));
        }    
        
        $client_with_mail_exist = $this->ConnectionClientAtCompanyWorkPosition->find('first',array(
            'conditions'=>$con,
            'fields'=>array('Client.name')
        ));
        
        if($client_with_mail_exist){
            die(json_encode(array('result'=>true, 'zam'=>$client_with_mail_exist['Client']['name'])));
        }
        else
            die(json_encode(array('result'=>false)));
    }     
    
    /**
     * Vygenerujeme pristup do ATEPu pro intyerniho zamestnance, na zaklade emailu
     * A pošleme mu na mail vygenerovane heslo
     */
    function generate_int_zam_login($client,$cms_group_id){
        $heslo = substr(md5($client['Client']['id'].time()),0,6);
        
        $save_data['CmsUser'] = $client['Client'];
        $save_data['CmsUser']['cms_group_id'] = $cms_group_id;
        $save_data['CmsUser']['at_employee_id'] = $client['Client']['id'];
        $save_data['CmsUser']['heslo'] = $heslo;
        
        unset($save_data['CmsUser']['id']);
        $this->loadModel('CmsUser');
        $this->CmsUser->id = null;
        $this->CmsUser->save($save_data);
        
        $replace_list = array(
        	'##Client.email##'=>$client['Client']['email'],
            '##heslo##'=> $heslo,
        );
        //$this->Email->set_company_data(array('project_manager_id'=>$project['AtProject']['manager_id'],'office_manager_id'=>$project['AtProject']['office_manager_id']));
	    $this->Email->clear_company_data();
        $this->Email->send_from_template_new(33,array($client['Client']['email']),$replace_list);
            
    }
    
    
     /**
     * funkce pro vytvoreni exportu do excelu - CSV
     * podle filtrace vyber dane klienty a vygeneruj je do csv
     */
    function export_excel(){
       //Configure::write('debug',1);
        
       $start = microtime ();
       $start = explode ( " " , $start );
       $start = $start [ 1 ]+ $start [ 0 ];  

         $fields_sql = array(
          "ClientView.id",
            "ClientView.email",
            "ClientView.private_email",
            "ClientView.name",
            "ClientView.Profese",
            "ClientView.client_manager",
            "ClientView.mobil",
            "ClientView.vek",
            "ClientView.mesto",
            "ClientView.okres",
            "ClientView.stav",
            "ConnectionClientRequirement.from",
            "ClientView.company",
            "ClientView.created",
            'Round(AVG(ClientRating.kvalita_prace),1) as kp',
            'Round(AVG(ClientRating.spolehlivost),1) as do',
            'Round(AVG(ClientRating.alkohol),1) as al',
            'MAX(ClientRating.blacklist) as bl',
            'HistoryCompany.company_id',
            'ConnectionClientRecruiter.cms_user_id',
			'ClientView.parent_id',
			'ConnectionClientCareerItem.setting_career_item_id',
			'ConnectionClientRecruiter.cms_user_id',
			'ConnectionClientRequirement.company_id',
			'ClientView.countries_id',
            'ClientView.express',
            'ClientView.import_adresa',
         );
         
               
        $criteria = $this->ViewIndex->filtration();        
      
    
         header("Pragma: public"); // požadováno
         header("Expires: 0");
         header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
         header("Cache-Control: private",false); // požadováno u některých prohlížečů
         header("Content-Transfer-Encoding: binary");
         Header('Content-Type: application/octet-stream');
         Header('Content-Disposition: attachment; filename="'.date('Ymd_His').'.csv"');
     
      
        /*
         * Celkovy pocet zaznamu
         */
         
        $limit = 100;   
        $page = 1;     
        $this->ClientView->bindModel($this->renderSetting['bindModel'],false);   
        $count = $this->ClientView->find('first',
        	array(
        		'fields' =>  array("COUNT(DISTINCT ClientView.id) as count"),
        		'conditions'=>am($criteria,$this->renderSetting['SQLcondition']),
        		'recursive'	=>1
        	)
        );    
        $count = $count[0]['count'];
        $this->renderSetting['items'] = am($this->renderSetting['items'],array(
            'email'=>'Email|ClientView|email|text|',
            'private_email'=>'Soukromý Email|ClientView|private_email|text|'
        )); 
        unset($this->renderSetting['items']['status_imp']);
        unset($this->renderSetting['items']['express']);
        // hlavicka
        foreach($this->renderSetting['items'] as &$item_setting){
            list($caption, $model, $col, $type, $fnc) = explode('|',$item_setting);
        	$item_setting = compact(array("caption", "model","col","type","fnc"));	
            if($type != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',$caption).'";'; 
        }
        echo "\n";   
        unset($item_setting, $caption, $model, $col, $type, $fnc);
        
        $str_array=array("<br/>"=>', ',':'=>',',';'=>',','?'=>'', '#'=>' ');
        /*
         * Cyklicky vypis dat po $limit zaznamu
         */
        
        for ($exported = 0; $exported < $count; $exported += $limit){
            $this->ClientView->bindModel($this->renderSetting['bindModel'],false);
            foreach($this->ClientView->find('all',array(
                   'fields'=>$fields_sql,
                    'conditions'=>am($criteria,$this->renderSetting['SQLcondition']),
                    'group'=>$this->renderSetting['group_by'],
                    'limit'=>$limit,
                    'page'=>$page,
                    'recursive'=>1,
                    'order'=>'ClientView.id ASC'
            )) as $item){
                foreach($this->renderSetting['items'] as $key => $td){
                    if($td['type'] != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',strtr($this->ViewIndex->generate_td($item,$td,$this->viewVars),$str_array)).'";';     
                }
                echo "\n";  
            }
            $page++;
        }
       
        //time
         $end = microtime ();
         $end = explode ( " " , $end );
         $end = $end [ 1 ]+ $end [ 0 ]; 
         echo 'Generate in '.($end - $start).'s';
         echo "\n"; 
        
        die();
    }

    function client_buy($id = null, $domwin = null, $show = null)
    {
        if(!$this->data){
            if($id){
                $mime = false;
                $this->set('id',$id);
                $this->set('cms_user_id', $this->logged_user['CmsUser']['id']);

                $this->loadModel('SettingCareerItem');
                $work_positions = $this->SettingCareerItem->find('list');
                $this->set('work_list',$work_positions);
                $this->loadModel('Client');
                $client = $this->Client->read(null, $id);
                $this->set('client',$client);

                $this->loadModel('ClientBuy');
                $this->ClientBuy->bindModel(array(
                    'joinSpec' => array(
                        'CmsUser' => array(
                            'primaryKey' => 'CmsUser.id',
                            'foreignKey' => 'ClientBuy.to_cms_user_id'
                        ),)
                ));
                $owner = $this->ClientBuy->find('first', array('fields'=>array('CmsUser.name','CmsUser.id','ClientBuy.*'),'conditions'=>array('client_id' =>$id,'pay_date !='=>null, 'to_date ='=>'0000-00-00')));
                if($owner['ClientBuy']['to_cms_user_id'] == $this->logged_user['CmsUser']['id']){
                    $mine = true;
                }
                $this->ClientBuy->bindModel(array(
                    'joinSpec' => array(
                        'CmsUser' => array(
                            'primaryKey' => 'CmsUser.id',
                            'foreignKey' => 'ClientBuy.to_cms_user_id'
                        ),)
                ));
                $request = $this->ClientBuy->find('first', array('fields'=>array('ClientBuy.*','CmsUser.name'),'conditions'=>array('client_id' =>$id,'ISNULL(pay_date)', 'ClientBuy.status'=>1)));
                if($request){
                    $this->set('request',$request);
                }

                if(!$owner){
                    $this->loadModel('CmsUser');
                    $owner = $this->CmsUser->read(null, $client['Client']['cms_user_id']);
                    if($owner['Client']['id'] == $this->logged_user['CmsUser']['id']){
                        $mine = true;
                    }
                    if(!$owner){
                        $owner['CmsUser']['name'] = -1;
                    }
                }
                $this->set('owner',$owner);
                $this->set('mine',$mine);

                $this->render('client_buy');

            }else{

                die();
            }

        }
        else //SAVE
        {
            $this->loadModel('ClientBuy');
            $this->data['ClientBuy']['client_id'] = $id;
            $this->data['ClientBuy']['from_date'] = date('Y-m-d');

            if($this->data['Action'] == 'buy' ){
                $this->loadModel('CmsUser');
                $this->loadModel('Client');
                $client = $this->Client->read(null,  $this->data['ClientBuy']['client_id']);
                $user_to =  $this->CmsUser->read(null,  $this->data['ClientBuy']['to_cms_user_id']);
                $user = $this->CmsUser->read(null,  $this->data['ClientBuy']['from_cms_user_id']);
                $ret = $this->ClientBuy->save($this->data);
                if($ret){

                    $replace_list = array(
                        '##client##'=>$client['Client']['name'],
                        '##cms_user##'=>$user_to['CmsUser']['name'],
                    );
                    $this->Email->clear_company_data();
                    $this->Email->send_from_template_new(63,array($user['CmsUser']['email']),$replace_list); //$user['CmsUser']['email']

                    $return = true;
                    $msg = 'Objednávka úspěšně dokončena (odesláno na email '.$user['CmsUser']['email'].')';
                }else{
                    $return = false;
                }
            }
            else if($this->data['Action'] == 'make_free'){
                $to_save['ClientBuy']['id'] = $this->data['Request']['id'];
                $to_save['ClientBuy']['status'] = 0;

                $ret = $this->ClientBuy->save( $to_save );
                if($ret){
                    $return = true;
                    $msg = 'Objednávka zrušena';
                }else{
                    $return = false;
                }
            }
            else if( $this->data['Action'] == 'request_to_free') {
                $this->loadModel('CmsUser');
                $request_user = $this->CmsUser->read(null,  $this->data['Request']['to_cms_user_id']);
                $user = $this->CmsUser->read(null, $this->logged_user['CmsUser']['id'] );
                $client = $this->Client->read(null,  $this->data['ClientBuy']['client_id']);
                $text = $this->data['RequestText'];
                if($request_user){

                    $replace_list = array(
                        '##client##'=>$client['Client']['name'],
                        '##cms_user##'=>$user['CmsUser']['name'],
                        '##text##'=>$text
                    );
                    $this->Email->clear_company_data();
                    $this->Email->send_from_template_new(64,array($user['CmsUser']['email']),$replace_list); //$user['CmsUser']['email']

                    $return = true;
                    $msg = 'Žádost zaslána na email '.$request_user['CmsUser']['email'];
                }else{
                    $return = false;
                }
            }

            die(json_encode(array('result'=>$return, 'msg'=>$msg)));
        }
    }


}
?>