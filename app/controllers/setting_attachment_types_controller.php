<?php
class SettingAttachmentTypesController extends AppController {
	var $name = 'SettingAttachmentTypes';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('SettingAttachmentType');
	var $renderSetting = array(
		'controller'=>'setting_attachment_types',
		'SQLfields' => array('id','name','updated','created','status','poradi'),
		'page_caption'=>'Nastavení typů aktivit',
		'sortBy'=>'SettingAttachmentType.poradi.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
		),
		'filtration' => array(),
		'items' => array(
			'id'		=>	'ID|SettingAttachmentType|id|text|',
			'poradi'	=>	'Pořadí|SettingAttachmentType|poradi|text|',
			'name'		=>	'Název|SettingAttachmentType|name|text|',
			'updated'	=>	'Upraveno|SettingAttachmentType|updated|datetime|',
			'created'	=>	'Vytvořeno|SettingAttachmentType|created|datetime|'
		),
		'posibility' => array(
			'status'	=> 	'status|Změna stavu|status',
			'edit'		=>	'edit|Editace položky|edit',
			'trash'	=>	'trash|Do košiku|trash'
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Nastavení kategorií příloh'=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null)
				$this->data = $this->SettingAttachmentType->read(null,$id);
			$this->render('edit');
		} else {
			$this->SettingAttachmentType->save($this->data);
			die();
		}
	}
}
?>