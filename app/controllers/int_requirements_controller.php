<?php

class IntRequirementsController  extends AppController {
	var $name = 'IntRequirements';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Email','Upload');
	var $uses = array('RequirementsForRecruitment');
	var $renderSetting = array(
		'bindModel'	=> array('belongsTo'=>array('Company','CmsUser','SettingCareerCat')),
		'controller'=>'int_requirements',
		'SQLfields' => array('RequirementsForRecruitment.express','SettingCareerCat.name','Company.name','RequirementsForRecruitment.id','RequirementsForRecruitment.id','RequirementsForRecruitment.cms_user_id','RequirementsForRecruitment.name','count_of_free_position','count_of_substitute','salary','job_city','typ_externi_nabor','datetime_of_start',
			'(SELECT Count(`ConnectionClientRequirement`.`id`) FROM `wapis__connection_client_requirements` AS `ConnectionClientRequirement` WHERE (type=2 OR type=1)  AND ConnectionClientRequirement.requirements_for_recruitment_id=RequirementsForRecruitment.id) pocet_zam'
		),
		'SQLcondition'=> array(
			'RequirementsForRecruitment.checkbox_interni_nabor'=>1,
			'(RequirementsForRecruitment.publikovani_typ=1 OR RequirementsForRecruitment.publikovani_typ =2)'
		),
		'page_caption'=>'Seznam požadavků pro interní nábor',
		'sortBy'=>'Company.name.ASC',
		'top_action' => array(
			//'add_item'		=>	'Přidat|edit|Pridat popis|add',
		),
		'filtration' => array(
			'RequirementsForRecruitment-name|'			=>	'text|Pozice|',
			'RequirementsForRecruitment-company_id'		=>	'select|Společnost|company_list',
		),
		'items' => array(
			'id'				=>	'ID|RequirementsForRecruitment|id|hidden|',
			'cms_user_id'		=>	'CMS|RequirementsForRecruitment|cms_user_id|hidden|',
			'firma'				=>	'Firma|Company|name|text|',
			'name'				=>	'Pracovní pozice|RequirementsForRecruitment|name|text|',
			'pocet_mist'		=>	'Počet míst|RequirementsForRecruitment|count_of_free_position|text|',
			//'pocet_nahradniku'	=>	'Počet náhradníků|RequirementsForRecruitment|count_of_substitute|text|',
			'obsazeno'			=>	'Obsazenost|0|pocet_zam|text|',
			'kat'				=>	'Kategorie odměny|SettingCareerCat|name|text|',
			'nabizena_mzda'		=>	'Nabízená mzda|RequirementsForRecruitment|salary|text|',
			'datum_nastupu'		=>	'Datum nástupu|RequirementsForRecruitment|datetime_of_start|datetime|',
			'job_city'		=>	'Město|RequirementsForRecruitment|job_city|text|',
			'typ'		=>	'Typ|RequirementsForRecruitment|typ_externi_nabor|var|typ_externi_nabor',
             'express' => '#|RequirementsForRecruitment|express|text|status_to_ico#express'
		),
		'posibility' => array(
			'show'			=>	'show|Detail|show',
			//'attach'	=>	'attachs|Přílohy|attach'	
			'pozice'		=>	'nabor_pozice|Obsazení pracovních pozic|pozice'			
			//'zamestnanci'	=>	'zamestnanci|Zemestnani lide|edit',			
		),
		'domwin_setting' => array(
		'sizes'			=>	'[800,900]',
		)	
	);
	
	function index(){

		// set JS and Style
		$this->set('scripts',array('uploader/uploader','clearbox/clearbox'));
		$this->set('styles',array('../js/uploader/uploader','../js/clearbox/clearbox'));
		// set FastLinks
		$this->set('fastlinks',array('ATEP'=>'/','Nábor'=>'#','Seznam požadavků pro interní nábor'=>'#'));
		
	
		$this->loadModel('Company'); 	
		
		$this->set('company_list',		$this->Company->find('list',array('conditions'=>array('Company.kos'=>0),'order'=>array('Company.name ASC'))));
		unset($this->Company);

		//obsazeni :rendring
		foreach($this->viewVars['items'] as &$item){
			$obsazenych_mist = $item['0']['pocet_zam'];
			$pocet_mist = $item['RequirementsForRecruitment']['count_of_free_position'];
			$pocet_nahradniku = $item['RequirementsForRecruitment']['count_of_substitute'];

			$celkem_mozno_obsadit = $pocet_mist + $pocet_nahradniku;

			$item['0']['pocet_zam'] = $obsazenych_mist." z ".$celkem_mozno_obsadit;
			
			//novy pocet mist
			$item['RequirementsForRecruitment']['count_of_free_position'] = $pocet_mist.' + '.($pocet_nahradniku <> '' ? $pocet_nahradniku : 0);
		}
		

		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function show($id = null,$print = null){
		echo $this->requestAction('companies/nabor_edit/-2/'.$id.'/'.$print);
		die();
	}
	

	function nabor_pozice($requirements_for_recruitment_id){
		echo $this->requestAction('report_requirements_for_recruitments/nabor_pozice/' . $requirements_for_recruitment_id);
		die();
	}
	
	
	
	
}
?>