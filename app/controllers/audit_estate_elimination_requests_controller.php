<?php
Configure::write('debug',1);
	define('fields','*,
        ( 
          CONCAT_WS(" ",AuditEstate.first_cost,if(AuditEstate.currency = 0,",- EUR",",- Kč"))  
		) as first_cost,
        ( 
          CONCAT_WS(" ",AuditEstate.hire_price,if(AuditEstate.currency = 0,",- EUR",",- Kč"))  
		) as hire_price		
	');
class AuditEstateEliminationRequestsController extends AppController {
	var $name = 'AuditEstateEliminationRequests';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('AuditEstate');
	var $renderSetting = array(
		'bindModel'	=> array(
            'belongsTo'=>array('AtCompany','AtProjectCentre'),
            'joinSpec'=>array(
                'SpravceMajetku'=>array(
    				'className'=>'CmsUser',
    				'primaryKey'=>'SpravceMajetku.id',
    				'foreignKey'=>'AtProjectCentre.spravce_majetku_id'
			    )
            )
        ),
        'SQLcondition'=>array(
            'AuditEstate.elimination_request'=>1,
            'AuditEstate.elimination_date IS NULL'
        ),
        'SQLfields'=>array(fields),
		'controller'=> 'audit_estate_elimination_requests',
		'page_caption'=>'Žádosti o vyřazení majetku',
		'sortBy'=>'AuditEstate.id.DESC',
		'top_action' => array(
			//'add_item'		=>	'Přidat majetek|edit|Pridat majetek|add',
		),
		'filtration' => array(
			'AuditEstate-name'				=>	'text|Jméno|',
			'AuditEstate-at_company_id'	    =>	'select|Firma|company_list',
            'AuditEstate-odpisovatelny'	    =>	'checkbox|Odpisovat.|',
		),
		'items' => array(
			'id'		=>	'ID|AuditEstate|id|text|',
			'name'		=>	'Název|AuditEstate|name|text|',
			'company'	=>	'Firma|AtCompany|name|text|',
			'company_person'	=>	'Správce|SpravceMajetku|name|text|',
			'company_centre'	=>	'Středisko|AtProjectCentre|name|text|',
			'first_cost'=>	'Pořizovací cena|0|first_cost|text|',
			'payment_date'	=>	'Datum pořízení|AuditEstate|payment_date|date|',
			'hire_price'	=>	'Měs. pronájem|0|hire_price|text|'
		),
		'posibility' => array(
			'show'		        =>	'show|Detail položky|show',
			'elimination'		=>	'elimination|Vyřazení majetku|elimination',
		),
        'domwin_setting' => array(
			'sizes' 		=> '[500,300]',
		)
	);
    function beforeFilter(){
        parent::beforeFilter();
        
        $sum = $this->AuditEstate->find('all',array(
            'conditions'=>array('kos'=>0,'elimination_request'=>1,'AuditEstate.elimination_date IS NULL'),
            'fields'=>array('SUM(first_cost) as celkem')
        ));
        $this->renderSetting['page_caption'] .= '<br /><br /><small>Celkem vyřazený majetek v hodnotě: '.$sum[0][0]['celkem'].' EUR</small>';
    }
    
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Evidence majetku'=>'#',$this->renderSetting['page_caption']=>'#'));
        $this->set('company_list',$this->get_list('AtCompany'));

		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
    

	function show($id = null){
	    $list = $this->get_list('AuditEstateWithdrawalType');
        $this->set('cat_list',$list);
        $this->data = $this->AuditEstate->read(null,$id);
        $this->render('detail');
	}
    
    /**
     * funkce pro vyrazeni majetku
     * zadava se datum ke kteremu bude vyrazeno
     */
	function elimination($id = null){
	   if(empty($this->data)){
            $this->data['AuditEstate']['id'] = $id;
            $this->render('../audit_estates/elimination');
       }
       else{        
            $this->AuditEstate->save($this->data);             
	   }
	}

}
?>