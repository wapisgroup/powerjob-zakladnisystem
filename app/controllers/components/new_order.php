<?php
class NewOrderComponent extends Object{
	var $components = array('Pagination','Session');
    public $criteria = array();
	
	function startup(&$controller) {
		$this->controller =$controller;
	
		switch($this->controller->action){
		//	case 'edit': $this->edit(); break;
			case 'index':
				if (!isset($this->controller->noComponentIndex)){
					$this->renderSetting = $this->controller->renderSetting;
					$this->render_index();
				}
				break;
			default:
				break;
		}
	}
	    
    function get_bindmodels($data){
        $data2 = $data; 
        $first = array_keys(array_shift($data));
        $second = array_values(array_shift($data2));
        
        $r = array_unique(am($second,$first));
        
        foreach($r as $id=>$item){ 
            if(is_int($item))
                unset($r[$id]);
        }
        
        return $r;
    }
	
	function render_index(){
        $criteria = array();
		$model_class = $this->controller->uses[0];
        
        /**
		// START permission
		$logged_user = $this->Session->read('logged_user');
		$cntrl_perm = $logged_user['CmsGroup']['permission'][$this->renderSetting['controller']];
			
		//print_r($logged_user['CmsGroup']['permission'][$this->renderSetting['controller']]);
		if (isset($logged_user['CmsGroup']['permission'][$this->renderSetting['controller']]['index']) && $logged_user['CmsGroup']['permission'][$this->renderSetting['controller']]['index'] == 2){
			if ((isset($cntrl_perm['group_id']) && !empty($cntrl_perm['group_id'])) || isset($this->renderSetting['TypUserCompany'])){ // pro companyController
				
				if (isset($this->renderSetting['permModel']))
					$criteria[$this->renderSetting['permModel'].'.'.$logged_user['CmsGroup']['permission']['companies']['group_id']]= $logged_user['CmsUser']['id'];
				else if($cntrl_perm['group_id'] == 'coordinator_double'){
					//$criteria[$model_class.'.'.'coordinator_id']= $logged_user['CmsUser']['id'];
					$criteria[$model_class.'.'.'coordinator_id2']= $logged_user['CmsUser']['id'];
				}
				else
					$criteria[$model_class.'.'.$cntrl_perm['group_id']]= $logged_user['CmsUser']['id'];
			} 
            else if(isset($this->renderSetting['only_his'])){ 
                if(!isset($this->renderSetting['only_his']['group']) || $this->renderSetting['only_his']['group'] === false) // jen sve cms_user_id
                    $criteria[]= $model_class.'.id IN(SELECT '.$this->renderSetting['only_his']['col'].' FROM '.$this->renderSetting['only_his']['table'].' Where cms_user_id = '.$logged_user['CmsUser']['id'].')';	   
                else    
                    $criteria[]= $model_class.'.id IN(SELECT '.$this->renderSetting['only_his']['col'].' FROM '.$this->renderSetting['only_his']['table'].' Where cms_group_id = '.$logged_user['CmsGroup']['id'].')';	                   
            }           
            else {
				if (isset($this->renderSetting['permModel']))
					$criteria[$this->renderSetting['permModel'].'.cms_user_id']= $logged_user['CmsUser']['id'];
				else
					$criteria[$model_class.'.cms_user_id']= $logged_user['CmsUser']['id'];
			}
		} else if (isset($logged_user['CmsGroup']['permission'][$this->renderSetting['controller']]['index']) && $logged_user['CmsGroup']['permission'][$this->renderSetting['controller']]['index'] == 3){
			if(isset($logged_user['CmsGroup']['permission'][$this->renderSetting['controller']]['spec_permision']) && !Empty($logged_user['CmsGroup']['permission'][$this->renderSetting['controller']]['spec_permision']))
				$criteria[]=$logged_user['CmsGroup']['permission'][$this->renderSetting['controller']]['spec_permision'];
			else if ((isset($cntrl_perm['group_id']) && !empty($cntrl_perm['group_id'])) || isset($this->renderSetting['TypUserCompany'])){ // pro companyController
				$model_class_coo = (($model_class == 'CompanyView' || in_array('CompanyView',$this->get_bindmodels($this->renderSetting['bindModel']))) ? 'CompanyView' : 'Company' );
                      
                if($model_class == 'ClientView')
					$model_class_coo = 'ClientView';
                    
				if(isset($cntrl_perm['group_id']) && ($cntrl_perm['group_id'] == 'coordinator_double' || $cntrl_perm['group_id'] == 'client_manager_id')){
					/**
					 * @since 9.11.2009
                     * @author Sol
                     * @abstract slouceni pristupu COO a CM k reportum
					 *
                    
                    $criteria['or'] = array(
						$model_class_coo.'.'.'coordinator_id' => $logged_user['CmsUser']['id'],
						$model_class_coo.'.'.'coordinator_id2' => $logged_user['CmsUser']['id'],
						$model_class_coo.'.'.'client_manager_id' => $logged_user['CmsUser']['id']
					);
				}
				else if(isset($cntrl_perm['group_id']) && $cntrl_perm['group_id'] <> ''){
					$criteria[$model_class_coo.'.'.$cntrl_perm['group_id']]= $logged_user['CmsUser']['id'];
				}
			}
		} else {
			$this->controller->redirect('/');
		}
        
        */
		$this->criteria = $criteria;
        
        $this->controller->set('renderSetting',$this->renderSetting);
        
	}
    
	
	
	function replace_fnc($string,$Session){
		$logged_user = $this->Session->read('logged_user');
		$string = strtr($string,array(
			'#CmsUserId#' => $logged_user['CmsUser']['id'],
			'#CmsGroupId#' => $logged_user['CmsGroup']['id'],
			'#CURRENT_YEAR#' => defined('CURRENT_YEAR')?CURRENT_YEAR:'',
			'#CURRENT_MONTH#' => defined('CURRENT_MONTH')?CURRENT_MONTH:'',
			'#CUSTOM_ID#' => defined('CUSTOM_ID')?CUSTOM_ID:'',
		));

		return $string;
	}
		
	function foreach_SQLcondition($session, $array = array()){
		if (is_array($array)){
			foreach($array as $key=>$item){
				$array[$key] = $this->foreach_SQLcondition($session,$item);
			}
		} else {
			$array = $this->replace_fnc($array,$session);
		}
		return $array;
	}	
	
	function filtration($name){
		$filtr = array();
		
		if (isset($this->controller->renderSetting[$name]['SQLcondition'])):
			$plc = $this->foreach_SQLcondition($this->Session,$this->controller->renderSetting[$name]['SQLcondition']);
			$filtr = $plc;
		endif;
        
		if (isset($this->controller->params['url'])){
			foreach($this->controller->params['url'] as $key => $value){
				if (substr($key,0,11) == 'filtration_'){
			
                    if ($value == '')
						unset($this->controller->params['url'][$key]);
					else {
						if(substr($key,-3)=="mlm"){
							$filtr['AND']=array(
								'ConnectionMlmRecruiter.created <='=>$value,
								array('OR'=>array('ConnectionMlmRecruiter.closed >='=>$value,'ConnectionMlmRecruiter.closed'=>'0000-00-00'))
							);
						}
						if(substr($key,-5)=="cmkoo"){
							$params = explode('-',substr($key,11));
							$model = $params[0];
							$cols = $params[1];
							$cons = explode('|',$cols);
							$str = array();
							foreach($cons as $k=>$item){
								if (!empty($item))
									$str[$model.'.'.$item] = $value;
							}
							if (count($str) > 1)
								$filtr['AND']['OR'] = $str;
							else 
								$filtr = am($filtr, $str);
						}
						else if(substr($key,-3)=="cce"){
							//nic nedelej
						}
                        else if(substr($key,-7)=="int_ccr" && isset($filtr['OR'])){
						   $filtr['OR'] = am($filtr['OR'],array('ConnectionClientRecruiter.cms_user_id = '.$value));
						}
                        else if($value == "NOTNULL"){
							$filtr[strtr(substr($key,11),array('-'=>'.')).' <> '] = '';
						}
                        else if(substr($value,0,8) == "SUBQUERY"){
                            if(isset($this->controller->subQuery[substr($value,9)]))
							   $filtr=$this->controller->subQuery[substr($value,9)];
						}
                        /**
                         * funkce pro checkbox
                         * pokud je on a neni nastaven dalsi parametr
                         * hleda se value 1
                         */
                        else if($value == "on"){
							$value = 1;
                            //pozadujeme nejaky sloupce jako null
                            if(substr($key,-7) == "NOTNULL"){
                                $key =  substr($key,0,-8);  
                                $filtr[strtr(substr($key,11),array('-'=>'.')).' <> '] = ''; 
                            }
                            else if(substr($key,-8) == "NOTNULL2"){
                                $key =  substr($key,0,-9);  
                                $filtr[strtr(substr($key,11),array('-'=>'.')).' <> '] = '0000-00-00'; 
                            }
						}
						else if(substr($key,-5)=="month"){
							$value = ($value < 10? '0'.$value : $value);
							$filtr['AND']=array('MONTH(SmsMessage.created)'=>$value);
						}
						else if(substr($key,-4)=="year"){
							$filtr['AND']=array('YEAR(SmsMessage.created)'=>$value);
						}
						else if (strrpos($key,"|")){
								$key = strtr(substr($key,11),array('-'=>'.'));
								$cons = explode('|',$key);
								$str = array();
								foreach($cons as $k=>$item){
									if (!empty($item))
										$str[$item . ' LIKE'] = '%'.$value.'%';
								}
								if (count($str) > 1)
									$filtr['AND']['OR'] = $str;
								else 
									$filtr = am($filtr, $str);
							} else {
								if (substr($key,-5) == 'month')
									$filtr['MONTH('.strtr(substr($key,11,-6),array('-'=>'.')).')'] = $value;
								elseif (substr($key,-4) == 'name')
									$filtr[strtr(substr($key,11),array('-'=>'.')).' LIKE'] = '%'.$value.'%';
								elseif (substr($key,-4) == 'from')
									$filtr[strtr(substr($key,11,-5),array('-'=>'.')).' >= '] = $value;
								elseif (substr($key,-3) == '-to')
									$filtr[strtr(substr($key,11,-3),array('-'=>'.')).' <= '] = $value;
								else { 
									$filtr[strtr(substr($key,11),array('-'=>'.'))] = (strpos($value,'|') === false)?$value:explode('|',$value);
								}
							}
					}
				}
			}
		}
		return $filtr;
	}
    
    
    
    function generate_td($item, $td, $viewVars = array()){

        App::import('Helper', 'Fastest');
        $loadHelper = 'FastestHelper';
        $this->Fastest = new $loadHelper();   
        
		$value =  @$item[$td['model']][$td['col']];
		switch($td['type']){
			case 'text':
				if (!empty($td['fnc'])){
					list($fnc, $params) = explode('#', $td['fnc']);
					return $this->Fastest->$fnc($value,$params);
				} else {
					return $value;
				}
				break;
			case 'procenta':
				return $value .(empty($value)?'':'%');
				break;
			case 'download':
				return ((($value!='')?'<a href="'.$td['fnc'].'/null/null/'.$value.'">OtevĹ™Ă­t soubor</a>':''));
				break;
			
			case 'datetime':
				return ($value!='0000-00-00 00:00:00' && $value != null)?$this->Fastest->czechDateTime($value,$td['fnc']):'';
				break;
			case 'date':
				return ($value !='0000-00-00' && $value !='0000-00-00 00:00:00' && $value != null)?$this->Fastest->czechDate($value, $td['fnc']):'';
				break;
			case 'var':
				return @$viewVars[$td['fnc']][$value];
				break;
			case 'viewVars':
				return @$viewVars[$td['fnc']][$value];
				break;
			case 'hidden':
				return;
				break;
			case 'money':
				return number_format($value, 0, ',', ' ');;
				break;
			case 'pocet_dni_po_splatnosti':
				$po_splatnosti = (strtotime(date('Y-m-d')) - strtotime($item[$td['model']]['datum_splatnosti'])) / 3600 /24;
				if ($po_splatnosti > 0) {
					if ($item[$td['model']]['datum_uhrady'] != '0000-00-00')
						return 'Uhrazeno';
					else 
						return $po_splatnosti;
				} else
					return ;
				break;
		}
	}
}
?>