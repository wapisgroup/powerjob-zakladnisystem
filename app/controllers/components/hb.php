<?php
/**
 * HB componenta
 * @author Jakub Matuš
 * @created 22.2.10
 * 
 * komponenta pro vytvoreni importniho souboru .txt pro Slovenskou Sporitelnu
 */
class HbComponent{
    private $out;
    public  $testing = false;
    private $count_payments = 0;  // pocet prikazu
    private $data;

    private $error = array(); //pole chybových hlášek
    
    // Constructor
	function Hb() {
        App::import('Vendor', 'dataItem2', array('file' => 'hb'.DS.'data_item.php')); 

    }
    
    /**
     * funkce pro vytvorani souboru
     */
    public function create_file(){
         if($this->testing === true){  
            echo $this->out;
         }
         else{  
            $dir = "ss/";
            $filename = date('Ymd_His').".txt";
            
             //write new data to the file, along with the old data
            $handle = fopen($dir.$filename, "w+");
                if (fwrite($handle, $this->out) === false) {
                    return "Cannot write to text file. <br />";          
                }
            fclose($handle); 
            
            return $filename;
         }    
    }
    
    /**
     * funkce pro nastaveni promennych
     */
    public function __set($var,$value){
        $this->$var = $value;
    }
    
    /**
     * funkce pro ziskani hodnot promennych
     */
    public function __get($var){
        return $this->$var;
    }
    
    
    /**
     * funkce kontoluje spravnost pridani jednotliveho prikazu
     * a pridava do datove vetve pokud je vse v poradku
     */
    public function add_data($values = array()){
        if(!empty($values)){
            if($values['account'] != 0 && $values['amount'] != 0 && $values['code_bank'] != ''){              
                
                $this->out .= $this->data->add_data(($this->count_payments+1),$values['date_to_pay'],(int)$values['amount'],$values['code_bank'],$values['prefix_account'],$values['account'],$values['message_commiter']);
                
                //inkremetace poctu prikazu
                $this->count_payments ++;
            }
            else {
                /**
                 * castka, cislo uctu nebo kod banky bylo nulove
                 */
                 $this->error[] = "Příkaz č.".($this->count_payments+1).". Částka(".$values['amount']."),číslo účtu(".$values['account'].") nebo kod banky(".$values['code_bank'].") bylo nulové.";
            }     
        }
        else{
            $this->error[] = "Příkaz č.".$this->count_payments.". Pole hodnot pro přidání je prázdné";
        }
        
        
    }

    
   /**
    * vytvoreni sablony pro prikazy
    * @param $prefix - prefix cisla uctu prikazce
    * @param $account_committer - cislo prikazce
    * @param $vs - variabilni symbol
    * cislo prikazce (AT SK) 
    */
    public function make_data_template($prefix,$account_committer,$vs){
        $this->data = new DataItem2($prefix,$account_committer,$vs);  
    }
    
    
   /**
    * funkce pro zjisteni zda byla zjistena nejaka chyba
    * pokud ano tak vrat pole chybovych hlasek, jinak vrat true 
    */
    public function get_error(){
        if(empty($this->error))
            return true;
        else
            return $this->error;        
    }
    
}
?>