<?php
App::import('Vendor', 'phpmailer', array('file' => 'phpmailer'.DS.'class.phpmailer.php'));
//Configure::write('debug',1);
class EmailComponent{
    var $from         = 'robot@power-job.cz';
    var $fromName     = 'Robot PowerJob';
    var $smtpUserName = 'robot@power-job.cz';  	// SMTP username
    var $smtpPassword = 'BeNDeR78'; 		// SMTP password
    var $smtpHostNames=  'smtp.savana.cz';  // specify main and backup server
    var $text_body = null;
    var $html_body = null;
    var $to = null;
    var $toName = null;
    var $subject = null;
    var $cc = null;
    var $bcc = null;
    var $template = null;
    var $attachments = null;
    var $layout = 'email';
    private $company_data = array(); // udaje o spolecnosti, name,CM,COO,COO2,SM
    private $create_log = false; // uloz log o emailu zatim primitivne
    private $log_data = array(); // nastaveni pro log

    var $controller;

    function startup( $controller ) {
        $this->controller = $controller;
        $this->mail = new PHPMailer();
    }

    function bodyHTML() {
        ob_start();
        $temp_layout 				= 	$this->controller->layout;
        $this->controller->layout 	= 	$this->layout;
        $this->controller->render($this->template);
        $mail 					= 	ob_get_clean();
        $this->controller->layout 	= 	$temp_layout;
        return $mail;
    }

    function AddStringAttachment($string, $filename, $encoding = "base64", $type = "application/octet-stream") {
        $this->mail->AddStringAttachment($string, $filename, $encoding, $type);
    }

    function addAttachFromFunc($filename= 'attach.txt', $fn = null, $layout = '', $encoding = "base64", $type = "application/octet-stream"){
        $ex = explode('/',ltrim($fn,'/'));
        $action = $ex[1];
        $params = $ex[2];

        ob_start();
        $temp_layout = $this->controller->layout;
        $this->controller->layout = $layout;
        $this_data = $this->controller->data;
        unset($this->controller->data);

        $this->controller->priloha = true;
        echo $this->controller->setAction($action,array($params));
        $attach = ob_get_contents();
        ob_end_clean();

        $this->controller->layout = $temp_layout;
        $this->controller->data = $this_data;
        unset($this_data);
        $this->AddStringAttachment($attach,$filename,$encoding,$type);
    }

    function addAttachFromView($filename = 'attach.txt',$view = null, $layout = 'email',$encoding = "base64", $type = "application/octet-stream"){
        ob_start();
        $temp_layout = $this->controller->layout;
        $this->controller->layout = $layout;
        $this->controller->render($view);
        $attach = ob_get_clean();
        $this->controller->layout = $temp_layout;
        $this->AddStringAttachment($attach,$filename,$encoding,$type);
    }

    function attach($filename, $asfile = '') {
        if (empty($this->attachments)) {
            $this->attachments = array();
            $this->attachments[0]['filename'] = $filename;
            $this->attachments[0]['asfile'] = $asfile;
        } else {
            $count = count($this->attachments);
            $this->attachments[$count+1]['filename'] = $filename;
            $this->attachments[$count+1]['asfile'] = $asfile;
        }
    }

    function addAttachFromFtp($filename,$destroyUploaded = true){
        $filename = ltrim($filename,'/');
        $ex = explode('/',$filename);
        $filename = $ex[count($ex)-1];
        unset($ex[count($ex)-1]);
        $path = implode('/',$ex);
        if($destroyUploaded === true)
            $path = strtr($path,array('uploaded'=>''));

        $conn_id = ftp_connect($this->ftp_host) or die("Couldn't connect to $this->ftp_host");
        if (@ftp_login($conn_id, $this->ftp_user, $this->ftp_heslo)) {
            $temp_file = tmpfile();
            ftp_chdir($conn_id, $path);
            if (ftp_fget($conn_id, $temp_file, $filename, FTP_BINARY, 0)) {
                fseek($temp_file, 0);
                $fs = ftp_size($conn_id, $filename);
                $string =  fread($temp_file,$fs);
                $this->AddStringAttachment($string, $filename);
            }
        }

        ftp_close($conn_id);
    }

    function send_from_template($template_id = null, $recipient_list = array(), $replace_list = array(),$attachment_list = array()){
        $this->mail->IsSMTP();            // set mailer to use SMTP
        $this->mail->SMTPAuth 	= true;     // turn on SMTP authentication
        $this->mail->Host   	= $this->smtpHostNames;
        $this->mail->Username 	= $this->smtpUserName;
        $this->mail->Password 	= $this->smtpPassword;
        $this->mail->From     	= 'robot@power-job.cz';
        $this->mail->FromName 	= 'Robot PowerJob';


        foreach ($recipient_list as $to):
            $this->mail->AddAddress($to);
        endforeach;

        $this->mail->CharSet  = 'UTF-8';
        $this->mail->WordWrap = 50;  // set word wrap to 50 characters

        if (!empty($attachment_list)) {
            foreach ($attachment_list as $attachment) {
                if (empty($attachment['asfile'])) {
                    $this->mail->AddAttachment($attachment['filename']);
                } else {
                    $this->mail->AddAttachment($attachment['filename'], $attachment['asfile']);
                }
            }
        }

        $this->mail->IsHTML(true);

        $this->controller->loadModel('MailTemplate');
        $MailTemplate = new MailTemplate();
        $sablona = $MailTemplate->read(array('name','text'),$template_id);
        unset($MailTemplate);
        print_r($replace_list);
        echo $sablona['MailTemplate']['text'];
        $this->mail->Subject = strtr($sablona['MailTemplate']['name'],$replace_list);
        $this->mail->Body = '<p>'.strtr($sablona['MailTemplate']['text'],$replace_list).'</p>';

        $result = $this->mail->Send();
        $this->mail->attachment = array();

        if($result == false ){
            $result = $this->mail->ErrorInfo;
            die($result);
        }
        return $result;
    }


    function send_without_template($options = array(),$recipient_list = array(), $replace_list = array(),$attachment_list = array(),$attachment_path = null){
        $this->mail->IsSMTP();            // set mailer to use SMTP
        $this->mail->SMTPAuth 	= true;     // turn on SMTP authentication
        $this->mail->Host   	= $this->smtpHostNames;
        $this->mail->Username 	= $this->smtpUserName;
        $this->mail->Password 	= $this->smtpPassword;
        $this->mail->From     	= $options['from'];
        $this->mail->FromName 	= $options['from_name'];

        $attachment_path = (isset($options['attach_path']) ? $options['attach_path'] : '');

        foreach ($recipient_list as $to):
            $this->mail->AddAddress($to);
        endforeach;

        $this->mail->CharSet  = 'UTF-8';
        $this->mail->WordWrap = 50;  // set word wrap to 50 characters


        if (!empty($attachment_list)) {
            foreach ($attachment_list as $attachment) {
                if (empty($attachment['asfile'])) {
                    $this->mail->AddAttachment($attachment_path.$attachment['filename']);
                } else {
                    $ext = explode('.',$attachment_path.$attachment['filename']);
                    $this->mail->AddAttachment($attachment_path.$attachment['filename'], $attachment['asfile'].'.'.$ext[count($ext)-1]);
                }
            }
        }






        $this->mail->IsHTML(true);

        $this->mail->Subject = strtr($options['subject'],$replace_list);
        $this->mail->Body = '<p>'.strtr($options['text'],$replace_list).'</p>';

        $result = $this->mail->Send();
        $this->mail->attachment = array();

        if($result == false ){
            $result = $this->mail->ErrorInfo;
            die($result);
        }
        return $result;
    }

    function send(){
        $this->mail->IsSMTP();            // set mailer to use SMTP
        $this->mail->SMTPAuth = true;     // turn on SMTP authentication
        $this->mail->Host   = $this->smtpHostNames;
        $this->mail->Username = $this->smtpUserName;
        $this->mail->Password = $this->smtpPassword;
        $this->mail->From     = $this->from;
        $this->mail->FromName = $this->fromName;

        foreach ($this->to as $to):
            $this->mail->AddAddress($to);
        endforeach;
        $this->mail->AddReplyTo($this->from, $this->fromName );

        $this->mail->CharSet  = 'UTF-8';
        $this->mail->WordWrap = 50;  // set word wrap to 50 characters

        if (!empty($this->attachments)) {
            foreach ($this->attachments as $attachment) {
                if (empty($attachment['asfile'])) {
                    $this->mail->AddAttachment($attachment['filename']);
                } else {
                    $this->mail->AddAttachment($attachment['filename'], $attachment['asfile']);
                }
            }
        }

        $this->mail->IsHTML(true);  // set email format to HTML

        $this->mail->Subject = $this->subject;
        $this->mail->Body    = $this->bodyHTML();
        //$this->mail->AltBody = $this->bodyText();

        $result = $this->mail->Send();
        $this->mail->attachment = array();

        if($result == false )
            $result = $this->mail->ErrorInfo;
        return $result;
    }


    /**
     * 	nova funkce poslani emailove sablony
     * @param $template_id - id sablony
     * @param $recipient_list - pole prijemcu z kodu
     * @param $replace_list - pole pro prepsani
     * @param $email_podpis - string podpisu
     * @param $attachment_list - pole priloh
     * @created 22.1.10
     * @author Jakub Matus
     */
    function send_from_template_new($template_id = null, $recipient_list = array(), $replace_list = array(), $email_podpis=null,$attachment_list = array()){
        $this->mail->IsSMTP();            // set mailer to use SMTP
        $this->mail->SMTPAuth 	= true;     // turn on SMTP authentication
        $this->mail->Host   	= $this->smtpHostNames;
        $this->mail->Username 	= $this->smtpUserName;
        $this->mail->Password 	= $this->smtpPassword;
        $this->mail->From     	= 'robot@power-job.cz';
        $this->mail->FromName 	= 'Robot PowerJob';

        // nascteni sablony
        $this->controller->loadModel('MailTemplate');
        $MailTemplate= new MailTemplate();
        $sablona = $MailTemplate->read(array('name','text','status'),$template_id);
        unset($MailTemplate);

        if($sablona['MailTemplate']['status'] == 0)
            return false;


        /**
         * nacteni prijemcu
         */
        $this->controller->loadModel('ConnectionMailTemplate');
        $ConnectionMailTemplate =new ConnectionMailTemplate();
        $to_list = $ConnectionMailTemplate->find('all',array(
            'fields'=>array('id','cms_user_id','cms_group_id','ignore','company_connection'),
            'conditions'=>array(
                'mail_template_id'=>$template_id,
                'kos'=>0
            )
        ));
        unset($ConnectionMailTemplate);

        /**
         * rozrazeni do poli pro cms_user a cms_group
         * a odstraneni nulovych value
         */
        $to_list_cms_user_id = self::unset_zero_value(Set::extract($to_list,'/ConnectionMailTemplate/cms_user_id'));
        $to_list_cms_group_id = self::unset_zero_value(Set::extract($to_list,'/ConnectionMailTemplate/cms_group_id'));

        /**
         * pokud existuji nejaci prijemci
         * najdi jejich emaily
         */
        if(!empty($to_list_cms_user_id) || !empty($to_list_cms_group_id)){
            $condition = array();

            /**
             * SM, CM, COO mohou byt v propojeni tedy vybrat coo jen spolecnosti s kterou jsou napojeni
             */
            $company_connection_list = Set::combine($to_list,'{n}.ConnectionMailTemplate.id','{n}.ConnectionMailTemplate.cms_group_id','{n}.ConnectionMailTemplate.company_connection');

            /**
             * pokud ma nejak skupina propojeni tak ji odstran z group listu pro vyber emailu
             * budou se nacitat vzdy jen email daneho CM, COO, SM od daneho podniku
             */
            if(!empty($company_connection_list[1])){
                $to_list_cms_group_id = array_diff($to_list_cms_group_id, $company_connection_list[1]);

                /**
                 * jsou nastaveny data pro konkretni firmu
                 * nastav id pro load jejich emailu
                 */
                if(!empty($this->company_data)){
                    foreach($company_connection_list[1] as $key=>$val){
                        switch($val){
                            case 2: //SM
                                if(isset($this->company_data['self_manager_id']))
                                    $to_list_cms_user_id[] = $this->company_data['self_manager_id'];
                                break;
                            case 3: //CM
                                if(isset($this->company_data['client_manager_id']))
                                    $to_list_cms_user_id[] = $this->company_data['client_manager_id'];
                                break;
                            case 4: //COO, COO2
                                if(isset($this->company_data['client_manager_id']))
                                    $to_list_cms_user_id[] = $this->company_data['client_manager_id'];
                                if(isset($this->company_data['coordinator_id']))
                                    $to_list_cms_user_id[] = $this->company_data['coordinator_id'];
                                if(isset($this->company_data['coordinator_id2']))
                                    $to_list_cms_user_id[] = $this->company_data['coordinator_id2'];
                                break;
                            case 18: //MR
                                if(isset($this->company_data['manazer_realizace_id']))
                                    $to_list_cms_user_id[] = $this->company_data['manazer_realizace_id'];
                                break;
                            case 30: //projekt manager
                                if(isset($this->company_data['project_manager_id']))
                                    $to_list_cms_user_id[] = $this->company_data['project_manager_id'];
                                break;
                            case 31: //MR
                                if(isset($this->company_data['office_manager_id']))
                                    $to_list_cms_user_id[] = $this->company_data['office_manager_id'];
                                break;
                            case 32: //Spravce Majetku
                                if(isset($this->company_data['spravce_majetku_id']))
                                    $to_list_cms_user_id[] = $this->company_data['spravce_majetku_id'];
                                break;
                        }
                    }

                }

            }



            /**
             * Vyjimka, ktera nemusi byt nastavena v sablone jako prijemce
             * reditel strediska,spravce majektu projektu
             */
            if(isset($this->company_data['reditel_strediska_id']) && $this->company_data['reditel_strediska_id'] != '')
                $to_list_cms_user_id[] = $this->company_data['reditel_strediska_id'];

            if(isset($this->company_data['project_spravce_majetku_id']) && $this->company_data['project_spravce_majetku_id'] != '')
                $to_list_cms_user_id[] = $this->company_data['project_spravce_majetku_id'];

            //odstraneni prazdnych id
            $to_list_cms_user_id = self::remove_array_empty_values($to_list_cms_user_id,true);
            $to_list_cms_group_id = self::remove_array_empty_values($to_list_cms_group_id,true);

            /**
             * condition pro cms_user
             */
            if(!empty($to_list_cms_user_id))
                $condition[] = 'id IN('.implode(',',$to_list_cms_user_id).')';

            /**
             * condition pro cms group
             */
            if(!empty($to_list_cms_group_id)){

                if(!empty($condition)){
                    $condition[0] = '('.$condition[0].' OR cms_group_id IN('.implode(',',$to_list_cms_group_id).') )';
                }
                else
                    $condition[] = 'cms_group_id IN('.implode(',',$to_list_cms_group_id).') ';
            }

            if(!empty($condition)){//nikdy to nemuze jit vsem
                $this->controller->loadModel('CmsUser');
                $CmsUser = new CmsUser();
                $cms_email_list = $CmsUser->find('list',array(
                    'conditions'=>am(
                        array(
                            'kos'=>0,
                            'status'=>1,
                            'id <> '=>-1,
                        ),
                        $condition
                    ),
                    'fields'=>array('CmsUser.id','CmsUser.email'),
                    'group'=>array('email')
                ));

                /**
                 * odstraneni adress ktere maji byt odstraneni
                 */
                if(!empty($to_list_cms_group_id)){
                    /**
                     * ignorovane id
                     */
                    $ignore_list = Set::combine($to_list,'{n}.ConnectionMailTemplate.cms_user_id','{n}.ConnectionMailTemplate.id','{n}.ConnectionMailTemplate.ignore');

                    if(!empty($ignore_list[1]))
                        $cms_email_list = array_diff_key($cms_email_list, $ignore_list[1]);
                }

                //spojeni skupinovych emailu a zadanych jednotlivcu
                $recipient_list = am($recipient_list,$cms_email_list);
            }
        }

        //pr($recipient_list);
        //unikatní
        $recipient_list = array_unique($recipient_list);
        //prijemci
        foreach ($recipient_list as $to):
            $this->mail->AddAddress($to);
        endforeach;

        $this->mail->CharSet  = 'UTF-8';
        $this->mail->WordWrap = 50;  // set word wrap to 50 characters

        //prilohy
        if (!empty($attachment_list)) {
            foreach ($attachment_list as $attachment) {
                if (empty($attachment['asfile'])) {
                    $this->mail->AddAttachment($attachment['filename']);
                } else {
                    $this->mail->AddAttachment($attachment['filename'], $attachment['asfile']);
                }
            }
        }

        $this->mail->IsHTML(true);

        //$this->mail->Subject = $sablona['MailTemplate']['name'];
        $this->mail->Subject = strtr($sablona['MailTemplate']['name'],$replace_list);
        $this->mail->Body = '<p>'.strtr($sablona['MailTemplate']['text'],$replace_list).'</p>';

        if($this->create_log == true && isset($this->log_data) && !empty($this->log_data)){
            if(isset($this->log_data['model']) && $this->log_data['model'] != ''){
                $_model = $this->log_data['model'];
                $this->controller->loadModel($_model);
                $this->controller->$_model->save(array(
                    $this->log_data['col_name_parent_id']=>$this->log_data['parent_id'],
                    $this->log_data['col_name_text']=>$this->mail->Body,
                    'mail_template_id'=>$template_id
                ));
                unset($this->controller->$_model);
            }
        }

        $result = $this->mail->Send();

        $this->mail->attachment = array();

        if($result == false )
            $result = $this->mail->ErrorInfo;
        return $result;
    }

    /**
     * funkce odstrani nulove polozky
     */
    private function unset_zero_value($array = array()){
        foreach($array as $key=>$value){
            if($value == 0){
                unset($array[$key]);
            }
        }

        return $array;
    }

    /**
     * funkce nastavuje promennou company a jeji udaje
     */
    public function set_company_data($data){
        $this->company_data = $data;
    }

    //vyprazdeni dat
    public function clear_company_data(){
        $this->company_data = array();
    }

    /**
     * funkce nastavuje kam se ma email logovat
     */
    public function set_log($data){
        $this->create_log = true;
        $this->log_data = $data;
    }

    private function remove_array_empty_values($array, $remove_null_number = true)
    {
        $new_array = array();

        $null_exceptions = array();

        foreach ($array as $key => $value)
        {
            $value = trim($value);

            if($remove_null_number)
            {
                $null_exceptions[] = '0';
            }

            if(!in_array($value, $null_exceptions) && $value != "")
            {
                $new_array[] = $value;
            }
        }
        return $new_array;
    }
}
?>