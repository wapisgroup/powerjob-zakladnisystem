<?php
/**
 * @author Jakub Matus
 * @created 17.2.2010
 * 
 * komponenta ktera bude zdruzovat funkce pro Employees Controller - hlavne pro Odpracovane hodiny
 * tak abychom zpřehlednili controller a oddělili funkce od controlu a jeho použití samotného
 * 
 */
//Configure::write('debug',1);
class EmployeesComponent extends Object{
    private $connection_id;
    public  $dochazka_model = 'ClientWorkingHour';
    public  $connection_column = 'connection_client_requirement_id';
    private $from;
    private $year; 
    private $month;
    private $this_data;
    private $dovolena = array();
    public $permission;
    public $logged_user;
    private $h1000 = false;
    private $type;
    private $warning_about_cw_tempalte_miss = false;
    private $odmeny = array( 
        1 => array(
            'label'=>'Odměna 1',
            'max'=>0.00
        ),
        2 => array(
            'label'=>'Odměna 2',
             'max'=>0.00
        ),
        3 => array(
            'label'=>'Odměna 3',
             'max'=>0.00
        )
    );

	function startup(&$controller) {
		$this->controller =$controller;
	}
    
    public function set($var,$value){
        $this->$var = $value;
    }
    
    public function set_variables_for_view($type = 0){
        $this->type = $type;
        if($type == 0){
          $this->controller->set('allow_transfer_dohoda_to_cash',self::dohoda_to_cash()); 
          $this->controller->set('allow_ignore_dpc',self::allow_ignore_dpc());   
          $this->controller->set('permission_alow',self::permision_to_edit_mi_and_wp());
          $this->controller->set('opp_list',self::drawback_for_opp());
          $this->controller->set('company_cw_template_list',self::company_cw_template_list());
          $this->controller->set('warning_about_cw_tempalte_miss',$this->warning_about_cw_tempalte_miss);
        }
        else if($type == 2){//int. dochazky zam.
            $this->controller->set('npp_list',self::neplneni_prac_povinosti());
            $this->controller->set('mzda_setting',self::mzda_setting());
            self::get_drawbacks_from_activity(); 
            self::get_drawbacks_from_autorization();  
        }
        
        //spolecne
        $this->controller->set('odmeny',$this->odmeny);
        $this->controller->set('zbyva_dovolene',self::zbyva_dovolene());
        $this->controller->set('dovolena',$this->dovolena);
        $this->controller->set('permission_to_editation_max',self::access_to_max());
    }
    
    public function dohoda_to_cash(){
       if(isset($this->permission['allow_transfer_dohoda_to_cash']) 
        && $this->permission['allow_transfer_dohoda_to_cash'] == 1
        && (in_array($this->this_data['money_item_name'],array('0100','0101','0111')))
       ){
           return true;    
       }
       else
           return false;
    }
    
    public function allow_ignore_dpc(){
       if(isset($this->permission['allow_ignore_dpc']) 
        && $this->permission['allow_ignore_dpc'] == 1
        && ($this->this_data['money_item_name'] == '0010')
       ){
           return true;    
       }
       else
           return false;
    }
    
    /**
     * nastaveni pole odmen
     * @param data - obsha nastaveni pro odmeny
     * @param $spec - muzeme specifikovat dat pouze pro konkretni odmenu, tedy 1 nebo 2
     * @param $col - sloupec ktery chceme nastavit pro konkretni sloupec
     */
    public function set_new_odmeny($data, $spec = false, $col = false){
         if($spec && ($spec == 1 || $spec == 2)){
            if($col)
                $this->odmeny[$spec][$col] = $data;
            else    
                $this->odmeny[$spec] = $data;
         }
         else
            $this->odmeny = $data;  
    }
    


    /**
     * funkce ktera kontroluje zmenu udaju u ulozeni autorizovane dochazky
     * @return string text
     */
	public function check_changes_in_wh($data){
	    $text = '';
       
        if(!isset($this->controller->ClientWorkingHour))
            $this->controller->loadModel('ClientWorkingHour');
        
        /**
         * polozky ktere se maji hlidat, je nutne je vytahnout ktere jsou v db
         * a porovnat v tech ktere ukladame
         * nastavujeme 
         *  nazec atributu
         *  text ktery se ma zobrazit pri zmene
         */
        $whats_checks = array(
            'celkem_hodin' => 'Celkem hodin bylo změněno',
            'salary_per_hour' => 'Na hodinu celkem se změnilo',
            'stop_payment' => 'Pozastavení mzdy se změněno'
        ); 
        
        
        $is_saves_values = $this->controller->ClientWorkingHour->read(array_keys($whats_checks),$data['ClientWorkingHour']['id']);   
       
        /**
         * pokud jsou hodnoty rozdilne, tak vypis text zmeny
         */
	    foreach($whats_checks as $att=>$msg){
	       if(is_double($is_saves_values['ClientWorkingHour'][$att]) && (round($is_saves_values['ClientWorkingHour'][$att],3) != round($data['ClientWorkingHour'][$att],3))){
	         if($att != 'stop_payment')
                $text .= $msg.' z '.$is_saves_values['ClientWorkingHour'][$att].' na '.$data['ClientWorkingHour'][$att].'<br/>';
             else   
                $text .= $msg.' z '.$this->controller->ano_ne_checkbox[$is_saves_values['ClientWorkingHour'][$att]].' na '.$this->controller->ano_ne_checkbox[$data['ClientWorkingHour'][$att]].'<br/>';
	       }
	       else if($is_saves_values['ClientWorkingHour'][$att] != $data['ClientWorkingHour'][$att]){  
             if($att != 'stop_payment')
                $text .= $msg.' z '.$is_saves_values['ClientWorkingHour'][$att].' na '.$data['ClientWorkingHour'][$att].'<br/>';
             else   
                $text .= $msg.' z '.$this->controller->ano_ne_checkbox[$is_saves_values['ClientWorkingHour'][$att]].' na '.$this->controller->ano_ne_checkbox[$data['ClientWorkingHour'][$att]].'<br/>';
           }
        }
        
        //pocatecni hlaska bloku
        if($text != '')
            $text = '<br />Byly zaznamenány následujicí změny:<br />'.$text;
        
        return $text;
    
    }
    
    
    /**
     * POKUD je nastaveno v permission povoleni pro editaci forem a profesi na dochazek
     * nastav skupinu uzivatelu ktere to je povoleno
     */
    function permision_to_edit_mi_and_wp(){
       if(isset($this->permission['editable_money_item_workposition']) && $this->permission['editable_money_item_workposition'] == 1){
        return array(0 => $this->logged_user['CmsGroup']['id']);     
       }
       else
        return array();
    }
    
    
    function zbyva_dovolene(){
        //Configure::write('debug',1);
        $koeficient = 0.0833;
        $pocet_dovolene_v_akutalni_dochazce = $this->this_data[$this->dochazka_model]['dovolena'];
        $pocet_oh_v_akutalni_dochazce = $this->this_data[$this->dochazka_model]['working_days_count'];
        
        if(!isset($this->controller->{$this->dochazka_model}))
            $this->controller->loadModel($this->dochazka_model);
        
        $f = $this->controller->{$this->dochazka_model}->find('first',array(
            'conditions'=>array(
                $this->connection_column=>$this->connection_id
            ),
            'fields'=>array(
                'SUM(dovolena) as sum_dovolene, SUM(working_days_count) as sum_odpracovanych_dni'
            ),
            'group'=>$this->connection_column
        ));
            
        $sum_dovolene = $sum_odpracovanych_dni = 0;        
        if(isset($f) && !empty($f)){
            $zbyva_dovolene = floor($f[0]['sum_odpracovanych_dni']*$koeficient);
            $sum_dovolene = $f[0]['sum_dovolene'];
            $sum_odpracovanych_dni = $f[0]['sum_odpracovanych_dni'];
            $zbyva_dovolene = $zbyva_dovolene - ($f[0]['sum_dovolene']); 
        }
        else
            $zbyva_dovolene = 0;
            
        //defaultni pocet vybrane dovolene    
        $this->dovolena = array(
            'zbyva_dovolene' => ($zbyva_dovolene + $pocet_dovolene_v_akutalni_dochazce),  
            'd_sum_odpracovanych_dni' => ($sum_odpracovanych_dni - $pocet_oh_v_akutalni_dochazce),  
            'koeficient'=>$koeficient
        );  
        
        return $zbyva_dovolene;
              
    }
    
    /**
     * Kontrolni funkce ktera ma za ukol,
     * zkontrolovat zda dana connection pro mesic a rok uz nema ulozenou dochazku.
     * Pokud ano vyhod chybu. Timto by meli byt osetreny duplicitni dochazky
     */
    public function if_exist_client_working_hour(){
        if(!isset($this->controller->{$this->dochazka_model}))
            $this->controller->loadModel($this->dochazka_model);
        
        $f = $this->controller->{$this->dochazka_model}->find('first',array(
            'conditions'=>array(
                $this->dochazka_model.'.'.$this->connection_column=>$this->connection_id,
                $this->dochazka_model.'.year'=>$this->year,
                $this->dochazka_model.'.month'=>$this->month
            ),
            'fields'=>array('id')
        ));
        
        if($f)
            return true;
        else
            return false;    
    }
    
    /**
     * Nacteni seznamu pridelenych OPP v danem mesici a roce
     * a nastaveni hodnoty srazky za tyto OPP ktere meli narok = ne
     */
    private function drawback_for_opp(){
        //Configure::write('debug',2);
         if(!isset($this->controller->ConnectionClientOpp))
            $this->controller->loadModel('ConnectionClientOpp');
        
        $this->controller->ConnectionClientOpp->bindModel(array(
            'belongsTo'=>array('ConnectionClientRequirement')
        ));
        $f = $this->controller->ConnectionClientOpp->find('all',array(
            'conditions'=>array(
					'ConnectionClientOpp.kos'=>0,
                    'ConnectionClientOpp.connection_client_requirement_id'=>$this->connection_id,
                    'OR'=>array(
                        array(
                            'ConnectionClientOpp.narok'=>'ne',
                			'(DATE_FORMAT(ConnectionClientOpp.created,"%Y-%m"))' => $this->year.'-'.$this->month,   
                        ),
                        array(
                            'ConnectionClientOpp.narok'=>'ano',
                            'ConnectionClientOpp.srazka_ze_mzdy'=>1,
                            '(DATE_FORMAT(ConnectionClientRequirement.to,"%Y-%m"))' => $this->year.'-'.$this->month,   
                        )
                    )    
                            
			),
            'fields'=>array('ConnectionClientOpp.id','ConnectionClientOpp.typ','ConnectionClientOpp.name','ConnectionClientOpp.size','ConnectionClientOpp.price')
        ));
        

        $this->controller->set('opp_sum',(isset($f) && !empty($f) ? array_sum(Set::extract($f,'/ConnectionClientOpp/price')) : 0));
        return $f; 
    }
    
    /**
     * Nacteni sablon pro tabulku hodin
     */
    function company_cw_template_list(){
        if(!isset($this->controller->CompanyClientWorkingTemplateItem))
           $this->controller->loadModel('CompanyClientWorkingTemplateItem');
           
        $this->controller->CompanyClientWorkingTemplateItem->bindModel(array(
            'belongsTo'=>array('CompanyClientWorkingTemplate')
        ));
        $f = $this->controller->CompanyClientWorkingTemplateItem->find('all',array(
            'conditions'=>array(
					'CompanyClientWorkingTemplateItem.kos'=>0,
                    'CompanyClientWorkingTemplateItem.company_id'=>$this->this_data['ClientWorkingHour']['company_id'],
                    'CompanyClientWorkingTemplateItem.rok'=>$this->year,
                    'CompanyClientWorkingTemplateItem.mesic'=>$this->month                            
			),
            'fields'=>array('CompanyClientWorkingTemplateItem.id','CompanyClientWorkingTemplate.name')
        )); 
        
        $result = ($f ? Set::combine($f,'/CompanyClientWorkingTemplateItem/id','/CompanyClientWorkingTemplate/name') : array());
        
        if(!$result && 
            $this->this_data[$this->dochazka_model]['id'] == '' && 
            $this->this_data[$this->dochazka_model]['company_cw_template_id'] != ''
        ){
            $this->warning_about_cw_tempalte_miss = true;
        }
        
        return array(''=>'bez šablony') + $result;
    }
    
    /**
     * Nacteni seznam pracovnich povinosti
     * Pouze strasi max 6 mesicu!
     */
    function neplneni_prac_povinosti(){
        if(!isset($this->controller->ConnectionClientMisconduct))
           $this->controller->loadModel('ConnectionClientMisconduct');
           
        $list = $this->controller->ConnectionClientMisconduct->find('all',array(
            'conditions'=>array(
					'ConnectionClientMisconduct.client_id'=>$this->this_data[$this->dochazka_model]['client_id'],
                    'ConnectionClientMisconduct.misconduct_id'=>9,//upozorneni
                    'DATE_ADD(ConnectionClientMisconduct.datum,INTERVAL 6 MONTH) >= DATE_FORMAT(NOW(),"%Y-%m-%d")'            
			)
        ));
        return $list;      
    }

    /**
     * Pro dochazky int. zam
     * načteme defaulní nastevení podle formy odměny a projektu v kterem je zamestnan
     * hodnoty - preda se JS funkci ktera nastaveni zpracuje
     */
    function mzda_setting(){
        $id = $this->this_data[$this->dochazka_model]['id'];
        $return = false;
        if($id == null || $id == ''){
            if(!isset($this->controller->AtProjectMoneyItem))
                $this->controller->loadModel('AtProjectMoneyItem');
                
            $t = $this->controller->AtProjectMoneyItem->find('first',array(
                'conditions'=>array(
                    'kos'=>0,
                    'at_project_id'=>$this->this_data['ConnectionClientAtCompanyWorkPosition']['at_project_id'],
                    'at_company_money_item_id'=>$this->this_data['ConnectionClientAtCompanyWorkPosition']['at_company_money_item_id']
                )
            ));
            
            $must_set = array('stravenky','setting_shift_working_id','standard_hours');
            foreach($must_set as $_sett){
               $this->controller->data[$this->dochazka_model][$_sett] = $t['AtProjectMoneyItem'][$_sett];
            }
            $return = $t['AtProjectMoneyItem'];
        }
        
        return $return;
    }
    
    
    function get_drawbacks_from_activity(){
        $supervisor_id = 2;//jen pro coo
        $client_id = $this->this_data[$this->dochazka_model]['client_id'];      
     
        $pocet_dnu_v_mesici = cal_days_in_month(CAL_GREGORIAN, $this->month, $this->year);
        $last_day = date('N', mktime(0, 0, 0, $this->month, 1,$this->year)); 
        $current_day = $last_day;
        $pocet_pracovnich_dnu = 0;
        $vikendy = array();
        if($pocet_dnu_v_mesici > 0)
            for ($i=1; $i <= $pocet_dnu_v_mesici; $i++){
            	if(!in_array($current_day,array(6,7))){
            	   $pocet_pracovnich_dnu++;
            	} 
                else{
                    $vikendy[] = $i;
                }
                $current_day = ($current_day == 7)?$current_day=1:$current_day+1;    
            }

        define('fields','
          SUM(
                    (('.$pocet_pracovnich_dnu.' -(SELECT COUNT(den) FROM wapis__tting_stat_svateks WHERE DAYOFWEEK(datum) NOT IN (1,7) AND rok = "'.$this->year.'" AND mesic = "'.$this->month.'" and setting_stat_id = Company.stat_id) - (SELECT COUNT(DISTINCT activity_datetime) FROM fawawapis__y_activities as CompanyActivity WHERE
                             CompanyActivity.cms_user_id = CmsUser.id AND CompanyActivity.company_id = Company.id AND
                             CompanyActivity.kos = 0 AND
                             ((DATE_FORMAT(CompanyActivity.activity_datetime,"%Y-%m") = "'.$this->year.'-'.$this->month.'")) AND
                             DATE_FORMAT(CompanyActivity.created,"%Y-%m") = DATE_FORMAT(CompanyActivity.activity_datetime,"%Y-%m") AND
                             DAYOFWEEK(CompanyActivity.activity_datetime) NOT IN (1,7) AND
                             DAY(CompanyActivity.activity_datetime) NOT IN (SELECT den FROM fastwapiwapis__at_svateks WHERE DAYOFWEEK(datum) NOT IN (1,7) AND rok = "'.$this->year.'" AND mesic = "'.$this->month.'" and setting_stat_id = Company.stat_id)
                        ))
                        *
                        (SELECT value FROM fasteswapis_wapis__tting Where id = IF(Company.stat_id = 1,7,8) LIMIT 1)
                    )  
          )  as srazka_celkem
        ');
        
        if(!isset($this->controller->CmsUser))
                $this->controller->loadModel('CmsUser');
                
        $this->controller->CmsUser->bindModel(array(
            'belongsTo'=>array('CmsGroup'),
            'joinSpec'=>array(
                'Company'=>array(
                    'primaryKey'=>'client_manager_id',
                    'foreignKey'=>'CmsUser.id',
                )
            )
        ));        
        $result = $this->controller->CmsUser->find('first',array(
            'conditions'=>array(
                'CmsUser.at_employee_id'=>$client_id,
                'CmsUser.kos'=>0,
                'CmsGroup.cms_group_superior_id'=>$supervisor_id,//koo
            ),
            'fields'=>array(fields),
            'group'=>'CmsUser.id'
        ));
        
        if(isset($result[0]['srazka_celkem']) && $result[0]['srazka_celkem'] != ''){
            if($this->this_data[$this->dochazka_model]['id'] == '')
               $this->controller->data[$this->dochazka_model]['drawback_activity'] = $result[0]['srazka_celkem'];
            else 
               $this->controller->data['different_drawback_activity'] = $result[0]['srazka_celkem'];
        }
    }
    
    function get_drawbacks_from_autorization(){
        $supervisor_id = array(2,3);//koo,MR
        $client_id = $this->this_data[$this->dochazka_model]['client_id'];      
        unset($this->controller->CmsUser);
        $this->controller->loadModel('CmsUser');      
        $this->controller->CmsUser->bindModel(array(
            'belongsTo'=>array('CmsGroup'),
            'joinSpec'=>array()
        ));
        $cms_user = $this->controller->CmsUser->find('first',array(
            'conditions'=>array(
                'CmsUser.at_employee_id'=>$client_id,
                'CmsUser.kos'=>0,
                'CmsGroup.cms_group_superior_id'=>$supervisor_id
            ),
            'fields'=>array('CmsUser.id','CmsGroup.cms_group_superior_id')
        ));
        if(!$cms_user)
            return false;
        
        if(!isset($this->controller->ConnectionCompanyToAuthorization))
                $this->controller->loadModel('ConnectionCompanyToAuthorization');
                
        $this->controller->ConnectionCompanyToAuthorization->bindModel(array(
            'belongsTo'=>array('Company')
        ));        
        $result = $this->controller->ConnectionCompanyToAuthorization->find('all',array(
            'conditions'=>array(
                'OR'=>array(
                    'Company.client_manager_id'=>$cms_user['CmsUser']['id'],
                    'Company.manazer_realizace_id'=>$cms_user['CmsUser']['id'],
                ),
                'ConnectionCompanyToAuthorization.month'=>$this->month,
                'ConnectionCompanyToAuthorization.year'=>$this->year,
            ),
            'fields'=>'
                IF(DAY(closed) > 6,(SELECT value FROM fastest_wapis__ wapis__g Where id = 9 LIMIT 1),0) as srazka_coo,
                IF(DAY(auth_date) > 8,(SELECT value FROM wapis__settings as setting Where id = 10 LIMIT 1),0) as srazka_mr
            '
        ));
        
        if($cms_user['CmsGroup']['cms_group_superior_id'] == 2){
            $srazka = array_sum(Set::extract('/0/srazka_coo',$result)); 
        }
        else if($cms_user['CmsGroup']['cms_group_superior_id'] == 3){
            $srazka = array_sum(Set::extract('/0/srazka_mr',$result)); 
        }

        if($srazka == 0){$srazka = '0';}    

        if(isset($srazka) && $srazka != ''){
            if($this->this_data[$this->dochazka_model]['id'] == '')
               $this->controller->data[$this->dochazka_model]['drawback_close_cw'] = $srazka;
            else 
               $this->controller->data['different_drawback_close_cw'] = '00';
        }
    }
    
    /**
     * zjistujeme z permission, zda muze editovat maximalky
     */
    function access_to_max(){
        if(isset($this->permission['editace_maximalek']) && $this->permission['editace_maximalek'] == 1)
            return true;
        else
            return false;    
    }
}
?>