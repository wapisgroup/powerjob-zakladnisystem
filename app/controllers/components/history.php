<?php

class Item{
    private $source;
    private $path;
    private $caption;
    private $type = array();
    private $func;
    
    function __construct($data = null){
        $this->source = isset($data->source) ? $data->source : null;            
        $this->path = isset($data->path) ? $data->path : null;            
        $this->caption = isset($data->caption) ? $data->caption : null;     
        $this->func = isset($data->func) ? $data->func : null;     
        
        if(isset($data->type))
            $this->set_type($data->type);
    }      
    
    
    private function set_type($type){
        $temp = (array)$type->cols->children();

        if(isset($temp['col']))
            $temp = $temp['col'];
        else 
            $temp = array();    

        $tmp_array = array(
            'name' => (string)$type->name,
            'model' => (string)$type->model,
            'cols' => $temp           
        );
        
        
        $this->type = $tmp_array;
    }
    
    public function get_type($col = null){
        if($col == null)
            $tmp = $this->type;
            
        else {
            $tmp = $this->type[$col];
        }
        
        return $tmp;
    }
        
    public function __get($key){
        if(isset($this->$key))
            return (string)$this->$key;
        else
            return false;    
    }
}


class MyXmlParser{
    var $data = array();
    
    
    function __construct($data = null){
        if($data == null)
            return false;
        else
            return $this->zpracuj_data($data);
    }
    
    private function zpracuj_data($data){
        $xml = simplexml_load_string($data);
        
        foreach($xml->item as $item)
            $this->data[] = new Item($item); 
    }
    
    public function get_data(){
        return $this->data;
    }    
}




class HistoryComponent extends Object{
	//var $components = array('Pagination','Session');
	var $helpers = array('Fastest');
    var $init_helpers = array();
    
	function startup(&$controller) {
		$this->controller =$controller;
	
          foreach($this->helpers as $helper){
            if (!isset($this->init_helpers[$helper])){
             App::import('Helper', $helper);
             $loadHelper = $helper.'Helper';
             $this->init_helpers[$helper] = new $loadHelper();   
            } 
          }
    	
		/*
		 * Globalni ulozeni vsech aktivit
		 */
		$history_action = ClassRegistry::init('HistoryAction', 'Model');
		$history_action->save(array(
			'HistoryAction' => array(
				'controller' 	=> $this->controller->name,
				'action'		=> $this->controller->action,
				'cms_user_id'	=> (isset($this->controller->logged_user)?$this->controller->logged_user['CmsUser']['id']:'-99'),
				'url'			=> isset($this->controller->params['url']['url'])?$this->controller->params['url']['url']:'neznama URL'
			)
		));
		unset($history_action);
		
             
		/*
		 * Ulozeni historie klientu pro Paťáka
		 */
		// odesilaji se nejaka data pres $this->data?
		if (!empty($this->controller->data)){
			// najiti, zda odpovida controller a action nejakemu zaznamu
			
			$history_set = ClassRegistry::init('HistorySet', 'Model');
			
			$actions = $history_set->find(
				'all',
				array(
					'conditions' => array(
						'controller' 	=> $this->controller->name,
						'action'		=> $this->controller->action,
						'status'		=> 1
					)
				)
			);
	            
			// pokud nasel, projdi je, popripade jeste vyselektuj a prenes na dalsi akci
			if (isset($actions) && count($actions)>0){
				// natahnuti modelu pro ulozeni zaznamu
				$history_item = ClassRegistry::init('HistoryItem', 'Model');
							
				foreach($actions as $action){
					//zjisteni, o jakeho klienta se jedna
					if (!empty($action['HistorySet']['primary_id'])){
						list($client_from, $client_field_path) = explode('|',$action['HistorySet']['primary_id']);
						// nalezeni hodnoty client_id dle typu, zatim jenom this->controller->data
						switch($client_from){
							case 'data':
									$client_id	= Set::extract($client_field_path,$this->controller->data);					
								break;
                            case 'args':
                                    $args = $this->controller->params['pass'];
									$client_id	= $args[$client_field_path];					
								break;    
							default:
									// nasrat
								break;
						}
					}
                    
                    /**
                    * extract vraci pole, tedy pokud je jen jeden premen na string
                    */  
                    if(is_array($client_id))
                        $client_id = $client_id[0]; 
                     
                    /**
                     * nalezeni parenta pro vicenasobne zamestnavani 
                     */
                   	$client = ClassRegistry::init('Client', 'Model');
                    $parent_id = false;
                    $c = $client->read(array('parent_id'),$client_id);
                    if ($c['Client']['parent_id'] != 0){
                        $parent_id = true;
                    	$client_id = $c['Client']['parent_id'];
                    }
                    
                        
                    /**
                    * zavedeni conditions 
                    * pokud jsou conditions splneny tak true jinak vraci false a HistoryItem se neulozi
                    */
                    $save_item = true;    
                    if (!empty($action['HistorySet']['conditions'])){
						list($con_type, $con_path,$con_value) = explode('|',$action['HistorySet']['conditions']);
                        // nalezeni hodnoty          
						switch($con_type){
							case 'data':
									$value = Set::extract($con_path,$this->controller->data);
                                    $value = $value[0]; 
                                     
                                    //splneni nastavene podminky zatim pouze operator rovna se
                                    if($value != $con_value)
                                        $save_item = false;  
								break;
                            case 'args':
                                    //$args = $this->controller->params['pass'];
									//$client_id	= $args[$client_field_path];					
								break;    
							default:
									// nasrat
							break;
						}
					}    
                    
                    
                    /**
                     * zavedeni hlidani Empty ID
                     * pokud je zaskrtnuto, ukladej pouze pokud neni id definovano
                     * jedna se tedy o nejake pridani
                     */
					if (!empty($action['HistorySet']['empty_id'])){
						list($empty_id_from, $empty_id_path) = explode('|',$action['HistorySet']['empty_id']);
						// nalezeni hodnoty client_id dle typu, zatim jenom this->controller->data
						switch($empty_id_from){
							case 'data':
									$empty_id	= Set::extract($empty_id_path,$this->controller->data);						
								break;
                            case 'args':
                                    $args = $this->controller->params['pass'];
									$empty_id	= $args[$empty_id_path];					
								break;    
							default:
									// nasrat
								break;
						}
                        
                        /**
                         * extract vraci pole, tedy pokud je jen jeden premen na string
                         */ 
                        if(is_array($empty_id))
                            $empty_id = $empty_id[0]; 
                   
                        
                        /**
                         * pokud neni empty_id prazdne neukladej
                         */
                        if(!empty($empty_id))
                            $save_item = false;
					}
                    
                    if($save_item){
    					$history_item->save(array(
    						'HistoryItem' => array(
    							'category_id' 	=> $action['HistorySet']['category_id'],
    							'action_id' 	=> $action['HistorySet']['action_id'],
    							'client_id'		=> (isset($client_id) && !empty($client_id))?$client_id:-99,
    							'cms_user_id'	=> (isset($this->controller->logged_user)?$this->controller->logged_user['CmsUser']['id']:'-99')
    					
    						)
    					));	
                        $history_item_id = $history_item->id;    
                   
                    
					
    					// vytahnuti nadefinovanych sloupcu k ulozeni
    					//$colums = explode("\n",$action['HistorySet']['colums']);
                        $colums = new MyXmlParser($action['HistorySet']['colums']);
                        $colums = $colums->get_data();
                    
    					if (isset($colums) && count($colums)>0){
    						$history_data = ClassRegistry::init('HistoryData', 'Model');
    						foreach($colums as $col){
    
    							// ziskani dat, prozatim pouze z this->controller->data
    							switch ($col->__get('source')){
    								case 'data':
    										$value = Set::extract($col->__get('path'),$this->controller->data);											
    									break;
    								case 'arg':
    									   $args = $this->controller->params['pass']; 
    	                                   $value = $args[$col->__get('path')];	
    									break;
    								case 'get':
    									// cerpa z $this->controller->params['url'];
    									break;
    								default:
    										// nasrat
    									break;
    							}
    							/*
    							 * rozdeleni zpracovani value dle typu
    							 */
    							switch ($col->get_type('name')){
    								case 'text':
    									$value = $value;
    									break;
                                    case 'date':
                                        $fastest = $this->init_helpers['Fastest'];
    									$value = $fastest->czechDate($value[0]);
                                        
                                        unset($fastest);
    									break;    
    								case 'db_list':
                                    // nacitani z DB
    									/*
    									 * list($model,$field) = explode('@',$from);
    									 * $model = promenna s nazvem modelu
    									 * $field = sloupec
    									 * $temp_model = ClassRegistry::init($model, 'Model');
    									 * $value = Set::extract('/'.$model.'/'.$field,$temp_model->read(array($field),$value));
    									 * unset($temp_model); 
    									 */                            
                                       
                                        $cols = array();
                                        $cols = am($cols,$col->get_type('cols')); //vraci array sloupcu ktere se maji tahat z db
    	                               
                                        //nacteni potrebneho modelu
                                        $temp_model = ClassRegistry::init($col->get_type('model'), 'Model'); 
                                        
                                        if(count($cols) == 1){ //pokud je jen jeden sloupce
                                            $value = Set::extract('/'.$col->get_type('model').'/'.$cols[0],$temp_model->read(array($cols[0]),$value));
                                        }
                                        else { // pokud je jich vice
                                            $value = $temp_model->read($cols,$value);
    
                                            //bude existuje funkce ktera vrati string z techto poli
                                            $func = (string)$col->__get('func');
                                            if(in_array($func,get_class_methods($this))){
                                                $value = $this->$func($value[$col->get_type('model')]);
                                            }    
                                            else //nebo se vrati zasebou oddelene -
                                                $value = implode('-',$value[$col->get_type('model')]);
                                        }
                        
                                        
                                         //uvloneni modelu z pameti
                                         unset($temp_model); 
    									
    									break;
    								case 'list':
                                        $list = $col->get_type('model');
    									$value = $this->controller->{$list}[$value[0]];
                                       
    									break;
    								default:
    									// nasrat
    									break;
    							}
                                
                                /**
                                 * extract vraci pole, tedy pokud je jen jeden premen na string
                                 */  
                                if(is_array($value))
                                    $value = (isset($value[0])?$value[0]:'');
                                
                                /**
                                 * prazdne value nahradit za "nevypleno"  
                                 */
                                if($value == '')
                                    $value = 'Nevyplněno';         
    							
    							// ulozeni dat
    							$history_data->save(array(
    								'HistoryData' => array(
    									'history_item_id' 	=> $history_item_id,
    									'caption'			=> $col->__get('caption'),
    									'value'				=> $value,
    									'cms_user_id'		=> (isset($this->controller->logged_user)?$this->controller->logged_user['CmsUser']['id']:'-99')
    								)
    							));
    							$history_data->id = null;
    						}
    						
    					}
                        /**
                         * ulozeni data o tom ze se jedna o aktivitu k parentovi
                         */
                        if($parent_id){
                            if(isset($history_data))
                                $history_data = ClassRegistry::init('HistoryData', 'Model');
                            
                                $history_data->id = null;
                                $history_data->save(array(
    								'HistoryData' => array(
    									'history_item_id' 	=> $history_item_id,
    									'caption'			=> 'Vícenasobné zaměstnání, klient',
    									'value'				=> $client_id,
    									'cms_user_id'		=> (isset($this->controller->logged_user)?$this->controller->logged_user['CmsUser']['id']:'-99')
    								)
    							));    
                                $history_data->id = null;
                        }
                   }      
    			   $history_item->id = null;
                        
                        
				}
				unset($history_item);                 
			}
		} else {
			// ser na to, vubec nic neres 
		}
	}
    
    
    /**
     * funkce ktera vytvari string formy odmeny s textovym zobrazenim ubytovani a dopravy
     */
    private function created_full_money_item($value){
        //pr($value);
        
        $fastest = $this->init_helpers['Fastest'];
        $ub = $fastest->value_to_yes_no($value['cena_ubytovani_na_mesic']);
        $do = $fastest->value_to_yes_no($value['doprava']);

        unset($fastest);
        return String::insert(':name - Ubytování :ub / Doprava :do', array('name' => $value['name'], 'ub' => $ub,  'do' => $do));
    }
}
?>