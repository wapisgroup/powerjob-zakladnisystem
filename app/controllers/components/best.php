<?php
/**
 * BEST componenta
 * @author Jakub Matuš
 * @created 19.1.10
 * 
 * komponenta pro vytvoreni importniho souboru .ikm pro KB
 */
class BestComponent{
    private $out;
    public $testing = false;
    private $count_payments = 0;  // pocet prikazu
    private $control_sum = 0; // kontrolni suma
    private $data;
    private $no = 0; // ciselna rada prikazu - load a save do db, musi se uchovavat
    
    private $error = array(); //pole chybových hlášek
    
    // Constructor
	function Best() {
        App::import('Vendor', 'header', array('file' => 'best'.DS.'header.php')); 
        App::import('Vendor', 'dataItem', array('file' => 'best'.DS.'data_item.php')); 
        App::import('Vendor', 'footer', array('file' => 'best'.DS.'footer.php')); 
	   
        /**
         * vytvoreni hlavicky
         */
         $head = new Header();
         $this->out = $head->create();
    }
    
    /**
     * funkce pro vytvorani souboru
     */
    public function create_file(){
         if($this->testing === true){  
            echo $this->out;
         }
         else{  
            $dir = "kb/";
            $filename = date('Ymd_His').".ikm";
            
             //write new data to the file, along with the old data
            $handle = fopen($dir.$filename, "w+");
                if (fwrite($handle, $this->out) === false) {
                    return "Cannot write to text file. <br />";          
                }
            fclose($handle); 
            
            return $filename;
         }    
    }
    
    /**
     * funkce pro nastaveni promennych
     */
    public function __set($var,$value){
        $this->$var = $value;
    }
    
    /**
     * funkce pro ziskani hodnot promennych
     */
    public function __get($var){
        return $this->$var;
    }
    
    
    /**
     * funkce kontoluje spravnost pridani jednotliveho prikazu
     * a pridava do datove vetve pokud je vse v poradku
     */
    public function add_data($values = array()){
        if(!empty($values)){
            if($values['account'] != 0 && $values['amount'] != 0 && $values['code_bank'] != ''){
                /**
                 * jedinecne id kazdeho prikazu
                 * slozene z posledniho id ulozene v DB
                 * a cislo platby v tomto exportu
                 */
                $data_id = $this->no+$this->count_payments+1;
                
                
                $this->out .= $this->data->add_data($data_id,$values['date_to_pay'],(int)$values['amount'],$values['code_bank'],$values['account'],$values['message_commiter']);
                
                //inkremetace poctu prikazu
                $this->count_payments ++;
                
                //nacteni kontrolni sumy
                $this->control_sum += $values['amount'];
            }
            else {
                /**
                 * castka, cislo uctu nebo kod banky bylo nulove
                 */
                 $this->error[] = "Příkaz č.".$this->count_payments.". Částka,číslo účtu nebo kod banky bylo nulové.";
            }     
        }
        else{
            $this->error[] = "Příkaz č.".$this->count_payments.". Pole hodnot pro přidání je prázdné";
        }
        
        
    }
    
   /**
    * vytvoreni paticky
    */
    public function make_footer(){
         $footer =  new Footer(null,$this->count_payments,$this->control_sum);
         $this->out .= $footer->create();
    }
    
   /**
    * vytvoreni sablony pro prikazy
    * @param $account_committer - cislo prikazce
    * @param $vs - variabilni symbol
    * cislo prikazce (AT), pro testovani fastest - 357207600267 
    */
    public function make_data_template($account_committer, $vs){
        
        $this->data = new DataItem($account_committer,$vs);  
    }
    
    
   /**
    * funkce pro zjisteni zda byla zjistena nejaka chyba
    * pokud ano tak vrat pole chybovych hlasek, jinak vrat true 
    */
    public function get_error(){
        if(empty($this->error))
            return true;
        else
            return $this->error;        
    }
    
}
?>