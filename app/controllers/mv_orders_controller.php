<?php
Configure::write('debug', 0);
class MvOrdersController extends AppController
{
    var $name = 'MvOrder';
    var $helpers = array('htmlExt', 'Pagination', 'ViewIndex', 'FileInput');
    var $components = array('ViewIndex', 'RequestHandler', 'Upload', 'Email');
    var $uses = array('MvOrder');
    var $renderSetting = array(
        'bindModel' => array('belongsTo' => array('CmsUser')),
        'SQLfields' => '*',
        'controller' => 'mv_orders',
        'page_caption' => 'Objednavky na MV',
        'sortBy' => 'MvOrder.id.DESC',
        'top_action' => array(
            // caption|url|description|permission
            'add_item' => 'Přidat|edit|Pridat popis|add'
        ),
        'filtration' => array(

        ),
        'items' => array(
            'id' => 'ID|MvOrder|id|text|',
            'user' => 'Vytvořil|CmsUser|name|text|',
            'created' => 'Vytvořeno|MvOrder|created|datetime|',
            'updated' => 'Změněno|MvOrder|updated|datetime|'
        ),
        'posibility' => array(
            'edit' => 'edit|Editace položky|edit',
            'delete' => 'trash|Odstranit položku|delete'
        ),
        'domwin_setting' => array(
            'sizes' => '[900,900]'
        )
    );

    function index()
    {
        if ($this->RequestHandler->isAjax()) {
            $this->render('../system/items');
        } else {
            $this->set('fastlinks', array('ATEP' => '/', 'Administrace' => '#', $this->renderSetting['page_caption'] => '#'));
            $this->render('../system/index');
        }
    }

    function edit($id = null)
    {
        $this->autoLayout = false;
        if (empty($this->data)) {
            $this->loadModel('MvItem');
            $type_list = $this->MvItem->find('list', array('conditions' => array('kos' => 0, 'druh !=' => '-1'), 'group' => 'druh', 'fields' => array('druh', 'druh')));

            // $this->set('template_list',$this->get_list('MvTemplate'));

            if ($id != null) {
                $this->MvOrder->bindModel(array('hasMany' => array('MvOrderItem')));
                $this->data = $this->MvOrder->read(null, $id);

                $kos = true;

                //potvrzena objednavka nepotrebuje podminku na aktualni zbozi/historie
                if ($this->data['MvOrder']['status'] == 1) {
                    $kos = false;
                }

                $names = array();
                foreach (Set::extract('/MvOrderItem/type_id', $this->data) as $t) {
                    $names[$t] = self::load_typ($t, true, $kos);
                }
                $this->set('names', $names);

                $sizes = array();
                foreach (Set::extract('/MvOrderItem/name', $this->data) as $t)
                    $sizes[$t] = self::load_typ($t, true, $kos);
                $this->set('sizes', $sizes);
            } else {
                $this->set('new',true);
            }

            $this->set('type_list', $type_list);

            $con = array('kos' => 0);
            if (!in_array($this->logged_user['CmsGroup']['id'], array(1, 56, 57, 18))) {
                $con = array(
                    'kos' => 0,
                    'OR' => array(
                        'coordinator_id' => $this->logged_user['CmsUser']['id'],
                        'coordinator_id2' => $this->logged_user['CmsUser']['id'],
                        'client_manager_id' => $this->logged_user['CmsUser']['id'],
                    )
                );
            }
            else if ($this->logged_user['CmsGroup']['id'] == 18) {
                $con = array(
                    'kos' => 0,
                    'manazer_realizace_id' => $this->logged_user['CmsUser']['id'],
                );
            }

            if ($this->data['MvOrder']['status'] == 1) {
                $this->render('../mv_orders/confirm');
            }
            else
                $this->render('../mv_orders/edit');
        } else {
            /**
             * Nova objednávka, zašleme notifikaci
             */
            $send_notification = false;
            if ($this->data['MvOrder']['id'] == '') {
                $send_notification = true;
            }

            if ($this->MvOrder->save(array(
                'id' => $this->data['MvOrder']['id'],
                'cms_user_id' => $this->logged_user['CmsUser']['id']
            ))
            ) {
                $this->loadModel('MvOrderItem');
                $this->MvOrderItem->deleteAll(array('mv_order_id' => $this->MvOrder->id));

                $company_list = array();
                foreach ($this->data['MvOrderItem'] as $itm) {
                    $company_list[] = $itm['company_id'];
                    $itm['mv_order_id'] = $this->MvOrder->id;
                    $itm['cms_user_id'] = $this->logged_user['CmsUser']['id'];
                    $itm['count_order'] = $itm['count'];
                    $this->MvOrderItem->save($itm);
                    $this->MvOrderItem->id = null;
                }

                if ($send_notification === true) {
                    //prp koordinatory
                    $recipient_list = array();
                    if ($this->logged_user['CmsGroup']['cms_group_superior_id'] == 2) {
                        //COO jako samotny prijemce
                        $recipient_list[] = $this->logged_user['CmsUser']['email'];

                        //a vsichni MR firem z OPP kterym prirazuje
                        $company_list = array_unique($company_list);
                        $this->loadModel('Company');
                        $this->Company->bindModel(array(
                            'belongsTo' => array('CmsUser' => array('foreignKey' => 'manazer_realizace_id'))
                        ));
                        $mr_list = $this->Company->find('all', array(
                            'conditions' => array('Company.id' => $company_list),
                            'fields' => array('CmsUser.email')
                        ));
                        foreach ($mr_list as $mr) {
                            $recipient_list[] = $mr['CmsUser']['email'];
                        }
                    }

                    $replace_list = array(
                        '##id##' => $this->MvOrder->id,
                        '##vytvoril##' => $this->logged_user['CmsUser']['name']
                    );
                    $this->Email->send_from_template_new(54, $recipient_list, $replace_list);
                }

                die(json_encode(array('result' => true)));
            } else
                die(json_encode(array('result' => false, 'message' => 'Nepodarilo se ulozit do DB')));
        }
    }



    function load_typ($type = null, $return = false, $kos = true)
    {
        $conditions = array('druh' => $type);
        if ($kos == true) {
            $conditions['kos'] = 0;
        }

        $this->loadModel('MvItem');
        $list = $this->MvItem->find('all', array('conditions' => $conditions, 'fields' => array('id', 'name')));
        $list = Set::combine($list, '{n}.MvItem.name', array('%s|%s', '{n}.MvItem.name', '{n}.MvItem.id'));
        if ($return === false)
            die(json_encode($list));
        else
            return $list;
    }



    function load_size($type = null, $return = false, $kos = true)
    {
        $conditions = array('name' => $type);
        if ($kos == true) {
            $conditions['kos'] = 0;
        }

        $this->loadModel('MvItem');
        $list = $this->MvItem->find('all', array('conditions' => $conditions, 'fields' => array('id', 'size')));
        $list = Set::combine($list, '{n}.MvItem.size', array('%s|%s', '{n}.MvItem.size', '{n}.MvItem.id'));

        if ($return === false)
            die(json_encode($list));
        else
            return $list;
    }

    function load_count_price($id = null)
    {
        $this->loadModel('MvItem');
        $data = $this->MvItem->read(array('cena_eur', 'cena_cz', 'pocet'), $id);
        if ($data) {
            die(json_encode(array('result' => true, 'data' => $data)));
        } else {
            die(json_encode(array('result' => false, 'message' => 'Zaznam nebyl nalezen')));
        }
    }
}

?>