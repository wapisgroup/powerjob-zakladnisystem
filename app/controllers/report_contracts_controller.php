<?php
Configure::write('debug',1);
class ReportContractsController extends AppController {
	var $name = 'ReportContracts';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload','Email');
	var $uses = array('ReportContract');
	var $renderSetting = array(
        'bindModel'=>array('belongsTo'=>array('CmsUser','AtProject','AtProjectCentre','AtCompany')),
		'SQLfields' => '*',
		'controller'=> 'report_contracts',
		'page_caption'=>'Smlouvy',
		'sortBy'=>'ReportContract.name.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
			//'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
			//'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
			//'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
			'ReportContract-name|'				=>	'text|Název|',
			'ReportContract-at_project_id'		=>	'select|Projekt|project_list',
            'ReportContract-at_project_centre_id'		=>	'select|Středisko|centre_list'
		),
		'items' => array(
			'id'			=>	'ID|ReportContract|id|text|',
			'name'			=>	'Název|ReportContract|name|text|',
            'user'			=>	'Přidal|CmsUser|name|text|',
            'firma'			=>	'Firma|AtCompany|name|text|',
            'project'		=>	'Projekt|AtProject|name|text|',
            'centre'		=>	'Středisko|AtProjectCentre|name|text|',
            
			//'updated'		=>	'Změněno|Accommodation|updated|datetime|',
			'created'		=>	'Vytvořeno|ReportContract|created|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
			'attach'	=>	'attachs|Přílohy|attach',
			'contract_trash'	=>	'contract_trash|Odstranit položku|contract_trash'			
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Klienti'=>'#'));
		$this->set('project_list',$this->get_list('AtProject'));
		$this->set('centre_list',$this->get_list('AtProjectCentre'));
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			
			$this->set('scripts',array('uploader/uploader','clearbox/clearbox'));
			$this->set('styles',array('../js/uploader/uploader','../js/clearbox/clearbox'));
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			$cetre_list = array();
			$this->loadModel('SettingAccommodationType');
			// load accommodation_type_list_with_price_type
			$this->set('accommodationtype_list', $this->SettingAccommodationType->find('list', array('order'=>'name','conditions'=>array('SettingAccommodationType.kos'=>0))));
			$this->set('accommodationtype_title_list', $this->SettingAccommodationType->find('list', array('order'=>'name','conditions'=>array('SettingAccommodationType.kos'=>0),'fields'=>array('id','price_type'))));
			
			if ($id != null){
				$this->data = $this->ReportContract->read(null,$id);
                $cetre_list = $this->get_list('AtProjectCentre',array('at_project_id'=>$this->data['ReportContract']['at_project_id']));
			}
            
            $this->set('company_list',$this->get_list('AtCompany'));
            $this->set('project_list',$this->get_list('AtProject'));
            $this->set('centre_list',$cetre_list);
			$this->render('edit');
		} else {
			$this->data['ReportContract']['cms_user_id']=$this->logged_user['CmsUser']['id'];
			$this->ReportContract->save($this->data);
            
            
            $project = self::load_for_email($this->data['ReportContract']['at_project_id'],'AtProject',array('name','manager_id'));
		    $comp = self::load_for_email($this->data['ReportContract']['at_company_id'],'AtCompany',array('name'));
            $st = self::load_for_email($this->data['ReportContract']['at_project_centre_id'],'AtProjectCentre',array('name','spravce_majetku_id','reditel_strediska_id'));             
            $replace_list = array(
				'##AtProject.name##' 	=>$project['AtProject']['name'],
                '##AtCompany.name##' 	=>$comp['AtCompany']['name'],
                '##AtProjectCentre.name##' 	=>$st['AtProjectCentre']['name'],
                '##CmsUser.name##' 	=>$this->logged_user['CmsUser']['name']
			);
            foreach($this->data['ReportContract'] as $item=>$value){
                $replace_list['##ReportContract.'.$item.'##'] = $value;
            }
            
            if($this->data['ReportContract']['id'] == ''){
                //do systemu byl vložen nový majetek	
                $replace_list['##akce##'] = 'do systemu byl vložena nová smlouva';
            }
            else{
                $replace_list['##akce##'] = 'byla provedena změna smlouvy';
            }
            $this->Email->send_from_template_new(50,array($this->logged_user['CmsUser']['email']),$replace_list);
            
			die();
		}
	}
    
    /**
     * Eliminaci zajistuje viewindex
     * zde pouze odesalni emailu
     */
    function contract_trash($id = null){
        $this->data = $this->ReportContract->read(null,$id);
        
        $project = self::load_for_email($this->data['ReportContract']['at_project_id'],'AtProject',array('name','manager_id'));
	    $comp = self::load_for_email($this->data['ReportContract']['at_company_id'],'AtCompany',array('name'));
        $st = self::load_for_email($this->data['ReportContract']['at_project_centre_id'],'AtProjectCentre',array('name','spravce_majetku_id','reditel_strediska_id'));             
        $replace_list = array(
			'##AtProject.name##' 	=>$project['AtProject']['name'],
            '##AtCompany.name##' 	=>$comp['AtCompany']['name'],
            '##AtProjectCentre.name##' 	=>$st['AtProjectCentre']['name'],
            '##CmsUser.name##' 	=>$this->logged_user['CmsUser']['name'],
            '##akce##'=> 'byla smazána smlouva'
		);
        foreach($this->data['ReportContract'] as $item=>$value){
            $replace_list['##ReportContract.'.$item.'##'] = $value;
        }
        $this->Email->send_from_template_new(50,array($this->logged_user['CmsUser']['email']),$replace_list);
        
        $this->ViewIndex->render_trash($id);
        exit();
    }
    
    function load_for_email($id,$model,$fields = null){
        if(!isset($this->{$model}))
            $this->loadModel($model);
        
        return $this->{$model}->read($fields,$id);   
    }
	
	/**
 	* Seznam priloh
 	*
	* @param $ReportContract_id
 	* @return view
 	* @access public
	**/
	function attachs($report_contract_id){
		$this->autoLayout = false;
		$this->loadModel('ReportContractAttachment'); 
		$this->ReportContractAttachment->bindModel(array('belongsTo'=>array('SettingAttachmentType')));
		$this->set('attachment_list', $this->ReportContractAttachment->findAll(array('ReportContractAttachment.report_contract_id'=>$report_contract_id,'ReportContractAttachment.kos'=>0)));
		$this->set('report_contract_id',$report_contract_id);
		unset($this->ReportContractAttachment);
		$this->render('attachs/index');
	}
	
	/**
 	* Editace priloh
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function attachs_edit($report_contract_id = null, $id = null){
		$this->autoLayout = false;
		$this->loadModel('ReportContractAttachment'); 
		if (empty($this->data)){
			$this->loadModel('SettingAttachmentType'); $this->SettingAttachmentType = new SettingAttachmentType();
			$this->set('setting_attachment_type_list',$this->SettingAttachmentType->find('list',array('conditions'=>array('kos'=>0),'order'=>'poradi ASC')));
			unset($this->SettingAttachmentType);
			$this->data['ReportContractAttachment']['report_contract_id'] = $report_contract_id;
			if ($id != null){
				$this->data = $this->ReportContractAttachment->read(null,$id);
			}
			$this->render('attachs/edit');
		} else {
			$this->ReportContractAttachment->save($this->data);
			$this->attachs($this->data['ReportContractAttachment']['report_contract_id']);
		}
		unset($this->ReportContractAttachment);
	}
	/**
 	* Presun prilohy do kose
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function attachs_trash($report_contract_id, $id){
		$this->loadModel('ReportContractAttachment');
		$this->ReportContractAttachment->save(array('ReportContractAttachment'=>array('kos'=>1,'id'=>$id)));
		$this->attachs($report_contract_id);
		unset($this->ReportContractAttachment);
	}
	
	/**
 	* Nahrani prilohy na ftp
 	*
 	* @return view
 	* @access public
	**/
	function upload_attach() {
		$this->Upload->set('data_upload',$_FILES['upload_file']);
		if ($this->Upload->doit(json_decode($this->data['upload']['setting'],true))){
			echo json_encode(array('upload_file'=>array('name'=>$this->Upload->get('outputFilename')),'return'=>true));
		} else 
			echo json_encode(array('return'=>false,'message'=>$this->Upload->get('error_message')));		
		die();
	} 
	
	/**
 	* Stazeni prilohy
 	*
	* @param $file
	* @param $file_name
 	* @return download file
 	* @access public
	**/
	function  attachs_download($file,$file_name){
		$pripona = strtolower(end(Explode(".", $file)));
		$file = strtr($file,array("|"=>"/"));
		$filesize = filesize('./uploaded/'.$file);
		$cesta = "http://".$_SERVER['SERVER_NAME']."/uploaded/".$file;
				 
		header("Pragma: public"); // požadováno
	    header("Expires: 0");
	    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	    header("Cache-Control: private",false); // požadováno u některých prohlížečů
	    header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . $filesize);
		Header('Content-Type: application/octet-stream');
		Header('Content-Disposition: attachment; filename="'.$file_name.'.'.$pripona.'"');
		readfile($cesta);
		die();

	}
}
?>