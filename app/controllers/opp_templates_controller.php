<?php
Configure::write('debug',1);
class OppTemplatesController extends AppController {
	var $name = 'OppTemplates';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('OppTemplate');
	var $renderSetting = array(
		'controller'=>'opp_templates',
		'SQLfields' => array('id','name','updated','created'),
		'page_caption'=>'OPP šablony',
		'sortBy'=>'OppTemplate.created.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat šablonu|add',
		//	'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
		//	'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
		//	'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
		//	'SettingAccommodationType-status'	=>	'select|Stav|select_stav_zadosti',
		//	'SettingAccommodationType-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|OppTemplate|id|text|',
            'name'		=>	'Název|OppTemplate|name|text|',
			'updated'	=>	'Upraveno|OppTemplate|updated|datetime|',
			'created'	=>	'Vytvořeno|OppTemplate|created|datetime|'
		),
		'posibility' => array(
			
			'edit'		=>	'edit|Editace položky|edit',
            'items'	    => 	'items|Definování položek šablony|items',
			'delete'	=>	'trash|Do košiku|trash'			
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#',$this->renderSetting['page_caption']=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null)
				$this->data = $this->OppTemplate->read(null,$id);
			$this->render('edit');
		} else {
			$this->OppTemplate->save($this->data);
			die();
		}
	}
    
    function items($id = null){
        if (empty($this->data)){
            $this->loadModel('OppItem');
    		$this->set('type_list',$this->OppItem->find('list', array('conditions'=>array('kos'=>0,'type !='=>'-1'),'group'=>'type','fields'=>array('type','type'))));
    		$this->set('template_id',$id);
            
            $this->loadModel('OppTemplate');
            $this->OppTemplate->bindModel(array('hasMany'=>array('OppTemplateItem')));
			$this->data = $this->OppTemplate->read(null,$id);
            		
			$names = array();
			foreach(Set::extract('/OppTemplateItem/type_id',$this->data) as $t)
				$names[$t] = self::load_typ($t,true);
			$this->set('names',$names);
				
        } else {
	
            $this->loadModel('OppTemplateItem');
    		$this->OppTemplateItem->deleteAll(array('opp_template_id'=>$id));
    		foreach($this->data['OppTemplateItem'] as $i=>$itm){
    		    if($i != -1){
                    $itm['opp_template_id'] = $id;
        			$itm['cms_user_id']	= $this->logged_user['CmsUser']['id'];
        			$this->OppTemplateItem->save($itm);
        			$this->OppTemplateItem->id = null;
                }
    		}
    		
    		die(json_encode(array('result'=>true)));
        }
    }
    
    private function load_typ($type = null, $return = false){
		$this->loadModel('OppItem');
		$list = $this->OppItem->find('list',array('conditions'=>array('type'=>$type,'kos'=>0),'fields'=>array('name','name'),'group'=>'name'));
		if ($return === false)
			die(json_encode($list));
		else
			return $list;
	}
}
?>