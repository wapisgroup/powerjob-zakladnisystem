<?php
class WishsController extends AppController {
	var $name = 'Wishs';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('Wish');
	var $renderSetting = array(
		'SQLfields' => array('id','Wish.name','updated','created','status','cms_user'),
		'controller'=> 'wishs',
		'page_caption'=>'Kniha přání ATEP',
		'sortBy'=>'Wish.updated.DESC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat úkol|add'
		),
		'filtration' => array(
		//	'Wish-name'				=>	'text|Název firmy|',
			
		),
		'items' => array(
			'id'		=>	'ID|Wish|id|text|',
			'name'		=>	'Název|Wish|name|text|',
			'created'	=>	'Vytvořeno|Wish|created|datetime|',
			'updated'	=>	'Upraveno|Wish|updated|datetime|',
			'zadavatel'	=>	'Zadavatel|Wish|cms_user|text|',
		),
		'posibility' => array(
			//'status'	=> 	'status|Změna stavu|status',
			'edit'		=>	'edit|Editace položky|edit',			
			'delete'	=>	'trash|Odstranit položku|delete'
		),
		'domwin_setting' => array(
			'sizes' 		=> '[600,900]',
			'scrollbars'	=> true,
			'languages'		=> true,
			'defined_lang'	=> "['cz','en','de']",
		)
	);
	function index(){
			
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
		
			$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Kniha přání'=>'#'));
			
			
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
				
			if ($id != null){
				$this->data = $this->Wish->read(null,$id);		
			} 
			$this->render('edit');
		} else {
				
			$this->Wish->save($this->data);
			die();
		}
	}
	
}
?>