<?php
//Configure::write('debug',1);
class ReportCompanyTemplatesController extends AppController {
	var $name = 'ReportCompanyTemplates';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload','Email');
	var $uses = array('CompanyOrderTemplate');
	var $renderSetting = array(
		//'SQLfields' => array('id','Company.name','updated','created','status','SalesManager.name','ClientManager.name','Coordinator.name','mesto','SettingStav.namre'),
		'SQLfields' => '*',
		'bindModel' => array(
            'belongsTo' => array(
                'Company'=>array('foreignKey'=>'company_id')
            )
        ),
		'controller'=> 'report_company_templates',
		'page_caption'=>'Šablony požadavků pro nábor',
		'sortBy'=>'CompanyOrderTemplate.id.ASC',
		'top_action' => array(),
		'filtration' => array(
            'CompanyOrderTemplate-name' => 'text|Šablona|',
            'Company-id' => 'select|Společnost|company_list',
        ),
		'items' => array(
			'id'				=>	'ID|CompanyOrderTemplate|id|text|',
	        'firma'				=>	'Firma|Company|name|text|',
			'order'				=>	'Název|CompanyOrderTemplate|name|text|',	
            'created'			=>	'Vytvořeno|CompanyOrderTemplate|created|datetime|',
		),
		'posibility' => array(
            'show'		=>	'show|Detail|show',
		),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'		=> true,
			'languages'	=> 'false'
		)
	);
    
    function index(){
		 
            $this->loadModel('Company');
            $company_conditions =  array('Company.kos'=>0);
			if (isset($this->filtration_company_condition))
				$company_conditions = am($company_conditions, $this->filtration_company_condition);
			$this->set('company_list',			$this->Company->find('list',array('conditions'=>$company_conditions,'order'=>'name ASC')));
            unset($this->Company);
        
        if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			// set JS and Style
			$this->set('scripts',array('uploader/uploader','clearbox/clearbox'));
			$this->set('styles',array('../js/uploader/uploader','../js/clearbox/clearbox'));
			
			// set FastLinks
			$this->set('fastlinks',array('ATEP'=>'/','Firmy'=>'#',$this->renderSetting['page_caption']=>'#'));
			$this->render('../system/index');
           
		}
	}
    
    function show($template_id){
        $detail = $this->CompanyOrderTemplate->read('company_id',$template_id);
        echo $this->requestAction('companies/template_edit/'.$detail['CompanyOrderTemplate']['company_id'].'/'.$template_id.'/show');
		die();
    }
    
	
}
?>