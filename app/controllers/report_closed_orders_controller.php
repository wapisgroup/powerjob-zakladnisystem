<?php
//Configure::write('debug',1);
class ReportClosedOrdersController extends AppController {
	var $name = 'ReportClosedOrders';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload','Email');
	var $uses = array('CompanyOrderItem');
	var $renderSetting = array(
		//'SQLfields' => array('id','Company.name','updated','created','status','SalesManager.name','ClientManager.name','Coordinator.name','mesto','SettingStav.namre'),
		'SQLfields' => '*,
            CONCAT_WS(" + ",CompanyOrderItem.count_of_free_position,CompanyOrderItem.count_of_substitute) as objednavka,
            CONCAT_WS(" / ",CompanyOrderItem.count_of_free_position,
                IF(
                 CompanyOrderItem.order_status = 1,
                    (SELECT 
                        COUNT(c.id) 
                     FROM wapis__connection_client_requirements as c
                     Where 
                        c.company_id = CompanyOrderItem.company_id AND 
                        c.requirements_for_recruitment_id = CompanyOrderItem.id AND
                        type = 2 AND
                        objednavka = 1
                     ),
                 CompanyOrderItem.statistika
                )
            ) as statistika,
            IF((CompanyOrderItem.start_datetime < NOW() && CompanyOrderItem.order_status IN (-1,1)), 1 , 0) as expire                
        ',
        'SQLcondition'=>array(
            'order_status'=>array(2,3)
        ),		
		'bindModel' => array('belongsTo' => array(
            'Company'=>array('foreignKey'=>'company_id')
		)),
		'controller'=> 'report_closed_orders',
		'page_caption'=>'Objednávky - uzavřené/zrušené',
		'sortBy'=>'CompanyOrderItem.order_date.DESC',
		'top_action' => array(),
		'filtration' => array(
            'CompanyOrderItem-company_id'	=>	'select|Společnost|company_list',      
        ),
		'items' => array(
			'id'				=>	'ID|CompanyOrderItem|id|text|',
			'firma'				=>	'Firma|Company|name|text|',
			'order'				=>	'Profese|CompanyOrderItem|name|text|',	
			'order_date'		=>	'Datum objednávky|CompanyOrderItem|order_date|date|',
			'start_datetime'	=>	'Datum nástupu|CompanyOrderItem|start_datetime|date|',
			'objednavka'		=>	'Objednávka|0|objednavka|text|',
			'statistika'		=>	'Statistika|0|statistika|text|',
			'start_comment'		=>	'Komentář|CompanyOrderItem|start_comment|text|orez#30',
            'status'			=>	'Status|CompanyOrderItem|order_status|var|order_stav_list',
            'created'			=>	'Vytvořeno|CompanyOrderItem|created|datetime|',
            'nabor'			    =>	'Nábor|CompanyOrderItem|nabor|text|value_to_yes_no#long'
		),
        'class_tr'=>array(
            'class'=>'color_red',
            'model'=>0,
            'col'=>'expire',
            'value'=>1
        ),
		'posibility' => array(
			'pozice'		=>	'nabor_pozice|Obsazení pracovních pozic|pozice',
			//'show'		    =>	'show|Detail šablony|show'
		),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'		=> true,
			'languages'	=> 'false'
		)
	);
    
    
    function index(){
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
		    $this->set('company_list',$this->get_list('Company'));
          
			// set JS and Style
			$this->set('scripts',array('uploader/uploader','clearbox/clearbox'));
			$this->set('styles',array('../js/uploader/uploader','../js/clearbox/clearbox'));
			
			// set FastLinks
			$this->set('fastlinks',array('ATEP'=>'/','Nábor'=>'#',$this->renderSetting['page_caption']=>'#'));
			$this->render('../system/index');
		}
	}
    
	/**
	 * Zobrazeni obsazovani pracovnich pozic
	 * @param integer $order_item_id
	 * @return 
	 */
    public function nabor_pozice($order_item_id, $render = 'nabor'){
        echo $this->requestAction('orders_cast_posts/nabor_pozice/'.$order_item_id.'/'.$render.'/');
		die();
    }   
     
}
?>