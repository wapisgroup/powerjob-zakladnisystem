<?php
//Configure::write('debug',2);

$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');
define('CURRENT_YEAR', $_GET['current_year']); 
define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);

class AccommodationControlInvoicesController  extends AppController {
	var $name = 'AccommodationControlInvoices';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('ClientWorkingHour');
	var $mesic = CURRENT_MONTH;
	var $rok = CURRENT_YEAR;
	var $renderSetting = array(
		'bindModel'	=> array(
			'belongsTo'	=>	array('Accommodation','Company','Client')
		),
		'controller'	=>	'accommodation_control_invoices',
		'SQLfields' 	=>	'*,count(client_id) as pocet_ubytovanych,
                            SUM(accommodation_days_count) as pocet_ubytovacich_dnu,
                            (
                                CASE Accommodation.type
                                    WHEN 1 /* cena za den */
                                        THEN ROUND(Accommodation.price * SUM(accommodation_days_count),2)
                                    WHEN 2  /* cena za mesic */  
                                        THEN SUM(cena_za_ubytovani)
                                    ELSE "Chyba!!!"
                                END
                            ) as cena_celkem     
                                  
                            ',
		'SQLcondition'	=>  array(
			'ClientWorkingHour.kos'=>0,
            'ClientWorkingHour.year'=> CURRENT_YEAR,
			'ClientWorkingHour.month'=> CURRENT_MONTH,
			'ClientWorkingHour.accommodation_id !='=> 0
		),
        'count_col'=>'ClientWorkingHour.accommodation_id',
		'page_caption'=>'Report kontroly fakturace ubytovani',
		'sortBy'=>'Accommodation.name.ASC',
		'group_by'=>'ClientWorkingHour.accommodation_id',
		'top_action' => array(),
		'filtration' => array(
            'Client-name'		                    =>	'text|Klient|',
			'ClientWorkingHour-company_id'	        =>	'select|Společnost|company_list',
		    'GET-current_month'						=>	'select|Měsíc|mesice_list',
			'ClientWorkingHour-accommodation_id'	=>	'select|Ubytovna|accommodation_list',   
            'Company-client_manager_id|coordinator_id|coordinator_id2-cmkoo'		=>	'select|CM/KOO|cm_koo_list',
            'GET-current_year'						=>	'select|Rok|actual_years_list'
        ),
		'items' => array(
			'id'				=>	'ID|ClientWorkingHour|id|hidden|',
			'accommodation'		=>	'Ubytovna|Accommodation|name|text|',
			'city'		        =>	'Město|Accommodation|city|text|',
			'year'			    =>	'Rok|ClientWorkingHour|year|text|',
			'month'			    =>	'Měsíc|ClientWorkingHour|month|var|mesice_list',
            'pocet_ubytovanych'	=>	'Počet ubytovaných|0|pocet_ubytovanych|text|',
			'cena_celkem'    	=>	'Cena celkem|0|cena_celkem|text|',
			'pocet_ubytovacich_dnu'    	=>	'Počet ubytovacich dnu celkem|0|pocet_ubytovacich_dnu|text|',
		),
		'posibility' => array(
            'show'		=>	'show|Detail položky|show',
            'print_detail'		=>	'print_detail|Tisk položky|print_detail',
		),
        'posibility_link' => array(
            'show'		=>	'ClientWorkingHour.accommodation_id/ClientWorkingHour.year/ClientWorkingHour.month',
            'print_detail'		=>	'ClientWorkingHour.accommodation_id/ClientWorkingHour.year/ClientWorkingHour.month',
		),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'	=> true,
			'languages'		=> true,
		)
	);

	

	function beforeRender(){
		parent::beforeRender();

		// úprava nadpisu reportu a přidání do něj datumu
        if(isset($this->viewVars['renderSetting']))
		   $this->set('spec_h1', $this->viewVars['renderSetting']['page_caption'].' za měsíc '.$this->mesice_list[ltrim($this->mesic,'0')].' a rok '.$this->rok);
	}
	
	function index(){
		// set FastLinks
		$this->set('fastlinks',array('ATEP'=>'/','Ubytování'=>'#',$this->viewVars['renderSetting']['page_caption']=>'#'));
		
		$this->loadModel('CmsUser');
		$this->set('cm_koo_list',$cm_koo_list = $this->CmsUser->find('list',array(
			'conditions'=>array(
				'kos'=>0,
				'status'=>1,
				'cms_group_id IN(3,4)'
			)
		)));
        
		
		$this->loadModel('Accommodation');
		$accommodation_list = $this->Accommodation->find('list',array(
			 'conditions'=> array('kos'=>0),
			 'order'=>'name ASC'
		 ));
		 $this->set('accommodation_list',$accommodation_list);
		 
      
  		$this->loadModel('Company');
		$company_list = $this->Company->find('list',array(
			 'conditions'=> array('kos'=>0,'status'=>1),
			 'order'=>'name ASC'
		 ));
		 $this->set('company_list',$company_list);
		 
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
		
	}
	
    /**
     * prehled umistenych zamestanncu
     */
    function show($accommodation_id,$year,$month){
		if ($accommodation_id != null){
			
			$this->ClientWorkingHour->bindModel(array(
			'belongsTo'=>array('Client')
		    ));
	
			$this->set('zamestnanci_list',$this->ClientWorkingHour->find('all',array(
				'conditions'=>array(
					'ClientWorkingHour.accommodation_id'=>$accommodation_id,
					'ClientWorkingHour.year'=>$year,
					'ClientWorkingHour.month'=>$month
				),
				'fields'=>array(
					'Client.name',
					'ClientWorkingHour.id',
					'ClientWorkingHour.year','ClientWorkingHour.month',
                    'ClientWorkingHour.cena_za_ubytovani','accommodation_days','accommodation_days_count'
				),
				'order'=>'Client.name ASC'
			)));

			unset($this->ClientWorkingHour);

			//render
			$this->render('show');	
	    }
		else 
			die('Bez id ? :-)');
		
		
	}
    
    /**
     * tisk prehledu zamestancu
     */
    function print_detail($accommodation_id,$year,$month){
        $this->layout = 'print';
        
        if ($accommodation_id != null){
            $this->loadModel('Accommodation');
			$this->set('detail',$this->Accommodation->read(array('name','city'),$accommodation_id));
            unset($this->Accommodation);
            
            
			$this->ClientWorkingHour->bindModel(array(
			'belongsTo'=>array('Client')
		    ));
	
			$this->set('zamestnanci_list',$this->ClientWorkingHour->find('all',array(
				'conditions'=>array(
					'ClientWorkingHour.accommodation_id'=>$accommodation_id,
					'ClientWorkingHour.year'=>$year,
					'ClientWorkingHour.month'=>$month
				),
				'fields'=>array(
					'Client.name',
					'ClientWorkingHour.id',
					'ClientWorkingHour.year','ClientWorkingHour.month',
                    'ClientWorkingHour.cena_za_ubytovani','accommodation_days','accommodation_days_count'
				),
				'order'=>'Client.name ASC'
			)));

			unset($this->ClientWorkingHour);

			//render
			$this->render('print');	
	    }
		else 
			die('Bez id ? :-)');
		
		
	}    	
    
    /**
     * detail jednotlivych dni kdy byl dany zamestannec ubytovan
     */
    function detail($id){
        $data = $this->ClientWorkingHour->read(array('year','month','accommodation_days','company_id'),$id);
        $this->set('data',$data);
        
         // load free days for state, month, year
		$this->loadModel('SettingStatSvatek');
		$this->set('svatky',$this->SettingStatSvatek->find('list', array(
            'conditions'=>array( 
                'setting_stat_id'	=> av(av($data['ClientWorkingHour']['company_id'],'Company'),'stat_id'), 
                'mesic'=> $data['ClientWorkingHour']['month'], 
                'rok'=> $data['ClientWorkingHour']['year'] 
            ),
            'fields'=>array('id','den'),
            'order'=>'den ASC'
        ))); 				
		
        //render
        $this->render('detail');
    }
}    
?>