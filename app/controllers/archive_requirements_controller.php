<?php
//Configure::write('debug',2);
class ArchiveRequirementsController  extends AppController {
	var $name = 'ArchiveRequirements';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Email','Upload');
	var $uses = array('RequirementsForRecruitment');
	var $renderSetting = array(
		'bindModel'	=> array(
			'belongsTo'=>array('Company','CmsUser','SettingCareerCat')
		),
		'controller'=>'archive_requirements',
		'SQLfields' => array('RequirementsForRecruitment.express','SettingCareerCat.name','Company.name','Company.self_manager_id','Company.coordinator_id','Company.client_manager_id','RequirementsForRecruitment.id','RequirementsForRecruitment.cms_user_id','RequirementsForRecruitment.created','RequirementsForRecruitment.name','count_of_free_position','count_of_substitute','salary','datum_interni','datum_externi','publikovani_typ',
			'(SELECT Count(`ConnectionClientRequirement`.`id`) FROM `wapis__connection_client_requirements` AS `ConnectionClientRequirement` WHERE (type=2 OR type=1)  AND ConnectionClientRequirement.requirements_for_recruitment_id=RequirementsForRecruitment.id) pocet_zam'
		),
		'SQLcondition'=> array(
			'RequirementsForRecruitment.publikovani_typ'=>3
		),
		'page_caption'=>'Archivace požadavků pro nábor',
		'sortBy'=>'Company.name.ASC',
		'top_action' => array(
			//'add_item'		=>	'Přidat|edit|Pridat popis|add',
		),
		'filtration' => array(
			'RequirementsForRecruitment-name|'			=>	'text|Pozice|',
			'RequirementsForRecruitment-company_id'		=>	'select|Společnost|company_list',
			//'RequirementsForRecruitment-publikovani_typ'=>	'select|Publikovani|publikovani_typ_list',
		),
		'items' => array(
			'id'				=>	'ID|RequirementsForRecruitment|id|hidden|',
			'cms_user_id'		=>	'CMS|RequirementsForRecruitment|cms_user_id|hidden|',
			'firma'				=>	'Firma|Company|name|text|',
			'name'				=>	'Pracovní pozice|RequirementsForRecruitment|name|text|',
			'pocet_mist'		=>	'Počet míst|RequirementsForRecruitment|count_of_free_position|text|',
			//'pocet_nahradniku'	=>	'Počet náhradníků|RequirementsForRecruitment|count_of_substitute|text|',
			'obsazeno'			=>	'Obsazenost|0|pocet_zam|text|',
			'kat'				=>	'Kategorie odměny|SettingCareerCat|name|text|',
			'publikovani_typ'	=>	'Publikování|RequirementsForRecruitment|publikovani_typ|var|publikovani_typ_list',
			'created'			=>	'Vytvořeno|RequirementsForRecruitment|created|date|',
			'interni'			=>	'Datum interní|RequirementsForRecruitment|datum_interni|date|',
			'externi'			=>	'Datum externí|RequirementsForRecruitment|datum_externi|date|',
             'express' => '#|RequirementsForRecruitment|express|text|status_to_ico#express'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editovat položku|edit',
			'pozice'		=>	'nabor_pozice|Obsazení pracovních pozic|pozice'
			//'attach'	=>	'attachs|Přílohy|attach',	
			//'delete'	=>	'trash|Odstranit položku|trash'		
			//'zamestnanci'	=>	'zamestnanci|Zemestnani lide|edit',			
		),
		'domwin_setting' => array(
		'sizes'			=>	'[800,900]',
		)	
	);

	function beforeFilter(){
		parent::beforeFilter();
		switch($this->logged_user['CmsGroup']['id']){
			case 3: // CM
				if (!isset($_GET['filtration_CompanyView-client_manager_id']))
					$_GET['filtration_Company-client_manager_id'] = $this->params['url']['filtration_Company-client_manager_id'] = $this->logged_user['CmsUser']['id'];
				break;
			// case 4: // coordinator
				// if (!isset($_GET['filtration_CompanyView-coordinator_id']))
					// $_GET['filtration_Company-coordinator_id'] = $this->params['url']['filtration_Company-coordinator_id'] = $this->logged_user['CmsUser']['id'];
				// break;
			case 2: // SM
				if (!isset($_GET['filtration_CompanyView-self_manager_id']))
					$_GET['filtration_Company-self_manager_id'] = $this->params['url']['filtration_Company-self_manager_id'] = $this->logged_user['CmsUser']['id'];
				break;
			default:
		}	
	}
	
	function index(){

		// set JS and Style
		$this->set('scripts',array('uploader/uploader','clearbox/clearbox'));
		$this->set('styles',array('../js/uploader/uploader','../js/clearbox/clearbox'));
		// set FastLinks
		$this->set('fastlinks',array('ATEP'=>'/','Nábor'=>'#','Archivace požadavků pro nábor'=>'#'));
		

		$this->loadModel('Company'); 	
		
		$this->set('company_list',		$this->Company->find('list',array('conditions'=>array('Company.kos'=>0),'order'=>array('Company.name ASC'))));
        
		unset($this->Company);

		//obsazeni :rendring
		foreach($this->viewVars['items'] as &$item){
			$obsazenych_mist = $item['0']['pocet_zam'];
			$pocet_mist = $item['RequirementsForRecruitment']['count_of_free_position'];
			$pocet_nahradniku = $item['RequirementsForRecruitment']['count_of_substitute'];

			$celkem_mozno_obsadit = $pocet_mist + $pocet_nahradniku;

			$item['0']['pocet_zam'] = $obsazenych_mist." z ".$celkem_mozno_obsadit;
			//novy pocet mist
			$item['RequirementsForRecruitment']['count_of_free_position'] = $pocet_mist.' + '.($pocet_nahradniku <> '' ? $pocet_nahradniku : 0);
		}
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		
		echo $this->requestAction('companies/nabor_edit/-1/' . $id);
		die();
	}
	
	function nabor_pozice($requirements_for_recruitment_id){
		echo $this->requestAction('report_requirements_for_recruitments/nabor_pozice/' . $requirements_for_recruitment_id);
		die();
	}

	
		
}
?>