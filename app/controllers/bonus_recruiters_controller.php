<?php
if (isset($_GET['current_month']) && $_GET['current_month'] < 10) $_GET['current_month'] = '0'.$_GET['current_month'];
define('CURRENT_YEAR', (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y'));
define('CURRENT_MONTH', (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:date('m'));
class BonusRecruitersController extends AppController {
	var $name = 'BonusRecruiters';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('BonusRecruiterView');
	var $renderSetting = array(
		'controller'=>'bonus_recruiters',
		//'bindModel'	=> array('belongsTo'=>array('CmsUser')),
		//'SQLfields' => array('LastLogin.id','CmsUser.name','LastLogin.ip','LastLogin.created'),
		'SQLfields' => array('recruiter_name', 'GROUP_CONCAT(" ",name,": ",vyplatit, " hod. -> odmena:",vyplatit*prize/250 ) as hodiny' , 'SUM(vyplatit*prize/250) as RDIN'),
		'page_caption'=>'Provize recruiteru',
		'sortBy'=>'BonusRecruiterView.year.DESC',
		'group_by'=>'BonusRecruiterView.recruiter_id',
		'top_action' => array(
	
		),
		'SQLcondition'	=>  array(
			"BonusRecruiterView.month" => '#CURRENT_MONTH#',
			"BonusRecruiterView.year" => '#CURRENT_YEAR#'
			
		),
		'filtration' => array(
			'GET-current_year'	=>	'select|Rok|actual_years_list',
			'GET-current_month'	=>	'select|Měsíc|mesice_list',
		),
		'items' => array(
			'recruiter_name'	=>	'Recruiter|BonusRecruiterView|recruiter_name|text|',
			'hodiny'				=>	'Odměna za|0|hodiny|text|',
			'rdin'				=>	'RDIN|0|RDIN|text|'
			
		),
		'posibility' => array(
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Provize recruiteru'=>'#'));

		// $this->loadModel('CmsUser');
		// $this->set('cms_user_list', $this->CmsUser->find('list', array('conditions'=>array('kos'=>0))));
		// unset($this->CmsUser);

		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
}
?>