<?php
//Configure::write('debug',1);

class ReportRequestForCertificatesController extends AppController {
	var $name = 'ReportRequestForCertificates';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email','Best','Hb');
	var $uses = array('RequestForCertificate');
	var $renderSetting = array(
		'bindModel'	=> array('belongsTo'=>array('Company','CmsUser','Client')),
		'controller'=>'report_request_for_certificates',
		'SQLfields' => '*',
        'page_caption'=>'Seznam žádosti na certifikáty',
		'sortBy'=>'RequestForCertificate.created.DESC',
		//'SQLcondition'=>array('CompanyMoneyValidity.platnost_do'=>'0000-00-00','RequestForCertificate.schvaleno'=>0,'RequestForCertificate.kos'=>0),
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Přidat nový požadavek|add',
            //'multi_edit'		=>	'Uhradit hromadně|paid_all|Uhradit hromadně|multi_edit',
            //'kb'		        =>	'Bankovní Export|kb|Bankovní Export|kb',
            //'company_exception'	=>	'Firmy vyjimky|company_exception|Firmy vyjimky|company_exception',
		),
		'filtration' => array(
			'RequestForCertificate-cms_user_id'		=>	'select|Koordinátor|cm_list',
			'RequestForCertificate-company_id'		=>	'select|Společnost|company_list',
			'RequestForCertificate-zamestnanec'		=>	'text|Zaměstnanec|',
			'RequestForCertificate-date_of_pickup'	=>	'select|Neuhrazeno|filtr_uhrazeno',
			'RequestForCertificate-type'	=>	'select|Typ|certificate_list',
		),
		'items' => array(
			'id'				=>	'ID|RequestForCertificate|id|hidden|',
			'cms_user_id'		=>	'Přidal|CmsUser|name|text|',
			'company'			=>	'Firma|Company|name|text|',
			'zamestnanec'		=>	'Zaměstnanec|Client|name|text|',
		//	'cislo_uctu'		=>	'Č. účtu|0|cislo_uctu|text|',
			'castka'			=>	'Částka|RequestForCertificate|amount|text|',
			'date_of_pickup'	=>	'Datum|RequestForCertificate|date_of_pickup|date|',
			'type'		        =>	'Typ|RequestForCertificate|setting_certificate_id|var|certificate_list',
			'created'			=>	'Vytvořeno|RequestForCertificate|created|date|'
		),
		'posibility' => array(
		//	'show'		=>	'show|Zobrazit aktivitu|show',			
			'edit'		=>	'edit|Editovat položku|edit',
            'pick_up'	=>	'pick_up|Převzetí certifikátu|pick_up',			
			'trash'		=>	'trash|Smazat položku|trash',			
		),
		'domwin_setting' => array(
			'sizes' 		=> '[900,900]',
			'scrollbars'	=> true,
			'languages'		=> 'false',
			'defined_lang'	=> 'false'
		),
        'checkbox_setting' => array(
			'model'			=>	'RequestForCertificate',
			'col'			=>	'date_of_pickup',
			'stav_array'	=>	array('0000-00-00'),
			'month_year'	=>	false
		),
	);
	
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#','Vystavené certifikáty'=>'#'));
		
		/*
		 * Spolecnost List pro filtraci
		 */
		$this->loadModel('Company'); 
		$company_conditions =  array('Company.kos'=>0);
		if (isset($this->filtration_company_condition))
			$company_conditions = am($company_conditions, $this->filtration_company_condition);
		$this->set('company_list',		$this->Company->find('list',array('conditions'=>$company_conditions,'order'=>'Company.name ASC')));
		unset($this->Company);
		
        
        /**
         * nacteni listu pro certifikaty
         * novy zpusob, kazdy model nyni prebira funkci get_list z app_modelu
         * pro ulehceni se tuto funkce jeste vyvolavame pres app_controller, pro set modelu a unset
         * zprehlednime tim kod
         */
        $this->set('certificate_list', $this->get_list('SettingCertificate'));
                
        
        
		/*
		 * Seznam CM a KOO List pro filtraci
		 */
		$this->loadModel('CmsUser');
		$this->set('cm_list',			$this->CmsUser->find('list',array('conditions'=>array('CmsUser.cms_group_id'=>array(3,4)))));
		unset($this->CmsUser);
		
		/*
		 * Start Render
		 */
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		if (empty($this->data)){
			$this->loadModel('Company');
			
			if ($this->logged_user['CmsGroup']['id'] == 3 || $this->logged_user['CmsGroup']['id']==4){ 
        		$company_conditions =  array('Company.kos'=>0);
        		if (isset($this->filtration_company_condition))
        			$company_conditions = am($company_conditions, $this->filtration_company_condition);
        		$this->set('company_list',		$this->Company->find('list',array('conditions'=>$company_conditions,'order'=>'Company.name ASC')));
        		unset($this->Company);
            
            } else if (in_array($this->logged_user['CmsGroup']['id'],array(1,5,6,7))){
				$this->set('company_list', $this->Company->find('list',array('conditions'=>array('Company.kos'=>0),'order'=>'Company.name')));
			}	

			unset($this->Company);
            
            
            /**
             * nacteni certificate list
             */
            $this->set('certificate_list', $this->get_list('SettingCertificate')); 
						
			if ($id != null){
				$this->RequestForCertificate->bindModel(array('belongsTo'=>array('Company','Client')));
				$this->data = $this->RequestForCertificate->read(null,$id);
                
				$this->loadModel('ConnectionClientRequirement'); 
				$this->ConnectionClientRequirement->bindModel(array(
					'belongsTo'=>array('Client')
				));
                
               $output = null;
			   $output = $this->ConnectionClientRequirement->find('list', array(
                    'fields'=>array('Client.id','Client.name'),
                    'conditions'=>array(
                        'OR'=>array(
                            'ConnectionClientRequirement.client_id'=>$this->data['RequestForCertificate']['client_id'],
                            'AND'=>array(
                                'ConnectionClientRequirement.company_id'=>$this->data['RequestForCertificate']['company_id'],
                                'ConnectionClientRequirement.type'=>2, 
                                'ConnectionClientRequirement.to'=>'0000-00-00', 
                                'Client.kos'=>0
                            )
                       )       
                    ), 
                    'order'=>'Client.name ASC',
                    'recursive'=>1                    
                ));	
				$this->set('client_list',$output);				
				
				
			} else {
				$this->set('client_list',array());
				$this->data = array('RequestForCertificate'=>array('cms_user_id'=>$this->logged_user['CmsUser']['id']));
			}
		} else {
			$this->RequestForCertificate->save($this->data);
			die(json_encode(array('result'=>true)));
		}
	}
    
    
     /**
     * Funkce ktera vyplnuje dle aktualniho datumu nebo zvoleneho sloupce datum
     * a zadani castky kolik certifikat stala?
     * 
     * @param $id 
     * @author Sol
     * @created 18.03.2009
     */
    function pick_up($id = null){
   	    if (empty($this->data)){
            $this->data = $this->RequestForCertificate->read(null,$id);
            
            //render
            $this->render('pick_up');
		}
        else {
        
            if ($this->data['RequestForCertificate']['date_of_pickup'] != null){
                $this->RequestForCertificate->save($this->data);

                //data pro historii
                $data = array(
                    'date_of_pickup'=>$this->data['RequestForCertificate']['date_of_pickup'],
                    'company_id'=>$this->data['RequestForCertificate']['company_id'],
                    'setting_certificate_id'=>$this->data['RequestForCertificate']['setting_certificate_id'],
                    'amount'=>$this->data['RequestForCertificate']['amount']
                );
                //odeslani akce pro zachyceni historie
                $this->requestAction('report_request_for_certificates/record_pick_up_client/'.$this->data['RequestForCertificate']['client_id'],array('data'=>$data));
             
    
    			die(json_encode(array('result'=>true)));
    		}
    		die(json_encode(array('result'=>false,'message'=>'Nebylo zvoleno datum')));
       }     
	}
    
  
    /**
     * slepa funkce pro historii
     * @param $client_id
     * @data $date_of_pickup
     * @data $company_id
     * @data $amount
     */
    function record_pick_up_client($client_id){
       return true;      
    }
    
 
}
?>