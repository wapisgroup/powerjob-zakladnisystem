<?php
	$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
	$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:date('m');

	define('CURRENT_YEAR', $_GET['current_year']);
	define('CURRENT_MONTH', (strlen($_GET['current_month']) == 1)?'0'.$_GET['current_month']:$_GET['current_month']);

class ReportBonusCooCmController extends AppController {
	var $name = 'ReportBonusCooCm';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('BonusCooCmView');
	var $renderSetting = array(
		'controller'=>'report_bonus_coo_cm',
		'SQLfields' => '*',
		'SQLcondition' => 'year =#CURRENT_YEAR# AND month =#CURRENT_MONTH# ',
		'page_caption'=>'Odměny pro CM, KOO - nábor',
		'sortBy'=>'BonusCooCmView.recruiter_name.ASC',
		'top_action' => array(		
		),
		'filtration' => array(
			// 'GET-recruiter'								=>	'select|Uživatel|user_list',
			'GET-current_year'							=>	'select|Rok|actual_years_list',
			'GET-current_month'							=>	'select|Měsíc|mesice_list',
		),
		'items' => array(
			'client_name'		=>	'Klient|BonusCooCmView|client_name|text|',
			'company_name'		=>	'Firma|BonusCooCmView|company_name|text|',
			'user'		=>	'User|BonusCooCmView|recruiter_name|text|',
			'user_typ'		=>	'Typ|BonusCooCmView|umistovatel__cms_group_id|var|cms_group_list',
			'month'		=>	'Měsíc|BonusCooCmView|month|text|',
			'year'		=>	'Rok|BonusCooCmView|year|text|',
			
			'celkem_hodin'		=>	'Počet odp. hod. celkem|BonusCooCmView|celkem_hodin|text|',
			'vyplatit'		=>	'Odměna za odp. hodin|BonusCooCmView|vyplatit|text|',
			'zbyva'		=>	'Zbyva|BonusCooCmView|zbyva|text|',
			'odmena'	=>	'Odměna|BonusCooCmView|odmena|text|',
			'procenta'	=>	'% z RDIN|BonusCooCmView|procenta|text|',
			
		),
		'posibility' => array(
		)
	);
		
	function index(){		
		$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#','Odměny pro CM, KOO - nábor'=>'#'));
		$this->loadModel('CmsUser');
		$this->set('user_list',$this->CmsUser->find('list',array(
				'order'=>'name asc'
		)));
		
		$this->set('cms_group_list', array(
			3 => 'CM',
			4 => 'COO'
		));

			
			
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
		
	}
	
}
?>