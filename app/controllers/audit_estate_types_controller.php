<?php
Configure::write('debug',1);
class AuditEstateTypesController extends AppController {
	var $name = 'AuditEstateTypes';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('AuditEstateType');
	var $renderSetting = array(
		'controller'=>'audit_estate_types',
		'SQLfields' => '*',
		'page_caption'=>'Typy majetku',
		'sortBy'=>'AuditEstateType.created.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat|add',
		),
		'filtration' => array(
		//	'SettingAccommodationType-status'	=>	'select|Stav|select_stav_zadosti',
		//	'SettingAccommodationType-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|AuditEstateType|id|text|',
			'name'		=>	'Název|AuditEstateType|name|text|',
			'updated'	=>	'Upraveno|AuditEstateType|updated|datetime|',
			'created'	=>	'Vytvořeno|AuditEstateType|created|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
			'delete'	=>	'trash|Do košiku|trash'			
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Nastavení průmyslu'=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null)
				$this->data = $this->AuditEstateType->read(null,$id);
                
			$this->render('edit');
		} else {
			$this->AuditEstateType->save($this->data);
			die();
		}
	}
}
?>