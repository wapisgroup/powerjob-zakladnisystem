<?php
class SettingStavsController extends AppController {
	var $name = 'SettingStavs';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('SettingStav');
	var $renderSetting = array(
		'controller'=>'setting_stavs',
		'SQLfields' => array('id','name','updated','created','status','poradi'),
		'page_caption'=>'Nastavení stavů firem',
		'sortBy'=>'SettingStav.poradi.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
		//	'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
		//	'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
		//	'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
		//	'SettingStav-status'	=>	'select|Stav|select_stav_zadosti',
		//	'SettingStav-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|SettingStav|id|text|',
			'poradi'	=>	'Pořadí|SettingStav|poradi|text|',
			'name'		=>	'Název|SettingStav|name|text|',
			'updated'	=>	'Upraveno|SettingStav|updated|datetime|',
			'created'	=>	'Vytvořeno|SettingStav|created|datetime|'
		),
		'posibility' => array(
			'status'	=> 	'status|Změna stavu|status',
			'edit'		=>	'edit|Editace položky|edit',
			'trash'	=>	'trash|Do košiku|trash'
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Nastavení stavů firem'=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null)
				$this->data = $this->SettingStav->read(null,$id);
			$this->render('edit');
		} else {
			$this->SettingStav->save($this->data);
			die();
		}
	}
}
?>