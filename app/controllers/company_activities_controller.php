<?php
//Configure::write('debug',2);
class CompanyActivitiesController extends AppController {
	var $name = 'CompanyActivities';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('CompanyActivity');
	var $renderSetting = array(
		'bindModel'	=> array('belongsTo'=>array('Company','CompanyContact','CmsUser','SettingActivityType'=>array('className'=>'SettingActivityType','foreignKey'=>'company_activity_type_id'))),
		'controller'=>'company_activities',
		'SQLfields' => array('CmsUser.name','Company.name','CompanyActivity.id','CompanyActivity.name','CompanyContact.name','SettingActivityType.name','CompanyActivity.text','CompanyActivity.updated','CompanyActivity.created','CompanyActivity.company_id','CompanyActivity.mesto','CompanyActivity.activity_datetime'),
		'page_caption'=>'Report - aktivity v podnicích',
		'sortBy'=>'CompanyActivity.created.DESC',
		'top_action' => array(
			// caption|url|description|permission
			//'add_item'		=>	'Přidat|edit|Pridat popis|add',
		),
		'filtration' => array(
			'CompanyActivity-name'					=>	'text|Název aktivity|',
			'CompanyActivity-company_contact_id'	=>	'select|Kontakt|company_contact_list',
			'CompanyActivity-cms_user_id'			=>	'select|Vytvořil|cms_user_list',
			'CompanyActivity-company_id'			=>	'select|Společnost|company_list',
			'CompanyActivity-activity_datetime'		=>	'date_f_t|Datum|',
		),
		'items' => array(
			'id'		=>	'ID|CompanyActivity|id|hidden|',
			'company'	=>	'Firma|Company|name|text|',
			'name'		=>	'Název|CompanyActivity|name|text|',
			'typ_aktiv'	=> 	'Typ aktivity|SettingActivityType|name|text|',
			'kontakt'	=> 	'Kontaktní osoba|CompanyContact|name|text|',
			'creator'	=> 	'Vytvořil|CmsUser|name|text|',
			'date_acti'	=>	'Datum aktivity|CompanyActivity|activity_datetime|date|',
			'created'	=>	'Vytvořeno|CompanyActivity|created|date|'
		),
		'posibility' => array(
		//	'show'		=>	'show|Zobrazit aktivitu|show',			
			'edit'		=>	'edit|Editovat položku|edit',			
		),
        'thunderbird'=>true
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Firmy'=>'/companies/','Aktivity ve firmách'=>'#'));
		
			$this->loadModel('CmsUser'); 
			$this->loadModel('Company'); 
			$this->loadModel('CompanyContact'); 
			$clist = array();
			
			$creator = array($this->logged_user['CmsUser']['id'] => '-- Pouze moje --');
			$this->CmsUser->bindModel(array('belongsTo'=>array('CmsGroup')));
			foreach($this->CmsUser->find( 'all', array( 'conditions'=>array( 'CmsUser.status'=>1, 'CmsUser.kos'=>0 ), 'fields'=>array( 'CmsUser.name', 'CmsGroup.name', 'CmsUser.id' ), 'order'=>'CmsGroup.name, CmsUser.name ASC' ) ) as $item){
				$clist[$item['CmsGroup']['name']][$item['CmsUser']['id']] = $item['CmsUser']['name']; 
			}
			foreach($clist as $key => $group){
				$creator['DISABLED_'.$key] = $key;
				foreach($group as $k => $user)
					if ($k != $this->logged_user['CmsUser']['id'])
						$creator[$k] = $user;
			}
	
			$this->set('cms_user_list',	$creator);
			
			$company_conditions =  array('Company.kos'=>0);
			if (isset($this->filtration_company_condition))
				$company_conditions = am($company_conditions, $this->filtration_company_condition);
			$this->set('company_list',			$this->Company->find('list',array('conditions'=>$company_conditions)));
			$this->set('company_contact_list',	$this->CompanyContact->find('list',array('conditions'=>array('CompanyContact.kos'=>0))));
			unset($this->CmsUser);
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function show($id = null){
		$this->autoLayout = false;
		// START nacteni  zakladnich modelu pro view
		$this->loadModel('CompanyTask'); $this->CompanyTask = new CompanyTask();
		// END nacteni  zakladnich modelu pro view
		
		$this->CompanyActivity->bindModel(array('belongsTo'=>array('CompanyContact','Company','CmsUser'), 'hasOne'=>array('CompanyTask')));
		$this->CompanyActivity->CompanyTask->bindModel(array('belongsTo'=>array('SettingTaskType' => array('className' => 'SettingTaskType','foreignKey' => 'company_tasks_type_id','fields'=>array('SettingTaskType.name')))));
		// ??? doresit
		$fields = array(
			'CompanyActivity.name', 'CompanyActivity.activity_datetime','CompanyActivity.mesto', 'CompanyActivity.text',
			'Company.name', 'CmsUser.name','CompanyContact.name',
			'CompanyTask.name', 'CompanyTask.termin', 'CompanyTask.text'
		);
		$this->data = $this->CompanyActivity->find(array('CompanyActivity.id'=>$id),null,null,2);
		//pr($this->data['CompanyTask']);
		$this->render('show');
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		// START nacteni  zakladnich modelu pro editaci
		$this->loadModel('CompanyTask'); $this->CompanyTask = new CompanyTask();
		// END nacteni  zakladnich modelu pro editaci
		if (empty($this->data)){			
			// START nacteni  zakladnich modelu a dat pro vyctove listy
			$this->loadModel('CompanyContact'); $this->CompanyContact = new CompanyContact();
			$this->loadModel('SettingActivityType'); $this->SettingActivityType = new SettingActivityType();
			$this->loadModel('SettingTaskType'); $this->SettingTaskType = new SettingTaskType();
			$this->set('company_activity_type_list', $this->SettingActivityType->find('list',array('conditions'=>array('kos'=>0), 'order'=>'poradi ASC')));
			$this->set('company_task_type_list', $this->SettingTaskType->find('list',array('conditions'=>array('kos'=>0), 'order'=>'poradi ASC')));
			unset($this->SettingActivityType);
			unset($this->SettingTaskType);
			// END nacteni  zakladnich modelu a dat pro vyctove listy
						
			if ($id != null){
				$this->CompanyActivity->bindModel(array('hasOne'=>array('CompanyTask'),'belongsTo'=>array('Company')));
				$this->data = am($this->data, $this->CompanyActivity->read(null,$id));
				$this->set('comapany_contact_list',$this->CompanyContact->find('list',array('conditions'=>array('CompanyContact.kos'=>0,'CompanyContact.company_id'=>$this->data['Company']['id']))));
			} else {
				$this->set('comapany_contact_list',array());
				$this->loadModel('Company'); $this->Company = new Company();
				// ??? musi se udelat podminka pro vypis
				$this->set('company_list', $this->Company->find('list'));
			}
			unset($this->CompanyContact);
			$this->render('edit');
		} else {
			// START pridani cms_user_id pokud se vytvari aktivita nebo ukol
			if (empty($this->data['CompanyActivity']['id'])) 	$this->data['CompanyActivity']['cms_user_id'] 	= $this->logged_user['CmsUser']['id'];
			if (empty($this->data['CompanyTask']['id'])) 		$this->data['CompanyTask']['cms_user_id'] 		= $this->logged_user['CmsUser']['id'];
			// END pridani cms_user_id pokud se vytvari aktivita nebo ukol
			// ulozeni aktivity
			$this->CompanyActivity->save($this->data);
			// START ulozeni ukolu, pokud je k dane aktivite nadefinovan
			if (!empty($this->data['CompanyTask']['name'])){
				$this->data['CompanyTask']['company_activity_id'] = $this->CompanyActivity->id;
				$this->CompanyTask->save($this->data);
			}
			// END ulozeni ukolu, pokud je k dane aktivite nadefinovan
			// odeslani informacniho emailu ze je nova aktivita
			if (empty($this->data['CompanyActivity']['id'])){
				
				$this->loadModel('Company');
				$company = $this->Company->read(array('name','self_manager_id','coordinator_id','client_manager_id'),$this->data['CompanyActivity']['company_id']);
				unset($this->Company);


				$this->loadModel('CmsUser'); 
				$email_list = $this->CmsUser->find('list',array('fields'=>array('id','email'), 
				'conditions'=>array(
						'id IN ('.$company['Company']['self_manager_id'].')'
				)));
				unset($this->CmsUser);
                $email_list[] = $this->logged_user['CmsUser']['email'];

				$replace_list = array(
					'##Company.name##' 			=>$company['Company']['name'],
					'##CompanyActivity.name##' 	=>$this->data['CompanyActivity']['name'],
					'##CmsUser.name##' 			=>$this->logged_user['CmsUser']['name']
				);
				//ID3
				$this->Email->send_from_template_new(0,$email_list,$replace_list);
			}
			die();
			
		}
	}
	
	function load_contact_city($company_id = null){
		if ($company_id == null){
			die(json_encode(array('contact'=>array(), 'city'=>'')));
		} else {
			$this->loadModel('Company'); $this->Company = new Company();
			$this->loadModel('CompanyContact'); $this->CompanyContact = new CompanyContact();
			
			$city = $this->Company->read(array('mesto'),$company_id);
			$contact_list = $this->CompanyContact->find('list', array('conditions'=>array('company_id'=>$company_id)));
			
			unset($this->Company);
			unset($this->CompanyContact);
			
			die(json_encode(array('city'=>$city['Company']['mesto'], 'contact'=>$contact_list)));
		}
	}
	
}
?>