<?php

class CampaignItemsCallcentrumsController extends AppController {
	var $name = 'CampaignItemsCallcentrums';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput','Wysiwyg');
	var $components = array('ViewIndex','RequestHandler','Upload','Email');
	var $uses = array('CampaignItem');
	var $renderSetting = array(
		'SQLfields' => array('CampaignItem.id','CampaignItem.name','CampaignGroup.name','CampaignItem.stav','CampaignItem.pozice','CampaignItem.mail','CampaignItem.mobil','CampaignItem.telefon','Company.name','CampaignItem.created'),
		'controller'=> 'campaign_items_callcentrums',
		'bindModel'=>array('belongsTo'=>array('Company','CampaignGroup')),
		'page_caption'=>'Kampaně - CallCentrum',
		'sortBy'=>'CampaignItem.name.ASC',
		'top_action' => array(
			// caption|url|description|permission
		//	'add_item'	=>	'Přidat|edit|Přidání nové kampaně|add',
		//	'group'		=>	'Kampaně|groups|Přidat kampaň|groups',
		//	'email'		=> 	'Odeslání Emailu|email_send|Odeslání hromadného emailu kampaně|email',
			//'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'SQLcondition'=>array('OR'=>array(
			'CampaignItem.stav !=' => 1,
			'CampaignItem.mail' => ''
		), 'CampaignItem.stav !=5'),
		'filtration' => array(
			'CampaignItem-name|'				=>	'text|Jméno|',
			'CampaignItem-city|'				=>	'text|Město|'
		),
		'items' => array(
			'id'			=>	'ID|CampaignItem|id|text|',
			'kampan'		=>	'Kampaň|CampaignGroup|name|text|',
			'company'		=>	'Společnost|Company|name|text|',
			'pozice'		=>	'Pozice|CampaignItem|pozice|text|',
			'mail'			=>	'Email|CampaignItem|mail|text|',
			'mobil'			=>	'Mobil|CampaignItem|mobil|text|',
			'telefon'		=>	'Telefon|CampaignItem|telefon|text|',
			'stav'			=>	'Stav|CampaignItem|stav|viewVars|campaign_item_stavs',
		
			//'updated'		=>	'Změněno|CampaignItem|updated|datetime|',
			'created'		=>	'Vytvořeno|CampaignItem|created|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
			'stav'		=>	'campaign_stav|Přidat aktivitu|message'	
		), 
        'domwin_setting' => array(
            'sizes' => '[900,400]', 
            'scrollbars' => true, 
            'languages' => 'false' 
        )
	);
	
	/**
	 * (non-PHPdoc)
	 * @see FTP.FASTEST.CZ/secret/app/AppController#beforeRender()
	 */
	function beforeRender(){
		parent::beforeRender();
		$this->set('kampane_stav',array(
			0 => 'Založeno',
			1 => 'Odesláno'
		));		
	}
	
	/**
	 * Zaklandni view z komponenty
	 * @return unknown_type
	 */
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/', $this->renderSetting['page_caption']=>'#'));
				
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			
			$this->set('scripts',array('wysiwyg_old/wswg','uploader/uploader','clearbox/clearbox'));
			$this->set('styles',array('../js/wysiwyg_old/wswg','../js/uploader/uploader','../js/clearbox/clearbox'));
			$this->render('../system/index');
		}
	}
	
	/**
	 * Volani editace z kontroleru campaign_items_tippers
	 * @param $id, ID zaznamu pozadavku
	 * @return void
	 */
	function edit($id = null){
		echo $this->requestAction('/campaign_items_tippers/edit/' . $id);
		die();
	}
	
	/**
	 * Volani zmena stavu, resp. pridani aktivity, z kontroleru campaign_items_tippers
	 * @param $id, ID zaznamu pozadavku
	 * @return void
	 */
	function campaign_stav($id = null) {
		echo $this->requestAction('/campaign_items_tippers/campaign_stav/' . $id);
		die();
	}
		
 	
    
    
   
    
   
    
    
	
}
?>