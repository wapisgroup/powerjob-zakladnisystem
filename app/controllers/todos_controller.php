<?php
Configure::write('debug',1);
/**
 * TODO z atep DB byl vypnut
 * Vše se nyní zadává přes client.wapis.cz
 * Tento report slouzi prouze pro chekovani podrobnosti
 */
class TodosController extends AppController {
	var $name = 'Todos';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('WapisTask');
	var $renderSetting = array(
		//'SQLfields' => array('id','Todo.name','Todo.type_id','updated','fakturovano','priorita_list','created','status','termin','todo_stav_list','cena'),
        'SQLfields'=>'*',
        'bindModel'=>array('belongsTo'=>array('WapisClientProject'=>array('foreignKey'=>'client_project_id'))),
        'SQLcondition'=>array(
            'WapisTask.task_invoice_id'=>0,
            'WapisTask.client_id' => 1,//a&t
            //'WapisTask.client_project_id' => 2,//atep
            'WapisTask.kos'=>0,
        ),
		'controller'=> 'todos',
		'page_caption'=>'Úkolovník ATEP',
		'sortBy'=>'WapisTask.id.DESC',
		'top_action' => array(
			// caption|url|description|permission
			//'add_item'		=>	'Přidat|edit|Pridat úkol|add'
		),
		'filtration' => array(
			'WapisTask-state'	=>	'select|Stav|wapis_stav_list',
            //'Todo-next|'		=>	'select|Další|next_list',
		//	'Todo-name'				=>	'text|Název firmy|',
			
		),
		'items' => array(
			'id'		=>	'ID|WapisTask|id|text|',
			'name'		=>	'Název|WapisTask|name|text|',
            'projekt'	=>	'Projekt|WapisClientProject|name|text|',
			'description'	=>	'Popis|WapisTask|description|text|orez#100',
            'stav'		=>	'Stav|WapisTask|state|var|wapis_stav_list',
			'created'	=>	'Vytvořeno|WapisTask|created|datetime|',
			'updated'	=>	'Upraveno|WapisTask|updated|datetime|',
			//'priorita'	=>	'Priorita|Todo|priorita_list|var|priorita_list',
			
			//'cena'		=>	'Cena|Todo|cena|text|',
			//'fakturovano'		=>	'Fakturovano|Todo|fakturovano|var|ano_ne_checkbox',
		),
		'posibility' => array(
			//'status'	=> 	'status|Změna stavu|status',
			'edit'		=>	'edit|Detail položky|edit',
            //'attach'	=>	'attachs|Přílohy|edit',			
			//'delete'	=>	'trash|Odstranit položku|delete'
		),
		'domwin_setting' => array(
			'sizes' 		=> '[700,900]',
			'scrollbars'	=> true,
			'languages'		=> true,
			'defined_lang'	=> "['cz','en','de']",
		)
	);
    
    
	function index(){
			
        $this->set('wapis_stav_list',array('Zadáno', 'Probíhá', 'Hotovo', 'Zrušeno'));    
            
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
		
			$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Úkolovník ATEP'=>'#'));
			
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
				
			if ($id != null){
				$this->data = $this->WapisTask->read(null,$id);		
			} 
			$this->render('edit_new');
		} else {
				
			//$this->Todo->save($this->data);
			die();
		}
	}
	
	/**
 	* Seznam priloh
 	*
	* @param $client_id
 	* @return view
 	* @access public
	**/
	function attachs($todo_id){
		$this->autoLayout = false;
		$this->loadModel('TodoAttachment'); 
		$this->TodoAttachment->bindModel(array('belongsTo'=>array('SettingAttachmentType')));
		$this->set('attachment_list', $this->TodoAttachment->findAll(array('TodoAttachment.todo_id'=>$todo_id,'TodoAttachment.kos'=>0)));
		$this->set('todo_id',$todo_id);
		unset($this->TodoAttachment);
		$this->render('attachs/index');
	}
	
	/**
 	* Editace priloh
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function attachs_edit($todo_id = null, $id = null){
		$this->autoLayout = false;
		$this->loadModel('TodoAttachment'); 
		if (empty($this->data)){
			$this->loadModel('SettingAttachmentType');
			$this->set('setting_attachment_type_list',$this->SettingAttachmentType->find('list',array('conditions'=>array('kos'=>0),'order'=>'poradi ASC')));
			unset($this->SettingAttachmentType);
			$this->data['TodoAttachment']['todo_id'] = $todo_id;
			if ($id != null){
				$this->data = $this->TodoAttachment->read(null,$id);
			}
			$this->render('attachs/edit');
		} else {
			$this->TodoAttachment->save($this->data);
			$this->attachs($this->data['TodoAttachment']['todo_id']);
		}
		unset($this->TodoAttachment);
	}
	/**
 	* Presun prilohy do kose
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function attachs_trash($todo_id, $id){
		$this->loadModel('TodoAttachment');
		$this->TodoAttachment->save(array('TodoAttachment'=>array('kos'=>1,'id'=>$id)));
		$this->attachs($todo_id);
		unset($this->TodoAttachment);
	}
	
	/**
 	* Nahrani prilohy na ftp
 	*
 	* @return view
 	* @access public
	**/
	function upload_attach() {
		$this->Upload->set('data_upload',$_FILES['upload_file']);
		if ($this->Upload->doit(json_decode($this->data['upload']['setting'],true))){
			echo json_encode(array('upload_file'=>array('name'=>$this->Upload->get('outputFilename')),'return'=>true));
		} else 
			echo json_encode(array('return'=>false,'message'=>$this->Upload->get('error_message')));		
		die();
	} 
	
	/**
 	* Stazeni prilohy
 	*
	* @param $file
	* @param $file_name
 	* @return download file
 	* @access public
	**/
	function  attachs_download($file,$file_name){
		$pripona = strtolower(end(Explode(".", $file)));
		$file = strtr($file,array("|"=>"/"));
		$filesize = filesize('./uploaded/'.$file);
		$cesta = "http://".$_SERVER['SERVER_NAME']."/uploaded/".$file;
				 
		header("Pragma: public"); // požadováno
	    header("Expires: 0");
	    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	    header("Cache-Control: private",false); // požadováno u některých prohlížečů
	    header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . $filesize);
		Header('Content-Type: application/octet-stream');
		Header('Content-Disposition: attachment; filename="'.$file_name.'.'.$pripona.'"');
		readfile($cesta);
		die();

	}
	
	
}
?>