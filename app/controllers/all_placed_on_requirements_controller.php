<?php
//Configure::write('debug',1);
$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');
@define('CURRENT_YEAR', $_GET['current_year']); 
@define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);

class AllPlacedOnRequirementsController  extends AppController {
	var $name = 'AllPlacedOnRequirements';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('ConnectionClientRequirement');
	var $renderSetting = array(
		'bindModel'	=> array(
			'belongsTo'	=>	array(
                'Client','Company','CompanyWorkPosition',
                'RequirementsForRecruitment','CmsUser', 'CompanyMoneyItem'
            )
		),
		'controller'	=>	'all_placed_on_requirements',
		'SQLfields' 	=>	'*',
		'SQLcondition'	=>  array(
            "((DATE_FORMAT(ConnectionClientRequirement.from,'%Y-%m') = '#CURRENT_YEAR#-#CURRENT_MONTH#'))"
			//'((DATE_FORMAT(ConnectionClientRequirement.to,"%Y-%m") >= "#CURRENT_YEAR#-#CURRENT_MONTH#") OR (ConnectionClientRequirement.to = "0000-00-00 00:00"))',
	
        ),
		'page_caption'=>'Všichni umistění uchazeči',
		'sortBy'=>'ConnectionClientRequirement.created.DESC',
		'top_action' => array('export_excel' => 'Export Excel|export_excel|Export Excel|export_excel'),
		'filtration' => array(
            'Client-name'		=>	'text|Klient|',
			'ConnectionClientRequirement-company_id'	=>	'select|Společnost|company_list',
			//'CompanyWorkPosition-name|'			=>	'text|Profese|',	
			'GET-current_month'	=>	'select|Měsíc|mesice_list',
            'ConnectionClientRequirement-next|'		=>	'select|Další|next_list',
            'Company-client_manager_id|coordinator_id|coordinator_id2-cmkoo'		=>	'select|CM/KOO|cm_koo_list',
			'ConnectionClientRequirement-cms_user_id'		=>	'select|Umístil|umistil_list',
            'GET-current_year'	=>	'select|Rok|actual_years_list',
            
        ),
		'items' => array(
			'id'				=>	'ID|ConnectionClientRequirement|id|hidden|',
            'client_id'			=>	'Klient ID|Client|id|hidden|',
            'umisteni'			=>	'Datum umístění|ConnectionClientRequirement|created|date|',
            'client'			=>	'Klient|Client|name|text|',
			'cms_user'			=>	'Umístil|CmsUser|name|text|',
			'company'			=>	'Společnost|Company|name|text|',
			'req'				=>	'Požadavek|RequirementsForRecruitment|name|text|',
			'work_position'		=>	'Profese|CompanyWorkPosition|name|text|',
			'money_item'		=>	'Forma|CompanyMoneyItem|name|text|',
			'nastup'			=>	'Datum nástupu|ConnectionClientRequirement|from|date|',
			'ukonceni'			=>	'Datum ukončení|ConnectionClientRequirement|to|date|',

		),
		'posibility' => array(
			'client_info'	=>	'client_info|Karta klienta|client_info'
		),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'	=> true,
			'languages'		=> true,
		)
	);
    
    
    function beforeFilter(){
        parent::beforeFilter();
        
        if(isset($_GET['filtration_ConnectionClientRequirement-next|']) && !empty($_GET['filtration_ConnectionClientRequirement-next|'])){
            list($col,$value) = explode('-',$_GET['filtration_ConnectionClientRequirement-next|']);

            $this->params['url']['filtration_ConnectionClientRequirement-'.$col] = $value;
            
            unset($this->params['url']['filtration_ConnectionClientRequirement-next|']); 
            unset($col);
            unset($value);
        }

      
        //pr($_GET);    
    }

	
	function index(){
		// set FastLinks
		$this->set('fastlinks',array('ATEP'=>'/','Nábor'=>'#',$this->renderSetting['page_caption']=>'#'));
	    $this->loadModel('CmsUser');
		$this->set('cm_koo_list',$cm_koo_list = $this->CmsUser->find('list',array(
			'conditions'=>array(
				'kos'=>0,
				'status'=>1,
				'cms_group_id IN(3,4)'
			)
		)));
        
        $this->set('umistil_list',$umistil_list = $this->CmsUser->find('list',array(
			'conditions'=>array(
				'kos'=>0,
				'status'=>1
			),
            'order'=>'name ASC'
		)));
      
  		$this->loadModel('Company');
		$company_list = $this->Company->find('list',array(
			 'conditions'=> array('kos'=>0,'status'=>1),
			 'order'=>'name ASC'
		 ));
		 $this->set('company_list',$company_list);
		 
         
        /**
        * dalsi moznosit filtrace
        * moznsosti zadavame sloupec-stav => nazev moznosti
        */
        $this->set('next_list',array(
            'type-2'=> 'Zaměstnání'
        ));   
        
        
        /**
         * kompletni forma odmeny
         */
		foreach($this->viewVars['items'] as &$item){
         	//nastaveni forem odmeny a zobrazeni ubytovani a dopravy
			if(isset($item['CompanyMoneyItem']) && $item['CompanyMoneyItem']['name'] != ''){
					$doprava = 'Doprava '.($item['CompanyMoneyItem']['doprava'] != 0 ? 'Ano' : 'Ne');
					$ubytovani = 'Ubytování '.($item['CompanyMoneyItem']['cena_ubytovani_na_mesic'] != 0 ? 'Ano' : 'Ne');
	                $item['CompanyMoneyItem']['forma'] = $item['CompanyMoneyItem']['name'];
					$item['CompanyMoneyItem']['name'] = $item['CompanyMoneyItem']['id'].'-'.$item['CompanyMoneyItem']['name'].' - '.$ubytovani.' / '.$doprava;     
			}
		}
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
		
	}
	
    
    
	//zobrazeni karty klienta
	function client_info($id = null){
		echo $this->requestAction('clients/edit/'.$id.'/domwin/only_show');
		die();
	}
    
    
    
    /**
     * funkce pro vytvoreni exportu do excelu - CSV
     * podle filtrace vyber dane klienty a vygeneruj je do csv
     */
    function export_excel(){
        //Configure::write('debug',1);

         
       $start = microtime ();
       $start = explode ( " " , $start );
       $start = $start [ 1 ]+ $start [ 0 ];  
        
        $this->renderSetting['items'] = array(
            //'umisteni'			=>	'Datum umístění|ConnectionClientRequirement|created|date|',
            'client'			=>	'Klient|Client|name|text|',
			'cms_user'			=>	'Umístil|CmsUser|name|text|',
			'company'			=>	'Společnost|Company|name|text|',
			//'req'				=>	'Požadavek|RequirementsForRecruitment|name|text|',
			'work_position'		=>	'Profese|CompanyWorkPosition|name|text|',
			//'money_item'		=>	'Forma|CompanyMoneyItem|name|text|',
			'nastup'			=>	'Datum nástupu|ConnectionClientRequirement|from|date|',
			'ukonceni'			=>	'Datum ukončení|ConnectionClientRequirement|to|date|',
        );
         
         $fields_sql = array(
            "Client.name",
            "CmsUser.name",
            "Company.name",
            "ConnectionClientRequirement.from",
            "ConnectionClientRequirement.to",
            "CompanyWorkPosition.name"
         );
               
        $criteria = $this->ViewIndex->filtration();      
        
       
        //$this->loadModel('ClientView2');
      
   
         header("Pragma: public"); // požadováno
         header("Expires: 0");
         header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
         header("Cache-Control: private",false); // požadováno u některých prohlížečů
         header("Content-Transfer-Encoding: binary");
         Header('Content-Type: application/octet-stream');
         Header('Content-Disposition: attachment; filename="'.date('Ymd_His').'.csv"');
     
      
          /*
         * Celkovy pocet zaznamu
         */
         
        $limit = 100;   
        $page = 1;        
        $count = $this->ConnectionClientRequirement->find('first',
        	array(
        		'fields' =>  array("COUNT(DISTINCT ConnectionClientRequirement.id) as count"),
        		'conditions'=>$criteria,
        		'recursive'	=>1
        	)
        );    
        $count = $count[0]['count'];

        // hlavicka
        foreach($this->renderSetting['items'] as &$item_setting){
            list($caption, $model, $col, $type, $fnc) = explode('|',$item_setting);
        	$item_setting = compact(array("caption", "model","col","type","fnc"));	
            if($type != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',$caption).'";'; 
        }
        echo "\n";   
        unset($item_setting, $caption, $model, $col, $type, $fnc);
        
        $str_array=array("<br/>"=>', ',':'=>',',';'=>',',''=>'', '#'=>' ');
        /*
         * Cyklicky vypis dat po $limit zaznamu
         */
       
        for ($exported = 0; $exported < $count; $exported += $limit){
             $this->ConnectionClientRequirement->bindModel(array(
    			'belongsTo'	=>	array(
                    'Client','Company','CmsUser','CompanyWorkPosition'
                )
    		)); 
            foreach($this->ConnectionClientRequirement->find('all',array(
                   'fields'=>$fields_sql,
                    'conditions'=>$criteria,
                    'limit'=>$limit,
                    'page'=>$page,
                    'recursive'=>1,
                    'order'=>'ConnectionClientRequirement.from ASC'
            )) as $item){
                foreach($this->renderSetting['items'] as $key => $td){
                    if($td['type'] != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',strtr($this->ViewIndex->generate_td($item,$td,$this->viewVars),$str_array)).'";';     
                }
                echo "\n";  
            }
            $page++;
        }
       
        //time
         $end = microtime ();
         $end = explode ( " " , $end );
         $end = $end [ 1 ]+ $end [ 0 ]; 
         echo 'Generate in '.($end - $start).'s';
         echo "\n"; 
        
        die();
    }

    
}
?>