<?php
//Configure::write('debug',2);
class AccountantClientEmployeesController extends AppController {
	var $name = 'AccountantClientEmployees';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload','Email');
	var $uses = array('ClientView','Client');
	var $renderSetting = array(
		'bindModel' => array(
            'hasOne' => array(
				'ConnectionClientCareerItem' => array('foreignKey' => 'client_id'),
				'ConnectionClientRecruiter' => array('className' => 'ConnectionClientRecruiter', 'foreignKey' => 'client_id'),
				'ConnectionClientRequirement' => array(
					'className' => 'ConnectionClientRequirement', 
					'foreignKey' => 'client_id',
					'conditions' => array(
						'ConnectionClientRequirement.kos'=>0,
						'ConnectionClientRequirement.to'=>'0000-00-00',
						'(ConnectionClientRequirement.type = 2)'
						
					)
				)
            ),
            'belongsTo' =>array(
                'CompanyMoneyItem' => array(),
            )
        ),
		//'SQLfields' => '*,GROUP_CONCAT(profese_name SEPARATOR "<br/>") as Profese ',
		'SQLfields' => '*',
		'SQLcondition' => array(
			'ClientView.stav'=>2,
			'ConnectionClientRequirement.to'=>'0000-00-00'
		),
		'controller'=> 'accountant_client_employees',
		'page_caption'=>'Zaměstnanci AT - pro účetní',
		//'count_group_by' => 'ClientView.id',
		'group_by' => 'ClientView.id',
		'sortBy'=>'ClientView.id.ASC',
	
		'top_action' => array(
             'import_csv' => 'Import CSV|import_csv|Import CSV|import_csv', 
        ),
		'filtration' => array(
			'ClientView-name|'		=>	'text|Jméno|',
			//'ClientView-stav'		=>	'select|Status|stav_client_list',
			'ConnectionClientCareerItem-setting_career_item_id'		=>	'select|Profese|profese_list',
			'ConnectionClientRecruiter-cms_user_id#admin_ctrl'		=>	'select|Recruiter|cms_user_list',
			'ConnectionClientRequirement-company_id'		=>	'select|Firma|company_list',
			'ClientView-next|'		=>	'select|Další|next_list'
			//'ClientView-telefon1|'	=>	'text|Telefon|',
		),
		'items' => array(
			'id'		=>	'ID|ClientView|id|text|',
			'name'		=>	'Jméno|ClientView|name|text|',
			'firma'		=>	'Firma|ClientView|company|text|',
			'forma_odmeny'		=>	'Forma odměny|CompanyMoneyItem|name|text|',
			'os_cislo'		=>	'Os číslo|ClientView|os_cislo|text|',
			'zbyva_dovolene'		=>	'Zbývá dovolené|ClientView|zbyva_dovolene|text|',
			'cislo_uctu'		=>	'Číslo účtu|ClientView|cislo_uctu|text|',
            'cislo_uctu_control'	=>	'#|ClientView|cislo_uctu_control|text|status_to_ico2#chybna_dokumentace-Nezkontrolováno číslo účtu'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
			'attach'	=>	'attachs|Přílohy|attach',
			'message'	=>	'messages|Zprávy|message',
			'vyhodit'	=>	'rozvazat_prac_pomer|Rozvázat pracovní poměr|rozvazat_prac_pomer',
			'zmena_pp'	=>	'zmena_pp|Změna pracovního poměru|zmena_pp',
            'ucetni_dokumentace'	=>	'ucetni_dokumentace|Účetní dokumentace|ucetni_dokumentace',
			'delete'	=>	'trash|Odstranit položku|trash'			
		),
		'domwin_setting' => array(
			'sizes' 		=> '[800,900]',
			'scrollbars'	=> true,
			'languages'		=> true,
		)
	);
    
    var $subQuery = array(
        1 => '(CompanyMoneyItem.checkbox_pracovni_smlouva = 1 OR CompanyMoneyItem.checkbox_dohoda = 1) AND ClientView.os_cislo IS NULL'
    );
	
    function beforeFilter(){
        parent::beforeFilter();
        
        if(isset($_GET['filtration_ClientView-next|']) && !empty($_GET['filtration_ClientView-next|'])){
            list($col,$value) = explode('-',$_GET['filtration_ClientView-next|']);

            $this->params['url']['filtration_ClientView-'.$col] = $value;
            
            unset($this->params['url']['filtration_ClientView-next|']); 
            unset($col);
            unset($value);
        }

      
        //pr($_GET);    
    }

    
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/',$this->renderSetting['page_caption'] =>'#'));
			
		$this->loadModel('CmsUser');
		$this->set('cms_user_list', $this->CmsUser->find('list',
			array('conditions'=>array('kos'=>0), 'order'=>'name ASC')
		));

		$this->set('cm_coo_list', $this->CmsUser->find('list',
			array('conditions'=>array('cms_group_id IN (3,4)'),'fields'=>array('name','name'), 'order'=>'name ASC')
		));

		$this->loadModel('Company'); 	
		$company_conditions = array('Company.kos'=>0);
		
		if (isset($this->filtration_company_condition))
			$company_conditions = am($company_conditions, $this->filtration_company_condition);
		 
		$this->set('company_list',		$this->Company->find('list',array(
			'conditions'=>$company_conditions,
			'order'=>array('Company.name ASC')
		)));
		unset($this->Company);
        
        /**
        * dalsi moznosit filtrace
        * moznsosti zadavame sloupec-stav => nazev moznosti
        */
        $this->set('next_list',array(
            'express-1'=> 'Expresní klienti',
            'import_adresa-NOTNULL'=> 'Neprázdna importní adresa',
            'os_cislo-SUBQUERY_1'=> '11xx nemají os. číslo'
        ));
        
        
        //změna profese podle nastaveneho company_work_position_id
		foreach($this->viewVars['items'] as &$item){
            
			//nastaveni forem odmeny a zobrazeni ubytovani a dopravy
			if(isset($item['CompanyMoneyItem'])){
				
					$doprava = 'Doprava '.($item['CompanyMoneyItem']['doprava'] != 0 ? 'Ano' : 'Ne');
					$ubytovani = 'Ubytování '.($item['CompanyMoneyItem']['cena_ubytovani_na_mesic'] != 0 ? 'Ano' : 'Ne');
	                $item['CompanyMoneyItem']['forma'] = $item['CompanyMoneyItem']['name'];
					$item['CompanyMoneyItem']['name'] = $item['CompanyMoneyItem']['name'].' - '.$ubytovani.' / '.$doprava;;
			
			}
		}


		$this->loadModel('SettingCareerItem');
		$this->set('profese_list', $this->SettingCareerItem->find('list',
			array('conditions'=>array('kos'=>0), 'order'=>'name ASC')
		));

			
				
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->set('scripts',array('uploader/uploader'));
			$this->set('styles',array('../js/uploader/uploader'));
			$this->render('../system/index');
		}
	}
	
	
	
	function edit($id = null,$domwin = null, $show = null){
		echo $this->requestAction('clients/edit/'.$id.'/'.$domwin.'/'.$show);
		die();
	}

	
	
	
	/**
 	* Seznam priloh
 	*
	* @param $client_id
 	* @return view
 	* @access public
	**/
	function attachs($client_id){
		echo $this->requestAction('clients/attachs/'.$client_id);
		die();
	}
	
	
	
	
	function rozvazat_prac_pomer($client_id){
		echo $this->requestAction('clients/rozvazat_prac_pomer/'.$client_id);
		die();
	}
	

	// funkce na zmenu pracovniho pomeru
	function zmena_pp($client_id = null,$company_id = null,$company_work_position_id = null){
		echo $this->requestAction('clients/zmena_pp/'.$client_id.'/'.$company_id.'/'.$company_work_position_id);
		die();
	}
    
    
    
	// funkce na uceti dokumentaci umistena v dochazkach
	function ucetni_dokumentace ($connection_client_requirement_id = null, $forma = null){
		echo $this->requestAction('employees/ucetni_dokumentace/'.$connection_client_requirement_id.'/'.$forma);
		die();
	}
    
    
    function import_csv(){
        $this->render('import_csv');
    }
    
    /**
     * funkce pro import
     */
    function import(){
         $this->loadModel('ClientImportDovolena');
         $this->ClientImportDovolena->query('TRUNCATE wapis__client_import_dovolenas');
         $count = 0;
         if (($handle = fopen($_FILES['upload_file']['tmp_name'], "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                if(isset($data[0])){
                    $client_name = $data[0]; //jméno klienta
                    $os_cislo = isset($data[3]) ? $data[3] : ''; //os cislo
                    $pp = isset($data[6]) ? $data[6] : ''; //pracovni profese
                    $zbyva_dovolene = isset($data[16]) ? $data[16] : 0; //nenulové položky, csv udělalo automaticky
                    
                    /**
                     * řádky které mají nějaké jméno a pracovní poměr a osobni_cislo
                     */
                    if($client_name != '' && $pp != '' && $os_cislo != ''){
                        $this->ClientImportDovolena->id = null;
                          $to_save = array(
                              'name'=>iconv('Windows-1250','UTF-8',trim($client_name)),
                              'os_cislo'=>strtr(trim($os_cislo),array(' '=>'')),
                              'zbyva'=>$zbyva_dovolene
                          );
                        $this->ClientImportDovolena->save($to_save);
                        $count ++;  
                    }
                    //pr($data);
                } 
            }
        }
        die(json_encode(array('result'=>true,'upload_file'=>array('name'=>$count))));        
    } 
    
    
    /**
     * funkce ktera paruje naimportovane klienty
     * a aktualizuje os cisla a zustatek dovolene
     */
	function load_row(){
	   $this->loadModel('Client');
	   $this->loadModel('ClientImportDovolena');
       
       $find = $this->ClientImportDovolena->find('first',array('conditions'=>array('import'=>0)));
       
       /**
        * porad je koho parovat
        */
       if($find){
           $cl = $this->Client->findByOsCislo($find['ClientImportDovolena']['os_cislo']);
           
           /**
            * klient jiz ma pridelene oscislo pouze aktualizujem pocet zbyvajici dovolene
            */
           if($cl){
                $this->Client->id = $cl['Client']['id'];
                $this->Client->saveField('zbyva_dovolene',$find['ClientImportDovolena']['zbyva']);
           } 
           else{
                /**
                 * klient nema pridelen os_cislo
                 * musime jej zkusit najit a obe tyto hodnoty zaktualizovat
                 */ 
                $cl2 = $this->Client->findByName($find['ClientImportDovolena']['name']);
                if($cl2){
                    $this->Client->updateAll(
                        array(
                            'Client.os_cislo' => "'".$find['ClientImportDovolena']['os_cislo']."'",
                            'Client.zbyva_dovolene' => "'".$find['ClientImportDovolena']['zbyva']."'"
                        ),
                        array('Client.id' =>  $cl2['Client']['id'])
                    );
                }
           }
           $this->ClientImportDovolena->id = $find['ClientImportDovolena']['id'];
           $this->ClientImportDovolena->saveField('import',1);
           die(json_encode(array('result'=>true)));  
       }
       else{
         /**
          * konec skriptu tabulka byla sparovana
          */
          die(json_encode(array('result'=>true,'done'=>true)));  
       }
       
	}
    

}
?>