<?php
//Configure::write('debug',2);
class ReportRequirementsForRecruitmentsController  extends AppController {
	var $name = 'ReportRequirementsForRecruitments';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Email','Upload');
	var $uses = array('RequirementsForRecruitment');
	var $renderSetting = array(
		'bindModel'	=> array(
			'belongsTo'=>array('Company','CmsUser','SettingCareerCat')
		),
		'controller'=>'report_requirements_for_recruitments',
		'SQLfields' => array('RequirementsForRecruitment.express','SettingCareerCat.name','Company.name','Company.self_manager_id','Company.coordinator_id','Company.client_manager_id','RequirementsForRecruitment.id','RequirementsForRecruitment.cms_user_id','RequirementsForRecruitment.created','RequirementsForRecruitment.name','count_of_free_position','count_of_substitute','salary','datum_interni','datum_externi','publikovani_typ',
			'(SELECT Count(`ConnectionClientRequirement`.`id`) FROM `wapis__connection_client_requirements` AS `ConnectionClientRequirement` WHERE (type=2 OR type=1)  AND ConnectionClientRequirement.requirements_for_recruitment_id=RequirementsForRecruitment.id) pocet_zam'
		),
		'SQLcondition'=> array(
			'RequirementsForRecruitment.publikovani_typ !='=>3
		),
		'page_caption'=>'Editace požadavků pro nábor',
		'sortBy'=>'Company.name.ASC',
		'top_action' => array(
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
            'add_express_item' => 'Přidat Express|edit_express|Pridat popis|add_express' 
		),
		'filtration' => array(
			'RequirementsForRecruitment-name|'			=>	'text|Pozice|',
			'RequirementsForRecruitment-company_id'		=>	'select|Společnost|company_list',
			'RequirementsForRecruitment-publikovani_typ'=>	'select|Publikovani|publikovani_typ_list',
		),
		'items' => array(
			'id'				=>	'ID|RequirementsForRecruitment|id|hidden|',
			'cms_user_id'		=>	'CMS|RequirementsForRecruitment|cms_user_id|hidden|',
			'firma'				=>	'Firma|Company|name|text|',
			'name'				=>	'Pracovní pozice|RequirementsForRecruitment|name|text|',
			'pocet_mist'		=>	'Počet míst|RequirementsForRecruitment|count_of_free_position|text|',
			//'pocet_nahradniku'	=>	'Počet náhradníků|RequirementsForRecruitment|count_of_substitute|text|',
			'obsazeno'			=>	'Obsazenost|0|pocet_zam|text|',
			'kat'				=>	'Kategorie odměny|SettingCareerCat|name|text|',
			'publikovani_typ'	=>	'Publikování|RequirementsForRecruitment|publikovani_typ|var|publikovani_typ_list',
			//'nabizena_mzda'		=>	'Nabízená mzda|RequirementsForRecruitment|salary|text|',
			'created'			=>	'Vytvořeno|RequirementsForRecruitment|created|date|',
			'interni'			=>	'Datum interní|RequirementsForRecruitment|datum_interni|date|',
			'externi'			=>	'Datum externí|RequirementsForRecruitment|datum_externi|date|',
            'express' => '#|RequirementsForRecruitment|express|text|status_to_ico#express'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editovat položku|edit',
			'pozice'		=>	'nabor_pozice|Obsazení pracovních pozic|pozice',
			'attach'	=>	'attachs|Přílohy|attach',	
			'delete'	=>	'trash|Odstranit položku|trash'		
			//'zamestnanci'	=>	'zamestnanci|Zemestnani lide|edit',			
		),
		'domwin_setting' => array(
		'sizes'			=>	'[800,900]',
		)	
	);

	function beforeFilter(){
		parent::beforeFilter();
		switch($this->logged_user['CmsGroup']['id']){
			case 3: // CM
				if (!isset($_GET['filtration_CompanyView-client_manager_id']))
					$_GET['filtration_Company-client_manager_id'] = $this->params['url']['filtration_Company-client_manager_id'] = $this->logged_user['CmsUser']['id'];
				break;
			// case 4: // coordinator
				// if (!isset($_GET['filtration_CompanyView-coordinator_id']))
					// $_GET['filtration_Company-coordinator_id'] = $this->params['url']['filtration_Company-coordinator_id'] = $this->logged_user['CmsUser']['id'];
				// break;
			case 2: // SM
				if (!isset($_GET['filtration_CompanyView-self_manager_id']))
					$_GET['filtration_Company-self_manager_id'] = $this->params['url']['filtration_Company-self_manager_id'] = $this->logged_user['CmsUser']['id'];
				break;
			default:
		}	
	}
	
	function index(){

		// set JS and Style
		$this->set('scripts',array('uploader/uploader','clearbox/clearbox'));
		$this->set('styles',array('../js/uploader/uploader','../js/clearbox/clearbox'));
		// set FastLinks
		$this->set('fastlinks',array('ATEP'=>'/','Nábor'=>'#','Editace požadavků pro nábor'=>'#'));
		
		$this->loadModel('CmsUser'); 	$this->CmsUser = new CmsUser();
		$this->loadModel('Company'); 	$this->Company = new Company();
		
		$this->set('sm_list',			$this->CmsUser->find('list',array('conditions'=>array('CmsUser.status'=>1,'CmsUser.kos'=>0,'CmsUser.cms_group_id'=>2))));
		$this->set('cm_list',			$this->CmsUser->find('list',array('conditions'=>array('CmsUser.status'=>1,'CmsUser.kos'=>0,'CmsUser.cms_group_id'=>3))));
		
		$company_conditions =  array('Company.kos'=>0);
		if (isset($this->filtration_company_condition))
			$company_conditions = am($company_conditions, $this->filtration_company_condition);
		$this->set('company_list',		$this->Company->find('list',array('conditions'=>$company_conditions,'order'=>array('Company.name ASC'))));
        
		unset($this->CmsUser);
		unset($this->Company);

		//obsazeni :rendring
		foreach($this->viewVars['items'] as &$item){
			$obsazenych_mist = $item['0']['pocet_zam'];
			$pocet_mist = $item['RequirementsForRecruitment']['count_of_free_position'];
			$pocet_nahradniku = $item['RequirementsForRecruitment']['count_of_substitute'];

			$celkem_mozno_obsadit = $pocet_mist + $pocet_nahradniku;

			$item['0']['pocet_zam'] = $obsazenych_mist." z ".$celkem_mozno_obsadit;
			//novy pocet mist
			$item['RequirementsForRecruitment']['count_of_free_position'] = $pocet_mist.' + '.($pocet_nahradniku <> '' ? $pocet_nahradniku : 0);
		}
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		
		echo $this->requestAction('companies/nabor_edit/-1/' . $id);
		die();
	}
    
    //expresni pridani pozadavku
    function edit_express($company_id = null, $id =null,$print = null)
    {
        
        $this->set('print',false);

		// nacteni potrebnych DB modelu
		$this->loadModel('RequirementsForRecruitment'); 
		$this->loadModel('Company'); 
		$this->set('CmsGroupId',$this->logged_user['CmsGroup']['id']);
	
		//nastaveni layoutu pro tiskove sestavy
		if($print != null){
			$this->autoLayout = true;
			$this->layout = 'print';
			$this->set('print',true);
		}


		if (empty($this->data)){
			
			$this->loadModel('SettingCareerItem'); 
			$this->set('setting_career_list', $this->SettingCareerItem->find('list',array('order'=>'name ASC')));
			unset($this->SettingCareerItem);
			
			// load kategorii a jejich prize
			$this->loadModel('SettingCareerCat'); 
			$career_cats = $this->SettingCareerCat->find('all',array(
					'fields'=>array(
						'id',
						'name',
						'prize'
					),
					'order'=>'name ASC'
			));

			$carrer_cat_list = array();
			$carrer_cat_title = array();
			if ($career_cats){
				foreach($career_cats as $cat_id=>$item){
					$carrer_cat_list[$item['SettingCareerCat']['id']] = $item['SettingCareerCat']['name'];
					$carrer_cat_title[$item['SettingCareerCat']['id']] = $item['SettingCareerCat']['name'].'|'.$item['SettingCareerCat']['prize'];
				}
			}
			$this->set('carrer_cat_list', $carrer_cat_list);
			$this->set('carrer_cat_title', $carrer_cat_title);

			//zobraz firmy jen ktere patri pod nej a je CM,SM,COO
			if(!in_array($this->logged_user["CmsGroup"]["id"] ,array(1,5,8,9))){
				$this->set('company_list', $this->Company->find('list',array('conditions'=>array('AND'=>array('kos'=>0,'OR'=>array('client_manager_id'=>$this->logged_user["CmsUser"]["id"],'self_manager_id'=>$this->logged_user["CmsUser"]["id"],'coordinator_id'=>$this->logged_user["CmsUser"]["id"],'coordinator_id2'=>$this->logged_user["CmsUser"]["id"]))),'order'=>'name ASC')));
			// adminum vsechny
			} else
				$this->set('company_list', $this->Company->find('list',array('order'=>'name ASC','conditions'=>array('kos'=>0))));

		
			$this->data['RequirementsForRecruitment']['company_id'] = $company_id;
			$this->set('company_position_list',array());
			

			
			$this->set('z_reportu', true);		


			 //render
            $this->render('edit_express'); 


		} else { 
        //Save
			if($this->data['RequirementsForRecruitment']['id']=="")
				$this->data['RequirementsForRecruitment']['cms_user_id'] = $this->logged_user['CmsUser']['id'];
			
            $this->data['RequirementsForRecruitment']['express'] = 1;

			if ($this->RequirementsForRecruitment->save($this->data))
				die(json_encode(array('result'=>true)));
			else
				die(json_encode(array('result'=>false)));
			//$this->nabor($this->data['RequirementsForRecruitment']['company_id']);
			die();
		}
      
    }
	
	function zamestnanci($requirements_for_recruitment_id){
		$detail = $this->RequirementsForRecruitment->read(null,$requirements_for_recruitment_id);
		$this->set('detail',$detail);
		$this->render('zamestnanci_list');
		
		
		
		//pr($zamestnanci_list);
	}
	
	function zam_refresh($requirements_for_recruitment_id, $year =null, $month=null){
   	if ($year == null || $month == null) die(); 
		$detail = $this->RequirementsForRecruitment->read(null,$requirements_for_recruitment_id);

		$this->loadModel('ConnectionClientRequirement');
		$this->ConnectionClientRequirement->bindModel(array(
			'belongsTo'=>array('Client'),
			'hasOne'	=>	array(
            'ClientWorkingHour' => array( 'conditions' => 
                    array('ClientWorkingHour.year'=>$year,'ClientWorkingHour.month'=>$month)))
		    ));
		 if($month<10) $month="0".$month;
			$from=	'((DATE_FORMAT(ConnectionClientRequirement.from,"%Y-%m")<= "'.$year.'-'.$month.'"))';
			$to=	'((DATE_FORMAT(ConnectionClientRequirement.to,"%Y-%m") >= "'.$year.'-'.$month.'") OR (ConnectionClientRequirement.to = "0000-00-00 00:00"))';
  	
		$zamestnanci_list = $this->ConnectionClientRequirement->find(
			'all', 
			array(
				'conditions'=>array('ConnectionClientRequirement.type'=>2,'ConnectionClientRequirement.requirements_for_recruitment_id'=>$requirements_for_recruitment_id,$from,$to),
				'fields'=>array(
					'ConnectionClientRequirement.id',
					'ClientWorkingHour.svatky',
					'ClientWorkingHour.pracovni_neschopnost',
					'ClientWorkingHour.prescasy',
					'ClientWorkingHour.vikendy',
					'ClientWorkingHour.dovolena',
					'ClientWorkingHour.celkem_hodin',
					'Client.id',
					'Client.name',
					'ConnectionClientRequirement.from'
				),
				'order'=>'ConnectionClientRequirement.id ASC'
			)
		);
		$this->set('zamestnanci_list',$zamestnanci_list);
		$this->set('detail',$detail);
		$this->set('rok',$year);
		$this->set('mesic',$month);
		
		$this->render('zamestnanci_list_refresh');
		//pr($zamestnanci_list);
	}
	
	/**
 	* Returns a view of "odpracovane hodiny"
 	*
	* @param $connection_client_requirement_id (INT)
	* @param $year (DATE(Y))
	* @param $month DATE(m))
	* @param $inner (STRING)
 	* @return view
 	* @access public
	**/
	public function odpracovane_hodiny($connection_client_requirement_id = null, $year = null, $month = null, $inner = null){
		if (empty($this->data)){
			if ($connection_client_requirement_id == null || $year == null || $month== null){
				die('Chyba, neznami cinitele');
			}
			
			// loadmodel && bindModel
			$this->loadModel('ConnectionClientRequirement');
			$this->ConnectionClientRequirement->bindModel(array(
				'belongsTo'	=>	array('RequirementsForRecruitment','Client'),
				'hasOne'	=>	array('ClientWorkingHour' => array('conditions' =>array('ClientWorkingHour.year'=>$year,'ClientWorkingHour.month'=>$month)))
			));
				
			// read from db
			$this->data = $this->ConnectionClientRequirement->read(null,$connection_client_requirement_id);
			$this->data['ClientWorkingHour']['connection_client_requirement_id'] = $connection_client_requirement_id;
			//pr($this->data);
			//die();
			
			// if not exist ClientWorkingHour load dota from RequirementsForRecruitment
			if (empty($this->data['ClientWorkingHour']['id'])){
				$transfer_arrray_con_rec = array('client_id', 'company_money_item_id', 'requirements_for_recruitment_id', ); 		
				$transfer_arrray_rec = array( 'company_id', 'standard_hours', 'setting_shift_working_id', 'company_work_position_id', 'including_of_accommodation', 'including_of_meals_ticket' ); 		
				foreach($transfer_arrray_con_rec as $transform) $this->data['ClientWorkingHour'][$transform] = $this->data['ConnectionClientRequirement'][$transform];
				foreach($transfer_arrray_rec as $transform)		$this->data['ClientWorkingHour'][$transform] = $this->data['RequirementsForRecruitment'][$transform];
			}
			
			// load calculation for work_position
			$money_items = $this->load_calculation_for_prefese($this->data['ClientWorkingHour']['company_work_position_id'],$year, $month, false);
			$money_item_list = array();
			$money_item_title = array();
			if ($money_items){
				foreach($money_items as $money_item){
					$money_item_list[$money_item['id']] = $money_item['name'].' - '.$money_item['cista_mzda_z_pracovni_smlouvy_na_hodinu'].'/'.$money_item['cista_mzda_faktura_zivnostnika_na_hodinu'].'/'.$money_item['cista_mzda_dohoda_na_hodinu'].'/'.$money_item['cista_mzda_cash_na_hodinu'];
					$money_item_title[$money_item['id']] = $money_item['name'].'|'.$money_item['stravenka'];
				}
			}
			$this->set('money_item_list', $money_item_list);
			$this->set('money_item_title', $money_item_title);
			
			// set disabled || enabled sub_salary
			$disabled_arr = array();
			if (!empty($this->data['ClientWorkingHour']['company_money_item_id'])){
				for ($i = 0; $i < 4; $i++) $disabled_arr[$i+1] = ($money_item_title[$this->data['ClientWorkingHour']['company_money_item_id']][$i] == 0)?'disabled':null;
				$this->data['ClientWorkingHour']['max_salary'] = array_sum(explode('/',av(explode(' - ',$money_item_list[$this->data['ClientWorkingHour']['company_money_item_id']]),1)));
				$this->data['ClientWorkingHour']['stravenka'] = av(explode('|',$money_item_title[$this->data['ClientWorkingHour']['company_money_item_id']]),1);
			} else {
				for ($i = 0; $i < 4; $i++) $disabled_arr[$i+1] = 'disabled';
				$this->data['ClientWorkingHour']['max_salary'] = 0;
				$this->data['ClientWorkingHour']['stravenka'] = 0;
			}
			$this->set('sub_salary_disabled',$disabled_arr);
				
			// load list of accomodations, in future will be  add condition for cms_user_id
			$this->loadModel('Accommodation');
			$this->set('accommodation_list',$this->Accommodation->find('list',array('conditions'=>array('Accommodation.kos'=>0,))));
			$this->set('accommodation_title_list',$p = $this->Accommodation->find('superlist',array('conditions'=>array('Accommodation.kos'=>0),'fields'=>array('id','type','price'),'separator'=>'|')));
			unset($this->Accommodation);
			      
			// load list of work position and calculations
			$this->loadModel('CompanyWorkPosition');
			$this->set('company_work_position_list',$p = $this->CompanyWorkPosition->find('list',array('conditions'=>array('CompanyWorkPosition.kos'=>0,'OR'=>array('CompanyWorkPosition.id'=>$this->data["RequirementsForRecruitment"]["company_work_position_id"],'CompanyWorkPosition.parent_id'=>$this->data["RequirementsForRecruitment"]["company_work_position_id"])))));
			unset($this->CompanyWorkPosition);
						
			// default variable na index
			$this->set('connection_client_requirement_id', $connection_client_requirement_id); 
			$this->set('year', $year); 
			$this->set('month', $month); 
			
			// kill rendering, if client dont work in this year and month, or you select future;
			list($start_y, $start_m, $start_d) 	= explode('-', $this->data['ConnectionClientRequirement']['from']);
			list($end_y, $end_m, $end_d) 		= explode('-', $this->data['ConnectionClientRequirement']['to']);
			
			// for Test Only = $end_y = '2009'; 	$end_m = '06';
			// condition for kill rendering
			$render_element = 0;
			if ($month <= date('m') && $year <= date('Y')){
				if ($end_y == '0000' && $start_y <= $year && $start_m <= $month) {
					$render_element = 1;	
				} else if ($end_y != '0000' && $start_y <= $year && $start_m <= $month && $end_y >= $year && $end_m >= $month){
					$render_element = 1;
				}
			} else {
				$render_element = -1;
			}
			// load free days for state, month, year
			$this->loadModel('Company');
			$this->loadModel('SettingStatSvatek');
			$this->set('svatky',$this->SettingStatSvatek->find('list', array('conditions'=>array( 'setting_stat_id'	=> av(av($this->Company->read(array('stat_id'),$this->data['ConnectionClientRequirement']['company_id']),'Company'),'stat_id'), 'mesic'=> $month, 'rok'=> $year ),'fields'=>array('id','den'),'order'=>'den ASC'))); 				
			unset($this->Company);
			unset($this->SettingStatSvatek);
						
			// render view
			if ($inner != null)
				if ($render_element === 1)
					$this->render('odpracovane_hodiny/element');
				else if ($render_element === -1)
					die('<p>Ty umíš věštit? Jak to kruci víš, kolik toho udělá?</p>');
				else 
					die('<p>Hej!! Tady vubec nic nedelal!!</p>');
			else
				$this->render('odpracovane_hodiny/index');
		} else {
			$this->loadModel('ClientWorkingHour');
			if($this->ClientWorkingHour->save($this->data)){
				$this->loadModel('ConnectionClientRequirement');
				$this->ConnectionClientRequirement->id = $this->data['ClientWorkingHour']['connection_client_requirement_id'];
				$this->ConnectionClientRequirement->saveField('company_money_item_id',$this->data['ClientWorkingHour']['company_money_item_id']);	
				die(json_encode(array('result'=>true, 'id'=>$this->ClientWorkingHour->id, 'message' => 'Úspěšně uloženo do databáze.')));
			} else {
				die(json_encode(array('result'=>false, 'message' => 'Chyba: Nepodařilo se uložit do DB.')));
			}
		}
	}
	
	/**
 	* Returns a JSON list of calculation for "profese"
 	*
	* @param $company_work_position_id (INT)
	* @param $year (DATE(Y))
	* @param $month DATE(m))
	* @param $json Boolean
 	* @return JSON
 	* @access public
	**/
	public function load_calculation_for_prefese($company_work_position_id = '', $year = null, $month = null, $json = true){
		
		if ($company_work_position_id == null || $year == null || $month == null) 
			die(json_encode(array('result'=>false,'message'=>'Nelze zvolit prázdnou profesi.')));
		
		
		// load model of validity and calculation
		$this->loadModel('CompanyMoneyValidity');
		$this->CompanyMoneyValidity->bindModel(array('hasMany'=>array('CompanyMoneyItem'=>array('fields'=>array('id','name','vypocitana_celkova_cista_maximalni_mzda_na_hodinu','cista_mzda_z_pracovni_smlouvy_na_hodinu','cista_mzda_faktura_zivnostnika_na_hodinu','cista_mzda_dohoda_na_hodinu','cista_mzda_cash_na_hodinu','stravenka','fakturacni_sazba_na_hodinu')))));
		$money_items = $this->CompanyMoneyValidity->find(
				'all', 
				array(
					'conditions'=>array(
						'company_work_position_id' => $company_work_position_id,
						'MONTH(platnost_od) <=' => $month,
						'YEAR(platnost_od) <=' => $year,
						array('AND'=> array('OR' => array(
							'MONTH(platnost_do) >=' => $month,
							'MONTH(platnost_do)' => '00'
						))),
						array('AND'=> array('OR' => array(
							'YEAR(platnost_do) >=' => $year,
							'YEAR(platnost_do)' => '0000'
						)))
					)
				)
			);
		if ($money_items == false){
			if ($json == true)
				die(json_encode(array('result'=>false,'message'=>"Nebyla nalezena kalkulace s odpovídající platností pro $year/$month.")));
			else
				return false;
		} else {
			if (count($money_items) > 1){
				if ($json == true)
					die(json_encode(array('result'=>false,'message'=>"Pro tuto profesi byly naleznuty 2 nebo více odpovidajících platností pro $year/$month. Pravděpodobně se jedná o chybu v zadání nové platnosti fakturační sazby.")));
				else
					return false;
			} else {
				if ($json == true)
					die(json_encode(array('result'=>true,'data'=>$money_items[0]['CompanyMoneyItem'])));
				else
					return $money_items[0]['CompanyMoneyItem'];
			}
		}
		if ($json == true)
			die(json_encode(array('result'=>false,'message'=>"Chyba: Sem to vubec nemelo projit. Neco je spatne s podminkou.")));
		else
			return false;
	}


	public function load_calculation_for_prefese_second($company_work_position_id = null, $json = true){
		if ($company_work_position_id == null) 
			die(json_encode(array('result'=>false,'message'=>'Nelze zvolit prázdnou profesi.')));
	
		// load model of validity and calculation
		$this->loadModel('CompanyMoneyValidity');
		$this->CompanyMoneyValidity->bindModel(array('hasMany'=>array('CompanyMoneyItem'=>array('fields'=>array('id','name','vypocitana_celkova_cista_maximalni_mzda_na_hodinu','cista_mzda_z_pracovni_smlouvy_na_hodinu','cista_mzda_faktura_zivnostnika_na_hodinu','cista_mzda_dohoda_na_hodinu','cista_mzda_cash_na_hodinu','stravenka','fakturacni_sazba_na_hodinu','doprava','cena_ubytovani_na_mesic','pausal','ps_pausal_hmm','odmena_fakturace_1','odmena_fakturace_2')))));
		$money_items = $this->CompanyMoneyValidity->find(
				'all', 
				array(
					'conditions'=>array(
						'company_work_position_id' => $company_work_position_id,
						'platnost_od !='=>'0000-00-00', 
						'platnost_do '=>'0000-00-00'
					)
				)
			);
		if ($money_items == false){
			if ($json == true)
				die(json_encode(array('result'=>false,'message'=>"Nebyla nalezena kalkulace s odpovídající platností.")));
			else
				return false;
		} else {
			if (count($money_items) > 1){
				if ($json == true)
					die(json_encode(array('result'=>false,'message'=>"Pro tuto profesi byly naleznuty 2 nebo více odpovidajících platností pro $year/$month. Pravděpodobně se jedná o chybu v zadání nové platnosti fakturační sazby.")));
				else
					return false;
			} else {
				if ($json == true)
					die(json_encode(array('result'=>true,'data'=>$money_items[0]['CompanyMoneyItem'])));
				else
					return $money_items[0]['CompanyMoneyItem'];
			}
		}
		if ($json == true)
			die(json_encode(array('result'=>false,'message'=>"Chyba: Sem to vubec nemelo projit. Neco je spatne s podminkou.")));
		else
			return false;
	}
	
	
	
	
	function ubytovani($accommodation_id,$rok,$mesic){

   	if ($accommodation_id == null) die(); 
     $this->loadModel('Accommodation');
		$detail_ubytovani = $this->Accommodation->read(null,$accommodation_id);
		unset($this->Accommodation);
		
		$this->set('rok',$rok);
		$this->set('mesic',$mesic);
		$this->set('detail_ubytovani',$detail_ubytovani);
		
		$this->render('ubytovani');
	}
	
	function nabor_pozice($requirements_for_recruitment_id){
	//Configure::write('debug',2);
	$this->autoLayout = true;

		if(isset($_GET['print'])){
			$this->layout = "print";
			$this->set('print',true);
		}
		else 
			$this->set('print',false);

		$this->set('group_id',$this->logged_user['CmsGroup']['id']);
		$this->set('umistovatel',$this->logged_user['CmsUser']['name']);

		//print_r($this->logged_user['CmsGroup']['permission']['clients']['index']);
		$this->RequirementsForRecruitment->bindModel(array(
				'belongsTo'=>array('Company')
		));
		$detail = $this->RequirementsForRecruitment->read(null,$requirements_for_recruitment_id);
		
		$this->loadModel('ConnectionClientRequirement');
		$this->ConnectionClientRequirement->bindModel(array(
				'belongsTo'=>array('Client','CmsUser')
		));
		$add_clients = $this->ConnectionClientRequirement->find('all', array(
					'conditions'=>array(
						'ConnectionClientRequirement.requirements_for_recruitment_id'=>$detail['RequirementsForRecruitment']['id'],
						array('AND'=> 
							array('OR' => array(
								'ConnectionClientRequirement.type'=>1,
								array('AND'=> array(
									'ConnectionClientRequirement.to'=>'0000-00-00',
									'ConnectionClientRequirement.type'=>2
								))
							))
						)
					),
					'fields'=>array(
						'ConnectionClientRequirement.id',
						'Client.id',
						'Client.name',
						'CmsUser.name',
						'ConnectionClientRequirement.created',
						'ConnectionClientRequirement.type'),
					'order'=>'ConnectionClientRequirement.id ASC'
		));

		$this->set('add_clients',$add_clients);
		//nacteni seznamu klientu
		
		$this->loadModel('ConnectionClientCareerItem');
		$this->loadModel('ConnectionClientRequirement');
		$this->loadModel('ConnectionClientRecruiter');
		//odstraneni tech co jsou jiz zamestnani
		$neco = $this->ConnectionClientRequirement->find('list',array(
			'conditions'=>array(
				'ConnectionClientRequirement.type'=>'2',
				'ConnectionClientRequirement.to'=>'0000-00-00'
			),
			'fields'=>array('id','client_id')
		));
		$podminka = false;

		//vidi pouze sve clienty 
		if($this->logged_user['CmsGroup']['permission']['clients']['index']==2 || $this->logged_user['CmsGroup']['permission']['clients']['spec_permision']=="cms_user_id=-1"){
			// a je interni naborar
			if($this->logged_user['CmsGroup']['permission']['clients']['spec_permision']=="cms_user_id=-1"){
				$neco2 = $this->ConnectionClientRecruiter->find('list',array(
					'conditions'=>array(
						'ConnectionClientRecruiter.cms_user_id'=>-1
					),
					'fields'=>array('id','client_id')
				));
			}
			else {
				$neco2 = $this->ConnectionClientRecruiter->find('list',array('conditions'=>array('ConnectionClientRecruiter.cms_user_id'=>$this->logged_user['CmsUser']['id']),'fields'=>array('id','client_id')));
			}
			$podminka = true;
		}
		else
			$neco2="";

		$this->set('neco2',$neco2);
		$this->set('neco',$neco);
		
		$this->ConnectionClientCareerItem->bindModel(array('belongsTo'=>array('Client')));
		
		//load client listu
		if($podminka)
			$this->set('client_list',$this->ConnectionClientCareerItem->find('list',array('conditions'=>array('Client.kos'=>0,'ConnectionClientCareerItem.setting_career_item_id'=>$detail['RequirementsForRecruitment']['setting_career_item_id'],'ConnectionClientCareerItem.client_id IN ('.implode(',',$neco2).')'),'fields'=>array('Client.id','Client.name'),'recursive'=>2,'order'=>'Client.name')));
			//$this->set('client_list',$this->ConnectionClientCareerItem->find('list',array('conditions'=>array('Client.kos'=>0,'ConnectionClientCareerItem.setting_career_item_id'=>$detail['RequirementsForRecruitment']['setting_career_item_id'],'ConnectionClientCareerItem.client_id NOT IN ('.implode(',',$neco).')','ConnectionClientCareerItem.client_id IN ('.implode(',',$neco2).')'),'fields'=>array('Client.id','Client.name'),'recursive'=>2,'order'=>'Client.name')));
		else	
			$this->set('client_list',$this->ConnectionClientCareerItem->find('list',array('conditions'=>array('Client.kos'=>0,'ConnectionClientCareerItem.setting_career_item_id'=>$detail['RequirementsForRecruitment']['setting_career_item_id']),'fields'=>array('Client.id','Client.name'),'recursive'=>2,'order'=>'Client.name')));
			//$this->set('client_list',$this->ConnectionClientCareerItem->find('list',array('conditions'=>array('Client.kos'=>0,'ConnectionClientCareerItem.setting_career_item_id'=>$detail['RequirementsForRecruitment']['setting_career_item_id'],'ConnectionClientCareerItem.client_id NOT IN ('.implode(',',$neco).')'),'fields'=>array('Client.id','Client.name'),'recursive'=>2,'order'=>'Client.name')));
		

		//unset
		unset($this->ConnectionClientCareerItem);
		unset($this->ConnectionClientRequirement);
		unset($this->ConnectionClientRecruiter);
		unset($neco);
		unset($neco2);
		
		$this->loadModel('SettingCareerItem');
		$this->set('kvalifikace_list', $this->SettingCareerItem->find('list',array('conditions'=>array(),'order'=>'SettingCareerItem.name ASC')));
		unset($this->SettingCareerItem);
		
		$this->set('detail',$detail);
		$this->render('nabor_pozice_list');
	}
	
	
	
	function load_client($kvalifikace_id=null, $company_id = null, $requirement_id = null){
		$this->set('group_id',$this->logged_user['CmsGroup']['id']);
		$this->set('umistovatel',$this->logged_user['CmsUser']['name']);
	
		if ($kvalifikace_id == null){
			die ('Prosím vyberte kvalifikaci');
		}

		$this->loadModel('ConnectionClientCareerItem');
		$this->loadModel('ConnectionClientRequirement');
		$this->loadModel('ConnectionClientRecruiter');
		//odstraneni tech co jsou jiz zamestnani - 7.10.09 - vypnuto
		$neco = $this->ConnectionClientRequirement->find('list',array(
			'conditions'=>array(
				'ConnectionClientRequirement.type'=>'2',
				'ConnectionClientRequirement.to'=>'0000-00-00'
			),
			'fields'=>array('id','client_id')
		));
		$this->set('neco',$neco);
		$podminka = false;
 
		//vidi pouze sve clienty 
		if($this->logged_user['CmsGroup']['permission']['clients']['index']==2 || $this->logged_user['CmsGroup']['permission']['clients']['spec_permision']=="cms_user_id=-1"){
			// a je interni naborar
			if($this->logged_user['CmsGroup']['permission']['clients']['spec_permision']=="cms_user_id=-1"){
				$neco2 = $this->ConnectionClientRecruiter->find('list',array('conditions'=>array('ConnectionClientRecruiter.cms_user_id'=>-1),'fields'=>array('id','client_id')));
			}
			else {
				$neco2 = $this->ConnectionClientRecruiter->find('list',array('conditions'=>array('ConnectionClientRecruiter.cms_user_id'=>$this->logged_user['CmsUser']['id']),'fields'=>array('id','client_id')));
			}
			$podminka = true;
		}
		
		$this->ConnectionClientCareerItem->bindModel(array('belongsTo'=>array('Client')));
		
		//load client listu
		if($podminka)
			$this->set('client_list',$this->ConnectionClientCareerItem->find('list',array('conditions'=>array('Client.kos'=>0,'ConnectionClientCareerItem.setting_career_item_id'=>$kvalifikace_id,'ConnectionClientCareerItem.client_id IN ('.implode(',',$neco2).')'),'fields'=>array('Client.id','Client.name'),'recursive'=>2,'order'=>'Client.name')));
			//$this->set('client_list',$this->ConnectionClientCareerItem->find('list',array('conditions'=>array('Client.kos'=>0,'ConnectionClientCareerItem.setting_career_item_id'=>$kvalifikace_id,'ConnectionClientCareerItem.client_id NOT IN ('.implode(',',$neco).')','ConnectionClientCareerItem.client_id IN ('.implode(',',$neco2).')'),'fields'=>array('Client.id','Client.name'),'recursive'=>2,'order'=>'Client.name')));
		else	
			$this->set('client_list',$this->ConnectionClientCareerItem->find('list',array('conditions'=>array('Client.kos'=>0,'ConnectionClientCareerItem.setting_career_item_id'=>$kvalifikace_id),'fields'=>array('Client.id','Client.name'),'recursive'=>2,'order'=>'Client.name')));
			//$this->set('client_list',$this->ConnectionClientCareerItem->find('list',array('conditions'=>array('Client.kos'=>0,'ConnectionClientCareerItem.setting_career_item_id'=>$kvalifikace_id,'ConnectionClientCareerItem.client_id NOT IN ('.implode(',',$neco).')'),'fields'=>array('Client.id','Client.name'),'recursive'=>2,'order'=>'Client.name')));
		
		//unset
		unset($this->ConnectionClientCareerItem);
		unset($this->ConnectionClientRequirement);
		unset($this->ConnectionClientRecruiter);
		unset($neco);
		unset($neco2);
	
		
		$this->set('requirement_id', $requirement_id);
		$this->set('company_id', $company_id);
		
		$this->render('nabor_pozice_client_list');
	}
	
	function add_uchazec($requirement_id = null, $company_id = null, $client_id = null){

		if ($requirement_id == null || $company_id == null || $client_id == null){
			die(json_encode(array('result'=>false,'message'=>'Volane funkci chybi arguments')));
		}
		$this->loadModel('ConnectionClientRequirement');

		$find = array();
		$find = $this->ConnectionClientRequirement->find('first',array(
			'conditions'=>array(
				'requirements_for_recruitment_id' => $requirement_id,
				'client_id' => $client_id,
				'company_id' => $company_id,
				'type' => 1
			),
			'fields'=>array('id')
		));
		// osetreni dvojkliku aby nesel pridat jeden klient 2x po sobe
		if($find != null)
			die(json_encode(array('result'=>-1)));

		$to_save = array('ConnectionClientRequirement' => array(
			'requirements_for_recruitment_id' => $requirement_id,
			'client_id' => $client_id,
			'company_id' => $company_id,
			'cms_user_id' => $this->logged_user['CmsUser']['id'],
			'company_money_item_id' => 0,
			'type' => 1
		));
		if ($this->ConnectionClientRequirement->save($to_save)){
		  $this->loadModel('RequirementsForRecruitment');
		  $req = $this->RequirementsForRecruitment->read(null,$requirement_id);
		  
		  //company
		  $this->loadModel('Company');
		  $comp = $this->Company->read(null,$company_id);
		  unset($this->Company);

		  // nastaveni promennych 
		  $msg_name="Nábor - umístěn";
		  $msg_text="Uživatel_se_přihlásil_ve_firmě_".$comp["Company"]["name"]."_na_pozici_".$req["RequirementsForRecruitment"]["name"];
		  $msg_cms_user_id=$this->logged_user['CmsUser']['id'];
		  //poslani zpravy ($client_id, $name, $text, $cms_user_id)
		  $this->requestAction( 'clients/add_messages_edit/'.$client_id.'/'.$msg_name.'/'.$msg_text.'/'.$msg_cms_user_id);
			
			$this->loadModel('CmsUser');
			$email_list = $this->CmsUser->find('list',array(
                'fields'=>array('id','email'), 
                'conditions'=>array('id'=>7)
            ));
			unset($this->CmsUser);
		
	        //client
	        $this->loadModel('Client');
	        $cl= $this->Client->read(null,$client_id);
    
      	
			$replace_list = array(
				'##Company.name##' 	=>$comp['Company']['name'],
				'##RequirementsForRecruitment.name##' 	=>$req["RequirementsForRecruitment"]["name"],
				'##Client.name##' 	=>$cl["Client"]["name"],
				'##Recruiter.name##' 	=>$this->logged_user['CmsUser']['name'],
				'##Info.profese##' 	=>$req['RequirementsForRecruitment']['name'],
				'##Info.mesto##' 	=>$req['RequirementsForRecruitment']['job_city'],
				'##Info.poznamkakvolnemumistu##' 	=>$req['RequirementsForRecruitment']['comment'],
				'##Info.napln##' 	=>$req['RequirementsForRecruitment']['napln_prace'],
				'##Info.mzda##' 	=>$req['RequirementsForRecruitment']['salary'],
				'##Info.komentar##' 	=>$req['RequirementsForRecruitment']['mzdove_podminky_poznamka'],
				'##Info.smennost##' 	=>$this->smennost_list[$req['RequirementsForRecruitment']['setting_shift_working_id']],
				'##Info.komentarksmenosti##' 	=>$req['RequirementsForRecruitment']['komentar_smennosti']
			);
				
			$this->Email->send_from_template(5,$email_list,$replace_list);
			
			// ulozeni stavu clienta
			$this->Client->saveField('stav',1);
			unset($this->Client);
			
			
			die(json_encode(array('result'=>true,'id'=>$this->ConnectionClientRequirement->id)));
		}
		else 
			die(json_encode(array('result'=>false,'message'=>'Chyba behem ukladani .....')));
	}

	function remove_uchazec($connection_id = null){
		if ($connection_id == null){
			die(json_encode(array('result'=>false,'message'=>'Chyba, chybi id connection')));
		}
		$this->loadModel('ConnectionClientRequirement');
		$this->ConnectionClientRequirement->id = $connection_id;
		if ($this->ConnectionClientRequirement->saveField('type',-1))
			die(json_encode(array('result'=>true)));
		else 
			die(json_encode(array('result'=>false,'message'=>'Chyba behem ukladani .....')));
	}
	
	function remove_dotaznik($client_id=null,$company_id = null, $requirement_id = null){
  	
     if (empty($this->data))
  	 {
        $this->set('client_id',$client_id);
        $this->set('company_id',$company_id);
        $this->set('requirement_id',$requirement_id);
        $this->render('duvod');
  	 }
  	 else {
		//company
		$this->loadModel('Company');
        $comp = $this->Company->read(null,$company_id);
        unset($this->Company);
  	    // nastaveni promennych 
		  $msg_name="Nábor-Odstranění";
		  $msg_text="Uživatel_byl_odstraněn_ve_firmě_".$comp["Company"]["name"]."_z_náboru_duvod_".$this->data["text"];
		  $msg_cms_user_id=$this->logged_user['CmsUser']['id'];
		  //poslani zpravy ($client_id, $name, $text, $cms_user_id)
		  $this->requestAction('clients/add_messages_edit/'.$client_id.'/'.$msg_name.'/'.$msg_text.'/'.$msg_cms_user_id);
		  
			$this->loadModel('CmsUser');
			$email_list = $this->CmsUser->find('list',array('fields'=>array('id','email'), 'conditions'=>array('cms_group_id'=>5)));
			unset($this->CmsUser);
			//
			$this->loadModel('RequirementsForRecruitment');
			$req = $this->RequirementsForRecruitment->read(null,$requirement_id);
			unset($this->RequirementsForRecruitment);

		    //client
		    $this->loadModel('Client');
		    $cl= $this->Client->read(null,$client_id);
      	
			$replace_list = array(
					'##Company.name##' 	=>$comp['Company']['name'],
					'##RequirementsForRecruitment.name##' 	=>$req["RequirementsForRecruitment"]["name"],
					'##Client.name##' 	=>$cl["Client"]["name"],
					'##Message.text##' 	=>$this->data["text"]
			);
				
			$this->Email->send_from_template(6,$email_list,$replace_list);
		  
		  // ulozeni stavu clienta
			$this->Client->saveField('stav',0);
			unset($this->Client);
      die();
     }
	}
	
	function add_zamestnanec($connection_id = null){
		if ($connection_id == null){
			die(json_encode(array('result'=>false,'message'=>'Chyba, chybi id connection')));
		}
		$this->loadModel('ConnectionClientRequirement');
		$this->ConnectionClientRequirement->id = $connection_id;
		if ($this->ConnectionClientRequirement->saveField('type',2))
			die(json_encode(array('result'=>true)));
		else 
			die(json_encode(array('result'=>false,'message'=>'Chyba behem ukladani .....')));
	}
	
	function add_zam_dotaznik($client_id=null,$company_id = null, $requirement_id = null,$connection_id = null){
  	
     if (empty($this->data))
  	 {
        $this->set('client_id',$client_id);
        $this->set('company_id',$company_id);
        $this->set('requirement_id',$requirement_id);
        $this->set('connection_id',$connection_id);
		
		$this->loadModel('RequirementsForRecruitment');
		$req = $this->RequirementsForRecruitment->read(null,$requirement_id);

		// load list of work position and calculations
		$this->loadModel('CompanyWorkPosition');
		$this->set('company_work_position_list',$p = $this->CompanyWorkPosition->find('list',array(
			'conditions'=>array(
				'CompanyWorkPosition.kos'=>0,
				// 'OR'=>array(
					// 'CompanyWorkPosition.id'=>$req["RequirementsForRecruitment"]["company_work_position_id"],
					// 'CompanyWorkPosition.parent_id'=>$req["RequirementsForRecruitment"]["company_work_position_id"]
				// )
				'CompanyWorkPosition.company_id'=>$company_id,
				'CompanyWorkPosition.test'=>0
		
			),
			'order'=>'CompanyWorkPosition.name ASC'
		)));
				
		
		//uvolneni pameti
		unset($this->CompanyWorkPosition);
		unset($this->RequirementsForRecruitment);

		//render
    	$this->render('add_zam_duvod');
  	 }
  	 else {
		//company
		   $this->loadModel('Company');
	       $comp = $this->Company->read(null,$company_id);
	       unset($this->Company);

  	   // nastaveni promennych 
		  $msg_name="Nábor-zaměstnán";
		  $msg_text="Uživatel_byl_zaměstnán_ve_firmě_".$comp["Company"]["name"]."_duvod_".$this->data["text"];
		  $msg_cms_user_id=$this->logged_user['CmsUser']['id'];
		  $msg_datum=$this->data["from"];
		  //poslani zpravy ($client_id, $name, $text, $cms_user_id,$datum)
		  $this->requestAction('clients/add_messages_edit/'.$client_id.'/'.$msg_name.'/'.$msg_text.'/'.$msg_cms_user_id.'/'.$msg_datum);
		  
        //ulozeni data
        $this->loadModel('ConnectionClientRequirement');
  		$this->ConnectionClientRequirement->id = $connection_id;
  		$this->ConnectionClientRequirement->save($this->data);
 
		// doposud umistene pozice zrusit
		$this->ConnectionClientRequirement->updateAll(
			array('ConnectionClientRequirement.type' => '-1'),
		    array('ConnectionClientRequirement.client_id' => $client_id,'ConnectionClientRequirement.type' => 1)
		 );
  		unset($this->ConnectionClientRequirement);
		
		// zjisteni pokud ma mezi recruitery interni nabor
		$this->loadModel('ConnectionClientRecruiter');
		$ma_interni_nabor = $this->ConnectionClientRecruiter->find('first',array(
			'conditions'=>array(
				'client_id'=>$client_id,
				'cms_user_id'=>-1		
			)
		));
		if($ma_interni_nabor == null){ //nemá tak přidej
			$save['client_id'] = $client_id;
			$save['cms_user_id'] = -1;
			$this->ConnectionClientRecruiter->save($save);
		}

  		
			//email
			$this->loadModel('CmsUser');
			$email_list = $this->CmsUser->find('list',array('fields'=>array('id','email'), 
                'conditions'=>array('cms_group_id'=>array(5,7))));
			unset($this->CmsUser);
			//
			$this->loadModel('CompanyWorkPosition');
			$cwp = $this->CompanyWorkPosition->read(array('name'),$this->data['company_work_position_id']);
			unset($this->CompanyWorkPosition);
			
	       //client
	       $this->loadModel('Client');
	       $cl= $this->Client->read(null,$client_id);
	      	
			$replace_list = array(
					'##Company.name##' 	=>$comp['Company']['name'],
					'##CompanyWorkPosition.name##' 	=>$cwp["CompanyWorkPosition"]["name"],
					'##Client.name##' 	=>$cl["Client"]["name"],
					'##Message.text##' 	=>$this->data["text"]
			);
				
			$this->Email->send_from_template(7,$email_list,$replace_list);
			
			// ulozeni stavu clienta
			$this->Client->saveField('stav',2);
			unset($this->Client);
			
      die();
     }
	}
	
	function remove_zamestnanec($connection_id = null){
		if ($connection_id == null){
			die(json_encode(array('result'=>false,'message'=>'Chyba, chybi id connection')));
		}
		$this->loadModel('ConnectionClientRequirement');
		$this->ConnectionClientRequirement->id = $connection_id;
		if ($this->ConnectionClientRequirement->saveField('type',1))
			die(json_encode(array('result'=>true)));
		else 
			die(json_encode(array('result'=>false,'message'=>'Chyba behem ukladani .....')));
	}
	
	function del_zam_dotaznik($client_id=null,$company_id = null, $requirement_id = null,$connection_id = null){
  	
     if (empty($this->data))
  	 {
	        $this->set('client_id',$client_id);
	        $this->set('company_id',$company_id);
	        $this->set('requirement_id',$requirement_id);
	        $this->set('connection_id',$connection_id);
	
			$this->loadModel('ConnectionClientRequirement');
			$cwp = $this->ConnectionClientRequirement->read(array('company_work_position_id'),$connection_id);
			$this->data['company_work_position_id'] = $cwp['ConnectionClientRequirement']['company_work_position_id'];
			unset($this->ConnectionClientRequirement);

    		$this->render('del_zam_duvod');
  	 }
  	 else {
		//company
		$this->loadModel('Company');
        $comp = $this->Company->read(null,$company_id);
        unset($this->Company);
		// nastaveni promennych 
		$msg_name="Nábor-zaměstnán";
		$msg_text="Uživatel_byl_propuštěn_z_firmy_".$comp["Company"]["name"]."_duvod_".$this->data["text"];
		$msg_cms_user_id=$this->logged_user['CmsUser']['id'];
		$msg_datum=$this->data["to"];
		  
		//poslani zpravy ($client_id, $name, $text, $cms_user_id,$datum)
		$this->requestAction('clients/add_messages_edit/'.$client_id.'/'.$msg_name.'/'.$msg_text.'/'.$msg_cms_user_id.'/'.$msg_datum);
				
		//ulozeni data
		$this->loadModel('ConnectionClientRequirement');
  		$this->ConnectionClientRequirement->id = $connection_id;
  		$this->ConnectionClientRequirement->saveField('to',$this->data["to"]);
  		
	  		//email
	  		$this->loadModel('CmsUser');
			$email_list = $this->CmsUser->find('list',array('fields'=>array('id','email'), 'conditions'=>array('cms_group_id'=>array(5,7))));
			unset($this->CmsUser);
			//
			$this->loadModel('CompanyWorkPosition');
			$cwp = $this->CompanyWorkPosition->read(array('name'),$this->data['company_work_position_id']);
			unset($this->CompanyWorkPosition);
		
		    //client
		    $this->loadModel('Client');
		    $cl= $this->Client->read(null,$client_id);
      	
			$replace_list = array(
					'##Company.name##' 	=>$comp['Company']['name'],
					'##CompanyWorkPosition.name##' 	=>$cwp['CompanyWorkPosition']['name'],
					'##Client.name##' 	=>$cl["Client"]["name"],
					'##Message.text##' 	=>$this->data["text"]
			);
				
			$this->Email->send_from_template(8,$email_list,$replace_list);
      
      // ulozeni stavu clienta
			$this->Client->saveField('stav',1);
			unset($this->Client);
      die();
     }
	}
	
	
	/**
 	* Seznam priloh
 	*
	* @param $client_id
 	* @return view
 	* @access public
	**/
	function attachs($requirements_for_recruitment_id){
		$this->autoLayout = false;
		$this->loadModel('RequirementsAttachment');
		if($this->logged_user["CmsGroup"]["permission"]["report_requirements_for_recruitments"]["attach"]==2) 
			$pristup =  "RequirementsAttachment.cms_user_id=".$this->logged_user['CmsUser']['id'];
		else 
			$pristup="";

		$this->RequirementsAttachment->bindModel(array('belongsTo'=>array('SettingAttachmentType')));
		$this->set('attachment_list', $this->RequirementsAttachment->findAll(array('RequirementsAttachment.requirements_for_recruitment_id'=>$requirements_for_recruitment_id,'RequirementsAttachment.kos'=>0 ,$pristup)));
		$this->set('requirements_for_recruitment_id',$requirements_for_recruitment_id);
		unset($this->RequirementsAttachment);
		$this->render('attachs/index');
	}
	
	/**
 	* Editace priloh
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function attachs_edit($requirements_for_recruitment_id = null, $id = null){
		$this->autoLayout = false;
		$this->loadModel('RequirementsAttachment'); 
		if (empty($this->data)){
			$this->loadModel('SettingAttachmentType'); $this->SettingAttachmentType = new SettingAttachmentType();
			$this->set('setting_attachment_type_list',$this->SettingAttachmentType->find('list',array('conditions'=>array('kos'=>0),'order'=>'poradi ASC')));
			unset($this->SettingAttachmentType);
			$this->data['RequirementsAttachment']['requirements_for_recruitment_id'] = $requirements_for_recruitment_id;
			if ($id != null){
				$this->data = $this->RequirementsAttachment->read(null,$id);
			}
			$this->render('attachs/edit');
		} else {
			$this->data["RequirementsAttachment"]["cms_user_id"]=$this->logged_user['CmsUser']['id'];
			$this->RequirementsAttachment->save($this->data);
			$this->attachs($this->data['RequirementsAttachment']['requirements_for_recruitment_id']);
		}
		unset($this->RequirementsAttachment);
	}
	/**
 	* Presun prilohy do kose
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function attachs_trash($requirements_for_recruitment_id, $id){
		$this->loadModel('RequirementsAttachment');
		$this->RequirementsAttachment->save(array('RequirementsAttachment'=>array('kos'=>1,'id'=>$id)));
		$this->attachs($requirements_for_recruitment_id);
		unset($this->RequirementsAttachment);
	}
	
	/**
 	* Nahrani prilohy na ftp
 	*
 	* @return view
 	* @access public
	**/
	function upload_attach() {
		$this->Upload->set('data_upload',$_FILES['upload_file']);
		if ($this->Upload->doit(json_decode($this->data['upload']['setting'],true))){
			echo json_encode(array('upload_file'=>array('name'=>$this->Upload->get('outputFilename')),'return'=>true));
		} else 
			echo json_encode(array('return'=>false,'message'=>$this->Upload->get('error_message')));		
		die();
	} 
	
	/**
 	* Stazeni prilohy
 	*
	* @param $file
	* @param $file_name
 	* @return download file
 	* @access public
	**/
	function  attachs_download($file,$file_name){
		$pripona = strtolower(end(Explode(".", $file)));
		$file = strtr($file,array("|"=>"/"));
		$filesize = filesize('./uploaded/'.$file);
		$cesta = "http://".$_SERVER['SERVER_NAME']."/uploaded/".$file;
				 
		header("Pragma: public"); // požadováno
	    header("Expires: 0");
	    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	    header("Cache-Control: private",false); // požadováno u některých prohlížečů
	    header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . $filesize);
		Header('Content-Type: application/octet-stream');
		Header('Content-Disposition: attachment; filename="'.$file_name.'.'.$pripona.'"');
		readfile($cesta);
		die();

	}
	
	
}
?>