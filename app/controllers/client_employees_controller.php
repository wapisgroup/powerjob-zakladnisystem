<?php
//Configure::write('debug',1);
DEFINE('fields','ClientView.*,ConnectionClientCareerItem.*,ConnectionClientRecruiter.*,
    ConnectionClientRequirement.*,CompanyMoneyItem.*');
    /**check_client_requires(
        ClientView.jmeno,
        ClientView.mobil,
        ClientView.prijmeni,
        ClientView.datum_narozeni,
        ClientView.rodne_cislo,
        ClientView.ulice,
        ClientView.mesto,
        ClientView.email,
        ClientView.psc,
        ClientView.os_pohovor,
        ClientView.os_pohovor_poznamka,
        ClientView.info_about_job,
        ClientView.vek
    ) as requires_warning    
');*/
//check_client_requires(ClientView.id) as requires_warning
class ClientEmployeesController extends AppController {
	var $name = 'ClientEmployees';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload','Email');
	var $uses = array('ClientView','Client',);
	var $renderSetting = array(
		'bindModel' => array(
            'hasOne' => array(
				'ConnectionClientCareerItem' => array('foreignKey' => 'client_id'),
				'ConnectionClientRecruiter' => array('className' => 'ConnectionClientRecruiter', 'foreignKey' => 'client_id'),
				'ConnectionClientRequirement' => array(
					'className' => 'ConnectionClientRequirement', 
					'foreignKey' => 'client_id',
					'conditions' => array(
						'ConnectionClientRequirement.kos'=>0,
						'ConnectionClientRequirement.to'=>'0000-00-00',
						'(ConnectionClientRequirement.type = 2)'
						
					)
				)
            ),
            'belongsTo' =>array(
                'CompanyMoneyItem' => array(),
            )
        ),
		//'SQLfields' => '*,GROUP_CONCAT(profese_name SEPARATOR "<br/>") as Profese ',
		'SQLfields' => array(fields),
		'SQLcondition' => array(
			'ClientView.stav'=>2,
			'ConnectionClientRequirement.to'=>'0000-00-00'
		),
		'controller'=> 'client_employees',
		'page_caption'=>'leasingový zaměstnanci',
		//'count_group_by' => 'ClientView.id',
		'group_by' => 'ClientView.id',
		'sortBy'=>'ClientView.id.ASC',
	
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
            'add_express_item' => 'Přidat Express|edit_express|Pridat popis|add_express',
            'export_excel' => 'Export Excel|export_excel|Export Excel|export_excel',  
          //  'transport_card' => 'Kartička dopravy|transport_card|Tisk kartiček dopravy|transport_card',
		),
		'filtration' => array(
			'ClientView-name|'		=>	'text|Jméno|',
			//'ClientView-stav'		=>	'select|Status|stav_client_list',
			'ConnectionClientCareerItem-setting_career_item_id'		=>	'select|Profese|profese_list',
			'ConnectionClientRecruiter-cms_user_id#admin_ctrl'		=>	'select|Recruiter|cms_user_list',
			'ConnectionClientRequirement-company_id'		=>	'select|Firma|company_list',
			'ClientView-next|'		=>	'select|Další|next_list',
            'CompanyMoneyItem-name|'		=>	'text|Forma|',
            'CompanyMoneyItem-doprava|zero_or_more'		=>	'select|Doprava|ano_ne_checkbox',
			//'ClientView-telefon1|'	=>	'text|Telefon|',
		),
		'items' => array(
			'client_manager_id'		=>	'cmid|ClientView|client_manager_id|hidden|',
			'id'		=>	'ID|ClientView|id|text|',
            'surname'	=>	'Přijmení|ClientView|prijmeni|text|',
			'name'		=>	'Jméno|ClientView|jmeno|text|', 
			'profese'	=>	'Profese|ClientView|Profese|text|',
			'firma'		=>	'Firma|ClientView|company|text|',
			'cm'		=>	'CM|ClientView|client_manager|text|',
			'coo'		=>	'COO|ClientView|coordinator|text|',
			'coo2'		=>	'COO2|ClientView|coordinator2|text|',
			'datum_nastupu'	=>	'Datum nástupu|ConnectionClientRequirement|from|date|',
            'forma'		=>	'Forma|CompanyMoneyItem|name|text|',
            'stav_dok'	=>	'#|ClientView|chybna_dokumentace|text|status_to_ico2#chybna_dokumentace'
	
		
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
			'attach'	=>	'attachs|Přílohy|attach',
			'message'	=>	'messages|Zprávy|message',
			'rozvazat_prac_pomer'	=>	'rozvazat_prac_pomer|Rozvázat pracovní poměr|rozvazat_prac_pomer',	
			'zmena_pp'	=>	'zmena_pp|Změna pracovního poměru|zmena_pp',
            'ucetni_dokumentace'	=>	'ucetni_dokumentace|Účetní dokumentace|ucetni_dokumentace',
             'add_activity'	=>	'add_activity|Přidání aktivity|add_activity',
			'delete'	=>	'trash|Odstranit položku|trash',
			//'add_opp'		=>	'add_opp|Přidání OPP|add_opp',
           // 'add_doctor'	=>	'add_doctor|Přidání lékařské prohlídky|add_doctor',
           // 'opp_transfer'	=>	'opp_transfer|Přenos OPP na jiného klienta|opp_transfer'
            
		),
        'posibility_link' => array(
            'add_activity'=> 'ClientView.id',
            'rozvazat_prac_pomer'	=>	'ClientView.id/ConnectionClientRequirement.id',
            'add_opp'=> 'ClientView.id/ConnectionClientRequirement.company_work_position_id/ConnectionClientRequirement.company_id/ConnectionClientRequirement.id',
            'opp_transfer'=> 'ClientView.id',
        ),
		'domwin_setting' => array(
			'sizes' 		=> '[800,900]',
			'scrollbars'	=> true,
			'languages'		=> true,
		),
        'class_tr'=>array(
            'class'=>'color_red',
            'model'=>0,
            'col'=>'requires_warning',
            'value'=>0
        ),
        'checkbox_setting' => array(
			'model'			=>	'ClientView',
            'col'           =>  'id'
		)
	);
	
    function beforeFilter(){
        parent::beforeFilter();
        
        /**
         * povolit klienty i EN
         */
        $this->ClientView->set_ignore_status(true); 
        
        if(isset($_GET['filtration_ClientView-next|']) && !empty($_GET['filtration_ClientView-next|'])){
            list($col,$value) = explode('-',$_GET['filtration_ClientView-next|']);

            $this->params['url']['filtration_ClientView-'.$col] = $value;
            
            unset($this->params['url']['filtration_ClientView-next|']); 
            unset($col);
            unset($value);
        }

      
        //pr($_GET);    
    }

    
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Zaměstnanci AT'=>'#'));
			
		$this->loadModel('CmsUser');
		$this->set('cms_user_list', $this->CmsUser->find('list',
			array('conditions'=>array('kos'=>0), 'order'=>'name ASC')
		));

		$this->set('cm_coo_list', $this->CmsUser->find('list',
			array('conditions'=>array('cms_group_id IN (3,4)'),'fields'=>array('name','name'), 'order'=>'name ASC')
		));

		$this->loadModel('Company'); 	
		$company_conditions = array('Company.kos'=>0);
		
		if (isset($this->filtration_company_condition))
			$company_conditions = am($company_conditions, $this->filtration_company_condition);
      
		$this->set('company_list',		$this->Company->find('list',array(
			'conditions'=>$company_conditions,
			'order'=>array('Company.name ASC')
		)));
		unset($this->Company);
        
        /**
        * dalsi moznosit filtrace
        * moznsosti zadavame sloupec-stav => nazev moznosti
        */
        $this->set('next_list',array(
            'express-1'=> 'Expresní klienti',
            'import_adresa-NOTNULL'=> 'Neprázdna importní adresa'
        ));
        
        
        //změna profese podle nastaveneho company_work_position_id
		foreach($this->viewVars['items'] as &$item){
            
			//nastaveni forem odmeny a zobrazeni ubytovani a dopravy
			if(isset($item['CompanyMoneyItem'])){
				
					$doprava = 'Doprava '.($item['CompanyMoneyItem']['doprava'] != 0 ? 'Ano' : 'Ne');
					$ubytovani = 'Ubytování '.($item['CompanyMoneyItem']['cena_ubytovani_na_mesic'] != 0 ? 'Ano' : 'Ne');
	                $item['CompanyMoneyItem']['forma'] = $item['CompanyMoneyItem']['name'];
					$item['CompanyMoneyItem']['name'] = $item['CompanyMoneyItem']['name'].' - '.$ubytovani.' / '.$doprava;;
			
			}
		}


		$this->loadModel('SettingCareerItem');
		$this->set('profese_list', $this->SettingCareerItem->find('list',
			array('conditions'=>array('kos'=>0), 'order'=>'name ASC')
		));

			
				
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->set('scripts',array('uploader/uploader'));
			$this->set('styles',array('../js/uploader/uploader'));
			$this->render('../system/index');
		}
	}
	
	
	
	function edit($id = null,$domwin = 0, $show = 0){
		echo $this->requestAction('clients/edit/'.$id.'/'.$domwin.'/'.$show.'/0/');
		die();
	}

	
	
	
	/**
 	* Seznam priloh
 	*
	* @param $client_id
 	* @return view
 	* @access public
	**/
	function attachs($client_id){
		echo $this->requestAction('clients/attachs/'.$client_id);
		die();
	}
	
	
	
	
	function rozvazat_prac_pomer($client_id,$connection_id){
		echo $this->requestAction('clients/rozvazat_prac_pomer/'.$client_id.'/'.$connection_id);
		die();
	}
	
  
    

	// funkce na zmenu pracovniho pomeru
	function zmena_pp($client_id = null,$company_id = null,$company_work_position_id = null){
		echo $this->requestAction('clients/zmena_pp/'.$client_id.'/'.$company_id.'/'.$company_work_position_id);
		die();
	}
    
    
    
	// funkce na uceti dokumentaci umistena v dochazkach
	function ucetni_dokumentace ($connection_client_requirement_id = null, $forma = null){
		echo $this->requestAction('employees/ucetni_dokumentace/'.$connection_client_requirement_id.'/'.$forma);
		die();
	}
    
    function add_activity($id = null){
		echo $this->requestAction('clients/domwin_add_activity/'.$id.'/');
		die();
	}



    function add_opp($client_id = null,$company_work_position_id = null, $company_id = null, $connection_id = null){
        //Configure::write('debug',1);
        if(empty($this->data)){
            
            if($client_id == null || $company_work_position_id == null || $company_id == null || $connection_id == null)
                die('Špatně nastavené vstupní parametry');

            //if($_SERVER['REMOTE_ADDR'] == '90.176.43.89')
            //    $this->logged_user['CmsUser']['id'] = 311;

            /**
             * Klient
             */
            $this->loadModel('Client');
            $this->set('client',$this->Client->read(array('opp_nohavice','opp_bluza','opp_tricko','opp_obuv'),$client_id));
            unset($this->Client);
            
            /**
             * Nastaveni k dane profesi
             */
            $this->loadModel('ConnectionPositionOpp');
            $work_position = $this->ConnectionPositionOpp->find('all',array(
                'fields'=>array('id','type','yn','interval','created'),
                'conditions'=>array('position_id'=>$company_work_position_id)
            ));
            unset($this->ConnectionPositionOpp);
            
            /**
             * Nacteni koordinatoroveho skladu
             */
            $this->loadModel('OppOrderItem');
            $this->OppOrderItem->bindModel(array(
                'belongsTo'=>array('OppOrder')
            ));
            $coordinator_items = $this->OppOrderItem->find('all',array(
                'conditions'=>array(
                    'OppOrderItem.cms_user_id'=>$this->logged_user['CmsUser']['id'],
                    'OppOrderItem.kos'=>0,
                    'OppOrder.datum_potvrzeni != '=>'0000-00-00 00:00:00',
                    'OppOrder.kos'=>0
                )
            ));
            unset($this->OppOrderItem);
            
            /**
             * Nacteni co uz ma klient prideleno pro dany pracovni pomer
             */
            $this->loadModel('ConnectionClientOpp');
            $last_opp_client_date = $this->ConnectionClientOpp->find('all',array(
                'conditions'=>array('client_id'=>$client_id,'connection_client_requirement_id'=>$connection_id,'kos'=>0),
                'fields'=>array('typ','DATE_FORMAT(created,"%Y-%m-%d") as created'),
                'group'=>'typ',
                'order'=>'id DESC'
            )); 
            
            //pr($last_opp_client_date);
            $this->set('last_opp_client_date', Set::combine($last_opp_client_date,'{n}.ConnectionClientOpp.typ','{n}.0.created'));     
            unset($this->ConnectionClientOpp);
        
            $this->set('work_position',Set::combine($work_position,'{n}.ConnectionPositionOpp.type','{n}.ConnectionPositionOpp'));
            $this->set('kinds',Set::combine($coordinator_items,'{n}.OppOrderItem.type_id','{n}.OppOrderItem.type_id'));
            $this->set('types',Set::combine($coordinator_items,'{n}.OppOrderItem.name','{n}.OppOrderItem.name','{n}.OppOrderItem.type_id'));
            $this->set('sizes',Set::combine($coordinator_items,'{n}.OppOrderItem.size','{n}.OppOrderItem.size','{n}.OppOrderItem.name'));
            $this->set('client_id',$client_id);
            $this->set('company_id',$company_id);
            $this->set('connection_id',$connection_id);
            //pr($coordinator_items);
            //pr($r);
        }
        else{
            //SAVE
            $save = true;
            $client_id = $this->data['client_id'];
            $company_id = $this->data['company_id'];
            $connection_id = $this->data['connection_id'];
            
            $this->loadModel('Company');
            $stat_id = $this->Company->read('stat_id',$company_id);
            unset($this->Company);
            unset($this->data['client_id']);
            unset($this->data['company_id']);
            unset($this->data['connection_id']);
            
            /**
             * Kontrola prideleni a potvrzeni skladu
             */
            $this->loadModel('OppOrderItem');
            $this->OppOrderItem->bindModel(array(
                'belongsTo'=>array('OppOrder')
            ));
            $ci = $this->OppOrderItem->find('all',array(
                'conditions'=>array(
                    'OppOrderItem.cms_user_id'=>$this->logged_user['CmsUser']['id'],
                    'OppOrder.datum_potvrzeni != '=>'0000-00-00 00:00:00',
                    'OppOrder.kos'=>0
                )
            ));
            //unset($this->OppOrderItem);
            $coordinator_items = Set::combine($ci,'{n}.OppOrderItem.id','{n}.OppOrderItem','{n}.OppOrderItem.type_id');

            $not_store = array();
            foreach($this->data as $typ=>$data){
                   /**
                    * ty co nejsou potvrzeny se nevaliduji a neposilaji!
                    */ 
                   if($data['potvrzeni'] == "0"){
                        unset($this->data[$typ]);
                        continue;
                   } 
                
                   if(isset($coordinator_items[$typ])){ 
                       foreach($coordinator_items[$typ] as $opp_id=>$item){
                            if($item['size'] == $data['size'] && $item['name'] == $data['name'] && (isset($this->data[$typ]['opp_item_id']) == false || (isset($this->data[$typ]['opp_item_id']) && isset($this->data[$typ]['count']) && $this->data[$typ]['count'] < 1 && $item['count'] > 0))){
                                $this->data[$typ]['opp_item_id'] = $opp_id;
                                $this->data[$typ]['price'] = ($stat_id['Company']['stat_id'] == 1 ? $item['price_cz'] : $item['price_eu']);
                                $this->data[$typ]['count'] = $item['count'];
                            }  
                       }
                       
                       if(!isset($this->data[$typ]['count']) || $this->data[$typ]['count'] < 1){
                            $not_store[] = $typ." (".$this->data[$typ]['name'].") ,velikost: ".$this->data[$typ]['size']." - nemáte naskladněno";
                            $save = false;
                       } 
                       
                   }
                   else{
                     die(json_encode(array('result'=>false,'message'=>'Nemáte naskladněné, patřičné OPP!!!')));
                   }      
            }
            //pr($this->data);
            if($save === true){
                $this->loadModel('ConnectionClientOpp');

                foreach($this->data as $typ=>$data){
                    if(!isset($data['opp_item_id']) || $data['opp_item_id'] == '')
                        die(json_encode(array('result'=>false,'message'=>'Nenalezeno patřičné ID Opp prvku!')));
                    
                    
                    $data['client_id'] = $client_id;
                    $data['company_id'] = $company_id;
                    $data['connection_client_requirement_id'] = $connection_id;
                    $data['cms_user_id'] = $this->logged_user['CmsUser']['id'];
                    $data['typ'] = $typ;
                  //  pr($data);
                    $this->ConnectionClientOpp->id = null;
                    $this->ConnectionClientOpp->save($data);
                    
                    $this->OppOrderItem->updateAll(
                        array('count'=>'count - 1'),
                        array('id'=>$data['opp_item_id'],'cms_user_id'=>$this->logged_user['CmsUser']['id'])
                    );
                }
                unset($this->ConnectionClientOpp);
                
                die(json_encode(array('result'=>true)));
            }
            else
                die(json_encode(array('result'=>false,'message'=>"Nemáte naskladněné potřebné OPP :\n\n".join("\n",$not_store))));
        }
    }
    
    /**
     * Historie daneho klienta o pridelovani OPP
     */
    function history_client_opp($client_id = null){
        if($client_id == null)
            die('Nebyl nastaven klient ID');
            
        $this->loadModel('ConnectionClientOpp');
        $this->set('items',$this->ConnectionClientOpp->find('all',array(
            'conditions'=>array('client_id'=>$client_id,'kos'=>0)
        )));    
        
        
    }
    
    function print_opp(){
        $this->layout = 'print';
        if(isset($_POST)){$this->data = $_POST['data'];}

        $client_id = $this->data['client_id'];
        unset($this->data['client_id']);
        $company_id = $this->data['company_id'];
        unset($this->data['company_id']);
        $connection_id = $this->data['connection_id'];
        unset($this->data['connection_id']);
        
        $this->loadModel('Client');
        $cl = $this->Client->read('name',$client_id);
        $this->set('komu',$cl['Client']['name']);
        $this->set('opp',$this->data);
        //die('fdsafdsa');
    }
    
     /**
     * funkce pro vytvoreni exportu do excelu - CSV
     * podle filtrace vyber dane klienty a vygeneruj je do csv
     */
    function export_excel(){
        Configure::write('debug',1);
        
       $start = microtime ();
       $start = explode ( " " , $start );
       $start = $start [ 1 ]+ $start [ 0 ];  
        
         $fields_sql = array(
            "ClientView.id",
            "ClientView.jmeno",
            "ClientView.prijmeni",
            "ClientView.email",
            "ClientView.private_email",
            "ClientView.Profese",
            "ClientView.company",
            "ClientView.client_manager",
            "ClientView.coordinator",
            "ClientView.coordinator2",
            "ClientView.stav",
            "ConnectionClientRequirement.from",
            "ConnectionClientRequirement.to",
            "CompanyMoneyItem.name",
            "ClientView.chybna_dokumentace",
            "SettingStat.name"
         );
               
        $criteria = $this->ViewIndex->filtration();        
      
    
         header("Pragma: public"); // požadováno
         header("Expires: 0");
         header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
         header("Cache-Control: private",false); // požadováno u některých prohlížečů
         header("Content-Transfer-Encoding: binary");
         Header('Content-Type: application/octet-stream');
         Header('Content-Disposition: attachment; filename="'.date('Ymd_His').'.csv"');
     
      
        /*
         * Celkovy pocet zaznamu
         */
         
        $limit = 100;   
        $page = 1;     
        $this->ClientView->bindModel($this->renderSetting['bindModel'],false);   
        $this->ClientView->bindModel(array('belongsTo'=>array(
            'SettingStat' => array('foreignKey'=>'stat_id')
        )),false);
        $count = $this->ClientView->find('first',
        	array(
        		'fields' =>  array("COUNT(DISTINCT ClientView.id) as count"),
        		'conditions'=>am($criteria,$this->renderSetting['SQLcondition']),
        		'recursive'	=>1,
                //'limit'=>10
        	)
        );    
        $count = $count[0]['count'];
        $this->renderSetting['items'] = am($this->renderSetting['items'],array(
            'email'=>'Email|ClientView|email|text|',
            'private_email'=>'Soukromý Email|ClientView|private_email|text|',
            'stat'=>'Stat|SettingStat|name|text|',
        ));
        unset($this->renderSetting['items']['stav_dok']);
        // hlavicka
        foreach($this->renderSetting['items'] as &$item_setting){
            list($caption, $model, $col, $type, $fnc) = explode('|',$item_setting);
        	$item_setting = compact(array("caption", "model","col","type","fnc"));	
            if($type != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',$caption).'";'; 
        }
        echo "\n";   
        unset($item_setting, $caption, $model, $col, $type, $fnc);
        
        $str_array=array("<br/>"=>', ',':'=>',',';'=>',','?'=>'', '#'=>' ');
        /*
         * Cyklicky vypis dat po $limit zaznamu
         */
        
        for ($exported = 0; $exported < $count; $exported += $limit){
            $this->ClientView->bindModel($this->renderSetting['bindModel'],false);
            foreach($this->ClientView->find('all',array(
                   'fields'=>$fields_sql,
                    'conditions'=>am($criteria,$this->renderSetting['SQLcondition']),
                    'group'=>$this->renderSetting['group_by'],
                    'limit'=>$limit,
                    'page'=>$page,
                    'recursive'=>1,
                    'order'=>'ClientView.id ASC'
            )) as $item){
                foreach($this->renderSetting['items'] as $key => $td){
                    if($td['type'] != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',strtr($this->ViewIndex->generate_td($item,$td,$this->viewVars),$str_array)).'";';     
                }
                echo "\n";  
            }
            $page++;
        }
       
        //time
         $end = microtime ();
         $end = explode ( " " , $end );
         $end = $end [ 1 ]+ $end [ 0 ]; 
         echo 'Generate in '.($end - $start).'s';
         echo "\n"; 
        
        die();
    }
    
    /**
     * Vytovreni html sablony karticek
     * Vyber klientu dle zvolene filtrace a vytvoreni html sablony pro vsechny znich
     * Na okne se zobrazi 
     */
    function transport_card(){
        $filtr = null;
        foreach($_POST as $key=>$val){ $filtr.= $key.'='.$val.'&';}

        $this->set('route_list',$this->get_list('RouteList'));
        $this->set('refresh_url','/client_employees/transport_card_print/?'.rtrim($filtr,'&'));
    }
    
    /**
     * Vytovreni html sablony karticek
     * Vyber klientu dle zvolene filtrace a vytvoreni html sablony pro vsechny znich
     * Na okne se zobrazi 
     */
    function transport_card_print(){
        $this->layout = 'print';
        $this->data = $_GET;
        unset($this->data['url']);
        //if(!empty($this->data)){
            
        //trasa    
        $this->loadModel('RouteList');    
        $route = $this->RouteList->read('name',$this->data['route_id']);    
        $this->set('route',$route['RouteList']['name']);   
        unset($this->data['route_id']); 
        
        $this->set('from_date',$this->data['from_date']);   
        unset($this->data['from_date']); 
        
        $this->set('to_date',$this->data['to_date']);   
        unset($this->data['to_date']); 

        
         $fields_sql = array(
            "ClientView.id",
            "ClientView.name",
            "ClientView.company",
         );
         
        if(isset($this->data['id']) && $this->data['id'] != ''){
            $criteria = array('ClientView.id'=>explode('|',$this->data['id'])); 
        }       
        else
            $criteria = $this->ViewIndex->filtration();        
        
        $this->ClientView->bindModel($this->renderSetting['bindModel'],false);
        $clients = $this->ClientView->find('all',array(
               'fields'=>$fields_sql,
                'conditions'=>am($criteria,$this->renderSetting['SQLcondition']),
                'group'=>$this->renderSetting['group_by'],
                'recursive'=>1,
                'order'=>'ClientView.id ASC'
        ));
        $this->set('clients_list',$clients);
    }

    /**
     * Funkce odebira opp od klienta a vraci ho na sklad koordinátora
     */
    function client_opp_remove($connection_id){
        if($connection_id == null)
            die(json_encode(array('result'=>false,'message'=>'Chyba vstupu!')));
          
        //nacteni dat
        $this->loadModel('ConnectionClientOpp');
        $this->ConnectionClientOpp->bindModel(array('belongsTo'=>array('CmsUser')));
        $cco = $this->ConnectionClientOpp->read(array('ConnectionClientOpp.*','CmsUser.at_employee_id'),$connection_id);
        if($cco['ConnectionClientOpp']['srazka_ze_mzdy'] == 1)
            die(json_encode(array('result'=>false,'message'=>'Toto OPP již neleze vrátit, byla vykonána srážka ze mzdy!')));
        
        
        $this->loadModel('ConnectionClientAtCompanyWorkPosition');
        $is_zam = $this->ConnectionClientAtCompanyWorkPosition->find('first',array(
            'conditions'=>array(
                'client_id'=>$cco['CmsUser']['at_employee_id'],
                'datum_to'=>'0000-00-00',
                'kos'=>0
            )
        ));
        
        $message = "";
        if($is_zam){
            //je stale zamestany, muzeme klasicky odecist od coo
            $this->loadModel('OppOrderItem');    
             $opp = $this->OppOrderItem->find('first',array(
                'conditions'=>array(
                    'company_id'=>$cco['ConnectionClientOpp']['company_id'],
                    'cms_user_id'=>$cco['ConnectionClientOpp']['cms_user_id'],
                    'type_id'=>$cco['ConnectionClientOpp']['typ'],
                    'name'=>$cco['ConnectionClientOpp']['name'],
                    'size'=>$cco['ConnectionClientOpp']['size'],
                    'kos'=>0
                ),'fields'=>array('id','count')  
            ));
            if($opp){
                $this->OppOrderItem->id = $opp['OppOrderItem']['id'];
                $this->OppOrderItem->saveField('count',$opp['OppOrderItem']['count'] + 1);
                $message = "Opp bylo vráceno na sklad koordinátora.";
            }
            else{
                die(json_encode(array('result'=>false,'message'=>'OPP nenalezeno!')));
            }
        }
        else{
           //coo, nebo int. zam. byl propusten a opp vracime rovnou na sklad OPP
           $this->loadModel('OppItem'); 
           $exist = $this->OppItem->find('first',array('conditions'=>array(
                'name'=>$cco['ConnectionClientOpp']['name'],
                'size'=>$cco['ConnectionClientOpp']['size'],
                'type'=>$cco['ConnectionClientOpp']['typ'],
                'kos'=>0
           )));
           if($exist){
               $this->OppItem->query('UPDATE wapis__opp_items SET count = count + 1 WHERE id = '.$exist['OppItem']['id']);
           } 
           $message = "Opp bylo vráceno na sklad OPP, uživatel již není zaměstnán!";
        }
        
        //ulozeni stare connection,m priradime do kose
        $this->ConnectionClientOpp->id = $connection_id;
        $this->ConnectionClientOpp->saveField('kos',1);
      
        die(json_encode(array('result'=>true,'message'=>$message)));   
    }
    
    function opp_transfer($client_id = null){
        $this->loadModel('ConnectionClientOpp');
        if(empty($this->data)){
            
            $this->set('items',$this->ConnectionClientOpp->find('all',array(
                'conditions'=>array(
                    'client_id'=>$client_id,'kos'=>0,
                    'ConnectionClientOpp.to_other_client'=>0,
                    'ConnectionClientOpp.vraceno'=>0,
                    'ConnectionClientOpp.srazka_ze_mzdy'=>0
                )
            )));   
            
            $this->set('company_list',$this->get_list('Company'));
            $this->set('client_id',$client_id); 
        }
        else{//SAVE
            $connection_client_id = $this->data['connection_client_id'];
            $company_id = $this->data['company_id'];
            $this->loadModel('ConnectionClientRequirement');
            $_client_id = $this->ConnectionClientRequirement->read(null,$connection_client_id);
            $client_id = $_client_id['ConnectionClientRequirement']['client_id'];
            
            foreach($this->data['OppTransfer'] as $connection_id=>$chck){
                if($chck == 1 || $chck == 'on'){
                    $old_data = $this->ConnectionClientOpp->read(null,$connection_id);
                    //save stareho
                    $this->ConnectionClientOpp->id = $connection_id;
                    $this->ConnectionClientOpp->saveField('to_other_client',$client_id);
                    
                    $this->ConnectionClientOpp->id = null;
                    $new_data = array(
                        'connection_client_requirement_id'=>$connection_client_id,
                        'client_id'=>$client_id,
                        'cms_user_id'=>$this->logged_user['CmsUser']['id'],
                        'company_id'=>$company_id,
                        'typ'=>$old_data['ConnectionClientOpp']['typ'],
                        'name'=>$old_data['ConnectionClientOpp']['name'],
                        'size'=>$old_data['ConnectionClientOpp']['size'],
                        'price'=>$old_data['ConnectionClientOpp']['price'],
                        'narok'=>$old_data['ConnectionClientOpp']['narok']
                    );
                    $this->ConnectionClientOpp->save($new_data);
                    $this->ConnectionClientOpp->id = null;
                }
            }
            die(json_encode(array('result'=>true)));   
        }     
    }
    
    /**
     * nactnei aktivnich zamestnancu u spolencosti na ktere muzem prenest OPP
     */
    function load_employees_in_company($company_id,$client_id){
        $this->loadModel('ConnectionClientRequirement');
        $this->ConnectionClientRequirement->bindModel(array('belongsTo'=>array('Client')));
        $items = $this->ConnectionClientRequirement->find('all',array(
            'conditions'=>array(
                'ConnectionClientRequirement.company_id'=>$company_id,
                'ConnectionClientRequirement.kos'=>0,
                'ConnectionClientRequirement.to'=>'0000-00-00',
                'ConnectionClientRequirement.type'=>2,
                'Client.kos'=>0,
                'ConnectionClientRequirement.client_id != '=>$client_id 
            ),
            'fields'=>array('ConnectionClientRequirement.id','Client.name')
        ));
        
        die(json_encode(Set::combine($items,'/ConnectionClientRequirement/id','/Client/name')));  
    }
}
?>