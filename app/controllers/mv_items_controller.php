<?php
Configure::write('debug', 1);
class MvItemsController extends AppController
{
    var $name = 'MvItems';
    var $helpers = array('htmlExt', 'Pagination', 'ViewIndex', 'FileInput');
    var $components = array('ViewIndex', 'RequestHandler', 'Upload');
    var $uses = array('MvItem');
    var $renderSetting = array(
        'SQLfields' => '*',
        'bindModel' => array(
            'joinSpec' => array(
                'MvCategory' => array(
                    'foreignKey' => 'druh',
                    'primaryKey' => 'MvCategory.name'
                )
            )
        ),
        'controller' => 'mv_items',
        'page_caption' => 'Evidencia dolnkoveho tovaru MV',
        'sortBy' => 'MvItem.id.ASC',
        'top_action' => array(
            // caption|url|description|permission
            'add_item' => 'Přidat|edit|Přidat MV|add',
        ),
        'filtration' => array(

        ),
        'items' => array(
            'id' => 'ID|MvItem|id|text|',
            'cat' => 'Druh|MvCategory|name|text|',
            'name' => 'Název|MvItem|name|text|',
            'code' => 'Kód|MvItem|code|text|',
            'pocet' => 'Počet|MvItem|pocet|text|',
            'CZK' => 'CZK|MvItem|cena_cz|text|',
            'EUR' => 'EUR|MvItem|cena_eur|text|',
            'kurz' => 'Kurz|MvItem|kurz|text|',
        ),
        'posibility' => array(
            'status' => 'status|Změna stavu|status',
            'edit' => 'edit|Editace položky|edit',
            'trash' => 'trash|Do kosiku|trash'
        )
    );

    function index()
    {
        $this->set('fastlinks', array('ATEP' => '/', 'Administrace' => '#', $this->renderSetting['page_caption'] => '#'));

        if ($this->RequestHandler->isAjax()) {
            $this->render('../system/items');
        } else {
            $this->render('../system/index');
        }
    }

    function edit($id = null)
    {
        $this->autoLayout = false;
        if (empty($this->data)) {
            if ($id != null) {
                $this->data = $this->MvItem->read(null, $id);
            } else {
                $this->loadModel('OppItem');
                $sett = $this->OppItem->find('first', array('fields' => array('rate')));
                unset($this->OppItem);
                $this->data['MvItem']['kurz'] = $sett['OppItem']['rate'];
            }

            $this->loadModel('MvCategory');
            $this->set('type_list', $this->MvCategory->find('list', array('conditions' => array('kos' => 0, 'status' => 1), 'fields' => array('name', 'name'))));

            $this->render('edit');
        } else {
            $this->MvItem->save($this->data['MvItem']);
            die(json_encode(array('result' => true)));
        }
    }
}

?>