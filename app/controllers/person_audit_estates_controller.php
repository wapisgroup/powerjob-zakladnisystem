<?php
class PersonAuditEstatesController extends AppController {
	var $name = 'PersonAuditEstates';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('CompanyPerson');
	var $renderSetting = array(
		'bindModel'	=> array('belongsTo'=>array('CompanyAuditEstate'=>array('foreignKey'=>'company_id'))),
		'SQLfields' => '*,
                       (
                            SELECT COUNT(id) 
                            FROM wapis__connection_audit_estates
                            Where company_person_id=CompanyPerson.id and kos=0 and to_date = "0000-00-00"
                       ) as sum_estate,
                       (
                            SELECT SUM(a.hire_price) 
                            FROM wapis__connection_audit_estates as c
                            LEFT JOIN wapis__audit_estates as a ON(a.id=c.audit_estate_id)
                            Where company_person_id=CompanyPerson.id and c.kos=0 and c.to_date = "0000-00-00"
                       ) as sum_hire_price
        ',
        'SQLcondition'=>array(
            '(SELECT COUNT(id) 
                FROM wapis__connection_audit_estates
              Where company_person_id=`CompanyPerson.id` and kos=0 and to_date = "0000-00-00"
              ) > 0'
        ),
		'controller'=> 'person_audit_estates',
		'page_caption'=>'Majetek podle osob',
		'sortBy'=>'CompanyPerson.name.ASC',
		'top_action' => array(
			'add_item'		=>	'Přidat|edit|Pridat majetek|add',
		),
		'filtration' => array(
			'CompanyPerson-name'				=>	'text|Jméno|',
			'CompanyPerson-company_id'	    =>	'select|Firma|company_list',
		 	
		),
		'items' => array(
			'id'		=>	'ID|CompanyPerson|id|text|',
			'name'		=>	'Jméno|CompanyPerson|name|text|',
			'company'	=>	'Firma|CompanyAuditEstate|name|text|',
			'sum_estate'=>	'Celkem majetku|0|sum_estate|text|',
			'sum_hire_price'=>	'Mesíční pronájem|0|sum_hire_price|money2|'
		),
		'posibility' => array(
			'show'		        =>	'show|Detail položky|show',
           	'print_detail'		=>	'print_detail|Tisk položky|print_detail',
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Evidence majetku'=>'#',$this->renderSetting['page_caption']=>'#'));

            $this->loadModel('CompanyAuditEstate');
            $company_list = $this->CompanyAuditEstate->find('list',array('condtions'=>array('kos'=>0)));
            $this->set('company_list',$company_list);

		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
    
    /**
     * detail osoby a vypis jejiho majetku
     */
	function show($person_id = null){
            
            $this->loadModel('ConnectionAuditEstate');
            $this->ConnectionAuditEstate->bindModel(array('belongsTo'=>array('AuditEstate')));
            $estate_list = $this->ConnectionAuditEstate->find('all',array(
                'conditions'=>array(
                    'ConnectionAuditEstate.kos'=>0,
                    'ConnectionAuditEstate.company_person_id'=>$person_id
                )
            ));
            $this->set('estate_list',$estate_list);
	}
    
    
    /**
     * funkce pro odevzdani majetku zpet firme
     */
    function give_off($connection_audit_estate_id = null){
        if(empty($this->data)){
            $this->loadModel('ConnectionAuditEstate');
            $this->data = $this->ConnectionAuditEstate->read(array('audit_estate_id'),$connection_audit_estate_id);
            $this->data['ConnectionAuditEstate']['id'] = $connection_audit_estate_id;
            
            //render
            $this->render('give_off');
        }        
        else{            
            $this->loadModel('ConnectionAuditEstate');
            $this->loadModel('AuditEstate');
            if($this->ConnectionAuditEstate->save($this->data)){
                $this->AuditEstate->save(array(
                    'id'=>$this->data['ConnectionAuditEstate']['audit_estate_id'],
                    'stav'=>0 
                ));
                die(json_encode(array('result'=>true)));
            } else
                die(json_encode(array('result'=>false)));
         }
    }
    
    /**
     * print detailu
     */
    function print_detail($person_id){
       $this->layout = 'print';
            $this->CompanyPerson->bindModel(array('belongsTo'=>array('CompanyAuditEstate'=>array('foreignKey'=>'company_id'))));
            $this->set('detail',$this->CompanyPerson->read(null,$person_id));
        
        
            $this->loadModel('ConnectionAuditEstate');
            $this->ConnectionAuditEstate->bindModel(array('belongsTo'=>array('AuditEstate')));
            $estate_list = $this->ConnectionAuditEstate->find('all',array(
                'conditions'=>array(
                    'ConnectionAuditEstate.kos'=>0,
                    'ConnectionAuditEstate.to_date'=>'0000-00-00',
                    'ConnectionAuditEstate.company_person_id'=>$person_id
                )
            ));
            $this->set('estate_list',$estate_list);
    
        //render
        $this->render('print');
    }
    
   
}
?>