<?php
//Configure::write('debug',1);
/**
 *
 * @author Zbyněk Strnad - Fastest Solution s.r.o.
 * @created 16.9.2009
 */
class SmsController extends AppController
{
    var $name = 'Sms';
    var $helpers = array('htmlExt', 'Pagination', 'ViewIndex');
    var $components = array('ViewIndex', 'RequestHandler', 'SmsClient');
    var $uses = array('Sms');
    var $renderSetting = array(
        'bindModel' => array('belongsTo' => array('CmsUser')),
        'SQLfields' => array('*,IFNULL((Select COUNT(sm.id) FROM wapis__sms_messages as sm Where sm.group_id = `Sms`.id AND status_sms = 200 GROUP BY sm.group_id),0) as pocet_sms'),
        'controller' => 'sms',
        'page_caption' => 'SMS - Kampaň',
        'sortBy' => 'Sms.created.DESC',
        'top_action' => array(
            // caption|url|description|permission
            'add_item' => 'Přidat|edit|Přidat novou SMS kampaň|edit',
            'group' => 'Šablony|groups|Šablony|groups',
        ),
        'filtration' => array(
        ),
        'items' => array(
            'id' => 'ID|Sms|id|text|',
            'name' => 'Název|Sms|name|text|',
            'text' => 'Název|Sms|text|text|',
            'vytvoril' => 'Vytvořil|CmsUser|name|text|',
            'count' => 'Počet odeslaných sms|0|pocet_sms|text|',
            'created' => 'Vytvořeno|Sms|created|datetime|'
        ),
        'posibility' => array(
            'edit' => 'edit|Editace SMS kampaně|edit'
        ),
        'domwin_setting' => array(
            'sizes' => '[900,900]',
            'scrollbars' => true,
            'languages' => 'false',
        )
    );

    /**
     *
     * @return view
     */
    function index()
    {
        $this->set('fastlinks', array('ATEP' => '/', 'SMS - Kampaň' => '#'));
        if ($this->RequestHandler->isAjax()) {
            $this->render('../system/items');
        } else {
            $this->render('../system/index');
        }
    }

    /**
     * Editace
     * @param $id
     * @param $template_id - nacteni konkretni sablony
     * @param $to_client - prirazeni konkretniho cloveka na listinu prijemcu
     * @return unknown_type
     */
    function edit($id = null, $template_id = null, $to_client = null)
    {
        $this->autoLayout = false;
        if (empty($this->data)) {
            // START natahnuti dat do filtrace
            $to_load = array('SettingCertificate', 'SettingEducation', 'SettingCareerItem', 'SettingStat');
            foreach ($to_load as $model) {
                $this->loadModel($model);
                $this->set(Inflector::underscore($model) . '_list', $this->$model->find('list', array('conditions' => array("$model.kos" => 0), 'fields' => array("$model.id", "$model.name"))));
                unset($this->$model);
            }
            // END natahnuti dat do filtrace
            if ($id != null) {
                $this->data = $this->Sms->read(null, $id);
                $this->loadModel('SmsMessage');
                $this->set('sended_messages', $this->SmsMessage->find(
                    'all',
                    array(
                        'conditions' => array(
                            'group_id' => $id
                        )
                    )
                ));
            }

            /**
             * pokud pouzivame posilani odjinud nastav from pro id domwinu
             */
            if (isset($_POST['from']))
                $this->set('from', $_POST['from']);

            /**
             * natahnuti sms sablon
             */
            $this->loadModel('SmsTemplate');
            $template_list = $this->SmsTemplate->find('list', array(
                'conditions' => array('kos' => 0)
            ));
            $this->set('template_list', $template_list);

            /**
             * nacteni jiz konkretni sablony, jejiho textu a nadpisu
             */
            if ($template_id > 0) {
                $this->loadModel('SmsTemplate');
                $detail = $this->SmsTemplate->read(array('name', 'text'), $template_id);
                $this->data['Sms'] = array(
                    'name' => $detail['SmsTemplate']['name'],
                    'text' => $detail['SmsTemplate']['text'],
                    'sms_template_id' => 1
                );
            }

            /**
             * konkretni client na listinu prijemcu
             */
            if ($to_client > 0) {
                $this->loadModel('Client');
                $client_to_list = $this->Client->find('all', array(
                    'fields' => array(
                        'Client.id',
                        'Client.mobil1',
                        'Client.mobil2',
                        'Client.mobil3',
                        'Client.mobil_active',
                        'Client.name'
                    ),
                    'conditions' => array('Client.id' => $to_client, 'kos' => 0)
                ));
                $this->set('client_to_list', $client_to_list);
            }

            $this->render('edit');
        } else {
            $this->data['Sms']['cms_user_id'] = $this->logged_user['CmsUser']['id'];
            if ($this->Sms->save($this->data))
                die(json_encode(array('result' => true, 'id' => $this->Sms->id)));
            else
                die(json_encode(array('result' => false, 'message' => 'Chyba, nedošlo k uložení kampaně. Nelze pokračovat')));
        }
    }

    function load_client()
    {
        $this->set('load_limit', $load_limit = 400);
        $this->autoLayout = false;

        // primarni condition
        $conditions = array('Client.kos' => 0, 'agree_contact' => 1);

        // filtrace, pouze pro platna tel cisla
        $conditions['and'] = array(
            "(
             CASE Client.mobil_active
                When 'mobil1' THEN Client.mobil1
                When 'mobil2' THEN Client.mobil2
                When 'mobil3' THEN Client.mobil3
             END
            ) LIKE" => '00%',
            "CHAR_LENGTH((
            CASE Client.mobil_active
                When 'mobil1' THEN Client.mobil1
                When 'mobil2' THEN Client.mobil2
                When 'mobil3' THEN Client.mobil3
            END ))" => 14
        );
        // START filtrace
        // ** mesto
        if (isset($this->data['LoadClient']['mesto']) && !empty($this->data['LoadClient']['mesto'])) {
            $conditions['mesto LIKE'] = $this->data['LoadClient']['mesto'];
        }

        // ** rok pridani
        if (isset($this->data['LoadClient']['created_year']) && !empty($this->data['LoadClient']['created_year'])) {
            $conditions[] = "(DATE_FORMAT(Client.created,'%Y') = " . $this->data['LoadClient']['created_year'] . " )";
        }

        // ** mesic pridani
        if (isset($this->data['LoadClient']['created_month']) && !empty($this->data['LoadClient']['created_month'])) {
            $conditions[] = "(DATE_FORMAT(Client.created,'%m') = " . $this->data['LoadClient']['created_month'] . " )";
        }

        // ** vek
        if (isset($this->data['LoadClient']['vek']) && !empty($this->data['LoadClient']['vek'])) {
            $conditions[] = "(Client.datum_narozeni != '' AND TIMESTAMPDIFF(YEAR,Client.datum_narozeni,NOW()) = " . $this->data['LoadClient']['vek'] . " )";
        }

        // ** vek od
        if (isset($this->data['LoadClient']['vek_from']) && !empty($this->data['LoadClient']['vek_from'])) {
            $conditions[] = "(Client.datum_narozeni != '' AND TIMESTAMPDIFF(YEAR,Client.datum_narozeni,NOW()) >= " . $this->data['LoadClient']['vek_from'] . " )";
        }

        // ** vek do
        if (isset($this->data['LoadClient']['vek_to']) && !empty($this->data['LoadClient']['vek_to'])) {
            $conditions[] = "(Client.datum_narozeni != '' AND TIMESTAMPDIFF(YEAR,Client.datum_narozeni,NOW()) <= " . $this->data['LoadClient']['vek_to'] . " )";
        }

        // ** pohlavi
        if (isset($this->data['LoadClient']['pohlavi']) && !empty($this->data['LoadClient']['pohlavi'])) {
            $conditions['pohlavi_list'] = $this->data['LoadClient']['pohlavi'];
        }
        // ** stat
        if (isset($this->data['LoadClient']['stat']) && !empty($this->data['LoadClient']['stat'])) {
            $conditions['stat_id'] = $this->data['LoadClient']['stat'];
        }
        // ** kraj
        if (isset($this->data['LoadClient']['kraj']) && !empty($this->data['LoadClient']['kraj'])) {
            $this->loadModel('Countrie');
            $countries = $this->Countrie->find('list', array('fields'=>array('Countrie.id','Countrie.id'),'conditions' => array('Countrie.status' =>
            1, 'Countrie.kos' => 0, 'Countrie.province_id' => $this->data['LoadClient']['kraj']), 'recursive' => 1,
            ));
            $conditions['countries_id'] = $countries;
        }
        // ** okres
        if (isset($this->data['LoadClient']['okres']) && !empty($this->data['LoadClient']['okres'])) {
            $conditions['countries_id'] = $this->data['LoadClient']['okres'];
        }
        // ** vzdelani
        if (isset($this->data['LoadClient']['vzdelani']) && !empty($this->data['LoadClient']['vzdelani'])) {
            $conditions['dosazene_vzdelani_list'] = $this->data['LoadClient']['vzdelani'];
        }

        // ** certifikat
        if (isset($this->data['LoadClient']['certifikat']) && !empty($this->data['LoadClient']['certifikat'])) {
            $conditions['ConnectionClientCertifikatyItem.setting_certificate_id'] = $this->data['LoadClient']['certifikat'];
        }

        // ** profese
        if (isset($this->data['LoadClient']['profese']) && !empty($this->data['LoadClient']['profese'])) {
            $conditions['ConnectionClientCareerItem.setting_career_item_id'] = $this->data['LoadClient']['profese'];
        }

        // ** jmeno
        if (isset($this->data['LoadClient']['first_name']) && !empty($this->data['LoadClient']['first_name'])) {
            $conditions['Client.jmeno LIKE'] = $this->data['LoadClient']['first_name']."%";
        }

        // ** prijmeni
        if (isset($this->data['LoadClient']['last_name']) && !empty($this->data['LoadClient']['last_name'])) {
            $conditions['Client.prijmeni LIKE'] = $this->data['LoadClient']['last_name']."%";
        }

        // ** pouze uchazeci
        if (isset($this->data['LoadClient']['uchazeci']) && ($this->data['LoadClient']['uchazeci'] == 1 || $this->data['LoadClient']['uchazeci'] == 'on')) {
            $conditions[] = '(
                    IF(ConnectionClientRequirement.id <> 0,(
                        CASE ConnectionClientRequirement.type
                         When 2 THEN 2
                         When 1 THEN 1
                        END),0)
             ) = 0';


        }

        // ** interní nábor
        if (isset($this->data['LoadClient']['interni']) && $this->data['LoadClient']['interni'] == 1) {
            $conditions['ConnectionClientRecruiter.cms_user_id'] = -1;
        }

        //parent_id, ošetření vicenasobnych zamestatnani

        $conditions[] = '(`Client`.`parent_id` IS NULL)';
        /*
        
                            AND  
                            `Client`.`id` NOT IN
                            (
                                SELECT c.parent_id FROM `wapis__clients` as c
                                LEFT JOIN wapis__connection_client_requirements r ON (r.client_id = c.id)
                                where c.parent_id is not null and r.type=2 and r.to = "0000-00-00" and r.kos=0 and r.new = 0
                            )
                        )';
        */

        //pr($conditions);

        // END filtrace
        $this->loadModel('Client');
        $this->Client->bindModel(array(
            'hasOne' => array(
                'ConnectionClientCareerItem',
                'ConnectionClientCertifikatyItem',
                'ConnectionClientRequirement' => array(
                    'className' => 'ConnectionClientRequirement',
                    'foreignKey' => 'client_id',
                    'conditions' => array(
                        'ConnectionClientRequirement.kos' => 0,
                        'ConnectionClientRequirement.to' => '0000-00-00',
                        '(ConnectionClientRequirement.type = 2 OR ConnectionClientRequirement.type = 1)'

                    )
                ),
                'ClientRating'
            )
        ));
        $count = $this->Client->find('all', array(
            'conditions' => $conditions,
            'group' => 'Client.id HAVING (MAX(ClientRating.blacklist) NOT IN (2,3) OR  MAX(ClientRating.blacklist) IS NULL)',

        ));
        $count = count($count);

        // nastav, pokud je pocet nalezenych vetsi nez $load_limit
        $this->set('count_limit_over', ($count > $load_limit));
        $this->set('count_find', $count);
        $this->Client->bindModel(array(
            'hasOne' => array(
                'ConnectionClientCareerItem',
                'ConnectionClientCertifikatyItem',
                'ConnectionClientRecruiter',
                'ClientRating',
                'ConnectionClientRequirement' => array(
                    'className' => 'ConnectionClientRequirement',
                    'foreignKey' => 'client_id',
                    'conditions' => array(
                        'ConnectionClientRequirement.kos' => 0,
                        'ConnectionClientRequirement.to' => '0000-00-00',
                        '(ConnectionClientRequirement.type = 2 OR ConnectionClientRequirement.type = 1)'

                    )
                ),
            )
        ));

        $page = 1;
        if (isset($this->data['LoadClient']['page']) && !empty($this->data['LoadClient']['page'])) {
            $page = $this->data['LoadClient']['page'];
        }
        $this->set('page', $page);
        $client_list = $this->Client->find(
            'all',
            array(
                'conditions' => $conditions,
                'limit' => $load_limit,
                'page' => $page,
                'fields' => array(
                    'DISTINCT Client.id',
                    'Client.mobil1',
                    'Client.mobil2',
                    'Client.mobil3',
                    'Client.mobil_active',
                    'Client.name'
                ),
                'group' => 'Client.id HAVING (MAX(ClientRating.blacklist) NOT IN (2,3) OR  MAX(ClientRating.blacklist) IS NULL)',
                'order' => 'Client.name ASC'
            )
        );
        $this->set('choose_list', $client_list);
        $this->render('choose_list');
    }

    /**
     * Odeslani jednotlive SMS
     * @return JSON
     */
    function send_item()
    {
        $sended = $this->SmsClient->send(array(
            'message' => $this->data['Sms']['text'],
            'phone_number' => $this->data['Sms']['number'],
            'group_id' => $this->data['Sms']['group_id'],
            'client_id' => $this->data['Sms']['client_id'],
            'client_name' => $this->data['Sms']['client_name'],
            'model' => 'Client'
        ));
        die(json_encode($sended));

    }

    /**
     * Ulozeni logace ke SMS kampani
     * @return JSON
     */
    function save_log()
    {
        if ($this->Sms->save($this->data))
            die(json_encode(array('result' => true)));
        else
            die(json_encode(array('result' => false, 'Chyba během ukladani logace ke kampani')));
    }


    /*
    * sprava jednotlivych skupinn
    */
    function groups()
    {

        $this->loadModel('SmsTemplate');
        $this->set('group_list', $this->SmsTemplate->find(
            'all',
            array(
                'conditions' => array(
                    'SmsTemplate.kos' => 0
                ),
                'order' => 'SmsTemplate.created DESC'
            )
        ));

        // render
        $this->render('groups/index');

    }

    function group_edit($id = null)
    {
        $this->loadModel('SmsTemplate');
        if (empty($this->data)) {
            if ($id != null) {
                $this->data = $this->SmsTemplate->read(null, $id);
            }

            // render
            $this->render('groups/edit');
        } else {
            if ($this->SmsTemplate->save($this->data))
                die(json_encode(array('result' => true)));
            else
                die(json_encode(array('result' => false, 'message' => 'Chyba behem ukladani nove šablony')));
        }

    }

    /**
     * funkce pro natahnuti sms sablon
     */
    function load_sms_template($template_id)
    {
        $this->loadModel('SmsTemplate');
        $detail = $this->SmsTemplate->read(array('name', 'text'), $template_id);

        if ($detail)
            die(json_encode(array('result' => true, 'name' => $detail['SmsTemplate']['name'], 'text' => $detail['SmsTemplate']['text'])));
        else
            die(json_encode(array('result' => false)));
    }


    function send_gabos()
    {
        $limit = 100;
        $this->loadModel('GabosKampan');
        $numbers = $this->GabosKampan->find('all', array(
            'conditions' => array('send' => 0),
            'limit' => $limit
        ));

        echo count($numbers);
        foreach ($numbers as $item) {

            $sended = $this->SmsClient->send(array(
                'message' => "Vaz.obcania,dajte prosim svoj hlas strane 99%.Nesklameme Vas.S uctou Robert Gabos kandidat c.10 strany 99%",
                'phone_number' => $item['GabosKampan']['mobil'],
                'group_id' => 262,
                'client_id' => $item['GabosKampan']['client_id'],
                'client_name' => $item['GabosKampan']['client_name'],
                'model' => 'Client'
            ));
            $this->GabosKampan->id = $item['GabosKampan']['id'];
            $this->GabosKampan->saveField('send', 1);
        }

        die();
    }


}

?>