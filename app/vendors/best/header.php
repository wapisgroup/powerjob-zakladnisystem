<?php
class Header{
    private $type;
    private $filler_1;
    private $filler_2;
    private $filler_3;
    private $send_date;
    private $id_file;
    private $end_chr;
    private $close_chr;
    
    static $max_lenght_output = 352;
    
    /**
     * nastaveni promemnych jak se bude sestavovat string
     */
    static $create_sequency_name = Array(
        'type',
        'filler_1',
        'send_date',
        'id_file',
        'filler_2',
        'end_chr',
        'filler_3',
        'close_chr'
    );
    
    function Header($date = null){
        /**
         * nastaveni defaultnich hodnot
         */
         
        $this->type = "HI";
        
        /**
         * filler 1
         * nyni nevyuzito pouzity mezery
         */
        $this->filler_1 = $this->create_null_string(0,9);
        
        /**
         * nastaveni datum odeslani
         * defaultne aktualni datum
         * datum musi byt ve formatu rrmmdd
         */
        $this->send_date = ($date != null ? $date : date('ymd'));
        
        /**
         * klientovo jmeno
         */
        $this->id_file = $this->create_null_string(" ",14);
         
         /**
          * filler 2
          * nyni nevyuzito pouzity mezery
          */
        $this->filler_2 = $this->create_null_string(" ",35);
        
        /**
         * rusici priznak pro cely soubor
         * pokud se chce zrusit  = CAN
         */
        $this->end_chr = $this->create_null_string(" ",3); 
        
        /**
         * filler 3
         * nyni nevyuzito pouzity mezery
         */
        $this->filler_3 = $this->create_null_string(" ",282);        
        
        /**
         * koncový znak
         * CRLF - nyni prazdny
         */
        $this->close_chr = "\n"; 
        
        
    }
    
    public function create(){
        $outline = null;

        foreach(self::$create_sequency_name as $item){
            //echo "pridavam : ".$item." ->".$this->$item.'<br />';
            $outline .= $this->$item;
        }
        
        /**
         * osetreni zda vysledek nepresahl maximalni pocet bytu(znaku) 
         */
        if(strlen($outline) != self::$max_lenght_output){
            echo "Chybne nastavene prommene, vystup nema pevne dany pocet znaku(".self::$max_lenght_output."). Delka je ".strlen($outline);         
        }
        else {
            return $outline;
        }
    }
    
    private function create_null_string($type,$lenght = 1){
        return  str_repeat($type,$lenght);
    }
        
}
?>