<?php
$year = $data['ClientWorkingHour']['year'];
$month = $data['ClientWorkingHour']['month'];

	$pocet_dnu_v_mesici = cal_days_in_month(CAL_GREGORIAN, $month, $year);
	$last_day = date('N', mktime(0, 0, 0, $month, 1, $year)); 
	$current_day = $last_day;
	$dny = array( 1=>'Po', 2=>'Út', 3=>'St', 4=>'Čt', 5=>'Pá', 6=>'So', 7=>'Ne' );
	
?>
	<table class='table odpracovane_hodiny'>	
				<tr>
					<th>Den</th>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<th <?php 
							if (in_array(($i+1),$svatky)) 
								echo "class='svatky'";
							else if (in_array($current_day,array(6,7))) 
								echo "class='vikend'";
						?>>
							<?php echo $dny[$current_day];?>
							<br />
							<?php echo ($i+1);?>
							<?php $current_day = ($current_day == 7)?$current_day=1:$current_day+1;?>
							
						</th>
					<?php endfor;?>
				</tr>
                <tr class='tr_accommodation'>
					<td>Stravenky</td>
					<?php 
                    $k = 4;
                    for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
					<td><?php echo (isset($data['ClientWorkingHour']['food_days']['_' .$k. '_' .($i+1)]) ? $fastest->value_to_yes_no($data['ClientWorkingHour']['food_days']['_' .$k. '_' .($i+1)],'short') : 'N');?></td>
            						<?php $current_day = ($current_day == 7)?$current_day=1:$current_day+1;?>
                    <?php endfor;?>
				</tr>
     </table>  
     
    <div class='formular_action'>
		<input type='button' value='Zavřít' id='AddEditDetailClose'/>
	</div>  
<script>
    $('AddEditDetailClose').addEvent('click',function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin_detail');
	});
</script>                