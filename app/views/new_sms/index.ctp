<div id="NewOrderBox">
    <div id="OrdersBox">
        <div id="SmsAdd">
            <?php echo $this->renderElement('../new_sms/sms');?>
        </div>
         <br />
        <div id="SmsFormBoxInfo"></div> 
        <div id="SmsWaitList">
            <?php echo $this->renderElement('../new_sms/sms_waiting_list');?>
        </div>
    </div>
    <div id="ClientsBox">
        <div class="filtration_small"><?php echo $viewIndex->filtration($renderSetting['clients'],$this->viewVars, $logged_user);?></div>
        <form action='#' method='post' id='items_form'>
        	<div id='items_clients'><?php echo $this->renderElement('../new_order/items',array('items'=>$items_clients,'renderSetting'=>$renderSetting['clients'],'paging'=>$paging_clients));?></div>
        </form>
    </div>
    <br />
</div>
<script>
$$('.filtr_button').addEvent('click', function(e){
	new Event(e).stop();   
	new Request.HTML({
		update: this.getParent('form').getElement('input[name=update_box_name]').value,
		url: this.getParent('form').getProperty('action')
	}).get(this.getParent('form'));
});

if ($$('.filtr_button_cancel')){
	$$('.filtr_button_cancel').addEvent('click', function(e){
		new Event(e).stop();
		this.getParent('form').getElements('.fltr_input, .fltr_select').each(function(item){item.value = ''})
		new Request.HTML({
			update: this.getParent('form').getElement('input[name=update_box_name]').value,
			url: this.getParent('form').getProperty('action'),
			onComplete: function(){
			}
		}).get(this.getParent('form'));
	});
}


if ($('move_client_to_sms')){
	$('move_client_to_sms').addEvent('click', function(e){
		new Event(e).stop();
		
        var active_clients_id =  new Array();
        $each($('items_clients').getElements('tbody input[type=checkbox]'),function(item){
           if(item.checked){
                active_clients_id.push(item.getParent('tr').getProperty('id'));
           } 
        });
        
            var update_wl = new Request.HTML({
    			update: 'SmsWaitList',
    			url: '/new_sms/refresh_waiting_list/1/',
    			onComplete: function(){
    			      window.fireEvent('domready');
                      window.fireEvent('resize');
    			}
		    });
            
            new Request.JSON({
    			url:'/new_sms/multi_add_to_wl/',
    			data : {
    			     'data[clients][]':active_clients_id
                },
    			onComplete: (function(json){
    				if (json) {
    				   	if (json.result === true) {	
    						if(json.wrong_clients != '')
    						  message = message+json.wrong_clients;

                            update_wl.send();
    				   	}
    				   	else {
    				   		alert(json.message);
    				   	}
    				} else {
    					alert('Chyba aplikace');
    				}
    			}).bind(this)
		    }).send();
	});
}




posun = 203;
window.addEvents({
	'domready' : function(){
		basic_body_size_c = $('items_clients').getElement('tbody').getStyle('height').toInt();
		if (window.getSize().y - posun < basic_body_size_c)
			$('items_clients').getElement('tbody').setStyle('height',window.getSize().y - posun);
            
        basic_body_size_o = $('SmsWaitList').getElement('tbody').getStyle('height').toInt();
        var sms_add = $('SmsAdd').getStyle('height').toInt();
        var filtr_client = $('ClientsBox').getElement('.filtration_small').getStyle('height').toInt();
        var strankovani  = 55;
        
        res = (window.getSize().y - posun - sms_add) + filtr_client + strankovani;
		if (res < basic_body_size_o){
        	$('SmsWaitList').getElement('tbody').setStyle('height',res);
        }      
            
		var Tips_help = new Tips($$('.div_title'),{
			showDelay: 400,
			hideDelay: 400,
			className: 'detail_table_tip'
		});
		
	},
	'resize': function(){
		if (window.getSize().y - posun < basic_body_size_c)
			$('items_clients').getElement('tbody').setStyle('height',window.getSize().y - posun);
        
        var sms_add = $('SmsAdd').getStyle('height').toInt();
        var filtr_client = $('ClientsBox').getElement('.filtration_small').getStyle('height').toInt();
        var strankovani  = 55;
        res = (window.getSize().y - posun - sms_add) + filtr_client + strankovani; 
        
        if(basic_body_size_o < 250)
            basic_body_size_o = $('SmsWaitList').getElement('tbody').getStyle('height').toInt();      
        
        if (res < basic_body_size_o){
            if(res < 260)
        	   $('SmsWaitList').getElement('tbody').setStyle('height',res);
            else 
               $('SmsWaitList').getElement('tbody').setStyle('height',260);    
        }       
	}
})

if ($$('.filtr_more')){
$$('.filtr_more').addEvent('click', function(){
    var filtr = this.getParent('.filtration_small');
	if (filtr.hasClass('big_filtr')){
		filtr.removeClass('big_filtr');
		filtr.removeClass('up');
		filtr.tween('height', 44);
	} else {
		var pocet = this.getProperty('rel');
		
		var vyska_filtrace = Math.ceil(pocet/3)*23;
		filtr.addClass('big_filtr');
		filtr.addClass('up');
		filtr.tween('height', vyska_filtrace);
	}
	return false;
});
}	
</script>