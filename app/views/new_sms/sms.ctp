    <div id="SmsFormBox">
		 	<?php echo $htmlExt->selectTag('Sms/sms_template_id',$template_list,null,array('label'=>'Šablony kampaně','class'=>'long','label_class'=>'long'));?><br/>
		 	<?php echo $htmlExt->input('Sms/name',array('readonly'=>(isset($this->data['Sms']['id']))?'readonly':false,'label'=>'Název kampaně','class'=>'long','label_class'=>'long'));?><br/>
		 	<?php echo $htmlExt->textarea('Sms/text',array('label'=>'Text SMS','class'=>'long','label_class'=>'long'));?><br/>
	</div>
    <div class="sll">
        <label style="width: 36%;">Počet možných znaků:</label><var class='long' id='sms_len'>160</var><br />
    </div>
    <div class="slr">
        <div class='formular_action' id='form_buttons'>
    		<input type='button' value='Odeslat' id='send_sms_now' />
    	</div>
    </div>
    <br />
<script type="text/javascript">
<!--
	$('sms_len').setHTML(160 - $('SmsText').value.length);
	$('SmsText').addEvents({
		'keyup': function(){
		
			$('sms_len').setHTML(160 - this.value.length);
			if (this.value.length >= 160)
				this.value = this.value.substring(0,160);
		},
        'keypress':function(e){
            var event = new Event(e);
            if(event.code == 38)
				return false;
        }
	});


	// validace
	function valid_sms_sender(){
		error = [];
		if ($('SmsName').value == '') error.push('Musíte vyplnit název SMS kampaně');
		if ($('SmsText').value == '') error.push('Musíte vyplnit text SMSky');
		if ($('SmsWaitList').getElement('tbody').getElements('tr').length == 0) error.push('Musíte zvolit příjemce SMS');

		if (error.length > 0){
			var error_message = new MyAlert();
			error_message.show(error);
			return false;
		} else {
			return true;
		}
	}
	
	// odeslani SMS
	if($('send_sms_now'))
		$('send_sms_now').addEvent('click',function(e){
			new Event(e).stop();
			if (valid_sms_sender()){
				$('form_buttons').addClass('none');
                $('send_sms_now').addClass('none');
                $('SmsWaitList').addClass('none');
                $('SmsAdd').addClass('none');
                $('SmsFormBoxInfo').setHTML('Probíhá odesílání. Vyčkejte na dokončení...');
				new Request.JSON({
					url: '/new_sms/send/',
					data: {
						'data[Sms][name]' : $('SmsName').value,
						'data[Sms][text]' : $('SmsText').value
					},
					onComplete: function(json){
						if (json){
							if (json.result === true) {
								alert("Sms byli zpracovány.\n\nOdesláno:"+json.sms_true+"\nNeodeslano:"+json.sms_false);
                                window.location = window.location;
							} else {
								alert(json.message);
							}
						} else {
							alert('Chyba aplikace');
						}
					}
				}).send();
			}
		});
	   
    
    // natahnuti sablony SMS
	if($('SmsSmsTemplateId'))
		$('SmsSmsTemplateId').addEvent('change',function(e){
		  if(this.value != ''){
			new Event(e).stop();
				new Request.JSON({
					url: '/sms/load_sms_template/'+this.value,
					onComplete: function(json){
						if (json){
							if (json.result === true) {
								$('SmsName').value = json.name;
                                $('SmsText').value = json.text;
                                //kontrola textu a uprava pocet znaku
                                $('SmsText').fireEvent('keyup');

							} else {
								alert('Chyba nacteni sablony');
							}
						} else {
							alert('Chyba aplikace')
						}
					}
				}).send();
          }else{
            $('SmsName').value = '';
            $('SmsText').value = '';
            $('SmsText').fireEvent('keyup');
          }  
		});
//-->
</script>