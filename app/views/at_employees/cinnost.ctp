<form action='/at_employees/cinnost/' method='post' id='cinnost_formular'>
	<?php echo $htmlExt->hidden('ConnectionClientAtCompanyWorkPosition/id');?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Zaměstnání</legend>
                <?php echo '<label class="long">Projekt:</label><var>'.$this->data['AtProject']['name'].'</var>'?><br />
                <?php echo $htmlExt->selectTag('ConnectionClientAtCompanyWorkPosition/at_project_centre_id',$centre_list,null,array('label'=>'Sředisko','label_class'=>'long'));?><br />
                <?php echo $htmlExt->selectTag('ConnectionClientAtCompanyWorkPosition/at_project_cinnost_id',$cinnost_list,null,array('label'=>'Činnost','label_class'=>'long'),null,true);?><br />
        	</fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Editovat zaměstnání',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
<script language="javascript" type="text/javascript">
var domtab = new DomTabs({'className':'admin_dom'});

$('save_close').addEvent('click',function(e){
    new Event(e).stop();
    new Request.JSON({
    	url:$('cinnost_formular').action,
    	onComplete:function(){
    		domwin.closeWindow('domwin');
            click_refresh($('ConnectionClientAtCompanyWorkPositionId').value);
    	}
    }).post($('cinnost_formular'));
});

$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});

$('ConnectionClientAtCompanyWorkPositionAtProjectCentreId').ajaxLoad('/int_clients/ajax_load_at_company_cinnost_list/', ['ConnectionClientAtCompanyWorkPositionAtProjectCinnostId']);
</script>
    