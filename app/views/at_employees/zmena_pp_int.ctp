
<form action='/at_employees/zmena_pp_int/' method='post' id='formular'>
<?php echo  $htmlExt->hidden('Old/id');?>
<?php echo  $htmlExt->hidden('Old/datum_nastupu');?>
<?php echo  $htmlExt->hidden('Old/client_id');?>
<?php echo $htmlExt->hidden('generate_login',array('value'=>0));?>
<?php echo $htmlExt->hidden('group_id');?>
       
            <fieldset>
		      <legend>Starý pracovního poměr</legend>
			  <?php echo $htmlExt->inputDate('do',array('tabindex'=>1,'label'=>'Datum ukončení','class'=>'','label_class'=>'long'));?> <br class="clear">		
			</fieldset>
            
            <fieldset>
    		    <legend>Nový pracovní poměr</legend>
                <?php echo $htmlExt->selectTag('ConnectionClientAtCompanyWorkPosition/at_project_id',$project_list,null,array('label'=>'Project','label_class'=>'long'));?><br />
                <?php echo $htmlExt->selectTag('ConnectionClientAtCompanyWorkPosition/at_company_id',array(0=>'Vyberte projekt'),null,array('label'=>'Firma','label_class'=>'long'),null,false);?><br />
			    <?php echo $htmlExt->selectTag('ConnectionClientAtCompanyWorkPosition/at_project_centre_id',array(0=>'Vyberte firmu'),null,array('label'=>'Sředisko','label_class'=>'long'),null,false);?><br />
                <?php echo $htmlExt->selectTag('ConnectionClientAtCompanyWorkPosition/at_project_cinnost_id',$cinnost_list,null,array('label'=>'Činnost','label_class'=>'long'),null,false);?><br />
            	<?php echo $htmlExt->selectTag('ConnectionClientAtCompanyWorkPosition/at_project_centre_work_position_id',array(0=>'Vyberte firmu'),null,array('label'=>'Profese','label_class'=>'long'),null,false);?><br />
				<?php echo $htmlExt->selectTag('ConnectionClientAtCompanyWorkPosition/at_company_money_item_id',$money_item_list,null,array('label'=>'Forma zaměstnání','label_class'=>'long'));?><br />
				
				<?php echo $htmlExt->inputDate('ConnectionClientAtCompanyWorkPosition/datum_nastupu',array('tabindex'=>1,'label'=>'Datum nástupu', 'class'=>'','label_class'=>'long'));?> <br class="clear"/>
			    <?php echo $htmlExt->selectTag('ConnectionClientAtCompanyWorkPosition/typ_smlouvy',array(1=>'Smlouva na dobu neurčitou',2=>'Smlouva na dobu určitou'),null,array('label'=>'Typ smlouvy','label_class'=>'long'),null,false);?><br />
                <div id="expirace" class="none">
                    <?php echo $htmlExt->inputDate('ConnectionClientAtCompanyWorkPosition/expirace_smlouvy',array('tabindex'=>1,'label'=>'Expirace smlouvy', 'class'=>'','label_class'=>'long'));?> <br class="clear"/>
                </div>   
            </fieldset>

	<div class='formular_action'>
		<input type='button' value='Změnit' id='VyhoditSaveAndClose' />
		<input type='button' value='Zavřít' id='VyhoditClose'/>
	</div>
</form>
<script>

    function get_date(date, separator){
        date = date.split(separator);           
        return date[2] +'.' + date[1] + '.' + date[0];
    }
    
    function compare_date(date1,date2){
        date1 = date1.split('-');
        date2 = date2.split('-');                    
        if(date1[0] == date2[0] && date1[1] == date2[1])
         return true;
        else
         return false;    
    }
   
    from_old = get_date($('OldDatumNastupu').value, '-');
        
    $('ConnectionClientAtCompanyWorkPositionAtProjectId').ajaxLoad('/int_clients/ajax_load_at_company_list/', ['ConnectionClientAtCompanyWorkPositionAtCompanyId']); 
    $('ConnectionClientAtCompanyWorkPositionAtProjectId').ajaxLoad('/int_clients/ajax_load_at_company_centre_list/', ['ConnectionClientAtCompanyWorkPositionAtProjectCentreId']);
    $('ConnectionClientAtCompanyWorkPositionAtProjectCentreId').ajaxLoad('/int_clients/ajax_load_at_company_cinnost_list/', ['ConnectionClientAtCompanyWorkPositionAtProjectCinnostId']);
    $('ConnectionClientAtCompanyWorkPositionAtProjectCentreId').ajaxLoad('/int_clients/ajax_load_at_company_work_position_list/', ['ConnectionClientAtCompanyWorkPositionAtProjectCentreWorkPositionId']);
   
    $('ConnectionClientAtCompanyWorkPositionAtProjectCentreWorkPositionId').addEvent('change',function(e){
        e.stop();
        new Request.JSON({
			url:'/int_clients/need_genereate_login/'+this.value,		
			onComplete:function(json){
				if(json){
				    if(json.result == true){
				        $('GroupId').value = json.group_id;
				        $('GenerateLogin').value = json.need;
				    }
                    else{
                        alert(json.message);
                    }
				}
                else
                    alert('Chyba aplikace, obnovte akci');
			}
	    }).send();
    })    
   
    $('ConnectionClientAtCompanyWorkPositionTypSmlouvy').addEvent('click',function(e){
        e.stop();
        if(this.value == 2){
            $('expirace').removeClass('none');
        }
        else{
            $('expirace').addClass('none');
        }
    })    
        
	$('VyhoditClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_zamestnanci_hodiny');});
	$('VyhoditSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
        valid_result = validation.valideForm('formular');


        if( $('calbut_Do').value == '') 
			if(valid_result == true)
				valid_result = Array("Musite zvolit datum ukončení");
			else
				valid_result[valid_result.length]="Musite datum ukončení";
        else{
            if($('calbut_Do').value <= $('OldDatumNastupu').value)
                if(valid_result == true)
				    valid_result = Array("Chyba datum ukončení je menší než staré datum nástupu.\nToto datum musí být min. o jeden den větší než datumu nástupu.\n\nDatum nástupu bylo : "+from_old);
                else
				    valid_result[valid_result.length]="Chyba datum ukončení je menší než staré datum nástupu.\nToto datum musí být min. o jeden den větší než datumu nástupu.\n\nDatum nástupu bylo : "+from_old;   
        }         
                
        if( $('calbut_ConnectionClientAtCompanyWorkPositionDatumNastupu').value == '') 
			if(valid_result == true)
				valid_result = Array("Musite zvolit datum nástupu");
			else
				valid_result[valid_result.length]="Musite zvolit datum nástupu";        
        else{
            if($('calbut_ConnectionClientAtCompanyWorkPositionDatumNastupu').value <= $('calbut_Do').value)
                if(valid_result == true)
				    valid_result = Array("Chyba datum nástupu je menší než datum ukončení.\nToto datum musí být min. o jeden den větší než datumu ukončení.");
                else
				    valid_result[valid_result.length]="Chyba datum nástupu je menší než datum ukončení.\nToto datum musí být min. o jeden den větší než datumu ukončení.";   
        }        


      
        if (valid_result == true){
            button_preloader($('VyhoditSaveAndClose'));
			new Request.JSON({
				url:$('formular').action,		
				onComplete:function(json){
					if (json){
						if (json.result === true){
                            if(json.message)
                                alert(json.message);
                            click_refresh($('OldClientId').value);    
                            domwin.closeWindow('domwin_zamestnanci_hodiny');
						} else {
							alert(json.message);
						}
					} else {
						alert('Chyba aplikace');
					}
                    
                    button_preloader_disable($('VyhoditSaveAndClose'));
				}
			}).post($('formular'));
       } 
       else {
		  var error_message = new MyAlert();
		  error_message.show(valid_result)
	   }      
	});
    
    validation.define('formular',{	
        'ConnectionClientAtCompanyWorkPositionAtCompanyId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit firmu'}
		},
        'ConnectionClientAtCompanyWorkPositionAtProjectCentreWorkPositionId':{
            'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit profesi'}
        }
	});
	validation.generate('formular',false);
</script>	