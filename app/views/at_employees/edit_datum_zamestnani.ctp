<form action='/at_employees/edit_datum_zamestnani/' method='post' id='setting_career_edit_formular'>
	<?php echo $htmlExt->hidden('ConnectionClientAtCompanyWorkPosition/id');?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Zaměstnání</legend>
               	<?php echo $htmlExt->inputDate('ConnectionClientAtCompanyWorkPosition/datum_nastupu',array('tabindex'=>1,'label'=>'Datum nástupu', 'class'=>'','label_class'=>'long'));?> <br class="clear">
			</fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Editovat zaměstnání',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'});
     
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
			new Request.JSON({
				url:$('setting_career_edit_formular').action,		
				onComplete:function(){
					domwin.closeWindow('domwin_zamestnanci_hodiny');
                    click_refresh($('ConnectionClientAtCompanyWorkPositionId').value); 
				}
			}).post($('setting_career_edit_formular'));
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_zamestnanci_hodiny');});
</script>