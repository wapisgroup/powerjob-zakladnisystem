<form action='/companies/order_edit/' method='post' id='company_template_edit_formular'>
	<?php echo $htmlExt->hidden('CompanyOrderItem/id');?>
	<?php echo $htmlExt->hidden('CompanyOrderItem/order_status');?>

		<div class="domtabs field <?php echo $none;?>">
			<fieldset>
				<legend>Kontakty a pracoviště</legend>
				<div class='sll'>
					<?php echo $htmlExt->var_text('CompanyOrderItem/company_id',  array('label'=>'Firma','value'=>$company_info['Company']['name']));?>  <br class='clear' />
				</div>
				<div class='slr'>
					<?php echo $htmlExt->input('CompanyOrderItem/job_city', array('label'=>'Pracoviště město'));?> <br class='clear' />
				</div>
				<br class='clear' />
				<label>Pracoviště popis:</label><br/>
				<?php echo $htmlExt->textarea('CompanyOrderItem/job_description', array('style'=>'width:97%'));?> <br class='clear' />
				<br />
				<div class='sll3'>
					<?php echo $htmlExt->input('CompanyOrderItem/cont1_name', array('label'=>'KOO 1'		,'readonly'=>'readonly','value'=>$company_info['ClientManager']['name']));?>
					<?php echo $htmlExt->input('CompanyOrderItem/cont2_name', array('label'=>'KOO 2'	,'readonly'=>'readonly','value'=>$company_info['Koordinator1']['name']));?>
					<?php echo $htmlExt->input('CompanyOrderItem/cont3_name', array('label'=>'KOO 3'	,'readonly'=>'readonly','value'=>$company_info['Koordinator2']['name']));?>
				</div>
				<div class='sll3'>
					<?php echo $htmlExt->input('CompanyOrderItem/cont1_email', array('label'=>'KOO1 Email'	,'readonly'=>'readonly','value'=>$company_info['ClientManager']['name']));?>
					<?php echo $htmlExt->input('CompanyOrderItem/cont2_email', array('label'=>'KOO2 Email'	,'readonly'=>'readonly','value'=>$company_info['Koordinator1']['name']));?>
					<?php echo $htmlExt->input('CompanyOrderItem/cont3_email', array('label'=>'KOO3 Email'	,'readonly'=>'readonly','value'=>$company_info['Koordinator2']['name']));?>
				</div>
				<div class='sll3'>
					<?php echo $htmlExt->input('CompanyOrderItem/cont1_tel', array('label'=>'KOO1 Tel'	,'readonly'=>'readonly','value'=>$company_info['Koo1']['mobil2']));?>
					<?php echo $htmlExt->input('CompanyOrderItem/cont2_tel', array('label'=>'KOO2 Tel'	,'readonly'=>'readonly','value'=>$company_info['Koo2']['mobil2']));?>
					<?php echo $htmlExt->input('CompanyOrderItem/cont3_tel', array('label'=>'KOO3 Tel'	,'readonly'=>'readonly','value'=>$company_info['Koo3']['mobil2']));?>
				</div>
			</fieldset>
		</div>
		<div class="domtabs field <?php echo $none;?>">
			<fieldset>
				<legend>Parametry pozice</legend>
				<div class='sll'>
					<?php echo $htmlExt->selectTag('CompanyOrderItem/setting_career_item_id', $setting_career_list,null, array('label'=>'Profese pro nábor'));?> <br class='clear' />
				</div>
				<br />
				<?php echo $htmlExt->textarea('CompanyOrderItem/comment', array('label'=>'Poznámka k volnému místu', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />
				<?php echo $htmlExt->textarea('CompanyOrderItem/napln_prace', array('label'=>'Náplň práce', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />
				<div class='sll'>
					<?php echo $htmlExt->input('CompanyOrderItem/education', array('label'=>'Požadované vzdělání'));?> <br class='clear' />
					<?php echo $htmlExt->input('CompanyOrderItem/profession_duration', array('label'=>'Délka praxe'));?> <br class='clear' />
					<?php echo $htmlExt->input('CompanyOrderItem/age_restriction', array('label'=>'Věkové omezení'));?> <br class='clear' />
				</div>
				<div class='slr'>
					<?php echo $htmlExt->input('CompanyOrderItem/certification', array('label'=>'Certifikáty a zkoušky'));?> <br class='clear' />
					<?php echo $htmlExt->selectTag('CompanyOrderItem/sex_list', $vhodne_pro_list,null, array('label'=>'Je vhodné pro'));?> <br class='clear' />
				</div>
				<div class='sll'>
					<?php echo $htmlExt->selectTag('CompanyOrderItem/setting_shift_working_id', $smennost_list,null, array('label'=>'Směnnost'),null,false);?> <br class='clear' />
				</div><br />
					<?php echo $htmlExt->textarea('CompanyOrderItem/komentar_smennosti', array('label'=>'Komentář k směnnosti', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />
				<div class='sll'>
					<?php echo $htmlExt->input('CompanyOrderItem/working_time', array('label'=>'Pracovní doba'));?> <br class='clear' />
				</div>
				<div class='sll'>
					<?php echo $htmlExt->input('CompanyOrderItem/standard_hours', array('label'=>'Normo hodiny'));?> <br class='clear' />	
				</div>
			</fieldset>
		</div>
		<div class="domtabs field <?php echo $none;?>">
			<fieldset>
				<legend>Mzdové podmínky</legend>
				<div class='sll'>
					<?php echo $htmlExt->selectTag('CompanyOrderItem/company_work_position_id', $company_position_list ,null, array('label'=>'Profese dle kalkulace'));?> <br class='clear' />
				</div><br/>
				<label>Komentář ke mzdě:</label>
				<?php echo $htmlExt->textarea('CompanyOrderItem/mzdove_podminky_poznamka', array('style'=>'width:98%'));?> <br />
			</fieldset>
			<fieldset>
				<legend>Kalkulace</legend>
				<div id='kalkulace_list'>
					<?php echo $this->renderElement('../companies/templates/kalkulace');?>
				</div>
			</fieldset>
		</div>
		<div class="domtabs field <?php echo $none;?>">
			<fieldset>
				<legend>Vstupní zkouška</legend>
				<div class='sll'>
					<?php echo $htmlExt->selectTag('CompanyOrderItem/entrance_examination', $ano_ne_list,null, array('label'=>'Vstupní zkouška'));?> <br class='clear' />
				</div>
				<br class='clear'/>
				<?php echo $htmlExt->textarea('CompanyOrderItem/examination_description', array('label'=>'Popis vstupní zkoušky', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />
			</fieldset>
			<fieldset>
				<legend>Doklady potřebné při nástupu</legend>
				<div class='sll'>
					<label>OP:</label>				<?php echo $htmlExt->checkbox('CompanyOrderItem/checkbox_op', array('label'=>'OP'));?> <br class='clear' />
					<label>Zápočtový list:</label>		<?php echo $htmlExt->checkbox('CompanyOrderItem/checkbox_zl', array('label'=>'Zápočtový list'));?> <br class='clear' />
					<label>Kvalifikační doklady:</label>	<?php echo $htmlExt->checkbox('CompanyOrderItem/checkbox_kd', array('label'=>'Kvalifikační doklady'));?> <br class='clear' />
				</div>
				<div class='slr'>
					<label>Výuční list:</label>			<?php echo $htmlExt->checkbox('CompanyOrderItem/checkbox_vl', array('label'=>'Výuční list'));?> <br class='clear' />
					<label>Zdravotní prohlídka:</label>	<?php echo $htmlExt->checkbox('CompanyOrderItem/checkbox_zp', array('label'=>'Zdravotní prohlídka'));?> <br class='clear' />
				</div>
				<br class='clear'/>
				<?php echo $htmlExt->textarea('CompanyOrderItem/jine_doklady', array('label'=>'Co ještě mít sebou k nástupu', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />
			</fieldset>
		</div>
		<div class="domtabs field <?php echo $none;?>">
			<br />
			<script>
				function upload_file_complete(url){
					var ul = $('foto_list');
					var li = new Element('li',{'class':'obal_span'}).inject(ul);
					new Element('img', {src:'/uploaded/requirement/foto/small/' + url,'class':'nahrada_za_foto'}).inject(li);
					
					new Element('input', {type:'hidden', name:'data[CompanyOrderItem][imgs][]', value:url + '|', 'class':'file_caption_value'}).inject(li);
					var submenu = new Element('div', {'class':'submenu'}).inject(new Element('span').inject(li));
						var zoom = new Element('a', {'href':'/uploaded/requirement/foto/large/' + url,'target':'_blank','rel':'clearbox[detail_fotogalerie]'})
							.inject(submenu)
						new Element('img', {'class':'icon',title:'Zvětšit fotografii',src:'/css/fastest/upload/zoom.png'})
							.inject(zoom)
						new Element('img', {'class':'icon',title:'Smazat fotografii',src:'/css/fastest/upload/delete.png'})
							.inject(submenu)
							.addEvent('click', img_delete_button.bind());
						new Element('img', {'class':'icon',title:'Editovat popis fotografie',src:'/css/fastest/upload/edit.png'})
							.inject(submenu)
							.addEvent('click', title_edit_button.bind());
					
					
					document.slideshow.ajax_init($('foto_list'));	
					mysort.addItems(li);
					
					$('CompanyOrderItemFotoUploader').getElement('.input_file_over').value = '';
					$('CompanyOrderItemFotoUploader').getElements('.browse_icon, .input_file').setStyle('display','inline');
					$('CompanyOrderItemFotoUploader').getElement('.smazat_icon').setStyle('display','none');
				}
			</script>
			<div>
				<?php $fileinput_setting = array(
					'label'			=> 'Fotografie',
					'upload_type' 	=> 'php',
					'filename_type'	=> 'unique',
					'file_ext'		=> array('jpg'),
					'paths' 		=> array(
						'path_to_file'	=> 'uploaded/requirement/foto/',
						//'path_to_ftp'	=> 'uploaded/reality/',
						'upload_script'	=> '/companies/requirement_upload_foto/',
						'status_path' 	=> '/get_status.php',
					),
					'methods'=>array(
						array('methods'	=>	array(array(
							'method'		=>	'resize',
							'new_width'		=>	800,
							'new_height'	=>	600
						)),
						'addon_path'=> 'large/'
						),
						array('methods'	=>	array(array(
							'method'		=>	'resize',
							'new_width'		=>	80,
							'new_height'	=>	80
						)),
						'addon_path'=> 'small/'
						)
					),
					'onComplete'	=> 'upload_file_complete'
				);
				
			
				
				?>
				<?php echo $fileInput->render('CompanyOrderItem/foto',$fileinput_setting);?>
			</div>
			<br /> 
			<ul class='foto_list' id='foto_list' style='position:relative;'>
				<?php
					if (isset($this->data['CompanyOrderItem']['imgs']) && !empty($this->data['CompanyOrderItem']['imgs']) && count($this->data['CompanyOrderItem']['imgs']) > 0){
						foreach($this->data['CompanyOrderItem']['imgs'] as $img){
							list($file, $caption) = explode('|',$img);
							echo "<li class='obal_span'>
                                <img src='/uploaded/requirement/foto/small/$file' title='$caption' alt='$caption' class='nahrada_za_foto tip_win'/>
                                
                                <input class='file_caption_value' type='hidden' name='data[CompanyOrderItem][imgs][]' value='$img'/><span>
                                
                                <div class='submenu'><a href='/uploaded/requirement/foto/large/$file' title='$caption' target='_blank' rel='clearbox[detail_fotogalerie]'>
                                <img src='/css/fastest/upload/zoom.png' title='Zvětšit fotografii' class='img_zoom_button icon' /></a>
                                </div></span></li>";
						}
					
					}
				?>
			
			</ul>


		</div>

</form>
<script>        
  


	

	
	var mysort = new Sortables($('foto_list'), {
	  	constrain: false,
	  	clone: true
	});
	
	document.slideshow.ajax_init($('foto_list'));	
	
	
	var Tips_help = new Tips($$('.tip_win'),{
		showDelay: 100,
		hideDelay: 400,
		className: 'tip_win',
		offsets: {'x': -3, 'y': 70},
		fixed: true
	});
    
    /**
     * pokud je nastaven show dej vse disabled
     */
    <?php if(isset($show)): ?>
    	$('company_template_edit_formular').getElements('input, select, textarea')
    		.addClass('read_info')
    		.setProperty('disabled');
	<?php endif; ?>

</script>