   	<div class='sll'>
       <?php echo $htmlExt->var_text('CompanyOrderItem/name', array('label'=>'Požadovaná profese', 'class'=>'read_info', 'disabled'=>'disabled'));?> <br class='clear' /> 
	   <?php echo $htmlExt->var_text('profese',array('label'=>'Typ objednávky','class'=>'read_info','label_class'=>'','value'=>$order_type_list[$detail['CompanyOrderItem']['order_type']]));?> <br class="clear">		
       <?php echo $htmlExt->var_text('CompanyOrderItem/order_date',array('label'=>'Datum objednávky', 'class'=>'read_info','label_class'=>''));?><br />
	</div>
	<div class='slr'>
       <!--
       <div style="line-height: 25px; margin:0px; padding-left:5px; "><a href="#" name="komentare" id="comments">Skryt komentář</a></div>
	   -->
       <?php echo $htmlExt->var_text('CompanyOrderItem/order_status',array('label'=>'Stav objednávky','value'=>(isset($this->data['CompanyOrderItem']) ? $order_stav_list[$this->data['CompanyOrderItem']['order_status']] : 'read_info')));?><br />
	   <?php echo $htmlExt->var_text('CompanyOrderTemplate/name',array('label'=>'Šablona', 'class'=>'read_info ','label_class'=>'' ,'disabled'=>'disabled'));?><br />
	</div>
<br />

	<div class='sll'>
		<?php echo $htmlExt->var_text('CompanyOrderItem/count_of_free_position',array('label'=>'Požadované místa', 'class'=>'read_info integer','label_class'=>''));?><br />
		<?php echo $htmlExt->var_text('CompanyOrderItem/count_of_substitute',array('label'=>'Počet náhradníků', 'class'=>'read_info integer','label_class'=>''));?><br />	
	</div>
	<div class='slr'>
		<?php echo $htmlExt->var_text('CompanyOrderItem/start_datetime',array('label'=>'Nástup (datum/čas)', 'class'=>'read_info','label_class'=>''));?><br />
		<?php echo $htmlExt->var_text('CompanyOrderItem/start_place',array('label'=>'Místo nástupu', 'class'=>'read_info','label_class'=>''));?><br />
	</div>
	<br />
    <div id="comment_order">
	<?php echo $htmlExt->textarea('CompanyOrderItem/start_comment',array('label'=>'Komentář', 'class'=>'read_info long','label_class'=>'long'));?><br />
    </div>
<br />