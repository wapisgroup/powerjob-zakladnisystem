<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang='cs' xml:lang='cs' xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv='Content-language' content='cs' />
	<meta name='description' content='CMS Fastest Admin'/>
	<meta name='keywords' content='CMS'/>
	<meta name="robots" content="all,noindex,nofollow" />
	<meta name='Author' content='Fastest Solution, url: http://www.wapis.cz' />
	<meta name='Copyright' content='Fastest Solution, All Rights Reserved url: http://www.wapis.cz' />
	<link href='/css/fastest/icons/favicon.ico' rel='shortcut icon' />
	<link href='/css/reset.css' rel='stylesheet' type='text/css' media='screen' />
	<link href='/css/fastest/style.css' rel='stylesheet' type='text/css' media='screen' />
	<script type="text/javascript" src="/js/mootools1.2.js"></script>
	
	<title>Administrační systém</title>
</head>
<body>
		<div class="login_box">
			<h2>Administrace - Přihlášení uživatele</h2>
			<form action='/login/' id='login_formular' method='post'>
				<?php echo $htmlExt->input('CmsUser/email',array('label'=>'Email'));?><br class='clear' />
				<?php echo $htmlExt->password('CmsUser/heslo',array('label'=>'Heslo'));?><br class='clear' />
				<div>
				<input type='submit' value='Přihlásit' id='login_now' class='button'/>
			</div>
			</form>
		</div>

<script language="JavaScript" type="text/javascript">
	window.addEvent('load', function(){
		$('CmsUserHeslo').value = '';
		$('CmsUserEmail').value = Cookie.get('username');
	});

	$('login_now').addEvent('click', function(e){
		new Event(e).stop();
		new Request.JSON({
			url: '/login/',
			onComplete: function(json){
				if (json.result == true){
					Cookie.set('username', $('CmsUserEmail').value);
					window.location = '/';
				} else
					alert(json.message);
			}
		}).send($('login_formular'));
	});
</script>
</body>
</html>