<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang='cs' xml:lang='cs' xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv='Content-language' content='cs' />
	<meta name='description' content='CMS Wapis Admin'/>
	<meta name='keywords' content='CMS'/>
	<meta name="robots" content="all,noindex,nofollow" />
	<meta name='Author' content='Wapis Solution, url: http://www.wapis.cz' />
	<meta name='Copyright' content='Wapis Solution, All Rights Reserved url: http://www.wapis.cz' />
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="pragma" content="no-cache"/>
	<link href='' rel='shortcut icon' />
	<link href='/css/reset.css' rel='stylesheet' type='text/css' media='screen' />
	<link href='/css/print.css' rel='stylesheet' type='text/css' media='print' />
	<?php if (isset($_GET['fastest'])): ?>
		<link href='/css/fastest_own/menu.css' rel='stylesheet' type='text/css' media='screen' />
		<link href='/css/fastest_own/style.css' rel='stylesheet' type='text/css' media='screen' />
		<link href='/js/domwin/domwin_own.css' rel='stylesheet' type='text/css' media='screen' />
		<link href='/js/domtabs/domtabs_own.css' rel='stylesheet' type='text/css' media='screen' />
	
	<?php else: ?>
	<link href='/css/fastest/style.css' rel='stylesheet' type='text/css' media='screen' />
	<link href='/css/fastest/menu.css' rel='stylesheet' type='text/css' media='screen' />
	<link href='/js/domwin/domwin.css' rel='stylesheet' type='text/css' media='screen' />
	<link href='/js/domtabs/domtabs.css' rel='stylesheet' type='text/css' media='screen' />
    <link href='/js/uploader/uploader.css' rel='stylesheet' type='text/css' media='screen' />
	
	<?php endif; ?>
	<!--<link href='/js/domtabs/domtabs.css' rel='stylesheet' type='text/css' media='screen' />-->
	<link href='/js/calendar/calendar.css' rel='stylesheet' type='text/css' media='screen' />
	 <script src="http://www.google.com/jsapi?key=ABQIAAAADa4Cav41gA-1OgNf3EBaQxRvSr61w8uliGLWAi-8ZOkwbtesZhSUXGpwXau0YsZf4iI8pxT4cxZYMA" type="text/javascript"></script>
	 <script type="text/javascript">
    google.load("language", "1");
    </script>
	<script type="text/javascript" src="/js/calendar/calendar.js"></script>
	<script type="text/javascript" src="/js/mootools1.2.js"></script>
	<script type="text/javascript" src="/js/mootools.other.js"></script>
	<!--<script type="text/javascript" src="/js/domtabs/domtabs.js"></script>-->
	<script type="text/javascript" src="/js/domtabs/domtabs.js"></script>
	<script type="text/javascript" src="/js/domwin/domwin.js"></script>
	<script type="text/javascript" src="/js/validation/validation.js"></script>
    <script type="text/javascript" src="/js/uploader/uploader.js"></script>
<?php
	if (isset($scripts)){if (is_array($scripts)){foreach ($scripts as $link){echo $javascript->link($link);}} else {echo $javascript->link($scripts);}}
	if (isset($styles)){if (is_array($styles)){foreach ($styles as $style){echo $html->css($style);} } else {echo $html->css($styles);}} 
?>
	<title>Administrační systém</title>
</head>
<body>
<a name="top"></a>
<div class="obal" id="obal">
	<div class="header">
		<strong id="logo"><a href="/" title="Administrační systém"><span></span></a></strong>
		<div id="login_info">
				<? //<a href="#" class="right kos full" title="Koš"></a> ?>
				<?php if (isset($logged_user)):?>
				Právě je přihlášen: <?php echo $logged_user['CmsUser']['name'];?> (<?php echo $logged_user['CmsGroup']['name'];?>)<br class="no" />
				Poslední přihlášení: <?php echo $fastest->czechDateTime($logged_user['CmsUser']['last_log']);?><br class="no" />
				<div style='text-align:right'>

                    <? //<a href="/ftp/usershare/web/madep/uploaded/usershare/?win=true" title="Správce souboru" id="usershare_link">Správce souborů</a> | ?>
				
                <a href="/cms_users/change_password/" title="Změna hesla" id="change_password_link" onclick="return false;">Změna hesla</a> |
				<?php else:?>
				chyba aplikace
				<?php endif;?>
				<a href="/logout/">Odhlásit se</a>
				</div>
		</div>
	</div>
    <div class="drobeckova">
        &nbsp;
    </div>
	<?php echo $this->renderElement('menu');?>
    

	<div class="layout">
		<?php echo $content_for_layout; ?></div>

        <div class="footer">
            <p class="right">
                <a href="/logout/">Odhlásit se</a> |
                <a onclick="print(); return false;" href="#">Tisk stránky</a> |
                <a href="#top">Nahoru</a> |
            </p>
            <p>&copy; 2015 <a href="http://www.wapis.cz/" title="Wapis Group s.r.o.">Wapis Group s.r.o.</a></p>

        </div>
</div>

<div id="addon"></div>
<div id="preloader">Nahrávám data...</div>
<div id='logovani'>
	<var>Move me<span class='cls'>C</span><span class='close'>X</span></var>
	<ul></ul>
</div>
<script>
// START logace
var MakeLog = new Class({
	hide: true,
	initialize: function(){
		this.main = $('logovani');
		this.main.fade('hide');
		this.main.makeResizable();
		this.ul = this.main.getElement('ul');
		this.varEl = this.main.getElement('var');
		this.close = this.varEl.getElement('.close');
		this.cls = this.varEl.getElement('.cls');
		new Drag(this.main,{handle:this.varEl});
		this.close.addEvent('click', (function(){
			this.main.fade(0);
			this.hide = true;
		}).bind(this))
		
		this.cls.addEvent('click', (function(){this.clear()}).bind(this));
	},
	log: function(text){
		if (this.hide == true){
			this.hide = false;
			this.main.fade(0.9);
		}
		new Element('li').inject(this.ul).setHTML(text);
	},
	clear: function(){
		this.ul.empty();
	}
});
var con = new MakeLog();

// END logace

var MonthCal = new Class({
	Implements: [Options, Events],
	month_list :'<table><tr><td class="m_01" title="Leden">1</td><td class="m_02" title="Únor">2</td><td class="m_03" title="Březen">3</td><td class="m_04" title="Duben">4</td></tr><tr><td class="m_05" title="Květen">5</td><td class="m_06" title="Červen">6</td><td class="m_07" title="Červenec">7</td><td class="m_08" title="Srpen">8</td></tr><tr><td class="m_09" title="Září">9</td><td class="m_10" title="Říjen">10</td><td class="m_11" title="Listopad">11</td><td class="m_12" title="Prosinec">12</td></tr></table>',
	onComplete:$empty,
    date: {
		year: '2009',
		month: '01'
	},
	options: {
		element_id: null,
		window: 'addon',
		date: '2009-12-01'
	},
	initialize: function(el,options){
		this.element = el;
        if (options && options.onComplete){ this.onComplete = options.onComplete };
		this.setOptions(options);
		this.element.setProperty('readonly','readonly').addEvent('click', this.show_calenar.bind(this));
	},
	
	parse_date: function(date){
		if (date != ''){
			var dat_ar = date.split("-");
			this.date.year = dat_ar[0]; 
			this.date.month = dat_ar[1]; 
		} else {
			var datum = new Date();
			month = datum.getMonth()+1;
			this.date.month = ((month < 10)?"0":"") + month;		
			this.date.year = datum.getFullYear();
		}
	},
	show_calenar: function(){
		this.parse_date(this.element.value);
		this.el = new Element('div').inject($(this.options.window)).addClass('month_cal');
		this.el.setStyles({
			'z-index':getHighestIndex(),
			position:'absolute',
			top: this.element.getPosition().y,
			left: this.element.getPosition().x
		});
		this.header = new Element('div').inject(this.el).addClass('cal_header');
		this.body = new Element('div').inject(this.el).setHTML(this.month_list).addClass('cal_body');
		
		this.body.getElements('td').addEvent('click', this.apply_date.bindWithEvent(this));
		this.body.getElement('.m_' + this.date.month).addClass('selected');
		// zavreni kalendare
		new Element('div').inject(this.header).addClass('close').setHTML('').addEvent('click', (function(e){
			new Event(e).stop();
			this.el.dispose();
		}).bind(this));
		// obsluha roku
		new Element('div').inject(this.header).addClass('lft').setHTML('').addEvent('click', (function(e){
			new Event(e).stop();
			this.year.setHTML(this.year.getHTML().toInt() - 1);
		}).bind(this));
		new Element('div').inject(this.header).addClass('rght').setHTML('').addEvent('click', (function(e){
			new Event(e).stop();
			this.year.setHTML(this.year.getHTML().toInt() + 1);
		}).bind(this));
		this.year = new Element('div').inject(this.header).addClass('year').setHTML(this.date.year);
	},
	apply_date: function(e){
		var event = new Event(e), td = event.target;
		event.stop();
		date = this.year.getHTML() + '-' + ((td.getHTML().toInt() < 10)?'0'+td.getHTML():td.getHTML()) + '-01';
		
		this.element.value = date; 
		this.el.dispose();
        this.onComplete();	
	}
});
//new MonthCal($('date_test'));


$$('.menu_domwin').addEvent('click', function(e){
	new Event(e).stop();
	domwin.newWindow({
		id			: 'domwin_settings',
		sizes		: [605,450],
		scrollbars	: 'true',
		defined_lang: [<?php echo implode(',',array_map(create_function('$item', 'return  \'"\'.$item.\'"\';'),array_keys($languages))); ?>],
		languages	: true,
		title		: this.title,
		ajax_url	: this.href,
		closeConfirm: false,
		max_minBtn	: false,
		modal_close	: true
	}); 
});

$('change_password_link').addEvent('click', function(e){
	new Event(e).stop();
	domwin.newWindow({
		id			: 'domwin_change_password',
		sizes		: [500,250],
		scrollbars	: 'true',
		languages	: true,
		title		: this.title,
		ajax_url	: this.href,
		closeConfirm: false,
		max_minBtn	: false,
		modal_close	: true
	}); 
});

/*$('usershare_link').addEvent('click', function(e){
	new Event(e).stop();
	domwin.newWindow({
		id			: 'domwin_usershare',
		sizes		: [800,600],
		scrollbars	: 'true',
		languages	: true,
		title		: this.title,
		ajax_url	: this.href,
		closeConfirm: false,
		max_minBtn	: false,
		modal_close	: true
	}); 
});*/

window.addEvents({
	'domready' : function(){
	   new FormHint();
    }
});   

</script>
</body>
</html>