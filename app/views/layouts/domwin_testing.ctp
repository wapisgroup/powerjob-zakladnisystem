<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang='cs' xml:lang='cs' xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<link href='/css/reset.css' rel='stylesheet' type='text/css' media='screen' />
	<link href='/css/fastest/style.css' rel='stylesheet' type='text/css' media='screen' />
	<link href='/css/fastest/menu.css' rel='stylesheet' type='text/css' media='screen' />
	<link href='/js/domwin/domwin.css' rel='stylesheet' type='text/css' media='screen' />
	<link href='/js/domtabs/domtabs.css' rel='stylesheet' type='text/css' media='screen' />
	<link href='/js/calendar/calendar.css' rel='stylesheet' type='text/css' media='screen' />	
	<script type="text/javascript" src="/js/calendar/calendar.js"></script>
	<script type="text/javascript" src="/js/mootools1.2.js"></script>
	<script type="text/javascript" src="/js/mootools.other.js"></script>
	<script type="text/javascript" src="/js/domtabs/domtabs.js"></script>
	<script type="text/javascript" src="/js/domwin/domwin.js"></script>
	<script type="text/javascript" src="/js/validation/validation.js"></script>
	<title>systém - domwin testing layout</title>
</head>
<body>
<a name="top"></a>
<div class="obal" id="obal">
	<div class="layout">
		<div style='margin:10px auto; border:1px solid #61134C; overflow:auto;width:<?php echo $sizes[0];?>px; height:<?php echo $sizes[1];?>px;'><?php echo $content_for_layout; ?></div>
	</div>
</div>
<div id="addon"></div>
<div id="preloader">Nahrávám data...</div>
</body>
</html>