<?php echo $htmlExt->hidden('connection_id'); ?>
<div id="form_list_items">
<table class='table' id="form_list">
    <thead>
    	<tr>
    		<th>Formulář</th>
    		<th>Možnosti</th>
    	</tr>
    </thead>
    <tbody>
        <?php 
    	if (isset($form_list) && count($form_list) > 0){
    		foreach($form_list as $form):?>
    		<tr>
    			<td><?php echo $form['FormTemplate']['name'];?></td>
    			<td>
    			      <?php echo $html->link('Tisknout','/audit_estates_loss_events/print_form/'.$form['FormTemplate']['id'].'/'.$id.'/',array('class'=>'print_client_form','target'=>'_blank'));?> 
    			</td>
    		</tr>
    	<?php 
    		endforeach;
    	}
    	else echo "<tr id='NoneSearch'><td></td><td>Žádne formuláře nenalezeny</td></tr>"; 
    	?>
    </tbody>
</table>  
    
</div>	