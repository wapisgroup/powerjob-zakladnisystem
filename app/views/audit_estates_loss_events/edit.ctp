<form action='/audit_estates_loss_events/edit/' method='post' id='setting_stav_edit_formular'>
	<?php echo $htmlExt->hidden('AuditEstateLossEvent/id');?>
	<?php echo $htmlExt->hidden('AuditEstateLossEvent/audit_estate_id');?>
    <?php echo $htmlExt->hidden('AuditEstateLossEvent/cms_user_id');?>
    <?php echo $htmlExt->hidden('old_send_on_stav');?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Přidání opravy</legend>
               	<?php echo $htmlExt->radio('AuditEstateLossEvent/type', array('1' => 'Majetek ve vlastnictví ATGROUP','2' => 'Majetek ve vlastnictvi jine firmy'), ' ', array('class' => 'radio')); ?><br />
                
                <?php 
                if($this->data['AuditEstateLossEvent']['id'] == ''){
                    echo '<div>'.$htmlExt->input('search',array('rel'=>1,'tabindex'=>1,'label'=>'Vyhledávání majetku','class'=>'long50','label_class'=>'long50','value'=>'Zadejte název majetku pro vyhledávání'));?> <br class="clear">
				<?php echo '</div><div id="ae_results" class="none"></div>';}?> 
                  
               	<?php echo $htmlExt->input('AuditEstateLossEvent/audit_estate_name',array('rel'=>3,'tabindex'=>2,'label'=>'Majetek','class'=>'long50','label_class'=>'long50'));?> <br class="clear">	    
                <?php echo $htmlExt->input('AuditEstateLossEvent/imei_sn_vin',array('rel'=>3,'tabindex'=>3,'label'=>'IMEI/SN/VIN','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
			    <?php echo $htmlExt->input('AuditEstateLossEvent/internal_number',array('rel'=>3,'tabindex'=>4,'label'=>'Interní číslo','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
                <?php echo $htmlExt->input('AuditEstateLossEvent/at_company_name',array('rel'=>3,'tabindex'=>5,'label'=>'V majetku firmy','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
                <?php echo $htmlExt->input('AuditEstateLossEvent/at_project_centre_name',array('rel'=>3,'tabindex'=>6,'label'=>'Středisko','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
                <?php echo $htmlExt->input('AuditEstateLossEvent/name',array('tabindex'=>1,'label'=>'Popis události','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
			
                <?php echo $htmlExt->inputDate('AuditEstateLossEvent/date_event',array('tabindex'=>7,'label'=>'Datum škodové události','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
               	<?php echo '<div>';?>
                <?php echo $htmlExt->selectTag('AuditEstateLossEvent/client_id',$client_list,null,array('rel'=>1,'tabindex'=>8,'label'=>'Nájomca v čase škod. události','class'=>'long50','label_class'=>'long50'),null,false);?> <br class="clear">
                <?php echo '</div><div>';?>
                <?php echo $htmlExt->input('AuditEstateLossEvent/osoba',array('rel'=>2,'tabindex'=>9,'class'=>'long50','label'=>'Nájomca v čase škod. události','label_class'=>'long50'));?> <br class="none">
			   	<?php echo '</div>';?>
                <?php echo $htmlExt->var_text('CmsUser/name',array('label'=>'Škodovou událost vyplnil','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
               
                
                <?php echo $htmlExt->inputDate('AuditEstateLossEvent/date_to_repair',array('tabindex'=>10,'label'=>'Datum odeslání do opravy','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
               	<?php echo $htmlExt->inputDate('AuditEstateLossEvent/date_from_repair',array('tabindex'=>11,'label'=>'Datum přijetí z opravy','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
               	
                <?php echo $htmlExt->selectTag('AuditEstateLossEvent/currency',$currency_list,null,array('tabindex'=>2,'label'=>'Měna','class'=>'long50','label_class'=>'long50'),null,false);?> <br class="clear">
               
                <?php echo $htmlExt->input('AuditEstateLossEvent/expected_price',array('tabindex'=>12,'class'=>'integer long50','label'=>'Předpokládaná cena opravy','label_class'=>'long50'));?> <br class="clear">
			   	<?php echo $htmlExt->input('AuditEstateLossEvent/fa_price',array('tabindex'=>13,'class'=>'integer long50','label'=>'Fakturovaná cena opravy','label_class'=>'long50'));?> <br class="clear">
			   	
                <?php echo $htmlExt->textarea('AuditEstateLossEvent/faults_found',array('tabindex'=>14,'class'=>'long50','label'=>'Zištěné závady','label_class'=>'long50'));?> <br class="clear">
			   	<?php echo $htmlExt->textarea('AuditEstateLossEvent/repair_info',array('tabindex'=>15,'class'=>'long50','label'=>'Oprava - informace','label_class'=>'long50'));?> <br class="clear">
			   	
                <?php echo '<div>';?>
                <?php echo $htmlExt->checkbox('AuditEstateLossEvent/send_on_stav',null,array('disabled'=>(isset($this->data['AuditEstateLossEvent']['send_on_stav'])&&$this->data['AuditEstateLossEvent']['send_on_stav'] == 1?'disabled':false),'rel'=>1,'tabindex'=>16,'class'=>'','label'=>'Poslat do evidence oprav','label_class'=>'long50'));?> <br class="clear">
			    <?php echo '</div>';?>
                <?php echo $htmlExt->checkbox('AuditEstateLossEvent/send_to_maintenance',null,array('label'=>'Odeslat do reportu údržby','tabindex'=>16)) ?><br />
                <?php echo $htmlExt->input('AuditEstateLossEvent/service_center',array('tabindex'=>16,'label'=>'Servisní středisko'));?> <br class="clear">
			    <?php echo $htmlExt->input('AuditEstateLossEvent/stav_km',array('tabindex'=>16,'label'=>'Stav Km'));?> <br class="clear">
			
                <?php echo $htmlExt->selectTag('AuditEstateLossEvent/type_of_event',$list_type_of_loss_events,($this->data['AuditEstateLossEvent']['id'] == ''?3:null),array('tabindex'=>2,'label'=>'Typ škodové události','class'=>'long50','label_class'=>'long50'),null,false);?> <br class="clear">
                <div id="pu_box" class="<?= ($this->data['AuditEstateLossEvent']['type_of_event'] == 1?'':'none');?>">
                    <?php echo $htmlExt->input('AuditEstateLossEvent/number_insurance_event',array('tabindex'=>16,'label'=>'Číslo pojistné události','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
                </div>
                <div id="snp_box" class="<?= (in_array($this->data['AuditEstateLossEvent']['type_of_event'],array(1,2))?'':'none');?>">
                    <?php echo $htmlExt->input('AuditEstateLossEvent/snp_person',array('tabindex'=>17,'label'=>'Osoba které bylo fakturováno','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
                    <?php echo $htmlExt->input('AuditEstateLossEvent/snp_number_invoice',array('tabindex'=>18,'label'=>'Číslo faktury','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
                    <fieldset>
                        <legend>Plnění</legend>
                         <?php echo $htmlExt->input('AuditEstateLossEvent/snp_price',array('tabindex'=>19,'label'=>'Suma','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
                         <?php echo $htmlExt->input('AuditEstateLossEvent/snp_complicity',array('tabindex'=>20,'label'=>'Spoluúčast','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
                         <?php echo $htmlExt->textarea('AuditEstateLossEvent/snp_info',array('tabindex'=>21,'class'=>'long50','label'=>'Plnění informace','label_class'=>'long50'));?> <br class="clear">
			   	
                    </fieldset>
                </div>
                <?php echo $htmlExt->selectTag('AuditEstateLossEvent/snp_stav',$snp_stav_list,null,array('tabindex'=>22,'label'=>'Stav řešení události','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
               
            </fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>8));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>9));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'});
    
    if($('AuditEstateLossEventId').value == ''){//defaultne zapneme moznost jedna
        $('AuditEstateLossEventType_1').setAttribute('checked','checked');
    }
    
    function change_view_for_type(reset){
        if(reset == null){  reset = true; }
        
        var typ_1 = $('AuditEstateLossEventType_1');
        var typ_2 = $('AuditEstateLossEventType_2');
        
        $each($('setting_stav_edit_formular').getElements('select,input,textarea'),function(item){
           if(item.hasAttribute('rel')){
              if(item.getProperty('rel') == 1 && typ_1.checked){
                 item.getParent('div').removeClass('none');
              }
              else if(item.getProperty('rel') == 2 && typ_2.checked){
                 item.getParent('div').removeClass('none');
              }
              else if(item.getProperty('rel') != 3)
                 item.getParent('div').addClass('none');  
                 
              if(item.getProperty('rel') == 3 && typ_1.checked){
                 item.setAttribute('readonly','readonly');
                 item.addClass('read_info');
              }
              else{
                 item.removeProperty('readonly');
                 item.removeClass('read_info'); 
              }        
           }
           
           if(reset != false && item.getProperty('type') != 'button'){
                item.value = ''; 
           }
        })
    }
    
    change_view_for_type(false);//default
    $('AuditEstateLossEventType_1').addEvent('click',change_view_for_type);
    $('AuditEstateLossEventType_2').addEvent('click',change_view_for_type);
    
    
    if($('Search')){
      $('Search').addEvent('click',function(e){this.value = '';});
    
      $('Search').addEvent('keyup',function(e){
         var choices = $('ae_results');
        
         if(this.value.length > 2){
          
            new Request.JSON({
				url:'/audit_estates_loss_events/find_audit_estates/'+this.value,		
				onComplete:function(json){
				    choices.empty();
                    $('AuditEstateLossEventAuditEstateId').value = '';
					if(json){
					   if(json.result == true){
					       choices.removeClass('none');
                           var ul = new Element('ul');
                           ul.inject(choices);
                           var i = 0;
                           json.finds.each(function(f_item){
                               var info = f_item.AuditEstate.name+'|'+f_item.AuditEstate.imei_sn_vin+'|'+f_item.AuditEstate.internal_number+'|'+f_item.AtCompany.name+'|'+f_item.AtProjectCentre.name
                               var li = new Element('li',{id:f_item.AuditEstate.id,rel:info,class:((i%2)==0?"od":"")}).setHTML(f_item.AuditEstate.name+' ('+f_item.AuditEstate.imei_sn_vin+')').inject(ul);
                               li.addEvent('click',set_audit_estate_info.bindWithEvent(this));
                               i++;
                           })
					   }
                       else
                          choices.addClass('none');
					}
				}
			}).send();
         }
      });  
    }
    
    function set_audit_estate_info(e){
        var event = new Event(e); 
		obj = new Event(e).target;
 		event.stop();
        
        audit_estate = obj.getProperty("rel").split("|");
        $('AuditEstateLossEventAuditEstateId').value = obj.id;
        $('AuditEstateLossEventAuditEstateName').value = audit_estate[0];
        $('AuditEstateLossEventImeiSnVin').value = audit_estate[1];
        $('AuditEstateLossEventInternalNumber').value = audit_estate[2];
        $('AuditEstateLossEventAtCompanyName').value = audit_estate[3];
        $('AuditEstateLossEventAtProjectCentreName').value = audit_estate[4];
        $('ae_results').addClass('none');
        $('Search').value = "";
    }
    
    /* 22.8.2011 - vypnuto 
    $('calbut_AuditEstateLossEventDateEvent').addEvent('change',function(e){
        if($('AuditEstateLossEventType_1').checked){
            if($('AuditEstateLossEventAuditEstateId').value != ''){
                new Request.JSON({
				url:'/audit_estates_loss_events/find_audit_estated_connections/'+this.value+'/'+$('AuditEstateLossEventAuditEstateId').value,		
				onComplete:function(json){
				    var cl = $('AuditEstateLossEventClientId');
				    cl.empty();
                    var def = new Element('option',{value:''}).setHTML('Vyberte klienta').inject(cl);
					if(json){
					   if(json.result == true){
                           json.finds.each(function(item){
                               var li = new Element('option',{value:item.Client.id}).setHTML(item.Client.name).inject(cl);
                           })
					   }
                       else
                          choices.addClass('none');
					}
				}
			}).send();
            }
            else{
                alert("Musíte prvni zvolit majetek, aby se mohly vyhledat osoby v daném datumu.")
            }
        }
    })*/
    
    $('AuditEstateLossEventSendOnStav').addEvent('click',function(e){ 
        if(this.checked){
            if($('AuditEstateLossEventFaPrice').value == '' || $('calbut_AuditEstateLossEventDateFromRepair').value == ''){
                e.stop();
                alert('Musíte vyplnit Fakturovanou částku a Datum přijetí z opravy!');
            }
        }    
    })
    
     $('AuditEstateLossEventTypeOfEvent').addEvent('change',function(e){
        if(this.value == 1){
            $('pu_box').removeClass('none');
            $('snp_box').removeClass('none');
            //$each($('snp_box').getElements('select,input,textarea'),function(item){item.value = '';})
        }
        else if(this.value == 2){
            $('pu_box').addClass('none');
            $('snp_box').removeClass('none');
            $each($('pu_box').getElements('select,input,textarea'),function(item){item.value = '';})
        }
        else {
            $('pu_box').addClass('none');
            $('snp_box').addClass('none');
            $each($('snp_box').getElements('select,input,textarea'),function(item){item.value = '';})
            $each($('pu_box').getElements('select,input,textarea'),function(item){item.value = '';})
        }
     });
    
    
    $('setting_stav_edit_formular').getElements('.integer').inputLimit();
     
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('setting_stav_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('setting_stav_edit_formular').action,		
				onComplete:function(){
					click_refresh($('AuditEstateLossEventId').value);
					domwin.closeWindow('domwin');
				}
			}).post($('setting_stav_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('setting_stav_edit_formular',{
	   'AuditEstateLossEventName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte název události'},
		}
	});
	validation.generate('setting_stav_edit_formular',<?php echo (isset($this->data['AuditEstateLossEvent']['id']))?'true':'false';?>);
    
    <?php if(isset($show) && $show == true): ?>
	$('setting_stav_edit_formular').getElements('input, select, textarea')
		.addClass('read_info')
		.setProperty('readonly','readonly');
    $('save_close').addClass('none');
    $('close').removeEvents('click');  
    $('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_show');}); 
    //$('calbut_AuditEstateLossEventDateEvent').removeEvents('change');
    $('AuditEstateLossEventType_1').removeEvent('click',change_view_for_type);
    $('AuditEstateLossEventType_2').removeEvent('click',change_view_for_type);
	<?php endif; ?>
</script>