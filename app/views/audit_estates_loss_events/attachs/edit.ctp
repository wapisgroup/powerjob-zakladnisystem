<script type="text/javascript">
	function upload_file_complete(url){
		alert('Soubor byl nahrán.');
	}
</script>
<form action='/audit_estates_loss_events/attachs_edit/' method='post' id='company_attachs_edit_formular'>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Aktivita</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<?php echo $htmlExt->hidden('AuditEstateLossEventAttachment/id');?>
			<?php echo $htmlExt->hidden('AuditEstateLossEventAttachment/audit_estate_loss_event_id');?>
			<fieldset>
				<legend>Prosím vyplňte následující údaje</legend>
				<?php echo $htmlExt->input('AuditEstateLossEventAttachment/name',array('tabindex'=>1,'label'=>'Název','class'=>'long','label_class'=>'long'));?> <br class="clear">
				<div class='sll'>
					<?php 
						$upload_setting = array(
							'paths' => array(
								'upload_script' => '/audit_estates_loss_events/upload_attach/',
								'path_to_file'	=> '../administrace/uploaded/audit_estates_loss_events/attachment/',
								'status_path'	=> 'get_status.php'
							),
							'upload_type'	=> 'php',
							'onComplete'	=> 'upload_file_complete',
							'label'			=> 'Soubor'
						);
					?>
					<?php echo $fileInput->render('AuditEstateLossEventAttachment/file',$upload_setting);?> <br class="clear">
				</div>
				<div class='slr'>
					<?php echo $htmlExt->selectTag('AuditEstateLossEventAttachment/setting_attachment_type_id',$setting_attachment_type_list,null,array('tabindex'=>4,'label'=>'Typ přílohy'));?> <br class="clear">
				</div>
			</fieldset>
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditAuditEstateLossEventAttachmentSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditAuditEstateLossEventAttachmentClose'/>
	</div>
</form>
<script>
	var domtab = new DomTabs({'className':'admin_dom'}); 
	
	$('AddEditAuditEstateLossEventAttachmentClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_attach_add');});
	
	$('AddEditAuditEstateLossEventAttachmentSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('company_attachs_edit_formular');
		if (valid_result == true){
			new Request.HTML({
				url:$('company_attachs_edit_formular').action,		
				update: $('domwin_attach').getElement('.CB_ImgContainer'),
				onComplete:function(){
					domwin.closeWindow('domwin_attach_add');
				}
			}).post($('company_attachs_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('company_attachs_edit_formular',{
		'AuditEstateLossEventAttachmentName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
		}
	});
	validation.generate('company_attachs_edit_formular',<?php echo (isset($this->data['AuditEstateLossEventAttachment']['id']))?'true':'false';?>);
</script>    