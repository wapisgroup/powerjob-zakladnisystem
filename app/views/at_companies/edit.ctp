<?php
//echo count("123");
?>
<form id='add_edit_company_formular' action='/at_companies/edit/' method='post'>
	<?php echo $htmlExt->hidden('AtCompany/id');?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
            <li class="ousko"><a href="#krok2">Zálohy</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<?php echo $htmlExt->input('AtCompany/name',array('label'=>'Název firmy', 'class'=>'long','label_class'=>'long'));?><br />
			<div class='sll'>	
				<?php echo $htmlExt->selectTag('AtCompany/stat_id',$company_stat_list,null,array('label'=>'Stát'),null,false);?><br />
				<?php echo $htmlExt->selectTag('AtCompany/province_id',$province_list,null,array('label'=>'Kraj'));?><br />
				<?php echo $htmlExt->selectTag('AtCompany/country_id',(isset($country_list)?$country_list:array()),null,array('label'=>'Okres'));?><br />
				
				<?php echo $htmlExt->input('AtCompany/ulice',	array('label'=>'Ulice'));?><br />
				 <?php echo $htmlExt->input('AtCompany/mesto',	array('label'=>'Město'));?><br />
			</div>
			<div class='slr'>
               	
				<?php echo $htmlExt->input('AtCompany/psc',	array('label'=>'PSČ'));?><br />
				<?php echo $htmlExt->input('AtCompany/www',	array('label'=>'WWW'));?><br />	
				<?php echo $htmlExt->input('AtCompany/ico',	array('label'=>'IČO'));?><br />	
				<?php echo $htmlExt->input('AtCompany/dic',	array('label'=>'DIČ'));?><br />
				<?php //echo $htmlExt->selectTag('AtCompany/project_id',$project_list,null,array('label'=>'Projekt'));?><br />
                <?php //echo $htmlExt->selectTag('AtCompany/manager_id',$manager_list,null,array('label'=>'Správce'));?><br />		
			</div>
			<br />
			<?php echo $htmlExt->textarea('AtCompany/address2',	array('class'=>'long','label'=>'Korespondeční adresa:','label_class'=>'long'));?><br />
			<?php echo $htmlExt->textarea('AtCompany/note',	array('class'=>'long','label'=>'Popis','label_class'=>'long'));?><br />
	   		<?php echo $htmlExt->textarea('AtCompany/registration_in',	array('class'=>'long','label'=>'Registrace vedena','label_class'=>'long'));?><br />
	
    	</div>
        <div class="domtabs field">
            <div class="sll">
                <?php echo $htmlExt->checkbox('AtCompany/payment_cash',null,	array('label'=>'výplata v hotovosti'));?><br />
		  	    <?php echo $htmlExt->checkbox('AtCompany/payment_amount',null,	array('label'=>'zálohy'));?><br />
                <?php echo $htmlExt->checkbox('AtCompany/payment_once_in_month',null,	array('label'=>'Záloha jednou měsíčně'));?><br />
            </div>
            <div class="slr">
                 <?php echo $htmlExt->input('AtCompany/payment_cash_limit',	array('label'=>'Max(hotovost)'));?><br />
                 <?php echo $htmlExt->input('AtCompany/payment_amount_limit',	array('label'=>'Max(záloha)'));?><br />
            </div>
            <br />
        </div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditAtCompanySaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditAtCompanyClose'/>
	</div>
</form>
<script>
	
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('AddEditAtCompanyClose').addEvent('click',function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin');
	});
	
	$('AtCompanyStatId').ajaxLoad('/companies/load_province/', ['AtCompanyProvinceId']);
	$('AtCompanyProvinceId').ajaxLoad('/companies/load_country/', ['AtCompanyCountryId']);
	
	$('AddEditAtCompanySaveAndClose').addEvent('click', function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('add_edit_company_formular');
		if (valid_result == true){
			button_preloader($('AddEditAtCompanySaveAndClose'));
			new Request.JSON({
				url:$('add_edit_company_formular').action,		
				onComplete:function(){
					button_preloader_disable($('AddEditAtCompanySaveAndClose'));
					click_refresh($('AtCompanyId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('add_edit_company_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('add_edit_company_formular',{
		'AtCompanyName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit nazev firmy'}
		},
		'AtCompanyIco': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit ičo'},
			'isUnique':{'condition':{'model':'AtCompany','field':'ico'},'err_message':'Toto ičo je již použito'}
		}
	});
	validation.generate('add_edit_company_formular',<?php echo (isset($this->data['AtCompany']['id']))?'true':'false';?>);
	
</script>
