<form action='/setting_stats/edit/' method='post' id='setting_stat_edit_formular'>
	<?php echo $htmlExt->hidden('SettingStat/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Nastavení</legend>
				<?php echo $htmlExt->input('SettingStat/name',array('tabindex'=>1,'label'=>'Název','class'=>'long','label_class'=>'long'));?> <br class="clear">
			</fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('setting_stat_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('setting_stat_edit_formular').action,		
				onComplete:function(){
					click_refresh($('SettingStatId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('setting_stat_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('setting_stat_edit_formular',{
		'SettingStatName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
			'isUnique':{'condition':{'model':'SettingStat','field':'name'},'err_message':'Tento stav je již použit'}
		}
	});
	validation.generate('setting_stat_edit_formular',<?php echo (isset($this->data['SettingStat']['id']))?'true':'false';?>);
</script>