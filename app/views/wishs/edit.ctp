<form id='add_edit_wish_formular' action='/wishs/edit/' method='post'>
	<?php echo $htmlExt->hidden('Wish/id');?>
	<?php echo $htmlExt->hidden('Wish/cms_user',array());?><br />
			
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<?php echo $htmlExt->input('Wish/name',array('label'=>'Název přání', 'class'=>'long','label_class'=>'long'));?><br />
			<br />
				<?php echo $htmlExt->textarea('Wish/text',array('label'=>'Text přání', 'class'=>'long','label_class'=>'long'));?><br />
				<?php echo $htmlExt->textarea('Wish/poznamka',array('label'=>'Poznámka řešitele', 'class'=>'long','label_class'=>'long'));?><br />
		
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditWishSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditWishClose'/>
	</div>
</form>
<script>
	if ($('WishCmsUser').value==''){
		$('WishCmsUser').value='<?php echo $logged_user['CmsUser']['name'];?>';
	}
	
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('AddEditWishClose').addEvent('click',function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin');
	});
	
	
	$('AddEditWishSaveAndClose').addEvent('click', function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('add_edit_wish_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('add_edit_wish_formular').action,		
				onComplete:function(){
					click_refresh($('WishId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('add_edit_wish_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('add_edit_wish_formular',{
		'WishName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název úkolu'}
		}		
	});
	validation.generate('add_edit_wish_formular',<?php echo (isset($this->data['Wish']['id']))?'true':'false';?>);
	
</script>
