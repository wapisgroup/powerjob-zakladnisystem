<?php
	$pocet_dnu_v_mesici = cal_days_in_month(CAL_GREGORIAN, $month, $year);
	$last_day = date('N', mktime(0, 0, 0, $month, 1, $year)); 
	$current_day = $last_day;
	$dny = array( 1=>'Po', 2=>'Út', 3=>'St', 4=>'Čt', 5=>'Pá', 6=>'So', 7=>'Ne' );
?>
	<table class='table odpracovane_hodiny'>	
				<tr>
					<th>Den</th>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<th <?php 
							if (in_array(($i+1),$svatky)) 
								echo "class='svatky'";
							else if (in_array($current_day,array(6,7))) 
								echo "class='vikend'";
						?>>
							<?php echo $dny[$current_day];?>
							<br />
							<?php echo ($i+1);?>
							<?php $current_day = ($current_day == 7)?$current_day=1:$current_day+1;?>
							
						</th>
					<?php endfor;?>
				</tr>
                <tr class='tr_accommodation'>
					<td>Aktivita</td>
					<?php 
                    $current_day = $last_day;
                    for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
					<td><?php 
                        if(!in_array($current_day,array(6,7)))
                            echo (in_array($i+1,$aktivity_list)? 'A' : '1');
                        ?>
                    </td>
            		<?php $current_day = ($current_day == 7)?$current_day=1:$current_day+1;?>
                    <?php endfor;?>
				</tr>
     </table>  
    A - <em>report vytvořený</em><br />
    1 - <em>bez reportu</em><br />
    
    <div class='formular_action'>
		<input type='button' value='Zavřít' id='AddEditDetailClose'/>
	</div>  
<script>
    $('AddEditDetailClose').addEvent('click',function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin_show');
	});
</script>                