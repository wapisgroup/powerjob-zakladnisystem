<form action='/at_projects/money_item_edit/' method='post' id='setting_career_edit_formular'>
	<?php echo $htmlExt->hidden('AtCompanyMoneyItem/id');?>
	<?php echo $htmlExt->hidden('AtCompanyMoneyItem/at_company_id');?>

			<fieldset>
				<legend>Forma zaměstnání</legend>
				<?php echo $htmlExt->input('AtCompanyMoneyItem/name',array('tabindex'=>1,'label'=>'Název', 'class'=>'','label_class'=>'long'));?> <br class="clear">
			</fieldset>
	
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">   
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('setting_career_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('setting_career_edit_formular').action,		
				onComplete:function(){
					domwin.loadContent('domwin'); 
					domwin.closeWindow('domwin_profession');

				}
			}).post($('setting_career_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_profession');});
	validation.define('setting_career_edit_formular',{
		'AtCompanyMoneyItemName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'}
		}
	});
	validation.generate('setting_career_edit_formular',<?php echo (isset($this->data['AtCompanyMoneyItem']['id']))?'true':'false';?>);
</script>