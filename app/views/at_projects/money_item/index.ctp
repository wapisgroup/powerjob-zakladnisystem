<div class="win_fce top">
<a href='/at_projects/money_item_edit/' class='button' id='company_contact_add_new'>Přidat formu zaměstnání</a>
</div>
<table class='table' id='table_money_item'>
	<tr>
		<th>ID</th>
		<th>Název</th>
		<th>Upraveno</th>
		<th>Vytvořeno</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($money_item_list) && count($money_item_list) >0):?>
	<?php foreach($money_item_list as $item):?>
	<tr>
		<td><?php echo $item['AtCompanyMoneyItem']['id'];?></td>
        <td><?php echo $item['AtCompanyMoneyItem']['name'];?></td>
		<td><?php echo $fastest->czechDate($item['AtCompanyMoneyItem']['updated']);?></td>
		<td><?php echo $fastest->czechDate($item['AtCompanyMoneyItem']['created']);?></td>
		<td>
			<a title='Editace položky' 	class='ta edit' href='/at_projects/money_item_edit/<?php echo $item['AtCompanyMoneyItem']['id'];?>'/>edit</a>
			<?php if(in_array($logged_user['CmsGroup']['id'],array(1,5))){?>
            <a title='Odstranit položku'class='ta trash' href='/at_projects/money_item_trash/<?php echo $item['AtCompanyMoneyItem']['id'];?>'/>trash</a>
		    <?php } ?>
        </td>
	</tr>
	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='5'>Nebyly nadefinovány žádné formy zaměstnání.</td>
	</tr>
	<?php endif;?>
</table>
<script>
	$('company_contact_add_new').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_profession',
			sizes		: [500,250],
			scrollbars	: true,
			title		: 'Přidání nová forma zaměstnání',
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
			}); 
	});
	if(	$('table_money_item').getElements('.trash'))
	$('table_money_item').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tuto formu?')){
			new Request.JSON({
				url:this.href,
				onComplete: (function(){this.getParent('tr').dispose();}).bind(this)
			}).send();
		}
	});
	if(	$('table_money_item').getElements('.edit'))
	$('table_money_item').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_profession',
			sizes		: [500,250],
			scrollbars	: true,
			title		: 'Editace formy zaměstnání',
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>