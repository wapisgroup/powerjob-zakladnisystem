<form action='/at_projects/edit/' method='post' id='setting_career_edit_formular'>
	<?php echo $htmlExt->hidden('AtProject/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
            <?php if(isset($project_id) && $project_id != null){?>
            <li class="ousko"><a href="#krok3">Střediska</a></li>
            <?php }?>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Nastavení</legend>
				<?php echo $htmlExt->input('AtProject/name',array('tabindex'=>1,'label'=>'Název', 'class'=>'long','label_class'=>'long'));?> <br class="clear">
			    <?php echo $htmlExt->selectTag('AtProject/manager_id',$manager_list,null,array('label'=>'Project manager'));?><br /> 
            </fieldset>
		</div>
        <div class="domtabs field">
             <?php echo $this->renderElement('../at_projects/centre/index'); ?>
        </div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('setting_career_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('setting_career_edit_formular').action,		
				onComplete:function(){
					click_refresh($('AtProjectId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('setting_career_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('setting_career_edit_formular',{
		'AtProjectName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'}
		}
	});
	validation.generate('setting_career_edit_formular',<?php echo (isset($this->data['AtProject']['id']))?'true':'false';?>);
</script>