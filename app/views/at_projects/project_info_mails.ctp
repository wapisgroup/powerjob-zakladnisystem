<form action='/at_projects/project_info_mails/' method='post' id='setting_career_edit_formular'>
	<?php echo $htmlExt->hidden('AtProject/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Nastavení</legend>
			   <?php echo $htmlExt->selectTag('AtProject/email1_cms_user_id',$users_list,null,array('label'=>'Přijemce č.1'));?><br /> 
               <?php echo $htmlExt->selectTag('AtProject/email2_cms_user_id',$users_list,null,array('label'=>'Přijemce č.2'));?><br /> 
               <?php echo $htmlExt->selectTag('AtProject/email3_cms_user_id',$users_list,null,array('label'=>'Přijemce č.3'));?><br /> 
             
            </fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('setting_career_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('setting_career_edit_formular').action,		
				onComplete:function(){
					click_refresh($('AtProjectId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('setting_career_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('setting_career_edit_formular',{
	});
	validation.generate('setting_career_edit_formular',<?php echo (isset($this->data['AtProject']['id']))?'true':'false';?>);
</script>