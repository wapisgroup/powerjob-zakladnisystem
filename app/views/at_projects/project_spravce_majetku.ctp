<form action='/at_projects/project_spravce_majetku/' method='post' id='setting_career_edit_formular'>
	<?php echo $htmlExt->hidden('AtProject/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Nastavení</legend>
			   <?php echo $htmlExt->selectTag('AtProject/project_spravce_majetku_id',$manager_list,null,array('label'=>'Správce majetku'));?><br /> 
            </fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
			new Request.JSON({
				url:$('setting_career_edit_formular').action,		
				onComplete:function(){
					click_refresh($('AtProjectId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('setting_career_edit_formular'));

	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
</script>