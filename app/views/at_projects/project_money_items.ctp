<form action='/at_projects/project_money_items/' method='post' id='mail_templates_edit_formular'>
	<?php //echo $htmlExt->hidden('AtProjectMoneyItem/id');?>
	<?php echo $htmlExt->hidden('AtProjectMoneyItem/at_project_id');?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Nastavení</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
        <div class="domtabs field">
            <table class="table">
                <thead>
                    <tr>
                        <th>Nastavení</th>
                        <?php foreach($forma_zamestani as $forma){
                            echo '<th>'.$forma.'</th>';
                        }?>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                     foreach($items_setting as $col=>$sett){
                            echo '<tr><td>'.$sett['label'].'</td>';
                                foreach($forma_zamestani as $forma_id=>$forma){
                                    switch($sett['type']){
                                        case 'selectTag':
                                              echo '<td>'.$htmlExt->{$sett['type']}('AtProjectMoneyItem/setting/'.$forma_id.'/'.$col,$sett['select'],null,array(),null,false).'</td>';
                                        break;
                                        case 'input':
                                              if(isset($sett['max'])){
                                                  echo '<td>';
                                                  echo $htmlExt->input('AtProjectMoneyItem/setting/'.$forma_id.'/'.$col,array('class'=>'short float'));
                                                  echo "<label class='label_short'>/</label>";
                                                  echo $htmlExt->input('AtProjectMoneyItem/setting/'.$forma_id.'/'.$col.'_max',array('class'=>'short float'))."<br />";
                                                  echo '</td>';  
                                              }
                                              else{
                                                  echo '<td>'.$htmlExt->{$sett['type']}('AtProjectMoneyItem/setting/'.$forma_id.'/'.$col).'</td>';
                                              }
                                        break;
                                        default:
                                              echo '<td>'.$htmlExt->{$sett['type']}('AtProjectMoneyItem/setting/'.$forma_id.'/'.$col).'</td>';
                                        break;
                                    }
                                }    
                            echo '</tr>';
                     }
                    ?>
                </tbody>
            </table>
        </div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close'));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close'));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'}); 

    $('mail_templates_edit_formular').getElements('.float').inputLimit();

	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
			new Request.JSON({
				url:$('mail_templates_edit_formular').action,		
				onComplete:function(){
					//click_refresh();
					domwin.closeWindow('domwin');
				}
			}).post($('mail_templates_edit_formular'));
	
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
</script>