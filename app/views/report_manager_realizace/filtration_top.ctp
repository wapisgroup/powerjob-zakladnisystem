  <?php if(isset($filtration_sum_variables2)){
    $pagination->ignored_params = $pocet_coo = array();
    $pocet_zam = $sum_vh = 0;
    foreach($filtration_sum_variables2 as $sum_vars){
        
        $pocet_zam += $sum_vars['0']['poc_zamestnancu']; 
        
        $sum_vh += ($sum_vars['0']['company_hv'] == 'Neuvedeno'?0:$sum_vars['0']['company_hv']); 
        
        if($sum_vars['CompanyView']['client_manager_id'] != '' && $sum_vars['CompanyView']['client_manager_id'] != null && !in_array($sum_vars['CompanyView']['client_manager_id'],$pocet_coo))
            $pocet_coo[] = $sum_vars['CompanyView']['client_manager_id'];
        if($sum_vars['CompanyView']['coordinator_id'] != '' && $sum_vars['CompanyView']['coordinator_id'] != null && !in_array($sum_vars['CompanyView']['coordinator_id'],$pocet_coo))
            $pocet_coo[] = $sum_vars['CompanyView']['coordinator_id'];
        if($sum_vars['CompanyView']['coordinator_id2'] != '' && $sum_vars['CompanyView']['coordinator_id2'] != null && !in_array($sum_vars['CompanyView']['coordinator_id2'],$pocet_coo))
            $pocet_coo[] = $sum_vars['CompanyView']['coordinator_id2'];
                    
    }
    
    ?>
    <div id="filtration_variables2" style="width:420px !important;">
        <div class="sll">
            Počet koordinatoru pod MR: <strong><?= count($pocet_coo);?></strong><br />
            Počet podniků pod MR: <strong><?= count($filtration_sum_variables2);?></strong><br />
        </div>
        <div class="slr">
            Celkový počet zaměstnanců pod MR: <strong><?= $pocet_zam;?></strong><br />
            Celkový hodnota VH: <strong><?= $sum_vh;?></strong><br />     
        </div>
    </div>
    <?php }