    <div class="domtabs admin_dom2_links">
		<ul class="zalozky">
            <li class="ousko"><a href="#krok1">#</a></li>
			<li class="ousko  <?php if ($logged_user['CmsGroup']['id'] == 8) echo 'none'; // pokud se jedna o EN?>"><a rel="cm" href="#krok1">Nominační listina - CM</a></li>
			<li class="ousko  <?php if ($logged_user['CmsGroup']['id'] == 8) echo 'none'; // pokud se jedna o EN?>"><a rel="in" href="#krok2">Nominační listina - IN</a></li>
			<!--
            <li class="ousko"><a href="#krok3">Nominační listina - EN</a></li>
			-->
            <li class="ousko"><a href="#krok4">Čekací listina</a></li>
			<li class="ousko <?php if($detail['CompanyOrderItem']['order_status'] == -1) echo 'none';?>"><a href="#krok5">Zaměstnanci</a></li>
        </ul>
        <ul class="zalozky">
    			<li class="ousko"><a href="#krok1">Úvod</a></li>
    			<li class="ousko"><a href="#krok1">Pracoviště</a></li>
    			<li class="ousko"><a href="#krok2">Parametry pozice</a></li>
    			<li class="ousko"><a href="#krok4">Mzdové podmínky</a></li>
    			<li class="ousko"><a href="#krok5">Nástup</a></li>
    			<li class="ousko"><a href="#krok6">Fotografie</a></li>
        </ul>
	</div>
	<div id="domtab-field-box" class="domtabs admin_dom2">
        <div class="domtabs field" ></div>
		<div class="domtabs field krok-cm <?php if ($logged_user['CmsGroup']['id'] == 8) echo 'none'; // pokud se jedna o EN?>" >
            <?php //echo $this->renderElement('../orders_cast_posts/tabs/nominacni_listina2',array('typ'=>'cm','access'=>array(3,4,9),'client_list'=>(in_array($logged_user['CmsGroup']['id'], array(3,4,1)))));?>
        </div>
        <div class="domtabs field krok-in <?php if ($logged_user['CmsGroup']['id'] == 8) echo 'none'; // pokud se jedna o EN?>" >
            <?php //echo $this->renderElement('../orders_cast_posts/tabs/nominacni_listina2',array('typ'=>'in','access'=>array(3,4,9),'client_list'=>($logged_user['CmsGroup']['id'] == 9)));?>
        </div>
        <!--
        <div class="domtabs field" >
            <?php //echo $this->renderElement('../orders_cast_posts/tabs/nominacni_listina2',array('typ'=>'en','access'=>array(3,4,9,8),'client_list'=>($logged_user['CmsGroup']['id'] == 8)));?>
        </div>
        -->
        <div class="domtabs field" >
            <?php echo $this->renderElement('../orders_cast_posts/tabs/cekaci_listina');?>
        </div>
        <div class="domtabs field <?php if($detail['CompanyOrderItem']['order_status'] == -1) echo 'none';?>" >
            <?php  echo $this->renderElement('../orders_cast_posts/tabs/zamestnanci');?>
        </div>
        <div class="domtabs field">
            <?php  echo $this->renderElement('../orders_cast_posts/tabs/uvod');?>
        </div>
        <?php  echo $this->renderElement('../orders_cast_posts/show_template');?>
    </div>
<script language="javascript" type="text/javascript">
	// domtabs init
	var domtab = new DomTabs({'className':'admin_dom2'});
	 <?php if ($logged_user['CmsGroup']['id'] == 8): // pokud se jedna o EN?>
	 domtab.goTo(3);
	 <?php endif;?>
		/*
	 * inicializace presunu klienta z nominacni listiny do cekaci listiny
	 */	
	function move_client_to_cl_table(e){
			e.stop()
			obj = e.target, tr = obj.getParent('tr');
			
			find_id_arr = tr.getElement('.phone').getProperty('href').split('/');
			client_id = find_id_arr[3];
			
            var check_cl = new Request.JSON({
				 url: '/nabor_orders/is_allready_on_cl/' + client_id + '/',
				 onComplete: function(json){
				    if (json){
						if (json.result === true){
							    do_move_client_to_cl_table(e);
						} else {
							    if (confirm('Tento klient se nachází již na čekací listině! Chcete i přes to klienta přidat na čekací listinu?!')){
									do_move_client_to_cl_table(e);
							    }
						}
				    } else {
						alert('Chyba behem komunikace s DB');
				    }
				 }
			})
		
			
			obj = e.target, tr = obj.getParent('tr');
			new Request.JSON({
				 url: '/nabor_orders/is_blacklist/' + client_id + '/',
				 onComplete: function(json){
				    if (json){
						if (json.result === true){
							    check_cl.send();
						} else {
							    if (confirm('Tento klient se nachází na černé listině. Chcete i přesto umístit klineta na čekací listinu?')){
									check_cl.send();
							    }
						}
				    } else {
						alert('Chyba behem komunikace s DB');
				    }
				 }
			}).send();	
	}
	
	function do_move_client_to_cl_table(e){
	    e.stop();
	    obj = e.target, tr = obj.getParent('tr');
		new Request.JSON({
			url:obj.get('href'),
            data : {
			     'data[objednavka]':$('CompanyOrderItemName').getHTML(),
			     'data[firma]':$('Firma').value
            },
			onComplete: (function(json){
				if (json) {
				   	if (json.result === true) {
						// presun tr-ka do taublky cekaci listina 
                        new Element ('td',{html:json.data.Client.mobil}).inject(tr.getElements('td')[0],'after');
                        new Element ('td',{html:json.data.Client.mesto}).inject(tr.getElements('td')[1],'after');
                        new Element ('td',{html:json.data.Client.okres}).inject(tr.getElements('td')[2],'after');
                        new Element ('td',{html:json.data.Client.opp}).inject(tr.getElements('td')[3],'after');
                        new Element ('td',{html:json.data.Client.os_pohovor}).inject(tr.getElements('td')[4],'after');
                       // tr.getElements('td')[2].setHTML(json.data.Client.opp); 
						$('table_cekaci_listina').adopt(tr);
						var td_posibility = tr.getElement('td:last-child');
						// nove moznosti
						
						tdp_html = '<a onclick="return false;" class="ta delete remove_client_from_cl" href="/orders_cast_posts/remove_from_cl/' +json.data.ConnectionClientRequirement.id+ '">Odtranit</a>';
						/*
						 * pokud jde o predbeznou objednavku, nepridavej moznost prevest na zamestnance
						 */
						if ($('OrderStav').value != -1) {
							tdp_html += ' <a title="Zaměstnat" onclick="return false;" class="ta nabor_pozice move_zam" href="/orders_cast_posts/move_to_zam_domwin/' + json.data.ConnectionClientRequirement.id + '">Zamestnant</a>';
						}
                        tdp_html += ' <a href="/clients/edit/' + json.data.ConnectionClientRequirement.client_id + '/domwin/only_show" class="ta client_info" title="Informace o klientovi">Info</a>';
                        tdp_html += ' <a href="/clients/add_activity/' + json.data.ConnectionClientRequirement.client_id + '/1/'+json.data.ConnectionClientRequirement.id+'" class="ta phone"  title="Přidání aktivity">Add Activity</a>';
	                   
						td_posibility.setHTML(tdp_html);
						
						if(td_posibility.getElement('.move_zam')) td_posibility.getElement('.move_zam').addEvent('click', zamestnat_klient_domwin.bindWithEvent(this));
						if(td_posibility.getElement('.remove_client_from_cl')) td_posibility.getElement('.remove_client_from_cl').addEvent('click',odebrani_klient_z_cekaci_listina_domwin.bindWithEvent(this));
						if(td_posibility.getElement('.client_info')) td_posibility.getElement('.client_info').addEvent('click',call_domwin_client_info.bindWithEvent(this));
						if(td_posibility.getElement('.phone')) td_posibility.getElement('.phone').addEvent('click',call_domwin_add_activity.bindWithEvent(this));
						
						domtab.goTo(3);
				   	}
				   	else {
				   		alert(json.message);
				   	}
				} else {
					alert('Chyba aplikace');
				}
			}).bind(this)
		}).send();
	}
	
	/*
	 * Odstraneni klienta z nominacni listiny
	 */
    function remove_client_from_listina_table(e,tbody){
    	e.stop();
		var obj = e.target; 
		var tbody = $(tbody);
		new Request.JSON({
			url:obj.get('href'),
			data : {
			     'data[objednavka]':$('CompanyOrderItemName').getHTML(),
			     'data[firma]':$('Firma').value
            },
			onComplete: (function(json){
				if (json) {
				   	if (json.result === true) {
						temp_tr = $('client_list_item_'+json.data.ConnectionClientRequirement.client_id);
						if (temp_tr)
							temp_tr.removeClass('none');
						delete temp_tr;						
						obj.getParent('tr').dispose();
				   	}
				   	else {
				   		alert(json.message);
				   	}
				} else {
					alert('Chyba aplikace');
				}
			}).bind(this)
		}).send();
	}
    	/*
	 * Inicializace funkce pro zobrazeni inforamce klienta pres domwin
	 */
	$$('.client_info').addEvent('click', call_domwin_client_info.bindWithEvent(this));
	$$('.phone').addEvent('click', call_domwin_add_activity.bindWithEvent(this));
	
    /*
	 * Funkce pro zobrazeni domwin a informaci o klientovi,
	 * zobrazi se karta klienta
	 */
	function call_domwin_client_info (e){
		var event = new Event(e); 
		obj = new Event(e).target;
 		event.stop();
		domwin.newWindow({
			id			: 'domwin',
			sizes		: [1000,1000],
			scrollbars	: true,
			title		: 'Informace o klientovi',
			languages	: false,
			type		: 'AJAX',
			ajax_url	: obj.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	};
    
    /*
	 * Funkce pro zobrazeni domwin pro pridani activity ke klientovi
	 */
	function call_domwin_add_activity (e){
		var event = new Event(e); 
		obj = new Event(e).target;
 		event.stop();
		domwin.newWindow({
			id			: 'domwin_message_add',
			sizes		: [580,390],
			scrollbars	: true,
			title		: obj.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: obj.href,
			post_data	: {from:'objednavky'},
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	};
    
    
    //nacteni nominacni listin az kdyz klikneme na zalozku
    $('nabor_items').getElements('.ousko').getElement('a').addEvent('click',function(e){
        var rel = this.getProperty('rel');
        if(rel){
            var div_field = $('domtab-field-box').getElement('div.krok-'+rel);
            if(div_field.hasClass('loaded') == false){
                div_field.setHTML('Nahrávám...');
                div_field.addClass('loaded');    
                new Request.HTML({
        			url:'/orders_cast_posts/ajax_load_nl/<?php echo $detail['CompanyOrderItem']['company_id'];?>/<?php echo $order_template_id;?>/'+rel+'/<?php echo $kvalifikace_id;?>/',
                    update:div_field,		
        			onComplete:function(){}
        	    }).send();
            }
        }
    })
</script>