 <form action='/report_fuel_consumptions/edit/' method='post' id='ReportFuelConsumption_edit_formular'>
	<?php echo $htmlExt->hidden('ReportFuelConsumption/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Základní údaje</legend>
				<?php echo $htmlExt->input('ReportFuelConsumption/name',array('tabindex'=>1,'label'=>'Název','class'=>'long','label_class'=>'long'));?> <br class="clear">
				<?php echo $htmlExt->selectTag('ReportFuelConsumption/at_project_id',$project_list,null,array('tabindex'=>2,'label'=>'Projekt'));?> <br class="clear">
				<?php echo $htmlExt->selectTag('ReportFuelConsumption/at_project_centre_id',$centre_list,null,array('tabindex'=>3,'label'=>'Středisko'));?> <br class="clear">
                <?php echo $htmlExt->textarea('ReportFuelConsumption/description',array('tabindex'=>5,'label'=>'Poznámka'));?> <br class="clear">
				
			</fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','class'=>'button'));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','class'=>'button'));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'});
    $('ReportFuelConsumptionAtProjectId').ajaxLoad('/int_clients/ajax_load_at_company_centre_list/',['ReportFuelConsumptionAtProjectCentreId']);
    
		
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('ReportFuelConsumption_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('ReportFuelConsumption_edit_formular').action,		
				onComplete:function(){
					click_refresh($('ReportFuelConsumptionId').value);
					domwin.closeWindow('domwin');
				}
			}).post($('ReportFuelConsumption_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('ReportFuelConsumption_edit_formular',{
		'ReportFuelConsumptionName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
		},
        'ReportFuelConsumptionAtProjectId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit projekt'},
		},
        'ReportFuelConsumptionAtProjectCentreId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit středisko'},
		}
	});
	validation.generate('ReportFuelConsumption_edit_formular',<?php echo (isset($this->data['ReportFuelConsumption']['id']))?'true':'false';?>);
</script>