<div class="win_fce top">
<?php if(isset($permission['upload_file']) && $permission['upload_file'] == true){ ?>
<a href='/report_fuel_consumptions/attachs_edit/<?php echo $report_fuel_consumption_id;?>' class='button edit' id='Accommodation_attach_add_new' title='Přidání nové přílohy'>Přidat přílohu</a>
<?php } ?>
</div>
<table class='table' id='table_attachs'>
	<tr>
		<th>#ID</th>
		<th>Název</th>
		<th>Typ</th>
		<th>Vytvořeno</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($attachment_list) && count($attachment_list) >0):?>
	<?php foreach($attachment_list as $attachment_item):?>
	<tr>
		<td><?php echo $attachment_item['ReportFuelConsumptionAttachment']['id'];?></td>
		<td><?php echo $attachment_item['ReportFuelConsumptionAttachment']['name'];?></td>
		<td><?php echo $attachment_item['SettingAttachmentType']['name'];?></td>
		<td><?php echo $fastest->czechDateTime($attachment_item['ReportFuelConsumptionAttachment']['created']);?></td>
		<td>
			<a title='Editace přílohy "<?php echo $attachment_item['ReportFuelConsumptionAttachment']['name'];?>"' 	class='ta edit' href='/report_fuel_consumptions/attachs_edit/<?php echo $report_fuel_consumption_id;?>/<?php echo $attachment_item['ReportFuelConsumptionAttachment']['id'];?>'/>edit</a>
			<?php if(isset($permission['delete_file']) && $permission['delete_file'] == true){ ?>
            <a title='Odstranit přílohu'class='ta trash' href='/report_fuel_consumptions/attachs_trash/<?php echo $report_fuel_consumption_id;?>/<?php echo $attachment_item['ReportFuelConsumptionAttachment']['id'];?>'/>trash</a>
            <?php } ?>
            <?php if(isset($permission['download_file']) && $permission['download_file'] == true){ ?>
            <a title='Stáhnout přílohu'class='ta download' href='/report_fuel_consumptions/attachs_download/report_fuel_consumptions|attachment|<?php echo $attachment_item['ReportFuelConsumptionAttachment']['file'];?>/<?php echo $attachment_item['ReportFuelConsumptionAttachment']['alias_'];?>/'/>download</a>
            <?php } ?>		  
        </td>
	</tr>
	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='5'>K této položce nebyla nadefinována příloha</td>
	</tr>
	<?php endif;?>
</table>
<?php // pr($attachment_list);?>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='ListReportFuelConsumptionAttachmentClose' class='button'/>
	</div>
<script>
	$('ListReportFuelConsumptionAttachmentClose').addEvent('click', function(e){new Event(e).stop(); domwin.closeWindow('domwin_attach');});

	$('table_attachs').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tuto přílohu?')){
			new Request.HTML({
				url:this.href,
				update: $('domwin_attach').getElement('.CB_ImgContainer'),
				onComplete: function(){}
			}).send();
		}
	});
	
	$('domwin_attach').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_attach_add',
			sizes		: [580,390],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>