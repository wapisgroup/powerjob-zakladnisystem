<div>
	<?php echo $htmlExt->hidden('company_id',array('value' => $detail["RequirementsForRecruitment"]["id"]));?>
	<div class="sll" >  
		<?php echo $htmlExt->selectTag('year2', $actual_years_list,Date ('Y'),array('label'=>'Rok'),null,true);?>
	</div>
	<div class="slr" >  
		<?php echo $htmlExt->selectTag('month2',$mesice_list,Date ('n'),array('label'=>'Měsíc'));?> <br/>
	</div>
</div>
<br />

<div id="odpracovane_hodiny_element">
	<?php echo $this->renderElement('../report_requirements_for_recruitments/odpracovane_hodiny_element',array($mesic,$rok));?>
</div>




<script language="JavaScript" type="text/javascript">  
/* pokud je aktualni rok disable options */
	if  ($('Year2').value==<?php echo Date ('Y'); ?>){
		options = $('Month2').getElements("option");
		<?php foreach ($mesice_list AS $key => $mnth){ ?>
			<?php if ($key > Date ('n')){ ?>
				options[<?php echo $key ?>].setProperty("disabled","disabled");
			<?php } ?>
		<?php } ?>
	}

	
	$('Year2').addEvent('change', function(){
		if( $('Month2').value!=""){
	  	
			/* pokud je aktualni rok disable options */
			if  ($('Year2').value==<?php echo Date ('Y'); ?>){
				options = $('Month2').getElements("option");
				<?php foreach ($mesice_list AS $key => $mnth){ ?>
					<?php if ($key > Date ('n')){ ?>
						options[<?php echo $key ?>].setProperty("disabled","disabled");
					<?php } ?>
				<?php } ?>
			} else {
				<?php foreach ($mesice_list AS $key => $mnth){ ?>
						options[<?php echo $key ?>].removeProperty("disabled","disabled");
				<?php } ?>
			}
		
			new Request.HTML({
		  		url: '/report_requirements_for_recruitments/odpracovane_hodiny/' + <?php echo $connection_client_requirement_id; ?> + '/' + $('Year2').value +'/' + $('Month2').value + '/1',
		  		update:'odpracovane_hodiny_element'
		  	}).send();
		}
	});
	
	
 	$('Month2').addEvent('change', function(){
		if( $('Year2').value!=""){
		  	new Request.HTML({
		  		url: '/report_requirements_for_recruitments/odpracovane_hodiny/' + <?php echo $connection_client_requirement_id ;?> + '/' + $('Year2').value +'/' + $('Month2').value + '/1',
		  		update:'odpracovane_hodiny_element'
		  	}).send();
		}
	});
	
	 
	
</script>