<?php
	$month_list = $mesice_list;
	
	if (substr($mesic,0,1)==0)	$mesic = substr($mesic,1);
	
	
	$thismonth = $mesic ;
    $thisyear = $rok ;
    
	$numdaysinmonth = cal_days_in_month( CAL_GREGORIAN, $thismonth, $thisyear );
    $jd = cal_to_jd( CAL_GREGORIAN, $thismonth,date( 1 ), $thisyear);
    $startday = (jddayofweek( $jd , 0 )-1 <0)?6:jddayofweek( $jd , 0 )-1;
    $monthname = $month_list[$thismonth];
?>

    <table class="calandar_big" id="calendar_ubytovani">
        <tr>
            <td colspan="7" class="cal_name"><div align="center"><strong><?php echo $monthname.' '.$thisyear ?></strong></div></td>
        </tr>
        <tr class="cal_head">
            <td><strong>Po</strong></td>
            <td><strong>Út</strong></td>
            <td><strong>St</strong></td>
            <td><strong>Čt</strong></td>
            <td><strong>Pá</strong></td>
            <td><strong>So</strong></td>
			<td><strong>Ne</strong></td>
        </tr>
        <tr>
		    <?php
			$emptycells = 0;
			for( $counter = 0; $counter <  $startday; $counter ++ ) {
				echo "\t\t<td ".((in_array($emptycells,array(5,6)))?'class="vikend"':'').">-</td>\n";
				$emptycells ++;
			}
			
			$rowcounter = $emptycells;
			$numinrow = 7;
			for( $counter = 1; $counter <= $numdaysinmonth; $counter ++ ) {
				
				$rowcounter ++;
				echo "\t\t<td ".((in_array($rowcounter,array(6,7)))?'class="vikend"':'').">".$htmlExt->checkbox('SettingStatSvatek/day/'.$counter,null,array('class'=>'cal_day'))."$counter</td>\n";
				if( $rowcounter % $numinrow == 0 ) {
					echo "\t</tr>\n";
					if( $counter < $numdaysinmonth ) {
						echo "\t<tr>\n";
					}
					$rowcounter = 0;
				}
			}
			$numcellsleft = $numinrow - $rowcounter;
			if( $numcellsleft != $numinrow ) {
				for( $counter = 0; $counter < $numcellsleft; $counter ++ ) {
					echo "\t\t<td ".((in_array($counter+$rowcounter,array(5,6)))?'class="vikend"':'').">-</td>\n";
					$emptycells ++;
				}
			}
		?>
	</tr>
</table>