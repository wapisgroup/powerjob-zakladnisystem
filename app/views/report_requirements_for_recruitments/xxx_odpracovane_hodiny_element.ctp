<?php if (empty($detail['ClientWorkingHour']['id'])){?>
		<div class="domtabs admin_dom_links">
			<ul class="zalozky">
				<li class="ousko"><a href="#krok1">Základní informace</a></li>
			</ul>
		</div>
		<div class="domtabs admin_dom">
			<div class="domtabs field" >
				<p>V tomto měsíci klient nepracoval</p>
			</div>
		</div>
		<script language="JavaScript" type="text/javascript">  
			var domtab = new DomTabs({'className':'admin_dom'}); 
		</script>
<?php	} else { ?>


<?php 
  //nastaveni promennych z get
	$month = $mesic;
	$year = $rok;
	//end nastaveni z get
	$pocet_dnu_v_mesici = cal_days_in_month(CAL_GREGORIAN, $month, $year);
	$last_day = date('N', mktime(0, 0, 0, $month, 1, $year)); 
	$current_day = $last_day;
	$dny = array(
		1=>'Po',
		2=>'Ut',
		3=>'St',
		4=>'Ct',
		5=>'Pa',
		6=>'So',
		7=>'Ne'
	);
	$svatky = array(5,6);
?>
<form action='/report_requirements_for_recruitments/odpracovane_hodiny/' method='post' id='ClientWorkingHour_edit_formular'>
	<?php echo $htmlExt->hidden('ClientWorkingHour/id');?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/company_id',array('value'=>$detail['ConnectionClientRequirement']['company_id']));?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/client_id',array('value'=>$detail['ConnectionClientRequirement']['client_id']));?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/company_money_item_id',array('value'=>$detail['ConnectionClientRequirement']['company_money_item_id']));?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/requirements_for_recruitment_id',array('value'=>$detail['ConnectionClientRequirement']['requirements_for_recruitment_id']));?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/connection_client_requirement_id',array('value'=>$detail['ConnectionClientRequirement']['id']));?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/year',array('value'=>$year));?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/month',array('value'=>$month));?>
<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field" >
			<div class='sll'>
				<?php echo $htmlExt->selectTag('RequirementsForRecruitment/setting_shift_working_id', $smennost_list,null, array('label'=>'Směnnost'),null,false);?> <br /> 
					
				<?php echo $htmlExt->input('ClientWorkingHour/datum_nastupu',array('value'=>$detail['ConnectionClientRequirement']['from'],'readonly'=>'readonly','label'=>'Datum nastupu'));?>
				<?php echo $htmlExt->input('ClientWorkingHour/svatky',array('readonly'=>'readonly','label'=>'Svátky'));?>
				<?php echo $htmlExt->input('ClientWorkingHour/pn',array('readonly'=>'readonly','label'=>'PN'));?>
				<?php echo $htmlExt->input('ClientWorkingHour/prescasy',array('readonly'=>'readonly','label'=>'Přesčasy'));?>
			</div>
			<div class='slr'>
				<?php echo $htmlExt->input('ClientWorkingHour/norma_hod',array('value'=>$detail['RequirementsForRecruitment']['standard_hours'],'readonly'=>'readonly','label'=>'Norma hod.'));?>
				<?php echo $htmlExt->input('ClientWorkingHour/vikendy',array('readonly'=>'readonly','label'=>'Víkendy'));?>
				<?php echo $htmlExt->input('ClientWorkingHour/dovolenka',array('readonly'=>'readonly','label'=>'Dovolenka'));?>
				<?php echo $htmlExt->input('ClientWorkingHour/celkem_hodin',array('readonly'=>'readonly','label'=>'Celkem hodin'));?>
			</div>
			<br /><br />
			<table class='table odpracovane_hodiny'>
				
				<tr>
					<th>Den</th>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<th <?php 
							if (in_array(($i+1),$svatky)) 
								echo "class='svatky'";
							else if (in_array($current_day,array(6,7))) 
								echo "class='vikend'";
						?>>
							<?php echo $dny[$current_day];?>
							<br />
							<?php echo ($i+1);?>
							<?php $current_day = ($current_day == 7)?$current_day=1:$current_day+1;?>
							
						</th>
					<?php endfor;?>
				</tr>
				
				<?php for($k = 1; $k <= $detail['RequirementsForRecruitment']['setting_shift_working_id']; $k++):?>
				<tr>	
					<td>Směna<?php echo $k;?></td>
					<?php $current_day = $last_day;?>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<td <?php 
							if (in_array(($i+1),$svatky)) 
								echo "class='svatky'";
							else if (in_array($current_day,array(6,7))) 
								echo "class='vikend'";
						?>>
						<?php 
							if (in_array(($i+1),$svatky)){ 
								$class =  " svatky";
							} else if (in_array($current_day,array(6,7))){
								$class = " vikend";
							} else {
								$class = "";
							}
						?>
						<?php echo $htmlExt->input('ClientWorkingHour/days/'. '_' .$k. '_' .($i+1),array('class'=>'hodiny '.$class));?></td>
						<?php $current_day = ($current_day == 7)?$current_day=1:$current_day+1;?>
					<?php endfor;?>
				</tr>
				<?php endfor;?>
			</table>
			<br />
			<?php echo $this->renderElement('../report_requirements_for_recruitments/odpracovane_hodiny_ubytovani');?>
			<br />
			<?php echo $htmlExt->selectTag('ClientWorkingHour/company_work_position_id',$company_work_position_list,null,array('label_class'=>'long2','class'=>'long2','tabindex'=>2,'label'=>'Pozice'));?> <br class="clear">
		</div>
	</div>
	
	<div id='hodiny_od_do_form' class='none'>
	<label style='width:13%'>Typ:</label>
	<select class='add_handly_type'>
		<option value='1'>Hodiny</option>
		<option value='2'>PN</option>
		<option value='3'>D</option>
	</select>
	<div class='enter_od_do'>
		<div class='sll'>
			<label style='width:13%'>Od:</label><input type='text' value='' class='enter_od' maxlength='5'/><br/>
		</div>
		<div class='slr'>
			<label style='width:13%'>Do:</label><input type='text' value='' class='enter_do' maxlength='5'/><br/>
		</div>
		<br />	
	</div>
	<div class='text-center'>
		<input type='button' value='Vložit' class='enter_od_do_now'/>
	</div>
</div>

	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close_zamestnanci_hodiny','class'=>'button'));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close_zamestnanci_hodiny','class'=>'button'));?>
	</div>
</form>
 
<script language="javascript" type="text/javascript">

	

	var domtab = new DomTabs({'className':'admin_dom'}); 
	
	$('RequirementsForRecruitmentSettingShiftWorkingId').addEvent('change', function(e){
		new Event(e).stop();
			if (confirm('Opravdu chcete změnit směnnost? Uložené hodnoty u směn budou ztraceny.')){
				alert('zmena table');
			}
	});
	
	
	 
	
	  
	$$('.hodiny').addEvent('dblclick', function(e){
		var object = this;
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_hodiny_od_do',
			sizes		: [300,150],
			scrollbars	: true,
			title		: 'Vložte čas práce',
			languages	: false,
			type		: 'DOM',
			dom_id		: $('hodiny_od_do_form'),
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false,
			dom_onRender: function(){
				var selectTag = $('domwin_hodiny_od_do').getElement('.add_handly_type');
				var button = $('domwin_hodiny_od_do').getElement('.enter_od_do_now');
				
				selectTag.addEvent('change',function(){
					if (this.value == 1){
						$('domwin_hodiny_od_do').getElement('.enter_od_do').removeClass('none');
					} else {
						$('domwin_hodiny_od_do').getElement('.enter_od_do').addClass('none');
					}
				});
				
				button.addEvent('click',function(e){
					new Event(e).stop();
					switch(selectTag.value){
						case '1':
							var Eod = $('domwin_hodiny_od_do').getElement('.enter_od').value;
							var Edo = $('domwin_hodiny_od_do').getElement('.enter_do').value;
							Eod = str_replace(':','.',Eod).toFloat();
							Edo = str_replace(':','.',Edo).toFloat();
							if (Edo < Eod){
								val = 24 - Eod + Edo;
							} else {
								val = Edo - Eod;
							}
							
							test = val.toString().split('.');
							if (test[1].toInt() > 60){
								hodin = test[0].toInt() + 1;
								minut = 60 - test[1].toInt();
								val = hodin + '.' + minut;
							}
							object.value = val;
							break;
						case '2':
							object.value = 'PN';
							break;
						case '3':
							object.value = 'D';
							break;
						default:
							alert('kokot');
					}
					domwin.closeWindow('domwin_hodiny_od_do')
				});
			}
		}); 
	});
	
	$$('.hodiny').addEvent('change', function(e){
		var count = 0;
		var vikend = 0;
		var NH = $('ClientWorkingHourNormaHod').value.toFloat();
		var prescas = 0;
		var dni_dovolene = 0
		var dni_pn = 0
		var dni_svatky = 0
		
		$$('.hodiny').each(function(item){
			if (item.value == 'PN') {
				dni_pn ++;
			} else if (item.value == 'D') {
				dni_dovolene ++;
			} else {
				if (item.value == '') item.value = 0;
				
				// celkem
				count += item.value.toFloat();
				
				// vikend
				if (item.hasClass('vikend'))
					vikend += item.value.toFloat();
				// svatky
				if (item.hasClass('svatky'))
					dni_svatky += item.value.toFloat();
				// prescas
				if (item.value.toFloat() > NH)
					prescas += (item.value.toFloat() - NH);
				
			}
		});
		$('ClientWorkingHourCelkemHodin').value = count;
		$('ClientWorkingHourVikendy').value = vikend;
		$('ClientWorkingHourPrescasy').value = prescas;
		$('ClientWorkingHourDovolenka').value = dni_dovolene;
		$('ClientWorkingHourPn').value = dni_pn;
		$('ClientWorkingHourSvatky').value = dni_svatky;
	});
	
		
	$('save_close_zamestnanci_hodiny').addEvent('click',function(e){
		new Event(e).stop();
		//valid_result = validation.valideForm('ClientWorkingHour_edit_formular');
		//if (valid_result == true){
			new Request.HTML({
				url:$('ClientWorkingHour_edit_formular').action,	
				update: $('domwin_zamestnanci').getElement('.CB_ImgContainer'),
				onComplete:function(){
					//alert('ulozeno');
					//click_refresh($('AccommodationId').value);
					domwin.closeWindow('domwin_zamestnanci_hodiny');
				}
			}).post($('ClientWorkingHour_edit_formular'));
		//} else {
		//	var error_message = new MyAlert();
		//	error_message.show(valid_result)
		//}
	});
	
	$('close_zamestnanci_hodiny').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_zamestnanci_hodiny');});
	
	
	
	
  $('ClientWorkingHourAccommodationId').addEvent('change', function(){
	if( $('ClientWorkingHourAccommodationId').value!=""){
  	new Request.HTML({
  		url: '/report_requirements_for_recruitments/ubytovani/' + $('ClientWorkingHourAccommodationId').value + '/' + <?php echo $rok ?> + '/' + <?php echo $mesic ?>,
  		update:'ubytovani'
  	}).send();
	}
 });


 
 
</script>
<?php } ?>