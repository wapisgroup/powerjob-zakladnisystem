<form action='/report_requirements_for_recruitments/add_zam_dotaznik/<?php echo $client_id;?>/<?php echo $company_id;?>/<?php echo $requirement_id;?>/<?php echo $connection_id;?>' method='post' id='formular'>

      <fieldset>
				<legend>Komentář</legend>
					<?php echo $htmlExt->textarea('text',array('tabindex'=>1,'label'=>'Text','class'=>'long','label_class'=>'long'));?> <br class="clear">
					<?php echo $htmlExt->selectTag('company_work_position_id',$company_work_position_list,null,array('label'=>'Profese','class'=>'long','label_class'=>'long'));?><br/>
					<?php echo $htmlExt->selectTag('company_money_item_id',$money_item_list=array(),null,array('label'=>'Forma odměny','class'=>'long','label_class'=>'long'),null,false);?><br/>
					
					<?php echo $htmlExt->inputDate('from',array('tabindex'=>1,'value'=>Date ('Y-m-d'),'label'=>'Datum nástupu','class'=>'','label_class'=>'long'));?> <br class="clear">		
      </fieldset>
	<div class='formular_action'>
		<input type='button' value='Zaměstnat' id='AddEditClientMessageSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditClientMessageClose'/>
	</div>
</form>
<script>

	// add event for change Profese and load list for calculations
	$('CompanyWorkPositionId').addEvents({
			'change': function(){
				if($('calbut_From').value != null)
				{
					new Request.JSON({
						url: '/report_requirements_for_recruitments/load_calculation_for_prefese_second/' + this.value + '/',
						onComplete: (function (json){
							if (json){
								if (json.result === true){
									var sub_select = $('CompanyMoneyItemId');
									sub_select.empty();
									//new Element('option').setHTML('').inject(sub_select);
									$each(json.data, function(item){
										doprava = 'Doprava '+(item.doprava != 0 ? 'Ano' : 'Ne');
										stravenka = 'Stravenka '+(item.stravenka != 0 ? 'Ano' : 'Ne');
										ubytovani = 'Ubytování '+(item.cena_ubytovani_na_mesic != 0 ? 'Ano' : 'Ne');
                                        body = item.cista_mzda_z_pracovni_smlouvy_na_hodinu + '/' + item.cista_mzda_faktura_zivnostnika_na_hodinu + '/' + item.cista_mzda_dohoda_na_hodinu + '/' + item.cista_mzda_cash_na_hodinu;
										
                                        if(item.pausal == 1)
                                            body = item.ps_pausal_hmm + '/' + item.odmena_fakturace_1 + '/' + item.odmena_fakturace_2
                                        
                                        new Element('option',{value:item.id,title:item.name + '|' + item.stravenka + '|' + item.fakturacni_sazba_na_hodinu})
											.setHTML(item.name + ' - ' + body
											+ ' - ' + ubytovani +' / '+ doprava	+ ' / ' + stravenka
											)
											.inject(sub_select);
									})
									//this.currentSelection = this.selectedIndex;
								} else {
									this.options[this.currentSelection].selected = 'selected';
									alert(json.message);
								}
							} else {
								alert('Systemova chyba!!!');
							}
						}).bind(this)
					}).send();
				}
				else 
					alert('Musíte zvolit datum nástupu');
			},
			'focus': function(){
				this.currentSelection = this.selectedIndex;
			}
	});

	$('AddEditClientMessageClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_add_as_employment');});
	
	$('AddEditClientMessageSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		if($('CompanyWorkPositionId').value != ''){
			new Request.JSON({
				url:$('formular').action,		
				onComplete:function(){
				  vytvorit_zamestnance();
					domwin.closeWindow('domwin_add_as_employment');
				}
			}).post($('formular'));
		}
		else
			alert('Musíte vyplnit profesi');

	});
	</script>

	