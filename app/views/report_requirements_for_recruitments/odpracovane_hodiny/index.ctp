<div>
	<?php echo $htmlExt->hidden('connection_client_requirement_id',array('value' => $connection_client_requirement_id));?>
	<div class="sll3" >  
		<?php echo $htmlExt->var_text('Client/jmeno',array('label'=>'Jméno'));?> <br/>
		<?php echo $htmlExt->var_text('Client/prijmeni',array('label'=>'Příjmení'));?> <br/>
	</div>
	<div class="sll3" >  
		<?php echo $htmlExt->var_text('ConnectionClientRequirement/from',array('label'=>'Datum nástupu'));?> <br/>
		<?php echo $htmlExt->var_text('ConnectionClientRequirement/to', array('label'=>'Datum ukončení'));?>
	</div>
	<div class='sll3'>
		<?php echo $htmlExt->selectTag('month2',$mesice_list,$month,array('label'=>'Měsíc', 'class'=>'odpracovane_hodiny_change_date'));?> <br/>
		<?php echo $htmlExt->selectTag('year2', $actual_years_list,$year,array('label'=>'Rok', 'class'=>'odpracovane_hodiny_change_date'),null,true);?>
	</div>
</div>
<br />
<div id="odpracovane_hodiny_element">
	<?php echo $this->renderElement('../report_requirements_for_recruitments/odpracovane_hodiny/element',array('month'=>$month, 'year' =>$year));?>
</div> 
<script language="javascript" type="text/javascript">
	// init event for onChange year and month
	$$('.odpracovane_hodiny_change_date').addEvents({
		'change': function(e){
			this.dont_change_focus = true;
			$('odpracovane_hodiny_element').fade(0);
			if (!$('made_change_in_form') || $('made_change_in_form').value == 0 || confirm('Byly provedeny změny, po provedeni tohoto kroku nebudou změněné udaje uloženy. Pokud je chcete uložit, klikněte nejprve na tlačitko Uložit. Chcete pokračovat?')){
				new Request.HTML({
					url: '/report_requirements_for_recruitments/odpracovane_hodiny/' + $('ConnectionClientRequirementId').value + '/' + $('Year2').value + '/' + $('Month2').value + '/true',
					update: $('odpracovane_hodiny_element'),
					onComplete: function(){
						$('odpracovane_hodiny_element').fade(1);
					}
				}).send();
			} else {
				new Event(e).stop();
				this.options[this.currentSelection].selected = 'selected';
				$('odpracovane_hodiny_element').fade(1);
			}
			this.dont_change_focus = false;
		},
		'focus': function(){
			if (!this.dont_change_focus || this.dont_change_focus == false){
				this.currentSelection = this.selectedIndex;
			}
		}
	});
</script>