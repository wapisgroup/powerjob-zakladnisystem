<?php 
		
	$start 	= $this->data['ConnectionClientRequirement']['from'];
	$end 	= ($this->data['ConnectionClientRequirement']['to']== '0000-00-00')?date("Y-m-d"):$this->data['ConnectionClientRequirement']['to'];	
	
	$pocet_dnu_v_mesici = cal_days_in_month(CAL_GREGORIAN, $month, $year);
	$last_day = date('N', mktime(0, 0, 0, $month, 1, $year)); 
	$current_day = $last_day;
	$dny = array( 1=>'Po', 2=>'Út', 3=>'St', 4=>'Čt', 5=>'Pá', 6=>'So', 7=>'Ne' );
	
	// render list of disabled fields
	$not_to_disabled = array();
	for ($i=1; $i <= $pocet_dnu_v_mesici; $i++){
		$curr_date = $year.'-'.$month.'-'.((strlen($i) == 1)?'0'.$i:$i);
		if (strtotime($start) <= strtotime($curr_date) && strtotime($end) >= strtotime($curr_date))
			$not_to_disabled[] = $i;
	}
?>
<input type='hidden' value='0' id='made_change_in_form' />
<form action='/report_requirements_for_recruitments/odpracovane_hodiny/' method='post' id='ClientWorkingHour_edit_formular'>
	<!-- Basic Hidden Fields -->
	
	<?php echo $htmlExt->hidden('ClientWorkingHour/id');?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/company_id');?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/client_id');?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/requirements_for_recruitment_id');?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/connection_client_requirement_id');?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/year',array('value'=>$year));?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/month',array('value'=>$month));?>
	
	
	<?php echo $htmlExt->hidden('ClientWorkingHour/typ_cena_za_ubytovani');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
			<li class="ousko"><a href="#krok1">Nastavení</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field" >
			
			<fieldset>
				<legend>Základní nastavení</legend>
				<div class='sll3'>
					<?php echo $htmlExt->selectTag('ClientWorkingHour/company_work_position_id',$company_work_position_list,null,array('label'=>'Profese'));?><br/><br/>
					<?php echo $htmlExt->selectTag('ClientWorkingHour/setting_shift_working_id', $smennost_list,null, array('label'=>'Směnnost'),null,false);?> <br /> 
					<?php echo $htmlExt->selectTag('ClientWorkingHour/including_of_accommodation',array('Ne','Ano'),null,array('label'=>'Využívá ubytování'),null,false);?><br/>
					<?php echo $htmlExt->selectTag('ClientWorkingHour/including_of_meals_ticket',array('Ne','Ano'),null,array('label'=>'Dostává stravenky'),null,false);?><br/>
					<?php echo $htmlExt->selectTag('ClientWorkingHour/including_of_trafic',array('Ne','Ano'),null,array('label'=>'Využívá dopravu'),null,false);?><br/>
					<?php echo $htmlExt->selectTag('ClientWorkingHour/including_suit',array('Ne','Ano'),null,array('label'=>'Dostává prac. oděv'),null,false);?><br/>
			
				</div>
				<div class='sll3'>
					<?php echo $htmlExt->selectTag('ClientWorkingHour/company_money_item_id',$money_item_list,null,array('label'=>'Forma odměny'),array('title'=>$money_item_title));?><br/><br/>
					<?php echo $htmlExt->input('ClientWorkingHour/standard_hours',array('value'=>$this->data['RequirementsForRecruitment']['standard_hours'],'readonly'=>'readonly','label'=>'Norma hod.'));?>
					<?php echo $htmlExt->selectTag('ClientWorkingHour/accommodation_id',$accommodation_list,null,array('label'=>'Ubytování'),array('title'=>$accommodation_title_list));?><br/>
					<?php echo $htmlExt->input('ClientWorkingHour/stravenka',array('label'=>'Cena stravenky'));?><br />
				</div>
				<div class='sll3'>
					<br/>
					<div style='height:38px'></div><br/>
					<?php echo $htmlExt->input('ClientWorkingHour/cena_za_ubytovani',array('label'=>'Cena ubytování'));?><br/>
				</div>
			</fieldset>
			<fieldset>
				<legend>Základní nastavení</legend>
				<div class='sll3'>
					<?php echo $htmlExt->input('ClientWorkingHour/salary_part_1',array('label'=>'Na hodinu PS','class'=>'float salary','disabled'=>$sub_salary_disabled[1]));?>
					<?php echo $htmlExt->input('ClientWorkingHour/salary_part_2',array('label'=>'Na hodinu ŽL','class'=>'float salary','disabled'=>$sub_salary_disabled[2]));?>
					<?php echo $htmlExt->input('ClientWorkingHour/salary_part_3',array('label'=>'Na hodinu D','class'=>'float salary','disabled'=>$sub_salary_disabled[3]));?>
					<?php echo $htmlExt->input('ClientWorkingHour/salary_part_4',array('label'=>'Na hodinu C','class'=>'float salary','disabled'=>$sub_salary_disabled[4]));?>
					<?php echo $htmlExt->input('ClientWorkingHour/salary_per_hour',array('readonly'=>'readonly','label'=>'Na hodinu'));?>
					<?php echo $htmlExt->input('ClientWorkingHour/max_salary',array('readonly'=>'readonly','label'=>'Max. odměna'));?>
				</div>
				<div class='sll3'>
					<?php echo $htmlExt->input('ClientWorkingHour/salary_part_1_p',array('label'=>'Pracovní smlouva','readonly'=>'readonly'));?> <br/>
					<?php echo $htmlExt->input('ClientWorkingHour/salary_part_2_p',array('label'=>'ŽL','readonly'=>'readonly'));?><br/>
					<?php echo $htmlExt->input('ClientWorkingHour/salary_part_3_p',array('label'=>'Dohoda','readonly'=>'readonly'));?><br/>
					<?php echo $htmlExt->input('ClientWorkingHour/salary_part_4_p',array('label'=>'Abc','readonly'=>'readonly'));?><br/>
					<?php echo $htmlExt->input('ClientWorkingHour/salary_per_hour_p',array('label'=>'Celkem mzda', 'readonly'=>'readonly'));?><br/>
				</div>
				<div class='sll3'>
					<?php echo $htmlExt->input('ClientWorkingHour/celkem_hodin',array('readonly'=>'readonly','label'=>'Celkem hodin'));?>
					<?php echo $htmlExt->input('ClientWorkingHour/svatky',array('readonly'=>'readonly','label'=>'Svátky'));?>
					<?php echo $htmlExt->input('ClientWorkingHour/prescasy',array('readonly'=>'readonly','label'=>'Přesčasy'));?>				
					<?php echo $htmlExt->input('ClientWorkingHour/vikendy',array('readonly'=>'readonly','label'=>'Víkendy'));?>
					<?php echo $htmlExt->input('ClientWorkingHour/dovolena',array('readonly'=>'readonly','label'=>'Dovolenka'));?>
					<?php echo $htmlExt->input('ClientWorkingHour/neplacene_volno',array('readonly'=>'readonly','label'=>'NV'));?>
					<?php echo $htmlExt->input('ClientWorkingHour/ocestrovani_clena_rodiny',array('readonly'=>'readonly','label'=>'OČR'));?>
					<?php echo $htmlExt->input('ClientWorkingHour/neomluvena_absence',array('readonly'=>'readonly','label'=>'A'));?>
					<?php echo $htmlExt->input('ClientWorkingHour/pracovni_neschopnost',array('readonly'=>'readonly','label'=>'PN'));?>
					<?php echo $htmlExt->input('ClientWorkingHour/nahradni_volno',array('readonly'=>'readonly','label'=>'V'));?>
				</div>
			</fieldset>
			<br /><br />
			<table class='table odpracovane_hodiny'>	
				<tr>
					<th>Den</th>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<th <?php 
							if (in_array(($i+1),$svatky)) 
								echo "class='svatky'";
							else if (in_array($current_day,array(6,7))) 
								echo "class='vikend'";
						?>>
							<?php echo $dny[$current_day];?>
							<br />
							<?php echo ($i+1);?>
							<?php $current_day = ($current_day == 7)?$current_day=1:$current_day+1;?>
							
						</th>
					<?php endfor;?>
				</tr>
				
				<?php for($k = 1; $k <= 3; $k++):?>
				<tr class='tr_smennost_<?php echo $k;?> <?php if (($k == 2 && $this->data['ClientWorkingHour']['setting_shift_working_id'] == 1) || ($k == 3 && in_array($this->data['ClientWorkingHour']['setting_shift_working_id'],array(1,2)))) { echo 'none'; }?>'>	
					<td>Směna<?php echo $k;?></td>
					<?php $current_day = $last_day;?>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<td <?php 
							if (in_array(($i+1),$svatky)) 
								echo "class='svatky'";
							else if (in_array($current_day,array(6,7))) 
								echo "class='vikend'";
						?>>
						<?php 
							if (in_array(($i+1),$svatky)){ 
								$class =  " svatky";
							} else if (in_array($current_day,array(6,7))){
								$class = " vikend";
							} else {
								$class = "";
							}
						?>
						<?php echo $htmlExt->input('ClientWorkingHour/days/'. '_' .$k. '_' .($i+1),array('class'=>'hodiny float '.$class,'disabled'=>(!in_array(($i+1),$not_to_disabled))?'disabled':null));?></td>
						<?php $current_day = ($current_day == 7)?$current_day=1:$current_day+1;?>
					<?php endfor;?>
				</tr>
				<?php endfor;?>
				<tr class='tr_food_ticket <?php if (!isset($this->data['ClientWorkingHour']['including_of_meals_ticket']) || $this->data['ClientWorkingHour']['including_of_meals_ticket'] == 0) echo 'none';?>'>
					<td>Stravenky</td>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<td><?php echo $htmlExt->checkbox('ClientWorkingHour/food_days/'. '_' .$k. '_' .($i+1),null,array('class'=>'food_day','disabled'=>(!in_array(($i+1),$not_to_disabled))?'disabled':null));?></td> 
					<?php endfor;?>
				</tr>
				<tr class='tr_accommodation <?php if (!isset($this->data['ClientWorkingHour']['including_of_accommodation']) || $this->data['ClientWorkingHour']['including_of_accommodation'] == 0) echo 'none';?>'>
					<td>Ubytování</td>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<td><?php echo $htmlExt->checkbox('ClientWorkingHour/accommodation_days/'. '_' .$k. '_' .($i+1),null,array('class'=>'accommodation_day','disabled'=>(!in_array(($i+1),$not_to_disabled))?'disabled':null));?></td> 
					<?php endfor;?>
				</tr>
			</table>
			<br />
		</div>
		<div class="domtabs field" >
			<div class='sll'>
				<?php echo $htmlExt->input('ClientWorkingHour/salary_user_cost',array('label'=>'Osobní náklady'));?>
				<?php echo $htmlExt->input('ClientWorkingHour/salary_cost',array('label'=>'Mzdové náklady'));?>
				<?php echo $htmlExt->input('ClientWorkingHour/accommodation_cost',array('label'=>'Náklady na ubytování'));?>
				<?php echo $htmlExt->input('ClientWorkingHour/recruitment_cost',array('label'=>'Náklady na nábor'));?>
			</div>
			<div class='slr'>
				<?php echo $htmlExt->input('ClientWorkingHour/clothes_cost',array('label'=>'Náklady na oděv'));?>
				<?php echo $htmlExt->input('ClientWorkingHour/transfer_cost',array('label'=>'Náklady na dopravu'));?>
				<?php echo $htmlExt->input('ClientWorkingHour/food_cost',array('label'=>'Náklady na stravenky'));?>
			</div>
			<br />
		</div>
	</div>
	<div style='text-align:center'>
		<?php if (in_array($this->data['ClientWorkingHour']['stav'],array(1,2))):?>
		<input type='button' value='Uložit' id='only_save'/>
		<?php endif;?>
		<input type='button' value='Zavřít' id='only_close'/>
		<?php if ($this->data['ClientWorkingHour']['stav'] == 1):?>
			<input type='button' value='Uzavřít odpracované hodiny' id='close_close'/>
		<?php endif;?>
		<?php if ($this->data['ClientWorkingHour']['stav'] == 2):?>
			<input type='button' value='Autorizovat odpracované hodiny' id='auth_close'/>
		<?php endif;?>
		<?php if ($this->data['ClientWorkingHour']['stav'] == 3):?>
			<input type='button' value='Vyplaceno' id='auth_close'/>
		<?php endif;?>
	</div>
</form>

<div id='dblclick_input' class='none'>
<label>Typ:</label>
	<select class='choose_input'>
		<option value="1" 	title="Cas">Cas</option>
		<option value="PN" 	title="Pracovní neschopnost">Pracovní neschopnost</option>
		<option value="D" 	title="Dovolená">Dovolená</option>
		<option value="NV" 	title="Neplacené volno">Neplacené volno</option>
		<option value="OČR" title="Ošetřování člena rodiny">Ošetřování člena rodiny</option>
		<option value="A" 	title="Neomluvená absence">Neomluvená absence</option>
		<option value="V" 	title="Nahradní volno">Nahradní volno</option>
	</select>
	<br/>
	<div class='choose_do_div'>
		<label>Od:</label>
		<select class='choose_od_h' style='width:40px; float:left;'>
			<?php for($i = 0; $i<24; $i++):?>
				<option value="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>" 	title="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>"><?php echo ((strlen($i)==1)?'0'.$i:$i);?></option>
			<?php endfor;?>
		</select><span style='display:block; width:5px; float:left; line-height:20px'>:</span>
		<select class='choose_od_m' style='width:40px'>
			<?php for($i = 0; $i<60; $i += 5):?>
				<option value="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>" 	title="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>"><?php echo ((strlen($i)==1)?'0'.$i:$i);?></option>
			<?php endfor;?>
		</select>
		<br/>
	</div>
	<div class='choose_od_div'>
		<label>Do:</label>
		<select class='choose_do_h' style='width:40px; float:left;'>
			<?php for($i = 0; $i<24; $i++):?>
				<option value="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>" 	title="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>"><?php echo ((strlen($i)==1)?'0'.$i:$i);?></option>
			<?php endfor;?>
		</select><span style='display:block; width:5px; float:left; line-height:20px'>:</span>
		<select class='choose_do_m' style='width:40px'>
			<?php for($i = 0; $i<60; $i += 5):?>
				<option value="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>" 	title="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>"><?php echo ((strlen($i)==1)?'0'.$i:$i);?></option>
			<?php endfor;?>
		</select>
		<br/>
	</div>
	<p>
		<input type='button' value='Vložte' class='choose_do_it'/>
	</p>
</div>


<script language="javascript" type="text/javascript">
	$('ClientWorkingHourSalaryUserCost').addEvent('change', calculate_salary_cost);

	$('only_close').addEvent('click', function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin_zamestnanci_hodiny')
	});
	
	
	function calculate_salary_cost(){
		var pocet_hodin = $('ClientWorkingHourCelkemHodin').value.toFloat();
		var osobni_naklady = $('ClientWorkingHourSalaryUserCost').value.toFloat();
		
		var salary_part_1 = $('ClientWorkingHourSalaryPart1').value.toFloat();
		var salary_part_2 = $('ClientWorkingHourSalaryPart2').value.toFloat();
		var salary_part_3 = $('ClientWorkingHourSalaryPart3').value.toFloat();
		var salary_part_4 = $('ClientWorkingHourSalaryPart4').value.toFloat();
		
		var salary_to_count = salary_part_2 + salary_part_3 + salary_part_4;
		$('ClientWorkingHourSalaryCost').value = salary_to_count * pocet_hodin + osobni_naklady;
	}

	// input limit
	$('ClientWorkingHour_edit_formular').getElements('.integer, .float').inputLimit();
	
	// domtabs init
	var domtab = new DomTabs({'className':'admin_dom'}); 
	
	// save button
	$('only_save').addEvent('click', function(e){
		new Event(e).stop();
		new Request.JSON({
			url: $('ClientWorkingHour_edit_formular').getProperty('action'),
			onComplete: function(json){
				if (json){
					if (json.result === true){
						$('ClientWorkingHourId').value = json.id;
						$('made_change_in_form').value = 0;
						alert(json.message);
					} else {
						alert(json.message);
					}
				} else {
					alert('Chyba aplikace!');
				}
			}
		}).send($('ClientWorkingHour_edit_formular'));
	});
	
	// function for reacound day/hours
	$$('.hodiny').addEvent('change', recount_hours);
	function recount_hours(){
		var NH = $('ClientWorkingHourStandardHours').value.toFloat();
		var count = 0;
		var vikendy = 0;
		var svatky = 0;
		var prescas = 0;
		var dovolena = 0;
		var neplacene_volno = 0;
		var ocr = 0;
		var neomluvena_absence = 0;
		var pracovni_neschopnost = 0;
		var nahradni_volno = 0;
		
		$$('.hodiny').each(function(item){
			switch(item.value){
				case 'PN':
						pracovni_neschopnost ++;
					break;
				case 'OČR':
						ocr ++;
					break;
				case 'D':
						dovolena ++;
					break;
				case 'NV':
						neplacene_volno ++;
					break;
				case 'A':
						neomluvena_absence ++;
					break;	
				case 'V':
						nahradni_volno ++;
					break;
				default:
					if (item.value == '') item.value = 0;
					
					// celkem
					count += item.value.toFloat();
					
					// vikend
					if (item.hasClass('vikend')) vikendy += item.value.toFloat();
					// svatky
					if (item.hasClass('svatky')) svatky += item.value.toFloat();
					// prescas
					if (item.value.toFloat() > NH) prescas += (item.value.toFloat() - NH);
					break;
			}
		});

		
		$('ClientWorkingHourCelkemHodin').value = count;
		$('ClientWorkingHourSvatky').value = svatky;
		$('ClientWorkingHourPrescasy').value = prescas;
		$('ClientWorkingHourVikendy').value = vikendy;
		$('ClientWorkingHourDovolena').value = dovolena;
		$('ClientWorkingHourNeplaceneVolno').value = neplacene_volno;
		$('ClientWorkingHourOcestrovaniClenaRodiny').value = ocr;
		$('ClientWorkingHourNeomluvenaAbsence').value =  neomluvena_absence;
		$('ClientWorkingHourPracovniNeschopnost').value =  pracovni_neschopnost;
		$('ClientWorkingHourNahradniVolno').value =  nahradni_volno;
		calculate_salary_cost();
	}

	// enabled / disabled salary per hour
	$('ClientWorkingHourCompanyMoneyItemId').addEvent('change', function(){
		title = this.options[this.selectedIndex].title.split('|');
		type = title[0];
		stravenka = title[1];
		for (i = 0; i<4; i++){
			if (type[i] == 0) 
				$('ClientWorkingHourSalaryPart'+(i+1)).setProperty('disabled','disabled').setValue('0');
			else 
				$('ClientWorkingHourSalaryPart'+(i+1)).removeProperty('disabled');
		}
		var ar = this.options[this.selectedIndex].getHTML().split(' - ');
		max_salary = 0;
		if (ar[1]){
			ar = ar[1].split('/');	
			for(i=0;i<ar.length;max_salary+=ar[i++].toFloat());
		}
		$('ClientWorkingHourMaxSalary').value = max_salary;
		$('ClientWorkingHourStravenka').value = (stravenka)?stravenka:0;
		calculate_salary_cost();
		calculate_food_money();
	});
	// calculate sumary salary per hour
	$$('.salary').addEvent('change', function(){
		var sumary = 0;
		$$('.salary').each(function(el){
			if (el.value != '')
				sumary += el.value.toFloat();
		});
		$('ClientWorkingHourSalaryPerHour').value = sumary;
		if (sumary > $('ClientWorkingHourMaxSalary').value)
			$('ClientWorkingHourSalaryPerHour').addClass('invalid');
		else
			$('ClientWorkingHourSalaryPerHour').removeClass('invalid');
		
		calculate_salary_cost();
	})
	
	// condition for readonly for use_accommodation
	$('ClientWorkingHourAccommodationId').addEvent('change', function(){
		if ($('ClientWorkingHourIncludingOfAccommodation').value == 0){
			this.selectedIndex = 0;
		} else {
			if (this.value != ''){
				title = this.options[this.selectedIndex].title.split('|');
				$('ClientWorkingHourCenaZaUbytovani').value = title[1];
				$('ClientWorkingHourTypCenaZaUbytovani').value = title[0];
			} else {
				$('ClientWorkingHourCenaZaUbytovani').value = 0;
				$('ClientWorkingHourTypCenaZaUbytovani').value = -1;
			}
			calculate_accommodation_money();
		}
	})
	
	// change use accommodation
	$('ClientWorkingHourIncludingOfAccommodation').addEvent('change', function(){
		if (this.value == 0){
			$('ClientWorkingHour_edit_formular').getElements('.accommodation_day').removeProperty('checked');
			$('ClientWorkingHour_edit_formular').getElement('.tr_accommodation').addClass('none');
			$('ClientWorkingHourAccommodationId').selectedIndex = 0;
			$('ClientWorkingHourCenaZaUbytovani').value = 0;
			$('ClientWorkingHourTypCenaZaUbytovani').value = -1;
		} else {
			$('ClientWorkingHour_edit_formular').getElement('.tr_accommodation').removeClass('none');
		}
		calculate_accommodation_money();
	})
	
	// event to calculate  && declare function for calculate caccommodation
	function calculate_accommodation_money(){ 
		switch ($('ClientWorkingHourTypCenaZaUbytovani').value){
			case '-1':  $('ClientWorkingHourAccommodationCost').value = 0; break;
			case '1':
				$('ClientWorkingHourAccommodationCost').value = Math.round(($('ClientWorkingHourCenaZaUbytovani').value * $$('.accommodation_day').map(function(el){ if(el.checked) return el; else return null; }).clean().length) * 100)/100;
				break;
			case '2': $('ClientWorkingHourAccommodationCost').value = $('ClientWorkingHourCenaZaUbytovani').value; break;
			default:
				break;
		}
	}
	$$('.accommodation_day').addEvent('click', calculate_accommodation_money);
	
	// event to calculate  && declare function for calcultion food
	function calculate_food_money(){ 
		$('ClientWorkingHourFoodCost').value = $('ClientWorkingHourStravenka').value.toFloat() * $$('.food_day').map(function(el){ if(el.checked) return el; else return null; }).clean().length
	}
	$$('.food_day').addEvent('click', calculate_food_money);
	
	// change use food ticket
	$('ClientWorkingHourIncludingOfMealsTicket').addEvent('change', function(){
		if (this.value == 0){
			$('ClientWorkingHour_edit_formular').getElements('.food_day').removeProperty('checked');
			$('ClientWorkingHour_edit_formular').getElement('.tr_food_ticket').addClass('none');
		} else {
			$('ClientWorkingHour_edit_formular').getElement('.tr_food_ticket').removeClass('none');
		}
	})
	
	// change work shift
	$('ClientWorkingHourSettingShiftWorkingId').addEvents({
		'change': function(e){
			this.dont_change_focus = true;
			if (confirm('Opravdu si přejete změnit typ směn, může dojít ke ztrátě nadefinovaných počtů hodin?')){
				// define new selectedIndex
				this.currentSelection = this.selectedIndex;
				var count_shift = this.value;
				switch (count_shift){
					case '1':
						$('ClientWorkingHour_edit_formular').getElements('.tr_smennost_2, .tr_smennost_3').addClass('none');
						$('ClientWorkingHour_edit_formular').getElement('.tr_smennost_2').getElements('input').setValue('');
						$('ClientWorkingHour_edit_formular').getElement('.tr_smennost_3').getElements('input').setValue('');
						break;
					case '2':
						$('ClientWorkingHour_edit_formular').getElement('.tr_smennost_2').removeClass('none');
						$('ClientWorkingHour_edit_formular').getElement('.tr_smennost_3').addClass('none');
						$('ClientWorkingHour_edit_formular').getElement('.tr_smennost_3').getElements('input').setValue('');
						break;
					case '3':
						$('ClientWorkingHour_edit_formular').getElements('.tr_smennost_2, .tr_smennost_3').removeClass('none');
						break;
					default:
						alert('Neznamy typ smeny');
						break;
				}
				recount_hours();
			} else {
				new Event(e).stop();
				this.options[this.currentSelection].selected = 'selected';
			}
			this.dont_change_focus = false;
		},
		'focus': function(e){
			// add hack for cancel confirm, its select default selectedIndex
			if (!this.dont_change_focus || this.dont_change_focus == false){
				this.currentSelection = this.selectedIndex;
			}
		}
	});
	
	// add event for change Profese and load list for calculations
	$('ClientWorkingHourCompanyWorkPositionId').addEvents({
		'change': function(){
			new Request.JSON({
				url: '/report_requirements_for_recruitments/load_calculation_for_prefese/' + this.value + '/' + $('ClientWorkingHourYear').value + '/' + $('ClientWorkingHourMonth').value,
				onComplete: (function (json){
					if (json){
						if (json.result === true){
							var sub_select = $('ClientWorkingHourCompanyMoneyItemId');
							sub_select.empty();
							new Element('option').setHTML('').inject(sub_select);
							$each(json.data, function(item){
								new Element('option',{title:item.name + '|' + item.stravenka})
									.setHTML(item.name + ' - ' + item.cista_mzda_z_pracovni_smlouvy_na_hodinu + '/' + item.cista_mzda_faktura_zivnostnika_na_hodinu + '/' + item.cista_mzda_dohoda_na_hodinu + '/' + item.cista_mzda_cash_na_hodinu)
									.inject(sub_select);
							})
							this.currentSelection = this.selectedIndex;
							calculate_food_money();
						} else {
							this.options[this.currentSelection].selected = 'selected';
							alert(json.message);
						}
					} else {
						alert('Systemova chyba!!!');
					}
				}).bind(this)
			}).send();
		},
		'focus': function(){
			this.currentSelection = this.selectedIndex;
		}
	});
	
	// add event dblclick on textinput
	$('ClientWorkingHour_edit_formular').getElements('.hodiny:enabled').addEvent('dblclick', function(){
		// call
		var obj = this;
		domwin.newWindow({
			id			: 'domwin_hodiny_od_do',
			sizes		: [300,170],
			scrollbars	: true, 
			title		: 'Vložte čas práce',
			languages	: false,
			type		: 'DOM',
			dom_id		: $('dblclick_input'),
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: true,
			remove_scroll: false,
			dom_onRender: function(){
				var choose_select = $('domwin_hodiny_od_do').getElement('.choose_input');
				var choose_do_it  = $('domwin_hodiny_od_do').getElement('.choose_do_it');
				
				var choose_do_div = $('domwin_hodiny_od_do').getElement('.choose_do_div');
				var choose_od_div = $('domwin_hodiny_od_do').getElement('.choose_od_div');
				
			
				
				choose_select.addEvent('change', function(){
					if (this.value == 1){
						choose_do_div.removeClass('none');
						choose_od_div.removeClass('none');
					} else {
						choose_do_div.addClass('none');
						choose_od_div.addClass('none');
					}
				});
				
				choose_do_it.addEvent('click', function(e){
					new Event(e).stop();
					
					var choose_od = $('domwin_hodiny_od_do').getElement('.choose_od_h').value + ':' + $('domwin_hodiny_od_do').getElement('.choose_od_m').value;
					var choose_do = $('domwin_hodiny_od_do').getElement('.choose_do_h').value + ':' + $('domwin_hodiny_od_do').getElement('.choose_do_m').value;
				
					if (choose_select.value == 1)
						obj.value = compare_times(choose_od,choose_do);					
					else {
						obj.value = choose_select.value;
					}
					domwin.closeWindow('domwin_hodiny_od_do');
					recount_hours();
				});
			}
		});
	});
	
	function compare_times(time1, time2){
		var s1 = time1.split(':'), s2 = time2.split(':'), td;
		t1 = new Date(1970, 0, 0, s1[0] ? s1[0] : 0, s1[1] ? s1[1] : 0, s1[2] ? s1[2] : 0);
		t2 = new Date(1970, 0, ((s2[0] < s1[0]) || (s2[0]==s1[0] && s2[1] < s1[1]))?1:0, s2[0] ? s2[0] : 0, s2[1] ? s2[1] : 0, s2[2] ? s2[2] : 0);
		td = (t2 - t1)/60/1000;
		zbytek = ((td % 60) < 10)?'0'+(td % 60):(td % 60);
		output = (td / 60).toInt() + '' + ((zbytek == 0)?'':'.' + zbytek);
		return output;
	}
	
	$('odpracovane_hodiny_element').getElements('input, select').addEvent('change', function(){
		$('made_change_in_form').value = 1;
	})

</script>