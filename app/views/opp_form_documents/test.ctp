<fieldset>

<ul>
<? if(Count($firstLevel) > 0): ?>
<? foreach($firstLevel as $id=>$name): ?>
 <ul>
   <li class="first_level"><?=  $htmlExt->checkbox('OppFormDocumentList', null ,array( 'value'=>$id )).' '.$name; ?></li>

   <? if(isset($secondLevel[$id]) && Count($secondLevel[$id]) > 0): ?>
        <ul>
        <? foreach($secondLevel[$id] as $id2=>$name2): ?>

            <li class="second_level"><?=  $htmlExt->checkbox('OppFormDocumentList', null ,array( 'value'=>$id2 )).' '.$name2; ?></li>

            <? if(isset($thirdLevel[$id2]) && Count($thirdLevel[$id2]) > 0): ?>
                <ul>
                <? foreach($thirdLevel[$id2] as $id3=>$name3): ?>

                   <li class="third_level"><?= $htmlExt->checkbox('OppFormDocumentList', null ,array( 'value'=>$id3 )).' '.$name3; ?></li>

                <? endforeach; ?>
                </ul>
            <? endif; ?>

        <? endforeach; ?>
        </ul>
    <? endif; ?>
  </ul>
<? endforeach; ?>
 <? endif; ?>
</ul>
</fieldset>
<style>
   /*.second_level{
    text-indent:15px;
   }
   .third_level{
    text-indent:30px;
   }*/
</style>