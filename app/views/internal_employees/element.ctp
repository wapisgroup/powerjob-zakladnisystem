<?php 
		
	$start 	= $this->data['ConnectionClientAtCompanyWorkPosition']['datum_nastupu'];
	$end 	= ($this->data['ConnectionClientAtCompanyWorkPosition']['datum_to']== '0000-00-00')?date("Y-m-d"):$this->data['ConnectionClientAtCompanyWorkPosition']['datum_to'];	
	
	$pocet_dnu_v_mesici = cal_days_in_month(CAL_GREGORIAN, $month, $year);
	$last_day = date('N', mktime(0, 0, 0, $month, 1, $year)); 
	$current_day = $last_day;
	$dny = array( 1=>'Po', 2=>'Út', 3=>'St', 4=>'Čt', 5=>'Pá', 6=>'So', 7=>'Ne' );
	
	// render list of disabled fields
	$not_to_disabled = array();
	for ($i=1; $i <= $pocet_dnu_v_mesici; $i++){
		$curr_date = $year.'-'.$month.'-'.((strlen($i) == 1)?'0'.$i:$i);
		if (strtotime($start) <= strtotime($curr_date) && strtotime($end) >= strtotime($curr_date))
			$not_to_disabled[] = $i;
	}

?>
<input type='hidden' value='0' id='made_change_in_form' />
<form action='/internal_employees/odpracovane_hodiny/' method='post' id='ClientWorkingHoursInt_edit_formular'>
	<!-- Basic Hidden Fields -->
	
	<?php echo $htmlExt->hidden('ClientWorkingHoursInt/id');?>
	<?php echo $htmlExt->hidden('ClientWorkingHoursInt/at_company_id');?>
	<?php echo $htmlExt->hidden('ClientWorkingHoursInt/client_id');?>
	<?php echo $htmlExt->hidden('ClientWorkingHoursInt/connection_client_at_company_work_position_id');?>
	<?php echo $htmlExt->hidden('ClientWorkingHoursInt/year',array('value'=>$year));?>
	<?php echo $htmlExt->hidden('ClientWorkingHoursInt/month',array('value'=>$month));?>
    <?php echo $htmlExt->hidden('ClientWorkingHoursInt/standard_hours');?>
	<?php echo $htmlExt->hidden('ClientWorkingHoursInt/change_max_salary');?>
	<?php echo $htmlExt->hidden('ClientWorkingHoursInt/working_days_count');?>
    <?php echo $htmlExt->hidden('ClientWorkingHoursInt/setting_shift_working_id');?>
    <?php echo $htmlExt->hidden('ClientWorkingHoursInt/stravenky');?>
	<?php echo $htmlExt->hidden('Dovolena/zbyva_dovolene',array('value'=>$dovolena['zbyva_dovolene']));?>
    <?php echo $htmlExt->hidden('pocet_dnu_v_mesici',array('value'=>$pocet_dnu_v_mesici));?>
	<?php echo $htmlExt->hidden('ClientWorkingHoursInt/stav');?>
 	<?php echo $htmlExt->hidden('stop_payment_now',array('value'=>0));?>
	<?php echo $htmlExt->hidden('change_max_salary_now',array('value'=>0));?>
    <?php echo $htmlExt->hidden('different_drawback_activity');?>
    <?php echo $htmlExt->hidden('different_drawback_close_cw');?>
    <!-- Up zobrazeni pod elementem -->
	<div class='sll3'>
		<?php echo $htmlExt->var_text('AtProjectCentreWorkPosition/name',array('label'=>'Profese','class'=>'read_info'));?><br/>
        <?php echo $htmlExt->var_text('AtProjectCentre/name',array('label'=>'Středisko','class'=>'read_info'));?><br/>

    </div>
	<div class="sll3" >  
		<?php echo $htmlExt->var_text('AtProjectCentreWorkPosition/expirace_smlouvy',array('label'=>'Smlouva do','class'=>'read_info'));?><br/>
        <?php echo $htmlExt->var_text('AtCompanyMoneyItem/name',array('label'=>'Forma odměny','class'=>'read_info'));?><br/>
    </div>
	<div class="sll3" >  
    	<?php 
    		$text = $this->data['ClientWorkingHoursInt']['stravenky']<>0 ? 'Ano' : 'Ne';
    		echo $htmlExt->var_text('varStravenka', array('label'=>'Využívá stravenky','class'=>'read_info','value'=>$text))?><br />
	    <?php echo $htmlExt->input('ClientWorkingHoursInt/zbyva_dovolene',array('readonly'=>'readonly','label'=>'Zbývá dovolené','class'=>'read_info','value'=>$zbyva_dovolene)); ?>
	
	</div> <br /><br />
<!-- END -->

	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
            <li class="ousko"><a href="#krok1">Docházka</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field" >
			<table class='table odpracovane_hodiny'>	
				<tr>
					<th>Den</th>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<th <?php 
							if (in_array(($i+1),$svatky)) 
								echo "class='svatky'";
							else if (in_array($current_day,array(6,7))) 
								echo "class='vikend'";
						?>>
							<?php echo $dny[$current_day];?>
							<br />
							<?php echo ($i+1);?>
							<?php $current_day = ($current_day == 7)?$current_day=1:$current_day+1;?>
							
						</th>
					<?php endfor;?>
				</tr>
				<?php 
					$smena_captions = array(
						1	=> 'Ranní',
						2	=> 'Odpolední',
						3	=> 'Noční'
					);
				?>
				<?php for($k = 1; $k <= 3; $k++):?>
				<tr class='tr_smennost_<?php echo $k;?> <?php 
						if (($k == 2 && $this->data['ClientWorkingHoursInt']['setting_shift_working_id'] == 1) || 
							($k == 3 && in_array($this->data['ClientWorkingHoursInt']['setting_shift_working_id'],array(1,2)))
							|| (empty($this->data['ClientWorkingHoursInt']['setting_shift_working_id']))
							) { echo 'none'; }?>'>	
					<td><?php echo $smena_captions[$k];?></td>
					<?php $current_day = $last_day;?>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<td <?php 
							if (in_array(($i+1),$svatky)) 
								echo "class='svatky'";
							else if (in_array($current_day,array(6,7))) 
								echo "class='vikend'";
						?>>
						<?php 
							if (in_array(($i+1),$svatky)){ 
								$class =  " svatky";
							} else if (in_array($current_day,array(6,7))){
								$class = " vikend";
							} else {
								$class = "";
							}
						?>
						<?php echo $htmlExt->input('ClientWorkingHoursInt/days/'. '_' .$k. '_' .($i+1),array('class'=>'hodiny integer s'.$k.' '.$class,'disabled'=>(!in_array(($i+1),$not_to_disabled) || (isset($this->data['ClientWorkingHoursInt']['days']['_' .$k. '_' .($i+1)]) && $this->data['ClientWorkingHoursInt']['days']['_' .$k. '_' .($i+1)] == 'A') )?'disabled':null));?></td>
						<?php $current_day = ($current_day == 7)?$current_day=1:$current_day+1;?>
					<?php endfor;?>
				</tr>
				<?php endfor;?>
				<tr class='tr_food_ticket <?php if (!isset($this->data['ClientWorkingHoursInt']['stravenky']) || $this->data['ClientWorkingHoursInt']['stravenky'] == 0) echo 'none';?>'>
					<td>Stravenky - <a href="#" id="food_days_all">vše</a></td>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<td><?php echo $htmlExt->checkbox('ClientWorkingHoursInt/food_days/'. '_' .$k. '_' .($i+1),null,array('class'=>'food_day','disabled'=>(!in_array(($i+1),$not_to_disabled))?'disabled':null));?></td> 
					<?php endfor;?>
				</tr>
			</table>
			<br />

			<fieldset>
				<legend>Zadání mzdy pro aktuální měsíc</legend>
				<div class='slr3'>
					<label></label><strong style='margin-left:3px'>Informace o docházce</strong><br />
					<?php echo $htmlExt->input('ClientWorkingHoursInt/celkem_hodin',array('readonly'=>'readonly','label'=>'Celkem hodin','class'=>'read_info'));?>
					<?php echo $htmlExt->input('ClientWorkingHoursInt/svatky',array('readonly'=>'readonly','label'=>'Svátky','class'=>'read_info'));?>
					<?php echo $htmlExt->input('ClientWorkingHoursInt/prescasy',array('readonly'=>'readonly','label'=>'Přesčasy','class'=>'read_info'));?>				
					<?php echo $htmlExt->input('ClientWorkingHoursInt/vikendy',array('readonly'=>'readonly','label'=>'Víkendy','class'=>'read_info'));?>
					<?php echo $htmlExt->input('ClientWorkingHoursInt/dovolena',array('readonly'=>'readonly','label'=>'Dovolenka','class'=>'read_info'));?>
					<?php echo $htmlExt->input('ClientWorkingHoursInt/neplacene_volno',array('readonly'=>'readonly','label'=>'NV','class'=>'read_info'));?>
					<?php echo $htmlExt->input('ClientWorkingHoursInt/ocestrovani_clena_rodiny',array('readonly'=>'readonly','label'=>'OČR','class'=>'read_info'));?>
					<?php echo $htmlExt->input('ClientWorkingHoursInt/neomluvena_absence',array('readonly'=>'readonly','label'=>'A','class'=>'read_info'));?>
					<?php echo $htmlExt->input('ClientWorkingHoursInt/pracovni_neschopnost',array('readonly'=>'readonly','label'=>'PN','class'=>'read_info'));?>
					<?php echo $htmlExt->input('ClientWorkingHoursInt/nahradni_volno',array('readonly'=>'readonly','label'=>'V','class'=>'read_info'));?>
					<?php echo $htmlExt->input('ClientWorkingHoursInt/paragraf',array('readonly'=>'readonly','label'=>'PG','class'=>'read_info'));?>
					<?php for($k = 1; $k <= 3; $k++):?>
					<?php echo $htmlExt->input('ClientWorkingHoursInt/hour_s'.$k,array('readonly'=>'readonly','label'=>'S'.$k,'class'=>'read_info '.((($k == 2 && $this->data['ClientWorkingHoursInt']['setting_shift_working_id'] == 1) || ($k == 3 && in_array($this->data['ClientWorkingHoursInt']['setting_shift_working_id'],array(1,2))))?'none':''),'label_class'=>((($k == 2 && $this->data['ClientWorkingHoursInt']['setting_shift_working_id'] == 1) || ($k == 3 && in_array($this->data['ClientWorkingHoursInt']['setting_shift_working_id'],array(1,2))))?'none':'')));?>
					<?php endfor;?>
                    <?php echo $htmlExt->input('ClientWorkingHoursInt/prispevek_na_cestovne',array('readonly'=>'readonly','label'=>'Příspěvek na cestovné','class'=>'float read_info'));?>
					
				</div>
				
				<div class='sll3'>
					<label></label><strong style='margin-left:3px'>Mzda</strong><br />

					<?php echo $htmlExt->input('ClientWorkingHoursInt/salary_fix',array('label'=>'Fix','class'=>'short float salary'))."<label class='label_short'>/</label>".$htmlExt->input('ClientWorkingHoursInt/salary_fix_max',array('class'=>'short float for_null_past_money_item_change maximalky','disabled'=>'disabled'))."<br />";?>
				    <?php echo $htmlExt->input('ClientWorkingHoursInt/food_tickets',array('label'=>'Stravné','class'=>'short float salary'))."<label class='label_short'>/</label>".$htmlExt->input('ClientWorkingHoursInt/food_tickets_max',array('class'=>'short float for_null_past_money_item_change maximalky','disabled'=>'disabled'))."<br />";?> 
                    <?php echo $htmlExt->input('ClientWorkingHoursInt/odmena_1',array('label'=>$odmeny[1]['label'],'class'=>'short float salary'))."<label class='label_short'>/</label>".$htmlExt->input('ClientWorkingHoursInt/odmena_1_max',array('class'=>'short float for_null_past_money_item_change maximalky','disabled'=>'disabled','value'=>$odmeny[1]['max']))."<br />";?>
					<?php echo $htmlExt->input('ClientWorkingHoursInt/odmena_2',array('label'=>$odmeny[2]['label'],'class'=>'short float salary'))."<label class='label_short'>/</label>".$htmlExt->input('ClientWorkingHoursInt/odmena_2_max',array('class'=>'short float for_null_past_money_item_change maximalky','disabled'=>'disabled','value'=>$odmeny[2]['max']))."<br />";?>
					<?php echo $htmlExt->input('ClientWorkingHoursInt/odmena_3',array('label'=>$odmeny[3]['label'],'class'=>'short float salary'))."<label class='label_short'>/</label>".$htmlExt->input('ClientWorkingHoursInt/odmena_3_max',array('class'=>'short float for_null_past_money_item_change maximalky','disabled'=>'disabled','value'=>$odmeny[3]['max']))."<br />";?>
				
                    <label></label><strong style='margin-left:3px'>Srážky</strong><br />
					  <?php echo $htmlExt->input('ClientWorkingHoursInt/drawback_close_cw',array('label'=>'Uzavření docházek','class'=>'short salary_drawback float'));
                          echo $htmlExt->checkbox('ClientWorkingHoursInt/drawback_close_cw_add');?>
                          <span id="drawback_close_cw_different_box" class="none">
                          <img src="/img/../css/icons/status/zmena_maximalky.png"/>      
                          </span>    
                      <br />
                      <?php echo $htmlExt->input('ClientWorkingHoursInt/drawback_activity',array('label'=>'Aktivity','class'=>'short salary_drawback float'));
                          echo $htmlExt->checkbox('ClientWorkingHoursInt/drawback_activity_add');?>
                          <span id="drawback_activity_different_box" class="none">
                          <img src="/img/../css/icons/status/zmena_maximalky.png"/>      
                          </span>  
                      <br />
                      <?php echo $htmlExt->input('ClientWorkingHoursInt/drawback',array('label'=>'Jiné srážky','class'=>'short salary_drawback float'));
                          echo $htmlExt->checkbox('ClientWorkingHoursInt/drawback_add');?><br />
                    
                    <label></label><strong style='margin-left:3px'>Způsobené škody</strong><br />
                    <?php echo $htmlExt->input('ClientWorkingHoursInt/preliminary_damage',array('label'=>'Předběžná','class'=>'short float salary_damage hint-focus','hint'=>'V případě že předběžná škoda > 0, tak se automaticky zaškrtne políčko "Pozdržet výplatu".'))."<br />";?>
                    <?php echo $htmlExt->input('ClientWorkingHoursInt/final_damage',array('label'=>'Konečná','class'=>'short float salary_damage'))."<br />";?>
                    
					
				</div>
				<div class='sll3'>
					<label></label><strong style='margin-left:3px'>Mzda celkem</strong><br />
					<?php echo $htmlExt->input('ClientWorkingHoursInt/salary_total',array('label'=>'Celkem mzda', 'readonly'=>'readonly','class'=>'read_info'));?><br/>
					<?php echo $htmlExt->input('change_max_zalohy_now',array('label'=>'Zbývá k výplatě', 'readonly'=>'readonly','class'=>'read_info'));?><br/>
                    <?php echo $htmlExt->input('zalohy',array('label'=>'Zálohy', 'readonly'=>'readonly','class'=>'read_info'));?><br/>
                    <label>Pozdržet výplatu:</label><?php echo $htmlExt->checkbox('ClientWorkingHoursInt/stop_payment');?><br/>
				
                    <label></label><strong style='margin-left:3px'>Spotřeby</strong><br />
                    <?php echo $htmlExt->input('ClientWorkingHoursInt/phm_consumption',array('label'=>'PHM','class'=>'short float'))."<br />";?>
                    <?php echo $htmlExt->input('ClientWorkingHoursInt/mobile_consumption',array('label'=>'Telefon','class'=>'short float'))."<br />";?>
                    <?php echo $htmlExt->input('ClientWorkingHoursInt/data_consumption',array('label'=>'Dat.Služby','class'=>'short float'))."<br />";?>
                   
                </div>
					<br class="clear_left" />
					<?php echo $htmlExt->textarea('ClientWorkingHoursInt/comment',array('label'=>'Komentář k docházce', 'class'=>'textarea_spec','label_class'=>'small'));?>				
				<br class="clear_left" />
                <div id="payment_list_box">
                    <div id="payment_list_box_items">
                    <?php 
                    echo "&nbsp;Zálohy:<br class='clear_left' />";
                    foreach($payment_list as $item){
                        $out = null;
                        $out .= '<strong>'.$fastest->czechDate($item['ReportDownPayment']['created']).'</strong>';
                        $out .= '&nbsp;&nbsp;&nbsp;'.$item['CmsUser']['name'];
                        $out .= '&nbsp;&nbsp;&nbsp;částka - '.$item['ReportDownPayment']['amount'];
                        $out .= '&nbsp;&nbsp;&nbsp;hodin - '.$item['ReportDownPayment']['celkem_hodin'];
                        $out .= '&nbsp;&nbsp;&nbsp; uhrazeno - '.$fastest->czechDate($item['ReportDownPayment']['date_of_paid']);
                        
                        $payment_sum += $item['ReportDownPayment']['amount'];
                        echo $out.'<br class="clear_left" />';
                    }
                    ?>
                    </div>
                </div>
                <div id="npp_list_box">
                    <div id="npp_list_box_items">
                    <?php 
                    echo "&nbsp;Upozornění na neplnění pracovních poviností: ";
                    if(isset($npp_list))
                    foreach($npp_list as $i=>$item){
                        if($i > 0){ echo ', ';} 
                        echo '<strong>'.($i+1).'.</strong> ('.$fastest->czechDate($item['ConnectionClientMisconduct']['datum']).')';
                     }
                    ?>
                    <br class="clear_left" />
                    </div>
                </div>
			</fieldset>
		</div>
	</div>
	<div style='text-align:center'>
		<?php if ((empty($this->data['ClientWorkingHoursInt']['stav']) || in_array($this->data['ClientWorkingHoursInt']['stav'],array(1))) || in_array($logged_user,array(209,236,237)) || in_array($logged_group,array(1,5)) || in_array($logged_group,array(6,7)) && in_array($this->data['ClientWorkingHoursInt']['stav'],array(1,2))):?>
		<input type='button' value='Uložit' id='only_save'/>
		<?php endif;?>
        
        <?php if ((($year.'-'.$month) == date('Y-m')) && $allow_payment_0){?>
		      <input type='button' value='Požadavek na zálohu' rel='0' class='add_down_payments'/>
        <?php }?>
        
        <?php if ((($year.'-'.$month) == date('Y-m')) && $allow_payment_1){?>
		      <input type='button' value='Požadavek na VVH' rel='1' class='add_down_payments'/>
		<?php }?>
         
        <input type='button' value='Přidat negativní hodnocení' id='add_misconducts'/>
		<input type='button' value='Zavřít' id='only_close'/>
	</div>
</form>

<div id='dblclick_input' class='none'>
<label>Typ:</label>
	<select class='choose_input'>
		<option value="1" 	title="Cas">Cas</option>
		<option value="PN" 	title="Pracovní neschopnost">Pracovní neschopnost</option>
		<option value="D" 	title="Dovolená">Dovolená</option>
		<option value="NV" 	title="Neplacené volno">Neplacené volno</option>
		<option value="OČR" title="Ošetřování člena rodiny">Ošetřování člena rodiny</option>
		<!--
        <option value="A" 	title="Neomluvená absence">Neomluvená absence</option>
		-->
        <option value="V" 	title="Nahradní volno">Nahradní volno</option>
		<option value="PG" 	title="Paragraf">Paragraf</option>
	</select>
	<br/>
    <div class="dovolena_info none">
        <?php 	echo $htmlExt->var_text('Client/zbyva_dovolene', array('label'=>'Nárok na dovolenou','class'=>'read_info','value'=>$zbyva_dovolene)); ?>
    </div>
	<div class='choose_do_div'>
		<label>Od:</label>
		<select class='choose_od_h' style='width:40px; float:left;'>
			<?php for($i = 0; $i<24; $i++):?>
				<option value="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>" 	title="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>"><?php echo ((strlen($i)==1)?'0'.$i:$i);?></option>
			<?php endfor;?>
		</select><span style='display:block; width:5px; float:left; line-height:20px'>:</span>
		<select class='choose_od_m' style='width:40px'>
			<?php for($i = 0; $i<60; $i += 5):?>
				<option value="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>" 	title="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>"><?php echo ((strlen($i)==1)?'0'.$i:$i);?></option>
			<?php endfor;?>
		</select>
		<br/>
	</div>
	<div class='choose_od_div'>
		<label>Do:</label>
		<select class='choose_do_h' style='width:40px; float:left;'>
			<?php for($i = 0; $i<24; $i++):?>
				<option value="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>" 	title="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>"><?php echo ((strlen($i)==1)?'0'.$i:$i);?></option>
			<?php endfor;?>
		</select><span style='display:block; width:5px; float:left; line-height:20px'>:</span>
		<select class='choose_do_m' style='width:40px'>
			<?php for($i = 0; $i<60; $i += 5):?>
				<option value="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>" 	title="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>"><?php echo ((strlen($i)==1)?'0'.$i:$i);?></option>
			<?php endfor;?>
		</select>
		<br/>
	</div>
	<p>
		<input type='button' value='Vložte' class='choose_do_it'/>
	</p>
</div>
<?php echo $this->renderElement('../internal_employees/element_js');?>


