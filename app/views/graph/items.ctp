
<script type='text/javascript'>
      google.load('visualization', '1', {packages:["columnchart"]});
      google.setOnLoadCallback(drawChart);
      
        function drawChart(){
           drawWorkingHour();
            drawWorkingClient();
        }

      function drawWorkingHour() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Rok');
        data.addColumn('number', 'Odpracované hodiny');
    
        
        data.addRows(<?php echo count($graph_data_working_hour);?>);
        
        <?php foreach($graph_data_working_hour as $key=>$row):?>
        
        data.setValue(<?php echo $key;?>, 0, '<?php echo $row['ClientWorkingHour']['month'];?>. <?php echo $row['ClientWorkingHour']['year'];?>');
        data.setValue(<?php echo $key;?>, 1, <?php echo $row[0]['pocet'];?>);
        
        <?php endforeach;?>
        
        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, {width: 900, height: 400, min: 300, max: 1400, is3D: true, title: 'Počet odpracovaných hodin',legend:'none'});
      }

      function drawWorkingClient() {
        var data2 = new google.visualization.DataTable();
        data2.addColumn('string', 'Rok');
        data2.addColumn('number', 'Počet zaměstnanců');
    
        
        data2.addRows(<?php echo count($graph_data_working_clients);?>);
        
        <?php foreach($graph_data_working_clients as $key=>$row):?>
        
        data2.setValue(<?php echo $key;?>, 0, '<?php echo $row['ClientWorkingHour']['month'];?>. <?php echo $row['ClientWorkingHour']['year'];?>');
        data2.setValue(<?php echo $key;?>, 1, <?php echo $row[0]['pocet'];?>);
        
        <?php endforeach;?>
        
       

        var chart2 = new google.visualization.ColumnChart(document.getElementById('chart_div2'));
        chart2.draw(data2, {width: 900, height: 400, min: 300, max: 1400, is3D: true, title: 'Počet zaměstnanců',legend:'none'});
      }

    </script>
   
    <div id='chart_div' ></div>    
    <br />
    <div id='chart_div2' ></div>    
    <br />