<form action='/money_item_drawbacks/edit/' method='post' id='setting_career_edit_formular'>
	<?php echo $htmlExt->hidden('MoneyItemDrawback/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Nastavení</legend>
				<?php echo $htmlExt->input('MoneyItemDrawback/name',array('tabindex'=>1,'label'=>'Název', 'class'=>'long','label_class'=>'long'));?> <br class="clear">
			    <?php echo $htmlExt->selectTag('MoneyItemDrawback/typ',$list_type_of_drawback_and_bonus,null,array('tabindex'=>2,'label'=>'Typ','label_class'=>'long')); ?><br/> 
                <?php echo $htmlExt->input('MoneyItemDrawback/hodnota',array('tabindex'=>3,'label'=>'Hodnota', 'class'=>'integer long','label_class'=>'long'));?> <br/>
			    <?php echo $htmlExt->checkbox('MoneyItemDrawback/phm',null,array('tabindex'=>3,'label'=>'Prům. hod. mzda', 'class'=>'integer long','label_class'=>'long'));?> <br/>
			   
                <?php echo $htmlExt->selectTag('MoneyItemDrawback/money_item_connection_tool_id',$connection_tools,null,array('tabindex'=>5,'label'=>'Napojení na DB', 'class'=>'long','label_class'=>'long'));?> <br/>   
            </fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'}); 
    
    $('setting_career_edit_formular').getElements('.integer').inputLimit();

    
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('setting_career_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('setting_career_edit_formular').action,		
				onComplete:function(){
					click_refresh($('MoneyItemDrawbackId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('setting_career_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('setting_career_edit_formular',{
		'MoneyItemDrawbackName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'}
		}
	});
	validation.generate('setting_career_edit_formular',<?php echo (isset($this->data['MoneyItemDrawback']['id']))?'true':'false';?>);
</script>