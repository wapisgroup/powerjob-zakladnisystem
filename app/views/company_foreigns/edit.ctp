<form action='/company_foreigns/edit/' method='post' id='setting_career_edit_formular'>
	<?php echo $htmlExt->hidden('CompanyForeign/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Nastavení</legend>
				<?php echo $htmlExt->input('CompanyForeign/name',array('tabindex'=>1,'label'=>'Název', 'class'=>'long','label_class'=>'long'));?> <br class="clear">
			    <?php echo $htmlExt->selectTag('CompanyForeign/stat_id',$company_stat_list,null,array('label'=>'Stát'),null,false);?><br />
				
				<?php echo $htmlExt->input('CompanyForeign/ulice',	array('label'=>'Ulice'));?><br />
				<?php echo $htmlExt->input('CompanyForeign/mesto',	array('label'=>'Město'));?><br />
                <?php echo $htmlExt->input('CompanyForeign/psc',	array('label'=>'PSČ'));?><br />
				<?php echo $htmlExt->input('CompanyForeign/ico',	array('label'=>'IČO'));?><br />	
				<?php echo $htmlExt->input('CompanyForeign/dic',	array('label'=>'DIČ'));?><br />
                <?php echo $htmlExt->input('CompanyForeign/telefon',	array('label'=>'Telefon'));?><br />
                <?php echo $htmlExt->input('CompanyForeign/email',	array('label'=>'Email'));?><br />
            </fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('setting_career_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('setting_career_edit_formular').action,		
				onComplete:function(){
					click_refresh($('CompanyForeignId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('setting_career_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('setting_career_edit_formular',{
		'CompanyForeignName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'}
		},
        'CompanyForeignUlice': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit ulice'}
		},
        'CompanyForeignMesto': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit město'}
		},
        'CompanyForeignPsc': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit PSČ'}
		},
        'CompanyForeignIco': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit IČ'}
		},
        'CompanyForeignDic': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit DIČ'}
		},
        'CompanyForeignTelefon': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit telefon'}
		},
        'CompanyForeignEmail': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit email'}
		},
	});
	validation.generate('setting_career_edit_formular',<?php echo (isset($this->data['CompanyForeign']['id']))?'true':'false';?>);
</script>