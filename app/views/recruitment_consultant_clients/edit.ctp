<? if($clients): ?>
    <div style="margin:5px;">
        <div style="font-weight:bold;"><?= $recruiter['CmsUser']['name'] ?> (<?= $year.' - '.$month; ?>)</div>
        <span>celkem: <?= $counts[0]['celkem']; ?></span><span style="margin-left: 15px;">zaměstnaných: <?= $counts[0]['zam']; ?></span>
    </div>
    <table class="table">
    <tr>
        <th>Klient</th>
        <th>Společnost</th>
        <th>Pozice</th>
    </tr>
    <? foreach($clients as $client): ?>
        <tr><td><?= $client['Client']['name']; ?></td><td><?= $client[0]['company']; ?></td><td><?= $client[0]['position']; ?></tr>
    <? endforeach; ?>
    </table>
<? endif; ?>