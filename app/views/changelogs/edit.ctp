<form action='/changelogs/edit/' method='post' id='setting_career_edit_formular'>
	<?php echo $htmlExt->hidden('Changelog/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Nastavení</legend>
				<?php echo $htmlExt->input('Changelog/text',array('tabindex'=>1,'label'=>'Text'));?> <br class="clear">
				<?php echo $htmlExt->inputDate('Changelog/datum', array('label'=>'Datum'));?>
			</fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('setting_career_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('setting_career_edit_formular').action,		
				onComplete:function(){
					click_refresh($('ChangelogId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('setting_career_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('setting_career_edit_formular',{
		'ChangelogName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'}
		}
	});
	validation.generate('setting_career_edit_formular',<?php echo (isset($this->data['Changelog']['id']))?'true':'false';?>);
</script>