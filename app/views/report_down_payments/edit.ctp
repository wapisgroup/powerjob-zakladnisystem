<form action='/report_down_payments/edit/' method='post' id='report_down_payments_edit_formular'>
	<?php echo $htmlExt->hidden('ReportDownPayment/id');?>
	<?php echo $htmlExt->hidden('ReportDownPayment/cms_user_id');

?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Základní</legend>
				<div class="sll">  
					<?php if (isset($this->data['ReportDownPayment']['date_of_paid']) && $this->data['ReportDownPayment']['date_of_paid'] != '0000-00-00' && $logged_user['CmsGroup']['id'] != 1):?>
						<?php echo $htmlExt->var_text('Company/name',array('tabindex'=>1,'label'=>'Firma'));?> <br class="clear">
						<?php echo $htmlExt->var_text('Client/name',array('tabindex'=>2,'label'=>'Zaměstnanec'));?> <br class="clear">
						<?php echo $htmlExt->var_text('ReportDownPayment/amount',array('tabindex'=>4,'label'=>'Částka'));?> <br class="clear">
						<?php echo $htmlExt->var_text('ReportDownPayment/account',array('tabindex'=>3,'label'=>'Účet'));?> <br class="clear">
					    <?php echo $htmlExt->var_text('ReportDownPayment/date_of_paid',array('tabindex'=>5,'label'=>'Zaplaceno '));?> <br class="clear">(vyplňuje finanční účetní)
				
                    <?php else:?>
						<?php echo $htmlExt->selectTag('ReportDownPayment/company_id',$company_list,null,array('tabindex'=>1,'label'=>'Firma'));?> <br class="clear">
						<?php echo $htmlExt->selectTag('ReportDownPayment/client_id',$client_list,null,array('tabindex'=>2,'label'=>'Zaměstnanec'),array('title'=>$client_title_list));?> <br class="clear">
						<?php echo $htmlExt->input('ReportDownPayment/amount',array('tabindex'=>4,'label'=>'Částka'));?> <br class="clear">
						<?php echo $htmlExt->input('ReportDownPayment/account',array('tabindex'=>3,'label'=>'Účet','class'=>'read_info','disabled'=>'disabled'));?> <br class="clear">
					<?php endif;?>
				</div>
			</fieldset>
		</div>
	</div>
	
	<div class="win_save">
		<?php 
         if(!isset($this->data['ReportDownPayment']['id']) || in_array($logged_user['CmsGroup']['id'],array(1,6,7)))   
            echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>6));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>7));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'}); 

//if($('ReportDownPaymentCompanyId'))
//	$('ReportDownPaymentCompanyId').ajaxLoad('/report_down_payments/load_ajax_zamestnanci/',['ReportDownPaymentClientId']);


	// add event for change Profese and load list for calculations
    if($('ReportDownPaymentCompanyId'))
    	$('ReportDownPaymentCompanyId').addEvents({
    			'change': function(){
    					new Request.JSON({
    						url: '/report_down_payments/load_ajax_zamestnanci/' + this.value + '/',
    						onComplete: (function (json){
    							if (json){
    								if (json.result === true){
    									var sub_select = $('ReportDownPaymentClientId');
    									sub_select.empty();
    									new Element('option').setHTML('').inject(sub_select);
    									$each(json.data, function(item){
    										new Element('option',{value:item.Client.id,title:item.Client.cislo_uctu})
    											.setHTML(item.Client.name)
    											.inject(sub_select);
    									})
    									//this.currentSelection = this.selectedIndex;
    								} else {
    									this.options[this.currentSelection].selected = 'selected';
    									alert(json.message);
    								}
    							} else {
    								alert('Systemova chyba!!!');
    							}
    						}).bind(this)
    					}).send();
    			},
    			'focus': function(){
    				this.currentSelection = this.selectedIndex;
    			}
    	});
    
    if($('ReportDownPaymentClientId'))
    	$('ReportDownPaymentClientId').addEvent('change',function(e){
    		new Event(e).stop();
    		
    		if(this.value != 0)
    			$('ReportDownPaymentAccount').value = this.options[this.selectedIndex].title;
    	});

	if($('ReportDownPaymentClientId') && $('ReportDownPaymentId') != null && $('ReportDownPaymentClientId').value != 0 && ($('ReportDownPaymentAccount').value != '' || $('ReportDownPaymentAccount').value != 0))
		$('ReportDownPaymentAccount').value =  $('ReportDownPaymentClientId').options[$('ReportDownPaymentClientId').selectedIndex].title;


    if($('save_close'))
    	$('save_close').addEvent('click',function(e){
    		new Event(e).stop();
            
            	
    		valid_result = validation.valideForm('report_down_payments_edit_formular');
    		if (valid_result == true){
    		  $('ReportDownPaymentAccount').removeAttribute('disabled');
    	
    			new Request.JSON({
    				url:$('report_down_payments_edit_formular').action,		
    				onComplete:function(json){
    				    if(json.result == true){
    					   click_refresh($('ReportDownPaymentId').value);
    					   domwin.closeWindow('domwin');
                        }
                        else
                            alert('Chyba aplikace');
    
    				}
    			}).post($('report_down_payments_edit_formular'));
    		} else {
    			var error_message = new MyAlert();
    			error_message.show(valid_result)
    		}
    	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('report_down_payments_edit_formular',{
		'ReportDownPaymentClientId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit zaměstnance'},
		},
        'ReportDownPaymentAmount': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit částku'},
		}
	});
	validation.generate('report_down_payments_edit_formular',<?php echo (isset($this->data['ReportDownPayment']['id']))?'true':'false';?>);
</script>