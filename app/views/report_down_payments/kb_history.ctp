<div class="sll3">
 <fieldset>
				<legend>Historie Komerční banka</legend>
<div id="obal_tabulky">
				<table class='table' id='rating_table'>
					<tr>
						<th>#</th>
						<th>Vytvořeno</th>
						<th>Možnosti</th>
					</tr>
					<?php 
                    $i = 1;
					if (isset($file_list_kb) && count($file_list_kb) > 0){
						foreach($file_list_kb as $file):
                        list($date,$time) = explode('_',$file);
						?>
						<tr>
							<td><?php echo $i;?></td>
							<td><?php echo String::insert(':den.:mesic.:rok :hod::min::sec', array(
                                    'den' => substr($date,-2), 
                                    'mesic' => substr($date,-4,2), 
                                    'rok' => substr($date,0,4), 
                                    'hod' => substr($time,0,2), 
                                    'min' => substr($time,2,2), 
                                    'sec' => substr($time,4,2), 
                                ));;?>
                            </td>
                            <td><a title='Stáhnout znovu'class='ta download' href='/report_down_payments/download/kb/<?php echo $file;?>'/>download</a></td>
						</tr>
					<?php       
                        $i++;
						endforeach;
					}
					else echo "<tr id='NoneSearch'><td></td>Žádná historie pro Komerční banku</td></tr>"; 
					?>
					</table></div> 
				<br />
			</fieldset>
</div>            
<div class="sll3">            
<fieldset >
				<legend>Historie Slovenská spořitelna</legend>
<div id="obal_tabulky">
				<table class='table' id='rating_table'>
					<tr>
						<th>#</th>
						<th>Vytvořeno</th>
						<th>Možnosti</th>
					</tr>
					<?php 
                    $i = 1;
					if (isset($file_list_ss) && count($file_list_ss) > 0){
						foreach($file_list_ss as $file):
                        list($date,$time) = explode('_',$file);
						?>
						<tr>
							<td><?php echo $i;?></td>
							<td><?php echo String::insert(':den.:mesic.:rok :hod::min::sec', array(
                                    'den' => substr($date,-2), 
                                    'mesic' => substr($date,-4,2), 
                                    'rok' => substr($date,0,4), 
                                    'hod' => substr($time,0,2), 
                                    'min' => substr($time,2,2), 
                                    'sec' => substr($time,4,2), 
                                ));;?>
                            </td>
                            <td><a title='Stáhnout znovu'class='ta download' href='/report_down_payments/download/ss/<?php echo $file;?>'/>download</a></td>
						</tr>
					<?php       
                        $i++;
						endforeach;
					}
					else echo "<tr id='NoneSearch'><td></td>Žádná historie pro Slovenskou spořitelnu</td></tr>"; 
					?>
					</table></div> 
				<br />
			</fieldset>
</div>   
<div class="sll3">            
<fieldset >
				<legend>Historie Tatra banka</legend>
<div id="obal_tabulky">
				<table class='table' id='rating_table'>
					<tr>
						<th>#</th>
						<th>Vytvořeno</th>
						<th>Možnosti</th>
					</tr>
					<?php 
                    $i = 1;
					if (isset($file_list_tatra) && count($file_list_tatra) > 0){
						foreach($file_list_tatra as $file):
                        list($date,$time) = explode('_',$file);
						?>
						<tr>
							<td><?php echo $i;?></td>
							<td><?php echo String::insert(':den.:mesic.:rok :hod::min::sec', array(
                                    'den' => substr($date,-2), 
                                    'mesic' => substr($date,-4,2), 
                                    'rok' => substr($date,0,4), 
                                    'hod' => substr($time,0,2), 
                                    'min' => substr($time,2,2), 
                                    'sec' => substr($time,4,2), 
                                ));;?>
                            </td>
                            <td><a title='Stáhnout znovu'class='ta download' href='/report_down_payments/download/tatra/<?php echo $file;?>'/>download</a></td>
						</tr>
					<?php       
                        $i++;
						endforeach;
					}
					else echo "<tr id='NoneSearch'><td></td>Žádná historie pro Tatra banku</td></tr>"; 
					?>
					</table></div> 
				<br />
			</fieldset>
</div>                     
<br />                     