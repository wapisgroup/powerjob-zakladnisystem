<form action="/mv_orders/edit/" method="post" id='opp_order_form'>
<?php echo $htmlExt->hidden('MvOrder/id');?>
<table class="table tabulka">
	<tr>
		<th>Druh</th>
		<th>Typ</th>
		<th>Velikost</th>
		<th>Objednáno</th>
        <th>Zbývá</th>
		<th>Cena CZ</th>
        <th>Cena EU</th>
        <th>Podnik</th>
	</tr>
	<tbody id='sub_table'>
		<?php if (isset($this->data['MvOrderItem']) && count($this->data['MvOrderItem']) > 0):?>
		<?php
         foreach($this->data['MvOrderItem'] as $k => $itm):?>
		<tr>
			<td><?php echo $htmlExt->var_text('MvOrderItem/'.$k.'/type_id',$type_list, null, array('class'=>'select_type CE max_size'));?></td>
			<td><?php echo $htmlExt->var_text('MvOrderItem/'.$k.'/name',$names[$itm['type_id']], null, array('class'=>'select_name CE max_size'));?></td>
			<td><?php echo $htmlExt->var_text('MvOrderItem/'.$k.'/count_order',array('class'=>'input_count'));?></td>
			<td><?php echo $htmlExt->var_text('MvOrderItem/'.$k.'/count',array('class'=>'input_count'));?></td>
            <td><?php echo $htmlExt->var_text('MvOrderItem/'.$k.'/price_cz',array('class'=>'input_price_cz','readonly'=>'readonly'));?></td>
            <td><?php echo $htmlExt->var_text('MvOrderItem/'.$k.'/price_eu',array('class'=>'input_price_eu','readonly'=>'readonly'));?></td>
			<td><?php echo $company_list[$itm['company_id']];?></td>
		</tr>
		<?php endforeach;?>
		<?php endif;?>
	</tbody>
</table>

<div class='win_save'>
	<input type="button" id='close_order' value='Zavřít' />
</div>
</form>
<script language="javascript">
	$('close_order').addEvent('click', function(e){
		e.stop();
		domwin.closeWindow('domwin');	
	})
</script>