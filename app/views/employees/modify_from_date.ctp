<form action='/employees/modify_from_date/' method='post' id='formular'>
   <?php echo  $htmlExt->hidden('ConnectionClientRequirement/id');?>
   <?php echo  $htmlExt->hidden('Client/id',array('value'=>$client_id));?>

    <fieldset>
		<legend>Datum nástupu</legend>
		<?php echo $htmlExt->inputDate('ConnectionClientRequirement/from',array('tabindex'=>1,'label'=>'Datum nástupu','class'=>'','label_class'=>'long'));?> <br class="clear">		
		<?php echo $htmlExt->inputDate('ConnectionClientRequirement/to',array('tabindex'=>1,'label'=>'Datum ukončení','class'=>'','label_class'=>'long'));?> <a href="#" id="clear_to_date">Vymazat datum</a><br class="clear">		
    </fieldset>
    <fieldset>
		<legend>Ostatní connection</legend>
        <table class="table">
        <tr>
            <th>Firma</th>
            <th>Datum nástupu</th>
            <th>Datum ukončení</th>
        </tr>
        <?php 
        foreach($conection_list as $conn){
           echo '<tr>';
           echo '<td>'.$conn['Company']['name'].'</td>'; 
           echo '<td>'.$fastest->czechDate($conn['ConnectionClientRequirement']['from']).'</td>'; 
           echo '<td>'.$fastest->czechDate($conn['ConnectionClientRequirement']['to']).'</td>'; 
           echo '</tr>';
        }?>
        </table>
    </fieldset>  
    <br />  

    <div class='formular_action'>
		<input type='button' value='Změnit' id='modify_save' />
		<input type='button' value='Zavřít' id='modify_close'/>
	</div>
</form>
<script>
	$('modify_close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_zamestnanci_hodiny');});
	$('modify_save').addEvent('click',function(e){
		new Event(e).stop();

        if($('calbut_ConnectionClientRequirementFrom').value == ''){ 
			alert("Musite zvolit datum nástupu");
        }
        else{
                //odeslani requestu
    			new Request.JSON({
    				url:$('formular').action,		
    				onComplete:function(json){
    					if (json){
    						if (json.result === true){
                                click_refresh($('ConnectionClientRequirementId').value); 
    							domwin.closeWindow('domwin_zamestnanci_hodiny');
    						} else {
    							alert(json.message);
    						}
    					} else {
    						alert('Chyba aplikace');
    					}
    				}
    			}).post($('formular'));
       } 
  
	})
   
   
  $('clear_to_date').addEvent('click',function(e){
    e.stop();
    $('calbut_ConnectionClientRequirementTo').value = '';
  })   

     
</script>	