<form action='/employees/add_down_payments/' method='post' id='add_down_payments'>
            <?php echo $htmlExt->hidden('exception');?>
            <?php echo $htmlExt->hidden('ReportDownPayment/year');?>
            <?php echo $htmlExt->hidden('ReportDownPayment/month');?>
            <?php echo $htmlExt->hidden('ReportDownPayment/client_id');?>
            <?php echo $htmlExt->hidden('ReportDownPayment/company_id');?>
            <?php echo $htmlExt->hidden('ReportDownPayment/celkem_hodin');?>
            <?php echo $htmlExt->hidden('ReportDownPayment/connection_client_requirement_id');?>
            <?php echo $htmlExt->hidden('ReportDownPayment/cms_user_id');?>
            <?php echo $htmlExt->hidden('save',array('value'=>1));?>
            <?php echo $htmlExt->hidden('ReportDownPayment/type',array('value'=>$type));?>
			<fieldset>
				<legend><?php echo($type == 0 ? 'Požadavek na zálohu' : 'Požadavek na výplatu v hotovosti')?></legend>
					<?php echo $htmlExt->input('ReportDownPayment/amount',array('tabindex'=>1,'label'=>'Částka','class'=>'integer small','label_class'=>''));?> <br class="clear">
					<?php echo '<label>Typ:</label>'.$htmlExt->radio('ReportDownPayment/type', $down_payments_type, ' ', array('class' => 'radio','value'=>0)); ?><br />
                   
            </fieldset>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditClientMessageSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditClientMessageClose'/>
	</div>
</form>
<script>
    var limit = <?php echo (isset($this->data['limit']) && $this->data['limit'] != null ? $this->data['limit'] : 0)?>;
    var mena = (limit == 100 ? "€" : 'Kč');
    $('add_down_payments').getElements('.integer').inputLimit();
   

    $('AddEditClientMessageClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_down_payments');});
	
	$('AddEditClientMessageSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('add_down_payments');

        
        /**
         * pozadovana castka nesmi presahnout castku v celkem mzda v dochazce
         */
        if($('ReportDownPaymentAmount').value > limit) 
			if(valid_result == true)
				valid_result = Array("Částka přesahuje nastavený limit "+limit+",- "+mena);
			else
				valid_result[valid_result.length]="Částka přesahuje nastavený limit "+limit+",- "+mena;


        
		if (valid_result == true){
		    button_preloader($('AddEditClientMessageSaveAndClose'));  
			new Request.JSON({
				url:$('add_down_payments').action,		
				onComplete:function(json){
    				  if(json){  
    				    if(json.result == true){
                           alert('Požadavek byl přidán.');
                           text = $('payment_list_box_items').innerHTML;
                           
                           new_tr = "<?php echo '<strong>'.date('d.m.Y').'</strong>&nbsp;&nbsp;&nbsp;'.$logged_user['CmsUser']['name'];?>";
                           new_tr += '&nbsp;&nbsp;&nbsp;částka - '+$('ReportDownPaymentAmount').value;
                           new_tr += '&nbsp;&nbsp;&nbsp;hodin - '+$('ReportDownPaymentCelkemHodin').value;
                           new_tr += '<br class="clear_left"/>';
                           
                           $('ChangeMaxZalohyNow').value -= ($('ReportDownPaymentAmount').value).toFloat();
                           
                           $('payment_list_box_items').setHTML(text + new_tr);
                           domwin.closeWindow('domwin_down_payments');
                               
                        }
                        else
                           alert(json.message);
                           
                     }
                     else {
                        alert('Chyba aplikace.');
                     }
                     
                     button_preloader_disable($('AddEditClientMessageSaveAndClose'));         
				}
			}).post($('add_down_payments'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});

	validation.define('add_down_payments',{
		'ReportDownPaymentAmount': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit částku'},
		}
	});

	validation.generate('add_down_payments',false);
    
</script>