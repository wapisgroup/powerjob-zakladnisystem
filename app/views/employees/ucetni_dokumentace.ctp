<form action='/employees/ucetni_dokumentace/' method='post' id='company_work_position_edit_formular'>
	<?php echo $htmlExt->hidden('ClientUcetniDokumentace/id');?>
	<?php echo $htmlExt->hidden('ClientUcetniDokumentace/connection_client_requirement_id');?>
	<?php echo $htmlExt->hidden('ClientUcetniDokumentace/forma');?>
	<?php echo $htmlExt->hidden('ClientUcetniDokumentace/stav',array('value'=>(!isset($this->data['ClientUcetniDokumentace']['id']) ? 0 : $this->data['ClientUcetniDokumentace']['stav'] ) ));?>
	<fieldset id="forma_odmeny_dokumentace">
		<legend>Učetní dokumentace k formě odměny <?php echo $forma;?></legend>
		
	    <div class="sll">
            <fieldset>
            <legend>Povinné</legend>
            <?php 
            if($forma == '1000' || $forma =='1100' || $forma =='1001'){ 
                echo $htmlExt->checkbox('ClientUcetniDokumentace/dokumenty/ps_pracovni_smlouva',null,array('label'=>'Pracovni smlouva','class'=>'require')).'<br />';
                echo $htmlExt->checkbox('ClientUcetniDokumentace/dokumenty/ps_pokyn',null,array('label'=>'Pokyn','class'=>'require')).'<br />';
                echo $htmlExt->checkbox('ClientUcetniDokumentace/dokumenty/ps_cislo_uctu',null,array('label'=>'Cislo uctu','class'=>'require')).'<br />';
                echo $htmlExt->checkbox('ClientUcetniDokumentace/dokumenty/ps_dotaznik',null,array('label'=>'Dotazník','class'=>'require')).'<br />';
            }
            else if($forma == '0100' || $forma =='0101'){ 
                 echo $htmlExt->checkbox('ClientUcetniDokumentace/dokumenty/d_dohoda',null,array('label'=>'Dohoda','class'=>'require')).'<br />';
                 echo $htmlExt->checkbox('ClientUcetniDokumentace/dokumenty/d_cislo_uctu',null,array('label'=>'Cislo uctu','class'=>'require')).'<br />';
            }
            else if($forma == '0010'){ 
                 echo $htmlExt->checkbox('ClientUcetniDokumentace/dokumenty/z_smlouva_o_dilo',null,array('label'=>'Smlouva o dílo','class'=>'require')).'<br />';
            } 
            else if($forma == '0001'){ 
                 echo $htmlExt->checkbox('ClientUcetniDokumentace/dokumenty/c_dotaznik_pro_nabor',null,array('label'=>'Dotazník pro nábor','class'=>'require')).'<br />';
            }     
            ?>
            </fieldset>
        </div>
        <div class="slr">
            <fieldset>
            <legend>Nepovinné</legend>
            <?php 
            if($forma == '1000' || $forma =='1100' || $forma =='1001'){
                echo $htmlExt->checkbox('ClientUcetniDokumentace/dokumenty/ps_zapoctovy_list',null,array('label'=>'Zapoctovy list','class'=>'accommodation_day')).'<br />';
                echo $htmlExt->checkbox('ClientUcetniDokumentace/dokumenty/ps_prohlaseni_k_dani',null,array('label'=>'Prohlaseni k dani','class'=>'accommodation_day')).'<br />';
                echo $htmlExt->checkbox('ClientUcetniDokumentace/dokumenty/ps_dodatek_k_ps',null,array('label'=>'Dodatek k PS','class'=>'accommodation_day')).'<br />';
            }
            ?>
            </fieldset>
        </div>
        <div id="ukonceni_pp" class="sll none">
            <fieldset>
            <legend>Povinné</legend>
              <?php
               echo $htmlExt->checkbox('ClientUcetniDokumentace/fire_stav',null,array('label'=>'Ukončení pracovního poměru')).'<br />';
               ?>
            </fieldset>
        </div>
	</fieldset>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditCompanyWorkPositionSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyWorkPositionClose'/>
	</div>
</form>
<script>
var only_show = <?php echo (isset($only_show) && $only_show!="") ? $only_show : 'false';?>;
var only_show_ukonceni = <?php echo (isset($only_show_ukonceni) && $only_show_ukonceni!="") ? $only_show_ukonceni : 'false';?>;
var ukonceni_pracovniho_pomeru = <?php echo (isset($ukonceni_pracovniho_pomeru) && $ukonceni_pracovniho_pomeru!="") ? $ukonceni_pracovniho_pomeru : 'false';?>;


$('AddEditCompanyWorkPositionClose').addEvent('click',function(e){
	   new Event(e).stop(); 
       domwin.closeWindow('domwin_zamestnanci_hodiny');
});

// pouze zobrazeni a zamezit editaci
    if(only_show){
    	$('forma_odmeny_dokumentace').getElements('input,select,textarea,checkbox').setProperty('disabled','disabled');
    	$('AddEditCompanyWorkPositionSaveAndClose').addClass('hidden');
    	$('AddEditCompanyWorkPositionClose').removeProperty('disabled');
    }

if(ukonceni_pracovniho_pomeru){
    if(!only_show_ukonceni){
        $('AddEditCompanyWorkPositionSaveAndClose').removeClass('hidden');
        $('AddEditCompanyWorkPositionSaveAndClose').removeProperty('disabled');
        $('ukonceni_pp').removeClass('none');
        $('ClientUcetniDokumentaceFireStav').removeProperty('disabled');
    }
}
else{    	
        $('company_work_position_edit_formular').getElements('.require').addEvent('click',function(e){
            var all_checked = true;
            if(this.checked == true){
                $$('.require').each(function(item){
                    if(!item.checked)
                        all_checked = false;
                });
                
                if(all_checked)
                    $('ClientUcetniDokumentaceStav').value = 1;
            }
            else
                $('ClientUcetniDokumentaceStav').value = 0; 
        }); 
}

        //save je pro dochazku i pro report propustenych zamestnancu stejny
	   $('AddEditCompanyWorkPositionSaveAndClose').addEvent('click',function(e){
    		new Event(e).stop();
    		button_preloader($('AddEditCompanyWorkPositionSaveAndClose'));
            new Request.JSON({
    		  url:$('company_work_position_edit_formular').action,		
    		  onComplete:function(JSON){
    		        button_preloader_disable($('AddEditCompanyWorkPositionSaveAndClose'));
                    click_refresh($('ClientUcetniDokumentaceConnectionClientRequirementId').value);
                    domwin.closeWindow('domwin_zamestnanci_hodiny');
    		  }
    		}).post($('company_work_position_edit_formular'));
    	
    	});
	
</script>