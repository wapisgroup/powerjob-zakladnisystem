<form action="/opp_confirm_orders/edit/" method="post" id='opp_order_form'>
<?php echo $htmlExt->hidden('OppOrder/id');?>
<table class="table tabulka">
	<tr>
		<th>Druh</th>
		<th>Typ</th>
		<th>Velikost</th>
		<th>Pocet</th>
        <th>Obj. Počet</th>
		<th>Cena CZ</th>
        <th>Cena EU</th>
		<th>Potvrzeni</th>
	</tr>
	<tbody id='sub_table'>
		<?php if (isset($this->data['OppOrderItem']) && count($this->data['OppOrderItem']) > 0):?>
		<?php foreach($this->data['OppOrderItem'] as $k => $itm):?>
		<tr>
			<td><?php echo $htmlExt->selectTag('OppOrderItem/'.$k.'/type_id',$type_list, null, array('class'=>'select_type CE max_size','disabled'=>'disabled'));?></td>
			<td><?php echo $htmlExt->selectTag('OppOrderItem/'.$k.'/name',$names[$itm['type_id']], null, array('class'=>'select_name CE max_size','disabled'=>'disabled'));?></td>
			<td><?php echo $htmlExt->selectTag('OppOrderItem/'.$k.'/size',$sizes[$itm['name']], null, array('class'=>'select_size CE max_size','disabled'=>'disabled'));?></td>
			<td><?php echo $htmlExt->input('OppOrderItem/'.$k.'/count',array('class'=>'input_count','disabled'=>'disabled'));?></td>
		    <td><?php echo $htmlExt->input('OppOrderItem/'.$k.'/count_order',array('class'=>'input_count','disabled'=>'disabled'));?></td>
        	<td><?php echo $htmlExt->input('OppOrderItem/'.$k.'/price_cz',array('class'=>'input_price','readonly'=>'readonly','disabled'=>'disabled'));?></td>
			<td><?php echo $htmlExt->input('OppOrderItem/'.$k.'/price_eu',array('class'=>'input_price','readonly'=>'readonly','disabled'=>'disabled'));?></td>
            <td><?php echo $htmlExt->var_text('OppOrderItem/'.$k.'/stav',array('show_type'=>'checkbox','disabled'=>'disabled'));?></td>
		</tr>
		<?php endforeach;?>
		<?php endif;?>
	</tbody>
</table>
<div class='win_save'>
	<?php if ($this->data['OppOrder']['status'] != 1 && (isset($permission['access_confirm_order']) && $permission['access_confirm_order'] == 1)):?>
	<input type="button" id='save_order' value="Potvrdit objednávku" />
	<?php endif;?>
    <?php if (isset($permission['access_is_prepare']) && $permission['access_is_prepare'] == 1):?>
    <input type="button" id='prepare_order' value='Nachystané' />
    	<?php endif;?>
    <?php echo $html->link('Vytisknout','/opp_confirm_orders/print_prehled/'.$this->data['OppOrder']['id'].'/',array('class'=>'mr5 button','target'=>'_blank'));?>
	<input type="button" id='close_order' value='Zavřít' />
</div>
</form>
<script language="javascript">
	$('close_order').addEvent('click', function(e){
		e.stop();
		domwin.closeWindow('domwin');	
	})
    
    if($('prepare_order'))
    $('prepare_order').addEvent('click', function(e){
		e.stop();
		if(confirm('Chcete odeslat notifikaci o nachystané objednávce, danému uživateli?')){
		      new Request.JSON({
				url: '/opp_confirm_orders/prepare_notification/'+$('OppOrderId').value,
				onComplete: function(json){
					if (json){
						if (json.result === true){
							alert('Notifikace odeslána.');
						} else {
							alert(json.message);
						}
					} else {
						alert('Chyba aplikace');
					}
				}
			}).send();
		}	
	})
    
    if($('OppOrderId').value != '')
        change_size_value_to_rel(); //pri zacatku editaci nastav spravne selecty
        
    function change_size_value_to_rel(){
        $each($('sub_table').getElements('tr').getElement('select.select_size'),function(sel){
            $each(sel.options,function(opt){
                     if(opt.value != ""){ 
                        val_array = opt.title.split('|');
                        opt.title = val_array[0];
                        opt.setHTML(val_array[0]);
                        opt.setProperty('rel',val_array[1]); 
                     }   
            });
        })
    }

	if($('save_order'))
		$('save_order').addEvent('click', function(e){
			e.stop();
			new Request.JSON({
				url: $('opp_order_form').getProperty('action'),
				onComplete: function(json){
					if (json){
						if (json.result === true){
							alert('Objednávka byla přijata');
							domwin.closeWindow('domwin');
							click_refresh();
						} else {
							alert(json.message);
						}
					} else {
						alert('Chyba aplikace');
					}
				}
			}).send($('opp_order_form'));
		})

	
</script>