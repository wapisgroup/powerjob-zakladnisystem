<div>
<?php echo $htmlExt->hidden('connection_company_to_authorization_id',array('value' => $id));?>
</div>	
<form action='' method='post' id='setting_activity_edit_formular'>
		
	<div class="domtabs admin2_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Seznam Zaměstnanců</a></li>
		</ul>
</div>
	<div class="domtabs admin2_dom">
		<div class="domtabs field">
			<fieldset>
                <p><strong>Forma odměny formát :</strong> XXXX (Ubytování Ano/Ne - Doprava Ano/Ne - Stravenky Ano/Ne</p>
				<legend>Zaměstnanci</legend>
<div id="obal_tabulky">
				<table class='table' id='rating_table'>
					<tr>
						<th>Zaměstnanec</th>
						<th>Forma odměny</th>
						<th>Nástup</th>
						<th>Ukončení</th>
						<th>Počet hodin</th>
						<th class="text_center">PS-D-ZL-C</th>
						<th>Srážka</th>
						<th>Odměna1</th>
						<th>Odměna2</th>
						<th>Celk. výplata</th>
						<th>Výplata</th>
						<th>Možnosti</th>
					</tr>
					<?php 
					if (isset($zamestnanci_list) && count($zamestnanci_list) > 0){
						foreach($zamestnanci_list as $zam):	
						?>
						<tr>
							<td class='td_client'><?php echo $zam['Client']['name'];?></td>
							<td><?php echo $zam['CompanyMoneyItem']['name'];?> 
                                (<?php echo $fastest->value_to_yes_no($zam['CompanyMoneyItem']['cena_ubytovani_na_mesic'],'short');?> -
                                <?php echo $fastest->value_to_yes_no($zam['CompanyMoneyItem']['doprava'],'short');?> -
                                <?php echo $fastest->value_to_yes_no($zam['CompanyMoneyItem']['stravenka'],'short');?>) 
                            </td>
							<td><?php echo $fastest->czechDate($zam['ConnectionClientRequirement']['from']);?></td>
							<td><?php echo $fastest->czechDate($zam['ConnectionClientRequirement']['to']);?></td>
							<td><?php echo $zam['ClientWorkingHour']['celkem_hodin'];?></td>
							<td>
								<?php echo $zam['ClientWorkingHour']['salary_part_1_p'];?> - 
								<?php echo $zam['ClientWorkingHour']['salary_part_2_p'];?> -
								<?php echo $zam['ClientWorkingHour']['salary_part_3_p'];?> -
								<?php echo $zam['ClientWorkingHour']['salary_part_4_p'];?>
							</td>
							<td><?php echo $zam['ClientWorkingHour']['drawback'];?></td>
							<td><?php echo $zam['ClientWorkingHour']['odmena_1'];?></td>
							<td><?php echo $zam['ClientWorkingHour']['odmena_2'];?></td>
							<td><?php echo $zam['ClientWorkingHour']['salary_per_hour_p'];?></td>
							<td><?php echo $ano_ne[$zam['ClientWorkingHour']['stop_payment']];?></td>
							
							<td>
							<a href="/employees/odpracovane_hodiny/<?php echo $zam['ClientWorkingHour']['connection_client_requirement_id'].'/'.$zam['ClientWorkingHour']['year'].'/'.($zam['ClientWorkingHour']['month']<10 ? '0'.$zam['ClientWorkingHour']['month'] : $zam['ClientWorkingHour']['month']); ?>" class="detail" title="Editovat položku">Detail</a>
							
							</td>	
						</tr>
					<?php 
						endforeach;
					}
					else echo "<tr id='NoneSearch'><td></td>Žádní zaměstnanci</td></tr>"; 
					?>
					</table></div> 
				<br />
				<p class='pp15'>
				<strong>Forma odměny:</strong><br />
					1xxx - pracovní smlouva <br />
					x1xx - dohoda <br />
					xx1x - faktura <br />
					xxx1 - abc <br />
				</p>
			</fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin2_dom'}); 

	$$('.detail').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_zamestnanci_hodiny',
			sizes		: [950,900],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false
		}); 
	});



	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_show');});
</script>