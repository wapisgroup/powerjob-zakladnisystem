<form action="/opp_templates/items/<?= $template_id?>/" method="post" id='opp_order_form'>
<table class="table tabulka">
	<tr>
		<th>Druh</th>
		<th>Typ</th>
		<th>Moznosti</th>
	</tr>
	<tbody id='sub_table'>
		<tr class="od">
			<td><?php echo $htmlExt->selectTag('OppTemplateItem/-1/type_id',$type_list, null, array('class'=>'select_type CE max_size'));?></td>
			<td><select class='select_name CE max_size' name='data[OppTemplateItem][-1][name]'><option>---</option></select></td>
			<td>
				<a href='#' class="ta add add_row" id='clone_row'>Add</a>
				<a href='#' class="ta delete delete_row none CE">Delete</a>
			</td>
		</tr>
		<?php if (isset($this->data['OppTemplateItem']) && count($this->data['OppTemplateItem']) > 0):?>
		<?php foreach($this->data['OppTemplateItem'] as $k => $itm):?>
		<tr>
			<td><?php echo $htmlExt->selectTag('OppTemplateItem/'.$k.'/type_id',$type_list, null, array('class'=>'select_type CE max_size'));?></td>
			<td><?php echo $htmlExt->selectTag('OppTemplateItem/'.$k.'/name',$names[$itm['type_id']], null, array('class'=>'select_name CE max_size'));?></td>
			<td>
				<a href='#' class="ta delete delete_row CE">Delete</a>
			</td>
		</tr>
		<?php endforeach;?>
		<?php endif;?>
	</tbody>
</table>
<div class='win_save'>
	<input type="button" id='save_order' value="Uložit" />
	<input type="button" id='close_order' value='Zavřít' />
</div>
</form>
<script language="javascript">
	$('close_order').addEvent('click', function(e){
		e.stop();
		domwin.closeWindow('domwin');	
	})


	if($('save_order'))
	$('save_order').addEvent('click', function(e){
		e.stop();
		//$('sub_table').getElement('tr').getElements('input, select').setProperty('disabled','disabled');
        
		new Request.JSON({
			url: $('opp_order_form').getProperty('action'),
			onComplete: function(json){
				if (json){
					if (json.result === true){
						alert('Položky byly uloženy');
						domwin.closeWindow('domwin');
						click_refresh();
					} else {
						alert(json.message);
					}
				} else {
					alert('Chyba aplikace');
				}
			}
		}).send($('opp_order_form'));
	})

	$$('.delete_row').addEvent('click', function(e){
		e.stop();
		this.getParent('tr').dispose();
	})

	function valid_row(){
		var tr = $('sub_table').getElement('tr');
		var output = Array();
		
		if (tr.getElement('.select_type').value == '')  output.push('Musite zvolit druh opp');
		if (tr.getElement('.select_name').value == '')  output.push('Musite zvolit typ opp');
		
		if (output.length > 0){
			var al = new MyAlert();
			al.show(output);
		} else {
			return true;
		}
	}

	$('clone_row').addEvent('click', function(e){
		e.stop();
		
		if (valid_row()){
			var tr = this.getParent('tr'), clone = tr.clone();
			$('sub_table').adopt(clone);
		
            clone.removeClass('od');
			clone.getElement('.add_row').dispose();
			clone.getElement('.delete_row').removeClass('none');
		
			clone.getElement('.select_type').ajaxLoad('/opp_orders/load_typ/',[clone.getElement('.select_name')]);
			clone.getElement('.select_name').ajaxLoad('/opp_orders/load_size/',[clone.getElement('.select_size')]);
			tr.getElement('.select_name').empty();
			 
			tr.getElements('.select_type').setValue('');
		
			var uni = uniqid();
			increase_name(clone,1,uni);
		}
	})
	
	function increase_name(parent,index,prefix){
 		parent.getElements('input, select').each(function(el){
 			atrs = el.name.split('][');
 			prt = atrs[index];
			atrs[index] = prefix;
 		 	el.name = atrs.join('][');
	 	})
 	}

	$$('.select_type').each(function(itm){
		itm.ajaxLoad('/opp_orders/load_typ/',[itm.getParent('tr').getElement('.select_name')]);
	});
	
</script>