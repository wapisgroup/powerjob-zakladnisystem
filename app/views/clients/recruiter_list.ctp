<div class="win_fce top">
<a href='/clients/recruiter_add/<?php echo $this->data['Client']['id'];?>' class='button edit' id='recruiter_add_new' title='Přidání recruitera'>Přidat recruitera</a>
</div>
<table class='table' id='recruiter_table'>
	<tr>
		<th>Jméno</th>
		<th>Přiřazen</th>
	</tr>
	<?php if (isset($recruiter_list) && count($recruiter_list) > 0):?>
		<?php foreach($recruiter_list as $recruiter):?>
		<tr>
			<td><?php echo $recruiter['CmsUser']['name'];?></td>
			<td><?php echo $fastest->czechDate($recruiter['ConnectionClientRecruiter']['created']);?></td>
		</tr>
		<?php endforeach;?>
	<?php endif;?>
</table> 
<script>	
	<?php if(isset($logged_user['CmsGroup']['permission']['clients']['add_recruiter']) && $logged_user['CmsGroup']['permission']['clients']['add_recruiter']==1){?>
		$('recruiter_add_new').addClass('none');
	<?php } ?>

	$('recruiter_add_new').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_recruiter_add',
			sizes		: [580,390],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			post_data	: {from:'editace'},
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>