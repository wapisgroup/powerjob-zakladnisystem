<?php
/**
 * pokud je nastaven duplicitni mod vloz view
 */
if($duplicity_domwin)
    echo $this->renderElement('../clients/duplicity/index'); 

if($only_show){
    $domtabs = 'admin_dom_show';
    $form_id = 'client_edit_formular_show';
}
else{
    $form_id = 'client_edit_formular';
    $domtabs = 'admin_dom';
}

?>
<div id="client_edit_<?php echo ($duplicity_domwin == true ? 'half' : 'full');?>">
<form action='/clients/edit/' method='post' id='<?php echo $form_id?>'>
	<?php echo $htmlExt->hidden('Client/id'); ?>
    <?php echo $htmlExt->hidden('old_mail'); ?>
    
    <!-- POUZE pro aktivity zmenu uctu -->
        <?php echo $htmlExt->hidden('old_cislo_uctu'); ?>
        <?php echo $htmlExt->hidden('new_cislo_uctu'); ?>
        <?php echo $htmlExt->hidden('change_cislo_uctu_now',array('value'=>0)); ?>
    
	<input type="hidden" name="data[NewConnection]" id="new_connection" value="0" />
	
	<div class="domtabs <?php echo $domtabs?>_links">
		<ul class="zalozky noprint">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
            <li class="ousko"><a href="#krok2">Číslo účtu</a></li>
            <li class="ousko"><a href="#krok3">Os. číslo</a></li>
			<?php if(isset($this->data['Client']['id'])) { ?>
				<?php if(!isset($only_show)): ?>
				<li class="ousko"><a href="#krok4">Recruiters</a></li>
				<?php endif; ?>
			
				<?php //if($admin == 1): ?>
				<li class="ousko"><a href="#krok5">Formuláře</a></li>
                <?php //endif; ?>
				<li class="ousko"><a href="#krok6">Aktivity</a></li>
				<li class="ousko"><a href="#krok7">Aktivity - edit</a></li>
			     
			<?php } ?>
            <? /*<li class="ousko"><a href="#krok8">Opp</a></li> */ ?>
			<li class="ousko"><a href="#krok9">Majetek</a></li>
            <li class="ousko"><a href="#krok10">Středisko</a></li>
		</ul>
	</div>
	<div class="domtabs <?php echo $domtabs?>">
		<div class="domtabs field">
            <?php
                if($at_interni == true){
                    echo "<div class='sll'>";
                        echo $htmlExt->var_text('forma',array('label'=>'Forma zaměstnání','value'=>$int_connection['AtCompanyMoneyItem']['name']));
                    echo "</div>";
                    echo "<div class='slr'>";
                        echo $htmlExt->var_text('exp',array('label'=>'Expirace smlouvy','value'=>$fastest->czechDate($int_connection['ConnectionClientAtCompanyWorkPosition']['expirace_smlouvy'])));
                    echo "</div>";
                    echo "<br />";
                } 
                
            ?>
			<fieldset>
				<legend>Osobní údaje</legend>
				<div class="sll">  
					<?php echo $htmlExt->input('Client/jmeno', array('tabindex' => 1, 'label' =>'Jméno')); ?><br />
					<?php echo $htmlExt->input('Client/prijmeni', array('tabindex' => 3,'label' => 'Příjmení',  'value'=>((isset($hidden) && $hidden == true) ? substr($this->data['Client']['prijmeni'],0,1) : $this->data['Client']['prijmeni'])  )); ?><br />
				    <?php echo $htmlExt->input('Client/rodne_cislo', array('tabindex' => 5, 'label' =>'Rodné číslo','class'=>'integer')); ?><br />
				    <?php echo $htmlExt->selectTag('Client/setting_pojistovna_id', $pojistovna_list, null, array('tabindex' => 5, 'label' =>'Pojišťovna','class'=>'')); ?><br />
				    <?php echo $htmlExt->selectTag('Client/stav', $stav_client_list, null, array('tabindex' => 5, 'label' =>'Stav','class'=>''/*,'disabled'=>'disabled'*/)); ?><br />
                </div>
				<div class="slr">  
					<?php echo $htmlExt->selectTag('Client/pohlavi_list', $pohlavi_list, null,array('tabindex' => 2, 'label' => 'Pohlaví')); ?><br />
					<?php echo $htmlExt->selectTag('Client/nationality', $nationality_list, null,array('tabindex' => 4, 'label' => 'Narodnost')); ?><br />
					<?php echo $htmlExt->input('Client/datum_narozeni', array('tabindex' =>4, 'label' => 'Datum narození','class'=>'medium')); ?><img src='/css/fastest/icons/calendar.png' id='filtr_datum_narozeni'/><br />
			       <?php echo $htmlExt->input('Client/cislo_op', array('tabindex' => 5, 'label' =>'Číslo OP', 'class'=>'')); ?><br />
    
            	</div>
			</fieldset>
            <fieldset class="<?if(isset($hidden) && $hidden == true) echo "noprint";?>">
				<legend>Kontaktní údaje</legend>
				<div class="sll">
                    <div class="sll75">
                    <p>Telefony uvádějte ve formátu (00420123456789)</p>
					<?php echo $htmlExt->input('Client/mobil1', array('tabindex' => 5, 'label' =>($at_interni == true ? 'Tel. soukromý':'Telefon'), 'class' => 'mobile integer', 'MAXLENGTH' => 14)); ?><br />
					<?php echo $htmlExt->input('Client/mobil2', array('tabindex' => 6, 'label' =>($at_interni == true ? 'Tel. firemní':'Telefon 2'), 'class' => 'mobile integer', 'MAXLENGTH' => 14)); ?><br />
					<?php echo $htmlExt->input('Client/mobil3', array('tabindex' => 7, 'label' =>($at_interni == true ? 'Tel. firemní 2':'Telefon 3'), 'class' => 'mobile integer', 'MAXLENGTH' => 14)); ?><br />
		              <br />
                    <?php echo $htmlExt->input('Client/email', array('tabindex' =>8, 'label' => 'Email')); ?><br />
                    <?php echo $htmlExt->input('Client/private_email', array('tabindex' =>8, 'label' => 'Soukromý email')); ?><br />
			
					<label>Souhlasí se zpracovanim údajů a kontaktováním:</label><?php echo $htmlExt->checkbox('Client/agree_contact', null, array()); ?><br/>
                    </div>
                    <div class="sll25">
                        <p>&nbsp;</p>
                        <?php echo $htmlExt->radio('Client/mobil_active', array('mobil1' => 'Platný','mobil2' => 'Platný', 'mobil3' => 'Platný'), '<br />', array('class' => 'radio')); ?><br />
                        <?php if(isset($this->data['Client']['id'])) { ?>
                            <p><br />
                                <a href='/clients/add_activity/<?php echo $this->data['Client']['id'];?>' class='button edit add_activity' title='Přidání novou aktivitu'>Přidat aktivitu</a>
                            </p>
                        <?php }?>
                    </div>
                </div>
                <div class="slr" id="addresses">
                    <div class="sll" >
                        <p><strong>Kontaktní adresa: </strong></p>
                        <?php echo $htmlExt->input('Client/ulice', array('tabindex' => 9, 'label' =>'Ulice')); ?><br />
                        <?php echo $htmlExt->input('Client/mesto', array('tabindex' => 10, 'label' =>'Město')); ?><br />
                        <?php echo $htmlExt->input('Client/psc', array('tabindex' => 11, 'label' =>'PSČ')); ?><br />
                        <?php echo $htmlExt->selectTag('Client/countries_id', $client_countries_list, null,array('tabindex' => 12, 'label' => 'Okres'), null, false); ?><br />
                        <?php echo $htmlExt->selectTag('Client/stat_id', $client_stat_list, null,array('tabindex' => 12, 'label' => 'Stát')); ?><br />
                    </div>
                    <div class="slr" >
                        <p><strong>Trvala adresa: </strong> <a href="#" id="copy_address" style="font-weight: bold;">Přenést</a></p>
                        <?php echo $htmlExt->input('Client/contact_ulice', array('tabindex' => 11, 'label' =>'Ulice')); ?><br />
                        <?php echo $htmlExt->input('Client/contact_mesto', array('tabindex' => 11, 'label' =>'Město')); ?><br />
                        <?php echo $htmlExt->input('Client/contact_psc', array('tabindex' => 11, 'label' =>'PSČ')); ?><br />
                        <?php echo $htmlExt->selectTag('Client/contact_countries_id', $client_countries_list, null,array('tabindex' => 11, 'label' => 'Okres'), null, false); ?><br />
                        <?php echo $htmlExt->selectTag('Client/contact_stat_id', $client_stat_list, null,array('tabindex' => 11, 'label' => 'Stát')); ?><br />

                    </div>
				</div>

			</fieldset>	
			<fieldset>
				<legend>Konfekce</legend>
				<div class="sll">
					<?php echo $htmlExt->input('Client/opp_nohavice', array('tabindex' => 5, 'label' =>'Velikost kalhoty')); ?><br />
					<?php echo $htmlExt->input('Client/opp_bluza', array('tabindex' => 5, 'label' =>'Velikost blůzy')); ?><br />
				</div>
				<div class="slr">
					<?php echo $htmlExt->input('Client/opp_tricko', array('tabindex' => 5, 'label' =>'Velikost trička')); ?><br />
					<?php echo $htmlExt->input('Client/opp_obuv', array('tabindex' => 5, 'label' =>'Velikost obuvi')); ?><br />
				</div>
				<br />
			</fieldset>	
            <fieldset>
				<legend>Kde má zájem pracovat</legend>
					<?php echo $htmlExt->input('Client/location_work', array('tabindex' => 5, 'label' =>'Lokalita')); ?><br />
				<br />
			</fieldset>	

			<fieldset>
				<legend>Pracovní zkušenosti</legend>
                <div class="noprint">
				<?php if(!isset($only_show)): ?>
					<div class="sll">
						<?php echo $htmlExt->selectTag('Client/kvalifikace_list', $kvalifikace_list, null,  array('tabindex' => 13, 'label' => 'Profese')); ?>
						<?php echo $htmlExt->input('CarrerItems/data_name', array('tabindex' => 14,  'label' => 'Název zaměstnavatele')); ?><br />
				        <?php echo $htmlExt->input('CarrerItems/data_od', array('tabindex' => 16,  'label' => 'Období od')); ?>
						<?php echo $htmlExt->input('CarrerItems/data_do', array('tabindex' => 17,  'label' => 'Období do')); ?><br />
					    <label></label><input style="margin-top:10px; margin-left:3px;" type="button" value="Přidat pracovní zkušenost" tabindex="18" class="button" id="add_kvalifikace" />
					
                	</div>
					<div class="slr" >  
					  	<?php echo $htmlExt->textarea('CarrerItems/data_popis', array('tabindex' =>  15,'label' =>'Popis pracovní náplně', 'class' => 'long', 'rows' => 3)); ?><br/>
					
					</div><br />
				<br />
				<br />
                </div>
				<?php endif; ?>
					<table class="tabulka" id='kvalifikace_list'>
					<?php if(isset($kvalifikace_list_item)): ?>
						<?php foreach($kvalifikace_list_item as $kval): ?>
						<tr class="kvalifikace_row_<?php echo $kval['ConnectionClientCareerItem']['id'] ?>">	
							<td width="150px;">Kvalifikace:</td>
							<td><?php echo $kval['SettingCareerItem']['name']; ?><input type='hidden' name='data[Client][kvalifikace_list_item][]' value='<?php echo
    $kval['SettingCareerItem']['id']; ?>'/></td>
							<td>
								<?php if(!isset($only_show)): ?>
									<a href='#' rel="<?php echo $kval['ConnectionClientCareerItem']['id'] ?>" class='remove_from_kvalifikace_list'>Odstranit</a>
								<?php endif; ?>
							</td>
						</tr>
						<tr class="kvalifikace_row_<?php echo $kval['ConnectionClientCareerItem']['id'] ?>">
							<td>Název zaměstnavatele:</td>
							<td colspan="2"><?php echo $kval['ConnectionClientCareerItem']['name']; ?></td>
						</tr>
						<tr class="kvalifikace_row_<?php echo $kval['ConnectionClientCareerItem']['id'] ?>">
							<td>Období:</td>
							<td colspan="2"><strong>od</strong> <?php echo $kval['ConnectionClientCareerItem']['od']; ?> -
                                <strong>do</strong> <?php echo $kval['ConnectionClientCareerItem']['do']; ?></td>
						</tr>
						<tr class="kvalifikace_row_<?php echo $kval['ConnectionClientCareerItem']['id'] ?>">
							<td>Popis pracovní náplně:</td>
							<td colspan="2"><?php echo $kval['ConnectionClientCareerItem']['popis']; ?></td>
						</tr>
						<tr class="kvalifikace_row_<?php echo $kval['ConnectionClientCareerItem']['id'] ?>">
							<td colspan="3" class="line">
								<?php echo $htmlExt->hidden('ClientCarrerItems/' . $kval['ConnectionClientCareerItem']['id'] .
    '/name', array('value' => $kval['ConnectionClientCareerItem']['name'])); ?>
								<?php echo $htmlExt->hidden('ClientCarrerItems/' . $kval['ConnectionClientCareerItem']['id'] .
    '/popis', array('value' => $kval['ConnectionClientCareerItem']['popis'])); ?>
								<?php echo $htmlExt->hidden('ClientCarrerItems/' . $kval['ConnectionClientCareerItem']['id'] .
    '/od', array('value' => $kval['ConnectionClientCareerItem']['od'])); ?>
								<?php echo $htmlExt->hidden('ClientCarrerItems/' . $kval['ConnectionClientCareerItem']['id'] .
    '/do', array('value' => $kval['ConnectionClientCareerItem']['do'])); ?>
								<?php echo $htmlExt->hidden('ClientCarrerItems/' . $kval['ConnectionClientCareerItem']['id'] .
    '/setting_career_item_id', array('value' => $kval['SettingCareerItem']['id'])); ?>
							</td>
						</tr>
						<?php endforeach; ?>
					<?php endif; ?>
					</table>
			</fieldset>
			
			
	
			<fieldset>
				<legend>Certifikáty a zkoušky</legend>
                <div class="noprint">
			<?php if(!isset($only_show)): ?>
				<div class="sll">
						<?php echo $htmlExt->selectTag('CertifikatyItems/certifikaty_list', $certifikaty_list, null,  array('tabindex' => 19, 'label' => 'Název certifikátu')); ?>
						<?php echo $htmlExt->input('CertifikatyItems/certifikaty_platnost', array  ('tabindex' => 21, 'label' => 'Platný do', 'MAXLENGTH' => 4, 'class' =>
    'integer')); ?>
            	</div>
					<div class="slr" >  
							<label></label><input type="button" value="Přidat nový certifikát" class="button" tabindex="22" id="add_certifikat" />
				 </div><br />
				 <?php echo $htmlExt->input('CertifikatyItems/certifikaty_komentar', array('tabindex' =>  20, 'class' => 'long', 'label_class' => 'long', 'label' => 'Komentář')); ?><br />
				
				<br />
				<br />
                </div>
			<?php endif; ?>
					<table class="tabulka" id='certifikaty_list'>
					<?php if(isset($certifikaty_list_item)): ?>
						<?php foreach($certifikaty_list_item as $cert): ?>
						<tr class="kvalifikace_row_<?php echo $cert['ConnectionClientCertifikatyItem']['id'] ?>">	
							<td style="width:65px;">Typ:</td>
							<td style="width:30%"><?php echo $cert['SettingCertificate']['name']; ?></td>
							<td style="width:65px;"></td>
							<td>
								<?php if(!isset($only_show)): ?>
									<a href='#' rel="<?php echo $cert['ConnectionClientCertifikatyItem']['id'] ?>" class='remove_from_certifikaty_list'>Odstranit</a>
								<?php endif; ?>
							</td>
						</tr>
						
                        <tr class="kvalifikace_row_<?php echo $cert['ConnectionClientCertifikatyItem']['id'] ?>">
							<td style="width:65px;">Platný do:</td>
							<td><?php echo $cert['ConnectionClientCertifikatyItem']['platnost']; ?></td>
	                        <td colspan="2"></td>
                        </tr>
                        
						<tr class="kvalifikace_row_<?php echo $cert['ConnectionClientCertifikatyItem']['id'] ?>">
							<td style="width:65px;">Komentář:</td>
							<td style="width:30%;"><?php echo $cert['ConnectionClientCertifikatyItem']['komentar']; ?></td>
							<td colspan="2"></td>
						</tr>
                        
                    
						
						<tr class="kvalifikace_row_<?php echo $cert['ConnectionClientCertifikatyItem']['id'] ?>">
							<td colspan="4" class="line">
								<?php echo $htmlExt->hidden('CertifikatyItem/' . $cert['ConnectionClientCertifikatyItem']['id'] .
    '/komentar', array('value' => $cert['ConnectionClientCertifikatyItem']['komentar'])); ?>
								<?php echo $htmlExt->hidden('CertifikatyItem/' . $cert['ConnectionClientCertifikatyItem']['id'] .
    '/platnost', array('value' => $cert['ConnectionClientCertifikatyItem']['platnost'])); ?>
								<?php echo $htmlExt->hidden('CertifikatyItem/' . $cert['ConnectionClientCertifikatyItem']['id'] .
    '/list', array('value' => $cert['SettingCertificate']['id'])); ?>
							</td>
						</tr>
						<?php endforeach; ?>
					<?php endif; ?>
					</table>
			</fieldset>

			
			<fieldset>
				<legend>Vzdělání</legend>
					
				<div class="sll">  
					<?php echo $htmlExt->selectTag('Client/dosazene_vzdelani_list', $dosazene_vzdelani_list, null,
array('tabindex' => 23, 'label' => 'Dosažené vzdělání')); ?>
				</div>
				<div class='slr'>
					<?php echo $htmlExt->selectTag('Client/dosazene_vzdelani_obor', $kvalifikace_list, null,
array('tabindex' => 23, 'label' => 'Obor')); ?>
					
				</div>
				<br />
			</fieldset>
			<fieldset class="<?if(isset($hidden) && $hidden == true) echo "noprint";?>">
				<legend>Poznamka</legend>
					<?php echo $htmlExt->textarea('Client/poznamka', array('label' =>'Poznamka', 'class' => 'long', 'label_class' => 'long')); ?><br/>
					<?php echo $htmlExt->input('Client/spec_id', array('tabindex' => 8, 'label' =>'Speciální ID', 'class' => 'long', 'label_class' => 'long')); ?><br />
			</fieldset>
            <fieldset>
				<legend>Osobní pohovor</legend>
                    <?php echo $htmlExt->selectTag('Client/os_pohovor',$os_pohovor_list,null,array('tabindex' => 23, 'label' => 'Pohovor', 'class' => 'long', 'label_class' => 'long')); ?><br />
					<?php echo $htmlExt->textarea('Client/os_pohovor_poznamka', array('label' =>'Poznamka', 'class' => 'long', 'label_class' => 'long')); ?><br/>
			</fieldset>
			<fieldset class="<?if(isset($hidden) && $hidden == true) echo "noprint";?>">
				<legend>Stav importu</legend>
				<?php echo $htmlExt->selectTag('Client/import_stav', $stav_importu_list, null,array('label' => 'Stav importu', 'class' => 'long', 'label_class' => 'long')); ?>
			</fieldset>
            <? /*<fieldset class="<?if(isset($hidden) && $hidden == true) echo "noprint";?>">
				<legend>Informace o pracovním místě</legend>
                <div class="sll">
                <?php echo $htmlExt->input('InfoAboutJob/name', array('tabindex' => 30, 'label' =>'Zdroj informaci o prac. místě', 'class' => 'medium ', 'label_class' => '')); ?>
	            <a href='/clients/add_info_about_job/' class="ta add" id='add_info_about_job' title='Přidat bonus'></a><br />
               
                </div>
                <div class="slr">
				<?php echo $htmlExt->selectTag('Client/info_about_job', $info_about_job_list, null,array('class' => '')); ?>
			    </div><br /> 
            </fieldset> */ ?>
			<?php if(isset($this->data['Client']['id'])) { ?>
				<div class="krok">
					<a href='#krok2' class='admin_dom_move next'>Další krok</a>
				</div>
			<?php } ?> 
		</div>
		<div class="domtabs field <?if(isset($hidden) && $hidden == true) echo "noprint";?>">
			<?php echo $this->renderElement('../clients/tabs/cislo_uctu'); ?>
		</div>
        <div class="domtabs field <?if(isset($hidden) && $hidden == true) echo "noprint";?>">
			<?php echo $this->renderElement('../clients/dovolena'); ?>
		</div>
		<?php if(isset($this->data['Client']['id'])) { ?>
		<?php if(!isset($only_show)): ?>
		<div class="domtabs field noprint">
			<?php echo $this->renderElement('../clients/recruiter_list'); ?>
		</div>
		<?php endif; ?>
		
			<?php //if($admin == 1): ?>
			<div class="domtabs field noprint">
				<?php echo $this->renderElement('../clients/formulare'); ?>
			</div>
            <?php //endif; ?>
            <div class="domtabs field <?if(isset($hidden) && $hidden == true) echo "noprint";?>">
				<?php echo $this->renderElement('../clients/activity/index'); ?>
			</div>
            <div class="domtabs field noprint">
				<?php echo $this->renderElement('../clients/activity/index2'); ?>
			</div>
			
		<?php } ?>
       <? /*<div class="domtabs field noprint">
          <?php echo $this->renderElement('../client_employees/history_client_opp',array('close'=>false,'return'=>true,'items'=>$opp_items)); ?>  
       </div> */ ?>
       <div class="domtabs field noprint">
        <?php echo $this->renderElement('../clients/audit_estate'); ?>
       </div>
       <div class="domtabs field noprint">
        <?php echo $this->renderElement('../clients/stredisko'); ?>
       </div>	
	</div>
	<div class="win_save noprint">
        <?php 
        if(isset($this->data['Client']['id'])){ 
            echo $html->link('Tisk karty klienta','/clients/print_client_card/'.$this->data['Client']['id'],array('class'=>'mr5 button','target'=>'_blank'));
            echo $html->link('Slepá karta','/clients/print_client_card/'.$this->data['Client']['id'].'/1',array('class'=>'mr5 button','target'=>'_blank'));
            
             if(in_array($logged_user['CmsGroup']['id'],array(1)) && isset($only_show))
                echo $html->link('Smazat','/clients/trash/'.$this->data['Client']['id'],array('rel'=>$this->data['Client']['id'],'id'=>'client_trash','class'=>'mr5 button'));
             
            // if(in_array($logged_user['CmsGroup']['id'],array(1,9)))
                //echo $htmlExt->button('Poslat Sms', array('id' => 'sms_send', 'class' =>  'button'));
            
        }
		?>
        
        <?php if(!isset($only_show)): ?>
			<?php echo $htmlExt->button('Uložit', array('id' => 'save_close', 'class' =>  'button')); ?>&nbsp;
		<?php endif; ?>
		<?php echo $htmlExt->button('Zavřít', array('id' => 'close', 'class' =>'button')); ?>
	</div>
</form>
</div>
<?php $dataPocetProfesi = (isset($kvalifikace_list_item) ? count($kvalifikace_list_item) :0); ?>
<script language="javascript" type="text/javascript">
var form_id = '<?php echo $form_id;?>';
var parent_id = '<?php echo $parent_id;?>';
    
    if($('ClientId').value == ''){
        $('ClientAgreeContact').setProperty('checked','checked');
    }
    
    /**
     * pokud se jedna o parenta
     * vyhod informativni okno
     */
    if(parent_id > 0)
        alert('Jedná se o duplicitní kartu, editace těchto údaju proveďte v klientovi s ID: '+parent_id);
    
    <?php if(isset($print)): ?>
    $(form_id).getElements('input,select,textarea').each(function(item){

	//console.log(item.getNext().get('tag'));
	//alert(item.getNext().get('tag'));
		if(item.get('tag') == 'input'){
			if(item.getProperty('type') == 'text'){
				text = item.value;
				newtext = new Element('span').setHTML(text);
				newtext.inject(item,'after');
				item.dispose();
			}
			else{
			   if(item.getProperty('type') != 'hidden'){
				text = (item.checked ? ' Ano ' : ' Ne ');
				newtext = new Element('span').setHTML(text);
				newtext.inject(item,'after');
				item.dispose();
               } 
			}
		}
		else if(item.get('tag') == 'textarea'){
			text = item.value;
			newtext = new Element('p').setHTML(text);
			br = new Element('br');
			newtext.inject(item,'after');
			br.inject(item,'after');
			item.dispose();
		}
		else if(item.get('tag') == 'select'){		
			text = item.getOptionText();
			newtext = new Element('span').setHTML(text);
			newtext.inject(item,'after');
			item.dispose();
		}
		

	});

	window.print();
    <?php endif; ?>
    

	var domtab = new DomTabs({'className':'<?php echo $domtabs?>'}); 
	 $(form_id).getElements('.float, .integer').inputLimit();
     
     
     $$('.add_activity').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_message_add',
			sizes		: [580,390],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			post_data	: {from:'editace'},
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
    
    if($('sms_send'))   
     $('sms_send').addEvent('click',function(e){

		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_sms',
			sizes		: [1000,1000],
			scrollbars	: true,
			title		: 'Odeslání sms klientu '+$('ClientPrijmeni').value+' '+$('ClientJmeno').value ,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: '/sms/edit/null/1/'+$('ClientId').value,
			post_data	: {from:'editace'},
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});

	 
	$(form_id).getElements('.radio').addEvent('click',function(e){
        
		if($('Client' + ucfirst(this.value)).value == ''){
           e.stop();  
           alert('Hodnota telefonu je prázdna, telefon musí být vyplńen aby mohl být platný.');
        }
        else if($('Client' + ucfirst(this.value)).value.length < 14){
           e.stop();  
           alert('Telefon musí mít 14 znaků, aby mohl být platný.');
        }
	});
	 
	 
	if($('ClientStatId'))
		$('ClientStatId').ajaxLoad('/clients/load_ajax_okresy/',['ClientCountriesId']);

	if($('ClientContactStatId'))
		$('ClientContactStatId').ajaxLoad('/clients/load_ajax_okresy/',['ClientContactCountriesId']);
	
	var dataPocetProfesi = <?php echo $dataPocetProfesi; ?>;
	var pocetProfesi = ((dataPocetProfesi==0) ? 0 : dataPocetProfesi);

	/* VALIDATION CLIENT TO RECRUITER */
	
//	$('calbut_ClientDatumNarozeni').addEvent('change', function(e){
//		validuj_number();
//	});
//	
//	$('ClientMobil').addEvent('change', function(e){
//		validuj_number();
//	});
	
     $(form_id).getElements('.mobile').addEvent('change', function(e){
        e.stop();
        check_number(this.value);
     });
     
     $('ClientPrijmeni').addEvent('change', function(e){
        e.stop();
        check_surname(this.value);
     });
     
     
     // zjisteni zda je cislo jiz v db a zobrazeni seznamu u koho
     // by Sol
     function check_number(value){
        var clients_list = '';
        var i = 1;
        
        if (value == ''){
		
		} else if (value.length < 14){
			alert('Mobil je krátký - zadejte 14 znaků');
		} else {
		    new Request.JSON({
			    url:'/clients/check_number/'+value,		
				onComplete:function(json){
			     	if (json.result == true){		     	   
			     	   if(json.data != null)
                           $each(json.data,function(item){
                                clients_list += i+". "+item+"\n";
                                i++;
                           });
					   alert("Toto číslo "+value+" je již použito \nČíslo využívá :\n\n"+clients_list);
					}
				}
			}).send();		      
        }  
     }
     
      // zjisteni zda je prijmeni jiz v db a zobrazeni seznamu
     // by Sol
     function check_surname(value){
        var clients_list = '';
        var i = 1;
        
        if (value != ''){
		    new Request.JSON({
			    url:'/clients/check_surname/'+value,		
				onComplete:function(json){
			     	if (json.result == true){		     	   
			     	   if(json.data != null)
                           $each(json.data,function(item){
                                clients_list += i+". "+item+"\n";
                                i++;
                           });
					   alert("Toto přijmení "+value+" již existuje v systému.\nPřijmení se shoduje s:\n\n"+clients_list);
					}
				}
			}).send();		      
        }  
     }
     
	//nepouziva se - od 26.10.2009
	function validuj_number(){
		if ($('ClientMobil').value == ''){
		
		} else if ($('ClientMobil').value.length<12){
			alert('Mobil je krátký - zadejte 12 znaků');
		} else {
			<?php if(!isset($this->data['Client']['id'])) { ?>
			var url = '/clients/valid_requiter/'+$('ClientMobil').value;
			if ($('calbut_ClientDatumNarozeni').value != '')
				url += ('/' + $('calbut_ClientDatumNarozeni').value);
			new Request.JSON({
					url:url,		
					onComplete:function(json){
						if (json.result === 1){
							if (confirm('Klient ' + json.client.Client.name + ' již existuje, chcete nahrát jeho údaje ?')){
								
								$('ClientJmeno').removeClass('require');
								$('ClientPrijmeni').removeClass('require');
								$('ClientMobil').setProperty('readonly');
								//$('ClientPrijmeni').removeClass('require');
								
								
								$('ClientId').value=json.client.Client.id;	
								$('ClientJmeno').value=json.client.Client.jmeno;	
								$('ClientPrijmeni').value=json.client.Client.prijmeni;	
								$('ClientPohlaviList').value=json.client.Client.pohlavi_list;	
								$('calbut_ClientDatumNarozeni').value=json.client.Client.datum_narozeni;	
								$('ClientEmail').value=json.client.Client.email;	
								$('ClientTelefon1').value=json.client.Client.telefon1;	
								$('ClientTelefon2').value=json.client.Client.telefon2;	
								$('ClientUlice').value=json.client.Client.ulice;	
								$('ClientMesto').value=json.client.Client.mesto;	
								$('ClientPsc').value=json.client.Client.psc;	
								$('ClientStatId').value=json.client.Client.stat_id;	
								$('ClientDosazeneVzdelaniList').value=json.client.Client.dosazene_vzdelani_list;	
								//$('ClientHodnoceniKvalitaPrace').value=json.client.Client.hodnoceni_kvalita_prace;	
								//$('ClientHodnoceniAlkohol').value=json.client.Client.hodnoceni_alkohol;	
								//$('ClientHodnoceniSpolehlivost').value=json.client.Client.hodnoceni_spolehlivost;	
								//$('ClientHodnoceniBlacklist').value=json.client.Client.hodnoceni_blacklist;	
								
								$('new_connection').value = 1;
							
								if (json.kval){
									$each(json.kval,function(item){
										$('ClientKvalifikaceList').value = item.SettingCareerItem.id;
										$('CarrerItemsDataOd').value = item.ConnectionClientCareerItem.od;
										$('CarrerItemsDataName').value = item.ConnectionClientCareerItem.name; 
										$('CarrerItemsDataPopis').value = item.ConnectionClientCareerItem.popis
										$('CarrerItemsDataDo').value = item.ConnectionClientCareerItem.do
										add_kvalifikace();
									});
								
								}
							
							
							} else {
								$('ClientMobil').value = '';
							}
						} else if (json.result === -1){
							alert('Klienta  ' + json.client.Client.name + ' již máte ve Vaší databázi!');
							$('ClientMobil').value = '';
						} else {
							$('ClientMobil').removeClass('require');
							$('ClientMobil').removeClass('invalid');
							$('ClientMobil').addClass('valid');
						} 
					}
				}).send();
			
			<?php } else { ?>
			
				new Request.JSON({
					url:'/clients/valid_requiter_edit/'+$('ClientMobil').value,		
					onComplete:function(json){
						if (json.result === 1){
							alert('Toto číslo je již použito');
							$('ClientMobil').value = '';
						}
					}
				}).send();
			
			<?php } ?>
		}	
	};
	

	$$('.remove_from_kvalifikace_list').addEvent('click', remove_from_kvalifikace_list.bindWithEvent(this));
	
	/*ADD KVALIFIKACE */
	if($('add_kvalifikace'))
		$('add_kvalifikace').addEvent('click', add_kvalifikace.bindWithEvent(this));
	
	function add_kvalifikace(e){
		if (e)
			new Event(e).stop();
		var select = $('ClientKvalifikaceList');
		var rand_id = uniqid();
		
		// osetreni roku datumu
	//	if(($('CarrerItemsDataOd').value!="" && $('CarrerItemsDataOd').value.length!=4)||($('CarrerItemsDataDo').value!="" && $('CarrerItemsDataDo').value.length!=4)) 
    //  alert('Období musí mít 4 znaky');
	//	else {
    		var table = $('kvalifikace_list');
    		var tr = new Element('tr').inject(table);
    			tr.addClass('kvalifikace_row_'+rand_id);
    			new Element('td').setHTML('Kvalifikace').inject(tr);
    			new Element('td').setHTML(select.getOptionText()).inject(tr);
    			var del = new Element('a',{href:'#',rel:rand_id}).inject(new Element('td').inject(tr)).setHTML('Odstranit').addEvent('click', remove_from_kvalifikace_list.bindWithEvent());
    			del.addClass('remove_from_kvalifikace_list');
    			
    			
    		var tr = new Element('tr').inject(table);
    			tr.addClass('kvalifikace_row_'+rand_id);
    			new Element('td').setHTML('Název zaměstnavatele:').inject(tr);
    			new Element('td',{colspan:2}).setHTML($('CarrerItemsDataName').value).inject(tr);
    			
    		var tr = new Element('tr').inject(table);
    			tr.addClass('kvalifikace_row_'+rand_id);
    			new Element('td').setHTML('Období:').inject(tr);
    			new Element('td').setHTML("od "+$('CarrerItemsDataOd').value).inject(tr);
    			new Element('td').setHTML("do "+$('CarrerItemsDataDo').value).inject(tr);
    		
    		var tr = new Element('tr').inject(table);
    			tr.addClass('kvalifikace_row_'+rand_id);
    			new Element('td').setHTML('Popis pracovní náplně:').inject(tr);
    			new Element('td',{colspan:2}).setHTML($('CarrerItemsDataPopis').value).inject(tr);
    		
    		var tr = new Element('tr').inject(table);
    			tr.addClass('kvalifikace_row_'+rand_id);
    			new Element('td',{colspan:3}).addClass('line').inject(tr);
    		
    		new Element('input',{type:'hidden',value:$('CarrerItemsDataName').value,name:'data[ClientCarrerItems]['+rand_id+'][name]'}).inject(tr);
    		new Element('input',{type:'hidden',value:$('CarrerItemsDataPopis').value,name:'data[ClientCarrerItems]['+rand_id+'][popis]'}).inject(tr);
    		new Element('input',{type:'hidden',value:$('CarrerItemsDataOd').value,name:'data[ClientCarrerItems]['+rand_id+'][od]'}).inject(tr);
    		new Element('input',{type:'hidden',value:$('CarrerItemsDataDo').value,name:'data[ClientCarrerItems]['+rand_id+'][do]'}).inject(tr);
    		new Element('input',{type:'hidden',value:select.value,name:'data[ClientCarrerItems]['+rand_id+'][setting_career_item_id]'}).inject(tr);
    		
    		$('CarrerItemsDataName').value 	 = '';
    		$('CarrerItemsDataPopis').value  = '';
    		$('CarrerItemsDataOd').value	 = '';
    		$('CarrerItemsDataDo').value	 = '';
    		$('ClientKvalifikaceList').value = '';
			pocetProfesi++;
   //	  }
	};
	
	function remove_from_kvalifikace_list(e){
		var event=new Event(e);
		event.stop();
		var obj = event.target;
		
		if (confirm('Opravdu si přejete odstranit tuto kvalifikaci?')){
			var id = '.kvalifikace_row_'+obj.getProperty('rel');
			$('kvalifikace_list').getElements(id).dispose();
		
		}
		pocetProfesi--;
	}
	
	
	$$('.remove_from_certifikaty_list').addEvent('click', remove_from_certifikaty_list.bindWithEvent(this));
	
	/*ADD CERTIFIKAT */
	if($('add_certifikat'))
	$('add_certifikat').addEvent('click', function(e){
		new Event(e).stop();
		var select = $('CertifikatyItemsCertifikatyList');
		var rand_id = uniqid();
		
		var table = $('certifikaty_list');
		var tr = new Element('tr').inject(table);
			tr.addClass('kvalifikace_row_'+rand_id);
			new Element('td').setHTML('Typ').inject(tr);
			new Element('td').setHTML(select.getOptionText()).inject(tr);
			new Element('td').setHTML('').inject(tr);
			var del = new Element('a',{href:'#',rel:rand_id}).inject(new Element('td').inject(tr)).setHTML('Odstranit').addEvent('click', remove_from_certifikaty_list.bindWithEvent());
			del.addClass('remove_from_certifikaty_list');
			
			
		var tr = new Element('tr').inject(table);
			tr.addClass('kvalifikace_row_'+rand_id);
			new Element('td').setHTML('Komentář:').inject(tr);
			new Element('td').setHTML($('CertifikatyItemsCertifikatyKomentar').value).inject(tr);
			new Element('td').setHTML('Platný do:').inject(tr);
			new Element('td').setHTML($('CertifikatyItemsCertifikatyPlatnost').value).inject(tr);
		
		var tr = new Element('tr').inject(table);
			tr.addClass('kvalifikace_row_'+rand_id);
			new Element('td',{colspan:4}).addClass('line').inject(tr);
		
		new Element('input',{type:'hidden',value:$('CertifikatyItemsCertifikatyKomentar').value,name:'data[CertifikatyItem]['+rand_id+'][komentar]'}).inject(tr);
		new Element('input',{type:'hidden',value:$('CertifikatyItemsCertifikatyPlatnost').value,name:'data[CertifikatyItem]['+rand_id+'][platnost]'}).inject(tr);
		new Element('input',{type:'hidden',value:$('CertifikatyItemsCertifikatyList').value,name:'data[CertifikatyItem]['+rand_id+'][list]'}).inject(tr);
	
	});
	
	
	function remove_from_certifikaty_list(e){
		var event=new Event(e);
		event.stop();
		var obj = event.target;
		
		if (confirm('Opravdu si přejete odstranit tento certifikát?')){
			var id = '.kvalifikace_row_'+obj.getProperty('rel');
			$('certifikaty_list').getElements(id).dispose();
		
		}
	}

	if($(form_id).getElement('input[id=save_close]'))
	$(form_id).getElement('input[id=save_close]').addEvent('click',function(e){
	    var this_form_id = $('client_edit_formular');
		new Event(e).stop();
		valid_result = validation.valideForm(this_form_id);
		
		if(pocetProfesi==0) 
			if(valid_result == true)
				valid_result = Array("Musite zvolit profesi");
			else
				valid_result[valid_result.length]="Musite zvolit profesi";
                
        var rege=/^\d{4}\-\d{2}\-\d{2}$/;       
        if($('ClientDatumNarozeni').value != '' && rege.test($('ClientDatumNarozeni').value) === false) 
			if(valid_result == true)
				valid_result = Array("Datum narození musí být ve formátu RRRR-MM-DD (R - Rok, M - měsíc, D - den)");
			else
				valid_result[valid_result.length]="Datum narození musí být ve formátu RRRR-MM-DD (R - Rok, M - měsíc, D - den)";        
        else{
            datum_narozeni = $('ClientDatumNarozeni').value.split('-');
            if(datum_narozeni[0] < 1911){//sto let = chyba
                if(valid_result == true)
				    valid_result = Array("Člověk je starší jak 100let!!!");
    			else
    				valid_result[valid_result.length]="Člověk je starší jak 100let!!!";        
            }    
        }

        /*if( $('ClientRodneCislo').value == '' || !checkRC($('ClientRodneCislo').value) ){
            valid_result = Array("Nespravný formát Rodného čísla");
        }*/
        

		if ($('new_connection').value == 0 || confirm('Vámy zadaný klient již existuje v databázi, chcete jej přiřadit pod Vás?')){
		    
			var request = new Request.JSON({ 
				url:$(this_form_id).action,		 
				onComplete:function(json){
					//if(json.result == false){
						//alert('fasdsa');
					//	alert(json.message);
					//}
					//else{
						
						click_refresh($('ClientId').value); 
						domwin.closeWindow('domwin'); 
					//}
				}
			});
			
			if (valid_result == true){
                button_preloader($('save_close'));
				request.post($(this_form_id));
                button_preloader_disable($('save_close'));
			} else {
				var error_message = new MyAlert();
				error_message.show(valid_result)
			}
		}
	});

	$$('.integer, .float').inputLimit();	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define(form_id,{
		'ClientJmeno': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit jméno'},
		},
		'ClientMobil1': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit mobil'},
			'length': {'condition':{'min':14,'max':14}, 'err_message':'Mobil musí mít 14 znaků'},		
		},
		'ClientPrijmeni': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit příjmení'},
		},

        'ClientDatumNarozeni': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit datum narození'},
		},
        'ClientRodneCislo': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit rodné číslo'},
			'length': {'condition':{'min':9,'max':11}, 'err_message':'Rodné číslo musí mít 9-11 znaků'},
		},
        'ClientUlice': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit ulici'},
		},
        'ClientMesto': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit město'},
		},
        'ClientContactUlice': {
            'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit kontaktní ulici'},
        },
        'ClientContactMesto': {
            'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit kontaktní město'},
        },
	    'ClientEmail': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit email'},
		},
        'ClientPsc': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit PSČ'},
		},
        'ClientContactPsc': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit kontaktní PSČ'},
		},
        'ClientNationality': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit narodnost'},
		},
        'ClientOsPohovor': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit osobní pohovor'},
		},
        'ClientOsPohovorPoznamka': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit osobní pohovor - poznámka'},
		},
        'ClientInfoAboutJob': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vybrat zdroj inormaci o pracovní místě'},
		},
        'ClientSettingPojistovnaId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vybrat pojišťovnu'},
		},
        'ClientCountriesId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vybrat okres'},
		},
        'ClientStatId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vybrat stat'},
		},
        'ClientContactCountriesId': {
            'testReq': {'condition':'not_empty','err_message':'Musíte vybrat kontaktní okres'},
        },
        'ClientContactStatId': {
            'testReq': {'condition':'not_empty','err_message':'Musíte vybrat kontaktní stat'},
        },
        'ClientCisloOp': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vybrat pohlavi'},
		},
        'ClientDosazeneVzdelaniList': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vybrat vzdelani'},
		},
        'ClientDosazeneVzdelaniObor': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vybrat obor vzdělání'},
		},
        'ClientPohlaviList': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vybrat pohlavi'},
		},

         <?php if(isset($at_interni) && $at_interni == true): ?>
		,'ClientCuCisloUctu': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit číslo účtu'},
		},
        'ClientEmail': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit email'},
		},
        'ClientCuKodBanky': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit kód banky'},
		}
        <?php endif; ?>
	});
	validation.generate(form_id,<?php echo (isset($this->data['Client']['id'])) ?'true' : 'false'; ?>);
    validation.check_form(form_id);
    
	<?php if(isset($only_show)): ?>
	$(form_id).getElements('input, select, textarea')
		.addClass('read_info')
		.setProperty('readonly','readonly');
	<?php endif; ?>
    
    
    if($('add_info_about_job')){		
        $('add_info_about_job').addEvent('click',function(e){ 
             new Event(e).stop();
            if($('InfoAboutJobName').value != ''){
                new Request.JSON({
        			    url:this.href+'/'+$('InfoAboutJobName').value,	
        				onComplete:(function(json){
        			        if(json){
        			           if(json.result == true){
        			              var sel = $('ClientInfoAboutJob');
                                  new Element('option',{value:json.id}).setHTML($('InfoAboutJobName').value).inject(sel);
        			              $('InfoAboutJobName').value = '';
                               }
                               else
                                 alert('Chyba uložení zkuste to prosím znova');
        			        }
        				}).bind(this)
                }).send();
           }
        });  
    }
if($('client_trash')){		
    $('client_trash').addEvent('click',function(e){ 
         new Event(e).stop();
        if(confirm('Opravdu chcete smazat tohoto klienta?')){
            new Request.JSON({
    			    url:this.href,	
    				onComplete:(function(){
    			         $('duplicity_client_edit_view').empty();
                         $('duplicity_client_ul').getElement('li[rel='+this.rel+']').dispose();
                         slider_cl_dup.toggle();
    				}).bind(this)
            }).send();
       }
    });  
}
 <?php if(isset($at_interni) && $at_interni == true): ?>
 $('ClientEmail').addEvent('change', function(e){
        if(this.value != ''){
            e.stop();   
            button_preloader($('save_close'));
            new Request.JSON({
    			url:'/int_clients/check_mail_in_int_zam/'+this.value,		
    			onComplete:function(json){
    				if(json){
    				    if(json.result == true){			            				        
                            alert('Tento email už má interní zaměstnanec, jedná se o zaměstnance: '+json.zam+"\nEmaily musí být unikátní pro int. zaměstance. Změňte tento email na nějaký nepoužitý!");
    				    }
                        else{
                           button_preloader_disable($('save_close'));                
                        }                                                
    				}
    			}
    	    }).send();
            
             alert('Pokud jste kliknuli na tl. Uložit, stikněte jej znovu. Validace emailu tento proces přerušila.');
        }
 });
 <?php endif; ?>

$('filtr_datum_narozeni').addEvent('click',function(e){new Event(e).stop(); displayCalendar($('ClientDatumNarozeni'),'yyyy-mm-dd',this,true);});

//nacteni dokumentu az kdyz klikneme na zalozku c.uctu
$('domwin').getElements('.ousko').getElement('a').addEvent('click',function(e){
    if(this.getProperty('href') == '#krok2' && $('client_documents').hasClass('loaded') == false){
        $('client_documents').setHTML('Nahrávám...');
        $('client_documents').addClass('loaded');
        new Request.HTML({
			url:'/clients/client_document/'+$('ClientId').value+'/',
            update:'client_documents',		
			onComplete:function(){}
	    }).send();
    }
})

$('copy_address').addEvent('click', function(e){
    e.stop();
    $('ClientContactUlice').value = $('ClientUlice').value;
    $('ClientContactPsc').value = $('ClientPsc').value;
    $('ClientContactMesto').value = $('ClientMesto').value;
    $('ClientContactStatId').value = $('ClientStatId').value;
    $('ClientContactCountriesId').value = $('ClientContactCountriesId').value;

    $('ClientContactUlice').addClass('valid').removeClass('invalid');
    $('ClientContactPsc').addClass('valid').removeClass('invalid');
    $('ClientContactMesto').addClass('valid').removeClass('invalid');
    $('ClientContactStatId').addClass('valid').removeClass('invalid');
    $('ClientContactCountriesId').addClass('valid').removeClass('invalid');
});
 function checkRC( rc ){
        tmp = rc.match(/(\d\d)(\d\d)(\d\d)[ /]*(\d\d\d)(\d?)/); //
        //console.log(tmp);
        if(!tmp){
           return false;
        }

        var year = parseInt(tmp[1]);
        var month = parseInt(tmp[2]);
        var day = parseInt(tmp[3]);
        var ext = parseInt(tmp[4]);
        var c = parseInt(tmp[5]);

        if (c === '') {
            return year < 54;
        }

        var mod = parseInt(tmp[1] + tmp[2] + tmp[3] + tmp[4] ) % 11;
        if (mod === 10){
            mod = 0;
        }
        if (mod !== c) {
            return false;
        }

        return true;
 }
</script>