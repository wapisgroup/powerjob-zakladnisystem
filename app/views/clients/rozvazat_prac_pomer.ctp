<form action='/clients/rozvazat_prac_pomer/<?php echo $client_id;?>/<?php echo $connection_id;?>' method='post' id='formular'>
   <?php echo  $htmlExt->hidden('client_id',array('value'=>$client_id));?>
   <?php echo  $htmlExt->hidden('connection_id',array('value'=>$connection_id));?>
    <fieldset>
		<legend>Propuštění zaměstnance</legend>
		<?php echo $htmlExt->inputDate('to',array('tabindex'=>1,'label'=>'Datum ukončení','class'=>'','label_class'=>'long'));?> <br class="clear">		
        <?php echo $htmlExt->selectTag('reason_at_sacking', $duvod_propusteni_list, null, array('tabindex'=>2,'label'=>'Důvod propuštění','label_class'=>'long'));?>
    </fieldset>
    <fieldset>
	<legend>Hodnocení klienta</legend>
		<div class="sll" >  
			<?php echo $htmlExt->selectTag('ClientRating/kvalita_prace', $hodnoceni_list2, null, array('tabindex'=>3,'label'=>'Kvalita práce'),null,false);?>
			<?php echo $htmlExt->selectTag('ClientRating/spolehlivost', $hodnoceni_list2, null, array('tabindex'=>5,'label'=>'Docházka'),null,false);?>
			<?php echo $htmlExt->selectTag('ClientRating/zamestnan', $ano_ne_list, null, array('tabindex'=>7,'label'=>'Byl už zaměstnán v AT?'),null,false);?>
					
		</div>
		<div class="slr" >  
			<?php echo $htmlExt->selectTag('ClientRating/alkohol', $hodnoceni_list3, null, array('tabindex'=>4,'label'=>'Má problémy s alkoholem'),null,false);?>
			<?php echo $htmlExt->selectTag('ClientRating/blacklist', $doporuceni_pro_nabor, null, array('tabindex'=>6,'label'=>'Doporučení pro nábor'),null,false);?>
			
		</div><br />
		<?php echo $htmlExt->textarea('ClientRating/text',array('tabindex'=>8,'label'=>'Komentář k hodnocení','class'=>'long','label_class'=>'long'));?> <br class="clear">
	 </fieldset>
     <fieldset>
		<legend>Opp</legend>
        <table class="table">
            <tr>
                <th>Druh</th>
                <th>Typ</th>
                <th>Velikost</th>
                <th>Cena</th>
                <th>Datum přidělení</th>
                <th>Nárok</th>
                <th></th>
            </tr>
            <?php 
            if(isset($opp_list))
                foreach($opp_list as $item){
                    echo '<tr>';
                    echo '<td>'.$item['ConnectionClientOpp']['typ'].'</td>';
                    echo '<td>'.$item['ConnectionClientOpp']['name'].'</td>';
                    echo '<td>'.$item['ConnectionClientOpp']['size'].'</td>';
                    echo '<td>'.$item['ConnectionClientOpp']['price'].'</td>';
                    echo '<td>'.$fastest->czechDate($item['ConnectionClientOpp']['created']).'</td>';
                    echo '<td>'.$item['ConnectionClientOpp']['narok'].'</td>';
                    echo '<td>'.$htmlExt->selectTag('Opp/'.$item['ConnectionClientOpp']['id'].'/var',($item['ConnectionClientOpp']['narok'] == 'ne' ? $opp_rozvazat_pp_ne : $opp_rozvazat_pp),null,null,null,false);
                    echo $htmlExt->hidden('Opp/'.$item['ConnectionClientOpp']['id'].'/narok',array('value'=>$item['ConnectionClientOpp']['narok']));
                    echo $htmlExt->hidden('Opp/'.$item['ConnectionClientOpp']['id'].'/price',array('value'=>$item['ConnectionClientOpp']['price']));
                    echo '</td>';
			       
                    echo '</tr>';
                }
            
            ?>
        </table> 
	 </fieldset>
    <div class='formular_action'>
		<input type='button' value='Propustit' id='VyhoditSaveAndClose' />
		<input type='button' value='Zavřít' id='VyhoditClose'/>
	</div>
</form>
<script>
    $('calbut_To').addEvent('change',function(e){
        $('VyhoditSaveAndClose').addClass('none');
        new Request.JSON({
			url:'/at_employees/check_return_date_with_estates/'+$('ClientId').value+'/'+this.value+'/',		
			onComplete:(function(json){
				if (json){
					if (json.result == false){
					    this.value = ""; 
						alert(json.message);
					}
                    $('VyhoditSaveAndClose').removeClass('none');
				} else {
					alert('Chyba aplikace');
				}
			}).bind(this)
		}).send();
    });   
    
	$('VyhoditClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_zamestnanci_hodiny');});
	$('VyhoditSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
        valid_result = validation.valideForm('formular');
		
        if( $('calbut_To').value == '') 
			if(valid_result == true)
				valid_result = Array("Musite zvolit datum propuštění");
			else
				valid_result[valid_result.length]="Musite zvolit datum propuštění";
        
        if (valid_result == true){
            // nacteni shrnuti
            if(confirm(get_confirm_text())){
                //odeslani requestu
    			new Request.JSON({
    				url:$('formular').action,		
    				onComplete:function(json){
    					if (json){
    						if (json.result === true){
                                click_refresh($('ClientId').value); 
    							domwin.closeWindow('domwin_zamestnanci_hodiny');
    						} else {
    							alert(json.message);
    						}
    					} else {
    						alert('Chyba aplikace');
    					}
    				}
    			}).post($('formular'));
            }
       } 
       else {
		  var error_message = new MyAlert();
		  error_message.show(valid_result)
	   }     
	});
    
    validation.define('formular',{	
        'ReasonAtSacking': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit důvod ukončení'},
		},
        'ClientRatingKvalitaPrace': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit kvalitu práce u hodnocení'},
		},
        'ClientRatingAlkohol': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit má problémy s alkoholem u hodnocení'},
		},
        'ClientRatingSpolehlivost': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit docházku u hodnocení'},
		},
		'ClientRatingText': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit komentář k hodnocení'},
		},
	});
	validation.generate('formular',false);
    
    
    /**
     * funkce ktera vraci shrnuti pri propusteni klienta
     */
    function get_confirm_text(){
        /**
         * nastavnei promennych pouzitych do textu
         */
        var datum_ukonceni = $('calbut_To').value;
        var duvod_propusteni = $('ReasonAtSacking').options[$('ReasonAtSacking').selectedIndex].title;
        var kvalita_prace = $('ClientRatingKvalitaPrace').options[$('ClientRatingKvalitaPrace').selectedIndex].title;
        var alkohol = $('ClientRatingAlkohol').options[$('ClientRatingAlkohol').selectedIndex].title;
        var dochazka = $('ClientRatingSpolehlivost').options[$('ClientRatingSpolehlivost').selectedIndex].title;
        var doporuceni = $('ClientRatingBlacklist').options[$('ClientRatingBlacklist').selectedIndex].title;
        var zamestnan = $('ClientRatingZamestnan').options[$('ClientRatingZamestnan').selectedIndex].title;
        var komentar = $('ClientRatingText').value;
        /**
         * slozeni textu propusteni
         */
        var text  = "Shrnutí propuštění :\n\n";
            text += "Datum ukončení: "+datum_ukonceni+"\n";
            text += "Důvod propuštění: "+duvod_propusteni+"\n\n\n";
            
            text += "Hodnocení klienta :\n\n";
            text += "Kvalita práce: "+kvalita_prace+"\n";
            text += "Má problemy s alkoholem: "+alkohol+"\n";
            text += "Docházka: "+dochazka+"\n";
            text += "Doporučení pro nábor: "+doporuceni+"\n";
            text += "Byl už zaměstnán v AT?: "+zamestnan+"\n\n";
            text += "Komentář: "+komentar;
            
        //return    
        return text;
    } 
     
</script>	