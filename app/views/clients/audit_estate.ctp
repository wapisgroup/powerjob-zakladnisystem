<table class='table'>
	<tr>
		<th>Majetek</th>
        <th>IMEI/SN/VIN</th>
		<th>Od</th>
        <th>Do</th>
	</tr>
	<?php 
  

  if (isset($audit_estate_list) && count($audit_estate_list) > 0):?>
		<?php foreach($audit_estate_list as $item):?>
		<tr>
			<td><?php echo $item['AuditEstate']['name'];?></td>
            <td><?php echo $item['AuditEstate']['imei_sn_vin'];?></td>
			<td><?php echo $fastest->czechDate($item['ConnectionAuditEstate']['created']);?></td>
            <td><?php echo $fastest->czechDate($item['ConnectionAuditEstate']['to_date']);?></td>
		</tr>
		<?php endforeach;?>
	<?php endif;?>
</table> 
<br />
<table class='table'>
	<tr>
		<th>Majetek jako správce</th>
        <th>IMEI/SN/VIN</th>
		<th>Od</th>
	</tr>
	<?php 
  

  if (isset($ae_manager_list) && count($ae_manager_list) > 0):?>
		<?php foreach($ae_manager_list as $item):?>
		<tr>
			<td><?php echo $item['AuditEstate']['name'];?></td>
            <td><?php echo $item['AuditEstate']['imei_sn_vin'];?></td>
			<td><?php echo $fastest->czechDate($item['AuditEstate']['created']);?></td>
		</tr>
		<?php endforeach;?>
	<?php endif;?>
</table> 
<script>

</script>