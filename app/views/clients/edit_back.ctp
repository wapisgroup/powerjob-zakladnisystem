<form action='/clients/edit/' method='post' id='client_edit_formular'>
	<?php echo $htmlExt->hidden('Client/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
			<li class="ousko"><a href="#krok2">Předchozí zaměstnání (Kvalifikace)</a></li>
			<li class="ousko"><a href="#krok3">Dotazník CZ</a></li>
			<li class="ousko"><a href="#krok3">Dotazník SK</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Osobní údaje</legend>
				<div class="sll">  
					<?php echo $htmlExt->input('Client/jmeno',array('tabindex'=>1,'label'=>'Jméno'));?> <br class="clear">
					<?php echo $htmlExt->selectTag('Client/pohlavi_list',array(1=>'Muž',2=> 'Žena'),null,array('tabindex'=>1,'label'=>'Pohlaví'),null,false);?> <br class="clear">
					<?php echo $htmlExt->selectTag('Client/stat_id',$stat_list, null, array('tabindex'=>2,'label'=>'Stát'),null,false);?> <br class="clear">
					<?php echo $htmlExt->input('Client/ulice',array('tabindex'=>2,'label'=>'Ulice'));?> <br class="clear">
				</div>
				<div class="slr">  
					<?php echo $htmlExt->input('Client/prijmeni',array('tabindex'=>2,'label'=>'Příjmení'));?> <br class="clear">
					<?php echo $htmlExt->inputDate('Client/datum_narozeni',array('tabindex'=>2,'label'=>'Datum narození'));?> <br class="clear">
					<?php echo $htmlExt->input('Client/mesto',array('tabindex'=>2,'label'=>'Město'));?> <br class="clear">
					<?php echo $htmlExt->input('Client/psc',array('tabindex'=>2,'label'=>'PSČ'));?> <br class="clear">
				</div>
				<br class='clear' />
				<?php echo $htmlExt->input('Client/kvalifikace',array('tabindex'=>2,'label'=>'Kvalifikace','class'=>'long','label_class'=>'long'));?> <br class="clear">
				<?php echo $htmlExt->input('Client/posledni_zamestnavatel',array('tabindex'=>2,'label'=>'Poslední zaměstnavatel','class'=>'long','label_class'=>'long'));?> <br class="clear">
				<?php echo $htmlExt->textarea('Client/poznamka',array('tabindex'=>2,'label'=>'Poznámka','class'=>'long','label_class'=>'long'));?> <br class="clear">
				<div class="sll">  
					<?php echo $htmlExt->inputDate('Client/datum_naboru',array('tabindex'=>1,'label'=>'Datum náboru'));?> <br class="clear">
				</div>
				<div class='slr'>
					<?php echo $htmlExt->input('Client/forma_naboru',array('tabindex'=>1,'label'=>'Forma náboru'));?> <br class="clear">
				</div>
				<br class='clear'/>
			</fieldset>
			<fieldset>
				<legend>Fyzická / právnická osoba</legend>
				<div class="sll">  
					<?php echo $htmlExt->input('Client/obchodni_jmeno',array('tabindex'=>1,'label'=>'Obchodní jméno'));?> <br class="clear">
				</div>
				<div class="slr">  
					<?php echo $htmlExt->input('Client/ico',array('tabindex'=>2,'label'=>'IČO'));?> <br class="clear">
				</div>
			</fieldset>
			<fieldset>
				<legend>Kontaktní údaje</legend>
				<div class="sll">  
					<?php echo $htmlExt->input('Client/telefon1',array('maxlength'=>15,'tabindex'=>5,'label'=>'Telefon'));?> <br class="clear">
					<?php echo $htmlExt->input('Client/email',array('tabindex'=>3,'label'=>'Email'));?> <br class="clear">
				</div>
				<div class="slr">  
					<?php echo $htmlExt->input('Client/mobil',array('maxlength'=>15,'tabindex'=>4,'label'=>'Mobil'));?> <br class="clear">
					<?php echo $htmlExt->input('Client/other_contact',array('tabindex'=>6,'label'=>'Jiné'));?> <br class="clear">
				</div>
				<br class='clear' />
			</fieldset>
			<div class="krok">
				<a href='#krok2' class='admin_dom_move next'>Další krok</a>
			</div>
		</div>
		<div class="domtabs field">
			<fieldset>
				<legend>Volba předchozího zaměstnání</legend>
				<?php echo $htmlExt->selectTag('Client/kvalifikace_list', $kvalifikace_list, null, array('label'=>'Kvalifikace','class'=>'long','label_class'=>'long'));?><a href='#' id='add_kvalifikace'>Přidat</a><br class='clear'/>
				<fieldset>
					<legend>Seznam kvalifikací</legend>
					<ul id='kvalifikace_list'>
					<?php if (isset($kvalifikace_list_item)):?>
					<?php foreach($kvalifikace_list_item as $id=>$caption):?>
						<li><?php echo $caption;?><input type='hidden' name='data[Client][kvalifikace_list_item][]' value='<?php echo $id;?>'/><a href='#' class='remove_from_kvalifikace_list'>Odstranit</a></li>
					<?php endforeach;?>
					<?php endif;?>
					</ul>
				</fieldset>
			</fieldset>
		</div>
		<div class="domtabs field">
			<?php echo $this->renderElement('../clients/dotaznik_cz');?>
		</div>
		<div class="domtabs field">
			<?php echo $this->renderElement('../clients/dotaznik_sk');?>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','class'=>'button'));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','class'=>'button'));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'}); 
	
	$$('.remove_from_kvalifikace_list').addEvent('click', function(e){new Event(e).stop();remove_from_kvalifikace_list(this)});
	
	$('add_kvalifikace').addEvent('click', function(e){
		new Event(e).stop();
		var select = $('ClientKvalifikaceList'), ul = $('kvalifikace_list'), li;
		li = new Element('li').inject(ul).setHTML(select.getOptionText());
		new Element('input',{type:'hidden',value:select.value,name:'data[Client][kvalifikace_list_item][]'}).inject(li);
		new Element('a',{href:'#'})
			.setHTML('Odstranit')
			.addEvent('click', function(e){new Event(e).stop();remove_from_kvalifikace_list(this)})
			.inject(li);
		
	});
	
	function remove_from_kvalifikace_list(obj){
		if (confirm('Opravdu si přejete odstranit tuto kvalifikaci?')){obj.getParent('li').dispose();}
	}
	
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('client_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('client_edit_formular').action,		
				onComplete:function(){
					click_refresh($('ClientId').value);
					domwin.closeWindow('domwin');
				}
			}).post($('client_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('client_edit_formular',{
		'ClientJmeno': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit jméno'},
		},
		'ClientTelefon': {
			'testReq': {'condition':'email','err_message':'Musíte vyplnit telefon'},
		},
		'ClientPrijmeni': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit příjmení'},
		}
	});
	validation.generate('client_edit_formular',<?php echo (isset($this->data['Client']['id']))?'true':'false';?>);
</script>