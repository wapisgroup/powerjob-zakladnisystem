<p style="padding:10px;">
<a href='/clients/messages_edit/<?php echo $this->data['Client']['id'];?>' class='button edit' id='company_message_add_new2' title='Přidání nové zprávy'>Přidat zprávu</a>
</p>
<table class='table'>
	<tr>
		<th>Od</th>
		<th>Předmět</th>
		<th>Text</th>
		<th>Datum</th>
		<th>Vytvořeno</th>
		<th>Detail</th>
	</tr>
    <!-- Zpráva o vytvoření -->
    	<tr>
			<td>systém</td>
			<td>Vytvoření klienta</td>
			<td>Dnes byl tento klient vložen do systému.</td>
			<td></td>
			<td><?php echo $fastest->czechDate($this->data['Client']['created']);?></td>
			<td></td>
		</tr>

	<?php 
  

  if (isset($message_list) && count($message_list) > 0):?>
		<?php foreach($message_list as $msg):?>
		<tr>
			<td><?php echo $msg['CmsUser']['name'];?></td>
			<td><?php echo $msg['ClientMessage']['name'];?></td>
			<td><?php echo $fastest->orez($msg['ClientMessage']['text'],50);?></td>
			<td><?php echo $fastest->czechDate($msg['ClientMessage']['datum']);?></td>
			<td><?php echo $fastest->czechDate($msg['ClientMessage']['created']);?></td>
			<td><a href="/clients/messages_detail/<?php echo $msg['ClientMessage']['id'];?>" class="detail">Detail</a></td>
		</tr>
		<?php endforeach;?>
	<?php endif;?>
</table> 
<script>
	
	$('company_message_add_new2').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_message_add',
			sizes		: [580,390],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			post_data	: {from:'editace'},
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});

	$$('.detail').addEvent('click',function(e){
		new Event(e).stop();

		domwin.newWindow({
				id			: 'domwin_message_detail',
				sizes		: [500,300],
				scrollbars	: true,
				title		: 'Detail - zprávy',
				languages	: false,
				type		: 'AJAX',
				ajax_url	: this.href,
				closeConfirm: true,
				max_minBtn	: false,
				modal_close	: false,
				remove_scroll: false
		}); 
	});
</script>