<script type="text/javascript">
	function upload_file_complete(url){
		alert('Soubor byl nahrán.');
		$('ClientAttachmentFile').value = url;
	}
</script>
<form action='/clients/attachs_edit/' method='post' id='client_attachs_edit_formular'>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Příloha</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<?php echo $htmlExt->hidden('ClientAttachment/id');?>
			<?php echo $htmlExt->hidden('ClientAttachment/client_id');?>
			<fieldset>
				<legend>Prosím vyplňte následující údaje</legend>
				<?php echo $htmlExt->input('ClientAttachment/name',array('tabindex'=>1,'label'=>'Název','class'=>'long','label_class'=>'long'));?> <br class="clear">
				<div class='sll'>
					<?php 
						$upload_setting = array(
							'paths' => array(
								'upload_script' => '/clients/upload_attach/',
								'path_to_file'	=> 'uploaded/clients/attachment/'
							),
							'upload_type'	=> 'php',
							'onComplete'	=> 'upload_file_complete',
							'label'			=> 'Soubor'
						);
					?>
					<?php echo $fileInput->render('ClientAttachment/file',$upload_setting);?> <br class="clear">
				</div>
				<div class='slr'>
					<?php echo $htmlExt->selectTag('ClientAttachment/setting_attachment_type_id',$setting_attachment_type_list,null,array('tabindex'=>4,'label'=>'Typ přílohy'));?> <br class="clear">
				</div>
                <br/>
			</fieldset>
            <fieldset>
                <?= $htmlExt->checkbox('ClientAttachment/email');?><label for='ClientAttachmentEmail'>Odeslat přílohu a potvrzení emailem</label><br/>
            </fieldset>
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditClientAttachmentSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditClientAttachmentClose'/>
	</div>
</form>
<script>
	var domtab = new DomTabs({'className':'admin_dom'}); 
	
	$('AddEditClientAttachmentClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_attach_add');});
	
	$('AddEditClientAttachmentSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('client_attachs_edit_formular');
		if (valid_result == true){
			new Request.HTML({
				url:$('client_attachs_edit_formular').action,		
				update: $('domwin_attach').getElement('.CB_ImgContainer'),
				onComplete:function(){
					domwin.closeWindow('domwin_attach_add');
				}
			}).post($('client_attachs_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('client_attachs_edit_formular',{
		'ClientAttachmentName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
		}
	});
	validation.generate('client_attachs_edit_formular',<?php echo (isset($this->data['ClientAttachment']['id']))?'true':'false';?>);
</script>    