	<br /><table class='table' id='table_kontakty'>
		<tr>
			<th>Název</th>
			<th style='width:80px'></th>
			<th>Fakturovaná částka | Typ | Ubytování | Doprava | Marže | Max.</th>
		</tr>
		<?php if (isset($work_position_list_test) && count($work_position_list_test) >0):?>
		<?php foreach($work_position_list_test as $work_position_item):?>
		<tr>
			<td class='edit_name_td' style='width:200px; vertical-align:top;'>
			<div style='position:relative; height:100%; width:100%;'>
				<a href='#show' class='show_hide_other_tr ta open_list' style='position:absolute; left:-9px; top:0px;'>D</a>
				<?php echo (($work_position_item['CompanyWorkPosition']['parent_id']  != -1)?'<span style="float:left; margin:5px 0px 0px 5px;">->  </span>':'');?>
				<input class='editable_input invisible long68' type='text' value='<?php echo $work_position_item['CompanyWorkPosition']['name'];?>' readonly='readonly' />
				<a href='#' class='edit_item ' title='Editovat název pozice <?php echo $work_position_item['CompanyWorkPosition']['name'];?>'><img src='/css/fastest/table/ta_edit.png' /></a>
				<a href='/companies/position_change_name/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>/' class='accept_change none'><img src='/css/fastest/table/ta_accept.png' /></a>
				<a href='#' class='cancel_change none'><img src='/css/fastest/table/ta_cancel.png' /></a>
				<img class='preloader none' src='/css/fastest/table/ta_preloader.gif'/>
				<a title='Odstranit pracovní pozici <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta trash remove_position ' href='/companies/position_trash/<?php echo $company_id;?>/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>'/>remove_pozice</a>
			</div>	
			</td>
			<td>&nbsp;</td>
			<td colspan='3'>
				<table cellspacing='0' cellpadding='0'>
					<tr>			
						<td>
							<table cellspacing='0' cellpadding='0' style='width: 400px; text-align: center;' class="money_item_table">
							<?php foreach ($work_position_item['CompanyMoneyItem'] as $money_item):?>
								<tr class='td_kalkulacka'>
									<td style='width:75px; text-align:right;'><?php echo $money_item['fakturacni_sazba_na_hodinu'];?></td>
									<td style='width:20px;'><?php echo $money_item['name'];?></td>
									<td style='width:10px; text-align:center;'><?php echo (($money_item['cena_ubytovani_na_mesic'] > 0)?'A':'N');?></td>
									<td style='width:10px; text-align:center;'><?php echo (($money_item['doprava'] > 0)?'A':'N');?></td>
									<td style='width:35px;'><?php echo $money_item['provinzni_marze'];?>%</td>
									<td style='width:35px;'><?php echo $money_item['vypocitana_celkova_cista_maximalni_mzda_na_hodinu'] ;?></td>
									<td style='width:80px;'>
										<a title='Editace odměny "<?php echo $money_item['name'];?>" pozice <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta money edit_money ' href='/companies/work_money_edit/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>/-1/<?php echo $money_item['id'];?>'/>money</a>
									
										<a title='Odstranit kalkulaci "<?php echo $money_item['name'];?>" pozice <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta trash money_item_trash_test ' href='/companies/money_item_trash/<?php echo $company_id?>/<?php echo $money_item['id'];?>'/>money_item_trash</a>
								
									</td>
								</tr>
							<?php endforeach;?>
							</table>
						</td>
						<td style='width:150px;'>
							<a style='width: 150px; background-position: left center; text-indent: 21px;' title='Nová kalkulace pro pozici <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta money_add' href='/companies/work_money_edit/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>/-1/'/>Přidat kalkulaci</a><br />
							<a style='width: 150px; background-position: left center; text-indent: 21px;' title='Nová kalkulace pro pozici <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta money_add' href='/companies/work_money_edit/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>/-2/'/>Přidat kalkulaci - paušál</a>
							<a style='width: 150px; background-position: left center; text-indent: 21px;' title='Nová kalkulace pro pozici <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta money_add' href='/companies/work_money_edit/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>/-3/'/>Přidat kalkulaci - H1000</a>
					        <?php if ($logged_user['CmsGroup']['cms_group_superior_id'] == 1){?>
                                	<a style='width: 150px; background-position: left center; text-indent: 21px;' title='Nová kalkulace pro pozici <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta money_add' href='/companies/work_money_edit_stat/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>/1/'/>Přidat kalkulaci - cz</a><br />
							        <a style='width: 150px; background-position: left center; text-indent: 21px;' title='Nová kalkulace pro pozici <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta money_add' href='/companies/work_money_edit_stat/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>/2/'/>Přidat kalkulaci - sk</a><br />
	                        <?php }?>
                               
                            <br /><br />
                            <?php if ($logged_user['CmsGroup']['id'] == 1 || $logged_user['CmsGroup']['id'] == 5 || (isset($permission['access_to_autorizate_money_item']) && $permission['access_to_autorizate_money_item'] == 1)){?>
	                           <a style='width: 150px; background-position: left center; text-indent: 21px;' title='Autorizovat kalkulace pro pozici <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta stop0 money_auth' href='/companies/work_money_auth/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>/'/>Autorizace</a><br />
                           	<?php }?>
                               <a style='width: 150px; background-position: left center; text-indent: 21px;' title='Žádost o autorizaci kalkulace pro pozici <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta stop0 require_money_auth' href='/companies/require_work_money_auth/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>/'/>Žádost o autorizaci</a>
                    	</td>
					</tr>
				</table>
			</td>
		</tr>
		<?php endforeach;?>
		<?php else:?>
		<tr>
			<td colspan='5'>K této společnosti nebyla nadefinována testovací pracovní pozice</td>
		</tr>
		<?php endif;?>
	</table>
<script>

// odeslani zadosti o autorizaci
$$('.require_money_auth').addEvent('click', function(e){
	new Event(e).stop();
	if (confirm('Opravdu si přejete poslat žádost o autrizaci této kalkulace?'))
		new Request.JSON({
    		url: this.getProperty('href'),
    		onComplete: function(json){
    		    if(json){
        			if(json.result == true){
        				alert('Autorizace byla úspěšně odeslána.');	
        			}
                    else
                        alert('Chyba při odeslání.');
                }    
    			else
    				alert('Chyba : aplikace.');
    		}
		}).send();
});

// ostraneni money_item
$$('.money_auth').addEvent('click', function(e){
	new Event(e).stop();
	if (confirm('Opravdu si přejete autorizovat tuto kalkulaci?'))
		var myurl = this.getProperty('href');
		new Request.JSON({
		url: myurl,
		onComplete: function(JSON){
			if(JSON == true){
				alert('V pořádku fakturačky souhlasí.');
				domwin.newWindow({
					id			: 'domwin_work_position_auth',
					sizes		: [580,320],
					scrollbars	: true,
					title		: this.title,
					languages	: false,
					type		: 'AJAX',
					ajax_url	: myurl + '-1/',
					closeConfirm: true,
					max_minBtn	: false,
					modal_close	: false,
					remove_scroll: false
				}); 
			}
			else
				alert('Chyba : fakturované částky nesouhlasí.');
		}
		}).send();
});

</script>