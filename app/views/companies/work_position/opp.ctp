<?php
    for($mi = 1; $mi <= 24; $mi++){$month_list[$mi] = $mi;}
?>
<form action="/companies/position_opp/" method="post" id='position_opp_form'>
	<?php echo $htmlExt->hidden('position_id',array('value'=>$position_id));?>
	<table class="table tabulka">
		<tr>
			<th>Typ</th>
			<th>Nárok</th>
			<th>Měsíční nárok</th>
		</tr>
		<?php foreach($types as $typ):?>
		<tr>
			<td>
				<?php echo $typ;?>
				<?php echo $htmlExt->hidden('ConnectionPositionOpp/'.$typ.'/position_id',array('value'=>$position_id));?>
				<?php echo $htmlExt->hidden('ConnectionPositionOpp/'.$typ.'/company_id',array('value'=>$company_id));?>
			</td>
			<td><?php echo $htmlExt->selectTag('ConnectionPositionOpp/'.$typ.'/yn', $ne_ano_list, null, array('class'=>'change_yn'), null, false);?></td>
			<td><?php echo $htmlExt->selectTag('ConnectionPositionOpp/'.$typ.'/interval', $month_list,null,array('class'=>'interval','disabled'=>($this->data['ConnectionPositionOpp'][$typ]['yn']==1)?'a':'disabled'),null,true);?></td>
		</tr>
		<?php endforeach;?>
	</table>
	<div class="win_save">
		<input type="button" class="button" value="Uložit" id='save_form' />
		<input type="button" class="button" value="Zavřít" id='close_form' />
	</div>
</form>
<script>
	$$('.change_yn').addEvent('change', function(){
		var interval = this.getParent('tr').getElement('.interval');
		if (this.value == 1){
			interval.removeProperty('disabled');
		} else {
			interval.setProperty('disabled','disabled').setValue('');
		}
	})

	$('save_form').addEvent('click', function(e){
		e.stop();
		$$('.interval').removeProperty('disabled');
		new Request.JSON({
			url: $('position_opp_form').getProperty('action'),
			onComplete: function(json){
				if (json){
					if (json.result === true){
						domwin.closeWindow('domwin_work_position_edit');
					} else {	
						alert(json.message);
					}
				} else {	
					alert('Chyba aplikace');
				}
			}
		}).send($('position_opp_form'))
	})
	
	$('close_form').addEvent('click', function(e){
		e.stop();
		domwin.closeWindow('domwin_work_position_edit');
	})
</script>