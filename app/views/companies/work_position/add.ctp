<form action='/companies/work_position_add/' method='post' id='company_work_position_edit_formular'>
	<?php echo $htmlExt->hidden('CompanyWorkPosition/id');?>
	<?php echo $htmlExt->hidden('CompanyWorkPosition/company_id');?>
	<fieldset>
		<legend>Zadejte údaje</legend>
		<?php echo $htmlExt->input('CompanyWorkPosition/name',array('tabindex'=>1,'label'=>'Název pozice','class'=>'long','label_class'=>'long'));?> <br class="clear">
		<?php echo $htmlExt->checkbox('CompanyWorkPosition/new',null,array('tabindex'=>1,'label'=>'Nová','class'=>'long','label_class'=>'long'));?> <br class="clear">
		
        <?php echo $htmlExt->textarea('CompanyWorkPosition/text',array('tabindex'=>7,'label'=>'Komentář','class'=>'long','label_class'=>'long'));?> <br class="clear">
		
	</fieldset>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditCompanyWorkPositionSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyWorkPositionClose'/>
	</div>
</form>
<script>
	$('AddEditCompanyWorkPositionClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_work_position_edit');});
	
	$('AddEditCompanyWorkPositionSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('company_work_position_edit_formular');
		/*
		//pokud nevkladam testovaci tak osetri i vyplneni platnosti
		if(!$('CompanyWorkPositionTest').checked && $('CompanyMoneyValidityPlatnostOd').value == '')
			if(valid_result == true)
				valid_result = Array("Musite zvolit platnost");
			else
				valid_result[valid_result.length]="Musite zvolit platnost";
		*/
		if (valid_result == true){
			new Request.HTML({
				url:$('company_work_position_edit_formular').action,		
				update: $('domwin_pozice').getElement('.CB_ImgContainer'),
				onComplete:function(){
					domwin.closeWindow('domwin_work_position_edit');
					domtab.goTo(1);
				}
			}).post($('company_work_position_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('company_work_position_edit_formular',{
		'CompanyWorkPositionName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
		}
	});
	validation.generate('company_work_position_edit_formular',<?php echo (isset($this->data['CompanyWorkPosition']['id']))?'true':'false';?>);
</script>