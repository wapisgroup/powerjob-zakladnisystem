<div id='CompanyWorkPositionObal'>
	<div class="win_fce top">
		<a href='/companies/work_position_add/<?php echo $company_id;?>' class='button edit' id='company_work_position_add_new' title='Přidání pracovní pozice'>Přidat profesi</a>
	</div>
<div class="domtabs admin_dom2_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Platné profese</a></li>
			<li class="ousko"><a href="#krok2">Testovací</a></li>
            <li class="ousko"><a href="#krok2">New</a></li>
		</ul>
</div>
<div class="domtabs admin_dom2">
	<div class="domtabs field" >
<br />
	<table class='table' id='table_kontakty'>
		<tr>
			<th>Název</th>
			<th style='width:130px'>Fakturovaná částka</th>
			<th style='width:150px'>Platnost</th>
			<th>Typ | Ubytování | Doprava | Marže | Max.</th>
		</tr>
		<?php if (isset($work_position_list) && count($work_position_list) >0):?>
		<?php foreach($work_position_list as $work_position_item):?>
		<tr>
			<td class='edit_name_td' style='width:200px; vertical-align:top;'>
			<div style='position:relative; height:100%; width:100%;'>
				<a href='#show' class='show_hide_other_tr ta open_list' style='position:absolute; left:-9px; top:0px;'>D</a>
				<?php echo (($work_position_item['CompanyWorkPosition']['parent_id']  != -1)?'<span style="float:left; margin:5px 0px 0px 5px;">->  </span>':'');?>
				<input class='editable_input invisible long68' style='width:100px' type='text' value='<?php echo $work_position_item['CompanyWorkPosition']['name'];?>' readonly='readonly' />
				<a href='#' class='edit_item ' title='Editovat název pozice <?php echo $work_position_item['CompanyWorkPosition']['name'];?>'><img src='/css/fastest/table/ta_edit.png' /></a>
				<a href='/companies/position_change_name/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>/' class='accept_change none'><img src='/css/fastest/table/ta_accept.png' /></a>
				<a href='#' class='cancel_change none'><img src='/css/fastest/table/ta_cancel.png' /></a>
				<img class='preloader none' src='/css/fastest/table/ta_preloader.gif'/>
				
				<? /* <a href='/companies/position_opp/<?php echo $company_id;?>/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>'/' class='opp_setting' title='Nadefinovani OPP'>OPP</a> */ ?>
			<?php if ($logged_user['CmsGroup']['id'] == 1):?>
				<a title='Odstranit pracovní pozici <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta trash remove_position ' href='/companies/position_trash/<?php echo $company_id;?>/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>'/>remove_pozice</a>
			<?php endif;?>
			</div>	
			</td>
			<td colspan='3'>
				<table cellspacing='0' cellpadding='0'>
				<?php foreach ($work_position_item['CompanyMoneyValidity'] as $key => $platnost):?>
					<tr class='<?php echo ($key != 0)?'none hidden_tr':'';?>'>			
						<td style='width:125px'>
							<span style='width:80px; display:block; float:left;'><?php echo $platnost['fakturacka']; echo ' '.($stat_id['Company']['stat_id'] == 1 ? 'CZK' : 'EUR')?></span>
							<?php if ($platnost['platnost_do'] == '0000-00-00'):?>
							<!--a title='Změna fakturované částky pro pozici <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta fakturacka' href='/companies/work_money_validity_edit/<?php echo $platnost['id'];?>'/>change_fakturacka</a-->
							<?php endif;?>
						</td>
						<td style='width:138px'><?php echo $fastest->czechDate($platnost['platnost_od']);?> -> <?php echo $fastest->czechDate($platnost['platnost_do']);?></td>
						<td>
							<table cellspacing='0' cellpadding='0' style='width:230px'>
							<?php foreach ($platnost['CompanyMoneyItem'] as $money_item):?>
								<tr class='td_kalkulacka'>
									<td style='width:40px'><?php echo $money_item['name'];?></td>
									<td style='width:15px'><?php echo (($money_item['cena_ubytovani_na_mesic'] > 0)?'A':'N');?></td>
									<td style='width:15px'><?php echo (($money_item['doprava'] > 0)?'A':'N');?></td>
									<td style='width:40px'><?php echo $money_item['provinzni_marze'];?>%</td>
									<td style='width:40px'><?php echo $money_item['vypocitana_celkova_cista_maximalni_mzda_na_hodinu'] ;?></td>
									<td style='width:80px'>
										<a title='Náhled odměny "<?php echo $money_item['name'];?>" pozice <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta money edit_money ' href='/companies/work_money_edit/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>/<?php echo $platnost['id'];?>/<?php echo $money_item['id'];?>/-1/'/>money</a>
									<?php if ($logged_user['CmsGroup']['id'] == 1):?>
										<a title='Odstranit kalkulaci "<?php echo $money_item['name'];?>" pozice <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta trash money_item_trash ' href='/companies/money_item_trash/<?php echo $company_id?>/<?php echo $money_item['id'];?>'/>money_item_trash</a>
									<?php endif;?>
									</td>
								</tr>
							<?php endforeach;?>
							</table>
						</td>
					</tr>
				<?php endforeach;?>
				</table>
			</td>
		</tr>
		<?php endforeach;?>
		<?php else:?>
		<tr>
			<td colspan='5'>K této společnosti nebyla nadefinována pracovní pozice</td>
		</tr>
		<?php endif;?>
	</table>
    
    <br />
	<table class='table' id='table_kontakty'>
		<tr>
			<th>Název</th>
			<th style='width:130px'>Fakturovaná částka</th>
			<th style='width:150px'>Platnost</th>
			<th></th>
		</tr>
		<?php if (isset($work_position_list_new) && count($work_position_list_new) >0):?>
		<?php foreach($work_position_list_new as $work_position_item):?>
		<tr>
			<td class='edit_name_td' style='width:200px; vertical-align:top;'>
			<div style='position:relative; height:100%; width:100%;'>
				<a href='#show' class='show_hide_other_tr ta open_list' style='position:absolute; left:-9px; top:0px;'>D</a>
				<?php echo (($work_position_item['CompanyWorkPosition']['parent_id']  != -1)?'<span style="float:left; margin:5px 0px 0px 5px;">->  </span>':'');?>
				<input class='editable_input invisible long68' style='width:100px' type='text' value='<?php echo $work_position_item['CompanyWorkPosition']['name'];?>' readonly='readonly' />
				<a href='#' class='edit_item ' title='Editovat název pozice <?php echo $work_position_item['CompanyWorkPosition']['name'];?>'><img src='/css/fastest/table/ta_edit.png' /></a>
				<a href='/companies/position_change_name/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>/' class='accept_change none'><img src='/css/fastest/table/ta_accept.png' /></a>
				<a href='#' class='cancel_change none'><img src='/css/fastest/table/ta_cancel.png' /></a>
				<img class='preloader none' src='/css/fastest/table/ta_preloader.gif'/>
				
				<? /* <a href='/companies/position_opp/<?php echo $company_id;?>/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>'/' class='opp_setting' title='Nadefinovani OPP'>OPP</a> */ ?>
			<?php if ($logged_user['CmsGroup']['id'] == 1):?>
				<a title='Odstranit pracovní pozici <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta trash remove_position ' href='/companies/position_trash/<?php echo $company_id;?>/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>'/>remove_pozice</a>
			<?php endif;?>
			</div>	
			</td>
			<td colspan='3'>
				<table cellspacing='0' cellpadding='0'>
				<?php foreach ($work_position_item['CompanyMoneyValidity'] as $key => $platnost):?>
					<tr class='<?php echo ($key != 0)?'none hidden_tr':'';?>'>			
						<td style='width:125px'>
							<span style='width:80px; display:block; float:left;'><?php echo $platnost['fakturacka']; echo ' '.($stat_id['Company']['stat_id'] == 1 ? 'CZK' : 'EUR')?></span>
							<?php if ($platnost['platnost_do'] == '0000-00-00'):?>
							<!--a title='Změna fakturované částky pro pozici <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta fakturacka' href='/companies/work_money_validity_edit/<?php echo $platnost['id'];?>'/>change_fakturacka</a-->
							<?php endif;?>
						</td>
						<td style='width:138px'><?php echo $fastest->czechDate($platnost['platnost_od']);?> -> <?php echo $fastest->czechDate($platnost['platnost_do']);?></td>
						<td>
							<table cellspacing='0' cellpadding='0' style='width:230px'>
							<?php foreach ($platnost['NewMoneyItem'] as $money_item):?>
								<tr class='td_kalkulacka'>
									<td style='width:80px'>
										<a title='Náhled odměny pozice <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta money edit_money ' href='/companies/work_money_edit_new/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>/<?php echo $platnost['id'];?>/<?php echo $money_item['id'];?>/-1/'/>money</a>
									<?php if ($logged_user['CmsGroup']['id'] == 1):?>
										<!--
                                        <a title='Odstranit kalkulaci pozice <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta trash money_item_trash ' href='/companies/money_item_trash/<?php echo $company_id?>/<?php echo $money_item['id'];?>'/>money_item_trash</a>
									   -->
                                    <?php endif;?>
									</td>
								</tr>
							<?php endforeach;?>
							</table>
						</td>
					</tr>
				<?php endforeach;?>
				</table>
			</td>
		</tr>
		<?php endforeach;?>
		<?php else:?>
		<tr>
			<td colspan='5'>K této společnosti nebyla nadefinovány (nové) pracovní pozice</td>
		</tr>
		<?php endif;?>
	</table>
	</div>
	<div class="domtabs field" >
		<?php echo $this->renderElement('../companies/work_position/index_test');?>
	</div>
    	<div class="domtabs field" >
		<?php echo $this->renderElement('../companies/work_position/index_new');?>
	</div>
</div>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='ListCompanyWorkPositionClose' class='button'/>
	</div>
</div>
<?php //pr($work_position_list);?>
<script>
// domtabs init
	var domtab = new DomTabs({'className':'admin_dom2'}); 

$('CompanyWorkPositionObal').getElements('.add_subitem').addEvent('click', function(e){
	new Event(e).stop();
});




// ostraneni money_item
$$('.money_item_trash').addEvent('click', function(e){
	new Event(e).stop();
	if (confirm('Opravdu si přejete odstranit tuto kalkulaci?'))
		new Request.HTML({
			url: this.getProperty('href'),		
			update: $('domwin_pozice').getElement('.CB_ImgContainer')
		}).send();
});

// ostraneni money_item testu
$$('.money_item_trash_test').addEvent('click', function(e){
	new Event(e).stop();
	if (confirm('Opravdu si přejete odstranit tuto kalkulaci?'))
		new Request.HTML({
			url: this.getProperty('href'),
			update: $('domwin_pozice').getElement('.CB_ImgContainer'),
			onComplete: function(){
				domtab.goTo(1);
			}
		}).send();
});

// ostraneni platnosti
$$('.remove_platnost').addEvent('click', function(e){
	new Event(e).stop();
	if (confirm('Opravdu si přejete odstranit tuto platnost?'))
		new Request.HTML({
			url: this.getProperty('href'),
			update: $('domwin_pozice').getElement('.CB_ImgContainer')
		}).send();
});

// odstraneni pracovni pozice
$$('.remove_position').addEvent('click', function(e){
	new Event(e).stop();
	if (confirm('Opravdu si přejete odstranit tuto pracovní pozici?'))
		new Request.JSON({
			url: this.getProperty('href'),
			//update: $('domwin_pozice').getElement('.CB_ImgContainer'),
			onComplete: function(json){
				if(json.result == false)
					alert('Tato kategorie má subkategorii, smažte předtím všechny subkategorie');
				else		
					domwin.loadContent('domwin_pozice');
			}
		}).send();
});

$('CompanyWorkPositionObal').getElements('.show_hide_other_tr').addEvent('click', function(e){
	new Event(e).stop();
	var tr = this.getParent('tr'), table = tr.getElement('table'), trs = table.getElements('.hidden_tr');
	if (trs && trs[0]){
		if(trs[0].hasClass('none')){
			trs.removeClass('none');
			this.addClass('close_list');
			this.removeClass('open_list')
		} else {	
			trs.addClass('none');
			this.removeClass('close_list');
			this.addClass('open_list')
		}
	}
	
});


// kliknuti na tlacitko edit
$('CompanyWorkPositionObal').getElements('.edit_item').addEvent('click', function(e){
	new Event(e).stop();
	var tr = this.getParent('tr');
	var input = tr.getElement('.editable_input');
	input.old_value = input.value;
	tr.getElements('.accept_change, .cancel_change').removeClass('none');
	input.removeProperty('readonly');
	input.removeClass('invisible');
	tr.getElements('.edit_item,.remove_position, .add_subitem').addClass('none');
});

//kliknuti na tlacitko cancel 
$('CompanyWorkPositionObal').getElements('.cancel_change').addEvent('click', function(e){
	new Event(e).stop();
	var tr = this.getParent('tr'), input = tr.getElement('.editable_input');
	
	tr.getElements('.accept_change, .cancel_change').addClass('none');
	tr.getElements('.edit_item, .add_subitem').removeClass('none');
	input.value = input.old_value;
	input.setProperty('readonly','readonly');
	input.addClass('invisible');
});

// kliknuti na tlacitko accept
$('CompanyWorkPositionObal').getElements('.accept_change').addEvent('click', function(e){
	new Event(e).stop();
	var tr = this.getParent('tr'), input = tr.getElement('.editable_input');
	input.addClass('invisible');
	tr.getElements('.accept_change, .cancel_change').addClass('none');
	tr.getElement('.preloader').removeClass('none');
	new Request.JSON({
		url: this.getProperty('href') + input.value,
		onComplete: function(JSON){
			tr.getElement('.preloader').addClass('none');
			tr.getElements('.edit_item, .add_subitem').removeClass('none');
			input.setProperty('readonly','readonly');
		}
	}).send();
});


	$('ListCompanyWorkPositionClose').addEvent('click', function(e){new Event(e).stop(); domwin.closeWindow('domwin_pozice')});
	
	$('CompanyWorkPositionObal').getElements('.edit, .add_subitem, .fakturacka, .opp_setting').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_work_position_edit',
			sizes		: [580,320],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
	
	$('CompanyWorkPositionObal').getElements('.money, .money_add').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_work_money',
			sizes		: [780,580],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>