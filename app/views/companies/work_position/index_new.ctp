	<br /><table class='table' id='table_kontakty'>
		<tr>
			<th>Název</th>
			<th style='width:80px'></th>
			<th></th>
		</tr>
		<?php if (isset($work_position_list_test_new) && count($work_position_list_test_new) >0):?>
		<?php foreach($work_position_list_test_new as $work_position_item):?>
		<tr>
			<td class='edit_name_td' style='width:200px; vertical-align:top;'>
			<div style='position:relative; height:100%; width:100%;'>
				<a href='#show' class='show_hide_other_tr ta open_list' style='position:absolute; left:-9px; top:0px;'>D</a>
				<?php echo (($work_position_item['CompanyWorkPosition']['parent_id']  != -1)?'<span style="float:left; margin:5px 0px 0px 5px;">->  </span>':'');?>
				<input class='editable_input invisible long68' type='text' value='<?php echo $work_position_item['CompanyWorkPosition']['name'];?>' readonly='readonly' />
				<a href='#' class='edit_item ' title='Editovat název pozice <?php echo $work_position_item['CompanyWorkPosition']['name'];?>'><img src='/css/fastest/table/ta_edit.png' /></a>
				<a href='/companies/position_change_name/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>/' class='accept_change none'><img src='/css/fastest/table/ta_accept.png' /></a>
				<a href='#' class='cancel_change none'><img src='/css/fastest/table/ta_cancel.png' /></a>
				<img class='preloader none' src='/css/fastest/table/ta_preloader.gif'/>
				<a title='Odstranit pracovní pozici <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta trash remove_position ' href='/companies/position_trash/<?php echo $company_id;?>/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>'/>remove_pozice</a>
			</div>	
			</td>
			<td>
                <?php if(isset($work_position_item['NewMoneyItem']) && isset($work_position_item['NewMoneyItem']['id'])){
                   echo $html->link('Upravit kalkulaci','/companies/work_money_edit_new/'.$work_position_item['CompanyWorkPosition']['id'].'/-1/'.$work_position_item['NewMoneyItem']['id'],array('class'=>'ta money edit_money'));
                 }
                 else{   
                ?>
                <a style='width: 150px; background-position: left center; text-indent: 21px;' title='Nová kalkulace pro pozici <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta money_add' href='/companies/work_money_edit_new/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>/-1/'/>Přidat kalkulaci</a><br />			
			    <?php } ?> 
            </td>
            <td style='width:150px;'>
			    <?php if ($logged_user['CmsGroup']['id'] == 1 || $logged_user['CmsGroup']['id'] == 5 || (isset($permission['access_to_autorizate_money_item']) && $permission['access_to_autorizate_money_item'] == 1)){?>
	               <a style='width: 150px; background-position: left center; text-indent: 21px;' title='Autorizovat kalkulace pro pozici <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta stop0 new_money_auth' href='/companies/new_work_money_auth/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>/'/>Autorizace</a><br />
               	<?php }?>
                   <a style='width: 150px; background-position: left center; text-indent: 21px;' title='Žádost o autorizaci kalkulace pro pozici <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta stop0 require_money_auth' href='/companies/require_work_money_auth/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>/'/>Žádost o autorizaci</a>
            </td>
		</tr>
		<?php endforeach;?>
		<?php else:?>
		<tr>
			<td colspan='5'>K této společnosti nebyla nadefinována (nová) testovací pracovní pozice</td>
		</tr>
		<?php endif;?>
	</table>
<script>
// ostraneni money_item
$$('.new_money_auth').addEvent('click', function(e){
	new Event(e).stop();
	if (confirm('Opravdu si přejete autorizovat tuto kalkulaci?'))
		        domwin.newWindow({
					id			: 'domwin_work_position_auth',
					sizes		: [580,320],
					scrollbars	: true,
					title		: this.title,
					languages	: false,
					type		: 'AJAX',
					ajax_url	: this.href,
					closeConfirm: true,
					max_minBtn	: false,
					modal_close	: false,
					remove_scroll: false
				}); 
});
</script>