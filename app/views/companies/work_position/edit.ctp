<form action='/companies/work_position_add/' method='post' id='company_work_position_edit_formular'>
	<?php echo $htmlExt->hidden('CompanyWorkPosition/id');?>
	<?php echo $htmlExt->hidden('CompanyWorkPosition/company_id');?>
	<fieldset>
		<legend>Zadejte údaje</legend>
		<?php echo $htmlExt->input('CompanyWorkPosition/name',array('tabindex'=>1,'label'=>'Název pozice','class'=>'long','label_class'=>'long'));?> <br class="clear">
		<?php echo $htmlExt->textarea('CompanyWorkPosition/text',array('tabindex'=>7,'label'=>'Komentář','class'=>'long','label_class'=>'long'));?> <br class="clear">
	</fieldset>
	<fieldset>
		<legend>Platnosti</legend>
		<?php echo $htmlExt->input('CompanyMoneyValidity/platnost_od',array('tabindex'=>1,'label'=>'Platnost od','class'=>'long','label_class'=>'long'));?> <br class="clear">
		<?php echo $htmlExt->input('CompanyMoneyValidity/platnost_do',array('tabindex'=>1,'label'=>'Platnost do','class'=>'long','label_class'=>'long'));?> <br class="clear">
	</fieldset>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditCompanyWorkPositionSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyWorkPositionClose'/>
	</div>
</form>
<script>
	if ($('CompanyWorkPositionChange'))
		$('CompanyWorkPositionChange').addEvent('click', function(){
			if (this.checked) $('company_work_position_edit_formular').getElement('.hidden_field').removeClass('none');
			else $('company_work_position_edit_formular').getElement('.hidden_field').addClass('none');	
		});

	$('AddEditCompanyWorkPositionClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_work_position_edit');});
	
	$('AddEditCompanyWorkPositionSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('company_work_position_edit_formular');
		if (valid_result == true){
			new Request.HTML({
				url:$('company_work_position_edit_formular').action,		
				update: $('domwin_pozice').getElement('.CB_ImgContainer'),
				onComplete:function(){
					domwin.closeWindow('domwin_work_position_edit');
				}
			}).post($('company_work_position_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('company_work_position_edit_formular',{
		'CompanyWorkPositionName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
		}
	});
	validation.generate('company_work_position_edit_formular',<?php echo (isset($this->data['CompanyWorkPosition']['id']))?'true':'false';?>);
</script>