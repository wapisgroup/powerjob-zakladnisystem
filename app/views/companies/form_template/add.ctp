<form action='/companies/form_template_add/' method='post' id='company_activity_edit_formular'>
			<?php echo $htmlExt->hidden('CompanyFormTemplate/company_id');?>
			<fieldset>
				<legend>Prosím vyplňte následující údaje</legend>
				<div class='sll100'>
						<?php echo $htmlExt->selectTag('CompanyFormTemplate/form_template_group_id',$group_list,null,array('tabindex'=>3,'label'=>'Skupina'));?> <br class="clear">
						<?php echo $htmlExt->selectTag('CompanyFormTemplate/parent_id',array(),null,array('tabindex'=>3,'label'=>'Šablona'));?> <br class="clear">
				</div>
				<br />
			</fieldset>
	
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditCompanyActivitySaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyActivityClose'/>
	</div>
</form>
<script>
    if ($('CompanyFormTemplateFormTemplateGroupId'))
		$('CompanyFormTemplateFormTemplateGroupId').ajaxLoad('/companies/ajax_load_form_template_group/',['CompanyFormTempleateParentId']);
	
	$('AddEditCompanyActivityClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_aktivity_add');});
	
	$('AddEditCompanyActivitySaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('company_activity_edit_formular');
		if (valid_result == true){
            button_preloader($('AddEditCompanyActivitySaveAndClose'));
			new Request.JSON({
				url:$('company_activity_edit_formular').action,		
				onComplete: function(json){
	   					if (json) {
	   						if (json.result === true) {
	   							domwin.loadContent('domwin');
	   							domwin.closeWindow('domwin_aktivity_add');
                                button_preloader_disable($('AddEditCompanyActivitySaveAndClose'));
	   						}
	   						else {
	   							alert(json.message);
	   						}
	   					}
	   					else {
	   						alert('Chyba apliakce');
	   					}
	   				}
			}).post($('company_activity_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('company_activity_edit_formular',{
		'CompanyFormTemplateParentId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vybrat šablonu'},
		}
	});
	validation.generate('company_activity_edit_formular',<?php echo (isset($this->data['CompanyActivity']['id']))?'true':'false';?>);
	

</script>