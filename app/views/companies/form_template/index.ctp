<div class="win_fce top">
<a href='/companies/form_template_add/<?php echo $company_id;?>' class='button add' id='company_contact_add_new' title='Přidání nové šablony'>Přidat šablony</a>
</div>
<table class='table' id='table_kontakty'>
	<tr>
		<th>Název</th>
		<th>Skupina</th>
		<th>Stav</th>
		<th>Upraveno</th>
		<th>Vytvořeno</th>
		<th width="16px">#</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($form_list) && count($form_list) >0):?>
	<?php foreach($form_list as $item):?>
	<tr>
		<td><?php echo $item['CompanyFormTemplate']['name'];?></td>
		<td><?php echo $item['FormTemplateGroup']['name'];?></td>
		<td><?php echo $item['CompanyFormTemplate']['history'] == 1 ? 'Archivováno' : 'Aktivní'; ?></td>
		<td><?php echo $fastest->czechDateTime($item['CompanyFormTemplate']['updated']);?></td>
		<td><?php echo $fastest->czechDate($item['CompanyFormTemplate']['created']);?></td>
		<td><?php echo $fastest->status_to_ico2($item['CompanyFormTemplate']['change'],'change_form-Globální šablona byla změněna!',true);?></td>
		<td>
			<a title='Editace šablony "<?php echo $item['CompanyFormTemplate']['name'];?>"' 	class='ta edit' href='/companies/form_template_edit/<?php echo $company_id;?>/<?php echo $item['CompanyFormTemplate']['id'];?>'/>edit</a>
			<?php if($item['CompanyFormTemplate']['change'] == 1){?>
            <a title='Aktualizovat šablonu'class='ta refresh' href='/companies/form_template_refresh/<?php echo $item['CompanyFormTemplate']['id'];?>'/>refresh</a>
		    <?php } ?>            
            <?php if(in_array(6,array(1,5))){?>
            <a title='Odstranit položku'class='ta trash' href='/companies/form_template_trash/<?php echo $company_id;?>/<?php echo $contact_item['CompanyActivity']['id'];?>'/>trash</a>
		    <?php } ?>
            <?php 
				$status = $status1 = $item['CompanyFormTemplate']['status'];
				$status = '_new'.$item['CompanyFormTemplate']['status'];
				echo "<a title='Změnit stav' class='ta status$status' onclick='return false;' href='/companies/company_template_status/".$item['CompanyFormTemplate']['id']."/$status1/'>Změna stavu</a>";
            ?>
        </td>
	</tr>
	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='7'>K této společnosti nebyly nadefinovány žádne formulářové šablony</td>
	</tr>
	<?php endif;?>
</table>
<?php // pr($contact_list);?>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='ListCompanyActivityClose' class='button'/>
	</div>
<script>
	$('ListCompanyActivityClose').addEvent('click', function(e){new Event(e).stop(); domwin.closeWindow('domwin');});

	$('table_kontakty').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tuto aktivitu?')){
			new Request.HTML({
				url:this.href,
				update: $('domwin').getElement('.CB_ImgContainer'),
				onComplete: function(){}
			}).send();
		}
	});
    
    //START change status new
	$('table_kontakty').getElements('.status_new0, .status_new1').addEvent('click',function(e){
		new Event(e).stop();
		new Request.JSON({
			url:this.href,
			onComplete:(function(){
				var old_status = this.href.substring(this.href.length-2,this.href.length-1);
				var new_status = (old_status==0)?1:0;
				this.href = this.href.substring(0,this.href.length-2) + new_status + '/';
				this.removeClass('status_new'+old_status);
				this.addClass('status_new'+new_status);
			}).bind(this)
		}).send();
	});
	//END change status
    
    $('table_kontakty').getElements('.refresh').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm("Opravdu si přejet aktualizovat tuto šablonu?\n\nŠablona bude převedena do historie a nahrazena aktualiní verzí.")){
			new Request.JSON({
				url:this.href,
				onComplete: function(json){
				        if (json) {
	   						if (json.result === true) {
	   							domwin.loadContent('domwin');
	   						}
	   						else {
	   							alert(json.message);
	   						}
	   					}
	   					else {
	   						alert('Chyba apliakce');
	   					}
				}
			}).send();
		}
	});
	
	$('domwin').getElements('.add').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_aktivity_add',
			sizes		: [400,250],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
    
    $('table_kontakty').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_form_template_edit',
			sizes		: [1000,1000],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
    
</script>