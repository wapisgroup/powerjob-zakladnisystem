<div id="myDisable">
<form action='/companies/nabor_edit/' method='post' id='company_nabor_edit_formular'>
	<div class="domtabs admin_dom_links <?php if($print) echo "noprint";?>">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Souhrné info</a></li>
			<li class="ousko"><a href="#krok2">Pracovište</a></li>
			<li class="ousko"><a href="#krok3">Parametry pozice</a></li>
			<li class="ousko"><a href="#krok4">Místa</a></li>
			<li class="ousko"><a href="#krok5">Mzdové podmínky</a></li>
			<li class="ousko"><a href="#krok6">Nástup</a></li>
			<li class="ousko"><a href="#krok7">Fotografie</a></li>
			<li class="ousko"><a href="#krok8">Kategorie odměny</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<?php echo $this->renderElement('../companies/requirements_for_recruitment/souhrn');?>
		</div>	
		<div class="domtabs field ">
			<?php echo $this->renderElement('../companies/requirements_for_recruitment/pracoviste');?>
		</div>
		<div class="domtabs field ">
			<?php echo $this->renderElement('../companies/requirements_for_recruitment/parametry');?>
		</div>
		<div class="domtabs field ">
			<fieldset>
			<div class='sll'>
				<?php echo $htmlExt->input('RequirementsForRecruitment/count_of_free_position', array('label'=>'Počet volných míst'));?> <br class='clear' />
				<?php echo $htmlExt->input('RequirementsForRecruitment/count_of_substitute', array('label'=>'Míst pro náhradníky'));?> <br class='clear' />
			</div>
			</fieldset>
		</div>
		<div class="domtabs field ">
			<?php echo $this->renderElement('../companies/requirements_for_recruitment/mzdove_podminky');?>
		</div>
		<div class="domtabs field ">
			<?php echo $this->renderElement('../companies/requirements_for_recruitment/doklady');?>
		</div>
		<div class="domtabs field <?php if($print) echo "noprint";?>">
			<?php echo $this->renderElement('../companies/requirements_for_recruitment/fotografie');?>
		</div>
		<div class="domtabs field ">
			<?php echo $this->renderElement('../companies/requirements_for_recruitment/odmeny');?>
		</div>
	</div>
	<p class="text_center <?php if($print) echo "noprint";?>">
			<a href="show/<?php echo $this->data['RequirementsForRecruitment']['id'];?>/1" target="_blank">Tisknout Souhrné info</a> | 
			<a href="show/<?php echo $this->data['RequirementsForRecruitment']['id'];?>/2" target="_blank">Tisknout Vše</a>
		</p>
	<div class='formular_action <?php if($print) echo "noprint";?>'>
		<input type='button' value='Zavřít' id='AddEditRequirementsForRecruitmentClose'/>
	</div>
</form>
</div>
<script>

	

<?php if(!$print) {?>
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('company_nabor_edit_formular').getElements('input,select,textarea').setProperty('disabled','disabled');
	$('AddEditRequirementsForRecruitmentClose').removeProperty('disabled');
	
	$('AddEditRequirementsForRecruitmentClose').addEvent('click',function(e){new Event(e).stop(); 
		domwin.closeWindow('domwin_show');
	});

<?php }else {?>

$('myDisable').getElements('label').each(function(item){
	next = item.getNext() ;
	var br = new Element('br');

	//console.log(item.getNext().get('tag'));
	//alert(item.getNext().get('tag'));
		if(next.get('tag') == 'textarea'){
			text = next.value;
			next.dispose();
			newtext = new Element('p').setHTML(text);
			newtext.inject(item,'after');
			br.inject(item,'after');
		}
		else if(next.get('tag') == 'input'){
			if(next.getProperty('type') == 'text'){
				text = next.value;
				next.dispose();
				newtext = new Element('span').setHTML(text);
				newtext.inject(item,'after');
			}
			else{
				next = next.getNext();
				text = (next.checked ? ' Ano' : ' Ne');
				next.dispose();
				newtext = new Element('span').setHTML(text);
				newtext.inject(item,'after');
			}
		}
		else if(next.get('tag') == 'select'){		
			text = next.getOptionText();
			next.dispose();
			newtext = new Element('span').setHTML(text);
			newtext.inject(item,'after');
		}

	});


	window.print();

<?php }?>

</script>