<fieldset>
<legend>Typ externiho naboru</legend>
<div class='sl3'>
<?php echo $htmlExt->selectTag('RequirementsForRecruitment/typ_externi_nabor', $typ_externi_nabor,(isset($this->data['RequirementsForRecruitment']['id'])?$this->data['RequirementsForRecruitment']['typ_externi_nabor']:2), array('label'=>'Typ požadavku'),null,false);?> 
<br class='clear' />

</div>
</fieldset>
<fieldset>
<legend>Publikování</legend>
<div class='sl3'>
<?php echo $htmlExt->selectTag('RequirementsForRecruitment/publikovani_typ', $publikovani_typ_list,null, array('label'=>'Typ publikování'),null,false);?> 

</div>
</fieldset>
<script>

var data = <?php echo isset($this->data["RequirementsForRecruitment"]["id"]) ? 1 : 0;?>;

// defaultne zaskrtni interni nabor pokud se zaklada nova karta
if(data==0) 
	$('RequirementsForRecruitmentPublikovaniTyp').value = 1;
<?php

if(in_array($CmsGroupId ,array(1,2,3,4,5)))
{
	$disable=array();
	if(in_array($CmsGroupId ,array(2,3,4))){
		?>
			$('RequirementsForRecruitmentPublikovaniTyp').getElement('option[value=2]').setProperty('disabled','disabled');	
			$('RequirementsForRecruitmentPublikovaniTyp').getElement('option[value=3]').setProperty('disabled','disabled');	<?php
		if($this->data["RequirementsForRecruitment"]["checkbox_interni_nabor"]==1){
			?>
			$('RequirementsForRecruitmentPublikovaniTyp').setProperty('disabled','disabled');
	<?php
		}
	}
	else{ 
		?>$('RequirementsForRecruitmentPublikovaniTyp').removeProperty('disabled','disabled');	<?php
	}
}
?>
</script>