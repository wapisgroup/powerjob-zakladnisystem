<fieldset>
				<legend>Informace a doklady potřebné při nástupu</legend>
				<div class='sll'>
					<?php echo $htmlExt->inputDateTime('RequirementsForRecruitment/datetime_of_start', array('label'=>'Datum a hodina nástupu'));?> <br class='clear' />
				</div>
				<div class='slr'>
					
				</div>	
				<br />
					<?php echo $htmlExt->textarea('RequirementsForRecruitment/komentar_datum_nastupu', array('label'=>'Komentář k datumu nástupu', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />
				<div class='sll'>
					<?php echo $htmlExt->selectTag('RequirementsForRecruitment/entrance_examination', $ano_ne_list,null, array('label'=>'Vstupní zkouška'));?> <br class='clear' />
					<?php echo $htmlExt->inputDateTime('RequirementsForRecruitment/datetime_of_entrance examination', array('label'=>'Datum a hodina vstupní zkoušky'));?> <br class='clear' />
				</div>
				
				<br class='clear'/>
				<?php echo $htmlExt->textarea('RequirementsForRecruitment/examination_description', array('label'=>'Popis vstupní zkoušky', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />
				<div class='sll'>
					<label>OP:</label><?php echo $htmlExt->checkbox('RequirementsForRecruitment/checkbox_op', array('label'=>'OP'));?> <br class='clear' />
					<label>Zápočtový list:</label><?php echo $htmlExt->checkbox('RequirementsForRecruitment/checkbox_zl', array('label'=>'Zápočtový list'));?> <br class='clear' />
					<label>Kvalifikační doklady:</label><?php echo $htmlExt->checkbox('RequirementsForRecruitment/checkbox_kd', array('label'=>'Kvalifikační doklady'));?> <br class='clear' />
				</div>
				<div class='slr'>
					<label>Výuční list:</label><?php echo $htmlExt->checkbox('RequirementsForRecruitment/checkbox_vl', array('label'=>'Výuční list'));?> <br class='clear' />
					<label>Zdravotní prohlídka:</label><?php echo $htmlExt->checkbox('RequirementsForRecruitment/checkbox_zp', array('label'=>'Zdravotní prohlídka'));?> <br class='clear' />
				</div>
				<br class='clear'/>
				<?php echo $htmlExt->textarea('RequirementsForRecruitment/jine_doklady', array('label'=>'Co ještě mít sebou k nástupu', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />
			</fieldset>