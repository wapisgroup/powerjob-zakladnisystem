<fieldset>
	<legend>Kontakty a pracoviště</legend>
	<div class='sll'>
		<?php echo $htmlExt->selectTag('RequirementsForRecruitment/company_id', $company_list,null, array('label'=>'Firma'));?>  <br class='clear' />
	</div>
	<div class='slr'>
		<?php echo $htmlExt->input('RequirementsForRecruitment/job_city', array('label'=>'Pracoviště město'));?> <br class='clear' />
	</div><br class='clear' />
	<div>
	<?php echo $htmlExt->textarea('RequirementsForRecruitment/job_description', array('label'=>'Pracoviště popis', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />
	</div>
	<div class='sll'>
		<?php echo $htmlExt->input('RequirementsForRecruitment/cm_name', array('label'=>'Kontakt CM'));?> <br />
		<?php echo $htmlExt->var_text('RequirementsForRecruitment/cm_tel', array('label'=>'CM Tel.','class'=>'read_info','value'=>''));?><br />
		<?php echo $htmlExt->hidden('RequirementsForRecruitment/company_contact_id');?> <br />
	</div>
	<div class='slr'>
	
		<?php echo $htmlExt->var_text('RequirementsForRecruitment/cm_email', array('label'=>'CM email','class'=>'read_info','value'=>''));?><br />
	</div>
</fieldset>