<form action='/companies/nabor_edit/' method='post' id='company_nabor_edit_formular'>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Pracovište</a></li>
			<li class="ousko"><a href="#krok2">Parametry pozice</a></li>
			<li class="ousko"><a href="#krok3">Místa</a></li>
			<li class="ousko"><a href="#krok4">Mzdové podmínky</a></li>
			<li class="ousko"><a href="#krok5">Nástup</a></li>
			<li class="ousko"><a href="#krok6">Fotografie</a></li>
			<li class="ousko"><a href="#krok7">Publikování</a></li>
			<li class="ousko"><a href="#krok8">Kategorie odměny</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<?php echo $this->renderElement('../companies/requirements_for_recruitment/pracoviste');?>
		</div>
		<div class="domtabs field">
			<?php echo $this->renderElement('../companies/requirements_for_recruitment/parametry');?>
		</div>
		<div class="domtabs field">
			<fieldset>
			<div class='sll'>
				<?php echo $htmlExt->input('RequirementsForRecruitment/count_of_free_position', array('label'=>'Počet volných míst'));?> <br class='clear' />
				<?php echo $htmlExt->input('RequirementsForRecruitment/count_of_substitute', array('label'=>'Míst pro náhradníky'));?> <br class='clear' />
			</div>
			</fieldset>
		</div>
		<div class="domtabs field">
			<?php echo $this->renderElement('../companies/requirements_for_recruitment/mzdove_podminky');?>
		</div>
		<div class="domtabs field">
			<?php echo $this->renderElement('../companies/requirements_for_recruitment/doklady');?>
		</div>
		<div class="domtabs field">
			<?php echo $this->renderElement('../companies/requirements_for_recruitment/fotografie');?>
		</div>
		<div class="domtabs field">
			<?php echo $this->renderElement('../companies/requirements_for_recruitment/publikovani');?>
		</div>
		<div class="domtabs field">
			<?php echo $this->renderElement('../companies/requirements_for_recruitment/odmeny');?>
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditRequirementsForRecruitmentSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditRequirementsForRecruitmentClose'/>
	</div>
</form>
<script>

	var domtab = new DomTabs({'className':'admin_dom'}); 
	var externi = <?php echo (isset($disabledSave) && $disabledSave!="") ? true : 'false';?>;

	if(externi) // pokud je zamezen save skryj tl. save
		$('AddEditRequirementsForRecruitmentSaveAndClose').addClass('none');
/*
	$('RequirementsForRecruitmentCompanyContactId').addEvent('change', function(){
		new Request.JSON({
			url: '/companies/load_cms_user_kontakt/' + this.value,
			onComplete: function(json){
				if(json){
					$('RequirementsForRecruitmentPhone').value = json.CmsUser.telefon;
					$('RequirementsForRecruitmentEmail').value = json.CmsUser.email;
				} else {
					$('RequirementsForRecruitmentPhone').value = '';
					$('RequirementsForRecruitmentEmail').value = '';
				}
			}
		}).post();
	});
*/
	$('RequirementsForRecruitmentSettingCareerItemId').addEvent('change', function(e){
		new Request.JSON({
			url: '/companies/load_position_group/' + this.value,
			onComplete: function(json){
				if (json){
					if (json.result == true){
						$('RequirementsForRecruitmentSettingCareerCatId').value = json.selected;
						if(json.selected != 0)
							setPrize($('RequirementsForRecruitmentSettingCareerCatId'));

					} else {
						alert(json.message);
					}
				} else {
					alert('Chyba aplikace');
				}
			}
		}).send();
	});
	
	$('RequirementsForRecruitmentCompanyId').addEvent('change', function(){
		new Request.JSON({
			url: '/companies/load_profesion_by_company/' + this.value,
			onComplete: function(json){
				if (json){
					if (json.result === true){
						// pozice dle kalkulace
						var select = $('RequirementsForRecruitmentCompanyWorkPositionId');
						select.empty();
						new Element('option',{title:'', value:''}).setHTML('').inject(select);
						$each(json.data.kalkulace, function(item,key){
							new Element('option',{title:item, value:key}).setHTML(item).inject(select);
						});
						
						// CM kontakt
						$('RequirementsForRecruitmentJobCity').value = json.data.mesto;
						if(json.data.mesto != null)
							$('RequirementsForRecruitmentJobCity').removeClass('require');
						else
							$('RequirementsForRecruitmentJobCity').addClass('invalid');

						$('RequirementsForRecruitmentCompanyContactId').value = json.data.company_contact_id;
						$('RequirementsForRecruitmentCmName').value = json.data.cm_name;
						$('RequirementsForRecruitmentCmTel').setHTML(json.data.cm_tel);
						$('RequirementsForRecruitmentCmEmail').setHTML(json.data.cm_email);
						
						
					} else {
						
					}
				} else {
					alert('chyba aplikace');
				}
			}
		}).send();
	});
	
	
	$('AddEditRequirementsForRecruitmentClose').addEvent('click',function(e){new Event(e).stop(); 
	<?php if (isset($z_reportu)):?>
		domwin.closeWindow('domwin');
	<?php else:?>
		domwin.closeWindow('domwin_nabor_edit');
	<?php endif;?>
	});
	
	$('AddEditRequirementsForRecruitmentSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('company_nabor_edit_formular');
		if (valid_result == true){
			<?php if (isset($z_reportu)):?>
			var request = new Request.JSON({url:$('company_nabor_edit_formular').action, onComplete:function(){domwin.closeWindow('domwin');click_refresh($('RequirementsForRecruitmentId').value);}})			
			<?php else:?>
			var request = new Request.HTML({url:$('company_nabor_edit_formular').action, update: $('domwin_nabor_list').getElement('.CB_ImgContainer'), onComplete:function(){domwin.closeWindow('domwin_nabor_edit');}})			
			<?php endif;?>
			request.post($('company_nabor_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});

	validation.define('company_nabor_edit_formular',{
		'RequirementsForRecruitmentName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit požadovanou profesi'}
		},
		'RequirementsForRecruitmentNaplnPrace': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit náplň práce'}
		},
		'RequirementsForRecruitmentSalary': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit mzda od-do'}
		},
		'RequirementsForRecruitmentSettingShiftWorkingId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit směnnost'}
		},
		'RequirementsForRecruitmentJobCity': {
			'testReq': {'condition':'not_empty','err_message':'Není zvoleno město'}
		}
	});
	validation.generate('company_nabor_edit_formular',<?php echo (isset($this->data['RequirementsForRecruitment']['id']))?'true':'false';?>);
	

</script>