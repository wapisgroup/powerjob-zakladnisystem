<div class="win_fce top">
<a href='/companies/invoice_item_edit/<?php echo $company_id;?>' class='button edit_invoice_item' id='company_invoice_item_add'>Přidat Fakturu</a>
</div>
<table class='table' id='table_kontakty'>
	<tr>
		<th>Měsíc</th>
		<th>Datum splatnosti</th>
		<th>Částka</th>
		<th>Bez DPH</th>
		<th>S DPH</th>
		<th>Měna</th>
		<th>Datum úhrady</th>
		<th>Po splatnosti</th>
		<th>Komentář</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($invoice_item_list) && count($invoice_item_list) >0):?>
	<?php foreach($invoice_item_list as $invoice_item):?>
	<tr>
		<td><?php echo date('Y-m',strtotime($invoice_item['CompanyInvoiceItem']['za_mesic']));?></td>
		<td><?php echo $fastest->czechDate($invoice_item['CompanyInvoiceItem']['datum_splatnosti']);?></td>
		<td><?php echo $invoice_item['CompanyInvoiceItem']['cena'];?></td>
		<td><?php echo $invoice_item['CompanyInvoiceItem']['cena_bez_dph'];?></td>
		<td><?php echo $invoice_item['CompanyInvoiceItem']['spolu_s_dph'];?></td>
		<td>
			<?php
				if($invoice_item['CompanyInvoiceItem']['currency_id'] == 0){
					if ($stat_id == 1){
						$currency_id = 1;
					} else {
						$currency_id = 2;
					}
				} else {
					$currency_id = $invoice_item['CompanyInvoiceItem']['currency_id'];
				}
				echo $currency_list2[$currency_id];
			?>
		</td>
		<td><?php echo $fastest->czechDate($invoice_item['CompanyInvoiceItem']['datum_uhrady']);?></td>
		<td><?php 
			$po_splatnosti = (strtotime(date('Y-m-d')) - strtotime($invoice_item['CompanyInvoiceItem']['datum_splatnosti'])) / 3600 /24;
			
			if ($po_splatnosti > 0) {
				if ($invoice_item['CompanyInvoiceItem']['datum_uhrady'] != '0000-00-00')
					echo 'Uhrazeno';
				else 
					echo $po_splatnosti;
			} else
				echo '';
		?></td>
		<td><?php echo $invoice_item['CompanyInvoiceItem']['comment'];?></td>
		
		<td>
			<a title='Editace položky' 	class='ta edit edit_invoice_item' href='/companies/invoice_item_edit/<?php echo $company_id;?>/<?php echo $invoice_item['CompanyInvoiceItem']['id'];?>'/>edit</a>
		<?php if ($logged_user['CmsGroup']['id'] == 1):?>
			<a title='Do kosiku' 		class='ta trash' href='/companies/invoice_item_trash/<?php echo $company_id;?>/<?php echo $invoice_item['CompanyInvoiceItem']['id'];?>'/>trash</a>
		<?php endif;?>
		</td>
	</tr>
	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='5'>K této společnosti nebyla nadefinována faktura</td>
	</tr>
	<?php endif;?>
</table>
<?php // pr($contact_list);?>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='ListCompanyInvoiceItemClose' class='button'/>
	</div>
<script>
	$('ListCompanyInvoiceItemClose').addEvent('click', function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin_invoice_item');
	});


	
	$('domwin_invoice_item').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tuto fakturu?')){
			new Request.HTML({
				url:this.href,
				update: $('domwin_kontakty').getElement('.CB_ImgContainer'),
				onComplete: function(){}
			}).send();
		}
	});
	
	$('domwin_invoice_item').getElements('.edit_invoice_item').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_invoice_item_add',
			sizes		: [580,420],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>