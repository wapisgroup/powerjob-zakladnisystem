<form action='/companies/invoice_item_edit/' method='post' id='company_invoice_edit_formular'>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Fakturace</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<?php echo $htmlExt->hidden('CompanyInvoiceItem/id');?>
			<?php if (isset($from_report)):?>
			<fieldset>
				<legend>Informace o firmě</legend>
				<div class='sll'>
					<?php echo $htmlExt->selectTag('CompanyInvoiceItem/company_id',$company_list,null,array('label'=>'Firma','tabindex'=>1));?>
					<?php echo $htmlExt->input('Company/sm',array('readonly'=>'readonly','label'=>'SM','tabindex'=>3));?>
				</div>
				<div class='slr'>
					<?php echo $htmlExt->input('Company/ico',array('readonly'=>'readonly','label'=>'IČO','tabindex'=>2));?>
				</div>
			</fieldset>
			<?php else:?>
				<?php echo $htmlExt->hidden('CompanyInvoiceItem/company_id');?>
			<?php endif;?>
			<fieldset>
				<legend>Prosím vyplňte následující údaje</legend>
				<div class='sll'>
					<?php echo $htmlExt->input('CompanyInvoiceItem/za_mesic',array('tabindex'=>5,'label'=>'Za měsíc'));?> <br class="clear">
					<?php echo $htmlExt->inputDate('CompanyInvoiceItem/datum_uhrady',array('tabindex'=>7,'label'=>'Datum úhrady'));?> <br class="clear">				
					<?php echo $htmlExt->inputDate('CompanyInvoiceItem/datum_splatnosti',array('tabindex'=>9,'label'=>'Datum splatnosti'));?> <br class="clear">
				    <?php echo $htmlExt->input('CompanyInvoiceItem/pocet_hodin_firma',array('tabindex'=>11,'label'=>'Počet hodin firma'));?> <br class="clear">
	               <?php echo $htmlExt->input('CompanyInvoiceItem/pocet_hodin_atep',array('disabled'=>'disabled','class'=>'read_info','tabindex'=>13,'label'=>'Počet hodin systém'));?> <br class="clear">
	               <?php echo $htmlExt->input('CompanyInvoiceItem/hodnota_vh',array('class'=>'integer','tabindex'=>15,'label'=>'Hodnota VH'));?> <br class="clear">
				
                </div>
				<div class='slr'>
					<?php echo $htmlExt->input('CompanyInvoiceItem/cena_bez_dph',array('class'=>'integer','tabindex'=>6,'label'=>'Částka bez DPH'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyInvoiceItem/dph',array('class'=>'integer','tabindex'=>8,'label'=>'Dph'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyInvoiceItem/spolu_s_dph',array('class'=>'integer','tabindex'=>12,'label'=>'Částka s DPH'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyInvoiceItem/cena',array('disabled'=>'disabled','class'=>'read_info','tabindex'=>14,'label'=>'Částka systém'));?> <br class="clear">
					<?php echo $htmlExt->selectTag('CompanyInvoiceItem/currency_id',$currency_list2,null,array('tabindex'=>14,'label'=>'Měna'),null,false);?> <br class="clear">
				</div>
                <br />
				<?php echo $htmlExt->textarea('CompanyInvoiceItem/comment',array('tabindex'=>17,'label'=>'Komentář','class'=>'long','label_class'=>'long','style'=>'height:30px'));?><br class='clear'/>
			</fieldset>
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditCompanyInvoiceItemSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyInvoiceItemClose'/>
	</div>
</form>
<script>
var stat_id = null;

	$('CompanyInvoiceItemCompanyId').addEvent('change',function(e){
		new Event(e).stop();
		new Request.JSON({
			url: '/report_invoice_items/load_company_information/' + this.value,
			onComplete: (function(json){
				$('CompanySm').value = json.SalesManager.name;
				$('CompanyIco').value = json.Company.ico;
				stat_id = json.Company.stat_id;
                             
                prepocitej_cenu_bez_dph($('CompanyInvoiceItemSpoluSDph').value);             
                              
                if($('CompanyInvoiceItemZaMesic').value != '')
                    load_details(this.value,$('CompanyInvoiceItemZaMesic').value);
			}).bind(this)
		}).send();
	});
	
    

    $('company_invoice_edit_formular').getElements('.integer').inputLimit();
    
    /**
     * zmena nastaveni ceny s DPH, podle statu spocitej dph castku a zobraz cenu bez dph
     */
    $('CompanyInvoiceItemSpoluSDph').addEvent('change',function(e){
        if(this.value != ''){
           prepocitej_cenu_bez_dph(this.value);
        }
    });
    
    
    function prepocitej_cenu_bez_dph(s_dph){
        if(s_dph > 0){
             var dph = 0;
                
                if(stat_id == 1)
                    bez_dph = (s_dph/1.2).round(2);
                else if(stat_id == 2)
                    bez_dph = (s_dph/1.2).round(2);
            
            
            $('CompanyInvoiceItemDph').value = s_dph - bez_dph;
            $('CompanyInvoiceItemCenaBezDph').value = bez_dph;
        }
    }
    

    new MonthCal($('CompanyInvoiceItemZaMesic'),{onComplete: function (){
        load_details($('CompanyInvoiceItemCompanyId').value,$('CompanyInvoiceItemZaMesic').value);
	}});
    
    /**
     * nacteni castky a pocet hodin z fakturaci dane firmy
     * pri zmene roku a mesice
     */
    function load_details(company_id,date){
        if(company_id == '')
            return false;
    
		new Request.JSON({
			url: '/report_invoice_items/load_company_system_detail/'+company_id+'/' + date,
			onComplete: function(json){
			    if(json){
                    if(json.result == true){
                        $('CompanyInvoiceItemCena').value  = json.amount;
    				    $('CompanyInvoiceItemPocetHodinAtep').value  = json.pocet_hodin;
                    }
                    else{
                        alert('Chyba při načítání informací : Nenalezeny žádné docházky.');
                        $('CompanyInvoiceItemCena').value  = '';
    				    $('CompanyInvoiceItemPocetHodinAtep').value  = '';
                    }
                }
                else
                    alert('Chyba aplikace');          	
			}
		}).send();
    }
    
	
	var domtab = new DomTabs({'className':'admin_dom'}); 
	
	$('AddEditCompanyInvoiceItemClose').addEvent('click',function(e){
		new Event(e).stop(); 
		<?php if (isset($from_report)):?>
		domwin.closeWindow('domwin');
		<?php else:?>
		domwin.closeWindow('domwin_invoice_item_add');
		<?php endif;?>
	});
    
	$('AddEditCompanyInvoiceItemSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
        $('company_invoice_edit_formular').getElements('.read_info').removeProperty('disabled');
        
		valid_result = validation.valideForm('company_invoice_edit_formular');
        if($('CompanyInvoiceItemZaMesic').value != ''){
        		<?php if (isset($from_report)):?>
        			var request = new Request.JSON({url:$('company_invoice_edit_formular').action, onComplete:function(){click_refresh($('CompanyInvoiceItemId').value);domwin.closeWindow('domwin');}})			
        		<?php else:?>
        			var request = new Request.HTML({url:$('company_invoice_edit_formular').action, update: $('domwin_invoice_item').getElement('.CB_ImgContainer'), onComplete:function(){domwin.closeWindow('domwin_invoice_item_add');}})			
        		<?php endif;?>
        		if (valid_result == true){
        			request.post($('company_invoice_edit_formular'));
                     $('company_invoice_edit_formular').getElements('.read_info').setProperty('disabled','disabled');
        		} else {
        			var error_message = new MyAlert();
        			error_message.show(valid_result)
        		}
        }
        else
            alert('Musíte vyplnit datum!');
        
	});
	
	validation.define('company_invoice_edit_formular',{
		'CompanyInvoiceItemCompanyId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit firmu'},
		}
	});
	validation.generate('company_invoice_edit_formular',<?php echo (isset($this->data['CompanyInvoiceItem']['id']))?'true':'false';?>);
	
	<?php if (!isset($from_report)):?>
	$('CompanyInvoiceItemCompanyId').removeClass('require').addClass('valid');
	<?php endif;?>
</script>