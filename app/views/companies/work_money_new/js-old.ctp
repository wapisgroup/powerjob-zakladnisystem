<script>
domtab = new DomTabs({'className':'admin_dom'}); 

$('company_work_money_new_edit_formular').getElements('.integer').inputLimit();
$('AddEditCompanyWorkMoneyClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_work_money');});

$('company_money_item_settings_box').getElements('.add_setting').addEvent('click',function(e){
    e.stop();
    var select = this.getParent('div').getElement('select');
    	new Request.JSON({
				url:'/companies/make_money_item_setting/',		
				data: {
				    'typ':select.getProperty('rel'),
                    'money_item_type_id':select.value,
                    'company_work_position_id':$('NewMoneyItemCompanyWorkPositionId').value
				},
				onComplete:(function(json){
					if(json){
					   if(json.result == true){
					       var li = new Element('li',{'html':select.getOptionText()}).inject(this.getParent('div').getElement('.list_box'));
                           new Element('a',{'href':'/companies/delete_money_item_setting/'+json.id,'html':'Smazat','class':'delete_setting ta status0'}).inject(li).addEvent('click',remove_setting_from_list.bindWithEvent(this));
					   }
                       else if(json.message)
                        alert(json.message);
					}
				}).bind(this)
		}).send();
})	

$('company_money_item_settings_box').getElements('.delete_setting').addEvent('click',remove_setting_from_list.bindWithEvent(this));

/*
 * Odstraneni klienta z nominacni listiny
 */
function remove_setting_from_list(e){
    	e.stop();
		var obj = e.target; 
		new Request.JSON({
			url:obj.get('href'),
			onComplete: (function(json){
				if (json) {
				   	if (json.result === true) {				
						obj.getParent('li').dispose();
				   	}
				   	else {
				   		alert(json.message);
				   	}
				} else {
					alert('Chyba aplikace');
				}
			}).bind(this)
		}).send();
}


/**
 * Pridani jednotlive formy odmeny
 */
$('add_new_money_item_form').addEvent('click',function(e){
    e.stop();
    var stravenky = $('NewMoneyItemFormStravenky').getOptionText();
    var doprava = $('NewMoneyItemFormDoprava').getOptionText();
    	new Request.JSON({
				url:'/companies/add_new_money_item_form/',
                data: {
                    'company_work_position_id':$('NewMoneyItemCompanyWorkPositionId').value,
                    'stravenky':$('NewMoneyItemFormStravenky').value,
                    'doprava':$('NewMoneyItemFormDoprava').value,
                    'pp':($('NewMoneyItemFormPp').checked?1:0),
                    'do':($('NewMoneyItemFormDo').checked?1:0),
                    'zl':($('NewMoneyItemFormZl').checked?1:0),
                    'cash':($('NewMoneyItemFormCash').checked?1:0),
				},
				onComplete:(function(json){
					if(json){
					   if(json.result == true){
					       var li = new Element('li',{'html':'<strong>'+json.name+'</strong> - stravenky '+stravenky+' / doprava '+doprava+' '}).inject($('NewMoneyItemFormBox'));
                           new Element('a',{'href':'/companies/delete_money_item_form/'+json.id,'html':'Smazat','class':'delete_form ta status0'}).inject(li).addEvent('click',remove_from_money_item_form.bindWithEvent(this));
					   }
                       else if(json.message)
                        alert(json.message);
					}
				}).bind(this)
		}).send();
})	

$('NewMoneyItemFormBox').getElements('.delete_form').addEvent('click',remove_from_money_item_form.bindWithEvent(this));
function remove_from_money_item_form(e){
    	e.stop();
		var obj = e.target; 
		new Request.JSON({
			url:obj.get('href'),
			onComplete: (function(json){
				if (json) {
				   	if (json.result === true) {				
						obj.getParent('li').dispose();
				   	}
				   	else {
				   		alert(json.message);
				   	}
				} else {
					alert('Chyba aplikace');
				}
			}).bind(this)
		}).send();
}




//pro funkci ktera se vola az po nacteni loadcontent
	function load_content_param(){
        domtab.goTo(2);
    }

	$('AddEditCompanyWorkMoneySaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		
			$('company_work_money_new_edit_formular').getElements('input').removeProperty('disabled');
			var request = new Request.JSON({
					url: $('company_work_money_new_edit_formular').action, 
					onComplete: function(json){
					   if(json.result === true){  
					     domwin.loadContent('domwin_pozice',{'onComplete': 'load_content_param'}); 
						 domwin.closeWindow('domwin_work_money');
                       }
                       else
                        alert(json.message); 
					}
			});	
			request.post($('company_work_money_new_edit_formular'));
	});

</script>