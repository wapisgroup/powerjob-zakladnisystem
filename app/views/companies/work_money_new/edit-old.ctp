<form action='/companies/work_money_edit_new/' method='post' id='company_work_money_new_edit_formular'>
	<?php echo $htmlExt->hidden('NewMoneyItem/id');?>
	<?php echo $htmlExt->hidden('NewMoneyItem/company_id');?>
	<?php echo $htmlExt->hidden('NewMoneyItem/company_work_position_id');?>
	<?php echo $htmlExt->hidden('NewMoneyItem/company_money_validity_id');?>

	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
            <li class="ousko"><a href="krok1">Odměny</a></li>
            <li class="ousko"><a href="krok2">Formy mzdy</a></li>
            <li class="ousko"><a href="krok3">Nastaveni (Bonusy,srážky,příplatky)</a></li>
			<li class="ousko"><a href="krok4">Náklady mimo mzdu</a></li>
			<li class="ousko"><a href="krok5">Počet odpracovaných hodin</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field" >
    		<?php echo $this->renderElement('../companies/work_money_new/tabs/odmeny');?>
    	</div>
        <div class="domtabs field" >
    		<?php echo $this->renderElement('../companies/work_money_new/tabs/mzdy');?>
    	</div>
        <div class="domtabs field" id="company_money_item_settings_box">
    		<?php echo $this->renderElement('../companies/work_money_new/tabs/nastaveni');?>
    	</div>
        <div class="domtabs field" >
    		<?php echo $this->renderElement('../companies/work_money_new/tabs/naklady_mimo_mzdu');?>
    	</div>
        <div class="domtabs field" >
    		<?php echo $this->renderElement('../companies/work_money_new/tabs/poc_odp_hodin');?>
    	</div>
	</div>
	<div class='formular_action'>
		<input type='submit' value='Uložit' id='AddEditCompanyWorkMoneySaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyWorkMoneyClose'/>
	</div>
</form>
<?php echo $this->renderElement('../companies/work_money_new/js');?>