<form action='/companies/coordinators_edit/' method='post' id='company_contact_edit_formular'>
	<?php echo $htmlExt->hidden('CompanyCoordinator/id');?>
	<?php echo $htmlExt->hidden('CompanyCoordinator/company_id');?>
	<fieldset>
		<legend>Vyplňte následující údaje</legend>
		<?php echo $htmlExt->selectTag('CompanyCoordinator/cms_user_id',$coo_list,null,array('tabindex'=>1,'label'=>'Uživatel'));?> <br class="clear">	
	</fieldset>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditCompanyCoordinatorSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyCoordinatorClose'/>
	</div>
</form>
<script>
	$('AddEditCompanyCoordinatorClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_kontakty_add');});
	
	$('AddEditCompanyCoordinatorSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('company_contact_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('company_contact_edit_formular').action,		
				onComplete:function(){
				    domwin.loadContent('domwin',{goto:2});    
					domwin.closeWindow('domwin_kontakty_add');
				}
			}).post($('company_contact_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('company_contact_edit_formular',{
		'CompanyCoordinatorCmsUserId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit uživatele'}
		}
	});
	validation.generate('company_contact_edit_formular',<?php echo (isset($this->data['CompanyCoordinator']['id']))?'true':'false';?>);
	
</script>