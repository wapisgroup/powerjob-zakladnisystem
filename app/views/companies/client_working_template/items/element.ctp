<?php 
	$pocet_dnu_v_mesici = cal_days_in_month(CAL_GREGORIAN, $month, $year);
	$last_day = date('N', mktime(0, 0, 0, $month, 1, $year)); 
	$current_day = $last_day;
	$dny = array( 1=>'Po', 2=>'Út', 3=>'St', 4=>'Čt', 5=>'Pá', 6=>'So', 7=>'Ne' );
	// render list of disabled fields
	$not_to_disabled = array();
	for ($i=1; $i <= $pocet_dnu_v_mesici; $i++){
	   $not_to_disabled[] = $i;
	}

?>
<br />
<table class='table odpracovane_hodiny'>	
				<tr>
					<th>Den</th>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<th <?php 
							if (in_array(($i+1),$svatky)) 
								echo "class='svatky'";
							else if (in_array($current_day,array(6,7))) 
								echo "class='vikend'";
						?>>
							<?php echo $dny[$current_day];?>
							<br />
							<?php echo ($i+1);?>
							<?php $current_day = ($current_day == 7)?$current_day=1:$current_day+1;?>
							
						</th>
					<?php endfor;?>
				</tr>
				<?php 
					$smena_captions = array(
						1	=> 'Ranní',
						2	=> 'Odpolední',
						3	=> 'Noční'
					);
				?>
				<?php for($k = 1; $k <= 3; $k++):?>
				<tr class='tr_smennost_<?php echo $k;?> <?php 
						if (($k == 2 && $setting_shift_working_id == 1) || 
							($k == 3 && in_array($setting_shift_working_id,array(1,2)))
							|| (empty($setting_shift_working_id))
							) { echo 'none'; }?>'>	
					<td><?php echo $smena_captions[$k];?></td>
					<?php $current_day = $last_day;?>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<td <?php 
							if (in_array(($i+1),$svatky)) 
								echo "class='svatky'";
							else if (in_array($current_day,array(6,7))) 
								echo "class='vikend'";
						?>>
						<?php 
							if (in_array(($i+1),$svatky)){ 
								$class =  " svatky";
							} else if (in_array($current_day,array(6,7))){
								$class = " vikend";
							} else {
								$class = "";
							}
						?>
						<?php echo $htmlExt->input('CompanyClientWorkingTemplateItem/days/'. '_' .$k. '_' .($i+1),array('class'=>'hodiny integer s'.$k.' '.$class,'disabled'=>(!in_array(($i+1),$not_to_disabled) || (isset($this->data['CompanyClientWorkingTemplateItem']['days']['_' .$k. '_' .($i+1)]) && $this->data['CompanyClientWorkingTemplateItem']['days']['_' .$k. '_' .($i+1)] == 'A') )?'disabled':null));?></td>
						<?php $current_day = ($current_day == 7)?$current_day=1:$current_day+1;?>
					<?php endfor;?>
				</tr>
				<?php endfor;?>
		
</table>
<script>
$('domwin_cw_item_edit').getElements('.integer').inputLimit();

</script>