<div class="win_fce top">
<a href='/companies/cw_template_edit/<?php echo $company_id;?>' class='button edit' id='company_contact_add_new' title='Přidání nové šablony'>Přidat šablony</a>
</div>
<table class='table' id='table_kontakty'>
	<tr>
		<th>Název</th>
		<th>Vytvořeno</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($template_list) && count($template_list) >0):?>
	<?php foreach($template_list as $item):?>
	<tr>
		<td><?php echo $item['CompanyClientWorkingTemplate']['name'];?></td>
		<td><?php echo $fastest->czechDate($item['CompanyClientWorkingTemplate']['created']);?></td>
		<td>
			<a title='Editace šablony "<?php echo $item['CompanyClientWorkingTemplate']['name'];?>"' 	class='ta edit' href='/companies/cw_template_edit/<?php echo $company_id;?>/<?php echo $item['CompanyClientWorkingTemplate']['id'];?>'/>edit</a>
			<a title='Položky šablony "<?php echo $item['CompanyClientWorkingTemplate']['name'];?>"' 	class='ta client_working_template' href='/companies/cw_template_items/<?php echo $company_id;?>/<?php echo $item['CompanyClientWorkingTemplate']['id'];?>'/>items</a>
			          
            <?php if(in_array(6,array(1,5))){ //$logged_user['CmsGroup']['id'] ?>
            <a title='Odstranit položku'class='ta trash' href='/companies/form_template_trash/<?php echo $company_id;?>/<?php echo $item['CompanyClientWorkingTemplate']['id'];?>'/>trash</a>
		    <?php } ?>
        </td>
	</tr>
	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='7'>K této společnosti nebyly nadefinovány žádne šablony docházek</td>
	</tr>
	<?php endif;?>
</table>
<?php // pr($contact_list);?>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='CWTemplateClose' class='button'/>
	</div>
<script>
	$('CWTemplateClose').addEvent('click', function(e){new Event(e).stop(); domwin.closeWindow('domwin');});

	$('table_kontakty').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tuto aktivitu?')){
			new Request.HTML({
				url:this.href,
				update: $('domwin').getElement('.CB_ImgContainer'),
				onComplete: function(){}
			}).send();
		}
	});

	
	$('domwin').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_cw_edit',
			sizes		: [500,250],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
    
    $('table_kontakty').getElements('.client_working_template').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_cw_templates',
			sizes		: [600,500],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
    
</script>