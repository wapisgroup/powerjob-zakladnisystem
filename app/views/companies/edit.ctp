<?php
//echo count("123");
?>
<form id='add_edit_company_formular' action='/companies/edit/' method='post'>
	<?php echo $htmlExt->hidden('Company/id');?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
            <?php if(isset($permission['setting_payment']) && $permission['setting_payment'] == 1){ ?>
			<li class="ousko"><a href="#krok2">Zálohy</a></li>
            <?php }?>
            
			<li class="ousko"><a href="#krok3">Koordinátoři</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<?php echo $htmlExt->input('Company/name',array('label'=>'Název firmy', 'class'=>'long','label_class'=>'long'));?><br />
			<div class='sll'>	
				<?php echo $htmlExt->selectTag('Company/stat_id',$company_stat_list,null,array('label'=>'Stát'),null,false);?><br />
				<?php echo $htmlExt->selectTag('Company/province_id',$province_list,null,array('label'=>'Kraj'));?><br />
				<?php echo $htmlExt->selectTag('Company/country_id',(isset($country_list)?$country_list:array()),null,array('label'=>'Okres'));?><br />
				
				<?php echo $htmlExt->input('Company/ulice',	array('label'=>'Ulice'));?><br />
				<?php echo $htmlExt->input('Company/mesto',	array('label'=>'Město'));?><br />	
				<?php echo $htmlExt->input('Company/psc',	array('label'=>'PSČ'));?><br />
				<?php echo $htmlExt->input('Company/www',	array('label'=>'WWW'));?><br />	
				<?php echo $htmlExt->input('Company/ico',	array('label'=>'IČO'));?><br />	
				<?php echo $htmlExt->input('Company/dic',	array('label'=>'DIČ'));?><br />
                <?php echo $htmlExt->inputDate('Company/datum_podpisu_smlouvy',array('tabindex'=>1,'label'=>'Datum podpisu smlouvy s firmou'));?> <br class="clear"/>
                <?php echo $htmlExt->inputDate('Company/expirace_smlouvy',array('tabindex'=>1,'label'=>'Expirace smlouvy s firmou'));?> <br class="clear"/>
            	<?php echo $htmlExt->input('Company/internal_employee',	array('label'=>'Interní zaměstnanci'));?><br />
                <?php echo $htmlExt->selectTag('Company/industry_list_id',$industry_list,null,array('label'=>'Průmysl'));?><br />
              
			</div>
			<div class='slr'>
				<?php echo $htmlExt->selectTag('Company/parent_id',$setting_parent_company_list,null,array('label'=>'Nadřazená spol.'));?><br />
                <?php echo $htmlExt->selectTag('Company/at_company_id',$at_company_list,null,array('label'=>'firma'));?><br />
              
                <?php 
					if (isset($manazer_realizace_list) && (isset($permission['choose_manazer_realizace']) && $permission['choose_manazer_realizace'] == 1)){
						  echo $htmlExt->selectTag('Company/manazer_realizace_id',$manazer_realizace_list,null,array('label'=>'Manažer Realizace'));
					}
                    else 
						echo $htmlExt->var_text('ManzarRealizace/name',array('label'=>'Manažer Realizace'));
				?><br />
                 <?php 
					if (isset($asistent_obchodu_list) && (isset($permission['choose_asistent_obchodu']) && $permission['choose_asistent_obchodu'] == 1))
						echo $htmlExt->selectTag('Company/asistent_obchodu_id',$asistent_obchodu_list,null,array('label'=>'Asistent obchodu'));
					else 
						echo $htmlExt->var_text('AsistentObchodu/name',array('label'=>'Asistent obchodu'));
				?><br />
				<?php 
					if (isset($self_manager_list) && (isset($permission['choose_self_manager']) && $permission['choose_self_manager'] == 1))
						echo $htmlExt->selectTag('Company/self_manager_id',$self_manager_list,null,array('label'=>'Sales Manager'));
					else 
						echo $htmlExt->var_text('SalesManager/name',array('label'=>'Sales Manager'));
				?><br />
                	<?php 
					if (isset($client_manager_list)){
						echo $htmlExt->selectTag('Company/client_manager_id', $client_manager_list,null,array('label'=>'Koordinátor'));
					} else {
						echo $htmlExt->var_text('ClientManager/name',array('label'=>'Koordinátor'));
					}
				?><br />	
				<?php 
					if(isset($coordinator_list)){
						echo $htmlExt->selectTag('Company/coordinator_id',$coordinator_list,null,array('label'=>'Koordinátor 2'));
						echo $htmlExt->selectTag('Company/coordinator_id2',$coordinator_list,null,array('label'=>'Koordinátor 3'));
					} else{
						echo $htmlExt->var_text('Coordinator/name',array('label'=>'Koordinátor 2'));
						echo $htmlExt->var_text('Coordinator2/name',array('label'=>'Koordinátor 3'));
					}
				?><br />
                <?php echo $htmlExt->checkbox('Company/dy_prilezitost',null,	array('label'=>'Příležitost DY'));?><br />
		  	    <div id="kdy_box" class="<? echo (isset($this->data['Company']['dy_prilezitost']) && $this->data['Company']['dy_prilezitost'] == 1 ?'':'none');?>">
                   <?php echo $htmlExt->selectTag('Company/konzultant_dy_id',$konzultant_dy_list,null,array('label'=>'Konzultant DY')); ?>
                </div>
                <?php 
					if (isset($konzultant_list)){
					    if(isset($permission['choose_supervisor']) && $permission['choose_supervisor'] == 1)
						  echo $htmlExt->selectTag('Company/supervisor_id', $konzultant_list,null,array('label'=>'Konzultant'));
				        else {
                          echo $htmlExt->var_text('Company/supervisor_id',array('label'=>'Konzultant','value'=>isset($konzultant_list[$this->data['Company']['supervisor_id']])?$konzultant_list[$this->data['Company']['supervisor_id']]:''));
                        }
                    } 
				?><br />		
			
				<?php echo $htmlExt->selectTag('Company/setting_stav_id',$setting_stav_list,null,array('label'=>'Stav'));?><br />
			     <?php echo $htmlExt->checkbox('Company/vip',null,	array('label'=>'VIP'));?><br />
		  	    
            </div>
			<br />
			<?php echo $htmlExt->textarea('Company/address2',	array('class'=>'long','label'=>'Korespondeční adresa:','label_class'=>'long'));?><br />
			<?php echo $htmlExt->textarea('Company/note',	array('class'=>'long','label'=>'Popis','label_class'=>'long'));?><br />
		</div>
        
        <?php if(isset($permission['setting_payment']) && $permission['setting_payment'] == 1){ ?>	
		<div class="domtabs field">
            <div class="sll">
                <?php echo $htmlExt->checkbox('Company/payment_cash',null,	array('label'=>'výplata v hotovosti'));?><br />
		  	    <?php echo $htmlExt->checkbox('Company/payment_amount',null,	array('label'=>'zálohy'));?><br />
                <?php echo $htmlExt->checkbox('Company/payment_once_in_month',null,	array('label'=>'Záloha jednou měsíčně'));?><br />
            </div>
            <div class="slr">
                 <?php echo $htmlExt->input('Company/payment_cash_limit',	array('label'=>'Max(hotovost)'));?><br />
                 <?php echo $htmlExt->input('Company/payment_amount_limit',	array('label'=>'Max(záloha)'));?><br />
            </div>
            <br />
        </div>
        <?php }?>
        
		<div class="domtabs field">
            <?php echo $this->renderElement('../companies/coordinators/index');?>
        </div>

	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditCompanySaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyClose'/>
	</div>
</form>
<script>
	
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('AddEditCompanyClose').addEvent('click',function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin');
	});
    
    $('CompanyDyPrilezitost').addEvent('change',function(e){
		if(this.checked){$('kdy_box').removeClass('none');}
        else{$('kdy_box').addClass('none'); $('CompanyKonzultantDyId').value = '';}
	});
	
	$('CompanyStatId').ajaxLoad('/companies/load_province/', ['CompanyProvinceId']);
	$('CompanyProvinceId').ajaxLoad('/companies/load_country/', ['CompanyCountryId']);
	
	$('AddEditCompanySaveAndClose').addEvent('click', function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('add_edit_company_formular');
		if (valid_result == true){
			button_preloader($('AddEditCompanySaveAndClose'));
			new Request.JSON({
				url:$('add_edit_company_formular').action,		
				onComplete:function(){
					button_preloader_disable($('AddEditCompanySaveAndClose'));
					click_refresh($('CompanyId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('add_edit_company_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('add_edit_company_formular',{
		'CompanyName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit nazev firmy'}
		},
        'CompanyStatId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit stát firmy'}
		},
        'CompanyProvinceId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit kraj firmy'}
		},
        'CompanyCountryId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit okres firmy'}
		},
        'CompanyUlice': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit ulici firmy'}
		},
        'CompanyMesto': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit město firmy'}
		},
        'CompanyPsc': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit psč firmy'}
		},
        /*
        'calbut_CompanyDatumPodpisuSmlouvy': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit datum podpisu smlouvy s firmou'}
		},*/
		'CompanyIco': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit ičo'},
			'isUnique':{'condition':{'model':'Company','field':'ico'},'err_message':'Toto ičo je již použito'}
		}
	});
	validation.generate('add_edit_company_formular',<?php echo (isset($this->data['Company']['id']))?'true':'false';?>);
	//validation.check_form('add_edit_company_formular');
</script>
