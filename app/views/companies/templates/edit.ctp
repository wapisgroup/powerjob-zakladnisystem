<form action='/companies/template_edit/' method='post' id='company_template_edit_formular'>
	<?php echo $htmlExt->hidden('CompanyOrderTemplate/company_id', array('value'=>$company_id));?>
	<?php echo $htmlExt->hidden('CompanyOrderTemplate/id');?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Pracoviště</a></li>
			<li class="ousko"><a href="#krok2">Parametry pozice</a></li>
			<li class="ousko"><a href="#krok4">Mzdové podmínky</a></li>
			<li class="ousko"><a href="#krok5">Nástup</a></li>
			<li class="ousko"><a href="#krok6">Fotografie</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Kontakty a pracoviště</legend>
				<div class='sll'>
					<?php echo $htmlExt->var_text('CompanyOrderTemplate/company_id',  array('label'=>'Firma','value'=>$company_info['Company']['name']));?>  <br class='clear' />
				</div>
				<div class='slr'>
					<?php echo $htmlExt->input('CompanyOrderTemplate/job_city', array('label'=>'Pracoviště město'));?> <br class='clear' />
				</div>
				<br class='clear' />
				<label>Pracoviště popis:</label><br/>
				<?php echo $htmlExt->textarea('CompanyOrderTemplate/job_description', array('style'=>'width:97%'));?> <br class='clear' />
				<br />
				<div class='sll3'>
					<?php echo $htmlExt->input('CompanyOrderTemplate/cont1_name', array('label'=>'KOO 1'		,'readonly'=>'readonly','value'=>$company_info['ClientManager']['name']));?>
					<?php echo $htmlExt->input('CompanyOrderTemplate/cont2_name', array('label'=>'KOO 2'	,'readonly'=>'readonly','value'=>$company_info['Koordinator1']['name']));?>
					<?php echo $htmlExt->input('CompanyOrderTemplate/cont3_name', array('label'=>'KOO 3'	,'readonly'=>'readonly','value'=>$company_info['Koordinator2']['name']));?>
				</div>
				<div class='sll3'>
					<?php echo $htmlExt->input('CompanyOrderTemplate/cont1_email', array('label'=>'KOO1 Email'	,'readonly'=>'readonly','value'=>$company_info['ClientManager']['name']));?>
					<?php echo $htmlExt->input('CompanyOrderTemplate/cont2_email', array('label'=>'KOO2 Email'	,'readonly'=>'readonly','value'=>$company_info['Koordinator1']['name']));?>
					<?php echo $htmlExt->input('CompanyOrderTemplate/cont3_email', array('label'=>'KOO3 Email'	,'readonly'=>'readonly','value'=>$company_info['Koordinator2']['name']));?>
				</div>
				<div class='sll3'>
					<?php echo $htmlExt->input('CompanyOrderTemplate/cont1_tel', array('label'=>'KOO1 Tel'	,'readonly'=>'readonly','value'=>@$phone_list[$company_info['ClientManager']['at_employee_id']]));?>
					<?php echo $htmlExt->input('CompanyOrderTemplate/cont2_tel', array('label'=>'KOO2 Tel'	,'readonly'=>'readonly','value'=>@$phone_list[$company_info['Koordinator1']['at_employee_id']]));?>
					<?php echo $htmlExt->input('CompanyOrderTemplate/cont3_tel', array('label'=>'KOO3 Tel'	,'readonly'=>'readonly','value'=>@$phone_list[$company_info['Koordinator2']['at_employee_id']]));?>
				</div>
			</fieldset>
		</div>
		<div class="domtabs field">
			<fieldset>
				<legend>Parametry pozice</legend>
				<div class='sll'>
					<?php echo $htmlExt->input('CompanyOrderTemplate/name', array('label'=>'Požadovaná profese'));?> <br class='clear' />
				</div>
				<div class='slr'>
					<?php echo $htmlExt->selectTag('CompanyOrderTemplate/setting_career_item_id', $setting_career_list,null, array('label'=>'Profese pro nábor'));?> <br class='clear' />
				</div>
				<?php echo $htmlExt->textarea('CompanyOrderTemplate/comment', array('label'=>'Poznámka k volnému místu', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />				
				<?php echo $htmlExt->textarea('CompanyOrderTemplate/napln_prace', array('label'=>'Náplň práce', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />
				<div class='sll'>
					<?php echo $htmlExt->input('CompanyOrderTemplate/education', array('label'=>'Požadované vzdělání'));?> <br class='clear' />
					<?php echo $htmlExt->input('CompanyOrderTemplate/profession_duration', array('label'=>'Délka praxe'));?> <br class='clear' />
					<?php echo $htmlExt->input('CompanyOrderTemplate/age_restriction', array('label'=>'Věkové omezení'));?> <br class='clear' />
				</div>
				<div class='slr'>
					<?php echo $htmlExt->input('CompanyOrderTemplate/certification', array('label'=>'Certifikáty a zkoušky'));?> <br class='clear' />
					<?php echo $htmlExt->selectTag('CompanyOrderTemplate/sex_list', $vhodne_pro_list,null, array('label'=>'Je vhodné pro'));?> <br class='clear' />
				</div>
				<div class='sll'>
					<?php echo $htmlExt->selectTag('CompanyOrderTemplate/setting_shift_working_id', $smennost_list,null, array('label'=>'Směnnost'));?> <br class='clear' />
				</div><br />
					<?php echo $htmlExt->textarea('CompanyOrderTemplate/komentar_smennosti', array('label'=>'Komentář k směnnosti', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />
				<div class='sll'>
					<?php echo $htmlExt->input('CompanyOrderTemplate/working_time', array('label'=>'Pracovní doba'));?> <br class='clear' />
				</div>
				<div class='sll'>
					<?php echo $htmlExt->input('CompanyOrderTemplate/standard_hours', array('label'=>'Normo hodiny'));?> <br class='clear' />	
				</div>
			</fieldset>
		</div>
		<div class="domtabs field">
			<fieldset>
				<legend>Mzdové podmínky</legend>
				<div class='sll'>
					<?php echo $htmlExt->selectTag('CompanyOrderTemplate/company_work_position_id', $company_position_list ,null, array('label'=>'Profese dle kalkulace'));?> <br class='clear' />
				</div>
				<br/>
				<label style='clear:both'>Komentář ke mzdě:</label>
				<?php echo $htmlExt->textarea('CompanyOrderTemplate/mzdove_podminky_poznamka', array('style'=>'width:98%'));?> <br />
			</fieldset>
			<fieldset>
				<legend>Kalkulace</legend>
				<div id='kalkulace_list'>
					<?php echo $this->renderElement('../companies/templates/kalkulace');?>
				</div>
			</fieldset>
		</div>
		<div class="domtabs field">
			<fieldset>
				<legend>Vstupní zkouška</legend>
				<div class='sll'>
					<?php echo $htmlExt->selectTag('CompanyOrderTemplate/entrance_examination', $ano_ne_list,null, array('label'=>'Vstupní zkouška'));?> <br class='clear' />
				</div>
				<br class='clear'/>
				<?php echo $htmlExt->textarea('CompanyOrderTemplate/examination_description', array('label'=>'Popis vstupní zkoušky', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />
			</fieldset>
			<fieldset>
				<legend>Doklady potřebné při nástupu</legend>
				<div class='sll'>
					<label>OP:</label>				<?php echo $htmlExt->checkbox('CompanyOrderTemplate/checkbox_op', array('label'=>'OP'));?> <br class='clear' />
					<label>Zápočtový list:</label>		<?php echo $htmlExt->checkbox('CompanyOrderTemplate/checkbox_zl', array('label'=>'Zápočtový list'));?> <br class='clear' />
					<label>Kvalifikační doklady:</label>	<?php echo $htmlExt->checkbox('CompanyOrderTemplate/checkbox_kd', array('label'=>'Kvalifikační doklady'));?> <br class='clear' />
				</div>
				<div class='slr'>
					<label>Výuční list:</label>			<?php echo $htmlExt->checkbox('CompanyOrderTemplate/checkbox_vl', array('label'=>'Výuční list'));?> <br class='clear' />
					<label>Zdravotní prohlídka:</label>	<?php echo $htmlExt->checkbox('CompanyOrderTemplate/checkbox_zp', array('label'=>'Zdravotní prohlídka'));?> <br class='clear' />
				</div>
				<br class='clear'/>
				<?php echo $htmlExt->textarea('CompanyOrderTemplate/jine_doklady', array('label'=>'Co ještě mít sebou k nástupu', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />
			</fieldset>
		</div>
		<div class="domtabs field">
			<br />
			<script>
				function upload_file_complete(url){
					var ul = $('foto_list');
					var li = new Element('li',{'class':'obal_span'}).inject(ul);
					new Element('img', {src:'/uploaded/requirement/foto/small/' + url,'class':'nahrada_za_foto'}).inject(li);
					
					new Element('input', {type:'hidden', name:'data[CompanyOrderTemplate][imgs][]', value:url + '|', 'class':'file_caption_value'}).inject(li);
					var submenu = new Element('div', {'class':'submenu'}).inject(new Element('span').inject(li));
						var zoom = new Element('a', {'href':'/uploaded/requirement/foto/large/' + url,'target':'_blank','rel':'clearbox[detail_fotogalerie]'})
							.inject(submenu)
						new Element('img', {'class':'icon',title:'Zvětšit fotografii',src:'/css/fastest/upload/zoom.png'})
							.inject(zoom)
						new Element('img', {'class':'icon',title:'Smazat fotografii',src:'/css/fastest/upload/delete.png'})
							.inject(submenu)
							.addEvent('click', img_delete_button.bind());
						new Element('img', {'class':'icon',title:'Editovat popis fotografie',src:'/css/fastest/upload/edit.png'})
							.inject(submenu)
							.addEvent('click', title_edit_button.bind());
					
					
					document.slideshow.ajax_init($('foto_list'));	
					mysort.addItems(li);
					
					$('CompanyOrderTemplateFotoUploader').getElement('.input_file_over').value = '';
					$('CompanyOrderTemplateFotoUploader').getElements('.browse_icon, .input_file').setStyle('display','inline');
					$('CompanyOrderTemplateFotoUploader').getElement('.smazat_icon').setStyle('display','none');
				}
			</script>
			<div>
				<?php $fileinput_setting = array(
					'label'			=> 'Fotografie',
					'upload_type' 	=> 'php',
					'filename_type'	=> 'unique',
					'file_ext'		=> array('jpg'),
					'paths' 		=> array(
						'path_to_file'	=> 'uploaded/requirement/foto/',
						//'path_to_ftp'	=> 'uploaded/reality/',
						'upload_script'	=> '/companies/requirement_upload_foto/',
						'status_path' 	=> '/get_status.php',
					),
					'methods'=>array(
						array('methods'	=>	array(array(
							'method'		=>	'resize',
							'new_width'		=>	800,
							'new_height'	=>	600
						)),
						'addon_path'=> 'large/'
						),
						array('methods'	=>	array(array(
							'method'		=>	'resize',
							'new_width'		=>	80,
							'new_height'	=>	80
						)),
						'addon_path'=> 'small/'
						)
					),
					'onComplete'	=> 'upload_file_complete'
				);
				
			
				
				?>
				<?php echo $fileInput->render('CompanyOrderTemplate/foto',$fileinput_setting);?>
			</div>
			<br /> 
			<ul class='foto_list' id='foto_list' style='position:relative;'>
				<?php
					if (isset($this->data['CompanyOrderTemplate']['imgs']) && !empty($this->data['CompanyOrderTemplate']['imgs']) && count($this->data['CompanyOrderTemplate']['imgs']) > 0){
						foreach($this->data['CompanyOrderTemplate']['imgs'] as $img){
							list($file, $caption) = explode('|',$img);
							echo "<li class='obal_span'><img src='/uploaded/requirement/foto/small/$file' title='$caption' alt='$caption' class='nahrada_za_foto tip_win'/><input class='file_caption_value' type='hidden' name='data[CompanyOrderTemplate][imgs][]' value='$img'/><span><div class='submenu'><a href='/uploaded/requirement/foto/large/$file' title='$caption' target='_blank' rel='clearbox[detail_fotogalerie]'><img src='/css/fastest/upload/zoom.png' title='Zvětšit fotografii' class='img_zoom_button icon' /></a><img src='/css/fastest/upload/delete.png' title='Smazat fotografii' class='img_delete_button icon' /><img src='/css/fastest/upload/edit.png' title='Editovat popis fotografie' class='title_edit_button icon' /></div></span></li>";
						}
					
					}
				?>
			
			</ul>
			<div id='editable_title'>
				<textarea style='width:95%'></textarea><br/>
				<p style='text-align:center'>
					<input type='button' class='button' value='Změnit' id='apply_editable_title'/>
					<input type='button' class='button' value='Zrušit' id='close_editable_title'/>
				</p>
			</div>
			<br />
			<br />
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditCompanyOrderTemplateSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyOrderTemplateClose'/>
	</div>
</form>
<script>

	var domtab = new DomTabs({'className':'admin_dom'}); 
	
		
	if ($('CompanyOrderTemplateCompanyWorkPositionId'))
		$('CompanyOrderTemplateCompanyWorkPositionId').addEvent('change', function(e){
			e.stop();
			if (this.value == '') {
				$('kalkulace_list').empty();	 	 
			 } else {
			 	new Request.HTML({
			 		url: '/companies/load_kalkulace/' + this.value,
					update: $('kalkulace_list')
			 	}).send();
			 }
		});	
	
	$('AddEditCompanyOrderTemplateClose').addEvent('click',function(e){new Event(e).stop(); 
	   <?php if(isset($show)){ ?>
		domwin.closeWindow('domwin_show');
        <?php }else{ ?>
		domwin.closeWindow('domwin_pozice_add');
        <?php } ?>
	});
	
	$('AddEditCompanyOrderTemplateSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('company_template_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('company_template_edit_formular').action, 
				onComplete:function(json){
					if (json){
						if (json.result === true){
							domwin.loadContent('domwin_pozice');
							domwin.closeWindow('domwin_pozice_add');
						} else {
							alert(json.message);
						}
					} else {
						alert('Chyba apliakce');	
					}
				}
			}).post($('company_template_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});

	validation.define('company_template_edit_formular',{
		'CompanyOrderTemplateName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit požadovanou profesi'}
		},
		'CompanyOrderTemplateNaplnPrace': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit náplň práce'}
		},
		'CompanyOrderTemplateSalary': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit mzda od-do'}
		},
		'CompanyOrderTemplateSettingShiftWorkingId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit směnnost'}
		},
		'CompanyOrderTemplateJobCity': {
			'testReq': {'condition':'not_empty','err_message':'Není zvoleno město'}
		},
		'CompanyOrderTemplateEducation': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit Požadované vzdělání'}
		},
		'CompanyOrderTemplateProfessionDuration': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit Délku praxe'}
		},
		'CompanyOrderTemplateWorkingTime': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit Pracovní dobu'}
		},
		'CompanyOrderTemplateStandardHours': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit Normo hodiny'}
		},
		'CompanyOrderTemplateJobDescription': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit Popis pracoviště'}
		}
	});
	validation.generate('company_template_edit_formular',<?php echo (isset($this->data['CompanyOrderTemplate']['id']))?'true':'false';?>);
	
	/**
	 * Pokud se jedna o pridani, ne editaci a neni prazdne mesto, tak to zvaliduj,
	 * tzn. prohod class
	 */
	if ($('CompanyOrderTemplateId').value == '' && $('CompanyOrderTemplateJobCity').value != ''){
		$('CompanyOrderTemplateJobCity').addClass('valid').removeClass('require');
	}
	function title_edit_button(e){
		var event = new Event(e);
		event.stop();
		$('editable_title').setStyle('display','block');
		textarea = $('editable_title').getElement('textarea');
		curr_img = event.target.getParent('li').getElement('img');
		curr_img.addClass('edit_now');
		textarea.value = curr_img.getProperty('title');
	}
	
	function img_delete_button(e){
		var event = new Event(e);
		event.stop();
		if (confirm('Opravdu si přejete odstranit tuto fotografii?'))
			event.target.getParent('li').dispose();
	}
	
	$$('.title_edit_button').addEvent('click', title_edit_button.bind());
	$$('.img_delete_button').addEvent('click', img_delete_button.bind());
	
	$('close_editable_title').addEvent('click', function(e){
		new Event(e).stop();
		$('editable_title').setStyle('display','none');
	});
	
	$('apply_editable_title').addEvent('click', function(e){
		new Event(e).stop();
		textarea = $('editable_title').getElement('textarea');
		element = $('foto_list').getElement('.edit_now');
		element
			.setProperties({
				title: textarea.value,
				alt: textarea.value
			})
			.removeClass('edit_now');
		
		hidden = element.getNext('.file_caption_value');
		hidden_text = hidden.value.split('|');
		hidden_text[1] = textarea.value;
		hidden.value = hidden_text[0] + '|' + hidden_text[1];
		textarea.value = '';
		$('editable_title').setStyle('display','none');
	});
	
	var mysort = new Sortables($('foto_list'), {
	  constrain: false,
	  clone: true
	});
	
	document.slideshow.ajax_init($('foto_list'));	
	
	
	var Tips_help = new Tips($$('.tip_win'),{
			showDelay: 100,
			hideDelay: 400,
			className: 'tip_win',
			offsets: {'x': -3, 'y': 70},
			fixed: true
	});
    
    /**
     * pokud je nastaven show dej vse disabled
     */
    <?php if(isset($show)): ?>
    	$('company_template_edit_formular').getElements('input, select, textarea')
    		.addClass('read_info')
    		.setProperty('disabled');
       if($('AddEditCompanyOrderTemplateSaveAndClose'))    
          $('AddEditCompanyOrderTemplateSaveAndClose').addClass('hidden');   
       $('AddEditCompanyOrderTemplateClose').removeAttribute('disabled');
	<?php endif; ?>

</script>