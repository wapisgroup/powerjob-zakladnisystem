<?php if (isset($kalkulace_list) && count($kalkulace_list)>0):?>
	<table class='table'>
		<tr>
			<th>Typ</th>
			<th>Maximalka</th>
			<th>Ubytovani</th>
			<th>Doprava</th>
			<th>Stravovani</th>
		</tr>
	<?php foreach($kalkulace_list as $kalkulace):?>
		<tr>
			<td><?php echo $kalkulace['CompanyMoneyItem']['name'];?></td>
			<td><?php echo $kalkulace['CompanyMoneyItem']['vypocitana_celkova_cista_maximalni_mzda_na_hodinu'];?></td>
			<td><?php echo $fastest->value_to_yes_no($kalkulace['CompanyMoneyItem']['cena_ubytovani_na_mesic']);?></td>
			<td><?php echo $fastest->value_to_yes_no($kalkulace['CompanyMoneyItem']['doprava']);?></td>
			<td><?php echo $fastest->value_to_yes_no($kalkulace['CompanyMoneyItem']['stravenka']);?></td>
		</tr>
	<?php endforeach;?>
	</table>
<?php endif;?>