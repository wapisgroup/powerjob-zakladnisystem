<div class="win_fce top">
<a href='/companies/template_edit/<?php echo $company_id;?>' class='button edit' id='company_template_add_new' title='Přidání nové šablony'>Přidat šablonu</a>
</div>
<table class='table' id='table_templates'>
	<tr>
		<th>#ID</th>
		<th>Název</th>
		<th>Vytvořeno</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($template_list) && count($template_list) >0):?>
	<?php foreach($template_list as $template_item):?>
	<tr>
		<td><?php echo $template_item['CompanyOrderTemplate']['id'];?></td>
		<td><?php echo $template_item['CompanyOrderTemplate']['name'];?></td>
		<td><?php echo $fastest->czechDateTime($template_item['CompanyOrderTemplate']['created']);?></td>
		<td>
			<a title='Editace "<?php echo $template_item['CompanyOrderTemplate']['name'];?>"' 	class='ta edit' href='/companies/template_edit/<?php echo $company_id;?>/<?php echo $template_item['CompanyOrderTemplate']['id'];?>'/>edit</a>
			<a title='Odstranit 'class='ta trash' href='/companies/template_trash/<?php echo $company_id;?>/<?php echo $template_item['CompanyOrderTemplate']['id'];?>'/>trash</a>
		</td>
	</tr>
	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='5'>K této společnosti nebyla nadefinována šablona</td>
	</tr>
	<?php endif;?>
</table>
<?php // pr($attachment_list);?>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='ListCompanyTemplateClose' class='button'/>
	</div>
<script>
	$('ListCompanyTemplateClose').addEvent('click', function(e){new Event(e).stop(); domwin.closeWindow('domwin_pozice');});

	$('table_templates').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tuto přílohu?')){
			new Request.HTML({
				url:this.href,
				onComplete: (function(){
				    this.getParent('tr').dispose();
				}).bind(this)
			}).send();
		}
	});
	
	$('domwin_pozice').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_pozice_add',
			sizes		: [880,590],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>