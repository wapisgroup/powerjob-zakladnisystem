<form id='add_edit_company_formular' action='/companies/convert_users/' method='post'>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">	
				<?php echo $htmlExt->selectTag('field',$fields,null,array('label'=>'Typ'));?><br />
				<?php echo $htmlExt->selectTag('user_from',array('Zvolte typ'),null,array('label'=>'Odebrat od uživatele'),null,false);?><br />
				<?php echo $htmlExt->selectTag('user_to',array('Zvolte typ'),null,array('label'=>'Přiřadit uživateli'),null,false);?><br />
    	</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditCompanySaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyClose'/>
	</div>
</form>
<script>
	
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('AddEditCompanyClose').addEvent('click',function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin');
	});
    
	
	$('Field').ajaxLoad('/companies/ajax_load_users/', ['UserFrom']);
    $('Field').ajaxLoad('/companies/ajax_load_users/', ['UserTo']);
	
	$('AddEditCompanySaveAndClose').addEvent('click', function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('add_edit_company_formular');
		if (valid_result == true){
			button_preloader($('AddEditCompanySaveAndClose'));
			new Request.JSON({
				url:$('add_edit_company_formular').action,		
				onComplete:function(){
					button_preloader_disable($('AddEditCompanySaveAndClose'));
					click_refresh();
					domwin.closeWindow('domwin');

				}
			}).post($('add_edit_company_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('add_edit_company_formular',{
		'Field': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit typ'}
		},
        'UserFrom': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit uživatele od kterého chcete firmy odebrat'}
		},
        'UserTo': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit uživatele kterému chcete přiřadit'}
		}
	});
	validation.generate('add_edit_company_formular',false);
</script>
