<form action='/companies/order_edit/' method='post' id='company_template_edit_formular'>
	<?php echo $htmlExt->hidden('CompanyOrderItem/company_id', array('value'=>$company_id));?>
	<?php echo $htmlExt->hidden('CompanyOrderItem/id');?>
	<?php echo $htmlExt->hidden('CompanyOrderItem/order_status'); ?>
	<?php 
      if($generate)  
        echo $htmlExt->hidden('CompanyOrderItem/generate',array('value'=>$generate)); 
        
      $none = false;
      if(!isset($this->data['CompanyOrderItem']) || $generate) 
        $none = 'none'; 
    ?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Objednávka</a></li>
            <?php if(!$none){?>
    			<li class="ousko"><a href="#krok1">Pracoviště</a></li>
    			<li class="ousko"><a href="#krok2">Parametry pozice</a></li>
    			<li class="ousko"><a href="#krok4">Mzdové podmínky</a></li>
    			<li class="ousko"><a href="#krok5">Nástup</a></li>
    			<li class="ousko"><a href="#krok6">Fotografie</a></li>
    		
            <?php }
            if(isset($this->data['CompanyOrderItem']) && $this->data['CompanyOrderItem']['order_status']==2){?>
        	   <li class="ousko"><a href="#krok7">Shrnutí</a></li>
            <?php } ?>
   	    </ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Základní informace</legend>
                    <?php
                    /**
                      * pokud je toto detail nebo editace, zobraz datum vytvoření
                      */ 
                    if(isset($this->data['CompanyOrderItem'])){?>
                    <div class='sll'>
    				<?php echo $htmlExt->var_text('CompanyOrderItem/vytvoreno',array('label'=>'Vytvořeno','value'=>$fastest->czechDateTime($this->data['CompanyOrderItem']['created'])));?><br />
    				</div><br />
                    <?php  }?>
                    
                <?php echo $htmlExt->input('CompanyOrderItem/name', array('label'=>'Požadovaná profese', 'class'=>'long '.($generate == true ? 'read_info':''), 'label_class'=>'long','disabled'=>($generate == true ? 'disabled' :'')));?> <br class='clear' />
				<div class='sll'>
					<?php echo $htmlExt->selectTag('CompanyOrderItem/order_type', $order_type_list,null,array('label'=>'Typ objednávky'),null,false);?><br />
					<?php echo $htmlExt->inputDate('CompanyOrderItem/order_date',array('label'=>'Datum objednávky', 'class'=>'','label_class'=>''));?><br />
				</div>
				<div class='slr'>
					<?php echo $htmlExt->var_text('CompanyOrderItem/order_status',array('label'=>'Stav objednávky','value'=>(isset($this->data['CompanyOrderItem']) ? $order_stav_list[$this->data['CompanyOrderItem']['order_status']] : '')));?><br />
					<?php echo $htmlExt->checkbox('CompanyOrderItem/nabor',null,array('label'=>'Požadavek pro NÁBOR'));?><br />
					<?php 
                        if(isset($this->data['CompanyOrderItem']['id'])){
                            echo $htmlExt->var_text('CompanyOrderItem/order_template_id',array('label'=>'Šablona','value'=>$template_list[$this->data['CompanyOrderItem']['order_template_id']])).'<br />';
                        }
                        else
                            echo $htmlExt->selectTag('CompanyOrderItem/order_template_id',$template_list,null,array('label'=>'Načtení ze šablony', 'class'=>' '.($generate == true ? 'read_info':''),'label_class'=>'' ,'disabled'=>($generate == true ? 'disabled' :false)));
                    ?><br />
				</div>
			</fieldset>
			<fieldset>
				<legend>Informace o nástupu</legend>
				<div class='sll'>
					<?php echo $htmlExt->input('CompanyOrderItem/count_of_free_position',array('label'=>'Požadované místa', 'class'=>'integer','label_class'=>''));?><br />
					<?php echo $htmlExt->input('CompanyOrderItem/count_of_substitute',array('label'=>'Počet náhradníků', 'class'=>'integer','label_class'=>''));?><br />	
				</div>
				<div class='slr'>
					<?php echo $htmlExt->inputDateTime('CompanyOrderItem/start_datetime',array('label'=>'Nástup (datum/čas)', 'class'=>'','label_class'=>''));?><br />
					<?php echo $htmlExt->input('CompanyOrderItem/start_place',array('label'=>'Místo nástupu', 'class'=>'','label_class'=>''));?><br />
					<?php echo $htmlExt->selectTag('CompanyOrderItem/okres_id',$country_list,null,array('label'=>'okres nástupu', 'class'=>'','label_class'=>''));?><br />
					<?php echo $htmlExt->selectTag('CompanyOrderItem/contact_id',$cm_koo_list,null,array('label'=>'Kontakt', 'class'=>'','label_class'=>''));?><br />
				</div>
				<br />
                <label class="long">Komentář:</label>
                <div style="color:red; width:79%; padding:8px; float:right;">
                    Pri nástupe musí mať každý uchádzač:<br />
                    - občianský preukaz a preukaz poistenca ( aj ich kopie pre mzdové),pokiaľ ich nedonesú nesmieme ich zamestnať<br />
                    - finančnú hotovosť na minimálne 14 dní<br />
                    - upozorniť na nutnosť lekárskej prehliadky (upresniť by mal coordinátor,pokiaľ sa dá nech už ho donesú na nástup) bude im preplatená po  mesiaci práce.<br />
                </div>
                <br />
                <label class="long"></label>
				<?php echo $htmlExt->textarea('CompanyOrderItem/start_comment',array('class'=>'long','label_class'=>'long'));?><br />
			    <?php echo $htmlExt->input('CompanyOrderItem/lekarska_prehlidka_na', array('label'=>'Lekárska prehlidka na', 'class'=>'long '.($generate == true ? 'read_info':''), 'label_class'=>'long','disabled'=>($generate == true ? 'disabled' :'')));?> <br class='clear' />
			 
            </fieldset>
		</div>
		<div class="domtabs field <?php echo $none;?>">
			<fieldset>
				<legend>Kontakty a pracoviště</legend>
				<div class='sll'>
					<?php echo $htmlExt->var_text('CompanyOrderItem/company_id',  array('label'=>'Firma','value'=>$company_info['Company']['name']));?>  <br class='clear' />
				</div>
				<div class='slr'>
					<?php echo $htmlExt->input('CompanyOrderItem/job_city', array('label'=>'Pracoviště město'));?> <br class='clear' />
				</div>
				<br class='clear' />
				<label>Pracoviště popis:</label><br/>
				<?php echo $htmlExt->textarea('CompanyOrderItem/job_description', array('style'=>'width:97%'));?> <br class='clear' />
				<br />
				<div class='sll3'>
					<?php echo $htmlExt->input('CompanyOrderItem/cont1_name', array('label'=>'KOO 1'		,'readonly'=>'readonly','value'=>$company_info['ClientManager']['name']));?>
					<?php echo $htmlExt->input('CompanyOrderItem/cont2_name', array('label'=>'KOO 2'	,'readonly'=>'readonly','value'=>$company_info['Koordinator1']['name']));?>
					<?php echo $htmlExt->input('CompanyOrderItem/cont3_name', array('label'=>'KOO 3'	,'readonly'=>'readonly','value'=>$company_info['Koordinator2']['name']));?>
				</div>
				<div class='sll3'>
					<?php echo $htmlExt->input('CompanyOrderItem/cont1_email', array('label'=>'KOO1 Email'	,'readonly'=>'readonly','value'=>$company_info['ClientManager']['name']));?>
					<?php echo $htmlExt->input('CompanyOrderItem/cont2_email', array('label'=>'KOO2 Email'	,'readonly'=>'readonly','value'=>$company_info['Koordinator1']['name']));?>
					<?php echo $htmlExt->input('CompanyOrderItem/cont3_email', array('label'=>'KOO3 Email'	,'readonly'=>'readonly','value'=>$company_info['Koordinator2']['name']));?>
				</div>
				<div class='sll3'>
					<?php echo $htmlExt->input('CompanyOrderTemplate/cont1_tel', array('label'=>'KOO1 Tel'	,'readonly'=>'readonly','value'=>@$phone_list[$company_info['ClientManager']['at_employee_id']]));?>
					<?php echo $htmlExt->input('CompanyOrderTemplate/cont2_tel', array('label'=>'KOO2 Tel'	,'readonly'=>'readonly','value'=>@$phone_list[$company_info['Koordinator1']['at_employee_id']]));?>
					<?php echo $htmlExt->input('CompanyOrderTemplate/cont3_tel', array('label'=>'KOO3 Tel'	,'readonly'=>'readonly','value'=>@$phone_list[$company_info['Koordinator2']['at_employee_id']]));?>
			    </div>
			</fieldset>
		</div>
		<div class="domtabs field <?php echo $none;?>">
			<fieldset>
				<legend>Parametry pozice</legend>
				<div class='sll'>
					<?php echo $htmlExt->selectTag('CompanyOrderItem/setting_career_item_id', $setting_career_list,null, array('label'=>'Profese pro nábor'));?> <br class='clear' />
				</div>
				<br />
				<?php echo $htmlExt->textarea('CompanyOrderItem/comment', array('label'=>'Poznámka k volnému místu', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />
				<?php echo $htmlExt->textarea('CompanyOrderItem/napln_prace', array('label'=>'Náplň práce', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />
				<div class='sll'>
					<?php echo $htmlExt->input('CompanyOrderItem/education', array('label'=>'Požadované vzdělání'));?> <br class='clear' />
					<?php echo $htmlExt->input('CompanyOrderItem/profession_duration', array('label'=>'Délka praxe'));?> <br class='clear' />
					<?php echo $htmlExt->input('CompanyOrderItem/age_restriction', array('label'=>'Věkové omezení'));?> <br class='clear' />
				</div>
				<div class='slr'>
					<?php echo $htmlExt->input('CompanyOrderItem/certification', array('label'=>'Certifikáty a zkoušky'));?> <br class='clear' />
					<?php echo $htmlExt->selectTag('CompanyOrderItem/sex_list', $vhodne_pro_list,null, array('label'=>'Je vhodné pro'));?> <br class='clear' />
				</div>
				<div class='sll'>
					<?php echo $htmlExt->selectTag('CompanyOrderItem/setting_shift_working_id', $smennost_list,null, array('label'=>'Směnnost'),null,false);?> <br class='clear' />
				</div><br />
					<?php echo $htmlExt->textarea('CompanyOrderItem/komentar_smennosti', array('label'=>'Komentář k směnnosti', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />
				<div class='sll'>
					<?php echo $htmlExt->input('CompanyOrderItem/working_time', array('label'=>'Pracovní doba'));?> <br class='clear' />
				</div>
				<div class='sll'>
					<?php echo $htmlExt->input('CompanyOrderItem/standard_hours', array('label'=>'Normo hodiny'));?> <br class='clear' />	
				</div>
			</fieldset>
		</div>
		<div class="domtabs field <?php echo $none;?>">
			<fieldset>
				<legend>Mzdové podmínky</legend>
				<div class='sll'>
					<?php echo $htmlExt->selectTag('CompanyOrderItem/company_work_position_id', $company_position_list ,null, array('label'=>'Profese dle kalkulace'));?> <br class='clear' />
				</div><br/>
				<label>Komentář ke mzdě:</label>
				<?php echo $htmlExt->textarea('CompanyOrderItem/mzdove_podminky_poznamka', array('style'=>'width:98%'));?> <br />
			</fieldset>
			<fieldset>
				<legend>Kalkulace</legend>
				<div id='kalkulace_list'>
					<?php echo $this->renderElement('../companies/templates/kalkulace');?>
				</div>
			</fieldset>
		</div>
		<div class="domtabs field <?php echo $none;?>">
			<fieldset>
				<legend>Vstupní zkouška</legend>
				<div class='sll'>
					<?php echo $htmlExt->selectTag('CompanyOrderItem/entrance_examination', $ano_ne_list,null, array('label'=>'Vstupní zkouška'));?> <br class='clear' />
				</div>
				<br class='clear'/>
				<?php echo $htmlExt->textarea('CompanyOrderItem/examination_description', array('label'=>'Popis vstupní zkoušky', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />
			</fieldset>
			<fieldset>
				<legend>Doklady potřebné při nástupu</legend>
				<div class='sll'>
					<label>OP:</label>				<?php echo $htmlExt->checkbox('CompanyOrderItem/checkbox_op', array('label'=>'OP'));?> <br class='clear' />
					<label>Zápočtový list:</label>		<?php echo $htmlExt->checkbox('CompanyOrderItem/checkbox_zl', array('label'=>'Zápočtový list'));?> <br class='clear' />
					<label>Kvalifikační doklady:</label>	<?php echo $htmlExt->checkbox('CompanyOrderItem/checkbox_kd', array('label'=>'Kvalifikační doklady'));?> <br class='clear' />
				</div>
				<div class='slr'>
					<label>Výuční list:</label>			<?php echo $htmlExt->checkbox('CompanyOrderItem/checkbox_vl', array('label'=>'Výuční list'));?> <br class='clear' />
					<label>Zdravotní prohlídka:</label>	<?php echo $htmlExt->checkbox('CompanyOrderItem/checkbox_zp', array('label'=>'Zdravotní prohlídka'));?> <br class='clear' />
				</div>
				<br class='clear'/>
				<?php echo $htmlExt->textarea('CompanyOrderItem/jine_doklady', array('label'=>'Co ještě mít sebou k nástupu', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />
			</fieldset>
		</div>
		<div class="domtabs field <?php echo $none;?>">
			<br />
			<script>
				function upload_file_complete(url){
					var ul = $('foto_list');
					var li = new Element('li',{'class':'obal_span'}).inject(ul);
					new Element('img', {src:'/uploaded/requirement/foto/small/' + url,'class':'nahrada_za_foto'}).inject(li);
					
					new Element('input', {type:'hidden', name:'data[CompanyOrderItem][imgs][]', value:url + '|', 'class':'file_caption_value'}).inject(li);
					var submenu = new Element('div', {'class':'submenu'}).inject(new Element('span').inject(li));
						var zoom = new Element('a', {'href':'/uploaded/requirement/foto/large/' + url,'target':'_blank','rel':'clearbox[detail_fotogalerie]'})
							.inject(submenu)
						new Element('img', {'class':'icon',title:'Zvětšit fotografii',src:'/css/fastest/upload/zoom.png'})
							.inject(zoom)
						new Element('img', {'class':'icon',title:'Smazat fotografii',src:'/css/fastest/upload/delete.png'})
							.inject(submenu)
							.addEvent('click', img_delete_button.bind());
						new Element('img', {'class':'icon',title:'Editovat popis fotografie',src:'/css/fastest/upload/edit.png'})
							.inject(submenu)
							.addEvent('click', title_edit_button.bind());
					
					
					document.slideshow.ajax_init($('foto_list'));	
					mysort.addItems(li);
					
					$('CompanyOrderItemFotoUploader').getElement('.input_file_over').value = '';
					$('CompanyOrderItemFotoUploader').getElements('.browse_icon, .input_file').setStyle('display','inline');
					$('CompanyOrderItemFotoUploader').getElement('.smazat_icon').setStyle('display','none');
				}
			</script>
			<div>
				<?php $fileinput_setting = array(
					'label'			=> 'Fotografie',
					'upload_type' 	=> 'php',
					'filename_type'	=> 'unique',
					'file_ext'		=> array('jpg'),
					'paths' 		=> array(
						'path_to_file'	=> 'uploaded/requirement/foto/',
						//'path_to_ftp'	=> 'uploaded/reality/',
						'upload_script'	=> '/companies/requirement_upload_foto/',
						'status_path' 	=> '/get_status.php',
					),
					'methods'=>array(
						array('methods'	=>	array(array(
							'method'		=>	'resize',
							'new_width'		=>	800,
							'new_height'	=>	600
						)),
						'addon_path'=> 'large/'
						),
						array('methods'	=>	array(array(
							'method'		=>	'resize',
							'new_width'		=>	80,
							'new_height'	=>	80
						)),
						'addon_path'=> 'small/'
						)
					),
					'onComplete'	=> 'upload_file_complete'
				);
				
			
				
				?>
				<?php echo $fileInput->render('CompanyOrderItem/foto',$fileinput_setting);?>
			</div>
			<br /> 
			<ul class='foto_list' id='foto_list' style='position:relative;'>
				<?php
					if (isset($this->data['CompanyOrderItem']['imgs']) && !empty($this->data['CompanyOrderItem']['imgs']) && count($this->data['CompanyOrderItem']['imgs']) > 0){
						foreach($this->data['CompanyOrderItem']['imgs'] as $img){
							list($file, $caption) = explode('|',$img);
							echo "<li class='obal_span'><img src='/uploaded/requirement/foto/small/$file' title='$caption' alt='$caption' class='nahrada_za_foto tip_win'/><input class='file_caption_value' type='hidden' name='data[CompanyOrderItem][imgs][]' value='$img'/><span><div class='submenu'><a href='/uploaded/requirement/foto/large/$file' title='$caption' target='_blank' rel='clearbox[detail_fotogalerie]'><img src='/css/fastest/upload/zoom.png' title='Zvětšit fotografii' class='img_zoom_button icon' /></a><img src='/css/fastest/upload/delete.png' title='Smazat fotografii' class='img_delete_button icon' /><img src='/css/fastest/upload/edit.png' title='Editovat popis fotografie' class='title_edit_button icon' /></div></span></li>";
						}
					
					}
				?>
			
			</ul>
			<div id='editable_title'>
				<textarea style='width:95%'></textarea><br/>
				<p style='text-align:center'>
					<input type='button' class='button' value='Změnit' id='apply_editable_title'/>
					<input type='button' class='button' value='Zrušit' id='close_editable_title'/>
				</p>
			</div>
			<br />
			<br />
		</div>
         <?php if(isset($this->data['CompanyOrderItem']) && $this->data['CompanyOrderItem']['order_status']==2){?>
         <div class="domtabs field">
             <fieldset>
    			<legend>Shrnutí</legend>
                  <div class='sll'>
        				<?php echo $htmlExt->var_text('CompanyOrderItem/closed_comment',array('label'=>'Zavěrečný komentář'));?><br />
        				<?php echo $htmlExt->var_text('CompanyOrderItem/created',array('label'=>'Datum vytvoření','value'=>$fastest->czechDateTime($this->data['CompanyOrderItem']['created'])));?><br />
        				<?php echo $htmlExt->var_text('CompanyOrderItem/start_datetime',array('label'=>'Datum nástupu','value'=>$fastest->czechDateTime($this->data['CompanyOrderItem']['start_datetime'])));?><br />
        				<?php echo $htmlExt->var_text('CompanyOrderItem/closed_date',array('label'=>'Datum uzavření','value'=>$fastest->czechDateTime($this->data['CompanyOrderItem']['closed_date'])));?><br />
    		      </div><br />
             </fieldset>
             
             <fieldset>
    			<legend>Statistiky obsazení</legend>
                  <div class='sll'>
        				<?php echo $htmlExt->var_text('CompanyOrderItem/nl_count',array('label'=>'Počet lidí na nominační listině'));?><br />
        				<?php echo $htmlExt->var_text('CompanyOrderItem/cl_count',array('label'=>'Počet lidí na čekací listině'));?><br />
        				<?php echo $htmlExt->var_text('CompanyOrderItem/zam_count',array('label'=>'Počet zaměstnanců'));?><br />
        		  </div><br />
             </fieldset>                  
         </div>
         <?php } ?>
         
         
         
	</div>
	<div class='formular_action'>
		<?php if (empty($this->data['CompanyOrderItem']['id']) || ($this->data['CompanyOrderItem']['order_status'] == -1) || (isset($permission['update_order']) && $permission['update_order'] == 1)):?>
			<input type='button' value='Uložit' id='AddEditCompanyOrderItemSaveAndClose' />
		<?php endif;?>
		<input type='button' value='Zavřít' id='AddEditCompanyOrderItemClose'/>

       <? /* <input type='button' value='Publikovat na Facebook' id='AddOnFacebook'/> */ ?>

	</div>
</form>
<script>
	/*
	 * Zakladni funkce
	 */
	var domtab = new DomTabs({'className':'admin_dom'}); 
    $$('.integer, .float').inputLimit();

    if ($('AddOnFacebook')){
        $('AddOnFacebook').addEvent('click', function(e){
            var pp = $('company_template_edit_formular').clone();
            pp.getElements('input, select, textarea').removeClass('read_info').removeProperty('disabled');
            e.stop();
            domwin.newWindow({
                id			: 'domwin_facebook',
                sizes		: [500,500],
                scrollbars	: true,
                title		: 'Publikace na Facebook',
                languages	: false,
                type		: 'AJAX',
                ajax_url	: '/nabor_orders/fcb/',
                closeConfirm: true,
                max_minBtn	: false,
                modal_close	: false,
                remove_scroll: false,
                post_data: pp.toQueryString()
            });
            pp.dispose();
           // $('company_template_edit_formular').getElements('input, select, textarea').addClass('read_info').setProperty('disabled');
            $('AddOnFacebook').removeAttribute('disabled');
            $('AddEditCompanyOrderItemClose').removeAttribute('disabled');
        })
    }

    /*
    * Prepis nazvu profese do WWW sekce
    
    */
    $('CompanyOrderItemName').addEvent('change',function(e){
        if($('CompanyOrderItemWwwName').value == '')
            $('CompanyOrderItemWwwName').value = $('CompanyOrderItemName').value;
    });
	
	/*
	 * Pri zmene pracovni pozice se updatne seznam kalkulaci pro danou kalkulaci
	 * company_position_id  
	 */
	function load_money_items(){
		if ($('CompanyOrderItemCompanyWorkPositionId').value == '') {
			$('kalkulace_list').empty();	 	 
		 } else {
		 	new Request.HTML({
		 		url: '/companies/load_kalkulace/' + $('CompanyOrderItemCompanyWorkPositionId').value,
				update: $('kalkulace_list')
		 	}).send();
		 }
	}
	if ($('CompanyOrderItemCompanyWorkPositionId'))
		$('CompanyOrderItemCompanyWorkPositionId').addEvent('change', load_money_items);	

	/*
	 * Natahnuti dat z CompanyOrderTemplate, tyto data
	 * se propisi do jednotlivych policek, natahne se i seznam fotek
	 * a nacte se kalkulace
	 */
	$('CompanyOrderItemOrderTemplateId').addEvent('change',function(e){
		new Request.JSON({
			url: '/companies/load_template/' + this.value,
			onComplete:function(json){
				if (json){
					if (json.result === true){
						$('foto_list').empty();
						$each(json.data, function(value,col){
							el = $('CompanyOrderItem' + col.camelCase2().capitalize());
							if (el)
								el.setValue(value);	
							else if (col == 'imgs'){
								$each(json.data.imgs,function(img_url){
									ar = img_url.split('|')
									upload_file_complete(ar[0]);
								});
							}
						});
						load_money_items()
					} else {
						alert(json.message);
					}
				} else {
					alert('Chyba aplikace');
				}
			}
		}).send();
	});
	
	/*
	 * Zavreni Domwinu, bez ulozeni
	 */
	$('AddEditCompanyOrderItemClose').addEvent('click',function(e){new Event(e).stop(); 
         <?php if(isset($show)){ ?>
		domwin.closeWindow('domwin_show');
        <?php }else{ ?>
		domwin.closeWindow('domwin_pozice_add');
        <?php } ?>
	});
	

    
        
    
	/*
	 * Ulozeni objednavky, respektive pridani objednavky,
	 * jelikoz nelze objednavka nelze objednavka,
	 * az tedy na urcite skupiny uzivatelu !!! dodelat
	 */
	if ($('AddEditCompanyOrderItemSaveAndClose')) {
	   	$('AddEditCompanyOrderItemSaveAndClose').addEvent('click', function(e){
	   		new Event(e).stop();
	   		valid_result = validation.valideForm('company_template_edit_formular');
	   		
            /**
             * defaultne nastav status objednavky na objednano(zavazna)
             */
            if($('CompanyOrderItemId').value == '')
                $('CompanyOrderItemOrderStatus').value = 1;
            
            /**
             * Moznost kdy pridavam novou objednavku a pokud jsem nevyplnil datum a cas nastupu
             * tak mohu ulozit predbeznou...
             */
            if($('calbut_CompanyOrderItemStartDatetime').value == '' && valid_result == true && $('CompanyOrderItemId').value == '')
                if(confirm('Přejete si přidat objednávku jako přeběžnou?')){
                    $('CompanyOrderItemOrderStatus').value = -1;
                }
                else
                    valid_result = Array("Musite zvolit datum nástupu");

            /**
             * podminka ktera hlida editaci predbezne objednavky a pokud je vyplnen nastup tak se muze ulozit na zavaznou
             */
            if(($('calbut_CompanyOrderItemStartDatetime').value == '') && (valid_result == true) && ($('CompanyOrderItemOrderStatus').value == -1) && ($('CompanyOrderItemId').value != ''))        
               valid_result = Array("Musite zvolit datum nástupu");   
                  
            if (valid_result == true) {
                //pokud ukladam predbeznou objednavku muze se zmenit pouze na objednanou(zavaznou)
                if($('CompanyOrderItemOrderStatus').value == -1 && $('CompanyOrderItemId').value != '')
                    $('CompanyOrderItemOrderStatus').value = 1;

                button_preloader($('AddEditCompanyOrderItemSaveAndClose'));
	   			new Request.JSON({
	   				url: $('company_template_edit_formular').action,
	   				onComplete: function(json){
	   					if (json) {
	   						if (json.result === true) {
	   							domwin.loadContent('domwin_pozice');
	   							domwin.closeWindow('domwin_pozice_add');
                                button_preloader_disable($('AddEditCompanyOrderItemSaveAndClose'));
	   						}
	   						else {
	   							alert(json.message);
	   						}
	   					}
	   					else {
	   						alert('Chyba apliakce');
	   					}
	   				}
	   			}).post($('company_template_edit_formular'));
	   		} else {
	   			var error_message = new MyAlert();
	   			error_message.show(valid_result)
	   		}
	   	});
	}

	/* 
	 * Nadefinovani validace, u objednavky se jedna o veskere data na uvodni strance
	 * a plus o smennost (smennost odstranena, misto toho odebrano ze smennosti prazdne pole)
	 */

	validation.define('company_template_edit_formular',{
		'CompanyOrderItemName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit požadovanou profesi'}
		},
        'calbut_CompanyOrderItemOrderDate': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit datum objednávky'}
		},
		'CompanyOrderItemCountOfFreePosition': {
			'testReq': {'condition':'not_empty','err_message':'Musíte zadat počet požadovaných zaměstnanců'}
		},
		'CompanyOrderItemCountOfSubstitute': {
			'testReq': {'condition':'not_empty','err_message':'Musíte zadat počet požadovaných náhradníků'}
		},
		'CompanyOrderItemStartPlace': {
			'testReq': {'condition':'not_empty','err_message':'Musíte zadat místo nástupu'}
		},
		'CompanyOrderItemStartComment': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vložit komentář k nástupu'}
		},
		'CompanyOrderItemOrderTemplateId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vybrat šablonu'}
		}
	});
	validation.generate('company_template_edit_formular',<?php echo (isset($this->data['CompanyOrderItem']['id']) || ($generate == true) )?'true':'false';?>);

	/*
	 * Veskere funkce spojene s fotogalerii
	 */
	function title_edit_button(e){
		var event = new Event(e);
		event.stop();
		$('editable_title').setStyle('display','block');
		textarea = $('editable_title').getElement('textarea');
		curr_img = event.target.getParent('li').getElement('img');
		curr_img.addClass('edit_now');
		textarea.value = curr_img.getProperty('title');
	}
	
	function img_delete_button(e){
		var event = new Event(e);
		event.stop();
		if (confirm('Opravdu si přejete odstranit tuto fotografii?'))
			event.target.getParent('li').dispose();
	}
	
	$$('.title_edit_button').addEvent('click', title_edit_button.bind());
	$$('.img_delete_button').addEvent('click', img_delete_button.bind());
	
	$('close_editable_title').addEvent('click', function(e){
		new Event(e).stop();
		$('editable_title').setStyle('display','none');
	});
	
	$('apply_editable_title').addEvent('click', function(e){
		new Event(e).stop();
		textarea = $('editable_title').getElement('textarea');
		element = $('foto_list').getElement('.edit_now');
		element
			.setProperties({
				title: textarea.value,
				alt: textarea.value
			})
			.removeClass('edit_now');
		
		hidden = element.getNext('.file_caption_value');
		hidden_text = hidden.value.split('|');
		hidden_text[1] = textarea.value;
		hidden.value = hidden_text[0] + '|' + hidden_text[1];
		textarea.value = '';
		$('editable_title').setStyle('display','none');
	});
	
	var mysort = new Sortables($('foto_list'), {
	  	constrain: false,
	  	clone: true
	});
	
	document.slideshow.ajax_init($('foto_list'));	
	
	
	var Tips_help = new Tips($$('.tip_win'),{
		showDelay: 100,
		hideDelay: 400,
		className: 'tip_win',
		offsets: {'x': -3, 'y': 70},
		fixed: true
	});
    
    /**
     * pokud je nastaven show dej vse disabled
     */
    <?php if(isset($show)): ?>
    	$('company_template_edit_formular').getElements('input, select, textarea')
    		.addClass('read_info')
    		.setProperty('disabled');
       if($('DoFinallyOrder'))     
          $('DoFinallyOrder').addClass('hidden');   
       if($('AddEditCompanyOrderItemSaveAndClose'))    
          $('AddEditCompanyOrderItemSaveAndClose').addClass('hidden');   
       $('AddEditCompanyOrderItemClose').removeAttribute('disabled');

        if($('AddOnFacebook')){
            $('AddOnFacebook').removeAttribute('disabled')
        }
	<?php endif; ?>




</script>