<form action='/companies/aktivity_edit/' method='post' id='company_activity_edit_formular'>
    <?php echo $htmlExt->hidden('DeleteExtends');?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Aktivita</a></li>
			<li class="ousko"><a href="#" id='add_task_from_activity'>Vytvořit k této aktivitě úkol</a></li>
		</ul>
	</div>

	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<?php echo $htmlExt->hidden('CompanyActivity/id');?>
			<?php echo $htmlExt->hidden('CompanyActivity/company_id');?>
			<?php echo $htmlExt->hidden('CompanyActivity/self_manager_id');?>
			<div class='sll'>
			    <?php echo $htmlExt->selectTag('CompanyActivity/company_activity_type_id',$company_activity_type_list,null,array('tabindex'=>4,'label'=>'Typ aktivity'));?> <br class="clear">
			</div>
			<div class='slr' id="shift">
			    <?php echo $htmlExt->hidden('Ids/shift_type',array('value'=>$extended['shift_type']['id']));?>
			    <?php echo $htmlExt->selectTag('Extended/shift_type',$shift_type_list,$extended['shift_type']['value'],array('tabindex'=>4,'label'=>'Směna'));?> <br class="clear">
			</div>
			<fieldset id="classical">
				<legend>Prosím vyplňte následující údaje</legend>
				<?php echo $htmlExt->input('CompanyActivity/name',array('tabindex'=>1,'label'=>'Název','class'=>'long','label_class'=>'long'));?> <br class="clear">
				<div class='sll'>
					<?php echo $htmlExt->inputDate('CompanyActivity/activity_datetime',array('tabindex'=>1,'label'=>'Datum'));?> <br class="clear">
					<?php echo $htmlExt->selectTag('CompanyActivity/company_contact_id',$comapany_contact_list,null,array('tabindex'=>3,'label'=>'Kontakt'));?> <br class="clear">
				</div>
				<div class='slr'>
					<?php echo $htmlExt->input('CompanyActivity/mesto',array('tabindex'=>2,'label'=>'Město'));?> <br class="clear">
				</div>
			</fieldset>

			<fieldset id="extended" class="none">
                <div class='sll'>
                    <?php echo $htmlExt->hidden('Ids/suppose_come',array('value'=>$extended['suppose_come']['id']));?>
                    <?php echo $htmlExt->input('Extended/suppose_come',array('value'=>$extended['suppose_come']['value'],'tabindex'=>1,'label'=>'Mělo nastoupit'));?> <br class="clear">
                    <?php echo $htmlExt->hidden('Ids/replaced',array('value'=>$extended['replaced']['id']));?>
                    <?php echo $htmlExt->input('Extended/replaced',array('value'=>$extended['replaced']['value'],'tabindex'=>1,'label'=>'Nahrazeno'));?> <br class="clear">
                </div>
                <div class='slr'>
                     <?php echo $htmlExt->hidden('Ids/comes',array('value'=>$extended['comes']['id']));?>
                     <?php echo $htmlExt->input('Extended/comes',array('value'=>$extended['comes']['value'], 'tabindex'=>1,'label'=>'Nastoupilo'));?> <br class="clear">
                     <?php echo $htmlExt->hidden('Ids/sanctions',array('value'=>$extended['sanctions']['id']));?>
                     <?php echo $htmlExt->input('Extended/sanctions',array('value'=>$extended['sanctions']['value'],'tabindex'=>1,'label'=>'Sankce <br />(suma, duvod)'));?> <br class="clear">
                </div>
                <br class="clear">
                <div style="float:left;">
                <label style="position:relative; float:left; width:96px; display:inline;">Nenastoupilo <br />(jmenovitě, duvod):</label>
                    <table id="not-comes-table" width="100%" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr><td width="32%"></td><td width="32%"><input type="button" value="Přidat pole" tabindex="18" class="button" id="add-not-comes" /></td><td width="32%"></td></tr>
                        <tr><td><?php echo $htmlExt->hidden('Ids/1|not_comes',array('value'=>$extended['1|not_comes']['id']));?>
                                <?php echo $htmlExt->input('Extended/1/not_comes',array('value'=>$extended['1|not_comes']['value'],'tabindex'=>1, 'class'=>'not_comes'));?></td>
                            <td><?php echo $htmlExt->hidden('Ids/2|not_comes',array('value'=>$extended['2|not_comes']['id']));?>
                                <?php echo $htmlExt->input('Extended/2/not_comes',array('value'=>$extended['2|not_comes']['value'], 'tabindex'=>1, 'class'=>'not_comes'));?></td>
                            <td><?php echo $htmlExt->hidden('Ids/3|not_comes',array('value'=>$extended['3|not_comes']['id']));?>
                                <?php echo $htmlExt->input('Extended/3/not_comes',array('value'=>$extended['3|not_comes']['id'], 'tabindex'=>1, 'class'=>'not_comes'));?></td>
                        </tr>
                        <? if(Count($extended['not_comes']) > 0):  ?>
                            <?php $i = 0; ?>
                            <?  foreach($extended['not_comes'] as $rand => $item): ?>
                                    <? if($i % 3 == 0){ echo '<tr>'; } ?>
                                        <?php $i++; ?>
                                        <td><?php echo $htmlExt->hidden('Ids/'.$rand.'|not_comes',array('value'=>$item['id'], 'tabindex'=>1, 'class'=>'not_comes'));?>
                                        <?php echo $htmlExt->input('Extended/'.$rand.'/not_comes',array('value'=>$item['value'], 'tabindex'=>1, 'class'=>'not_comes'));?></td>
                                     <? if($i % 3 == 0){ echo '<td><a href="#" class="ta trash delete-row" > &nbsp </a></td></tr>'; } ?>

                            <? endforeach; ?>

                        <? endif;  ?>
                    </tbody>
                    </table>
                    <br class="clear">
                </div>
                <div class='sll'>
                    <?php //echo $htmlExt->hidden('Ids/order',array('value'=>$extended['order']['id']));?>
                    <?php //echo $htmlExt->input('Extended/order',array('value'=>$extended['order']['value'],'tabindex'=>4,'label'=>'Objednávka'));?> <br class="clear">
                    <?php echo $htmlExt->hidden('Ids/new_comes',array('value'=>$extended['new_comes']['id']));?>
                    <?php echo $htmlExt->input('Extended/new_comes',array('value'=>$extended['new_comes']['value'] ,'tabindex'=>1,'label'=>'Nové nástupy'));?> <br class="clear">
                </div>
                <div class='slr'>
                    <?php //echo $htmlExt->hidden('Ids/profession',array('value'=>$extended['profession']['id']));?>
                    <?php //echo $htmlExt->input('Extended/profession',array('value'=>$extended['profession']['value'],'tabindex'=>4,'label'=>'Profese'));?>
                    <?php echo $htmlExt->hidden('Ids/leaves',array('value'=>$extended['leaves']['id']));?>
                    <?php echo $htmlExt->input('Extended/leaves',array('value'=>$extended['leaves']['value']  ,'tabindex'=>1,'label'=>'Odchody'));?> <br class="clear">
                </div>
                  <br class="clear">
                <div class="textarea">
                    <?php echo $htmlExt->hidden('Ids/hire_activity',array('value'=>$extended['hire_activity']['id']));?>
                    <?php echo $htmlExt->textarea('Extended/hire_activity',array('value'=>$extended['hire_activity']['value']  ,'tabindex'=>1,'label'=>'Náborová aktivita','style'=>'width:78%; height: 48px;'));?> <br class="clear">
                </div>
           </fieldset>
           <fieldset id="extended2" class="none">

                        <div class="form-extended">
                          <div>
                            <table id="report_table_1">
                              <thead>
                                  <tr>
                                      <td colspan="3" class="form-head">Konkurence</td>
                                      <td></td>
                                      <td class="form-head">Naši zaměstnanci</td>

                                  </tr>
                                  <tr>
                                      <th width="22%">Agentura</th>
                                      <th width="22%">Profese</th>
                                      <th width="22%">Počet zaměstnanců</th>
                                      <th width="12%"></th>
                                      <th width="22%">Aktuální počet</th>

                                  </tr>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td><?php echo $htmlExt->hidden('Ids/1|agency',array('value'=>$extended['1|agency']['id']));?>
                                          <?php echo $htmlExt->input('Extended/1/agency',array('value'=>$extended['1|agency']['value'],'tabindex'=>1));?></td>
                                      <td><?php echo $htmlExt->hidden('Ids/1|profession_report',array('value'=>$extended['1|profession_report']['id']));?>
                                          <?php echo $htmlExt->input('Extended/1/profession_report',array('value'=>$extended['1|profession_report']['value'],'tabindex'=>1));?></td>
                                      <td><?php echo $htmlExt->hidden('Ids/1|employe_count',array('value'=>$extended['1|employe_count']['id']));?>
                                          <?php echo $htmlExt->input('Extended/1/employe_count',array('value'=>$extended['1|employe_count']['value'],'tabindex'=>1,'class'=>'make_change employe_c need_proff'));?></td>
                                      <td></td>
                                      <td><?php echo $htmlExt->hidden('Ids/1|current_count',array('value'=>$extended['1|current_count']['id']));?>
                                          <?php echo $htmlExt->input('Extended/1/current_count',array('value'=>$extended['1|current_count']['value'],'tabindex'=>1 , 'class'=>'make_change current_c need_proff'));?></td>

                                       <? if(Count($extended['profession_report']) > 0):  ?>
                                           <?  foreach($extended['profession_report'] as $rand => $item): ?>
                                            <? if($rand != 1): ?>
                                             <tr><td><?php echo $htmlExt->hidden('Ids/'.$rand.'|agency',array('value'=>$extended[$rand.'|agency']['id']));?>
                                                     <?php echo $htmlExt->input('Extended/'.$rand.'/agency',array('value'=>$extended[$rand.'|agency']['value'],'tabindex'=>1));?></td>
                                                 <td><?php echo $htmlExt->hidden('Ids/'.$rand.'|profession_report',array('value'=>$extended[$rand.'|profession_report']['id']));?>
                                                     <?php echo $htmlExt->input('Extended/'.$rand.'/profession_report',array('value'=>$extended[$rand.'|profession_report']['value'],'tabindex'=>1));?></td>
                                                  <td><?php echo $htmlExt->hidden('Ids/'.$rand.'|employe_count',array('value'=>$extended[$rand.'|employe_count']['id']));?>
                                                     <?php echo $htmlExt->input('Extended/'.$rand.'/employe_count',array('value'=>$extended[$rand.'|employe_count']['value'],'tabindex'=>1,'class'=>'make_change employe_c need_proff'));?></td>
                                                  <td></td>
                                                  <td><?php echo $htmlExt->hidden('Ids/'.$rand.'|current_count',array('value'=>$extended[$rand.'|current_count']['id']));?>
                                                     <?php echo $htmlExt->input('Extended/'.$rand.'/current_count',array('value'=>$extended[$rand.'|current_count']['value'],'tabindex'=>1 , 'class'=>'make_change current_c need_proff'));?></td>
                                                  <td><a href="#" class="ta trash delete-row" > &nbsp </a></td>
                                            </tr>
                                            <? endif; ?>
                                           <? endforeach; ?>
                                       <? endif;  ?>
                                  </tr>
                               </tbody>
                               <tfoot>
                                  <tr>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td><span id="employe_count_sum"></span></td>
                                      <td><span id="current_sum"></span></td>
                                  </tr>
                                  <tr><td><input type="button" value="Přidat pole" tabindex="18" class="button" id="add-profession" style="width:100px; margin:4px;" /></td>
                                      <td></td><td></td><td></td></tr>
                               </tfoot>
                            </table>
                          </div>

                          <div>
                              <table id="report_table_4">
                                  <tr><td rowspan="2" class="form-head">Fluktuace</td>
                                      <th>Shrnutí začátek týdne</th><th>Odchodů</th><th>Nahrazeno</th>
                                      <tr>
                                          <td><?php echo $htmlExt->hidden('Ids/start_week',array('value'=>$extended['start_week']['id']));?>
                                              <?php echo $htmlExt->input('Extended/start_week',array('value'=>$extended['start_week']['value'],'tabindex'=>1));?></td>
                                          <td><?php echo $htmlExt->hidden('Ids/leaves_report',array('value'=>$extended['leaves_report']['id']));?>
                                              <?php echo $htmlExt->input('Extended/leaves_report',array('value'=>$extended['leaves_report']['value'],'tabindex'=>1));?></td>
                                          <td><?php echo $htmlExt->hidden('Ids/replaced_report',array('value'=>$extended['replaced_report']['id']));?>
                                              <?php echo $htmlExt->input('Extended/replaced_report',array('value'=>$extended['replaced_report']['value'],'tabindex'=>1));?></td>
                                      </tr>
                                  </tr>
                               </table>
                          </div>

                          <div>
                              <table id="report_table_2">
                                   <tr><td rowspan="2" class="form-head">Shrnutí týdeních objednávek</td>
                                       <th>Volných pozic</th><th>Obsazených pozic</th>
                                       <tr>
                                          <td><?php echo $htmlExt->hidden('Ids/free_positions',array('value'=>$extended['free_positions']['id']));?>
                                              <?php echo $htmlExt->input('Extended/free_positions',array('value'=>$extended['free_positions']['value'],'tabindex'=>1));?></td>
                                          <td><?php echo $htmlExt->hidden('Ids/filled_positions',array('value'=>$extended['filled_positions']['id']));?>
                                              <?php echo $htmlExt->input('Extended/filled_positions',array('value'=>$extended['filled_positions']['value'],'tabindex'=>1));?></td>
                                       </tr>
                                   </tr>
                                   <tr><th>Odpracovaných hodin</th><td><?php echo $htmlExt->hidden('Ids/worked_hours',array('value'=>$extended['worked_hours']['id']));?>
                                                                             <?php echo $htmlExt->input('Extended/worked_hours',array('value'=>$extended['worked_hours']['value'],'tabindex'=>1));?></td>
                                   </tr>
                                   <tr><th>Předběžná týdenní fakturace</th><td><?php echo $htmlExt->hidden('Ids/week_billing',array('value'=>$extended['week_billing']['id']));?>
                                                                               <?php echo $htmlExt->input('Extended/week_billing',array('value'=>$extended['week_billing']['value'],'tabindex'=>1));?></td>
                                   </tr>
                              </table>
                          </div>

                          <div><table id="report_table_3" width="98%">
                                   <thead>
                                      <tr><td colspan="3" class="form-head">Schůzky v podniku<td/></tr>
                                      <tr><th>Jméno</th><th>Důvod schůzky</th><th>Řešení</th></tr>
                                   </thead>
                                   <tbody>
                                      <tr><td><?php echo $htmlExt->hidden('Ids/1|company_meetings',array('value'=>$extended['1|company_meetings']['id']));?>
                                              <?php echo $htmlExt->input('Extended/1/company_meetings',array('value'=>$extended['1|company_meetings']['value'],'tabindex'=>1));?></td>
                                          <td><?php echo $htmlExt->hidden('Ids/1|reason_of_meeting',array('value'=>$extended['1|reason_of_meeting']['id']));?>
                                              <?php echo $htmlExt->input('Extended/1/reason_of_meeting',array('value'=>$extended['1|reason_of_meeting']['value'],'tabindex'=>1));?></td>
                                          <td><?php echo $htmlExt->hidden('Ids/1|solution',array('value'=>$extended['1|solution']['id']));?>
                                              <?php echo $htmlExt->input('Extended/1/solution',array('value'=>$extended['1|solution']['value'],'tabindex'=>1));?></td>
                                       </tr>
                                          <? if(Count($extended['company_meetings']) > 0):  ?>
                                              <?  foreach($extended['company_meetings'] as $rand => $item): ?>
                                               <? if($rand != 1): ?>
                                                <tr><td><?php echo $htmlExt->hidden('Ids/'.$rand.'|company_meetings',array('value'=>$extended[$rand.'|company_meetings']['id']));?>
                                                        <?php echo $htmlExt->input('Extended/'.$rand.'/company_meetings',array('value'=>$extended[$rand.'|company_meetings']['value'],'tabindex'=>1));?></td>
                                                     <td><?php echo $htmlExt->hidden('Ids/'.$rand.'|reason_of_meeting',array('value'=>$extended[$rand.'|reason_of_meeting']['id']));?>
                                                        <?php echo $htmlExt->input('Extended/'.$rand.'/reason_of_meeting',array('value'=>$extended[$rand.'|reason_of_meeting']['value'],'tabindex'=>1));?></td>
                                                     <td><?php echo $htmlExt->hidden('Ids/'.$rand.'|solution',array('value'=>$extended[$rand.'|solution']['id']));?>
                                                         <?php echo $htmlExt->input('Extended/'.$rand.'/solution',array('value'=>$extended[$rand.'|solution']['value'],'tabindex'=>1));?></td>
                                                     <td><a href="#" class="ta trash delete-row" > &nbsp </a></td>
                                               </tr>
                                               <? endif; ?>
                                              <? endforeach; ?>
                                          <? endif;  ?>
                                   </tbody>
                                   <tfoot>
                                        <tr><td><input type="button" value="Přidat pole" tabindex="18" class="button" id="add-meeting" style="width:100px; margin:4px;"/></td>
                                            <td></td><td></td><td></td></tr>
                                   </tfoot>
                               </table>
                          </div>
                        </div>
                      </fieldset>

                </div>
                <br />
                <p>
                    <strong>V reporte očakávame informácie o</strong><br />
                    Kontrole vyťaženosti a včasných príchodov dopravy k podnikom:<br />
                    Kontrole na prítomnosť alkoholu v dychu : Menný zoznam, výsledok testu:<br />
                    Kontrole zamestnancov či nosia predpísané OPP:stav,príp.porušenia a pokuty<br />
                    Spracovaní stavu dochádzky:kto prečo nenastúpil do práce a kým bol nahradený<br />
                    Administratívnej práci:iba v skratke napr. spracovanie zmluv pre nových zamestnancov<br />
                    Riešení žiadostí zamestnancov:    iba v skratke napr. riešenie reklamácie hodín u pána Baláža z výsledkom takým a takým...<br />
                    Komunikácii z personálnym,majstrami,vedúcim výroby ... :čo najčastejšie<br />
                    Zháňaní informácii o práci konkurencie:aspoň raz týždenne
                </p>
				<br class='clear' />
				<?php echo $htmlExt->textarea('CompanyActivity/text',array('tabindex'=>5,'rows'=>10,'label'=>'Popis','class'=>'long','label_class'=>'long'));?><br class='clear' />

		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditCompanyActivitySaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyActivityClose'/>
	</div>
</form>
<style>
    #not-comes-table{
       /* margin-left: 100px;*/
        width:427px;
        float:left; position:relative;

    }
    .form-head{
        font-size: 14px;
        font-weight: bold;
        padding: 5px;
    }
    #not-comes-table input[type="text"]{
        width:92%;
    }
    #report_table_1, #report_table_3{
        text-align:center;
    }
    #report_table_1 th, #report_table_2 th , #report_table_3 th, #report_table_4 th{
        font-weight:bold;
    }

    .form-extended input{
      width: 90%;
      text-align:center;
    }
    .form-extended .form-head, th{
      text-align:center;
    }
    .textarea label{
        width: 17%;
    }
</style>
<script>
      var cms_user_group = <?= $cms_user; ?>;

      function addToValid( type ){
            if(type == '1'){
                validation.val['company_activity_edit_formular']['ExtendedSupposeCome'] = { testReq:{'condition':'not_empty','err_message': 'Musíte vyplnit pole "Mělo nastoupit"'}};
                $('ExtendedSupposeCome').addEvent('keyup', validation.validation_event.bindWithEvent(validation,validation.val['company_activity_edit_formular']['ExtendedSupposeCome'] ));
                validation.val['company_activity_edit_formular']['ExtendedComes'] = { testReq:{'condition':'not_empty','err_message': 'Musíte vyplnit pole "Nastoupilo"'}};
                $('ExtendedComes').addEvent('keyup', validation.validation_event.bindWithEvent(validation,validation.val['company_activity_edit_formular']['ExtendedComes'] ));
                validation.val['company_activity_edit_formular']['ExtendedReplaced'] = { testReq:{'condition':'not_empty','err_message': 'Musíte vyplnit pole "Nahrazeno"'}};
                $('ExtendedReplaced').addEvent('keyup', validation.validation_event.bindWithEvent(validation,validation.val['company_activity_edit_formular']['ExtendedReplaced'] ));
                validation.val['company_activity_edit_formular']['ExtendedSanctions'] = { testReq:{'condition':'not_empty','err_message': 'Musíte vyplnit pole "Sankce"'}};
                $('ExtendedSanctions').addEvent('keyup', validation.validation_event.bindWithEvent(validation,validation.val['company_activity_edit_formular']['ExtendedSanctions'] ));


                if ($('ExtendedOrder')){
                    validation.val['company_activity_edit_formular']['ExtendedOrder'] = { testReq:{'condition':'not_empty','err_message': 'Musíte vyplnit pole "Objednávka"'}};
                    $('ExtendedOrder').addEvent('keyup', validation.validation_event.bindWithEvent(validation,validation.val['company_activity_edit_formular']['ExtendedOrder'] ));
                }

                //validation.val['company_activity_edit_formular']['ExtendedProfession'] = { testReq:{'condition':'not_empty','err_message': 'Musíte vyplnit pole "Profese"'}};
                //$('ExtendedProfession').addEvent('keyup', validation.validation_event.bindWithEvent(validation,validation.val['company_activity_edit_formular']['ExtendedProfession'] ));
                validation.val['company_activity_edit_formular']['ExtendedNewComes'] = { testReq:{'condition':'not_empty','err_message': 'Musíte vyplnit pole "Nové nástupy"'}};
                $('ExtendedNewComes').addEvent('keyup', validation.validation_event.bindWithEvent(validation,validation.val['company_activity_edit_formular']['ExtendedNewComes'] ));
                validation.val['company_activity_edit_formular']['ExtendedLeaves'] = { testReq:{'condition':'not_empty','err_message': 'Musíte vyplnit pole "Odchody"'}};
                $('ExtendedLeaves').addEvent('keyup', validation.validation_event.bindWithEvent(validation,validation.val['company_activity_edit_formular']['ExtendedLeaves'] ));
                validation.val['company_activity_edit_formular']['Extended1NotComes'] = { testReq:{'condition':'not_empty','err_message': 'Musíte vyplnit alespoň první pole "Nenastoupilo"'}};
                $('Extended1NotComes').addEvent('keyup', validation.validation_event.bindWithEvent(validation,validation.val['company_activity_edit_formular']['Extended1NotComes'] ));
                validation.val['company_activity_edit_formular']['CompanyActivityName'] = { testReq:{'condition':'not_empty','err_message': 'Musíte vyplnit název'}};
                $('CompanyActivityName').addEvent('keyup', validation.validation_event.bindWithEvent(validation,validation.val['company_activity_edit_formular']['CompanyActivityName'] ));
            }else if(type == '2'){
                validation.val['company_activity_edit_formular']['ExtendedStartWeek'] = { testReq:{'condition':'not_empty','err_message': 'Musíte vyplnit pole "Shrnutí začátek týdne"'}};
                $('ExtendedStartWeek').addEvent('keyup', validation.validation_event.bindWithEvent(validation,validation.val['company_activity_edit_formular']['ExtendedStartWeek'] ));
                validation.val['company_activity_edit_formular']['ExtendedLeavesReport'] = { testReq:{'condition':'not_empty','err_message': 'Musíte vyplnit pole "Počet odchodů"'}};
                $('ExtendedLeavesReport').addEvent('keyup', validation.validation_event.bindWithEvent(validation,validation.val['company_activity_edit_formular']['ExtendedLeavesReport'] ));
                validation.val['company_activity_edit_formular']['ExtendedReplacedReport'] = { testReq:{'condition':'not_empty','err_message': 'Musíte vyplnit pole "Počet nahrazeno"'}};
                $('ExtendedReplacedReport').addEvent('keyup', validation.validation_event.bindWithEvent(validation,validation.val['company_activity_edit_formular']['ExtendedReplacedReport'] ));
                validation.val['company_activity_edit_formular']['ExtendedReplacedReport'] = { testReq:{'condition':'not_empty','err_message': 'Musíte vyplnit pole "Mělo nastoupit"'}};
                $('ExtendedFreePositions').addEvent('keyup', validation.validation_event.bindWithEvent(validation,validation.val['company_activity_edit_formular']['ExtendedFreePositions'] ));
                validation.val['company_activity_edit_formular']['ExtendedFreePositions'] = { testReq:{'condition':'not_empty','err_message': 'Musíte vyplnit pole "Počet volných pozic"'}};
                $('ExtendedWorkedHours').addEvent('keyup', validation.validation_event.bindWithEvent(validation,validation.val['company_activity_edit_formular']['ExtendedWorkedHours'] ));
                validation.val['company_activity_edit_formular']['ExtendedWorkedHours'] = { testReq:{'condition':'not_empty','err_message': 'Musíte vyplnit pole "Odpracovaných hodin"'}};
                $('ExtendedFilledPositions').addEvent('keyup', validation.validation_event.bindWithEvent(validation,validation.val['company_activity_edit_formular']['ExtendedFilledPositions'] ));
                validation.val['company_activity_edit_formular']['ExtendedFilledPositions'] = { testReq:{'condition':'not_empty','err_message': 'Musíte vyplnit pole "Počet obsazených pozic"'}};
                $('ExtendedWeekBilling').addEvent('keyup', validation.validation_event.bindWithEvent(validation,validation.val['company_activity_edit_formular']['ExtendedWeekBilling'] ));
                validation.val['company_activity_edit_formular']['ExtendedWeekBilling'] = { testReq:{'condition':'not_empty','err_message': 'Musíte vyplnit pole "Předběžná týdenní fakturace"'}};
                $('Extended1CompanyMeetings').addEvent('keyup', validation.validation_event.bindWithEvent(validation,validation.val['company_activity_edit_formular']['Extended1CompanyMeetings'] ));
                validation.val['company_activity_edit_formular']['Extended1CompanyMeetings'] = { testReq:{'condition':'not_empty','err_message': 'Musíte vyplnit alespoň jedno pole "Schuzka"'}};
            }
      }
      function removeFromValid(type ){
            if(type== '1'){
                $('ExtendedSupposeCome').removeClass('invalid').removeClass('require').removeClass('valid');
                validation.remove('company_activity_edit_formular','ExtendedSupposeCome');
                $('ExtendedComes').removeClass('invalid').removeClass('require').removeClass('valid');
                validation.remove('company_activity_edit_formular','ExtendedComes');
                $('ExtendedReplaced').removeClass('invalid').removeClass('require').removeClass('valid');
                validation.remove('company_activity_edit_formular','ExtendedReplaced');
                $('ExtendedSanctions').removeClass('invalid').removeClass('require').removeClass('valid');
                validation.remove('company_activity_edit_formular','ExtendedSanctions');

                if ($('ExtendedOrder')){
                    $('ExtendedOrder').removeClass('invalid').removeClass('require').removeClass('valid');
                    validation.remove('company_activity_edit_formular','ExtendedOrder');
                }

                //$('ExtendedProfession').removeClass('invalid').removeClass('require').removeClass('valid');
                //validation.remove('company_activity_edit_formular','ExtendedProfession');
                $('ExtendedLeaves').removeClass('invalid').removeClass('require').removeClass('valid');
                validation.remove('company_activity_edit_formular','ExtendedLeaves');
                $('ExtendedNewComes').removeClass('invalid').removeClass('require').removeClass('valid');
                validation.remove('company_activity_edit_formular','ExtendedNewComes');
                $('Extended1NotComes').removeClass('invalid').removeClass('require').removeClass('valid');
                validation.remove('company_activity_edit_formular','Extended1NotComes');

            }else if(type== '2'){
                $('ExtendedStartWeek').removeClass('invalid').removeClass('require').removeClass('valid');
                validation.remove('company_activity_edit_formular','ExtendedStartWeek');
                $('ExtendedLeavesReport').removeClass('invalid').removeClass('require').removeClass('valid');
                validation.remove('company_activity_edit_formular','ExtendedLeavesReport');
                $('ExtendedReplacedReport').removeClass('invalid').removeClass('require').removeClass('valid');
                validation.remove('company_activity_edit_formular','ExtendedReplacedReport');
                $('ExtendedFreePositions').removeClass('invalid').removeClass('require').removeClass('valid');
                validation.remove('company_activity_edit_formular','ExtendedFreePositions');
                $('ExtendedFilledPositions').removeClass('invalid').removeClass('require').removeClass('valid');
                validation.remove('company_activity_edit_formular','ExtendedFilledPositions');
                $('ExtendedWorkedHours').removeClass('invalid').removeClass('require').removeClass('valid');
                validation.remove('company_activity_edit_formular','ExtendedWorkedHours');
                $('ExtendedWeekBilling').removeClass('invalid').removeClass('require').removeClass('valid');
                validation.remove('company_activity_edit_formular','ExtendedWeekBilling');
                $('Extended1CompanyMeetings').removeClass('invalid').removeClass('require').removeClass('valid');
                validation.remove('company_activity_edit_formular','Extended1CompanyMeetings');
            }
      }

     function viewExtended(){
        validation.define('company_activity_edit_formular',{
                   'CompanyActivityName': {
                       'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
                   }
        });
        var type = $('CompanyActivityCompanyActivityTypeId').value;

           if( cms_user_group == 4 || cms_user_group == 1){
                $('shift').removeClass('none');
                if( type == 14){
                    $('extended').removeClass('none');
                    addToValid('1');
                }else{
                    $('extended').addClass('none');

                    removeFromValid('1');
                }
           }
           if( cms_user_group == 18 || cms_user_group == 1) { //|| cms_user_group == 1
                $('shift').addClass('none');
                if( type == 15){
                    $('extended2').removeClass('none');
                        // $('classical').addClass('none');
                    addToValid('2');
                }else{
                    $('extended2').addClass('none');
                        // $('classical').removeClass('none');
                    removeFromValid('2');
                }
           }
        validation.generate('company_activity_edit_formular',<?php echo (isset($this->data['CompanyActivity']['id']))?'true':'false';?>);
        if($('CompanyActivityName').value != ''){
            $('CompanyActivityName').addClass('valid');
        }
     }

     function validateExtReport(){
        if( cms_user_group == 18 ){
            var check = true;
            var els =  $$('.need_proff');
            Array.each( els, function(el, i){
                if(el.value != ''){
                     var r = el.get('id').match(/\d/g);
                     var rand = r.join("");
                     if($('Extended'+rand+'ProfessionReport').value == ''){
                         check = false;
                     }
                }
            });
            if(check != true){
                alert('nevyplněno pole "Profese"');
            }
            return check;
        }else{
            return true;
        }
     }


    function makeCount(possible, current){
        return  $(current).value / $(possible).value *100;
    }



    function uniqid()
    {
        var newDate = new Date;
        return newDate.getTime();
    }


  	$('add-not-comes').addEvent('click',function(e){
        var rand_id1 = uniqid();
        var rand_id2 = rand_id1 + 1;
        var rand_id3 = rand_id1 + 2;
        var table = $('not-comes-table').getElement('tbody');
        var tr = new Element('tr').inject(table);
            tr.addClass('not_comes_row');
            new Element('td').set('html', '<input name="data[Extended]['+rand_id1+'][not_comes]" tabindex="1" value="" type="text" class="not_comes">').inject(tr);
            new Element('td').set('html', '<input name="data[Extended]['+rand_id2+'][not_comes]" tabindex="1" value="" type="text" class="not_comes">').inject(tr);
            new Element('td').set('html', '<input name="data[Extended]['+rand_id3+'][not_comes]" tabindex="1" value="" type="text" class="not_comes">').inject(tr);
            new Element('td').set('html', '<a class="ta trash" href="#" id="remove_'+rand_id1+'"> &nbsp </a>').inject(tr);
            $('remove_'+rand_id1).addEvent('click', function(e){e.stop; this.getParent('tr').dispose()});
    });

  	$('add-profession').addEvent('click',function(e){
  	    var rand_id1 = uniqid();
        var table = $('report_table_1').getElement('tbody');
        var tr = new Element('tr').inject(table);
            tr.addClass('not_comes_row');
            new Element('td').set('html', '<input id="Extended'+rand_id1+'Agency" name="data[Extended]['+rand_id1+'][agency]" tabindex="1" value="" type="text" class="not_comes">').inject(tr);
            new Element('td').set('html', '<input id="Extended'+rand_id1+'ProfessionReport" name="data[Extended]['+rand_id1+'][profession_report]" tabindex="1" value="" type="text" class="not_comes">').inject(tr);
            new Element('td').set('html', '<input id="Extended'+rand_id1+'EmployeCount" name="data[Extended]['+rand_id1+'][employe_count]" tabindex="1" value="" type="text" class="make_change employe_c">').inject(tr);
            new Element('td').set('html', '').inject(tr);
            new Element('td').set('html', '<input id="Extended'+rand_id1+'CurrentCount" name="data[Extended]['+rand_id1+'][current_count]" tabindex="1" value="" type="text" class="make_change current_c">').inject(tr);
            new Element('td').set('html', '<a class="ta trash" href="#" id="remove_'+rand_id1+'"> &nbsp </a>').inject(tr);
            $('remove_'+rand_id1).addEvent('click', function(e){e.stop; this.getParent('tr').dispose()});
           /* $('Extended'+rand_id1+'PossibleCount').addEvent("change", function(){
                    percentCount();
                    percentSum();
            });*/
            $('Extended'+rand_id1+'EmployeCount').addEvent("change", function(){
                   percentCount();
                   percentSum();
            });
            $('Extended'+rand_id1+'CurrentCount').addEvent("change", function(){
                    percentCount();
                    percentSum();
            });
    });
    $('add-meeting').addEvent('click',function(e){
        var rand_id1 = uniqid();
        var table = $('report_table_3').getElement('tbody');
        var tr = new Element('tr').inject(table);
            //tr.addClass();
            new Element('td').set('html', '<input id="Extended'+rand_id1+'CompanyMeetings" name="data[Extended]['+rand_id1+'][company_meetings]" tabindex="1" value="" type="text" class="not_comes">').inject(tr);
            new Element('td').set('html', '<input id="Extended'+rand_id1+'ReasonOfMeetings" name="data[Extended]['+rand_id1+'][reason_of_meeting]" tabindex="1" value="" type="text" class="not_comes">').inject(tr);
            new Element('td').set('html', '<input id="Extended'+rand_id1+'Solution" name="data[Extended]['+rand_id1+'][solution]" tabindex="1" value="" type="text" class="not_comes">').inject(tr);
            new Element('td').set('html', '<a class="ta trash" href="#" id="remove_'+rand_id1+'"> &nbsp </a>').inject(tr);
            $('remove_'+rand_id1).addEvent('click', function(e){e.stop; this.getParent('tr').dispose()});
    });

    $$('.delete-row').addEvent('click',function(e){
        e.stop;
        var tr =  this.getParent('tr');
        var ids = tr.getElements('input[type="hidden"]');
        var value = '' + $('DeleteExtends').value;
        Array.each(ids, function( el, i){
            value += el.value + '|';
        });
        $('DeleteExtends').set('value', value);
        tr.dispose();
       // alert($('DeleteExtends').value);

    });

    $('CompanyActivityCompanyActivityTypeId').addEvent('change',function(e){
            viewExtended();
    });

    var domtab = new DomTabs({'className':'admin_dom'});


	$('AddEditCompanyActivityClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_aktivity_add');});
	
	$('AddEditCompanyActivitySaveAndClose').addEvent('click',function(e){
		new Event(e).stop();

     //   if(cms_user_group == 1){
            valid_result2 = validateExtReport();
            valid_result = validation.valideForm('company_activity_edit_formular');
       /*  }else{
            valid_result = valid_result2 = true;
         }*/



		if (valid_result == true && valid_result2 == true){
            button_preloader($('AddEditCompanyActivitySaveAndClose'));
			new Request.HTML({
				url:$('company_activity_edit_formular').action,		
				update: $('domwin_aktivity').getElement('.CB_ImgContainer'),
				onComplete:function(){	    
					domwin.closeWindow('domwin_aktivity_add');
                    button_preloader_disable($('AddEditCompanyActivitySaveAndClose'));
				}
			}).post($('company_activity_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});


	$('add_task_from_activity').removeEvents('click').addEvent('click', function(e){
			new Event(e).stop();
			domwin.newWindow({
				id				: 'domwin_tasks_add',
				sizes			: [580,320],
				scrollbars		: true,
				title			: 'Přidání nového úkolu',
				languages		: false,
				type			: 'AJAX',
				ajax_url		: '/companies/tasks_edit/' + $('CompanyActivityCompanyId').value,
				closeConfirm	: true,
				max_minBtn		: false,
				modal_close		: false,
				remove_scroll	: false,
				self_data		: {from:'activity_edit'}
			}); 
		});

    function percentSum(){

        var els=  $$('.employe_c');
        var sum = 0;
        Array.each( els, function(el, i){
             sum += parseInt(el.value);
        });
        $('employe_count_sum').set('html', sum );

        var els=  $$('.current_c');
        var sum = 0;
        Array.each( els, function(el, i){
             sum += parseInt(el.value);
        });
        $('current_sum').set('html', sum );

       /* var els=  $$('.percent');
        var sum = 0;
        var count = 0;
        Array.each( els, function(el, i){
             sum += parseInt(el.get('html'));
             count ++;
        });
        $('percent_sum').set('html', parseInt(sum/count) );*/
    }

    function percentCount(){
         var els =  $$('.percent');
         var sum = 0;
         Array.each( els, function(el, i){
            var id = el.get('id');
            var r = id.split('-');
            rand = r[1];
            sum = $('Extended'+rand+'CurrentCount').value / $('Extended'+rand+'PossibleCount').value * 100;
            $('percent-'+rand).set('html', parseInt(sum) );
         });

    }



    $$('.make_change').addEvent('change', function(){
          //  percentCount();
           // percentSum();
    });

    window.addEvent('domready', function() {

        viewExtended();
        //percentSum();

    });
</script>