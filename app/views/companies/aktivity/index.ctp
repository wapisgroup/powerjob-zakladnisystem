<div class="win_fce top">
<a href='/companies/aktivity_edit/<?php echo $company_id;?>' class='button edit' id='company_contact_add_new' title='Přidání nové aktivity'>Přidat aktivitu</a>
</div>
<table class='table' id='table_kontakty'>
	<tr>
		<th>Firma</th>
		<th>Název</th>
		<th>Typ aktivity</th>
		<th>Kont. osoba</th>
		<th>Vytvořil</th>
		<th>Datum aktivity</th>
		<th>Vytvořeno</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($contact_list) && count($contact_list) >0):?>
	<?php foreach($contact_list as $contact_item):?>
	<tr>
		<td><?php echo $contact_item['Company']['name'];?></td>
		<td title='<?php echo $contact_item['CompanyActivity']['name'];?>'><?php echo $fastest->orez($contact_item['CompanyActivity']['name'],30);?></td>
		
		<td><?php echo $contact_item['SettingActivityType']['name'];?></td>
		<td title="<?php echo $contact_item['CompanyContact']['name'];?> Tel:<?php echo $contact_item['CompanyContact']['telefon1'];?>"><?php echo $contact_item['CompanyContact']['name'];?></td>
		<td><?php echo $contact_item['CmsUser']['name'];?></td>
		<td><?php echo $fastest->czechDate($contact_item['CompanyActivity']['activity_datetime']);?></td>
		<td><?php echo $fastest->czechDate($contact_item['CompanyActivity']['created']);?></td>
		<td>
			<a title='Editace aktivity "<?php echo $contact_item['CompanyActivity']['name'];?>"' 	class='ta edit' href='/companies/aktivity_edit/<?php echo $company_id;?>/<?php echo $contact_item['CompanyActivity']['id'];?>'/>edit</a>
			<?php if(isset($permission['delete_activity']) && $permission['delete_activity'] == 1){?>
            <a title='Odstranit položku'class='ta trash' href='/companies/aktivity_trash/<?php echo $company_id;?>/<?php echo $contact_item['CompanyActivity']['id'];?>'/>trash</a>
		    <?php } ?>
        </td>
	</tr>
	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='7'>K této společnosti nebyla nadefinována aktivita</td>
	</tr>
	<?php endif;?>
</table>
<?php // pr($contact_list);?>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='ListCompanyActivityClose' class='button'/>
	</div>
<script>
	$('ListCompanyActivityClose').addEvent('click', function(e){new Event(e).stop(); domwin.closeWindow('domwin_aktivity');});

	$('table_kontakty').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tuto aktivitu?')){
			new Request.HTML({
				url:this.href,
				update: $('domwin_aktivity').getElement('.CB_ImgContainer'),
				onComplete: function(){}
			}).send();
		}
	});
	
	$('domwin_aktivity').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_aktivity_add',
			sizes		: [580,650],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>