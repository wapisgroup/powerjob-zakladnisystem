<form action='/int_clients/employ_on_project/' method='post' id='setting_career_edit_formular'>
	<?php echo $htmlExt->hidden('ConnectionClientAtCompanyWorkPosition/id');?>
	<?php echo $htmlExt->hidden('ConnectionClientAtCompanyWorkPosition/client_id');?>
    <?php echo $htmlExt->hidden('Client/email');?>
    <?php echo $htmlExt->hidden('generate_login',array('value'=>0));?>
    <?php echo $htmlExt->hidden('group_id');?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Zaměstnání</legend>
                <?php echo $htmlExt->selectTag('ConnectionClientAtCompanyWorkPosition/at_project_id',$project_list,null,array('label'=>'Project','label_class'=>'long'));?><br />
                <?php echo $htmlExt->selectTag('ConnectionClientAtCompanyWorkPosition/at_company_id',array(0=>'Vyberte projekt'),null,array('label'=>'Firma','label_class'=>'long'),null,false);?><br />
			    <?php echo $htmlExt->selectTag('ConnectionClientAtCompanyWorkPosition/at_project_centre_id',array(0=>'Vyberte firmu'),null,array('label'=>'Sředisko','label_class'=>'long'),null,false);?><br />
			    <?php echo $htmlExt->selectTag('ConnectionClientAtCompanyWorkPosition/at_project_cinnost_id',array(0=>'Vyberte středisko'),null,array('label'=>'Činnost','label_class'=>'long'),null,false);?><br />
            	<?php echo $htmlExt->selectTag('ConnectionClientAtCompanyWorkPosition/at_project_centre_work_position_id',array(0=>'Vyberte firmu'),null,array('label'=>'Profese','label_class'=>'long'),null,false);?><br />
				<?php echo $htmlExt->selectTag('ConnectionClientAtCompanyWorkPosition/at_company_money_item_id',$money_item_list,null,array('label'=>'Forma zaměstnání','label_class'=>'long'));?><br />
				
				<?php echo $htmlExt->inputDate('ConnectionClientAtCompanyWorkPosition/datum_nastupu',array('tabindex'=>1,'label'=>'Datum nástupu', 'class'=>'','label_class'=>'long'));?> <br class="clear"/>
			    <?php echo $htmlExt->selectTag('ConnectionClientAtCompanyWorkPosition/typ_smlouvy',array(1=>'Smlouva na dobu neurčitou',2=>'Smlouva na dobu určitou'),null,array('label'=>'Typ smlouvy','label_class'=>'long'),null,false);?><br />
                <div id="expirace" class="none">
                    <?php echo $htmlExt->inputDate('ConnectionClientAtCompanyWorkPosition/expirace_smlouvy',array('tabindex'=>1,'label'=>'Expirace smlouvy', 'class'=>'','label_class'=>'long'));?> <br class="clear"/>
                </div>
            </fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Zaměstnat',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'});
    var exist_email = false;
    $('ConnectionClientAtCompanyWorkPositionAtProjectId').ajaxLoad('/int_clients/ajax_load_at_company_list/', ['ConnectionClientAtCompanyWorkPositionAtCompanyId']); 
    $('ConnectionClientAtCompanyWorkPositionAtProjectId').ajaxLoad('/int_clients/ajax_load_at_company_centre_list/', ['ConnectionClientAtCompanyWorkPositionAtProjectCentreId']);
    $('ConnectionClientAtCompanyWorkPositionAtProjectCentreId').ajaxLoad('/int_clients/ajax_load_at_company_work_position_list/', ['ConnectionClientAtCompanyWorkPositionAtProjectCentreWorkPositionId']);
    $('ConnectionClientAtCompanyWorkPositionAtProjectCentreId').ajaxLoad('/int_clients/ajax_load_at_company_cinnost_list/', ['ConnectionClientAtCompanyWorkPositionAtProjectCinnostId']);

    $('ConnectionClientAtCompanyWorkPositionAtProjectCentreWorkPositionId').addEvent('change',function(e){
        e.stop();
        new Request.JSON({
			url:'/int_clients/need_genereate_login/'+this.value,		
			onComplete:function(json){
				if(json){
				    if(json.result == true){
				        $('GroupId').value = json.group_id;
				        $('GenerateLogin').value = json.need;
				    }
                    else{
                        alert(json.message);
                    }
				}
                else
                    alert('Chyba aplikace, obnovte akci');
			}
	    }).send();
    })    
   
    $('ConnectionClientAtCompanyWorkPositionTypSmlouvy').addEvent('click',function(e){
        e.stop();
        if(this.value == 2){
            $('expirace').removeClass('none');
        }
        else{
            $('expirace').addClass('none');
        }
    })    
    
    function validate_mail(email){
        new Request.JSON({
			url:'/int_clients/check_mail_in_int_zam/'+email,		
			onComplete:function(json){
				if(json){
				    if(json.result == true){
                        exist_email = 'Tento email už má interní zaměstnanec, jedná se o zaměstnance: '+json.zam+"\nEmaily musí být unikátní pro int. zaměstance.";
				    }
				}
			}
	    }).send();
    } 
    
    if($('ClientEmail').value != ''){
       validate_mail($('ClientEmail').value);
    }
    
   
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();

		valid_result = validation.valideForm('setting_career_edit_formular');
		if (valid_result == true){
            if($('GenerateLogin').value == 0 || ($('GenerateLogin').value == 1 && $('ClientEmail').value != '')){
                if($('GenerateLogin').value == 0 || ($('GenerateLogin').value == 1 && exist_email == false)){
        			new Request.JSON({
        				url:$('setting_career_edit_formular').action,		
        				onComplete:function(){
        					domwin.closeWindow('domwin');
                            window.location = '/int_clients/';
        				}
        			}).post($('setting_career_edit_formular'));
                }
                else{ alert(exist_email); }
            }
            else{
                alert("Profesi kterou jste zvolili, vyžaduje vygenerování přístupu do systému.\nEmail tedy musí být u klienta vyplněn - slouží jako přístupové údaje!!!");
            }
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('setting_career_edit_formular',{
		'ConnectionClientAtCompanyWorkPositionAtCompanyId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit firmu'}
		},
        'ConnectionClientAtCompanyWorkPositionAtProjectCentreWorkPositionId':{
            'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit profesi'}
        },
        'calbut_ConnectionClientAtCompanyWorkPositionDatumNastupu': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit datum nástupu'}
		}
	});
	validation.generate('setting_career_edit_formular',<?php echo (isset($this->data['AtProject']['id']))?'true':'false';?>);
</script>