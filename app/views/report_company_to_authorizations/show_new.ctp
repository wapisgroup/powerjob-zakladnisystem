<div>
<?php echo $htmlExt->hidden('connection_company_to_authorization_id',array('value' => $id));?>
</div>	
<form action='' method='post' id='setting_activity_edit_formular'>
		
	<div class="domtabs admin2_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Seznam Zaměstnanců</a></li>
		</ul>
</div>
	<div class="domtabs admin2_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Zaměstnanci</legend>
<div id="obal_tabulky">
				<table class='table' id='rating_table'>
					<tr>
						<th>Zaměstnanec</th>
						<th>Profese</th>
						<th>Počet hodin</th>
                        <th>Základní hrubá mzda</th>
                        <th>Bonusy</th>
						<th>Srážky</th>
						<th>Příplatky</th>
						<th>Celk. výplata</th>
						<th>Možnosti</th>
					</tr>
					<?php 
					if (isset($zamestnanci_list) && count($zamestnanci_list) > 0){
						foreach($zamestnanci_list as $zam):
						?>
						<tr id="<?php echo $zam['NewClientWorkingHour']['id'];?>" class="<?php if( $zam['NewClientWorkingHour']['director_read'] == 1){ echo "dir_read";}?>">
							<td class='td_client'><?php echo $zam['Client']['name'];?></td>
							<td><?php echo $zam['CompanyWorkPosition']['name'];?></td>
							<td><?php echo $zam['NewClientWorkingHour']['celkem_hodin'];?></td>
                            <td><?php echo $zam['NewClientWorkingHour']['salary_start'];?></td>
							<td><?php echo $zam['NewClientWorkingHour']['sum_bonus'];?></td>
                            <td><?php echo $zam['NewClientWorkingHour']['sum_drawback'];?></td>
                            <td><?php echo $zam['NewClientWorkingHour']['sum_extra_pay'];?></td>
							<td><?php echo $zam['NewClientWorkingHour']['salary_per_hour_p'];?></td>
							
							<td><?php if($zam['NewClientWorkingHour']['stav']==2){?>
							<a href="/report_company_to_authorizations/repair/<?php echo $id.'/'.$zam['NewClientWorkingHour']['id'];?>/1" class="repair">K úpravě</a> |
							<?php } ?>
							<a href="/new_employees/odpracovane_hodiny/<?php echo $zam['NewClientWorkingHour']['connection_client_requirement_id'].'/'.$zam['NewClientWorkingHour']['year'].'/'.($zam['NewClientWorkingHour']['month']<10 ? '0'.$zam['NewClientWorkingHour']['month'] : $zam['NewClientWorkingHour']['month']); ?>" class="detail" title="Editovat položku">Detail</a>
							
							</td>	
						</tr>
					<?php 
						endforeach;
					}
					else echo "<tr id='NoneSearch'><td></td>Žádní zaměstnanci</td></tr>"; 
					?>
					</table></div> 
				<br />
				<p class='pp15'>
				<strong>Forma odměny:</strong><br />
					1xxx - pracovní smlouva <br />
					x1xx - dohoda <br />
					xx1x - faktura <br />
					xxx1 - abc <br />
				</p>
			</fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin2_dom'}); 

	$$('.repair').addEvent('click', repair_client.bindWithEvent(this));

	$$('.detail').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_zamestnanci_hodiny',
			sizes		: [950,900],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
            remove_scroll : false
		}); 
	});
	
  
	function repair_client(e){
	    var event = new Event(e);
	    event.stop();
		var obj = event.target;
	    var tr=obj.getParent('tr');
			
			domwin.newWindow({
				id			: 'domwin_repair',
				sizes		: [580,270],
				scrollbars	: true,
				title		: 'K opravě klienta ' + tr.getElement('.td_client').getHTML(),
				languages	: false,
				type		: 'AJAX',
				//ajax_url	: '/report_company_to_authorizations/repair/<?php echo $id.'/'.$zam['NewClientWorkingHour']['id'];?>',
				ajax_url	: tr.getElement('.repair').href,
				closeConfirm: true,
				max_minBtn	: false,
				modal_close	: false,
				remove_scroll: false
			}); 
	};

	function repair(e){
	//var event = new Event(e);
	//event.stop();
		obj=$('rating_table').getElement('.repair');
	    tr = obj.getParent('tr'), table = tr.getParent('table');
		obj.getParent('tr').getElement('.repair').addClass('none');	
		click_refresh($('ConnectionCompanyToAuthorizationId').value);
	}
    
    /**
     * funkce pro zmenu class precteno/neprecteno zmeni class a posila request
     * kdy se v NewClientWorkingHour meni sloupec director_read 
     * 1 - precteno, 0 - neprecteno
     */
	$('rating_table').getElements('tr').addEvent('click',function(e){
	   e.stop();
       var cw_id = this.id;
       var stav = 0;
       
       //byl precten odstran precteni a stav zustava 0 pro request
       if(this.hasClass('dir_read'))
          this.removeClass('dir_read');
       else{
       //jinak pridej precteni a stav zmen na 1 pro request   
          this.addClass('dir_read');
          stav = 1;
       }   
    
       //odeslani requestu pro zmenu stavu do db   
       new Request.JSON({
			url:'/report_company_to_authorizations/change_status_read/'+cw_id+'/'+stav+'/1',
			onComplete:function(json){
				if(json.result == false)
                    alert('Chyba při změně stavu, prosím zkuste později znova.');
			}
	   }).send();
       
	});


	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_show');});
</script>