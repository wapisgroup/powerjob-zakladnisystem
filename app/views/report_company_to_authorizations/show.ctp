<div class="noprint">
<?php echo $htmlExt->hidden('connection_company_to_authorization_id',array('value' => $id));?>
</div>	
<form action='' method='post' id='setting_activity_edit_formular'>
		
	<div class="domtabs admin2_dom_links noprint">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Seznam Zaměstnanců</a></li>
		</ul>
</div>
	<div class="domtabs admin2_dom">
		<div class="domtabs field">
        <?php if($print == false){?>
			<fieldset>
				<legend>Zaměstnanci</legend>
        <?php }?>        
                <div>
                    <?php
                        if($not_exist_cw != false){
                            echo '<strong>Upozornění, v docházkách existují klienti kteří 
                            mají stále otevřenou kartu(nebyla uložena)!</strong><br />
                            Jedná se o tyto klienty:<br />
                            ';
                            foreach($not_exist_cw as $client){
                                echo '#'.$client['Client']['id'].' - '.$client['Client']['name'].'</br >';
                            }
                        }
                    ?>
                </div>
<div id="obal_tabulky">
				<table class='table' id='rating_table'>
					<tr>
						<th>Zaměstnanec</th>
                        <th>Číslo účtu</th>
						<th>Profese</th>
						<th>Forma odměny</th>
						<th>Počet hodin</th>
						<th>Na hodinu</th>
						<th>PS-D-ZL-C</th>
						<th>Max celkem</th>
						<th>Srážka</th>
                        <th>Srážka OPP</th>
                        <th>Zálohy,VVH</th>
						<th>Odměna1</th>
						<th>Odměna2</th>
						<th>Celk. výplata</th>
                        <th>Zbývá k výplatě</th>
						<th class="noprint">Možnosti</th>
					</tr>
					<?php 
					if (isset($zamestnanci_list) && count($zamestnanci_list) > 0){
						foreach($zamestnanci_list as $zam):
						?>
						<tr id="<?php echo $zam['ClientWorkingHour']['id'];?>" class="<?php if( $zam['ClientWorkingHour']['director_read'] == 1){ echo "dir_read";}?>">
							<td class='td_client'><?php echo $zam['Client']['name'];?></td>
                            <td><?php echo $zam['Client']['cislo_uctu'];?></td>
							<td><?php echo $zam['CompanyWorkPosition']['name'];?></td>
							<td><?php echo $zam['CompanyMoneyItem']['name'];?></td>
							<td><?php echo $zam['ClientWorkingHour']['celkem_hodin'];?></td>
							<td><?php echo $zam['ClientWorkingHour']['salary_per_hour'];?></td>
							<td>
								<?php 
								if($zam['CompanyMoneyItem']['name'] == '1001' &&  $zam['ClientWorkingHour']['salary_part_1']==0){
									if($zam['Company']['stat_id']==1) 
										echo '67';
									else	
										echo '2.35';
								}
								else
									echo $zam['ClientWorkingHour']['salary_part_1'];
								?>-
								<?php echo $zam['ClientWorkingHour']['salary_part_2'];?>-
								<?php echo $zam['ClientWorkingHour']['salary_part_3'];?>-
								<?php echo $zam['ClientWorkingHour']['salary_part_4'];?>
							</td>
							<td><?php echo ($zam['ClientWorkingHour']['salary_part_1_max']+$zam['ClientWorkingHour']['salary_part_2_max']+$zam['ClientWorkingHour']['salary_part_3_max']+$zam['ClientWorkingHour']['salary_part_4_max'])?></td>
							<td><?php echo $zam['ClientWorkingHour']['drawback'];?></td>
                            <td><?php echo ($zam['ClientWorkingHour']['drawback_opp_add'] == 1?$zam['ClientWorkingHour']['drawback_opp']:0);?></td>
							<td><?php echo $zam['0']['zalohy'];?></td>
                            <td><?php echo $zam['ClientWorkingHour']['odmena_1'];?></td>
							<td><?php echo $zam['ClientWorkingHour']['odmena_2'];?></td>
							<td><?php echo $zam['ClientWorkingHour']['salary_per_hour_p'];?></td>
							<td><?php echo ($zam['ClientWorkingHour']['salary_per_hour_p']-($zam['0']['zalohy'] > 0 ? $zam['0']['zalohy'] : 0));?></td>
							
							<td class="noprint"><?php if($zam['ClientWorkingHour']['stav']==2){?>
							<a href="/report_company_to_authorizations/repair/<?php echo $id.'/'.$zam['ClientWorkingHour']['id'];?>" class="repair">K úpravě</a> |
							<?php } ?>
							<a href="/employees/odpracovane_hodiny/<?php echo $zam['ClientWorkingHour']['connection_client_requirement_id'].'/'.$zam['ClientWorkingHour']['year'].'/'.($zam['ClientWorkingHour']['month']<10 ? '0'.$zam['ClientWorkingHour']['month'] : $zam['ClientWorkingHour']['month']); ?>" class="detail" title="Editovat položku">Detail</a>
							
							</td>	
						</tr>
					<?php 
						endforeach;
					}
					else echo "<tr id='NoneSearch'><td></td>Žádní zaměstnanci</td></tr>"; 
					?>
					</table></div> 
				<br />
				<p class='pp15'>
				<strong>Forma odměny:</strong><br />
					1xxx - pracovní smlouva <br />
					x1xx - dohoda <br />
					xx1x - faktura <br />
					xxx1 - abc <br />
				</p>
            <?php if($print == false){?>    
			</fieldset>
             <?php }?>  
		</div>
	</div>
	<div class="win_save noprint">
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
        <?php echo $html->link('Tisk','/report_company_to_authorizations/show/'.$id.'/1',array('id'=>'print_button','class'=>'mr5 button','target'=>'_blank'));?>
   
	</div>
</form>
 
<script language="javascript" type="text/javascript">
var pprint = "<?= $print;?>";

if(pprint == 1) {
    window.print();
} else {
    
	var domtab = new DomTabs({'className':'admin2_dom'}); 

    if($('rating_table').getElements('a.repair'))
	   $$('.repair').addEvent('click', function(e){repair_client(e)});

    if($$('.detail'))
    	$$('.detail').addEvent('click',function(e){
    		new Event(e).stop();
    		domwin.newWindow({
    			id			: 'domwin_zamestnanci_hodiny',
    			sizes		: [950,900],
    			scrollbars	: true,
    			title		: this.title,
    			languages	: false,
    			type		: 'AJAX',
    			ajax_url	: this.href,
    			closeConfirm: true,
    			max_minBtn	: false,
    			modal_close	: false,
                remove_scroll : false
    		}); 
    	});
	
 
	function repair_client(e){
	    var event = new Event(e);
	    event.stop();
		var obj = event.target;
	    var tr=obj.getParent('tr');
			
			domwin.newWindow({
				id			: 'domwin_repair',
				sizes		: [580,270],
				scrollbars	: true,
				title		: 'K opravě klienta ' + tr.getElement('.td_client').getHTML(),
				languages	: false,
				type		: 'AJAX',
				//ajax_url	: '/report_company_to_authorizations/repair/<?php echo $id.'/'.$zam['ClientWorkingHour']['id'];?>',
				ajax_url	: tr.getElement('.repair').href,
				closeConfirm: true,
				max_minBtn	: false,
				modal_close	: false,
				remove_scroll: false
			}); 
	};

	function repair(e){
	//var event = new Event(e);
	//event.stop();
		obj=$('rating_table').getElement('.repair');
	    tr = obj.getParent('tr'), table = tr.getParent('table');
		obj.getParent('tr').getElement('.repair').addClass('none');	
		click_refresh($('ConnectionCompanyToAuthorizationId').value);
	}
    
    /**
     * funkce pro zmenu class precteno/neprecteno zmeni class a posila request
     * kdy se v ClientWorkingHour meni sloupec director_read 
     * 1 - precteno, 0 - neprecteno
     */
	$('rating_table').getElements('tr').addEvent('click',function(e){
	   e.stop();
       var cw_id = this.id;
       var stav = 0;
       
       //byl precten odstran precteni a stav zustava 0 pro request
       if(this.hasClass('dir_read'))
          this.removeClass('dir_read');
       else{
       //jinak pridej precteni a stav zmen na 1 pro request   
          this.addClass('dir_read');
          stav = 1;
       }   
    
       //odeslani requestu pro zmenu stavu do db   
       new Request.JSON({
			url:'/report_company_to_authorizations/change_status_read/'+cw_id+'/'+stav,
			onComplete:function(json){
				if(json.result == false)
                    alert('Chyba při změně stavu, prosím zkuste později znova.');
			}
	   }).send();
       
	});


	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_show');});
}

</script>