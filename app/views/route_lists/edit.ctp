 <form action='/route_lists/edit/' method='post' id='RouteList_edit_formular'>
	<?php echo $htmlExt->hidden('RouteList/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Základní údaje</legend>
				<?php echo $htmlExt->input('RouteList/name',array('tabindex'=>1,'label'=>'Název','class'=>'long','label_class'=>'long'));?> <br class="clear">
				
			</fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','class'=>'button'));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','class'=>'button'));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'});  
		
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('RouteList_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('RouteList_edit_formular').action,		
				onComplete:function(){
					click_refresh($('RouteListId').value);
					domwin.closeWindow('domwin');
				}
			}).post($('RouteList_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('RouteList_edit_formular',{
		'RouteListName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
		}
	});
	validation.generate('RouteList_edit_formular',<?php echo (isset($this->data['RouteList']['id']))?'true':'false';?>);
</script>