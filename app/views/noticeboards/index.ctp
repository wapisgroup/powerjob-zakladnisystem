<h1>Statistiky</h1>
<p style='padding:20px'><strong>Počty firem po SM:</strong></p>
<table class='table'>
	<tr>
		<th>SM</th>
		<th>Počet firem</th>
		<th>Počet firem se smlouvou</th>
		<th>Počet firem se zaměstnanci</th>
		<th>Počet kontaktu</th>
	</tr>
	<?php foreach($pocty_firem_pro_sm as $item):?>
	<tr>
		<td><?php echo $item['CmsUser']['name'];?></td>
		<td><?php echo $item[0]['pocet_firem'];?></td>
		<td><?php echo $item[0]['pocet_firem_se_smlouvou'];?></td>
		<td><?php echo $item[0]['pocet_firem_se_smlouvou_a_zamestnanci'];?></td>
		<td><?php echo $item[0]['pocet_kontaktu'];?></td>
	</tr>
	<?php endforeach;?>
</table>

<p style='padding:20px'><strong>Počty aktivit pro SM, CM, KOO:</strong></p>
<table class='table'>
	<tr>
		<th>Jméno uživatele</th>
		<th>Role</th>
		<th>Počet aktivit</th>
	</tr>
	<?php foreach($pocty_aktivit_pro_users as $item):?>
	<tr>	
		<td><?php echo $item['CmsUser']['name'];?></td>
		<td><?php echo $item['CmsGroup']['name'];?></td>
		<td><?php echo $item[0]['pocet_aktivit'];?></td>
	</tr>
	<?php endforeach;?>	
</table>
</p>