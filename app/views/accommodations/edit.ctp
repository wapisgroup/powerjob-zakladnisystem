 <form action='/accommodations/edit/' method='post' id='Accommodation_edit_formular'>
	<?php echo $htmlExt->hidden('Accommodation/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
			<?php if (isset($this->data['Accommodation']['id'])){ ?>
			<li class="ousko"><a href="#krok2">Přístupy</a></li>
			<?php } ?>
			<li class="ousko"><a href="#krok3">Fotografie</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Zadejte údaje</legend>
				<?php echo $htmlExt->input('Accommodation/name',array('tabindex'=>1,'label'=>'Název','class'=>'long','label_class'=>'long'));?> <br class="clear">
				<div class="sll">  
					<?php echo $htmlExt->input('Accommodation/city',array('tabindex'=>2,'label'=>'Město'));?> <br class="clear">
					<?php echo $htmlExt->input('Accommodation/price',array('tabindex'=>4,'label'=>$price_for_list[$this->data['Accommodation']['type']]));?> <br class="clear">
				</div>
				<div class='slr'>
					<?php echo $htmlExt->selectTag('Accommodation/type',$accommodationtype_list,null,array('tabindex'=>3,'label'=>'Typ'),array('title'=>$accommodationtype_title_list),true);?> <br class="clear">
					<?php echo $htmlExt->input('Accommodation/account',array('tabindex'=>5,'label'=>'Účet'));?> <br class="clear">
				</div>
			</fieldset>
			<fieldset>
				<legend>Finanční údaje</legend>
				<div class="sll">  
					<?php echo $htmlExt->input('Accommodation/day_of_payment',array('tabindex'=>6,'label'=>'Den splatnosti'));?> <br class="clear">
				</div>	
				<div class="slr">  
					<?php echo $htmlExt->input('Accommodation/max_count',array('tabindex'=>7,'label'=>'Max. počet'));?> <br class="clear">
				</div>
			</fieldset>

		</div>
		<?php if (isset($this->data['Accommodation']['id'])){ ?>	
			<div class="domtabs field">
				<?php echo $this->renderElement('../accommodations/access/index');?>
			</div>
		<?php }?>
		
		<div class="domtabs field">
			<?php echo $this->renderElement('../accommodations/fotogalerie/fotografie');?>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','class'=>'button'));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','class'=>'button'));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var cena_type = JSON.decode('<?php echo json_encode($price_for_list);?>');
	var domtab = new DomTabs({'className':'admin_dom'});

	$('AccommodationType').addEvent('change', function(){
		type = this.options[this.selectedIndex].title;
		label = $('AccommodationPrice').getPrevious('label');
		label.setHTML(cena_type[type]);
	});
		
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('Accommodation_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('Accommodation_edit_formular').action,		
				onComplete:function(){
					click_refresh($('AccommodationId').value);
					domwin.closeWindow('domwin');
				}
			}).post($('Accommodation_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('Accommodation_edit_formular',{
		'AccommodationName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit jméno'},
		}
	});
	validation.generate('Accommodation_edit_formular',<?php echo (isset($this->data['Accommodation']['id']))?'true':'false';?>);
</script>