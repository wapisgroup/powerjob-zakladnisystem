<div class="win_fce top">
<a href='/accommodations/kontakty_edit/<?php echo $Accommodation_id;?>' class='button' id='Accommodation_contact_add_new'>Přidat kontakt</a>
</div>
<table class='table' id='table_kontakty'>
	<tr>
		<th>#ID</th>
		<th>Název</th>
		<th>Email</th>
		<th>Telefon</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($contact_list) && count($contact_list) >0):?>
	<?php foreach($contact_list as $contact_item):?>
	<tr>
		<td><?php echo $contact_item['AccommodationContact']['id'];?></td>
		<td><?php echo $contact_item['AccommodationContact']['name'];?></td>
		<td><?php echo $contact_item['AccommodationContact']['email'];?></td>
		<td><?php echo $contact_item['AccommodationContact']['telefon1'];?></td>
		<td>
			<a title='Editace položy' 	class='ta edit' href='/accommodations/kontakty_edit/<?php echo $Accommodation_id;?>/<?php echo $contact_item['AccommodationContact']['id'];?>'/>edit</a>
			<a title='Do kosiku' 		class='ta trash' href='/accommodations/kontakty_trash/<?php echo $Accommodation_id;?>/<?php echo $contact_item['AccommodationContact']['id'];?>'/>trash</a>
		</td>
	</tr>
	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='5'>K této společnosti nebyl nadefinován kontakt</td>
	</tr>
	<?php endif;?>
</table>
<?php // pr($contact_list);?>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='ListAccommodationContactClose' class='button'/>
	</div>
<script>
	$('ListAccommodationContactClose').addEvent('click', function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin_kontakty');
	});

	$('Accommodation_contact_add_new').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_kontakty_add',
			sizes		: [580,320],
			scrollbars	: true,
			title		: 'Přidání nového kontaktu',
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
	
	$('table_kontakty').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tento kontakt?')){
			new Request.HTML({
				url:this.href,
				update: $('domwin_kontakty').getElement('.CB_ImgContainer'),
				onComplete: function(){}
			}).send();
		}
	});
	
	$('table_kontakty').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_kontakty_add',
			sizes		: [580,320],
			scrollbars	: true,
			title		: 'Editace kontaktu',
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>