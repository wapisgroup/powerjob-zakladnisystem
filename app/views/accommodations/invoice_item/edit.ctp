<form action='/accommodations/invoice_item_edit/' method='post' id='Accommodation_invoice_edit_formular'>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Fakturace</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<?php echo $htmlExt->hidden('AccommodationInvoiceItem/id');?>
			<?php echo $htmlExt->hidden('AccommodationInvoiceItem/accommodation_id');?>
			<fieldset>
				<legend>Prosím vyplňte následující údaje</legend>
				<div class='sll'>
					<?php echo $htmlExt->input('AccommodationInvoiceItem/za_mesic',array('tabindex'=>5,'label'=>'Za měsíc'));?> <br class="clear">
					<?php echo $htmlExt->inputDate('AccommodationInvoiceItem/datum_uhrady',array('tabindex'=>6,'label'=>'Datum úhrady'));?> <br class="clear">				
					<?php echo $htmlExt->inputDate('AccommodationInvoiceItem/datum_splatnosti',array('tabindex'=>7,'label'=>'Datum splatnosti'));?> <br class="clear">
					<?php echo $htmlExt->inputDate('AccommodationInvoiceItem/datum_vystaveni',array('tabindex'=>8,'label'=>'Datum vystavení'));?> <br class="clear">
				</div>
				<div class='slr'>
					<?php echo $htmlExt->input('AccommodationInvoiceItem/cena_bez_dph',array('tabindex'=>9,'label'=>'Částka bez DPH'));?> <br class="clear">
					<?php echo $htmlExt->input('AccommodationInvoiceItem/dph',array('tabindex'=>10,'label'=>'Dph'));?> <br class="clear">
					<?php echo $htmlExt->input('AccommodationInvoiceItem/spolu_s_dph',array('tabindex'=>11,'label'=>'Částka s DPH'));?> <br class="clear">
					<?php echo $htmlExt->input('AccommodationInvoiceItem/cena',array('tabindex'=>12,'label'=>'Částka systém'));?> <br class="clear">
				</div>
				<?php echo $htmlExt->textarea('AccommodationInvoiceItem/comment',array('tabindex'=>13,'label'=>'Komentář','class'=>'long','label_class'=>'long','style'=>'height:30px'));?><br class='clear'/>
			</fieldset>
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditAccommodationInvoiceItemSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditAccommodationInvoiceItemClose'/>
	</div>
</form>
<script>
	$('AccommodationInvoiceItemAccommodationId').addEvent('change',function(e){
		new Event(e).stop();
		new Request.JSON({
			url: '/report_invoice_items/load_Accommodation_information/' + this.value,
			onComplete: function(json){
				$('AccommodationCm').value  = json.ClientManager.name;
				$('AccommodationSm').value = json.SalesManager.name;
				$('AccommodationIco').value = json.Accommodation.ico;
				
			}
		}).send();
	});
	new MonthCal($('AccommodationInvoiceItemZaMesic'));
	
	var domtab = new DomTabs({'className':'admin_dom'}); 
	
	$('AddEditAccommodationInvoiceItemClose').addEvent('click',function(e){
		new Event(e).stop(); 
		<?php if (isset($from_report)):?>
		domwin.closeWindow('domwin');
		<?php else:?>
		domwin.closeWindow('domwin_invoice_item_add');
		<?php endif;?>
	});
	
	$('AddEditAccommodationInvoiceItemSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('Accommodation_invoice_edit_formular');
		<?php if (isset($from_report)):?>
			var request = new Request.JSON({url:$('Accommodation_invoice_edit_formular').action, onComplete:function(){click_refresh($('AccommodationInvoiceItemId').value);domwin.closeWindow('domwin');}})			
		<?php else:?>
			var request = new Request.HTML({url:$('Accommodation_invoice_edit_formular').action, update: $('domwin_invoice_item').getElement('.CB_ImgContainer'), onComplete:function(){domwin.closeWindow('domwin_invoice_item_add');}})			
		<?php endif;?>
		if (valid_result == true){
			request.post($('Accommodation_invoice_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('Accommodation_invoice_edit_formular',{
		'AccommodationInvoiceItemName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
		}
	});
	validation.generate('Accommodation_invoice_edit_formular',<?php echo (isset($this->data['AccommodationInvoiceItem']['id']))?'true':'false';?>);
</script>