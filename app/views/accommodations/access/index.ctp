<div class="win_fce top">
<a href='/accommodations/access_add/<?php echo $this->data['Accommodation']['id'];?>' class='button edit' id='recruiter_add_new' title='Přidání'>Přidat uživatele</a>
</div>
<table class='table' id='recruiter_table'>
	<tr>
		<th>Jméno</th>
		<th>Přiřazen</th>
		<th></th>
	</tr>
	<?php if (isset($users_list) && count($users_list) > 0):?>
		<?php foreach($users_list as $user):?>
		<tr>
			<td><?php echo ($user['ConnectionAccommodationUser']['cms_user_id'] != -2 ? $user['CmsUser']['name'] : 'Všichni');?></td>
			<td><?php echo $fastest->czechDate($user['ConnectionAccommodationUser']['created']);?></td>
			<td><a href="#" rel="<?php echo $user['ConnectionAccommodationUser']['id'];?>" class="ta trash">Odebrat</a></td>
		</tr>
		<?php endforeach;?>
	<?php endif;?>
</table> 
<script>	
	$('recruiter_table').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat toto spojení?')){
			new Request.HTML({
				url:'/accommodations/access_trash/'+ this.rel,
				onComplete: (function(){
					this.getParent('tr').dispose();
				}).bind(this)
			}).send();
		}
	});
	
	
	$('recruiter_add_new').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_recruiter_add',
			sizes		: [580,390],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			post_data	: {from:'editace'},
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>