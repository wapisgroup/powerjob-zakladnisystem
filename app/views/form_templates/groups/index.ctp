<br/>
<a href='/form_templates/group_edit/' class='button add_new_group' title='Přidat novou skupinu'>Přidat novou skupinu</a>
<br /><br />
<table class='table' id='groups_table'>
	<tr>
		<th>ID</th>
		<th>Název</th>
		<th>Vytvořeno</th>
		<th>Možnosti</th>
	</tr>
<?php if (isset($group_list) && count($group_list)>0): ?>
	<?php foreach($group_list as $group):?>
	<tr>
		<td><?php echo $group['FormTemplateGroup']['id'];?></td>
		<td><?php echo $group['FormTemplateGroup']['name'];?></td>
		<td><?php echo $fastest->czechDateTime($group['FormTemplateGroup']['created']);?></td>
		<td>
			<?php if ($group['FormTemplateGroup']['stav'] == 0):?>
			<a href='/form_templates/group_edit/<?php echo $group['FormTemplateGroup']['id'];?>' class='ta edit' title='Editace skupiny: <?php echo $group['FormTemplateGroup']['name'];?>'>Edit</a>
			<?php endif;?>
			<!-- a href='/form_templates/groups/delete/<?php echo $group['FormTemplateGroup']['id'];?>' class='ta trash' title='Smazání kmapaně: <?php echo $group['FormTemplateGroup']['name'];?>'>Trash</a-->
		</td>
	</tr>
	<?php endforeach;?>
<?php else: ?>
	<tr>
		<td colspan='3'>Nenalezeno</td>
	</tr>
<?php endif; ?>
</table>
<div class="win_save">
	<?php echo $htmlExt->button('Zavřít',array('id'=>'DomwinGroupsClose','class'=>'button','tabindex'=>2));?>
</div>
<script>
$('DomwinGroupsClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_groups');});
$('domwin_groups').getElement('.add_new_group').addEvent('click', function(e){
	new Event(e).stop();
	domwin.newWindow({
		id			: 'domwin_group_edit',
		sizes		: [550,180],
		scrollbars	: false,
		languages	: false,
		title		: this.title,
		ajax_url	: this.href,
		closeConfirm: true,
		max_minBtn	: false,
		modal_close	: false,
		remove_scroll: false
	}); 
});

$('groups_table').getElements('.edit').addEvent('click', function(e){
	new Event(e).stop();
	domwin.newWindow({
		id			: 'domwin_group_edit',
		sizes		: [550,180],
		scrollbars	: false,
		languages	: false,
		title		: this.title,
		ajax_url	: this.href,
		closeConfirm: true,
		max_minBtn	: false,
		modal_close	: false,
		remove_scroll: false
	}); 
});

$('groups_table').getElements('.trash').addEvent('click', function(e){
	new Event(e).stop();
	if (confirm('Opravdu si přejete odstranit tuto položku?')){
		new Request.HTML({
			url: this.href,
			update: $('domwin_groups').getElement('.CB_ImgContainer')
		}).send();
	}
});
</script>
