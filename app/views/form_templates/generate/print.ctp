﻿<div id="html_code">
<?php 
echo $this->data['FormData']['html_code'];

?>
</div>	
<script>

	$('html_code').getElements('input,select,textarea').each(function(item){

	//console.log(item.getNext().get('tag'));
	//alert(item.getNext().get('tag'));
		if(item.get('tag') == 'input'){
			if(item.getProperty('type') == 'text'){
				text = item.value;
				newtext = new Element('span').setHTML(text);
				newtext.inject(item,'after');
				item.dispose();
			}
			else{
				text = (item.checked ? ' Ano' : ' Ne');
				newtext = new Element('span').setHTML(text);
				newtext.inject(item,'after');
				item.dispose();
			}
		}
		else if(item.get('tag') == 'textarea'){
			text = item.value;
			newtext = new Element('p').setHTML(text);
			br = new Element('br');
			newtext.inject(item,'after');
			br.inject(item,'after');
			item.dispose();
		}
		else if(item.get('tag') == 'select'){		
			text = item.getOptionText();
			newtext = new Element('span').setHTML(text);
			newtext.inject(item,'after');
			item.dispose();
		}
		

	});

	window.print();
</script>