<form action='/client_foreigns/edit/' method='post' id='setting_career_edit_formular'>
	<?php echo $htmlExt->hidden('ClientForeign/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Nastavení</legend>
				<?php echo $htmlExt->input('ClientForeign/jmeno',array('tabindex'=>1,'label'=>'Jméno', 'class'=>'long','label_class'=>'long'));?> <br class="clear">
			    <?php echo $htmlExt->input('ClientForeign/prijmeni',array('tabindex'=>2,'label'=>'Přijmení', 'class'=>'long','label_class'=>'long'));?> <br class="clear">
				<?php echo $htmlExt->input('ClientForeign/ulice',	array('label'=>'Ulice'));?><br />
				<?php echo $htmlExt->input('ClientForeign/mesto',	array('label'=>'Město'));?><br />
                <?php echo $htmlExt->input('ClientForeign/psc',	array('label'=>'PSČ'));?><br />
                <?php echo $htmlExt->selectTag('ClientForeign/stat_id',$company_stat_list,null,array('label'=>'Stát'),null,false);?><br />
                <?php echo $htmlExt->input('ClientForeign/telefon',	array('label'=>'Telefon'));?><br />
                <?php echo $htmlExt->input('ClientForeign/email',	array('label'=>'Email'));?><br />
                <?php echo $htmlExt->input('ClientForeign/datum_narozeni', array('tabindex' =>4, 'label' => 'Datum narození','class'=>'medium')); ?><img src='/css/fastest/icons/calendar.png' id='filtr_datum_narozeni'/><br />
			    <?php echo $htmlExt->input('ClientForeign/cislo_op', array('tabindex' => 5, 'label' =>'Číslo OP', 'class'=>'')); ?><br />
     
            </fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('setting_career_edit_formular');
        
        var rege=/^\d{4}\-\d{2}\-\d{2}$/;       
        if($('ClientForeignDatumNarozeni').value != '' && rege.test($('ClientForeignDatumNarozeni').value) === false) 
			if(valid_result == true)
				valid_result = Array("Datum narození musí být ve formátu RRRR-MM-DD (R - Rok, M - měsíc, D - den)");
			else
				valid_result[valid_result.length]="Datum narození musí být ve formátu RRRR-MM-DD (R - Rok, M - měsíc, D - den)";        
        else{
            datum_narozeni = $('ClientForeignDatumNarozeni').value.split('-');
            if(datum_narozeni[0] < 1911){//sto let = chyba
                if(valid_result == true)
				    valid_result = Array("Člověk je starší jak 100let!!!");
    			else
    				valid_result[valid_result.length]="Člověk je starší jak 100let!!!";        
            }    
        }
        
		if (valid_result == true){
			new Request.JSON({
				url:$('setting_career_edit_formular').action,		
				onComplete:function(){
					click_refresh($('ClientForeignId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('setting_career_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
    
    $('filtr_datum_narozeni').addEvent('click',function(e){new Event(e).stop(); displayCalendar($('ClientForeignDatumNarozeni'),'yyyy-mm-dd',this,true);});

	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('setting_career_edit_formular',{
		'ClientForeignJmeno': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit jméno'}
		},
        'ClientForeignPrijmeni': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit přijmení'}
		},
        'ClientForeignUlice': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit ulice'}
		},
        'ClientForeignMesto': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit město'}
		},
        'ClientForeignPsc': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit PSČ'}
		},
        'ClientForeignTelefon': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit telefon'}
		},
        'ClientForeignEmail': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit email'}
		},
        'ClientForeignDatumNarozeni': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit datum narození'},
		},
	});
	validation.generate('setting_career_edit_formular',<?php echo (isset($this->data['ClientForeign']['id']))?'true':'false';?>);
</script>