<form action="/mv_items/edit" method="post" id='form_opp'>
	<?php echo $htmlExt->hidden('MvItem/id');?>
	<fieldset>
		<legend>Popis OOP</legend>
		<div class="sll">
			<?php echo $htmlExt->selectTag('MvItem/druh',$type_list, null, array('label'=>'Druh'),null,false);?><br/>
			<?php echo $htmlExt->input('MvItem/code',array('label'=>'Kód'));?><br/>
		</div>
		<div class="slr">
			<?php echo $htmlExt->input('MvItem/name',array('label'=>'Název'));?><br/>
			<?php echo $htmlExt->input('MvItem/pocet',array('label'=>'Počet','class'=>'integer'));?><br/>
		</div>
		<br />
	</fieldset>
	<fieldset>
		<legend>Nastavení cen</legend>
		<div class="sll">
			<?php echo $htmlExt->input('MvItem/cena_cz',array('label'=>'Cena CZK', 'class'=>'float'));?><br/>
			<?php echo $htmlExt->input('MvItem/cena_eur',array('label'=>'Cena EURO', 'class'=>'read_info'));?><br/>
		</div>
		<div class="slr">
			<?php echo $htmlExt->input('MvItem/kurz',array('label'=>'Kurz', 'readonly'=>'readonly','class'=>'read_info'));?><br/>
		</div>
		<br />
	</fieldset>
	<div class="win_save">
		<input type="button" value="Uložit" id='save_form'  class="button"/>
		<input type="button" value="Zavřít" id='close_form' class="button"/>
	</div>
</form>
<script>
	$$('.float, .integer').inputLimit();

	$('save_form').addEvent('click', function(e){
		e.stop();
        valid_result = validation.valideForm('form_opp');
		if (valid_result == true){
    		new Request.JSON({
    			url: $('form_opp').getProperty('action'),
    			onComplete: function(json){
    				if (json){
    					if (json.result === true){
    						click_refresh($('MvItemId').value);
    						domwin.closeWindow('domwin');
    					} else {
    						alert(json.message);
    					}
    				} else {
    					alert('Chyba aplikace');
    				}
    			}
    		}).send($('form_opp'));
        } else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	})
    
    validation.define('form_opp',{
		'MvItemCode': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit kód'}
		},
        'MvItemName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'}
		},
        'MvItemCount': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit počet'}
		},
        'MvItemSize': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit velikost'}
		},
        'MvItemPriceCz': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit cenu'}
		}
	});
	validation.generate('form_opp',<?php echo (isset($this->data['MvItem']['id']))?'true':'false';?>);

	
	$('close_form').addEvent('click', function(e){
		e.stop();
		domwin.closeWindow('domwin');
	})
	
	$('MvItemCenaCz').addEvent('change', function(){
		if(this.value == '') this.value = 0;
		var kurz = $('MvItemKurz').value, euro = $('MvItemCenaEur');
		euro.value = Math.round(this.value / kurz * 100) / 100;
		
	}).focus();

    $('MvItemCenaEur').addEvent('change', function(){
    		if(this.value == '') this.value = 0;
    		var kurz = $('MvItemKurz').value, euro = $('MvItemCenaCz');
    		euro.value = Math.round(this.value * kurz * 100) / 100;

    	})
</script>