
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">WWW nabídka</a></li>
			<li class="ousko"><a href="#krok2">Profesia</a></li>
        </ul>
	</div>
	<div class="domtabs admin_dom">
        <form action='/www_insertions/edit/' method='post' id='company_template_edit_formular'>
             <?php echo $htmlExt->hidden('WwwInsertion/id');?>
             <?php //echo $htmlExt->hidden('WwwInsertion/cms_user_id');?>
             <?php echo $htmlExt->hidden('DeleteExtends');?>
         <div class="domtabs field">
                    <fieldset>

                        <legend>Informace pro www stránky</legend>
                            <?php echo $htmlExt->selectTag('WwwInsertion/stat_id',$company_stat_list,null,array('label'=>'Stát','class'=>'long','label_class'=>'long'),null,false);?><br />
                            <?php echo $htmlExt->selectTag('WwwInsertion/province_id',$province_list,null,array('label'=>'Kraj','class'=>'long','label_class'=>'long'));?><br />
                            <?php echo $htmlExt->selectTag('WwwInsertion/setting_career_item_id',$setting_career_item_list,null,array('label'=>'Zaměření','class'=>'long','label_class'=>'long'));?><br />

                            <?php echo $htmlExt->input('WwwInsertion/name', array('class'=>'long','label'=>'Název pozice','label_class'=>'long'));?> <br />
                            <?php echo $htmlExt->input('WwwInsertion/plat', array('class'=>'long','label'=>'Plat','label_class'=>'long'));?> <br />

                            <?php echo $htmlExt->input('WwwInsertion/city', array('class'=>'long','label'=>'Místo výkonu práce','label_class'=>'long'));?> <br />
                            <?php echo $htmlExt->input('WwwInsertion/napln', array('class'=>'long','label'=>'Náplň práce','label_class'=>'long'));?> <br />
                            <?php echo $htmlExt->textarea('WwwInsertion/pozadavky', array('class'=>'long','label'=>'Požadujeme','label_class'=>'long'));?> <br />
                            <?php echo $htmlExt->textarea('WwwInsertion/nabizime', array('class'=>'long','label'=>'Nabizíme','label_class'=>'long'));?> <br />
                            <?php echo $htmlExt->textarea('WwwInsertion/dalsi_informace', array('class'=>'long','label'=>'Další informace','label_class'=>'long'));?> <br />

                        </fieldset>
        </div>
        <div class="domtabs field">

               <fieldset>
                   <legend>Informace pro www stránky profesia</legend>
                       <?php echo $htmlExt->selectTag('WwwInsertion/profesia_contact_user_id',$contact_list_profesia,null, array('label'=>'Uživatel pro kontakt:'),null,true);?><br />
                       <?php echo $htmlExt->checkbox('WwwInsertion/profesia_view_agreement',null,array('label'=>'Souhlas se zveřejněním','value'=>1));?><br />
                       <?php echo $htmlExt->selectTag('WwwInsertion/profesia_send_cv',$sendcv_list_profesia,null, array('label'=>'Zasílání vyhovujících životopisů emailem'),null,true);?><br />

                      <table id='RegionTable' class="table">
                         <thead>
                            <tr>
                                <th width="33%">Stát</th><th width="33%">Kraj</th><th width="33%">Okres</th>
                            </tr>
                         </thead>
                         <tbody>
                           <tr>
                                <td><?php   if(isset($insertion['1|profesia_stat_id']['id'])) { echo $htmlExt->hidden('Ids/1|profesia_stat_id',array('value'=>$insertion['1|profesia_stat_id']['id'])); }
                                            if(isset($insertion['1|profesia_stat_id']['value'])){ $val = $insertion['1|profesia_stat_id']['value']; }else{ $val=''; }
                                            echo $htmlExt->selectTag('ProfesiaWwwInsertion/1/profesia_stat_id',$stat_list_profesia , $val ,array('class'=>'selectStat'));?></td>

                                <td><?php   if(isset($insertion['1|profesia_country_id']['id'])) { echo $htmlExt->hidden('Ids/1|profesia_country_id',array('value'=>$insertion['1|profesia_country_id']['id'])); }
                                            if(isset($insertion['1|profesia_country_id']['value'])){ $val = $insertion['1|profesia_country_id']['value'];}else{ $val = ''; }
                                            echo $htmlExt->selectTag('ProfesiaWwwInsertion/1/profesia_country_id', $code_book_profesia['regions'], $val , array('class'=>'selectCountry')); ?></td>

                                <td><?php   if(isset($insertion['1|profesia_province_id']['id'])){ echo  $htmlExt->hidden('Ids/1|profesia_province_id',array('value'=>$insertion['1|profesia_province_id']['id'])); }
                                            if(isset($insertion['1|profesia_province_id']['value'])){ $val = $insertion['1|profesia_province_id']['value'];}else{ $val=''; }
                                            echo $htmlExt->selectTag('ProfesiaWwwInsertion/1/profesia_province_id', $code_book_profesia['regions'], $val , array('class'=>'selectProvince'));?></td>
                            </tr>
                                <? if(isset($insertion['profesia_stat_id']) && Count($insertion['profesia_stat_id']) > 0):  ?>
                                       <?  foreach($insertion['profesia_stat_id'] as $rand => $item): ?>

                                               <td><?php   if(isset($insertion[$rand.'|profesia_stat_id']['id'])) { echo $htmlExt->hidden('Ids/'.$rand.'|profesia_stat_id',array('value'=>$insertion[$rand.'|profesia_stat_id']['id'])); }
                                                           if(isset($insertion[$rand.'|profesia_stat_id']['value'])){ $val = $insertion[$rand.'|profesia_stat_id']['value']; }else{ $val=''; }
                                                           echo $htmlExt->selectTag('ProfesiaWwwInsertion/'.$rand.'/profesia_stat_id',$stat_list_profesia , $val ,array('class'=>'selectStat', 'rel'=>$rand));?></td>

                                               <td><?php   if(isset($insertion[$rand.'|profesia_country_id']['id'])) { echo $htmlExt->hidden('Ids/'.$rand.'|profesia_country_id',array('value'=>$insertion[$rand.'|profesia_country_id']['id'])); }
                                                           if(isset($insertion[$rand.'|profesia_country_id']['value'])){ $val = $insertion[$rand.'|profesia_country_id']['value'];}else{ $val=''; }
                                                           echo $htmlExt->selectTag('ProfesiaWwwInsertion/'.$rand.'/profesia_country_id', $code_book_profesia['regions'], $val , array('class'=>'selectCountry')); ?></td>
                                               <td><?php   if(isset($insertion[$rand.'|profesia_province_id']['id'])){
                                                                echo $htmlExt->hidden('Ids/'.$rand.'|profesia_province_id',array('value'=>$insertion[$rand.'|profesia_province_id']['id']));
                                                                $val = $insertion[$rand.'|profesia_province_id']['value'];
                                                           }else{ $val=''; }
                                                           echo $htmlExt->selectTag('ProfesiaWwwInsertion/'.$rand.'/profesia_province_id', $code_book_profesia['regions'], $val , array('class'=>'selectProvince'));?>
                                                           <a href="#" class="ta trash delete-row" > &nbsp </a></td>
                                               </tr>

                                       <? endforeach; ?>
                                <? endif;  ?>
                        </tbody>
                        <tfoot>
                             <tr><td><input type='button' id='addRegion' value="Přidat" /></td><td></td><td></td></tr>
                        </tfoot>
                        </table>

                      <table id='PosCatTable' class="table">
                      <thead><tr><th>Kategorie</th><th>Pozice</th></tr></thead>
                      <tbody>
                      <tr>
                           <td><?php   if(isset($insertion['1|profesia_category_id']['id'])) {
                                            $val = $insertion['1|profesia_category_id']['value'];
                                            echo $htmlExt->hidden('Ids/1|profesia_category_id',array('value'=>$insertion['1|profesia_category_id']['id']));
                                       }else{ $val = ''; }
                                       echo $htmlExt->selectTag('ProfesiaWwwInsertion/1/profesia_category_id',$code_book_profesia['categories'], $val,array('class'=>'selectCategory'));?></td>
                           <td><?php   if(isset($insertion['1|profesia_position_id']['id'])) {
                                           $val = $insertion['1|profesia_position_id']['value'];
                                           echo $htmlExt->hidden('Ids/1|profesia_position_id',array('value'=>$insertion['1|profesia_position_id']['id']));
                                       }else{ $val = ''; }
                                       echo $htmlExt->selectTag('ProfesiaWwwInsertion/1/profesia_position_id',$code_book_profesia['positions'], $val ,array('class'=>'selectPosition'));?></td>
                       </tr>
                               <? if(isset($insertion['profesia_category_id']) && Count($insertion['profesia_category_id']) > 0):  ?>
                                          <?  foreach($insertion['profesia_category_id'] as $rand => $item): ?>
                                              <? if($rand != 1): ?>
                                                  <tr>
                                                       <td><?php   if(isset($insertion[$rand.'|profesia_category_id']['id'])) {
                                                                        $val = $insertion[$rand.'|profesia_category_id']['value'];
                                                                        echo $htmlExt->hidden('Ids/'.$rand.'|profesia_category_id',array('value'=>$insertion[$rand.'|profesia_category_id']['id']));
                                                                   }else{ $val = ''; }
                                                                   echo $htmlExt->selectTag('ProfesiaWwwInsertion/'.$rand.'/profesia_category_id',$code_book_profesia['categories'], $val,array());?></td>
                                                       <td><?php   if(isset($insertion[$rand.'|profesia_position_id']['id'])) {
                                                                       $val = $insertion[$rand.'|profesia_position_id']['value'];
                                                                       echo $htmlExt->hidden('Ids/'.$rand.'|profesia_position_id',array('value'=>$insertion[$rand.'|profesia_position_id']['id']));
                                                                   }else{ $val = ''; }
                                                                   echo $htmlExt->selectTag('ProfesiaWwwInsertion/'.$rand.'/profesia_position_id',$code_book_profesia['positions'], $val ,array());?>
                                                                   <a href="#" class="ta trash delete-row" > &nbsp </a></td>
                                                   </tr>
                                               <? endif; ?>
                                          <? endforeach; ?>
                               <? endif;  ?>

                       </tbody>
                      <tfoot>
                           <tr><td><input type='button' id='addPossCat' value="Přidat" /></td><td></td></tr>
                      </tfoot>
                      </table>

                       <?php echo $htmlExt->input('WwwInsertion/profesia_specialization', array('load_ajax'=>'Zaměření, obor'));?> <br />
                       <?php echo $htmlExt->inputDate('WwwInsertion/profesia_nastup_date',array('tabindex'=>1,'label'=>'Datum nástupu'));?> <br class="clear"/>
                       <?php echo $htmlExt->inputDate('WwwInsertion/profesia_platnost_do',array('tabindex'=>1,'label'=>'Platné do'));?> <br class="clear"/>
                       <?php echo $htmlExt->selectTag('WwwInsertion/profesia_education_id',$code_book_profesia['educationlevels'],null,array('label'=>'Vzdělání'));?><br />
                       <?php echo $htmlExt->selectTag('WwwInsertion/profesia_skillcategory_id',$code_book_profesia['skillcategories'],null,array('label'=>'Kategorie dovednosti'));?><br />
                       <?php echo $htmlExt->selectTag('WwwInsertion/profesia_skill_id', $code_book_profesia['skills'],null,array('label'=>'Dovednost'));?><br />
                       <?php echo $htmlExt->selectTag('WwwInsertion/profesia_skilllevel_id',$code_book_profesia['skilllevels'],null,array('label'=>'Stupeň dovednosti'));?><br />

                       <table id='businessAreasTable' width="100%" cellpadding="0" cellspacing="0">
                       <tr>
                            <? if(isset($insertion['1|profesia_businessarea_id'])){ $val = $insertion['1|profesia_businessarea_id']['value']; $id=$insertion['1|profesia_businessarea_id']['id']; }else{ $id=''; $val=''; } ?>
                            <td><?php echo $htmlExt->selectTag('ProfesiaWwwInsertion/1/profesia_businessarea_id',$code_book_profesia['businessareas'],$val,array('label'=>'Oblast působení'));?>
                                <?php echo $htmlExt->hidden('Ids/1|profesia_businessarea_id',array('value'=>$id));?></td></tr>
                        </tr>
                       </table>

                       <table id='JobTypeTable' class="table">
                       <tbody>
                       <tr><th>Typ smlouvy</th><th></th></tr>
                       <tr>
                            <? if(isset($insertion['1|profesia_jobtype_id'])){ $val = $insertion['1|profesia_jobtype_id']['value']; $id=$insertion['1|profesia_jobtype_id']['id']; }else{ $id=''; $val=''; } ?>
                           <td></td><td><?php echo $htmlExt->selectTag('ProfesiaWwwInsertion/1/profesia_jobtype_id', $jobtype_list_profesia , $val,array());?>
                           <?php echo $htmlExt->hidden('Ids/1|profesia_jobtype_id',array('value'=>$id)) ;?></td>
                       </tr>
                             <? if(isset($insertion['profesia_jobtype_id']) && Count($insertion['profesia_jobtype_id']) > 0):  ?>
                                      <?  foreach($insertion['profesia_jobtype_id'] as $rand => $item): ?>
                                          <? if($rand != 1): ?>
                                              <? if(isset($insertion[$rand.'|profesia_jobtype_id'])){ $val = $insertion[$rand.'|profesia_jobtype_id']['value']; $id=$insertion[$rand.'|profesia_jobtype_id']['id']; }else{ $id=''; $val=''; } ?>
                                               <tr><td></td><td><?php echo $htmlExt->hidden('Ids/'.$rand.'|profesia_jobtype_id',array('value'=>$insertion[$rand.'|profesia_jobtype_id']['id']));?>
                                                       <?php echo $htmlExt->selectTag('ProfesiaWwwInsertion/'.$rand.'/profesia_jobtype_id', $jobtype_list_profesia ,$insertion[$rand.'|profesia_jobtype_id']['value'], array('tabindex'=>1));?>
                                                       <a href="#" class="ta trash delete-row" > &nbsp </a></td>
                                              </tr>
                                           <? endif; ?>
                                      <? endforeach; ?>
                             <? endif;  ?>
                       </tbody>
                       <tfoot>
                           <tr><td><input type='button' id='addJbT' value="Přidat" /></td><td></td></tr>
                       </tfoot>
                       </table>


                       <?php echo $htmlExt->selectTag('WwwInsertion/profesia_currency_id',$code_book_profesia['currencies'],null,array('label'=>'Měna platu'));?><br />
                       <?php echo $htmlExt->selectTag('WwwInsertion/profesia_driving_licence',$drive_licence,null,array('label'=>'Řidičské oprávnění'));?><br />
                       <?php echo $htmlExt->input('WwwInsertion/profesia_benefits',array('label'=>'Další výhody'));?><br />
                       <?php echo $htmlExt->textarea('WwwInsertion/profesia_company_characteristic',array('label'=>'Krátká charakteristika společnosti'));?><br />

               </fieldset>
        </div>
            <div class='formular_action'>
                <input type='button' value='Uložit' id='AddEditWwwInsertionSaveAndClose' />
                <input type='button' value='Zavřít' id='AddEditWwwInsertionClose'/>
            </div>
        </form>
	</div>

<script>
    function uniqid()
    {
        var newDate = new Date;
        return newDate.getTime();
    }

  /*	$('addBussiness').addEvent('click',function(e){
        var rand_id = uniqid();
        var table = $('businessAreasTable').getElement('tbody');
        var tr = new Element('tr').inject(table);
            new Element('td').set('html', '<label></label><select id="ProfesiaWwwInsertion'+rand_id+'ProfesiaBusinessareaId" name="data[ProfesiaWwwInsertion]['+rand_id+'][profesia_businessarea_id]"><?= $bussinesAreaOptions; ?></select><a class="ta trash" href="#" id="remove_'+rand_id+'"> &nbsp </a>').inject(tr);
            $('remove_'+rand_id).addEvent('click', function(e){e.stop; this.getParent('tr').dispose()});
    });*/

  	$('addJbT').addEvent('click',function(e){
        var rand_id = uniqid();
        var table = $('JobTypeTable').getElement('tbody');
        var tr = new Element('tr').inject(table);
            new Element('td').inject(tr);
            new Element('td').set('html', '<select id="ProfesiaWwwInsertion'+rand_id+'ProfesiaJobtypeId" name="data[ProfesiaWwwInsertion]['+rand_id+'][profesia_jobtype_id]"><?= $jobTypeOptions; ?></select><a class="ta trash" href="#" id="remove_'+rand_id+'"> &nbsp </a>').inject(tr);
            $('remove_'+rand_id).addEvent('click', function(e){e.stop; this.getParent('tr').dispose()});
    });

    $('addRegion').addEvent('click',function(e){
        var rand_id = uniqid();
        var table = $('RegionTable').getElement('tbody');
        var tr = new Element('tr').inject(table);
            new Element('td').set('html', '<select id="ProfesiaWwwInsertion'+rand_id+'ProfesiaStatId" name="data[ProfesiaWwwInsertion]['+rand_id+'][profesia_stat_id]"><?= $statOptions; ?></select>').inject(tr);
            new Element('td').set('html', '<select id="ProfesiaWwwInsertion'+rand_id+'ProfesiaCountryId" name="data[ProfesiaWwwInsertion]['+rand_id+'][profesia_country_id]"></select>').inject(tr);
            new Element('td').set('html', '<select id="ProfesiaWwwInsertion'+rand_id+'ProfesiaProvinceId" name="data[ProfesiaWwwInsertion]['+rand_id+'][profesia_province_id]"></select><a class="ta trash" href="#" id="remove_'+rand_id+'"> &nbsp </a>').inject(tr);
            $('remove_'+rand_id).addEvent('click', function(e){e.stop; this.getParent('tr').dispose()});
            $('ProfesiaWwwInsertion'+rand_id+'ProfesiaStatId').ajaxLoad('/www_insertions/load_region_profesia/', ['ProfesiaWwwInsertion'+rand_id+'ProfesiaCountryId']);
            $('ProfesiaWwwInsertion'+rand_id+'ProfesiaCountryId').ajaxLoad('/www_insertions/load_region_profesia/', ['ProfesiaWwwInsertion'+rand_id+'ProfesiaProvinceId']);
    });
    $('addPossCat').addEvent('click',function(e){
      var rand_id = uniqid();
      var table = $('PosCatTable').getElement('tbody');
      var tr = new Element('tr').inject(table);
          new Element('td').set('html', '<select id="ProfesiaWwwInsertion'+rand_id+'ProfesiaCategoryId" name="data[ProfesiaWwwInsertion]['+rand_id+'][profesia_category_id]"><?= $categoryOptions; ?></select>').inject(tr);
          new Element('td').set('html', '<select id="ProfesiaWwwInsertion'+rand_id+'ProfesiaPositionId" name="data[ProfesiaWwwInsertion]['+rand_id+'][profesia_position_id]"></select><a class="ta trash" href="#" id="remove_'+rand_id+'"> &nbsp </a>').inject(tr);
          $('remove_'+rand_id).addEvent('click', function(e){e.stop; this.getParent('tr').dispose()});
          $('ProfesiaWwwInsertion'+rand_id+'ProfesiaCategoryId').ajaxLoad('/www_insertions/load_position_profesia/', ['ProfesiaWwwInsertion'+rand_id+'ProfesiaPositionId']);
    });

	/*
	 * Zakladni funkce
	 */
	var domtab = new DomTabs({'className':'admin_dom'}); 
    $$('.integer, .float').inputLimit();

	//$('ProfesiaWwwInsertion1ProfesiaStatId').ajaxLoad('/www_insertions/load_region_profesia/', ['ProfesiaWwwInsertion1ProfesiaCountryId']);
	//$('ProfesiaWwwInsertion1ProfesiaCountryId').ajaxLoad('/www_insertions/load_region_profesia/', ['ProfesiaWwwInsertion1ProfesiaProvinceId']);
	//$('ProfesiaWwwInsertion1ProfesiaCategoryId').ajaxLoad('/www_insertions/load_position_profesia/', ['ProfesiaWwwInsertion1ProfesiaPositionId']);

	$('WwwInsertionProfesiaSkillcategoryId').ajaxLoad('/www_insertions/load_skill_profesia/', ['WwwInsertionProfesiaSkillId']);

    $$('.selectStat').each( function( select ){
        select.ajaxLoad('/www_insertions/load_region_profesia/', [select.getParent('tr').getElement('.selectCountry').getProperty('id')]);
    });

    $$('.selectCountry').each( function( select ){
        select.ajaxLoad('/www_insertions/load_region_profesia/', [select.getParent('tr').getElement('.selectProvince').getProperty('id')]);
    });

    $$('.selectCategory').each( function( select ){
         select.ajaxLoad('/www_insertions/load_position_profesia/', [select.getParent('tr').getElement('.selectPosition').getProperty('id')]);
    });
	/*
	 * Zavreni Domwinu, bez ulozeni
	 */
	if($('AddEditWwwInsertionClose')){
        $('AddEditWwwInsertionClose').addEvent('click',function(e){new Event(e).stop();
            domwin.closeWindow('domwin');
        });
     }
	
	/*
	 * Ulozeni www objednavky
	 */

     $$('.delete-row').addEvent('click',function(e){
                 e.stop;
                 var tr =  this.getParent('tr');
                 var ids = tr.getElements('input[type="hidden"]');
                 var value = '' + $('DeleteExtends').value;
                 Array.each(ids, function( el, i){
                     value += el.value + '|';
                 });
                 $('DeleteExtends').set('value', value);
                 tr.dispose();
                // alert($('DeleteExtends').value);

    });

	if ($('AddEditWwwInsertionSaveAndClose')) {
	   	$('AddEditWwwInsertionSaveAndClose').addEvent('click', function(e){
	   	  new Event(e).stop();
	      
          valid_result = validation.valideForm('company_template_edit_formular');
		  if (valid_result == true){
                button_preloader($('AddEditWwwInsertionSaveAndClose'));
	   			new Request.JSON({
	   				url: $('company_template_edit_formular').action,
	   				onComplete: function(json){
	   					if (json) {
	   						if (json.result === true) {
	                            button_preloader_disable($('AddEditWwwInsertionSaveAndClose'));
	   							domwin.closeWindow('domwin');
       						}
	   						else {
	   							alert(json.message);
	   						}
	   					}
	   					else {
	   						alert('Chyba apliakce');
	   					}
	   				}
	   			}).post($('company_template_edit_formular'));
	   	  } else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		  }
	   });
	}
  if ($('AddEditWwwInsertionSaveAndClose2')) {
  	   	$('AddEditWwwInsertionSaveAndClose2').addEvent('click', function(e){
  	   	  new Event(e).stop();

            valid_result = validation.valideForm('company_template_edit_formular2');
  		  if (valid_result == true){
                  button_preloader($('AddEditWwwInsertionSaveAndClose2'));
  	   			new Request.JSON({
  	   				url: $('company_template_edit_formular2').action,
  	   				onComplete: function(json){
  	   					if (json) {
  	   						if (json.result === true) {
  	                            button_preloader_disable($('AddEditWwwInsertionSaveAndClose2'));
  	   							domwin.closeWindow('domwin');
         						}
  	   						else {
  	   							alert(json.message);
  	   						}
  	   					}
  	   					else {
  	   						alert('Chyba apliakce');
  	   					}
  	   				}
  	   			}).post($('company_template_edit_formular2'));
  	   	  } else {
  			var error_message = new MyAlert();
  			error_message.show(valid_result)
  		  }
  	   });
  	}
    validation.define('company_template_edit_formular',{
		'WwwInsertionName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'}
		},
		'WwwInsertionNapln': {
        	'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit náplň'}
        }
	});
	validation.generate('company_template_edit_formular',<?php echo (isset($this->data['WwwInsertion']['id']))?'true':'false';?>);

</script>