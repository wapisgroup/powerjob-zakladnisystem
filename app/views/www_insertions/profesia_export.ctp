<? foreach($insertions as $ins): ?>
<?
   $multi = $ins['multi'];
   $insertion = $ins['insertion'];
?>
<? if(isset($multi['profesia_view_agreement'])): ?>
<job externalid="<?= $insertion['WwwInsertion']['id']; ?>">
        <position><![CDATA[<?= $insertion['WwwInsertion']['name']; ?>]]></position>
        <refnr>1-12-1727/PF</refnr>
        <offerlocation description="">
            <? if(isset($multi['profesia_category_id'])): ?>
                <? foreach($multi['region_id'] as $uid=>$item): ?>
                      <location id="<?= $item; ?>" />
                <? endforeach; ?>
            <? endif; ?>

        </offerlocation>
        <jobtasks><![CDATA[<?= $insertion['WwwInsertion']['napln']; ?>]]></jobtasks>
        <minsalary><?= $insertion['WwwInsertion']['plat']; ?></minsalary>
        <maxsalary><?= $insertion['WwwInsertion']['plat']; ?></maxsalary>
        <salary></salary>
        <currency id="<?= $insertion['WwwInsertion']['profesia_currency_id']; ?>" />
        <startdate><?PHP  if($insertion['WwwInsertion']['profesia_nastup_date'] && $insertion['WwwInsertion']['profesia_nastup_date'] != '0000-00-00'){ echo date("d.m.Y", strtotime($insertion['WwwInsertion']['profesia_nastup_date']));} ?></startdate>
        <jobtype>
        <? if(isset($multi['profesia_jobtype_id'])): ?>
            <? foreach($multi['profesia_jobtype_id'] as $uid => $item): ?>
                     <type id="<?= $item; //MULTIPLE ?>" />
            <? endforeach; ?>
        <? endif; ?>
        </jobtype>
        <otherbenefits><![CDATA[<?= $insertion['WwwInsertion']['profesia_benefits']; ?>]]></otherbenefits>
        <noteforcandidate><![CDATA[<?= $insertion['WwwInsertion']['dalsi_informace']; ?>]]></noteforcandidate>
        <applicabletill><?PHP  if($insertion['WwwInsertion']['profesia_platnost_do'] && $insertion['WwwInsertion']['profesia_platnost_do'] != '0000-00-00 00:00:00'){ echo date("D, j M Y G:i:s T", strtotime($insertion['WwwInsertion']['profesia_platnost_do']));} ?></applicabletill>
        <educationlevel>
            <education id="<?= $insertion['WwwInsertion']['profesia_education_id']; ?>" />
        </educationlevel>
        <specialization><![CDATA[<?= $insertion['WwwInsertion']['profesia_specialization']; ?>]]></specialization>
        <languageconjuction>or</languageconjuction>
        <offerskills>
            <skill id="<?= $insertion['WwwInsertion']['profesia_skill_id']; ?>" level="<?= $insertion['WwwInsertion']['profesia_skilllevel_id']; //MULTIPLE ?>" />
        </offerskills>
        <drivinglicence><?= $insertion['WwwInsertion']['profesia_driving_licence']; ?></drivinglicence>
        <validforgraduate value="0" />
        <prerequisites><![CDATA[<?= $insertion['WwwInsertion']['pozadavky']; ?>]]></prerequisites>
        <contactperson id=""><?= $insertion['contact']['name']; ?></contactperson>
        <title_before></title_before>
        <first_name><?= $insertion['contact']['jmeno']; ?></first_name>
        <surname><?= $insertion['contact']['prijmeni']; ?></surname>
        <contactphone><?= $insertion['contact']['telefon']; ?></contactphone>
        <contactemail><?= $insertion['contact']['email']; ?></contactemail>
        <contactaddress><![CDATA[<?= $insertion['contact']['ulice'].', '.$insertion['contact']['mesto'].' '.$insertion['contact']['psc']; ?>]]></contactaddress>
        <businessarea id="<?= $multi['profesia_businessarea_id'][1]; ?>" />
        <shortcompanycharacteristics><![CDATA[<?= $insertion['WwwInsertion']['profesia_company_characteristic']; ?>]]></shortcompanycharacteristics>
        <offercategorypositions>
        <? if(isset($multi['profesia_category_id'])): ?>
            <? foreach($multi['profesia_category_id'] as $uid=>$item): ?>
                  <catpos category="<?= $item; ?>" position="<?= $multi['profesia_position_id'][$uid]; //MULTIPLE ?>"/>
            <? endforeach; ?>
        <? endif; ?>
        </offercategorypositions>
        <offerlanguage>cz</offerlanguage>
        <customcategory>1</customcategory>
        <sendcv id="<?= $insertion['WwwInsertion']['profesia_send_cv']; ?>"/>
    </job>
<? endif; ?>
<? endforeach; ?>