 <form action='/report_contracts/edit/' method='post' id='ReportContract_edit_formular'>
	<?php echo $htmlExt->hidden('ReportContract/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Základní údaje</legend>
				<?php echo $htmlExt->input('ReportContract/name',array('tabindex'=>1,'label'=>'Název','class'=>'long','label_class'=>'long'));?> <br class="clear">
				<?php echo $htmlExt->selectTag('ReportContract/at_company_id',$company_list,null,array('tabindex'=>2,'label'=>'V majetku firmy'));?> <br class="clear">
                <?php echo $htmlExt->selectTag('ReportContract/at_project_id',$project_list,null,array('tabindex'=>2,'label'=>'Projekt'));?> <br class="clear">
				<?php echo $htmlExt->selectTag('ReportContract/at_project_centre_id',$centre_list,null,array('tabindex'=>3,'label'=>'Středisko'));?> <br class="clear">
                <?php echo $htmlExt->textarea('ReportContract/description',array('tabindex'=>5,'label'=>'Poznámka'));?> <br class="clear">
				
			</fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','class'=>'button'));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','class'=>'button'));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'});
    $('ReportContractAtProjectId').ajaxLoad('/int_clients/ajax_load_at_company_centre_list/',['ReportContractAtProjectCentreId']);
    
		
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('ReportContract_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('ReportContract_edit_formular').action,		
				onComplete:function(){
					click_refresh($('ReportContractId').value);
					domwin.closeWindow('domwin');
				}
			}).post($('ReportContract_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('ReportContract_edit_formular',{
		'ReportContractName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
		},
        'ReportContractAtCompanyId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit firmu'},
		},
        'ReportContractAtProjectId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit projekt'},
		},
        'ReportContractAtProjectCentreId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit středisko'},
		}
	});
	validation.generate('ReportContract_edit_formular',<?php echo (isset($this->data['ReportContract']['id']))?'true':'false';?>);
</script>