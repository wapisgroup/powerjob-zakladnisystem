<div class="win_fce top">
<a href='/eb_employees/edit_type_works/' class='button' id='company_contact_add_new'>Přidat typ práce</a>
</div>
<table class='table' id='table_kontakty'>
	<tr>
		<th>#ID</th>
		<th>Typ práce</th>
		<th>Na hodinu</th>
		<th>Upraveno</th>
        <th>Vytvořeno</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($list_type_works) && count($list_type_works) >0):?>
	<?php foreach($list_type_works as $item):?>
	<tr>
		<td><?php echo $item['TypeWork']['id'];?></td>
		<td><?php echo $item['TypeWork']['name'];?></td>
		<td><?php echo $item['TypeWork']['price'];?></td>
		<td><?php echo $fastest->czechDateTime($item['TypeWork']['updated']);?></td>
        <td><?php echo $fastest->czechDateTime($item['TypeWork']['created']);?></td>
		<td>
			<a title='Editace položy' 	class='ta edit' href='/eb_employees/edit_type_works/<?php echo $item['TypeWork']['id'];?>'/>edit</a>
			<a title='Do kosiku' 		class='ta trash' href='/eb_employees/trash_type_works/<?php echo $item['TypeWork']['id'];?>'/>trash</a>
		</td>
	</tr>
	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='5'>K této společnosti nebyl nadefinován žáden typ práce</td>
	</tr>
	<?php endif;?>
</table>
<?php // pr($list_type_works);?>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='ListTypeWorkClose' class='button'/>
	</div>
<script>
	$('ListTypeWorkClose').addEvent('click', function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin');
	});

	$('company_contact_add_new').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_kontakty_add',
			sizes		: [580,320],
			scrollbars	: true,
			title		: 'Přidání nového typu práce',
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
	
	$('table_kontakty').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tento typ práce?')){
			new Request.JSON({
				url:this.href,
				onComplete: (function(json){
				    if(json){
                        if(json.result == true){this.getParent('tr').dispose();}
                    }
				}).bind(this)
			}).send();
		}
	});
	
	$('table_kontakty').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_kontakty_add',
			sizes		: [580,320],
			scrollbars	: true,
			title		: 'Editace typu práce',
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>