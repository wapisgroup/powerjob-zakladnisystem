<form action='/eb_employees/edit_type_works/' method='post' id='company_contact_edit_formular'>
	<?php echo $htmlExt->hidden('TypeWork/id');?>
	<fieldset>
		<legend>Typ práce</legend>
		<div class='sll'>
			<?php echo $htmlExt->input('TypeWork/name',array('tabindex'=>1,'label'=>'Název'));?> <br class="clear">		
		</div>
		<div class='slr'>
			<?php echo $htmlExt->input('TypeWork/price',array('tabindex'=>2,'label'=>'Na hodinu','class'=>'integer'));?> <br class="clear">		
		</div>
		<br/>
	</fieldset>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditTypeWorkSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditTypeWorkClose'/>
	</div>
</form>
<script>
	$('AddEditTypeWorkClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_kontakty_add');});
	$('company_contact_edit_formular').getElements('.integer').inputLimit();
	$('AddEditTypeWorkSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('company_contact_edit_formular');
		if (valid_result == true){
			new Request.HTML({
				url:$('company_contact_edit_formular').action,		
				onComplete:function(){
				    domwin.loadContent('domwin');    
					domwin.closeWindow('domwin_kontakty_add');
				}
			}).post($('company_contact_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('company_contact_edit_formular',{
		'TypeWorkName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'}
		},
		'TypeWorkPrice': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit částku na hodinu'}
		}
	});
	validation.generate('company_contact_edit_formular',<?php echo (isset($this->data['TypeWork']['id']))?'true':'false';?>);
	
</script>