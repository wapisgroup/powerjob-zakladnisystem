<?php 
    $connection_id = $connection['ConnectionEbcomClient']['id'];
    $order_id = $connection['ConnectionEbcomClient']['eb_company_id'];
	$start 	= $connection['ConnectionEbcomClient']['start'];
	$end 	= ($connection['ConnectionEbcomClient']['end']== '0000-00-00')?date("Y-m-d"):$connection['ConnectionEbcomClient']['end'];	
	
	$pocet_dnu_v_mesici = cal_days_in_month(CAL_GREGORIAN, $month, $year);
	$last_day = date('N', mktime(0, 0, 0, $month, 1, $year)); 
	$current_day = $last_day;
	$dny = array( 1=>'Po', 2=>'Út', 3=>'St', 4=>'Čt', 5=>'Pá', 6=>'So', 7=>'Ne' );
	
	// render list of disabled fields
	$not_to_disabled = array();
	for ($i=1; $i <= $pocet_dnu_v_mesici; $i++){
		$curr_date = $rok.'-'.$mesic.'-'.((strlen($i) == 1)?'0'.$i:$i);
		if (strtotime($start) <= strtotime($curr_date) && strtotime($end) >= strtotime($curr_date))
			$not_to_disabled[] = $i;
	}

?>
	<?php echo $htmlExt->hidden('connections/'.$connection_id.'/EbClientWorkingHour/id',array('value'=>$connection['EbClientWorkingHour']['id']));?>
    <?php echo $htmlExt->hidden('connections/'.$connection_id.'/EbClientWorkingHour/eb_company_id',array('value'=>$connection['ConnectionEbcomClient']['eb_company_id']));?>
   
<div class="sll3" >  
		<?php echo $htmlExt->var_text('connections/'.$connection_id.'/ConnectionEbcomClient/start',array('label'=>'Datum nástupu','class'=>'read_info'));?> 
</div>
<div class="sll3" >          
		<?php echo $htmlExt->var_text('connections/'.$connection_id.'/ConnectionEbcomClient/end', array('label'=>'Datum ukončení','class'=>'read_info'));?>
</div>
<table class='table odpracovane_hodiny'>	
				<tr>
					<th>Den</th>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<th <?php 
							if (in_array(($i+1),$svatky)) 
								echo "class='svatky'";
							else if (in_array($current_day,array(6,7))) 
								echo "class='vikend'";
						?>>
							<?php echo $dny[$current_day];?>
							<br />
							<?php echo ($i+1);?>
							<?php $current_day = ($current_day == 7)?$current_day=1:$current_day+1;?>
							
						</th>
					<?php endfor;?>
				</tr>
				<?php 
					$smena_captions = array(
						1	=> 'Dny'
					);
				?>
				<?php for($k = 1; $k <= 1; $k++):?>
				<tr class='tr_smennost_<?php echo $k;?> '>	
					<td><?php echo $smena_captions[$k];?></td>
					<?php $current_day = $last_day;?>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<td <?php 
							if (in_array(($i+1),$svatky)) 
								echo "class='svatky'";
							else if (in_array($current_day,array(6,7))) 
								echo "class='vikend'";
						?>>
						<?php 
							if (in_array(($i+1),$svatky)){ 
								$class =  " svatky";
							} else if (in_array($current_day,array(6,7))){
								$class = " vikend";
							} else {
								$class = "";
							}
						?>
						<?php echo $htmlExt->input('connections/'.$connection_id.'/EbClientWorkingHour/days/'. '_' .$k. '_' .($i+1),array('class'=>'hodiny integer s'.$k.' '.$class,'disabled'=>(!in_array(($i+1),$not_to_disabled))?'disabled':null));?></td>
						<?php $current_day = ($current_day == 7)?$current_day=1:$current_day+1;?>
					<?php endfor;?>
				</tr>
				<?php endfor;?>
				<tr class='tr_food_ticket <?php if (!isset($this->data['connections'][$connection_id]['EbClientWorkingHour']['stravenka']) || $this->data['connections'][$connection_id]['EbClientWorkingHour']['stravenka'] == 0) echo 'none';?>'>
					<td>Stravenky - <a href="#" id="food_days_all">vše</a></td>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<td><?php echo $htmlExt->checkbox('connections/'.$connection_id.'/EbClientWorkingHour/food_days/'. '_' .$k. '_' .($i+1),null,array('class'=>'food_day','disabled'=>(!in_array(($i+1),$not_to_disabled))?'disabled':null));?></td> 
					<?php endfor;?>
				</tr>
				<tr class='tr_accommodation <?php if (!isset($this->data['connections'][$connection_id]['EbClientWorkingHour']['accommodation_id']) || $this->data['connections'][$connection_id]['EbClientWorkingHour']['accommodation_id'] == 0) echo 'none';?>'>
					<td>Ubytování <a href="#" id="accommodation_days_all">vše</a></td>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<td><?php echo $htmlExt->checkbox('connections/'.$connection_id.'/EbClientWorkingHour/accommodation_days/'. '_' .$k. '_' .($i+1),null,array('class'=>'accommodation_day','disabled'=>(!in_array(($i+1),$not_to_disabled))?'disabled':null));?></td> 
					<?php endfor;?>
				</tr>
			</table>
			<br />
			<fieldset>
				<legend>Úkony</legend>
				<div class='slr3'>
					<label></label><a href='#' class='add_task' rel="<?= $connection_id?>|<?= $order_id?>" onclick='return false'>Přidat úkolovku</a>
				</div>
				<div class='slr3'>
				
				</div>
				<div class='slr3'>
					<?php echo $htmlExt->input('connections/'.$connection_id.'/EbClientWorkingHour/salary_per_task',array('label'=>'Mzda za úkoly','readonly'=>'readonly','class'=>'read_info for_null_past_money_item_change task_sum'));?><br />
				</div>
				<br/><br/>
				<table class='tabulka table'>
					<thead><tr>
						<th>Úkol</th>
						<th>Cena za jednotku</th>
						<th>Počet jednotek</th>
						<th>Celkem za úkon</th>
						<th></th>
					</tr></thead>
					<tbody id='task_list<?= $connection_id?>' class="ukony">
						<?php 
                        $rozpocet_item_list = json_decode($salary_item_list, true);
                        if (isset($this->data['connections'][$connection_id]['ukols']) && is_array($this->data['connections'][$connection_id]['ukols']) && count($this->data['connections'][$connection_id]['ukols'])>0): ?>
                            <?php foreach($this->data['connections'][$connection_id]['ukols'] as $key => $item): ?>
                                <tr>
                                    <td>
                                        <select name="data[connections][<?= $connection_id?>][ukols][<?php echo $key;?>][name]" class="prace_select_items">
                                            <option value="" title="0"></option>
                                            <?php 
                                                  if(isset($rozpocet_item_list[$this->data['connections'][$connection_id]['ConnectionEbcomClient']['eb_company_id']])) 
                                                  foreach($rozpocet_item_list[$this->data['connections'][$connection_id]['ConnectionEbcomClient']['eb_company_id']] as $it):?>
                                                <option <?php echo ($item['rozpocet_item_id'] == $it['id']?'selected="selected"':'')?> value="<?php echo $it['id'];?>" title="<?php echo $it['pay_max'];?>"><?php echo $it['name'];?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </td>
                                    <td><input class="cena_ks" name="data[connections][<?= $connection_id?>][ukols][<?php echo $key;?>][cena_ks]" type="text"  value="<?php echo $item['cena_ks'];?>"/></td>
                                    <td><input class="ks" name="data[connections][<?= $connection_id?>][ukols][<?php echo $key;?>][ks]" type="text"  value="<?php echo $item['ks'];?>"/></td>
                                    <td><input readonly="readonly" class="cena_row read_info" name="data[connections][<?= $connection_id?>][ukols][<?php echo $key;?>][cena]"  value="<?php echo $item['cena'];?>" type="text"/></td>
                                    <td><a class="ta delete delete_ukolovka_row" href="#">Odstranit</a></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
					</tbody>
				</table>	
			</fieldset>