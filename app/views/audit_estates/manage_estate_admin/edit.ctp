<form action='/audit_estates/edit_estate_datum/' method='post' id='setting_stav_edit_formular'>
	<?php echo $htmlExt->hidden('ConnectionAuditEstate/id');?>
    <?php echo $htmlExt->hidden('ConnectionAuditEstate/private_use');?>
	<?php echo $htmlExt->hidden('ConnectionAuditEstate/audit_estate_id');?>
    <?php echo $htmlExt->hidden('ConnectionAuditEstate/old_to_date');?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Editace historie majeteku</legend>
				<div class="sll">  
					<?php echo $htmlExt->inputDate('ConnectionAuditEstate/created',array('tabindex'=>1,'label'=>'Datum přidělení'));?> <br class="clear">               		        
            	</div>
				<div class="slr">  
				 	<?php echo $htmlExt->inputDate('ConnectionAuditEstate/to_date',array('tabindex'=>2,'label'=>'Datum vrácení','class'=>'medium'));?><span id="clear" class="ta status_new0"></span> <br class="clear">               		        
           	    </div>
                <br />
                <?php echo $htmlExt->textarea('ConnectionAuditEstate/description',array('tabindex'=>3,'label'=>'Dopl. informace'));?><br class="clear">               		        
                <label class=''>Počet možných znaků:</label><var class='' id='sms_len'>100</var><br />
			</fieldset>
		</div>
	</div>
	<div class="win_save">
        <?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'});
    var old_date = $('ConnectionAuditEstateOldToDate').value;
    var send_stav = false;
    
    $('sms_len').setHTML(100 - $('ConnectionAuditEstateDescription').value.length);

	$('ConnectionAuditEstateDescription').addEvents({
		'keyup': function(){
		
			$('sms_len').setHTML(100 - this.value.length);
			if (this.value.length >= 100){
			    $('sms_len').setHTML(0); 
				this.value = this.value.substring(0,100);
            }    
		},
        'keypress':function(e){
            var event = new Event(e);
            if(event.code == 38)
				return false;
        }
	});
     
    $('clear').addEvent('click',function(e){
        $('calbut_ConnectionAuditEstateToDate').value = '';
    }); 
    
    $('calbut_ConnectionAuditEstateToDate').addEvent('change',function(e){
        //console.log(old_date);
        var from = $('calbut_ConnectionAuditEstateCreated').value;
        if(this.value < from){
            alert('Zvolené datum je menší než datum přidělení!!');
            this.value = old_date;
        }    
        else{
            var client_id = "<?= $this->data['ConnectionAuditEstate']['client_id']?>";
            var comp_id = "<?= $this->data['ConnectionAuditEstate']['at_company_id']?>";
            if(client_id != '' && comp_id != ''){
            	new Request.JSON({
    				url:'/audit_estates/check_return_date/'+this.value+'/'+client_id+'/'+comp_id+'/',		
    				onComplete:(function(json){
    				    if(json){
    				        if(json.result == true){
                                call_add_stav();
                                old_date = this.value;
                            }
                            else {
                                if(confirm(json.message+' Opravdu chcete provést odebrání?')){
                                    call_add_stav();
                                    old_date = this.value;
                                }
                                    //$('calbut_ConnectionAuditEstateToDate').value = old_date;
                            }
                        }
                   }).bind(this)
               }).send();   
           }
           else{
             call_add_stav();
             old_date = this.value;               
           }  
        }                  
    });    
    
    function call_add_stav(){
        if(old_date == '0000-00-00'){//pouze pro prvni vraceni, jinak vubec
            domwin.newWindow({
        		id			: 'domwin_stav',
        		sizes		: [500,400],
        		scrollbars	: true,
        		title		: 'Přidání stavu majetku',
        		languages	: false,
        		type		: 'AJAX',
        		ajax_url	: '/audit_estates/add_stav/' + $('ConnectionAuditEstateAuditEstateId').value+'/1/',
        		closeConfirm: true,
        		max_minBtn	: false,
        		modal_close	: false,
                closeBtn:false
        	}); 
        }
    } 
      
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		    button_preloader($('save_close'));  
			new Request.JSON({
				url:$('setting_stav_edit_formular').action,		
				onComplete:function(json){
				    if(json){
				        if(json.result == true){
        					domwin.loadContent('domwin'); 
        					domwin.closeWindow('domwin_edit_estate');
                        }
                        else {
                            alert('Chyba pri ukladani, nejspise jste nezvolili osobu');
                        }
                    }
                    else 
                        alert('Chyba aplikace'); 
                 
                 button_preloader_disable($('save_close'));       
				}
			}).post($('setting_stav_edit_formular'));
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_edit_estate');});

</script>