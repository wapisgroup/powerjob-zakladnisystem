<table class='table' id='recruiter_table'>
	<tr>
		<th>Firma</th>
        <th>Osoba</th>
		<th>Pronájem od</th>
        <th>Pronájem do</th>
        <th>Cena pronájmu</th>
        <th>Soukromé účely</th>
        <th>Fa</th>
		<th></th>
	</tr>
	<?php if (isset($connections) && count($connections) > 0):?>
		<?php foreach($connections as $item):?>
            <?php
                $jednodenni = ($fastest->czechDate($item['ConnectionAuditEstate']['created']) == $fastest->czechDate($item['ConnectionAuditEstate']['to_date']))?true:false;
            ?>
		<tr>
			<td><?= $item['0']['spolecnost'];?></td>
            <td><?= $item['0']['klient'];?></td>
			<td><?php echo $fastest->czechDate($item['ConnectionAuditEstate']['created']);?></td>
            <td><?php echo $fastest->czechDate($item['ConnectionAuditEstate']['to_date']);?></td>
            <td class='change_price'><?= ($item['ConnectionAuditEstate']['fa'] == 1)?round($item['AuditEstate']['hire_price']/date('t',strtotime($item['ConnectionAuditEstate']['to_date'])),2):$item[0]['finally_price'];?></td>
            <td><?php echo $fastest->value_to_yes_no($item['ConnectionAuditEstate']['private_use']);?></td>
            <td>
                <?php if ($jednodenni === true):?>

                <?= $htmlExt->checkbox('fa/'.$item['ConnectionAuditEstate']['id'], null, array('checked'=>($item['ConnectionAuditEstate']['fa'] == 1)?'checked':null,'class'=>'fa_change','title'=>$item['ConnectionAuditEstate']['id']));?>
			    <?php endif;?>
            </td>
			<td>
                <a href="/audit_estates/edit_estate_datum/<?= $item['ConnectionAuditEstate']['id'];?>" class="ta edit">Edit</a>
                <?php if($logged_user['CmsGroup']['cms_group_superior_id'] == 1){?>
                <a href="/audit_estates/delete_connection_estate/<?= $item['ConnectionAuditEstate']['id'];?>/<?= $id;?>" class="ta trash">Odebrat</a>
                <?php } ?>
            </td>
            
		</tr>
		<?php endforeach;?>
	<?php endif;?>
</table> 
<script>

    $$('.fa_change').addEvent('change', function(){
        var val, priceEl = this.getParent('tr').getElement('.change_price');
        if (this.checked){
            val = 1;
        } else {
            val = 0;
        }


        new Request.JSON({
            url: '/audit_estates/change_fa/' + val + '/' + this.getProperty('title'),
            onComplete: function(json){
                if (json){
                    if (json.result === true){
                        alert('Změna byla zaznamenána');
                        priceEl.set('html',json.price);
                    } else {
                        alert(json.message);
                    }
                } else {
                    alert('Chyba aplikace');
                }
            }
        }).send();
    })

	$('recruiter_table').getElements('.trash').addEvent('click',function(e){
	   e.stop();
		if(confirm('Smazat tuto historii??')){
		  new Request.JSON({
				url:this.href,		
				onComplete:(function(json){
				    if(json){
				        if(json.result == true){
        					this.getParent('tr').dispose();
                        }
                        else {
                            alert('Chyba při ukládaní, zkuste to později.');
                        }
                    }
                    else 
                        alert('Chyba aplikace'); 
                  
				}).bind(this)
			}).send();
		}
	});
    
    $('recruiter_table').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_edit_estate',
			sizes		: [550,370],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
	
</script>