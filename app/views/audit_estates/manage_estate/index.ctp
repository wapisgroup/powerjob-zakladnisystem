<div class="win_fce top">
<a href='/audit_estates/add_estate/<?php echo $id;?>' class='button edit' id='recruiter_add_new' title='Přidání'>Přidat nového nájemce</a>
</div>
<table class='table' id='recruiter_table'>
	<tr>
		<th>Firma</th>
        <th>Osoba</th>
		<th>Pronájem od</th>
	</tr>
	<?php if (isset($connections) && count($connections) > 0):?>
		<?php foreach($connections as $item):?>
		<tr>
			<td><?= $item['0']['spolecnost'];?></td>
            <td><?= $item['0']['klient'];?></td>
			<td><?php echo $fastest->czechDate($item['ConnectionAuditEstate']['created']);?></td>
		</tr>
		<?php endforeach;?>
	<?php endif;?>
</table> 
<script>	
	$('recruiter_add_new').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_add_estate',
			sizes		: [580,500],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>