<form action='/audit_estates/add_estate/' method='post' id='setting_stav_edit_formular'>
	<?php echo $htmlExt->hidden('ConnectionAuditEstate/audit_estate_id');?>
	<?php echo $htmlExt->hidden('ConnectionAuditEstate/connection_client_at_company_work_position_id');?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Přiřadit majetek</legend>
                <?php echo $htmlExt->radio('ConnectionAuditEstate/type', array('1' => 'Majetek pro','2' => 'Majetek do pronájmu jine firmy/osoby'), ' ', array('class' => 'radio')); ?><br />
                
				<div class="sll">  
					<?php echo '<div>'.$htmlExt->selectTag('ConnectionAuditEstate/at_company_id',array('Vyberte osobu'),null,array('rel'=>1,'tabindex'=>1,'label'=>'Firma'),null,false).'<br /></div>';?>             		        
					<?php echo '<div>'.$htmlExt->selectTag('ConnectionAuditEstate/company_foreign_id',array('-- Vyberte společnost --')+$company_foreign_list,null,array('rel'=>2,'tabindex'=>1,'label'=>'Firma'),null,false).'<br /></div>';?>             		        
		
                    <?php echo $htmlExt->inputDate('ConnectionAuditEstate/created',array('tabindex'=>3,'label'=>'Datum přidělení','class'=>'no_reset','value'=>date('Y-m-d')));?> <br class="clear">               		        
            	</div>
				<div class="slr">  
				   <?php echo '<div>'.$htmlExt->selectTag('ConnectionAuditEstate/client_id',$person_list,null,array('rel'=>1,'tabindex'=>2,'label'=>'Osoba'),array('title'=>(array('0') + $person_list_title))).'<br /></div>';?>           		        
				   <?php echo '<div>'.$htmlExt->selectTag('ConnectionAuditEstate/client_foreign_id',array('-- Vyberte osobu --')+$client_foreign_list,null,array('rel'=>2,'tabindex'=>2,'label'=>'Osoba'),null,false).'<br /></div>';?>             		        
				   
                   <?php echo '<div>'.$htmlExt->input('ConnectionAuditEstate/centre',array('rel'=>1,'tabindex'=>4,'label'=>'Středisko','readonly'=>'readonly')).'<br /></div>';?>               		        
                   <?php echo '<div>'.$htmlExt->input('ConnectionAuditEstate/cinnost_id',array('rel'=>1,'tabindex'=>4,'label'=>'ČinnostId','readonly'=>'readonly')).'<br /></div>';?>
             	</div>
                <br />
                <?php echo $htmlExt->checkbox('ConnectionAuditEstate/private_use',null,array('class'=>'no_reset','tabindex'=>5,'label'=>'Majetek použi pro soukromé účely'));?> <br class="clear">               		        
                <?php echo $htmlExt->textarea('ConnectionAuditEstate/description',array('class'=>'no_reset','tabindex'=>6,'label'=>'Doplňujicí informace'));?> <br class="clear">               		        
                <label class=''>Počet možných znaků:</label><var class='' id='sms_len'>100</var><br />

			</fieldset>
            <fieldset class="">
				<legend>Info o majetku</legend>
                <?php echo $htmlExt->var_text('AuditEstate/name',array('tabindex'=>1,'label'=>'Název','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
				<?php echo $htmlExt->var_text('AuditEstate/imei_sn_vin',array('tabindex'=>2,'label'=>'IMEI/SN/VIN','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
			    <?php echo $htmlExt->var_text('AuditEstate/internal_number',array('tabindex'=>3,'label'=>'Interní číslo','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
                <?php echo $htmlExt->var_text('AuditEstate/at_company_id',array('show_type'=>'list','list'=>$company_list,'tabindex'=>2,'label'=>'V majetku firmy','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
                <?php echo $htmlExt->var_text('AuditEstate/at_project_centre_id',array('show_type'=>'list','list'=>$centre_list,'tabindex'=>2,'label'=>'Středisko','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
                <?php echo $htmlExt->var_text('AuditEstate/payment_date',array('show_type'=>'date','tabindex'=>3,'label'=>'Datum pořizení','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
               	<?php echo $htmlExt->var_text('AuditEstate/first_cost',array('tabindex'=>4,'class'=>'integer long50','label'=>'Pořizovací cena bez DPH','label_class'=>'long50'));?> <br class="clear">
			   	<?php echo $htmlExt->var_text('AuditEstate/accessories',array('tabindex'=>4,'class'=>'long50','label'=>'Příslušenství','label_class'=>'long50'));?> <br class="clear">
			   	<?php echo $htmlExt->var_text('AuditEstate/naklady_na_opravy',array('tabindex'=>6,'class'=>'integer long50','label'=>'Náklady na opravy','label_class'=>'long50','disabled'=>'disabled'));?> <br class="clear">
                <?php echo $htmlExt->var_text('AuditEstate/hire_price',array('tabindex'=>5,'class'=>'integer long50','label'=>'Měsiční cena pronájmu','label_class'=>'long50'));?> <br class="clear">
			   	<?php echo $htmlExt->var_text('AuditEstate/odpisovatelny',array('show_type'=>'date','tabindex'=>6,'class'=>'','label'=>'Odpisovatelný','label_class'=>'long50'));?> <br class="clear">
			    <?php echo $htmlExt->var_text('AuditEstate/currency',array('show_type'=>'list','list'=>$currency_list,'tabindex'=>2,'label'=>'Měna','class'=>'long50','label_class'=>'long50'),null,false);?> <br class="clear">
               
               	<?php echo $htmlExt->var_text('AuditEstate/elimination_date',array('show_type'=>'date','tabindex'=>7,'label'=>'Datum vyřazení','class'=>'long50','label_class'=>'long50','disabled'=>'disabled'));?> <br class="clear">
			</fieldset>
		</div>
	</div>
	<div class="win_save">
        <?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'});
    var typ_1 = $('ConnectionAuditEstateType_1');
    var typ_2 = $('ConnectionAuditEstateType_2');
    var spol = 'ConnectionAuditEstateCompanyForeignId';
    var osoba = 'ConnectionAuditEstateClientForeignId';
    
    if($('ConnectionAuditEstateDescription'))
        $('sms_len').setHTML(100 - $('ConnectionAuditEstateDescription').value.length);

    if($('ConnectionAuditEstateDescription'))
	$('ConnectionAuditEstateDescription').addEvents({
		'keyup': function(){
		
			$('sms_len').setHTML(100 - this.value.length);
			if (this.value.length >= 100){
			    $('sms_len').setHTML(0); 
				this.value = this.value.substring(0,100);
            }    
		},
        'keypress':function(e){
            var event = new Event(e);
            if(event.code == 38)
				return false;
        }
	});
    
    typ_1.setAttribute('checked','checked');
    
     function change_view_for_type(reset){
        if(reset == null){  reset = true; }
        
        $each($('setting_stav_edit_formular').getElements('select,input,textarea'),function(item){
           if(item.hasAttribute('rel')){
              if(item.getProperty('rel') == 1 && typ_1.checked){
                 item.getParent('div').removeClass('none');
                 $('ConnectionAuditEstatePrivateUse').removeAttribute('checked');
                 $('ConnectionAuditEstatePrivateUse').removeAttribute('disabled');
              }
              else if(item.getProperty('rel') == 2 && typ_2.checked){
                 item.getParent('div').removeClass('none');
                 $('ConnectionAuditEstatePrivateUse').setAttribute('disabled','disabled');
                 $('ConnectionAuditEstatePrivateUse').setAttribute('checked','checked');
              }
              else
                 item.getParent('div').addClass('none');        
           }
           
           if(reset != false 
                && item.getProperty('type') != 'button' 
                && item.getProperty('type') != 'hidden' 
                && item.getProperty('type') != 'radio' 
                && item.hasClass('no_reset') === false
           ){
                item.value = ''; 
           }
        })
    }
    
    change_view_for_type(false);//default
    typ_1.addEvent('click',change_view_for_type);
    typ_2.addEvent('click',change_view_for_type);
    
    function unset_another_select(e){
        var selected = e.target;
        if(selected.getProperty('id') == spol){$(osoba).value = 0; }
        else{ $(spol).value = 0;}
    }
    $(spol).addEvent('change',unset_another_select);
    $(osoba).addEvent('change',unset_another_select);
    
    if($('ConnectionAuditEstateClientId')){
	   $('ConnectionAuditEstateClientId').addEvent('change',function(){
	       //ukladani connection zamestani k pronajmu
	       $('ConnectionAuditEstateConnectionClientAtCompanyWorkPositionId').value = this.options[this.selectedIndex].title;
           new Request.JSON({
				url:'/audit_estates/load_ajax_list/'+this.options[this.selectedIndex].title,		
				onComplete:function(json){
				      if(json){
				        if(json.AtCompany){
				            obj = 'ConnectionAuditEstateAtCompanyId';
                            $(obj).empty();
                        	new Element('option', {value:json.AtCompany.id}).setHTML(json.AtCompany.name).inject($(obj));
				        }
                        if(json.AtProjectCentre){
                            $('ConnectionAuditEstateCentre').value = json.AtProjectCentre;
                        }

                        $('ConnectionAuditEstateCinnostId').value = json.cinnost_id;
				    }
				}.bind(this)  
	       }).send();    
       
       });
 	}
        
    $('setting_stav_edit_formular').getElements('.integer').inputLimit();
      
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
        result = true;
        if(typ_1.checked && $('ConnectionAuditEstateAtCompanyId').value == 0)
            result = 'Vyberte osobu a zvolí se automaticky firma';
        if(typ_2.checked && $('ConnectionAuditEstateCompanyForeignId').value == 0 && $('ConnectionAuditEstateClientForeignId').value == 0)
            result = 'Pro soukromé účely musíte zvolit firmu nebo osobu které bude majetek přiřazen';    
        
        $('ConnectionAuditEstatePrivateUse').removeAttribute('disabled');
		if (result === true){
		    button_preloader($('save_close'));  
			new Request.JSON({
				url:$('setting_stav_edit_formular').action,		
				onComplete:function(json){
				    if(json){
				        if(json.result == true){
        					domwin.loadContent('domwin'); 
        					domwin.closeWindow('domwin_add_estate');
                        }
                        else {
                            alert('Chyba pri ukladani, nejspise jste nezvolili osobu');
                        }
                    }
                    else 
                        alert('Chyba aplikace'); 
                 
                 button_preloader_disable($('save_close'));       
				}
			}).post($('setting_stav_edit_formular'));
		} else {
			alert(result);
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_add_estate');});

</script>