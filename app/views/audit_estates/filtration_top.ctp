<?php
if(isset($filtration_sum_variables)){      
    $filtration_sum_variables = Set::combine($filtration_sum_variables,'{n}.AuditEstate.currency','{n}.0');
    if(!isset($filtration_sum_variables[1]))
        $filtration_sum_variables[1] = array('pronajem'=>0,'cena'=>0);
        
    if(!isset($filtration_sum_variables[0]))
        $filtration_sum_variables[0] = array('pronajem'=>0,'cena'=>0);    
    ?>
    <div id="filtration_variables">
        <div class="sll">
            Pořizovací: <strong><?= $fastest->price($filtration_sum_variables[1]['cena'],',- CZK');?></strong><br />
            Pronájem: <strong><?= $fastest->price($filtration_sum_variables[1]['pronajem'],',- CZK');?></strong><br />
        </div>
        <div class="slr">
            Pořizovací: <strong><?= $fastest->price($filtration_sum_variables[0]['cena'],',- EUR',null,2);?></strong><br />
            Pronájem: <strong><?= $fastest->price($filtration_sum_variables[0]['pronajem'],',- EUR',null,2);?></strong><br />
        </div>
    </div>
<?php 
}
?>