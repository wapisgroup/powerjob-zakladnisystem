<form action='/audit_estates/remove_estate/' method='post' id='report_down_payments_uhrazeno'>
	<?php echo $htmlExt->hidden('ConnectionAuditEstate/id');?>
    <?php echo $htmlExt->hidden('ConnectionAuditEstate/private_use');?>
	<?php echo $htmlExt->hidden('AuditEstate/id');?>
		<fieldset>
				<legend>Základní</legend>
				<div class="sll100">  
                    <?php echo $htmlExt->inputDate('ConnectionAuditEstate/to_date',array('tabindex'=>3,'label'=>'Odevzdat ke dni','value'=>date('Y-m-d')));?> <br class="clear">
				</div>
		</fieldset>
	
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>6));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close2','tabindex'=>7));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	$('close2').addEvent('click',function(e){
	   new Event(e).stop(); 
       domwin.closeWindow('domwin_remove_estate');
    });


    if($('save_close'))
    	$('save_close').addEvent('click',function(e){
    		new Event(e).stop();
    		
    		valid_result = validation.valideForm('report_down_payments_uhrazeno');
    		if (valid_result == true){
    			new Request.JSON({
    				url:$('report_down_payments_uhrazeno').action,		
    				onComplete:function(json){
    				    if(json){
    				        if(json.result == true){
            				    domwin.loadContent('domwin'); 
            					domwin.closeWindow('domwin_remove_estate');
                            }
                            else
                                alert('Chyba při ukládání.');
                        }
                        else
                            alert('Chyba apikace');            
    				}
    			}).post($('report_down_payments_uhrazeno'));
    		} else {
    			var error_message = new MyAlert();
    			error_message.show(valid_result)
    		}
    	});
	
</script>