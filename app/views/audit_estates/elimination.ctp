<form action='/audit_estate_elimination_requests/elimination/' method='post' id='report_down_payments_uhrazeno'>
	<?php echo $htmlExt->hidden('AuditEstate/id');?>


		<fieldset>
				<legend>Základní</legend>
				<div class="sll100">  
                    <?php echo $htmlExt->inputDate('AuditEstate/elimination_date',array('tabindex'=>1,'label'=>'Vyřazeno dne','value'=>date('Y-m-d')));?> <br class="clear">
				</div>
		</fieldset>


	
	<div class="win_save">
		<?php echo $htmlExt->button('Vyřadit',array('id'=>'e_save_close','tabindex'=>2));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'e_close','tabindex'=>3));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">



    if($('e_save_close'))
    	$('e_save_close').addEvent('click',function(e){
    		new Event(e).stop();

    		valid_result = validation.valideForm('report_down_payments_uhrazeno');
    		if (valid_result == true){
    			new Request.JSON({
    				url:$('report_down_payments_uhrazeno').action,		
    				onComplete:function(){
    				    click_refresh();
    					domwin.closeWindow('domwin');
    				}
    			}).post($('report_down_payments_uhrazeno'));
    		} else {
    			var error_message = new MyAlert();
    			error_message.show(valid_result)
    		}
    	});
	
	$('e_close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
</script>