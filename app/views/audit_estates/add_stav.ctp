<form action='/audit_estates/add_stav/' method='post' id='estate_repair_add_form'>
	<?php echo $htmlExt->hidden('AuditEstateStav/audit_estate_id');?>
	
	<div class="domtabs admin_dom_repair_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom_repair">
		<div class="domtabs field">
			<fieldset>
				<legend>Přidání stavu</legend>

					<?php 
                    unset($audit_estate_type_list[0]);
                    echo $htmlExt->selectTag('AuditEstateStav/type',$audit_estate_type_list,null,array('tabindex'=>2,'label'=>'Stav','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
                    <?php echo $htmlExt->inputDate('AuditEstateStav/date_stav',array('tabindex'=>2,'label'=>'Datum','value'=>date('Y-m-d')));?> <br class="clear">               		        
		            <?php echo $htmlExt->textarea('AuditEstateStav/description',array('tabindex'=>1,'label'=>'Popis'));?> <br class="clear">               		        
					  
           </fieldset>
		</div>
	</div>
	<div class="win_save">
        <?php echo $htmlExt->button('Uložit',array('id'=>'save_repair','tabindex'=>3));?>
		<?php 
        if($in_return_estate == false)
            echo $htmlExt->button('Zavřít',array('id'=>'close_repair','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom_repair'});
    
    $('estate_repair_add_form').getElements('.integer').inputLimit();
    
    function load_content_param(){ domtab.goTo(3); }

	$('save_repair').addEvent('click',function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('estate_repair_add_form');
		if (valid_result == true){
		     button_preloader($('save_repair'));  
			new Request.JSON({
				url:$('estate_repair_add_form').action,		
				onComplete:function(json){
				    if(json){
				        if(json.result == true){
        					domwin.loadContent('domwin',{'onComplete': 'load_content_param'}); 
        					domwin.closeWindow('domwin_stav');
                        }
                        else {
                            alert('Chyba pri ukladani, nejspise jste nezvolili osobu');
                        }
                    }
                    else 
                        alert('Chyba aplikace'); 
                 
                 button_preloader_disable($('save_repair'));       
				}
			}).post($('estate_repair_add_form'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
    if($('close_repair'))
	   $('close_repair').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_stav');});
    validation.define('estate_repair_add_form',{
	   'AuditEstateStavType': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit stav'},
		}
	});
	validation.generate('estate_repair_add_form',false);

</script>