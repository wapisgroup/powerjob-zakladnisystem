<?php
/**
 * Funkce rpo zjisteni první ho datumu vraceni a posledniho datumu vraceni
 * @abstract zatim se nepouziva! (melo by se pouzit pro spravce majetku)
 */
function get_data_from_history($history_list){
    $data = array(
        'first_end'=>-1,
        'last_end'=>-1,
    );
    
    foreach($history_list as $item){
        if($item['ConnectionAuditEstate']['to_date'] == '0000-00-00')
            $data['last_end'] = false;//nejaky aktivni
        //else if($item['ConnectionAuditEstate']['to_date'] != '0000-00-00' && $item['ConnectionAuditEstate']['to_date'] && )
    }
    
    return $data;
}

?>
<form action='/audit_estates/edit/' method='post' id='setting_stav_edit_formular'>
	<?php echo $htmlExt->hidden('AuditEstate/id');?>
    <?php echo $htmlExt->hidden('AuditEstate/at_project_id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
            <?php if (isset($this->data['AuditEstate']['id'])):?>
			<li class="ousko"><a href="#krok1">Historie</a></li>
            <li class="ousko"><a href="#krok3">Opravy</a></li>
            <li class="ousko"><a href="#krok4">Stav</a></li>
             <?php endif;?>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Nastavení</legend>
                <?php echo $htmlExt->input('AuditEstate/name',array('tabindex'=>1,'label'=>'Název','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
				<?php echo $htmlExt->input('AuditEstate/imei_sn_vin',array('tabindex'=>2,'label'=>'IMEI/SN/VIN','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
			    <?php echo $htmlExt->input('AuditEstate/internal_number',array('tabindex'=>3,'label'=>'Interní číslo','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
			     
                <?php echo $htmlExt->selectTag('AuditEstate/at_company_id',$company_list,null,array('tabindex'=>2,'label'=>'V majetku firmy','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
                 <?php echo $htmlExt->selectTag('AuditEstate/at_project_centre_id',$centre_list,null,array('tabindex'=>2,'label'=>'Středisko','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
               
                <?php echo $htmlExt->inputDate('AuditEstate/payment_date',array('tabindex'=>3,'label'=>'Datum pořizení','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
               	<?php echo $htmlExt->input('AuditEstate/first_cost',array('tabindex'=>4,'class'=>'integer long50','label'=>'Pořizovací cena bez DPH','label_class'=>'long50'));?> <br class="clear">
			   	<?php echo $htmlExt->input('AuditEstate/accessories',array('tabindex'=>4,'class'=>'long50','label'=>'Příslušenství','label_class'=>'long50'));?> <br class="clear">
			   	
                <?php echo $htmlExt->input('AuditEstate/hire_price',array('tabindex'=>5,'class'=>'integer long50','label'=>'Měsiční cena pronájmu','label_class'=>'long50'));?> <br class="clear">
			   	<?php echo $htmlExt->checkbox('AuditEstate/odpisovatelny',null,array('tabindex'=>6,'class'=>'','label'=>'Odpisovatelný','label_class'=>'long50'));?> <br class="clear">
			    <?php echo $htmlExt->selectTag('AuditEstate/currency',$currency_list,null,array('tabindex'=>2,'label'=>'Měna','class'=>'long50','label_class'=>'long50'),null,false);?> <br class="clear">
                <?php echo $htmlExt->selectTag('AuditEstate/audit_estate_type_id',$ae_types_list,null,array('tabindex'=>7,'label'=>'Typ majetku','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
                <div id="type_box" class="none">
                    <?php echo $htmlExt->input('AuditEstate/registration_car_number',array('tabindex'=>8,'label'=>'Evid. číslo vozidla','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
    				<?php echo $htmlExt->input('AuditEstate/shell_card_number',array('tabindex'=>9,'label'=>'Číslo shell karty','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
    			</div>
                <div id="pin_box" class="none">
                    <?php echo $htmlExt->input('AuditEstate/shell_card_pin',array('tabindex'=>10,'label'=>'Pin shell karty','class'=>'long50','label_class'=>'long50'));?> <br class="clear">	
                </div>
               	<?php echo $htmlExt->inputDate('AuditEstate/elimination_date',array('tabindex'=>11,'label'=>'Datum vyřazení','class'=>'long50','label_class'=>'long50','disabled'=>'disabled'));?> <br class="clear">
			    <?php 
                $class = 'none';
                if($this->data['AuditEstate']['id'] == '' || $this->data['AuditEstate']['stav'] == 0 )
			          $class = '';
                 ?>
                <div id="add_info" class="<?= $class?>"> 
                    <?php echo $htmlExt->textarea('AuditEstate/add_info',array('class'=>'no_reset','tabindex'=>6,'label'=>'Doplňujicí informace'));?> <br class="clear">               		        
                    <label class=''>Počet možných znaků:</label><var class='' id='sms_len'>100</var><br />
                </div>
            </fieldset>
		</div>
        <?php if (isset($this->data['AuditEstate']['id'])):?>
          <div class="domtabs field" id="obal_tabulky">
          <fieldset>
				<legend>Historie zapujčení majetku</legend>
                <p>Správce majetku v čase bez pronájmu: <strong><?= $this->data['SpravceMajetku']['name'];?></strong></p>
			<table class='table' id='rating_table'>
					<tr>
						<th>Firma</th>
						<th>Osoba</th>
						<th>Pronájem od</th>
						<th>Vráceno</th>
                        <th>Soukromé účely</th>
					</tr>
					<?php 
					if (isset($history_list) && count($history_list) > 0){
						foreach($history_list as $item):
						?>
						<tr>
							<td><?php echo $item[0]['spolecnost'];?></td>
							<td><?php echo $item[0]['klient'];?></td>
							<td><?php echo $fastest->czechDate($item['ConnectionAuditEstate']['created']);?></td>
							<td><?php echo $fastest->czechDate($item['ConnectionAuditEstate']['to_date']);?></td>
						    <td><?php echo $fastest->value_to_yes_no($item['ConnectionAuditEstate']['private_use']);?></td>
						   
                        </tr>
					<?php 
						endforeach;
					}
					else echo "<tr id='NoneSearch'><td></td>Žádná historie</td></tr>"; 
					?>
			</table>
            </fieldset>
            <br />
            <fieldset>
				<legend>Email Log</legend>
                <table class='table' id='rating_table'>
					<tr>
						<th>Typ emailu</th>
						<th>Text</th>
						<th>Datum</th>
					</tr>
					<?php 
					if (isset($mail_list) && count($mail_list) > 0){
						foreach($mail_list as $item):
						?>
						<tr>
							<td><?php echo $item['MailTemplate']['name'];?></td>
							<td><?php echo $item['AuditEstateLog']['text'];?></td>
							<td><?php echo $fastest->czechDate($item['AuditEstateLog']['created']);?></td>
						</tr>
					<?php 
						endforeach;
					}
					else echo "<tr id='NoneSearch'><td></td>Žádná historie</td></tr>"; 
					?>
			</table>
            </fieldset>    
       </div> 
       
        <div class="domtabs field">
            <?php echo $htmlExt->input('AuditEstate/naklady_na_opravy',array('tabindex'=>6,'class'=>'integer long50','label'=>'Náklady na opravy','label_class'=>'long50','disabled'=>'disabled'));?> <br class="clear">
               
			<table class='table'>
					<tr>
						<th>Datum</th>
                        <th>Název</th>
                        <th>Cena</th>
					</tr>
					<?php 
					if (isset($repairs_list) && count($repairs_list) > 0){
						foreach($repairs_list as $item):
                     
						?>
						<tr>
							<td><?php echo $fastest->czechDate($item['AuditEstateRepair']['date_repair']);?></td>
							<td><?php echo $item['AuditEstateRepair']['name'];?></td>
							<td><?php echo $fastest->price($item['AuditEstateRepair']['price'],($item['AuditEstateRepair']['currency'] == 1?',- Kč':',- EUR'));?></td>
						</tr>
					<?php 
						endforeach;
					}
					else echo "<tr id='NoneSearch'><td></td>Žádné opravy</td></tr>"; 
					?>
			</table>
            <div class="win_save">
                <?php echo $htmlExt->button('Přidat opravu',array('id'=>'add_repair','tabindex'=>10));?>
            </div>
       </div> 
       <div class="domtabs field">
            <table class='table' id="stav_list">
					<tr>
						<th>Datum</th>
                        <th>Stav</th>
                        <th>Popis</th>
                        <th>Možnosti</th>
					</tr>
					<?php 
					if (isset($stavs_list) && count($stavs_list) > 0){
						foreach($stavs_list as $item):
                        
						?>
						<tr>
							<td><?php echo $fastest->czechDate($item['AuditEstateStav']['date_stav']);?></td>
							<td><?php echo $audit_estate_type_list[$item['AuditEstateStav']['type']];?></td>
                            <td><?php echo $fastest->orez($item['AuditEstateStav']['description'],30);?></td>
                            <td>
                             <?php
                             if(isset($permission['delete_stav']) &&$permission['delete_stav'] == 1)
                               echo $html->link('Smazat','/audit_estates/delete_stav/'.$item['AuditEstateStav']['id'],array('class'=>'ta trash'));?></td>
						</tr>
					<?php 
						endforeach;
					}
					else echo "<tr id='NoneSearch'><td>Žádné stavy</td></tr>"; 
					?>
			</table>
            <div class="win_save">
                <?php echo $htmlExt->button('Přidat stav',array('id'=>'add_stav','tabindex'=>10));?>
            </div>
       </div>
        <?php endif;?>
	</div>
	<div class="win_save">
		<?php 
        if(isset($permission['allow_save']) && $permission['allow_save'] == 1){
             echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>8));
        }
        ?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>9));?>
        <?php if (!empty($this->data['AuditEstate']['id']) && $this->data['AuditEstate']['elimination_date'] != '0000-00-00' and $this->data['AuditEstate']['stav'] == 0 and $this->data['AuditEstate']['elimination_request'] == 0):?>
		<?php     echo $htmlExt->button('Žádost o vyřazení majetku',array('id'=>'eliminate','tabindex'=>10));?>
        <?php endif;?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'});
    var permission_to_view_pin = '<?php echo (isset($permission['show_pin'])?$permission['show_pin']:false);?>';

    /**
     * Generovani interniho cisla
     * AAAABBBBCCCCDDDD
     * A - id projektu
     * B - id firmy
     * C - id strediska
     * D - id majetku
     */
    function strpad(inputString, chars, padSting) {
      result = padSting+inputString;
      remFromLeft=result.length-chars;
      return result.substr(remFromLeft).toString();
    } 
    function generate_internal_number(){
        var project_id = $('AuditEstateAtProjectId').value;
        var firma_id = $('AuditEstateAtCompanyId').value;
        var stredisko_id = $('AuditEstateAtProjectCentreId').value;
        var majetek_id = $('AuditEstateId').value;
        
        $('AuditEstateInternalNumber').value = strpad(project_id,4,'0000');
            $('AuditEstateInternalNumber').value += strpad(firma_id,4,'0000');
            $('AuditEstateInternalNumber').value += strpad(stredisko_id,4,'0000');
            $('AuditEstateInternalNumber').value += strpad(majetek_id,4,'0000');
    }
    if($('AuditEstateId').value != '' && $('AuditEstateInternalNumber').value == ''){generate_internal_number();}
    $('AuditEstateAtCompanyId').addEvent('change',generate_internal_number);
    $('AuditEstateAtProjectCentreId').addEvent('change',generate_internal_number);
    //END generate internal number
    
    $('AuditEstateAtCompanyId').ajaxLoad('/audit_estates/load_at_project_centre_list/',['AuditEstateAtProjectCentreId']);
    $('AuditEstateAtCompanyId').addEvent('change',function(e){
        if(this.value != '')
         	new Request.JSON({
				url:'/at_companies/check_stat_id/'+this.value,		
				onComplete:function(json){
					if(json){
					   if(json.stat_id){
					       if(json.stat_id == null)
                             alert('Firma nemá zvolen stát, nebyla zvolena měna.')
                           else
					         $('AuditEstateCurrency').value = (json.stat_id == '1'?1:0);
					   }
					}
				}
			}).send();
    });
    
    $('AuditEstateAuditEstateTypeId').addEvent('change',check_box_for_car);
    function check_box_for_car(){
        typ = $('AuditEstateAuditEstateTypeId').value;
        if(typ == 1){
            $('type_box').removeClass('none');
            if($('AuditEstateId').value == '' || permission_to_view_pin == true)
                $('pin_box').removeClass('none');
        }
        else{
            $('type_box').addClass('none');
            $('pin_box').addClass('none');
        }
        
    }
    check_box_for_car();
    
    //generovani project_id - podle strediska
    $('AuditEstateAtProjectCentreId').addEvent('change',function(e){
        if(this.value != ''){
         	new Request.JSON({
				url:'/at_projects/get_project_id/'+this.value,		
				onComplete:function(json){
					if(json){
					   if(json.at_project_id){
					         $('AuditEstateAtProjectId').value = json.at_project_id;
                             generate_internal_number();
					   }
					}
				}
			}).send();
        }
        else {$('AuditEstateAtProjectId').value = '0';generate_internal_number();}     
    });
    
    
    
    if ($('eliminate'))
        $('eliminate').addEvent('click', function(e){
                e.stop();
        		domwin.newWindow({
        			id			: 'domwin_repair',
        			sizes		: [500,400],
        			scrollbars	: true,
        			title		: 'Žádost o vyřazení majetku',
        			languages	: false,
        			type		: 'AJAX',
        			ajax_url	: '/audit_estates/elimination/' + $('AuditEstateId').value,
        			closeConfirm: true,
        			max_minBtn	: false,
        			modal_close	: false
    	});    
    });
    if ($('add_repair'))
        $('add_repair').addEvent('click', function(e){
            e.stop();
    		domwin.newWindow({
    			id			: 'domwin_repair',
    			sizes		: [500,400],
    			scrollbars	: true,
    			title		: 'Přidání opravy majetku',
    			languages	: false,
    			type		: 'AJAX',
    			ajax_url	: '/audit_estates/add_repair/' + $('AuditEstateId').value,
    			closeConfirm: true,
    			max_minBtn	: false,
    			modal_close	: false
    	}); 
    })
    
    if ($('add_stav'))
        $('add_stav').addEvent('click', function(e){
            e.stop();
    		domwin.newWindow({
    			id			: 'domwin_stav',
    			sizes		: [500,400],
    			scrollbars	: true,
    			title		: 'Přidání stavu majetku',
    			languages	: false,
    			type		: 'AJAX',
    			ajax_url	: '/audit_estates/add_stav/' + $('AuditEstateId').value,
    			closeConfirm: true,
    			max_minBtn	: false,
    			modal_close	: false
    	}); 
    })
    
    function load_content_param(){ domtab.goTo(3); }

    if($('stav_list')){
    	$('stav_list').getElements('.trash').addEvent('click',function(e){
    		new Event(e).stop();
    
    		if (confirm('Opravdu si přejete smazat tento stav?')){
    			new Request.JSON({
    				url:this.href,		
    				onComplete:(function(json){
    				    if(json){
    				        if(json.result == true){
            					this.getParent('tr').dispose();
                            }
                            else {
                                alert('Chyba pri mazani, zkuste to pozdeji');
                            }
                        }
                        else 
                            alert('Chyba aplikace');   
    				}).bind(this)
    			}).send();
    		} 
    	});
    }
    
    $('setting_stav_edit_formular').getElements('.integer').inputLimit();
    
    if($('save_close')) 
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('setting_stav_edit_formular');
		if (valid_result == true){
            button_preloader($('save_close'));
			new Request.JSON({
				url:$('setting_stav_edit_formular').action,		
				onComplete:function(){
                    button_preloader_disable($('save_close'));
					click_refresh($('AuditEstateId').value);
					domwin.closeWindow('domwin');
				}
			}).post($('setting_stav_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('setting_stav_edit_formular',{
	   'AuditEstateName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte název majetku'},
		},
        'AuditEstateImeiSnVin': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit IMEI/SN/VIN'},
            'isUnique':{'condition':{'model':'AuditEstate','field':'imei_sn_vin','id':$('AuditEstateId').value},'err_message':'Toto IMEI/SN/VIN je již použito'}
		},
        'AuditEstateAtCompanyId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit firmu'},
		},
        'AuditEstateAtProjectCentreId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit středisko'},
		},
        'calbut_AuditEstatePaymentDate': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit datum pořízení'},
		},
	});
	validation.generate('setting_stav_edit_formular',<?php echo (isset($this->data['AuditEstate']['id']))?'true':'false';?>);
    
    if($('AuditEstateAddInfo'))
        $('sms_len').setHTML(100 - $('AuditEstateAddInfo').value.length);

    if($('AuditEstateAddInfo'))
	$('AuditEstateAddInfo').addEvents({
		'keyup': function(){
		
			$('sms_len').setHTML(100 - this.value.length);
			if (this.value.length >= 100){
			    $('sms_len').setHTML(0); 
				this.value = this.value.substring(0,100);
            }    
		},
        'keypress':function(e){
            var event = new Event(e);
            if(event.code == 38)
				return false;
        }
	});
</script>