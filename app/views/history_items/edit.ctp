<form>
	<fieldset>
		<legend>Základní informace</legend>
		<div class='sll'>
			<?php echo $htmlExt->var_text('HistoryType/name',array('label'=>'Akce'))?><br/>
			<?php echo $htmlExt->var_text('Client/name',array('label'=>'Klient'))?><br/>
		</div>
		<div class='slr'>
			<?php echo $htmlExt->var_text('HistoryItem/name',array('label'=>'Kategorie','value'=>$history_category[$this->data['HistoryItem']['category_id']]))?><br/>
			<?php echo $htmlExt->var_text('CmsUser/name',array('label'=>'Uživatel'))?><br/>
		</div>
	</fieldset>
	<?php if (isset($this->data['HistoryData']) && count($this->data['HistoryData'])>0):?>
	<fieldset>
		<legend>Položky</legend>
		<?php foreach($this->data['HistoryData'] as $key=>$it):?>
			<label class='long'><?php echo $it['caption']?></label><var class='long <?php 
				if (isset($last_item['HistoryData'][$key]) && $it['value'] != $last_item['HistoryData'][$key]['value']) echo 'color_blue';?>'><?php echo $it['value']?></var><br/>
		<?php endforeach;?>
	</fieldset>
	<?php endif;?>
</form>