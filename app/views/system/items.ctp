<?php  
if (isset($pagination) && isset($paging)) 	$pagination->setPaging($paging); ?> 
<?php if(isset($filtration_sum_variables2)){ $pagination->ignored_params[] = 'chck';}?>
<div class="pagination_top"><?php echo $this->renderElement('pagination');?></div>
<?php if(isset($renderSetting['info_box'])){?>
<div style="position:absolute;left:20px; top:8px;"><?php if(isset($this->viewVars[$renderSetting['info_box']])) echo $this->viewVars[$renderSetting['info_box']];?></div>
<?php } 

/**
 * Specialni filtracni panel nahore
 * definujeme render element odkud ho vememe
 * abychom to neprali vsechno do items
 */
if(isset($renderSetting['sum_variables_on_top'])){
    echo $this->renderElement($renderSetting['sum_variables_on_top']['path']);
}  
?>

<table id='tabulka_item' class='table'>
<?php 
	echo '<thead><tr>';
	$th = $sum_rows = array();
	if (isset($renderSetting['items'])){
		if (!isset($renderSetting['no_checkbox'])) echo '<th class="no_print"><input type="checkbox" id="check_all"/></th>';
		foreach($renderSetting['items'] as &$item_setting){
			list($caption, $model, $col, $type, $fnc) = explode('|',$item_setting);
			//pr($col);
			$item_setting = compact(array("caption", "model","col","type","fnc"));	
			$th[] = $colth =  $pagination->sortByTh($col, $caption, $model);
			echo '<th'.(($colth[1] != null)?' class="'.$colth[1].' table_th_'.$col.'"':' class="table_th_'.$col.'"').'>'.(($type != 'hidden')?$colth[0]:'').'</th>'; 
			//echo '<th'.(($colth[1] != null)?' class="'.$colth[1].' table_th_'.$col.'"':' class="table_th_'.$col.'"').'><span><div class="table_header_over"></div></span>'.(($type != 'hidden')?$colth[0]:'').'</th>';
		}
		if (!empty($renderSetting['posibility'])) echo '<th class="no_print">Možnosti</th>';
	}
	echo '</tr></thead>';
	//pr($th);
	echo $viewIndex->generateColgroup($th);
	echo'<tbody>';
	$index = 0;
	$class_tr = $thunder_class = '';
	if (isset($items) && count($items)>0){
		foreach ($items as $num => $item):
            /**
             * pokud je nastavena podminka pro zobrazeni tridy pro jednotlivy radek
             */
            if(isset($renderSetting['class_tr']))
                $class_tr = $fastest->set_class_for_tr($renderSetting['class_tr'],$item);
            /**
             * Pokud je zvolena zobrazovaci komponenta thunderbird 
             */    
            if(isset($renderSetting['thunderbird']))
                $thunder_class = 'thunderbird';   
                
			echo '<tr class="'.(($index == 1)?'od ':'').$class_tr.' '.$thunder_class.'">';
				if (!isset($renderSetting['no_checkbox'])){ 
					$checkbox_id = $data = $disabled = $checked = null;
					if (isset($renderSetting['checkbox_setting'])){ 
						if(isset($renderSetting['checkbox_setting']['stav_array']) && in_array($item[$renderSetting['checkbox_setting']['model']][$renderSetting['checkbox_setting']['col']],$renderSetting['checkbox_setting']['stav_array'])){
							if((isset($renderSetting['checkbox_setting']['col2']) && $item[$renderSetting['checkbox_setting']['model']][$renderSetting['checkbox_setting']['col2']]==0) || !isset($renderSetting['checkbox_setting']['col2']))
							{
								$checkbox_id = $item[$renderSetting['checkbox_setting']['model']]['id'];
								$data =  "data[".$renderSetting['checkbox_setting']['model']."][id][".$item[$renderSetting['checkbox_setting']['model']]["id"]."]";
								if($renderSetting['checkbox_setting']['month_year']){
									$data .=  "[year][".$item[$renderSetting['checkbox_setting']['model']]['year']."]";
									$data .=  "[month][".$item[$renderSetting['checkbox_setting']['model']]['month']."]";
								}
							}
							else $disabled = "DISABLED";
						}
                        else if(isset($renderSetting['checkbox_setting']['model']) && isset($renderSetting['checkbox_setting']['col'])){
                            $checkbox_id = $item[$renderSetting['checkbox_setting']['model']][$renderSetting['checkbox_setting']['col']];
							$data =  "data[".$checkbox_id."]";
                            
                            if(isset($renderSetting['checkbox_setting']['model2']) && isset($renderSetting['checkbox_setting']['col2'])){
                                $add_checkbox_id =  $item[$renderSetting['checkbox_setting']['model2']][$renderSetting['checkbox_setting']['col2']];
                                $data .=  '['.$add_checkbox_id."]";
                                $checkbox_id = $checkbox_id.'_'.$add_checkbox_id;
                            }
								
                        }
						else $disabled = "DISABLED";
                        
                        if(isset($renderSetting['checkbox_setting']['checked_on_start']) && $renderSetting['checkbox_setting']['checked_on_start'] == true){
                            $checked = 'checked=checked';
                        }
                        if(isset($_ignored_items) && in_array($checkbox_id,$_ignored_items)){
                            $checked = null;
                        }
					}

					echo '<td class="no_print"><input type="checkbox" id="checkbox_'.$checkbox_id.'" name="'.$data.'" '.$disabled.' '.$checked.'/></td>';
				}
                $count_skip_td = 0; //pocet preskakovanych td
                $prefix = null; //prefix td, treba nejaky text v te bunce
                $counter = 0;
				foreach($renderSetting['items'] as $key => $td){
				   /**
                    * nova funkce ktera slucuje td v radku
				    */ 
				   if(isset($renderSetting['items_group'])){
				        /**
				         * pri splneni podminky pouze
                         * zbytecne testovat pokud podminka neni nastavena, protoze potom colspan nema vyznam???
				         */
                        $conditon = $renderSetting['items_group']['condition'];
                        if($item[$conditon['model']][$conditon['col']] == $conditon['value']){
                    
    				        //pokud je tento sloupce zacatkem colspanu
                            if($key == $renderSetting['items_group']['start']){
                                //pokud je jiny colspan
                                if(isset($renderSetting['items_group']['colspan_data'])){
                                    $td['model'] = $renderSetting['items_group']['colspan_data']['model'];
                                    $td['col'] = $renderSetting['items_group']['colspan_data']['col'];                           
                                }
                                //pokud je nastaven nejaky prefix pro colspan td
                                if(isset($renderSetting['items_group']['text']))
                                    $prefix = $renderSetting['items_group']['text'];
                                
                                echo '<td colspan="'.$renderSetting['items_group']['count'].'" class="'.(isset($renderSetting['items_group']['css'])?$renderSetting['items_group']['css']:'').'">'.$prefix.$viewIndex->generate_td($item,$td,$this->viewVars).'</td>';
                                /**
                                 * nastaveni poctu td koliks e ma preskocit
                                 * musi tam byt -1 protoze v podminka se rovna nule!
                                 */
                                $count_skip_td = $renderSetting['items_group']['count']-1;
                            }
                            else
                                if($count_skip_td == 0)
                                    echo '<td>'.$viewIndex->generate_td($item,$td,$this->viewVars).'</td>';
                                else
                                    $count_skip_td --;   
                        }
                        //pokud podminka neni splnena tak pokracuj klasicky
                        else{
                            echo '<td><span class="fleft">'.$viewIndex->generate_td($item,$td,$this->viewVars);  
                            if(isset($renderSetting['items_hint']) && isset($renderSetting['items_hint'][$key])){
                                $hint_text = null;
                                foreach($renderSetting['items_hint'][$key] as $hi){
                                    list($caption, $model, $col, $type, $fnc) = explode('|',$hi);
                                    $hi = compact(array("caption", "model","col","type","fnc"));	
                                    $hint_text .= $caption.': '.$viewIndex->generate_td($item,$hi,$this->viewVars).'<br /><br />';
                                }
                                echo '</span><p class="row-hint"><span class="info">'.$hint_text.'</span></p>';
      
                             }
                            
                            echo '</td>';   
                        }         
                   }
                   else {
                        echo '<td><span class="fleft">'.$viewIndex->generate_td($item,$td,$this->viewVars);  
                            if(isset($renderSetting['items_hint']) && isset($renderSetting['items_hint'][$key])){
                                $hint_text = null;
                                foreach($renderSetting['items_hint'][$key] as $hi){
                                    list($caption, $model, $col, $type, $fnc) = explode('|',$hi);
                                    $hi = compact(array("caption", "model","col","type","fnc"));	
                                    $hint_text .= $caption.': '.$viewIndex->generate_td($item,$hi,$this->viewVars).'<br /><br />';
                                }
                                echo '</span><p class="row-hint"><span class="info">'.$hint_text.'</span></p>';
      
                             }
                            
                            echo '</td>';
                        if(isset($renderSetting['sum_row']) && $counter > 0){
                            //echo $item[$td['model']][$td['col']];
                            if(!isset($sum_rows[$td['col']]))
                                $sum_rows[$td['col']] = @$item[$td['model']][$td['col']];
                            else
                                $sum_rows[$td['col']] += @$item[$td['model']][$td['col']];
                        }    
                   }  
                   
                   $counter++;  
				}
				if (!empty($renderSetting['posibility']))
					echo '<td class="no_print">'.$viewIndex->generate_posibility($renderSetting,$item,$logged_user).'</td>';
			echo '</tr>';
			$index = ($index ==0)?1:0;
		endforeach;
        /**
          * Pokud je nastaven sumarizani radek, prvni radek jako text, abytek sum
          */    
         if(isset($renderSetting['sum_row'])){
              echo '<tr><td>&nbsp;</td><td>'.$renderSetting['sum_row'].'</td>';
              foreach($sum_rows as $row){
                 echo '<td>'.$fastest->price($row,'').'</td>';
              }
              echo '</tr>';
         }       
    }    
	else
		echo '<tr><td colspan="10" style="text-align:center">Nic nebylo nenalezeno ...</td></tr>';
?>
</tbody></table>
<br class="clear" />
<div style='margin: 10px 20px;'>
    <?php echo '<span>Celkem nalezeno: '.$paging['total'].'</span>'; ?><br />
    
    <?php if (isset($list_of_sums)):?>
        <?php foreach($list_of_sums as $cap=>$val):?>
            <span><?= $cap;?>: <?=$val;?></span><br />
        <?php endforeach;?>
    <?php endif;?>
</div>

<?php echo $this->renderElement('pagination');?>

<script>

    <?php if(isset($filtration_sum_variables2)){?>
    $('tabulka_item').getElement('tbody').getElements('input[type=checkbox]').addEvent('click',function(e){
        //e.stop();
        set_uncheked_id();
    })
    
    function set_uncheked_id(){
        var refresher = $('refresh').getProperty('href');
        var data = '&chck=';
        $('tabulka_item').getElement('tbody').getElements('input[type=checkbox]').each(function(item){
            if(!item.checked){
                name = item.getProperty('name');
                var re1=/(\d+)/g; 
                var pole = name.match(re1);
                if(pole[1])
                    data += pole[0]+'_'+pole[1]+'|';
                else  
                    data += pole[0]+'|';  
            }  				    
        });
        
        new Request.HTML({
    		url: refresher + data,
            update: 'items',
    		onComplete: (function(){})
        }).send();      
    }
    
    <?php }?>

	// START check all checkbox in row
	$('check_all').addEvent('click',function(){
		if (this.checked){
				$('tabulka_item').getElements('input[type=checkbox]').each(function(item){
					if(item.disabled==false)
						item.setProperty('checked','checked');
				});		
		}
		else
			$('tabulka_item').getElements('input[type=checkbox]').removeProperty('checked');
	});
	// END check all checkbox in row

	//START change status
	$('tabulka_item').getElements('.status0, .status1').addEvent('click',function(e){
		new Event(e).stop();
		new Request.JSON({
			url:this.href,
			onComplete:(function(){
				var old_status = this.href.substring(this.href.length-2,this.href.length-1);
				var new_status = (old_status==0)?1:0;
				this.href = this.href.substring(0,this.href.length-2) + new_status + '/';
				this.removeClass('status'+old_status);
				this.addClass('status'+new_status);
			}).bind(this)
		}).send();
	});
	//END change status

	//START change status new
	$('tabulka_item').getElements('.status_new0, .status_new1').addEvent('click',function(e){
		new Event(e).stop();
		new Request.JSON({
			url:this.href,
			onComplete:(function(){
				var old_status = this.href.substring(this.href.length-2,this.href.length-1);
				var new_status = (old_status==0)?1:0;
				this.href = this.href.substring(0,this.href.length-2) + new_status + '/';
				this.removeClass('status_new'+old_status);
				this.addClass('status_new'+new_status);
			}).bind(this)
		}).send();
	});
	//END change status

	//START change stop
	$('tabulka_item').getElements('.stop1, .stop0').addEvent('click',function(e){
		new Event(e).stop();
		new Request.JSON({
			url:this.href,
			onComplete:(function(){
				click_refresh(this);
				var old_status = this.href.substring(this.href.length-2,this.href.length-1);
				var new_status = (old_status==0)?1:0;
				this.href = this.href.substring(0,this.href.length-2) + new_status + '/';
				this.removeClass('stop'+old_status);
				this.addClass('stop'+new_status);
			}).bind(this)
		}).send();
	});
	//END change stop

	//START autorizace
	$('tabulka_item').getElements('.autorizace').addEvent('click',function(e){
		new Event(e).stop();

		if(confirm('Autorizovat docházku pro tuto firmu, měsíc a rok?')){
			new Request.JSON({
				url:this.href,
				onComplete:function(){
					click_refresh();
					alert('Autorizace proběhla úspěšně!');
				}
			}).send();
		}
	});
	//END autorizace
    
    //START return_order
	$('tabulka_item').getElements('.return_order').addEvent('click',function(e){
		new Event(e).stop();
        this.addClass('none');
        
		if(confirm('Opravdu chcete zrušit zpracování této objednávky?')){
			new Request.JSON({
				url:this.href,
				onComplete:(function(json){
				    if(json){
				        if(json.result == true){
				           click_refresh();
					       alert('Zrušení proběhlo úspěšně!');
				        }
                        else{
                            alert(json.message);
                        }
				    }
                    
                    this.removeClass('none');  
				}).bind(this)
			}).send();
		}
        else
            this.removeClass('none');
	});
	//END return_order
    
    //START autorizace
	$('tabulka_item').getElements('.autorizace_dochazky').addEvent('click',function(e){
		new Event(e).stop();

		if(confirm('Opravdu chcete autorizovat docházku pro tohoto klienta?')){
			new Request.JSON({
				url:this.href,
				onComplete:function(json){
				    if(json){
				        if(json.result == true){
				            click_refresh();
					        alert('Autorizace proběhla úspěšně!');
				        }
				    }
				}
			}).send();
		}
	});
	//END autorizace
    
     //potvrzeni uzavreni
	$('tabulka_item').getElements('.potvrdit_uzavreni').addEvent('click',function(e){
			new Event(e).stop();
			if (confirm('Opravdu si přejete potvrdit tuto uzavřenou docházku?')){
				new Request.JSON({
					url:this.href,
					onComplete:function(){
						click_refresh();
					}
				}).send();
			}
		});
	
	//START autorizace klienta z webu do systému
	$('tabulka_item').getElements('.auth').addEvent('click',function(e){
		new Event(e).stop();

		if(confirm('Opravdu chcete převést tohoto klienta do systému?')){
			new Request.JSON({
				url:this.href,
				onComplete:function(json){
					if(json == true){
						click_refresh();
						alert('Klient úspěšně převeden!');
					}
					else 
						alert('Převod byl neúspěšný.');
				}
			}).send();
		}
	});
	//END autorizace

	//START vyplatit
	$('tabulka_item').getElements('.money').addEvent('click',function(e){
		new Event(e).stop();

		if(confirm('Vyplatit peníze pro toho klienta?')){
			new Request.JSON({
				url:this.href,
				onComplete:function(){
					click_refresh();
				}
			}).send();
		}
	});
	//END vyplatit
 
    //START fce fakturovano - ev.majektu
	$('tabulka_item').getElements('.fakturovano').addEvent('click',function(e){
		new Event(e).stop();

		if(confirm('Opravdu chcete tuto fakturu uvést do stavu: Fakturováno??')){
			new Request.JSON({
				url:this.href,
				onComplete:function(json){
					if(json && json.result == true){
						click_refresh();
						alert('Stav byl úspěšně změněn!');
					}
					else 
						alert('Převod byl neúspěšný.');
				}
			}).send();
		}
	});
	//END fakturovano
    
        
    //START obnovit klienta
	$('tabulka_item').getElements('.refresh').addEvent('click',function(e){
		new Event(e).stop();

		if(confirm('Opravdu chcete obnovit klienta?')){
			new Request.JSON({
				url:this.href,
				onComplete:function(){
					click_refresh();
				}
			}).send();
		}
	});
	//END obnovit klienta

	
	//START vyplatit
	$('tabulka_item').getElements('.money2').addEvent('click',function(e){
		new Event(e).stop();

		if(confirm('Vyplatit peníze pro toho recruitera?')){
			new Request.JSON({
				url:this.href,
				onComplete:function(){
					click_refresh();
				}
			}).send();
		}
	});
	//END vyplatit

	//START uzavrti dochazku
	$('tabulka_item').getElements('.uzavrit_odpracovane_hodiny').addEvent('click',function(e){
		this.addClass('none');
        
        new Event(e).stop();
		if(confirm('Opravdu si přejete uzavřít docházku?')){
			new Request.JSON({
				url:this.href,
                data: {'data[neco]':1},
				onComplete:function(json){
					if(json.result == true)
						click_refresh();
					else 
						alert(json.message);
				}
			}).send();
		}
        
        this.removeClass('none');  
	});
	//END uzavrti dochazku
    
    
    //START klonovat skupinu dochazku
	$('tabulka_item').getElements('.klon').addEvent('click',function(e){
		this.addClass('none');
        
        new Event(e).stop();
		if(confirm('Opravdu si přejete klonovat tuto skupinu?')){
			new Request.JSON({
				url:this.href,
				onComplete:function(json){
					if(json.result == true)
						click_refresh();
					else 
						alert(json.message);
				}
			}).send();
		}
        
        this.removeClass('none');  
	});
	//END uzavrti dochazku

    
    //START overeni import clienta
	$('tabulka_item').getElements('.overeni').addEvent('click',function(e){
		new Event(e).stop();
        var text = '';
			new Request.JSON({
				url:this.href,
				onComplete:function(json){
					if(json.result == true){
						if(json.data != 0){
						     $each(json.data,function(name,id){
						        text += "ID: " + id + " Jméno: " + name + "\n";  
						    });
                            alert("Klient se v telefonu nebo v datum narození shoduje s:\n\n" + text);   
						}
    					else 
    						alert('Tento klient se neshoduje(mobil, datum narození) s žádným v databází systému');
				    }
                    else
                        alert('Chyba aplikace!!!');
                }
			}).send();
	});
	//END overeni import clienta
    
    
    //START export_to_companies
	$('tabulka_item').getElements('.export_to_companies').addEvent('click',function(e){
		new Event(e).stop();
			new Request.JSON({
				url:this.href,
				onComplete:function(json){
				    if(!json)
                        alert('Chyba aplikace');
                    
					if(json.result == true){
					    alert('Export byl úspěšný. Počet ovlivněných firem : '+json.pocet);
					}
                    else
                        alert(json.message);
                }
			}).send();
	});
	//END export_to_companies
    

    //START duplicity_client
	$('tabulka_item').getElements('.duplicity_client').addEvent('click',function(e){
		new Event(e).stop();
        if(confirm('Opravdu si přejete vytvořit vícenásobné zaměstnání?')){
			new Request.JSON({
				url:this.href,
                data: {'data[neco]':1},
				onComplete:function(json){
				    if(!json)
                        alert('Chyba aplikace');
                    
					if(json.result == true){
					    alert('Novy klient byl vytvořen jeho id:'+json.id);
					}
                    else
                        alert(json.message);
                }
			}).send();
        }    
	});
	//END duplicity_client
    
    
	//START trash item
	$('tabulka_item').getElements('.trash, .trash_re, .delete_estate_loss_event,.delete_campaign,.fuel_trash,.contract_trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejete tuto položku odstranit')){
			new Request.JSON({
				url:this.href,
				onComplete:function(){
					click_refresh();
				}
			}).send();
		}
	});
	//END trash item
    
    $('tabulka_item').getElements('.add_estate_1,.add_estate_0').addEvent('click',function(e){
   	    e.stop();
       	var typ = (this.className.substring(this.className.length-1,this.className.length)==0?'add':'remove');
        if (typ == 'add'){
            var url = this.href;
        } else {
            arr = this.href.split('/');
            var url = '/audit_estates/remove_estate/' + arr[6]; 
        }
        
        domwin.newWindow({
			id			: 'domwin',
			sizes		: [600,700],
			scrollbars	: true,
			languages	: false,
			defined_lang: [],
			title		: (typ=='add')?this.title:'Vrácení majetku',
			ajax_url	: url,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false
		}); 
        
    });
	
	$('tabulka_item').getElements('.edit,.items,.cinnost,.newsletter,.slepy_rozpocet,.project_info_mails,.project_money_items, .setting,.form_items,.client_info,.client_working_template,.form_template,.access,.add_activity,.domwin_add_activity,.add_opp,.opp_transfer,.person,.history,.centre,.employ_on_project,.office_manager,.sharing_company,.elimination,.manage_estate,.manage_estate_admin,.project_spravce_majetku,.client_buy').addEvent('click',function(e){
		new Event(e).stop();

		domwin.newWindow({
			id			: 'domwin',
			sizes		: <?php echo (isset($renderSetting['domwin_setting']['sizes']))?$renderSetting['domwin_setting']['sizes']:'[600,900]'?>,
			scrollbars	: <?php echo (isset($renderSetting['domwin_setting']['scrollbars']))?$renderSetting['domwin_setting']['scrollbars']:'true'?>,
			languages	: <?php echo (isset($renderSetting['domwin_setting']['languages']) && $renderSetting['domwin_setting']['languages'])?$renderSetting['domwin_setting']['languages']:'false'?>,
			defined_lang: [<?php echo implode(',',array_map(create_function('$item', 'return  \'"\'.$item.\'"\';'),array_keys($languages))); ?>],
			title		: this.title,
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false
		}); 
		
	});
    
    //rozpocet
    $('tabulka_item').getElements('.rozpocet').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_rozpocet',
			sizes		: <?php echo (isset($renderSetting['domwin_setting']['sizes']))?$renderSetting['domwin_setting']['sizes']:'[600,900]'?>,
			scrollbars	: <?php echo (isset($renderSetting['domwin_setting']['scrollbars']))?$renderSetting['domwin_setting']['scrollbars']:'true'?>,
			languages	: <?php echo (isset($renderSetting['domwin_setting']['languages']))?$renderSetting['domwin_setting']['languages']:'false'?>,
			defined_lang: [<?php echo implode(',',array_map(create_function('$item', 'return  \'"\'.$item.\'"\';'),array_keys($languages))); ?>],
			title		: this.title,
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false
		}); 
		
	});
    
    //vynosovy rozpocet
    $('tabulka_item').getElements('.vynosovy_rozpocet').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_vynosovy_rozpocet',
			sizes		: <?php echo (isset($renderSetting['domwin_setting']['sizes']))?$renderSetting['domwin_setting']['sizes']:'[600,900]'?>,
			scrollbars	: <?php echo (isset($renderSetting['domwin_setting']['scrollbars']))?$renderSetting['domwin_setting']['scrollbars']:'true'?>,
			languages	: <?php echo (isset($renderSetting['domwin_setting']['languages']))?$renderSetting['domwin_setting']['languages']:'false'?>,
			defined_lang: [<?php echo implode(',',array_map(create_function('$item', 'return  \'"\'.$item.\'"\';'),array_keys($languages))); ?>],
			title		: this.title,
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false
		}); 
		
	});
	
	/* ********************* */
	/* START js for Companies */
	/* ********************* */
	$('tabulka_item').getElements('.kontakty').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_kontakty',
			sizes		: [600,900],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: true
		}); 
	});
	
	$('tabulka_item').getElements('.aktivity').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_aktivity',
			sizes		: [900,900],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: true
		}); 
	});
	$('tabulka_item').getElements('.attachs,.attach_exist').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_attach',
			sizes		: [800,900],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: true
		}); 
	});
	
	$('tabulka_item').getElements('.tasks').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_tasks',
			sizes		: [900,900],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: true
		}); 
	});
	$('tabulka_item').getElements('.pozice,.order,.template').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_pozice',
			sizes		: [950,900],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: true
		}); 
	});
	$('tabulka_item').getElements('.fakturacka').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_invoice_item',
			sizes		: [950,900],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: true
		}); 
	});
	$('tabulka_item').getElements('.nabor').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_nabor_list',
			sizes		: [950,900],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: true
		}); 
	});
	/* ******************* */
	/* END js for Companies */
	/* ******************* */
	
	
	/* **************************** */
	/* START js for Company Activities*/
	/* **************************** */
	
	$('tabulka_item').getElements('.show').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_show',
			sizes		: <?php echo (isset($renderSetting['domwin_setting']['sizes']))?$renderSetting['domwin_setting']['sizes']:'[800,900]'?>,
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: true
		}); 
	});
	
	$('tabulka_item').getElements('.nabor_pozice').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_nabor_pozice',
			sizes		: [950,900],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: true
		}); 
	});
	
	$('tabulka_item').getElements('.svatky,.pocet_dni').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_svatky',
			sizes		: [550,900],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false
		}); 
	});
	
	$('tabulka_item').getElements('.messages').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_messages',
			sizes		: [550,900],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false
		}); 
	});
	
	$('tabulka_item').getElements('.zamestnanci').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_zamestnanci',
			sizes		: [750,900],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false
		}); 
	});
	
	$('tabulka_item').getElements('.odpracovane_hodiny,.ucetni_dokumentace,.stats,.rozvazat_prac_pomer,.rozvazat_prac_pomer_int,.edit_prac_pomer').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_zamestnanci_hodiny',
			sizes		: [950,900],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false
		}); 
	});
	
	$('tabulka_item').getElements('.zmena_pp,.zmena_pp_int,.zmena_pp_admin,.zmena_pp_new,.modify_from_date,.edit_datum_zamestnani').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_zamestnanci_hodiny',
			sizes		: [800,900],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false
		}); 
	});

	$('tabulka_item').getElements('.campaign_stav').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_campaign_stav',
			sizes		: [580,400],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false
		}); 
	});
    
    	$('tabulka_item').getElements('.uhrazeno, .pick_up').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_uhrazeno',
			sizes		: [400,250],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false
		}); 
	});
	
	/* **************************** */
	/* END  js for Company Activities*/
	/* **************************** */
	
	
	<?php if (isset($spec_h1)):?>
		$('obal').getElement('h1').setHTML('<?php echo $spec_h1;?>');
	<?php endif;?>

	<?php if(isset($change_list_js)):?>
		var get_data =  JSON.decode('<?php echo json_encode($_GET);?>');

		var data_change = JSON.decode('<?php echo $change_list_js;?>');
		$each(data_change, function(items, elementId){
			var element = $(elementId);
			if (element){
				element.empty();
				new Element('option',{title:'-- všechny --', value:''}).setHTML('-- všechny --').inject(element);
				$each(items, function(caption, id){
					neco = (get_data['filtration_ConnectionClientRequirement-company_id']==id ? 'selected' : '');
					new Element('option',{title:caption, value:id, selected:neco}).setHTML(caption).inject(element);
				})
			}
		});
	<?php endif;?>
    
    <?php if(isset($renderSetting['thunderbird'])){?>
        new ThunderBird();
    <?php }?>
</script>
