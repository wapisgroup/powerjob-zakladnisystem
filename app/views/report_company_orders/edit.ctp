<form action='/report_company_orders/edit/' method='post' id='company_template_edit_formular'>
	<?php echo $htmlExt->hidden('CompanyOrderItem/id');?>
    <?php echo $htmlExt->hidden('www_insertion_id');?>
    <?php echo $htmlExt->hidden('CompanyOrderItem/start_place');?>
    <?php echo $htmlExt->hidden('CompanyOrderItem/setting_career_item_id');?>
	<?php echo $htmlExt->hidden('CompanyOrderItem/www_cms_user_id');?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">WWW nabídka</a></li>
        </ul>
	</div>
	<div class="domtabs admin_dom">
		
         <div class="domtabs field">
            <fieldset>
                <legend>Informace pro www stránky</legend>
               		
                    <div class="sll">
                       <?php echo $htmlExt->checkbox('CompanyOrderItem/www_show',null,array('label'=>'Zobrazovat na WWW stránce'));?><br /> 
                    </div>
                    <div class="slr">
                    
                    </div>
                    <br />
                    <?php echo $htmlExt->input('CompanyOrderItem/www_name', array('class'=>'long','label'=>'Název pozice','label_class'=>'long'));?> <br />
               	    <?php echo $htmlExt->input('CompanyOrderItem/www_mzda', array('class'=>'long','label'=>'Mzda','label_class'=>'long'));?> <br />
               	
                   	<?php echo $htmlExt->textarea('CompanyOrderItem/www_napln', array('class'=>'long','label'=>'Náplň práce','label_class'=>'long'));?> <br />
               		<?php echo $htmlExt->textarea('CompanyOrderItem/www_pozadujeme', array('class'=>'long','label'=>'Požadujeme','label_class'=>'long'));?> <br />
               		<?php echo $htmlExt->textarea('CompanyOrderItem/www_dalsi_informace', array('class'=>'long','label'=>'Další informace','label_class'=>'long'));?> <br />
		
            </fieldset>
            <?php //pr($this->data)?>
         </div> 
         
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditCompanyOrderItemSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyOrderItemClose'/>
		<input type='button' value='Publikovat na Facebook' id='AddOnFacebook'/>
	</div>
</form>
<script>
	/*
	 * Zakladni funkce
	 */
	var domtab = new DomTabs({'className':'admin_dom'}); 
    $$('.integer, .float').inputLimit();
    
   
	/*
	 * Zavreni Domwinu, bez ulozeni
	 */
	$('AddEditCompanyOrderItemClose').addEvent('click',function(e){new Event(e).stop(); 
		domwin.closeWindow('domwin');
	});
	

    if ($('AddOnFacebook')){
        $('AddOnFacebook').addEvent('click', function(e){
            e.stop();
            domwin.newWindow({
                id			: 'domwin_facebook',
                sizes		: [500,500],
                scrollbars	: true,
                title		: 'Publikace na Facebook',
                languages	: false,
                type		: 'AJAX',
                ajax_url	: '/nabor_orders/fcb/',
                closeConfirm: true,
                max_minBtn	: false,
                modal_close	: false,
                remove_scroll: false,
                post_data: $('company_template_edit_formular').toQueryString()
            });
        })
    }
    
        
    
	/*
	 * Ulozeni www objednavky
	 */
	if ($('AddEditCompanyOrderItemSaveAndClose')) {
	   	$('AddEditCompanyOrderItemSaveAndClose').addEvent('click', function(e){
	   		new Event(e).stop();
	            button_preloader($('AddEditCompanyOrderItemSaveAndClose'));
	   			new Request.JSON({
	   				url: $('company_template_edit_formular').action,
	   				onComplete: function(json){
	   					if (json) {
	   						if (json.result === true) {
	                            button_preloader_disable($('AddEditCompanyOrderItemSaveAndClose'));
	   							domwin.closeWindow('domwin');
       						}
	   						else {
	   							alert(json.message);
	   						}
	   					}
	   					else {
	   						alert('Chyba aplikace');
	   					}
	   				}
	   			}).post($('company_template_edit_formular'));
	   	
	   });
	}
	
	
</script>