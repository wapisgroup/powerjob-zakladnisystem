<form action='/company_activities/edit/' method='post' id='company_activity_edit_formular'>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Aktivita</a></li>
			<li class="ousko"><a href="#" id='add_task_from_activity'>Vytvořit k této aktivitě úkol</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<?php echo $htmlExt->hidden('CompanyActivity/id');?>
			<?php echo $htmlExt->hidden('CompanyActivity/company_id');?>
			<?php echo $htmlExt->hidden('CompanyActivity/self_manager_id');?>
			<fieldset>
				<legend>Prosím vyplňte následující údaje</legend>
				<?php echo $htmlExt->input('CompanyActivity/name',array('tabindex'=>1,'label'=>'Název','class'=>'long','label_class'=>'long'));?> <br class="clear">
				<div class='sll'>
					<?php echo $htmlExt->inputDate('CompanyActivity/activity_datetime',array('tabindex'=>1,'label'=>'Datum'));?> <br class="clear">
					<?php echo $htmlExt->selectTag('CompanyActivity/company_contact_id',$comapany_contact_list,null,array('tabindex'=>3,'label'=>'Kontakt'));?> <br class="clear">
				</div>
				<div class='slr'>
					<?php echo $htmlExt->input('CompanyActivity/mesto',array('tabindex'=>2,'label'=>'Město'));?> <br class="clear">
					<?php echo $htmlExt->selectTag('CompanyActivity/company_activity_type_id',$company_activity_type_list,null,array('tabindex'=>4,'label'=>'Typ aktivity'));?> <br class="clear">
				</div>
				<br class='clear' />
				<?php echo $htmlExt->textarea('CompanyActivity/text',array('tabindex'=>5,'label'=>'Popis','class'=>'long','label_class'=>'long'));?><br class='clear' />
			</fieldset>
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditCompanyActivitySaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyActivityClose'/>
	</div>
</form>
<script>
	var domtab = new DomTabs({'className':'admin_dom'}); 
	
	$('AddEditCompanyActivityClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	
	$('AddEditCompanyActivitySaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('company_activity_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('company_activity_edit_formular').action,		
				onComplete:function(){
					domwin.closeWindow('domwin');
				}
			}).post($('company_activity_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('company_activity_edit_formular',{
		'CompanyActivityName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
		}
	});
	validation.generate('company_activity_edit_formular',<?php echo (isset($this->data['CompanyActivity']['id']))?'true':'false';?>);
	
	$('add_task_from_activity')
		.removeEvents('click')
		.addEvent('click', function(e){
			new Event(e).stop();
			domwin.newWindow({
				id				: 'domwin_tasks_add',
				sizes			: [580,320],
				scrollbars		: true,
				title			: 'Přidání nového úkolu',
				languages		: false,
				type			: 'AJAX',
				ajax_url		: '/companies/tasks_edit/' + $('CompanyActivityCompanyId').value,
				closeConfirm	: true,
				max_minBtn		: false,
				modal_close		: false,
				remove_scroll	: false,
				self_data		: {from:'activity_edit'}
			}); 
		});
</script>