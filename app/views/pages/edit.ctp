<form action='/CmsUsers/edit/' method='post' id='cms_user_edit_formular'>
	<?php echo $htmlExt->hidden('CmsUser/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul>
			<li><a href="#krok1">Základní informace</a></li>
			<li><a href="#krok2">Přístupové údaje</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Osobní údaje</legend>
				<div class="sll">  
					<?php echo $htmlExt->input('CmsUser/jmeno',array('tabindex'=>1,'label'=>'Jméno'));?> <br class="clear">
					<?php echo $htmlExt->input('CmsUser/other/titul_pred',array('tabindex'=>3,'label'=>'Titul před'));?> <br class="clear">
				</div>
				<div class="slr">  
					<?php echo $htmlExt->input('CmsUser/prijmeni',array('tabindex'=>2,'label'=>'Příjmení'));?> <br class="clear">
					<?php echo $htmlExt->input('CmsUser/other/titul_za',array('tabindex'=>3,'label'=>'Titul za'));?> <br class="clear">
				</div>
			</fieldset>
			<fieldset>
				<legend>Kontaktní údaje</legend>
				<div class="sll">  
					<?php echo $htmlExt->input('CmsUser/email',array('tabindex'=>1,'label'=>'Email'));?> <br class="clear">
					<?php echo $htmlExt->input('CmsUser/telefon',array('maxlength'=>15,'tabindex'=>3,'label'=>'Telefon'));?> <br class="clear">
				</div>
				<div class="slr">  
					<?php echo $htmlExt->input('CmsUser/mobil',array('maxlength'=>15,'tabindex'=>2,'label'=>'Mobil'));?> <br class="clear">
					<?php echo $htmlExt->input('CmsUser/icq',array('tabindex'=>4,'label'=>'ICQ'));?> <br class="clear">
				</div>
				<br class='clear' />
				<div class="sll">  
					<?php echo $htmlExt->input('CmsUser/ulice',array('tabindex'=>1,'label'=>'Ulice'));?> <br class="clear">
					<?php echo $htmlExt->input('CmsUser/cp',array('tabindex'=>3,'label'=>'ČP'));?> <br class="clear">
				</div>
				<div class="slr">  
					<?php echo $htmlExt->input('CmsUser/mesto',array('tabindex'=>2,'label'=>'Město'));?> <br class="clear">
					<?php echo $htmlExt->input('CmsUser/psc',array('tabindex'=>3,'label'=>'PSČ'));?> <br class="clear">
				</div>
			</fieldset>
			<div class="krok">
				<a href='#krok2' class='admin_dom_move next'>Další krok</a>
			</div>
		</div>
		<div class="domtabs field">
			<?php if (isset($this->data['CmsUser']['id'])):?>
			<fieldset>
				<legend>'Statistiky'</legend>
				<div class="sll">  
					<label>Vytvořen:</label><span class="input"> <?php echo $fastest->czechDateTime($this->data['CmsUser']['created']);?> </span><br class="clear" />
					<label>Poslední přihlášení:</label><span class="input"><?php echo $fastest->czechDateTime($this->data['CmsUser']['last_log']);?> </span><br class="clear" />
				</div>
				<div class="slr">  
					<label>'Poslední úprava:</label><span class="input"> <?php echo $fastest->czechDateTime($this->data['CmsUser']['updated']);?>  </span><br class="clear" />
				</div>
			</fieldset>
			<?php endif;?>
			<fieldset>
				<legend>Přístupové údaje</legend>
				<div class="sll">  
					<?php // echo $htmlExt->selectTag('CmsUser/cms_group_id',$cms_group_id,null,array('tabindex'=>1,'label'=>lang_skupina),null,false);?><br class="clear" />
				</div>
				<div class="slr">  
					<?php echo isset($this->data['CmsUser']['user_name'])?'<label>Uživatelské jméno:</label><span class="input">'.$this->data['CmsUser']['user_name'].'</span>':$htmlExt->input('CmsUser/user_name',array('tabindex'=>2,'label'=>'Uživatelské jméno'));?> <br class="clear">
				</div>
				
				<?php if (isset($this->data['CmsUser']['id'])):?>
					<div class="sll">
					<label>Změnit heslo:</label><input type='checkbox' id='show_hide_pass'/><br class='clear'/>
					</div>
				<?php endif;?>
				
				<div class="sll password_enabled">  
					<?php echo $htmlExt->password('CmsUser/heslo',array('tabindex'=>3,'label'=>'Heslo','value'=>''));?> <br class="clear">
				</div>
				<div class="slr password_enabled">  
					<?php echo $htmlExt->password('CmsUser/heslo2',array('tabindex'=>4,'label'=>'Potvrzení hesla'));?> <br class="clear">
				</div>
			</fieldset>
		<div class="krok">
			<a href='#krok1' class='admin_dom_move prev'>Předchozí krok</a>
		</div>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close'));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close'));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	
	<?php if (isset($this->data['CmsUser']['id'])):?>
		$$('.password_enabled').setStyle('display','none');
		$('show_hide_pass').addEvent('click',function(){
			pass = $$('.password_enabled');
			if (pass[0].getStyle('display') == 'none'){
				pass.setStyle('display','block');
				pass.getElements('.require, .invalid, .valid').each(function(hide){
					hide.removeClass('valid')
					hide.addClass('require');
				});
			} else {
				pass.setStyle('display','none');
				pass.getElements('.require, .invalid, .valid').each(function(hide){
					hide.setValue('');
					hide.removeClass('require')
					hide.removeClass('invalid')
					hide.addClass('valid');
				});
			}
		});
	<?php endif;?>

	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('cms_user_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('cms_user_edit_formular').action,		
				onComplete:function(){
					click_refresh($('CmsUserId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('cms_user_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('cms_user_edit_formular',{
		'CmsUserUserName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit uživatelské jméno'},
			'length': {'condition':{'min':3,'max':10}, 'err_message':'Zadejte v rozsahu 3-10 znaků'},
			'not_equal': {'condition':'admin','err_message':'Nesmi se jmenovat "admin"'},
			'isUnique':{'condition':{'model':'CmsUser','field':'user_name'},'err_message':'Toto uživatelské jméno je již použito'}
		},
		'CmsUserJmeno': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit jméno'},
		},
		'CmsUserEmail': {
			'testReq': {'condition':'email','err_message':'Musíte vyplnit email ve správném formátu'},
		},
		'CmsUserPrijmeni': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit příjmení'},
		},
		'CmsUserHeslo': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit heslo'},
			'length': {'condition':{'min':5,'max':20}, 'err_message':'Heslo zadejte v rozsahu 5-20 znaků'},
		
		},
		'CmsUserHeslo2':{
			'isConfirm':{'condition':{'secObject':'CmsUserHeslo'},'err_message':'Potvrzeni hesla se neshoduje!'}
		}
	});
	validation.generate('cms_user_edit_formular',<?php echo (isset($this->data['CmsUser']['id']))?'true':'false';?>);
</script>