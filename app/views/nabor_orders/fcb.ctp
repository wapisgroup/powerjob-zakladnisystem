<form action='' id='FcbSendForm'>
    <?= $htmlExt->textarea('Fcb/text',array('value'=>$fcb_text,'label'=>'text','class'=>'long','label_class'=>'long','style'=>'width:97%; height:300px;'));?><br class="clear"/>
    <div class='formular_action'>
        <? if($koo_contact == false):?>
            <p style="color:red; font-weight: bold; margin: 12px auto;">Nelze publikovat na FB protože koordinátor nemá vyplněn kontaktní telefon</p>
        <? else: ?>
            <input type='button' value='Odeslat' id='FcbSend' />
        <? endif; ?>

        <input type='button' value='Zavřít' id='FcbClose'/>
    </div>
</form>
<script>
    if($('FcbSend')){
        $('FcbSend').addEvent('click', function(e){
            e.stop();
            new Request.JSON({
                url: '/nabor_orders/fcb_add/',
                onComplete: function(json){
                    if (json){
                        if (json.result === true){
                            alert('Příspěvek byl přidán na zeď!');
                        } else {
                            alert(json.message);
                        }
                    } else {
                        alert('Chyba aplikace, opakujte svuj pozadavek pozdeji')
                    }
                }
            }).send($('FcbSendForm'))
        })
    }

    $('FcbClose').addEvent('click',function(e){
        e.stop();
        domwin.closeWindow('domwin_facebook');
    })
</script>