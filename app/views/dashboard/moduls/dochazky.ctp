<h2>Dochazky:</h2>
<table class='table'>
	<tr>
		<td>Měsíc</td>
		<td><?php echo $htmlExt->input('dochazky_mesic',array('value'=>$mesic,'class'=>'months'));?></td>
	</tr>
    <tr>
		<td>Koordinátor</td>
		<td><?php echo $htmlExt->selectTag('coordinator_id',(array(''=>'Všichni')+$coo_list),$coo_id,array('class'=>''),null,false);?></td>
	</tr>
	
    <?php
    
        foreach($dochazky_data as $key=>$item){
            echo '<tr>';
            echo '<td>'.$item['label'].'</td>';
            echo '<td>';
                if(isset($item['type']) && $item['type'] == 'money')
                    echo $fastest->price($item['value'],',-');
                else
                    echo $item['value'];    
            echo '</td>';
            echo '</tr>';
        }
    
    ?>
    
</table>