<h2>Fakturace:</h2>
<table class='table'>
	<tr>
		<td>Měsíc</td>
		<td><?php echo $htmlExt->input('fakturace_mesic',array('value'=>$mesic,'class'=>'months'));?></td>
	</tr>
	
    <?php
    
        foreach($fakturace_data as $key=>$item){
            echo '<tr>';
            echo '<td>'.$item['label'].'</td>';
            echo '<td>';
                if(isset($item['type']) && $item['type'] == 'money')
                    echo $fastest->price($item['value'],',-');
                else
                    echo $item['value'];    
            echo '</td>';
            echo '</tr>';
        }
    
    ?>
    
</table>