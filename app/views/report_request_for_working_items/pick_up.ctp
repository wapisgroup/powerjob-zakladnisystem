<form action='/report_request_for_working_items/pick_up/' method='post' id='report_down_payments_uhrazeno'>
	<?php echo $htmlExt->hidden('RequestForWorkingItem/id');?>
	<?php echo $htmlExt->hidden('RequestForWorkingItem/client_id');?>
	<?php echo $htmlExt->hidden('RequestForWorkingItem/company_id');?>
	<?php echo $htmlExt->hidden('RequestForWorkingItem/working_item_id');?>


		<fieldset>
				<legend>Základní</legend>
				<div class="sll100">  
                    <?php echo $htmlExt->inputDate('RequestForWorkingItem/date_of_pickup',array('tabindex'=>3,'label'=>'Datum','value'=>date('Y-m-d')));?> <br class="clear">
				    <?php echo $htmlExt->input('RequestForWorkingItem/amount',array('tabindex'=>4,'label'=>'Částka'));?> <br class="clear">
				
                </div>
		</fieldset>


	
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>6));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>7));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">



    if($('save_close'))
    	$('save_close').addEvent('click',function(e){
    		new Event(e).stop();
    		
    		valid_result = validation.valideForm('report_down_payments_uhrazeno');
    		if (valid_result == true){
    			new Request.JSON({
    				url:$('report_down_payments_uhrazeno').action,		
    				onComplete:function(){
    					click_refresh($('RequestForWorkingItemId').value);
    					domwin.closeWindow('domwin_uhrazeno');
    
    				}
    			}).post($('report_down_payments_uhrazeno'));
    		} else {
    			var error_message = new MyAlert();
    			error_message.show(valid_result)
    		}
    	});
    
    validation.define('report_down_payments_uhrazeno',{
        'RequestForWorkingItemAmount': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit částku'},
		}
	});
	validation.generate('report_down_payments_uhrazeno',false);


	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_uhrazeno');});
</script>