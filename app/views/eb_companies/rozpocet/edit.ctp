<form action='/eb_companies/rozpocet_edit/' method='post' id='setting_rozpocet_item_edit_formular'>
	<?php echo $htmlExt->hidden('RozpocetItem/id');?>
	<?php echo $htmlExt->hidden('RozpocetItem/eb_company_id');?>
	
	<div class="domtabs admin_dom_edit_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
			
		</ul>
</div>
	<div class="domtabs admin_dom_edit">
		<div class="domtabs field">
			<fieldset>
				<legend>Nastavení</legend>
                <?php echo $htmlExt->input('RozpocetItem/name',array('tabindex'=>1,'label'=>'Název','class'=>'long','label_class'=>'long'));?> <br class="clear"/>
                <?php echo $htmlExt->selectTag('RozpocetItem/rozpocet_item_category_id', $category_list,null,array('label'=>'Kategorie'));?><br />
                    
                <div class="sll">
                    <?php echo $htmlExt->input('RozpocetItem/code',array('tabindex'=>2,'label'=>'Kód cenníka'));?> <br class="clear"/>
                    <?php echo $htmlExt->input('RozpocetItem/mn',array('tabindex'=>4,'label'=>'Množstvo','class'=>'recount float'));?> <br class="clear"/>
                    <?php echo $htmlExt->input('RozpocetItem/jc',array('tabindex'=>6,'label'=>'Jednotková cena','class'=>'recount float'));?> <br class="clear"/>
                    <?php echo $htmlExt->input('RozpocetItem/cena',array('tabindex'=>8,'label'=>'Cena EUR','readonly'=>'readonly'));?> <br class="clear"/>
                 </div>
                <div class="slr">
                    <?php echo $htmlExt->input('RozpocetItem/code_item',array('tabindex'=>3,'label'=>'Kód položky'));?> <br class="clear"/>
                    <?php echo $htmlExt->input('RozpocetItem/mj',array('tabindex'=>5,'label'=>'Měrná jednotka'));?> <br class="clear"/>
                    <?php echo $htmlExt->input('RozpocetItem/kurz',array('tabindex'=>7,'label'=>'Kurz','class'=>'recount float'));?> <br class="clear"/>
                    <?php echo $htmlExt->input('RozpocetItem/cena_sk',array('tabindex'=>9,'label'=>'Cena SK', 'readonly'=>'readonly'));?> <br class="clear"/>
                </div>
				<br />
			    <?php echo $htmlExt->textarea('RozpocetItem/poznamka',array('tabindex'=>10,'label'=>'Poznámka','class'=>'long','label_class'=>'long'));?> <br class="clear"/>
			</fieldset>
		</div>
    </div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>11));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>12));?>
	</div>
</form>
 
<script language="javascript" type="text/javascript">

    $$('.recount').addEvent('blur', function(){
        mnozstvi = $('RozpocetItemMn').value;
        jednotko = $('RozpocetItemJc').value;
        cena_eur = $('RozpocetItemCena');
        cena_skk= $('RozpocetItemCenaSk');
        kurz    = $('RozpocetItemKurz').value;
        
        cena_eur.value = Math.round(mnozstvi * jednotko*100)/100;
        cena_skk.value = Math.round(cena_eur.value * kurz*100)/100;
    }) 
 
    $$('.integer, .float').inputLimit();
 
	var domtab = new DomTabs({'className':'admin_dom_edit'}); 
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('setting_rozpocet_item_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('setting_rozpocet_item_edit_formular').action,		
				onComplete:function(){
					//click_refresh($('RozpocetItemId').value);
                    domwin.loadContent('domwin_rozpocet');
					domwin.closeWindow('domwin_rozpocet_edit');

				}
			}).post($('setting_rozpocet_item_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_rozpocet_edit');});
	validation.define('setting_rozpocet_item_edit_formular',{
		'RozpocetItemName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'}
		}
	});
	validation.generate('setting_rozpocet_item_edit_formular',<?php echo (isset($this->data['RozpocetItem']['id']))?'true':'false';?>);
</script>