<form id='formular_remove' action='/eb_companies/remove_worker/'>
    <?= $htmlExt->hidden('ConnectionEbcomClient/id', array('value'=>$id));?>
    <fieldset>
        <legend>Informace o ukončení</legend>
        <?= $htmlExt->inputDate('ConnectionEbcomClient/end',array('label'=>'Datum propuštění'));?>
        <br />
        <div class="win_save">
            <input type="submit" value='Uložit' id='win_save' onclick="return false;" />
            <input type="button" value='Zrušit' id='win_close' onclick="return false;"/>
        </div>
    </fieldset>
</form>
<script type="text/javascript">
    $('win_save').addEvent('click', function(e){
        e.stop();
        if ($('calbut_ConnectionEbcomClientEnd').value == ''){
            alert('Musíte vyplnit datum ukončení');
        } else {
            new Request.JSON({
                url: $('formular_remove').getProperty('action'),
                onComplete: function(json){
                    if (json && json.result == true){
                        alert('Zaměstnanec byl odstraněn.');
                        domwin.closeWindow('domwin_worker_remove');
                        domwin.loadContent('domwin',{goto:6});
                    } else {
                        alert('Nastala chyba během ukladání, opakujte akci prosím později');
                    }
                }
            }).send($('formular_remove'));
        }
    })

    $('win_close').addEvent('click', function(e){
        e.stop();
        domwin.closeWindow('domwin_worker_remove');
    })
</script>