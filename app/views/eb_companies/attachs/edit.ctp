<script type="text/javascript">
	function upload_file_complete(url){
		alert('Soubor byl nahrán.');
	}
</script>
<form action='/eb_companies/attachs_edit/' method='post' id='company_attachs_edit_formular'>
	<div class="domtabs admin_dom2_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Příloha</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom2">
		<div class="domtabs field">
			<?php echo $htmlExt->hidden('EbCompanyAttachment/id');?>
			<?php echo $htmlExt->hidden('EbCompanyAttachment/eb_company_id');?>
            <?php echo $htmlExt->hidden('EbCompanyAttachment/type');?>
			<fieldset>
				<legend>Prosím vyplňte následující údaje</legend>
				<?php echo $htmlExt->input('EbCompanyAttachment/name',array('tabindex'=>1,'label'=>'Název','class'=>'long','label_class'=>'long'));?> <br class="clear">
				<div class='sll'>
					<?php 
						$upload_setting = array(
							'paths' => array(
								'upload_script' => '/eb_companies/upload_attach/',
								'path_to_file'	=> '../administrace/uploaded/eb_companies/attachment/',
								'status_path'	=> 'get_status.php'
							),
							'upload_type'	=> 'php',
							'onComplete'	=> 'upload_file_complete',
							'label'			=> 'Soubor'
						);
					?>
					<?php echo $fileInput->render('EbCompanyAttachment/file',$upload_setting);?> <br class="clear">
				</div>
				<div class='slr'>
					<?php echo $htmlExt->selectTag('EbCompanyAttachment/setting_attachment_type_id',$setting_attachment_type_list,null,array('tabindex'=>4,'label'=>'Typ přílohy'));?> <br class="clear">
				</div>
			</fieldset>
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditEbCompanyAttachmentSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditEbCompanyAttachmentClose'/>
	</div>
</form>
<script>

	var domtab = new DomTabs({'className':'admin_dom2'}); 
	
	$('AddEditEbCompanyAttachmentClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_attach_add');});
	
	$('AddEditEbCompanyAttachmentSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('company_attachs_edit_formular');
		if (valid_result == true){
			new Request.HTML({
				url:$('company_attachs_edit_formular').action,		
				update: $('domwin_attach').getElement('.CB_ImgContainer'),
				onComplete:function(){
					domwin.closeWindow('domwin_attach_add');
				}
			}).post($('company_attachs_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('company_attachs_edit_formular',{
		'EbCompanyAttachmentName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
		}
	});
	validation.generate('company_attachs_edit_formular',<?php echo (isset($this->data['EbCompanyAttachment']['id']))?'true':'false';?>);
</script>