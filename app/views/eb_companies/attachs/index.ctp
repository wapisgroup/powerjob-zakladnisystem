<div class="win_fce top">
<a href='/eb_companies/attachs_edit/<?php echo $link;?>/' class='button edit' id='company_attach_add_new' title='Přidání nové přílohy'>Přidat přílohu</a>
</div>
<table class='table' id='table_attachs'>
	<tr>
		<th>#ID</th>
		<th>Název</th>
		<th>Typ</th>
		<th>Vytvořeno</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($attachment_list) && count($attachment_list) >0):?>
	<?php foreach($attachment_list as $attachment_item):?>
	<tr>
		<td><?php echo $attachment_item['EbCompanyAttachment']['id'];?></td>
		<td><?php echo $attachment_item['EbCompanyAttachment']['name'];?></td>
		<td><?php echo $attachment_item['SettingAttachmentType']['name'];?></td>
		<td><?php echo $fastest->czechDateTime($attachment_item['EbCompanyAttachment']['created']);?></td>
		<td>
			<a title='Editace přílohy "<?php echo $attachment_item['EbCompanyAttachment']['name'];?>"' 	class='ta edit' href='/eb_companies/attachs_edit/<?php echo $link;?>/<?php echo $attachment_item['EbCompanyAttachment']['id'];?>'/>edit</a>
			<a title='Odstranit přílohu'class='ta trash' href='/eb_companies/attachs_trash/<?php echo $link;?>/<?php echo $attachment_item['EbCompanyAttachment']['id'];?>'/>trash</a>
			<a title='Stáhnout přílohu'class='ta download' href='/eb_companies/attachs_download/eb_companies|attachment|<?php echo $attachment_item['EbCompanyAttachment']['file'];?>/<?php echo $attachment_item['EbCompanyAttachment']['alias_'];?>/'/>download</a>
		</td>
	</tr>
	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='5'>Nebyla nadefinována zatím žádna příloha</td>
	</tr>
	<?php endif;?>
</table>
<?php // pr($attachment_list);?>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='ListEbCompanyAttachmentClose' class='button'/>
	</div>
<script>
	$('ListEbCompanyAttachmentClose').addEvent('click', function(e){new Event(e).stop(); domwin.closeWindow('domwin_attach');});

	$('table_attachs').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tuto přílohu?')){
			new Request.HTML({
				url:this.href,
				update: $('domwin_attach').getElement('.CB_ImgContainer'),
				onComplete: function(){}
			}).send();
		}
	});
	
	$('domwin_attach').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_attach_add',
			sizes		: [580,390],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>