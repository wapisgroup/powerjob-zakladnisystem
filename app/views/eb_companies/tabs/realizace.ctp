<div class="sll"><?php echo $htmlExt->var_text('EbCompany/order_name',	array('label'=>'Zakázka'));?></div><br />

<table class='table' id='realizace_items'>
    <thead>
        <tr>
            <th>Pořadové číslo</th>
            <th>Popis činnosti</th>
            <th>Počet dní (předpoklad)</th>
            <th>Datum realizace</th>
            <th>Realizované</th>
            <th>Možnosti</th>
        </tr>
    </thead>
    <tbody>
    <?php 
    $i = 1;
    if(isset($order_items))
        foreach($order_items as $item):?>
        <tr>
            <td><?= $i;?>.</td>
            <td><?= $item['EbCompanyOrderItem']['name'];?></td>
            <td><?= $item['EbCompanyOrderItem']['finally_days'].' ('.$item['EbCompanyOrderItem']['supposed_days'].')';?></td>
            <td><?php echo $fastest->czechDate($item['EbCompanyOrderItem']['supposed_date']);
                      
                      if($item['EbCompanyOrderItem']['finally_days'] != 0){
                          echo ' - ';  
                          $datum = new DateTime($item['EbCompanyOrderItem']['supposed_date']);
                          $datum->modify('+'.$item['EbCompanyOrderItem']['finally_days'].' day');
                          echo $datum->format('d.m.Y');  
                      }  
                ?>
            </td>
            <td><?= $htmlExt->checkbox('EbCompanyOrderItem/'.$item['EbCompanyOrderItem']['id'],null,array('checked'=>($item['EbCompanyOrderItem']['stav'] == 1?'checked':null),'disabled'=>'disabled'));?></td>
            <td>
                <a title="Editovat bod" class="ta edit" href="/eb_companies/edit_order_item/<?= $this->data['EbCompany']['id']?>/<?= $item['EbCompanyOrderItem']['id'];?>/edit2/">Editovat</a>
            </td>
        </tr>
        <?php 
        $i++;
        endforeach;?>
    </tbody>
</table>
<script>

$('realizace_items').getElements('a.edit').addEvent('click', function(e){
        e.stop();
		domwin.newWindow({
			id			: 'domwin_order_item_edit',
			sizes		: [500,400],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		});
})

</script>