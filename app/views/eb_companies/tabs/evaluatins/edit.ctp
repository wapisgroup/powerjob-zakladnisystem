<form action='/eb_companies/edit_evaluation/' method='post' id='order_activity_edit_formular'>
	<div class="domtabs admin_dom2_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom2">
		<div class="domtabs field">
			<?php echo $htmlExt->hidden('EbCompanyEvaluation/id');?>
            <?php echo $htmlExt->hidden('EbCompanyEvaluation/type');?>
			<?php echo $htmlExt->hidden('EbCompanyEvaluation/eb_company_id');?>
			<fieldset>
				<legend>Základní informace</legend>
				<?php echo $htmlExt->input('EbCompanyEvaluation/name',array('tabindex'=>1,'label'=>'Název','class'=>'','label_class'=>''));?> <br class="clear">
				<?php echo $htmlExt->input('EbCompanyEvaluation/price',array('tabindex'=>2,'label'=>'Cena','class'=>'integer','label_class'=>''));?> <br class="clear">
				<?php echo $htmlExt->selectTag('EbCompanyEvaluation/currency',$currency_list2,null,array('tabindex'=>3,'label'=>'Měna'),null,false);?> <br class="clear">
				
                </fieldset>
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložiť' id='AddEditEbCompanyEvaluationSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditEbCompanyEvaluationClose'/>
	</div>
</form>
<script>
	var domtab = new DomTabs({'className':'admin_dom2'}); 
	
	$('AddEditEbCompanyEvaluationClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_evaluation_edit');});
	$('order_activity_edit_formular').getElements('.integer').inputLimit();
    function load_content_param(){ domtab.goTo(4); }
	$('AddEditEbCompanyEvaluationSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('order_activity_edit_formular');
		if (valid_result == true){
            button_preloader($('AddEditEbCompanyEvaluationSaveAndClose'));
			new Request.JSON({
				url:$('order_activity_edit_formular').action,		
				onComplete:function(){
				    domwin.loadContent('domwin',{'onComplete': 'load_content_param'}); 
					domwin.closeWindow('domwin_evaluation_edit');
                    button_preloader_disable($('AddEditEbCompanyEvaluationSaveAndClose'));
				}
			}).post($('order_activity_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('order_activity_edit_formular',{
		'EbCompanyEvaluationName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
		}
	});
	validation.generate('order_activity_edit_formular',<?php echo (isset($this->data['EbCompanyEvaluation']['id']))?'true':'false';?>);
</script>