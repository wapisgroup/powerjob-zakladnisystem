<div class="sll"><?php echo $htmlExt->var_text('EbCompany/order_name',	array('label'=>'Zakázka'));?></div><br />
<div id="evaluation_items">
    <h3 class="strong">Přímé náklady</h3>
    <table class='table'>
        <thead>
            <tr>
                <th>Pořadové číslo</th>
                <th>Popis činnosti</th>
                <th>Cena</th>
                <th>Měna</th>
                <th>Možnosti</th>
            </tr>
        </thead>
        <tbody>
            <tr><td colspan="5">
                <a href='/eb_companies/edit_evaluation/1/<?= $this->data['EbCompany']['id']?>' class="ta add" id='add_from_list_bonus' title='Přidat bod'>Přidat bod</a>
                &nbsp; Přidat další bod
                </td>
            </tr>
        <?php 
        $sum_prices = array(); 
        $i = 1;
        if(isset($evaluation_items))
            foreach($evaluation_items as $item)://typ 1
                if($item['EbCompanyEvaluation']['type'] == 2)
                    continue;
            ?>
            <tr>
                <td><?= $i;?>.</td>
                <td><?= $item['EbCompanyEvaluation']['name'];?></td>
                <td><?= $fastest->price($item['EbCompanyEvaluation']['price'],null);?></td>
                <td><?= $currency_list2[$item['EbCompanyEvaluation']['currency']];?></td>
                <td>
                    <a title="Editovat bod" class="ta edit" href="/eb_companies/edit_evaluation/1/<?= $this->data['EbCompany']['id']?>/<?= $item['EbCompanyEvaluation']['id'];?>">Editovat</a>
                </td>
            </tr>
            <?php 
            $sum_prices[$item['EbCompanyEvaluation']['currency']][$item['EbCompanyEvaluation']['type']] = (isset($sum_prices[$item['EbCompanyEvaluation']['currency']][$item['EbCompanyEvaluation']['type']])?$sum_prices[$item['EbCompanyEvaluation']['currency']][$item['EbCompanyEvaluation']['type']] + $item['EbCompanyEvaluation']['price']:$item['EbCompanyEvaluation']['price']);
            $i++;
            endforeach;
            
            foreach($currency_list2 as $cur_id => $cur){
                if($cur_id > 0){
                    if(isset($sum_prices[$cur_id][1]))
                        echo '<tr><td colspan="2">Přímé náklady celkem</td><td>'.$sum_prices[$cur_id][1].'</td><td>'.$cur.'</td></tr>';
                }
            }
            
            ?>
        </tbody>
    </table>
    <br />
    <h3 class="strong">Nepřímé náklady</h3>
    <table class='table'>
        <thead>
            <tr>
                <th>Pořadové číslo</th>
                <th>Popis činnosti</th>
                <th>Cena</th>
                <th>Měna</th>
                <th>Možnosti</th>
            </tr>
        </thead>
        <tbody>
            <tr><td colspan="5">
                <a href='/eb_companies/edit_evaluation/2/<?= $this->data['EbCompany']['id']?>' class="ta add" id='add_from_list_bonus' title='Přidat bod'>Přidat bod</a>
                &nbsp; Přidat další bod
                </td>
            </tr>
        <?php 
        $j = 1;
        if(isset($evaluation_items))
            foreach($evaluation_items as $item)://typ 2
                if($item['EbCompanyEvaluation']['type'] == 1)
                    continue;
            ?>
            <tr>
                <td><?= $j;?>.</td>
                <td><?= $item['EbCompanyEvaluation']['name'];?></td>
                <td><?= $fastest->price($item['EbCompanyEvaluation']['price'],null);?></td>
                <td><?= $currency_list2[$item['EbCompanyEvaluation']['currency']];?></td>
                <td>
                    <a title="Editovat bod" class="ta edit" href="/eb_companies/edit_evaluation/2/<?= $this->data['EbCompany']['id']?>/<?= $item['EbCompanyEvaluation']['id'];?>">Editovat</a>
                </td>
            </tr>
            <?php 
            $sum_prices[$item['EbCompanyEvaluation']['currency']][$item['EbCompanyEvaluation']['type']] = (isset($sum_prices[$item['EbCompanyEvaluation']['currency']][$item['EbCompanyEvaluation']['type']])?$sum_prices[$item['EbCompanyEvaluation']['currency']][$item['EbCompanyEvaluation']['type']] + $item['EbCompanyEvaluation']['price']:$item['EbCompanyEvaluation']['price']);
            $j++;
            endforeach;
            
            foreach($currency_list2 as $cur_id => $cur){
                if($cur_id > 0){
                    if(isset($sum_prices[$cur_id][2]))
                        echo '<tr><td colspan="2">Nepřímé náklady celkem</td><td>'.$sum_prices[$cur_id][2].'</td><td>'.$cur.'</td></tr>';
                }
            }
            
            ?>
        </tbody>
    </table>
    <br />
    <?php 
        foreach($currency_list2 as $cur_id => $cur){
            if($cur_id > 0){
                if(isset($sum_prices[$cur_id]))
                    echo '<label class="strong">Celkové náklady:</label> <var>'.array_sum($sum_prices[$cur_id]).',- '.$cur.'</var><br />';
            }
        }
    ?>
</div>
<br />
<div>
    <h3 class="strong">Poznámky</h3>
    <?php echo $htmlExt->textarea('EbCompany/evaluation_description',	array('class'=>'maximum'));?>
</div>
<br />
<script>

$('evaluation_items').getElements('a.edit,a.add').addEvent('click', function(e){
        e.stop();
		domwin.newWindow({
			id			: 'domwin_evaluation_edit',
			sizes		: [500,400],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		});
})

</script>