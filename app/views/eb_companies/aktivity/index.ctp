<div class="win_fce top">
<a href='/eb_companies/aktivity_edit/<?php echo $eb_company_id;?>' class='button edit' id='order_activity_add_new' title='Pridanie novej aktivity'>Pridať aktivitu</a>
</div>
<table class='table' id='table_kontakty'>
	<tr>
		<th>Názov</th>
		<th>Kapacita</th>
		<th>Problémy</th>
		<th>Vytvoril</th>
		<th>Vytvoreno</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($activity_list) && count($activity_list) >0):?>
	<?php foreach($activity_list as $activity_item):?>
	<tr>
		<td><?php echo $fastest->orez($activity_item['EbCompanyActivity']['name'],30);?></td>
		<td><?php echo $activity_item['EbCompanyActivity']['kapacita'];?></td>
		<td><?php echo $activity_item['EbCompanyActivity']['problem'];?></td>
		<td><?php echo $activity_item['CmsUser']['name'];?></td>
		<td><?php echo $fastest->czechDate($activity_item['EbCompanyActivity']['created']);?></td>
		<td>
			<a title='Úprava aktivity "<?php echo $activity_item['EbCompanyActivity']['name'];?>"' 	class='ta edit' href='/eb_companies/aktivity_edit/<?php echo $eb_company_id;?>/<?php echo $activity_item['EbCompanyActivity']['id'];?>'/>edit</a>
            <a title='Odstrániť položku'class='ta trash' href='/eb_companies/aktivity_trash/<?php echo $eb_company_id;?>/<?php echo $activity_item['EbCompanyActivity']['id'];?>'/>trash</a>
        </td>
	</tr>
	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='7'>K tejto zákazke nebola nájdená aktivita</td>
	</tr>
	<?php endif;?>
</table>
<?php // pr($activity_list);?>
	<div class='formular_action'>
		<input type='button' value='Zavrieť' id='ListEbCompanyActivityClose' class='button'/>
	</div>
<script>
	$('ListEbCompanyActivityClose').addEvent('click', function(e){new Event(e).stop(); domwin.closeWindow('domwin_aktivity');});

	$('table_kontakty').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tuto aktivitu?')){
			new Request.HTML({
				url:this.href,
				update: $('domwin_aktivity').getElement('.CB_ImgContainer'),
				onComplete: function(){}
			}).send();
		}
	});
	
	$('domwin_aktivity').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_aktivity_add',
			sizes		: [900,750],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>