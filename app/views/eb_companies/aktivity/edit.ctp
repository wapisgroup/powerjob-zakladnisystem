<form action='/eb_companies/aktivity_edit/' method='post' id='order_activity_edit_formular'>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Aktivita</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<?php echo $htmlExt->hidden('EbCompanyActivity/id');?>
			<?php echo $htmlExt->hidden('EbCompanyActivity/eb_company_id');?>
			<fieldset>
				<legend>Prosím vyplňte následující údaje</legend>
				<?php echo $htmlExt->input('EbCompanyActivity/name',array('tabindex'=>1,'label'=>'Název','class'=>'long','label_class'=>'long'));?> <br class="clear">
				<?php echo $htmlExt->textarea('EbCompanyActivity/kapacita',array('tabindex'=>2,'label'=>'Momentálna kapacita','class'=>'long','label_class'=>'long'));?><br class='clear' />
				<?php echo $htmlExt->textarea('EbCompanyActivity/prubeh',array('tabindex'=>3,'label'=>'Predpokladaný priebeh práce','class'=>'long','label_class'=>'long'));?><br class='clear' />
				<?php echo $htmlExt->textarea('EbCompanyActivity/material',array('tabindex'=>4,'label'=>'Požiadavky na materiál','class'=>'long','label_class'=>'long'));?><br class='clear' />
				<?php echo $htmlExt->textarea('EbCompanyActivity/lid_zdroje',array('tabindex'=>5,'label'=>'Požiadavky na ľudské zdroje','class'=>'long','label_class'=>'long'));?><br class='clear' />
				<?php echo $htmlExt->textarea('EbCompanyActivity/problem',array('tabindex'=>6,'label'=>'Problémy','class'=>'long','label_class'=>'long'));?><br class='clear' />
			</fieldset>
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložiť' id='AddEditEbCompanyActivitySaveAndClose' />
		<input type='button' value='Zavrieť' id='AddEditEbCompanyActivityClose'/>
	</div>
</form>
<script>
	var domtab = new DomTabs({'className':'admin_dom'}); 
	
	$('AddEditEbCompanyActivityClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_aktivity_add');});
	
	$('AddEditEbCompanyActivitySaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('order_activity_edit_formular');
		if (valid_result == true){
            button_preloader($('AddEditEbCompanyActivitySaveAndClose'));
			new Request.HTML({
				url:$('order_activity_edit_formular').action,		
				update: $('domwin_aktivity').getElement('.CB_ImgContainer'),
				onComplete:function(){	    
					domwin.closeWindow('domwin_aktivity_add');
                    button_preloader_disable($('AddEditEbCompanyActivitySaveAndClose'));
				}
			}).post($('order_activity_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('order_activity_edit_formular',{
		'EbCompanyActivityName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
		}
	});
	validation.generate('order_activity_edit_formular',<?php echo (isset($this->data['EbCompanyActivity']['id']))?'true':'false';?>);
</script>