<table class="table rozpocet_info">
    <tr>
        <th>Odberateľ</th>
        <td><?php echo $order_info['EbCompany']['name'];?></td>
    </tr>
    <tr>
        <th>Dodavateľ</th>
        <td>Estor Building s.r.o.</td>
    </tr>
</table><br />
<table class="table rozpocet_item">
    <tr>
        <th>#</th>
        <th>Kód cenníka</th>
        <th>Kód položky</th>
        <th>Popis</th>
        <th>Množstvo</th>
        <th>Merná jednotka</th>
        <th>Jednotková cena</th>
        <th>Spolu EUR</th>
        <th>Spolu SKK</th>
    </tr>
    <?php if (isset($items) && count($items)>0):?>
    <?php foreach($items as $key=>$item):?>
    <tr class="<?php echo (($key % 2)==0)?'':'odd';?>">
        <td><?php echo $key+1;?></td>
        <td><?php echo $item['RozpocetItem']['code'];?></td>
        <td><?php echo $item['RozpocetItem']['code_item'];?></td>
        <td><?php echo $item['RozpocetItem']['name'];?></td>
        <td><?php echo $item['RozpocetBlindItem']['mnozstvi'];?></td>
        <td> <?php echo $item['RozpocetItem']['mj'];?></td>
        <td><?php echo $item['RozpocetItem']['jc'];?></td>
        <td><?php echo $item['RozpocetItem']['cena'];?></td>
        <td><?php echo $item['RozpocetItem']['cena_sk'];?></td>
    </tr>
    <?php endforeach;?>
    <?php else:?>
    <tr>
        <td colspan="8" align="center">Nebola nájdená žiadna položka ...</td>
    </tr>
    <?php endif;?>
</table>