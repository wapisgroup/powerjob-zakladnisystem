<form action='/campaign_email/blacklist_add/' method='post' id='client_attachs_edit_formular'>
    <?php echo $htmlExt->hidden('CampaignEmailBlacklist/id'); ?>
	<fieldset>
					<?php echo $htmlExt->selectTag('CampaignEmailBlacklist/at_company_id', $realizator_list, null, array('tabindex'=>23,'label'=>'Realizátor'));?> <br class="clear">
			        <?php echo $htmlExt->input('CampaignEmailBlacklist/email',array('label'=>'Email'));?><br /> 
            </fieldset>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditClientMessageSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditClientMessageClose'/>
	</div>
</form>
<script>

	
	$('AddEditClientMessageClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_recruiter_add');});
	
	$('AddEditClientMessageSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('client_attachs_edit_formular');
		
		if (valid_result == true){
			new Request.JSON({
				url:$('client_attachs_edit_formular').action,		
				onComplete:function(json){
					domwin.closeWindow('domwin_recruiter_add');
                    domwin.loadContent('domwin');
				}
			}).post($('client_attachs_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
    
    validation.define('client_attachs_edit_formular',{
		'CampaignEmailBlacklistAtCompanyId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit realizátora'},
		},
		'CampaignEmailBlacklistEmail': {
			'testReq': {'condition':'email','err_message':'Musíte vyplnit email'},
		}
	});
	validation.generate('client_attachs_edit_formular',<?php echo (isset($this->data['Accommodation']['id']))?'true':'false';?>);

	
</script>