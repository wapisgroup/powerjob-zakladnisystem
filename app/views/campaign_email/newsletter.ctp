<form action='/campaign_email/newsletter/' method='post' id='newsletter_form'>
	<?php echo $htmlExt->hidden('CampaignEmail/id'); ?>
    <?php echo $htmlExt->hidden('CampaignEmail/type',array('value'=>1)); ?>			
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Text</a></li>
			<li class="ousko"><a href="#krok2">Odběratelé</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
            <?php echo $htmlExt->selectTag('CampaignEmail/at_company_id', $at_company_list, null,array('tabindex' => 1, 'label' => 'Realizátor','class'=>'long','label_class'=>'long')); ?><br />
            <div class="sll">
           	    <?php echo $htmlExt->input('CampaignEmail/email_from',array('readonly'=>(isset($this->data['CampaignEmail']['id']))?'readonly':false,'label'=>'Email-od'));?><br/>
            </div>
            <div class="slr">
                <?php echo $htmlExt->input('CampaignEmail/email_from_name',array('readonly'=>(isset($this->data['CampaignEmail']['id']))?'readonly':false,'label'=>'Email-od název'));?><br/>
            </div>
		 	<?php echo $htmlExt->input('CampaignEmail/name',array('readonly'=>(isset($this->data['CampaignEmail']['id']))?'readonly':false,'label'=>'Předmět emailu','class'=>'long','label_class'=>'long'));?><br/>
		 	<?php //echo $htmlExt->textarea('CampaignEmail/text',array('readonly'=>(isset($this->data['CampaignEmail']['id']))?'readonly':false,'label'=>'Text emailu','class'=>'long','label_class'=>'long','style'=>'height:300px'));?>
		  	<label class="long">Text:</label>
              <?php echo $wysiwyg->render('CampaignEmail/text',null,$logged_user['domena_upload']);?>
              <br class="clear" />
			
        </div>
		<div class="domtabs field">
		      <?php echo $this->renderElement('../campaign_email/news_choose_list');?>			
		</div>
	</div>
	<div class='formular_action' id='form_buttons'>
		<?php if (!isset($this->data['CampaignEmail']['id'])):?>
		<input type='button' value='Odeslat' id='send_sms_now' />
		<?php endif;?>
		<input type='button' value='Zavřít' id='close_sms_sender'/>
	</div>
	<div id='progress' class='none' style='width:200px; height:16px; padding:1px; position:absolute; top:45px; right:32px; border:1px solid #EFEFEF; text-align:center'>
		<div class='bar' style='background:green url(/css/fastest/action/progress_bar.png); width:0%; height:16px;'></div>
		<div class='label' style='position:absolute; top:1px; left:1px; width:200px; height:16px;'></div>
	</div>
</form>    
<script type="text/javascript">

	var domtab = new DomTabs({'className':'admin_dom'});
    $$('.wysiwyg').makeWswg();

    if($('send_sms_now'))
	$('send_sms_now').addEvent('click',function(e){
		new Event(e).stop();
        if ($$('.disabled_save').length != 0)
    			alert('Je aktivní HTML mód editoru. Přepněte jej prosím do normální módu.');
    	else {
            if(valid_form_newsletter()){
                $$('.wysiwyg').killWswg();
                new Request.JSON({
        			url: $('newsletter_form').action,
        			onComplete: function(json){
        			     if(json){
        			         if(json.result == true){
        			            alert(json.message);
                                domwin.closeWindow('domwin'); 
        			         }
                             else
                                alert('Odeslání se nezdařilo!Zkuste to později.');
        			     }
                         else
                            alert('Chyba aplikace');
        			}
        		}).send($('newsletter_form'));
            }
        }
	});
    
    // validace
	function valid_form_newsletter(){
		error = [];
		if ($('CampaignEmailName').value == '') error.push('Musíte vyplnit předmět emailu');
		if ($('CampaignEmailAtCompanyId').value == '') error.push('Musíte vyplnit realizátora kampaně');
	
		// validace pro wysiwyg
		textarea = $('CampaignEmailText').getPrevious('.textarea');
		textarea.level = -1;
		$('CampaignEmailText').value = generateHTLM(textarea);
		if ($('CampaignEmailText').value == '') error.push('Musíte vyplnit text emailu');
	
	
		if ($('CampaignEmailEmailFrom').value == '') error.push('Musíte vyplnit odchozí email');
		if ($('CampaignEmailEmailFromName').value == '') error.push('Musíte vyplnit popis odchozího emailu');
		
        if(is_selected_something() == false && $('CampaignEmailAdditionalRecipients').value == '') error.push('Musíte zvolit nějakou skupinu příjemců nebo další příjemce');
        //if ($('send_list').getElements('.wait_for_delivery').length == 0) error.push('Musíte zvolit příjemce Emailu');

		if (error.length > 0){
			var error_message = new MyAlert();
			error_message.show(error);
			return false;
		} else {
			return true;
		}
	}
    
    function is_selected_something(){
        var ret = false;
        $('group_list').getElements('input[type=checkbox]').each(function(item){
            if(item.checked){ ret = true; }
        })
            
        return ret;    
    }

	$('close_sms_sender').addEvent('click',function(e){
		new Event(e).stop();
        $$('.wysiwyg').killWswg();
		domwin.closeWindow('domwin');
	});
</script>