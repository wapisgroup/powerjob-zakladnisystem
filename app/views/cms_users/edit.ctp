<script>
	function upload_file_complete(foto){
		$('cms_user_foto').setProperty('src','/uploaded/cms_users/myself_foto/' + foto);
		alert('Obrázek byl nahrán');
	}
</script>
<form action='/cms_users/edit/' method='post' id='cms_user_edit_formular'>
	<?php echo $htmlExt->hidden('CmsUser/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
			<li class="ousko"><a href="#krok2">Přístupové údaje</a></li>
			<li class="ousko"><a href="#krok3">Facebook</a></li>
			<li class="ousko" id="ouskoMLM"><a href="#krok3">MLM</a></li>
			<li class="ousko" id="ouskoMLM2"><a href="#krok4">Spojení MLM</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Osobní údaje</legend>
				<div class="sll">  
					<?php echo $htmlExt->input('CmsUser/jmeno',array('tabindex'=>1,'label'=>'Jméno'));?> <br class="clear">
				</div>
				<div class="slr">  
					<?php echo $htmlExt->input('CmsUser/prijmeni',array('tabindex'=>2,'label'=>'Příjmení'));?> <br class="clear">
				</div>
			</fieldset>
			<fieldset>
				<legend>Kontaktní údaje</legend>
				<div class="sll">  
					<?php echo $htmlExt->input('CmsUser/email',array('tabindex'=>3,'label'=>'Email'));?> <br class="clear">
					<?php echo $htmlExt->input('CmsUser/telefon',array('maxlength'=>15,'tabindex'=>5,'label'=>'Telefon'));?> <br class="clear">
				</div>
				<div class="slr">  
					<?php echo $htmlExt->input('CmsUser/mobil',array('maxlength'=>15,'tabindex'=>4,'label'=>'Mobil'));?> <br class="clear">
					<?php echo $htmlExt->input('CmsUser/icq',array('tabindex'=>6,'label'=>'Skype'));?> <br class="clear">
				</div>
				<br class='clear' />
				<div class="sll">  
					<?php echo $htmlExt->input('CmsUser/mesto',array('tabindex'=>7,'label'=>'Město pracoviště'));?> <br class="clear">
					<?php $fileinput_setting = array(
						'label'			=> 'Fotografie',
						'upload_type' 	=> 'php',
						'filename_type'	=> 'unique',
						'file_ext'		=> array('jpg'),
						'paths' 		=> array(
							'path_to_file'	=> 'uploaded/cms_users/myself_foto/',
							'upload_script'	=> '/cms_users/upload_foto/',
							'status_path' 	=> '/get_status.php',
						),
						'methods'=>array(
								array('methods'	=>	array(array(
								'method'		=>	'resize',
								'new_width'		=>	130,
								'new_height'	=>	130
							)),
							'addon_path'=> ''
							)
						),
						'onComplete'	=> 'upload_file_complete'
					);?>
					<?php echo $fileInput->render('CmsUser/foto',$fileinput_setting);?>
				</div>
				<div class="slr">  
					<?php echo $htmlExt->input('CmsUser/born',array('tabindex'=>8,'label'=>'Datum narození'));?> <br class="clear">
					<?php 
						if (!empty($this->data['CmsUser']['foto'])){
							$url = '/uploaded/cms_users/myself_foto/'.$this->data['CmsUser']['foto'];
						} else {
							$url = '';
						}
						echo "<label>Náhled:</label><img src='$url' id='cms_user_foto' />";
					?>
				</div>
			</fieldset>
            <fieldset>
				<legend>Interní zaměstnanec</legend>
				<div class="sll">  
            		<?php echo $htmlExt->selectTag('CmsUser/at_employee_id',$at_employees_list,null,array('tabindex'=>1,'label'=>'jako iterní zaměstnanec'));?><br class="clear" />
		        </div>  
        	</fieldset>
			<div class="krok">
				<a href='#krok2' class='admin_dom_move next'>Další krok</a>
			</div>
		</div>
		<div class="domtabs field">
			<?php if (isset($this->data['CmsUser']['id'])):?>
			<fieldset>
				<legend>Statistiky</legend>
				<div class="sll">  
					<label>Vytvořen:</label><var><?php echo $fastest->czechDateTime($this->data['CmsUser']['created']);?> </var><br class="clear" />
					<label>Poslední přihlášení:</label><var><?php echo $fastest->czechDateTime($this->data['CmsUser']['last_log']);?></var><br class="clear" />
				</div>
				<div class="slr">  
					<label>Poslední úprava:</label><var><?php echo $fastest->czechDateTime($this->data['CmsUser']['updated']);?></var><br class="clear" />
				</div>
			</fieldset>
			<?php endif;?>
			<fieldset>
				<legend>Přístupové údaje</legend>
				<div class="sll">  
					<?php echo $htmlExt->selectTag('CmsUser/cms_group_id',$cms_group_list,null,array('tabindex'=>1,'label'=>'Skupina'),null,false);?><br class="clear" />
				</div>
				<div class="slr">  
					<?php // echo isset($this->data['CmsUser']['user_name'])?'<label>Uživatelské jméno:</label><var>'.$this->data['CmsUser']['user_name'].'</var>':$htmlExt->input('CmsUser/user_name',array('tabindex'=>2,'label'=>'Uživatelské jméno'));?> <br class="clear">
				</div>
				
				<?php if (isset($this->data['CmsUser']['id'])):?>
					<div class="sll">
					<label>Změnit heslo:</label><input type='checkbox' id='show_hide_pass'/><br class='clear'/>
					</div>
				<?php endif;?>
				
				<div class="sll password_enabled">  
					<?php echo $htmlExt->password('CmsUser/heslo',array('tabindex'=>3,'label'=>'Heslo','value'=>''));?> <br class="clear">
				</div>
				<div class="slr password_enabled">  
					<?php echo $htmlExt->password('CmsUser/heslo2',array('tabindex'=>4,'label'=>'Potvrzení hesla'));?> <br class="clear">
				</div>
			</fieldset>
		<div class="krok">
			<a href='#krok1' class='admin_dom_move prev'>Předchozí krok</a>
		</div>
		</div>
        <div class="domtabs field">
            <fieldset>
                <legend>Facebook napojeni</legend>
                <div class="sll">
                    <?php echo $htmlExt->input('CmsUser/fcb_token',array('tabindex'=>8,'label'=>'Token'));?> <br class="clear">
                </div>
                <div class="slr">
                    <?php echo $htmlExt->input('CmsUser/fcb_session_key',array('tabindex'=>8,'label'=>'SessionKey'));?> <br class="clear">
                </div>
                <br class="clear"/>
            </fieldset>
        </div>
		<div class="domtabs field">
			<fieldset>
				<legend>MLM kód</legend>
				<div class="sll"> 
				<?php echo $htmlExt->input('CmsUser/mlm_code',array('tabindex'=>8,'label'=>'MLM kód'));?> <br class="clear">
				</div>
			</fieldset>
			<fieldset>
				<legend>MLM nastaveni</legend>
				<div id="MLMcode"></div>
				<div class="sll">  
					<?php echo $htmlExt->selectTag('CmsUser/mlm_setting/kraj',$mlm_kraj_list,null,array('tabindex'=>1,'label'=>'Kraj','class'=>'mlmField'),null,true);?><br class="clear" />
					<?php echo $htmlExt->input('CmsUser/mlm_setting/oz',array('tabindex'=>8,'label'=>'Obchodní zastoupení','MAXLENGTH'=>2,'class'=>'integer mlmField'));?> <br class="clear">
					<?php echo $htmlExt->selectTag('CmsUser/mlm_setting/mena',$mlm_mena_list,null,array('tabindex'=>1,'label'=>'Měna','class'=>'mlmField'),null,true);?><br class="clear" />
					<?php echo $htmlExt->input('CmsUser/mlm_setting/pc',array('tabindex'=>8,'label'=>'Pořadové číslo','MAXLENGTH'=>5,'class'=>'integer mlmField'));?> <br class="clear">
					<?php echo $htmlExt->selectTag('CmsUser/mlm_setting/krajina',$mlm_krajina_list,null,array('tabindex'=>1,'label'=>'Krajina','class'=>'mlmField'),null,true);?><br class="clear" />
				</div>
			</fieldset>
		</div>
		<div class="domtabs field">
			<fieldset>
				<legend>Nastavení</legend>
				<div class="sll">  
					<?php echo $htmlExt->selectTag('ConnectionMlmRecruiter/mlm_group_id',$mlm_group_list,null,array('label'=>'Skupina MLM'),null,true);?><br />
				</div><br />
			</fieldset>
								
			<div id="recruiter"></div>	
		</div>
	</div>
	
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','class'=>'button'));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','class'=>'button'));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var code = new Array('X','XX','X','XXXXX','XX');
	$('ouskoMLM2').addClass('none');
	
	if($('CmsUserCmsGroupId').value != 8) {
		$('ouskoMLM').addClass('none');
		$('CmsUserMlmCode').setValue(code.join(""));
	}
	else {
		var code = new Array($('CmsUserMlmSettingKraj').value,$('CmsUserMlmSettingOz').value,$('CmsUserMlmSettingMena').value,$('CmsUserMlmSettingPc').value,$('CmsUserMlmSettingKrajina').value);
	}
	
	
	//MLM Kod
	$$('.mlmField').addEvents({
		'change': function(e){
			if(this.name == 'data[CmsUser][mlm_setting][kraj]') code[0]=this.value;
			if(this.name == 'data[CmsUser][mlm_setting][oz]') code[1]=this.value;
			if(this.name == 'data[CmsUser][mlm_setting][mena]') code[2]=this.value;
			if(this.name == 'data[CmsUser][mlm_setting][pc]') code[3]=this.value;
			if(this.name == 'data[CmsUser][mlm_setting][krajina]') code[4]=this.value;
			$('CmsUserMlmCode').setValue(code.join(""));
		
			
		}
	});

	$('cms_user_edit_formular').getElements('.integer').inputLimit();

	$('CmsUserCmsGroupId').addEvent('change',function(){
		if(this.value == 8){
			if($('CmsUserId').value == '')
				$('ouskoMLM2').removeClass('none');
			
			$('ouskoMLM').removeClass('none');	
		}
		else {
			$('ouskoMLM').addClass('none');
			$('ouskoMLM2').addClass('none');	
		}
	});


	//pro vytvoreni MLM spojeni nacteni aktualnich dat pro skupiny L1,L2 jejich mozna parenty
	$('ConnectionMlmRecruiterMlmGroupId').addEvent('change', function(){
		if( this.value > 1){
		  	new Request.HTML({
		  		url: '/setting_mlm_recruiters/load_recruiters/' + this.value + '/' + 0+'/'+0,
		  		update:'recruiter'
		  	}).send();
		}
		else $('recruiter').empty();
	});

	var domtab = new DomTabs({'className':'admin_dom'}); 


	<?php if (isset($this->data['CmsUser']['id'])):?>
		$$('.password_enabled').setStyle('display','none');
		$('show_hide_pass').addEvent('click',function(){
			pass = $$('.password_enabled');
			if (pass[0].getStyle('display') == 'none'){
				pass.setStyle('display','block');
				pass.getElements('.require, .invalid, .valid').each(function(hide){
					hide.removeClass('valid')
					hide.addClass('require');
				});
			} else {
				pass.setStyle('display','none');
				pass.getElements('.require, .invalid, .valid').each(function(hide){
					hide.setValue('');
					hide.removeClass('require')
					hide.removeClass('invalid')
					hide.addClass('valid');
				});
			}
		});
	<?php endif;?>

	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('cms_user_edit_formular');
		
		if($('CmsUserCmsGroupId').value==8 && $('CmsUserMlmCode').value.length != 11) 
			if(valid_result == true)
				valid_result = Array("Kód musi mit 11 znaků");
			else
				valid_result[valid_result.length]="Kód musi mit 11 znaků";

		if (valid_result == true){
			new Request.JSON({
				url:$('cms_user_edit_formular').action,		
				onComplete:function(){
					click_refresh($('CmsUserId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('cms_user_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('cms_user_edit_formular',{
		'CmsUserUserName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit uživatelské jméno'},
			'length': {'condition':{'min':3,'max':10}, 'err_message':'Zadejte v rozsahu 3-10 znaků'},
			'not_equal': {'condition':'admin','err_message':'Nesmi se jmenovat "admin"'},
			'isUnique':{'condition':{'model':'CmsUser','field':'user_name'},'err_message':'Toto uživatelské jméno je již použito'}
		},
		'CmsUserJmeno': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit jméno'},
		},
		'CmsUserEmail': {
			'testReq': {'condition':'email','err_message':'Musíte vyplnit email ve správném formátu'},
		},
		'CmsUserPrijmeni': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit příjmení'},
		},
		'CmsUserHeslo': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit heslo'},
			'length': {'condition':{'min':5,'max':20}, 'err_message':'Heslo zadejte v rozsahu 5-20 znaků'},
		
		},
		'CmsUserHeslo2':{
			'isConfirm':{'condition':{'secObject':'CmsUserHeslo'},'err_message':'Potvrzení hesla se neshoduje!'}
		}
	});
	validation.generate('cms_user_edit_formular',<?php echo (isset($this->data['CmsUser']['id']))?'true':'false';?>);
</script>