<form id='add_edit_todo_formular' action='/todos/edit/' method='post'>
	<?php echo $htmlExt->hidden('Todo/id');?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<?php echo $htmlExt->input('WapisTask/name',array('label'=>'Název úkolu', 'class'=>'long','label_class'=>'long'));?><br />
			<?php echo $htmlExt->textarea('WapisTask/description',array('label'=>'Text úkolu', 'class'=>'long','label_class'=>'long','style'=>'height:300px'));?><br />		
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='AddEditTodoClose'/>
	</div>
</form>
<script>

	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('AddEditTodoClose').addEvent('click',function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin');
	});
	/*
	
	$('AddEditTodoSaveAndClose').addEvent('click', function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('add_edit_todo_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('add_edit_todo_formular').action,		
				onComplete:function(){
					click_refresh($('TodoId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('add_edit_todo_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('add_edit_todo_formular',{
		'TodoName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název úkolu'}
		}		
	});
	validation.generate('add_edit_todo_formular',<?php echo (isset($this->data['WapisTask']['id']))?'true':'false';?>);
	*/
</script>
