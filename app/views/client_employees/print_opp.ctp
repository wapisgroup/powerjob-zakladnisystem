<table class='table table100'>
    <thead>
        <tr>
            <th colspan="2" align="left">Komu</th>
            <th colspan="4" align="left"><?= $komu;?></th>
        </tr>
        <tr>
            <th colspan="2" align="left">Kdy</th>
            <th colspan="4" align="left"><?= date('d.m.Y');?></th>
        </tr>
        <tr>
            <th colspan="2" align="left">Kdo</th>
            <th colspan="4" align="left"><?= $logged_user['CmsUser']['name'];?></th>
        </tr>
        <tr>
            <th colspan="6">&nbsp;</th>
        </tr>
        <tr>
            <th>Druh</th>
            <th>Typ</th>
            <th>Velikost</th>
            <th>Údaje klienta</th>
            <th>Potvrzení</th>
            <th>Nárok</th>
        </tr>
    </thead>
	<tbody align="center">
			<?php if (isset($opp) && count($opp)>0):?>
			<?php foreach($opp as $druh=>$item):?>
			<tr>
                <td><?php echo $druh;?> </td>
                <td><?php echo $item['name'];?> </td>
	    		<td><?php echo $item['size'];?> </td>
                <td><?php echo $item['client'];?> </td>
	    		<td><?php echo $ano_ne_checkbox[($item['potvrzeni'] == 'on' || $item['potvrzeni'] == '1' ? 1 : 0)];?> </td>
	    		<td><?php echo $item['narok'];?></td>
	    	</tr>
			<?php endforeach;?>
			<?php else :?>
			<?php endif;?>
	 </tbody>
</table>
<script>
window.print();
</script>