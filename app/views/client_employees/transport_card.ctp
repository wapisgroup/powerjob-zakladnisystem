<form action='/client_employees/transport_card_print/' method='post' id='add_down_payments'>
			<fieldset>
				<legend>Nastavení tisku kartiček</legend>
					<?php echo $htmlExt->selectTag('route_id',$route_list,null,array('tabindex'=>1,'label'=>'Trasa','class'=>'','label_class'=>''));?> <br class="clear">
				    <?php echo $htmlExt->inputDate('from_date',array('tabindex'=>1,'label'=>'Platnost - od','class'=>'','label_class'=>''));?> <br class="clear">
				    <?php echo $htmlExt->inputDate('to_date',array('tabindex'=>1,'label'=>'Platnost - do','class'=>'','label_class'=>''));?> <br class="clear">
            </fieldset>
	<div class='formular_action'>
		<?php echo $html->link('Tisk','/client_employees/transport_card_print/',array('id'=>'print_button','class'=>'mr5 button','target'=>'_blank'));?>
    	<input type='button' value='Zavřít' id='AddEditClientMessageClose'/>
	</div>
</form>
<script>
   
    $('AddEditClientMessageClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_cards');});
	
	$('print_button').addEvent('click',function(e){
		valid_result = validation.valideForm('add_down_payments');
		if (valid_result != true){ 
		    e.stop();
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
    
    $('RouteId').addEvent('change',make_link);
    $('calbut_FromDate').addEvent('change',make_link);
    $('calbut_ToDate').addEvent('change',make_link);  
     
     function make_link(){
        var route = $('RouteId').value;
        var from = $('calbut_FromDate').value;
        var to = $('calbut_ToDate').value;
        $('print_button').setProperty('href','<?= $refresh_url;?>&route_id='+route+'&from_date='+from+'&to_date='+to);
     }  

	validation.define('add_down_payments',{
		'ReportDownPaymentAmount': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit částku'},
		}
	});

	validation.generate('add_down_payments',false);
    
</script>