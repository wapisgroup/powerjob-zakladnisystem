<?php 
function get_client_info($kind,$client){
    $return = '';
    $kind = strtolower($kind);
    switch($kind){
        case 'obuv':
            $return = $client['Client']['opp_obuv'];
            break;
        case 'bluza':
            $return = $client['Client']['opp_bluza'];
            break;
        case 'nohavice':
            $return = $client['Client']['opp_nohavice'];
            break;
        case 'tricko':
            $return = $client['Client']['opp_tricko'];
            break;            
    }
    
    return $return;
}
?>
<form action="/client_employees/add_opp/" id="add_opp_form" method="POST">
<?php echo $htmlExt->hidden('client_id',array('value'=>$client_id));?>
<?php echo $htmlExt->hidden('company_id',array('value'=>$company_id));?>
<?php echo $htmlExt->hidden('connection_id',array('value'=>$connection_id));?>
<table class="table">
    <thead>
        <tr>
            <th>Druh</th>
            <th>Typ</th>
            <th>Velikost</th>
            <th>Údaje klienta</th>
            <th>Potvrzení</th>
            <th>Nárok</th>
        </tr>
    </thead>
    <tbody>
    <?php 
        foreach($kinds as $kind){
            echo '<tr>';
            echo '<td>'.$kind.'</td>';
            echo '<td>'.$htmlExt->selectTag($kind.'/name',$types[$kind],null,array('class'=>'select_typ')).'</td>';
            echo '<td>'.$htmlExt->selectTag($kind.'/size',array(),null,array('class'=>'select_size')).'</td>';
            echo '<td>'.get_client_info($kind,$client).$htmlExt->hidden($kind.'/client',array('value'=>get_client_info($kind,$client))).'</td>';
            echo '<td>'.$htmlExt->checkbox($kind.'/potvrzeni',null,array('checked'=>(isset($work_position[$kind]['yn']) && $work_position[$kind]['yn'] == 1?'checked':null),'class'=>'potvrzeni')).'</td>';
            
            $narok = 'ne';
            if(isset($work_position[$kind]['yn']) && $work_position[$kind]['yn'] == 1){
                if(isset($last_opp_client_date[$kind])){
                    $datetime1 = strtotime('now');
                    $datetime2 = strtotime($last_opp_client_date[$kind]);
        			
        			$day1 = date("j", $datetime2);
        			$mon1 = date("n", $datetime2);
        			$year1 = date("Y", $datetime2);
        			$day2 = date("j", $datetime1);
        			$mon2 = date("n", $datetime1);
        			$year2 = date("Y", $datetime1);
    
                    if($day1>$day2) {
    						$mdiff = (($year2-$year1)*12)+($mon2-$mon1-1);
    					} else {
    						$mdiff = (($year2-$year1)*12)+($mon2-$mon1);
    				}
                    
                    if(isset($work_position[$kind]) && $mdiff > $work_position[$kind]['interval'])
                        $narok = 'ano';  
                    
                }
                else
                    $narok = 'ano';
            }    
            
            echo '<td>'.$htmlExt->input($kind.'/narok',array('value'=>$narok)).'</td>';
            echo '</tr>';
        }
    
    ?>
    </tbody>
</table>
<div class='formular_action'>
		<input type='button' value='přidělit OPP' id='AddOppSave' />
        <input type='button' value='Tisknout' id='PrintOpp'/>
		<input type='button' value='Zavřít' id='AddOppClose'/>
		<input type='button' value='historie' id='ShowHistory'/>
	</div>
</form>
<script>
var size_array = JSON.decode('<?php echo json_encode($sizes);?>');

$('AddOppClose').addEvent('click',function(){domwin.closeWindow('domwin')});
$('PrintOpp').addEvent('click',function(){
       // domwin.closeWindow('domwin')
       save_url =  $('add_opp_form').getProperty('href');
       $('add_opp_form').setProperty('target','_blank');
       $('add_opp_form').setProperty('action','/client_employees/print_opp/');
       $('add_opp_form').submit();
       $('add_opp_form').removeProperty('target');
       $('add_opp_form').setProperty('action',save_url);
});

/**
 * Fce pro validaci formulare pro pridani opp
 * validujeme vsechny radky, pokud maji zaskrtnuto potvrzeni
 * musi mit vyplneny typ a velikost
 */
function validate_add_opp_form(){
    var message = new Array();
   
    $each($('add_opp_form').getElement('tbody').getElements('tr'),function(tr){
        var druh = tr.getElement('td:first').getHTML();
        
        if(tr.getElement('input.potvrzeni').checked && (tr.getElement('select.select_typ').value == '' || tr.getElement('select.select_size').value == '')){
            message.push(druh + " - nemá zvolen typ nebo velikost");
        }        
    });
    
    if(message.length < 1)
        return true;
    else 
        return message.join("\n");   
}


/**
 * Zmena typu. nacteni prislusnych velikosti
 */
$$('.select_typ').addEvent('change',function(){
    obj = this.getParent('tr').getElement('.select_size');
    obj.empty();
    
    if(this.value != ''){
        $each(size_array[this.value], function(value){
        	new Element('option', {title:value, value:value}).setHTML(value).inject(obj);
        }, this);
    }

});

$('AddOppSave').addEvent('click', function(e){
		e.stop();
        var v_result = validate_add_opp_form();
        
        if(v_result == true){ 
    		new Request.JSON({
    			url: $('add_opp_form').getProperty('action'),
    			onComplete: function(json){
    				if (json){
    					if (json.result === true){
    						alert('Přídání opp bylo úspěšné');
    						domwin.closeWindow('domwin');
    						click_refresh();
    					} else {
    						alert(json.message);
    					}
    				} else {
    					alert('Chyba aplikace');
    				}
    			}
    		}).send($('add_opp_form'));
            
        }
        else{
            alert(v_result);
        }
})


 
$('ShowHistory').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_history_client_opp',
			sizes		: [800,800],
			scrollbars	: true,
			title		: 'Historie přidělení OPP',
			languages	: false,
			type		: 'AJAX',
			ajax_url	: '/client_employees/history_client_opp/'+$('ClientId').value+'/',
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
});
</script>