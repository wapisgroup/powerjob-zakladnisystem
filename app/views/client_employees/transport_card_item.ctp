<div class="transport_card_item">
    <img src="../../css/fastest/layout/at_logo.png" class="logo"/>
    <img src="../../css/fastest/layout/at_bus.png" class="bus"/>
    <div class="tci_id"><?= $client['ClientView']['id'];?></div>
    <div class="tci_name"><?= $client['ClientView']['name'];?></div>
    <div class="tci_company">Podnik: <?= $client['ClientView']['company'];?></div>
    <div class="tci_route">Platná na trase: <?= $route;?></div>
    <div class="tci_validity"><?= $fastest->czechDate($from_date).' - '.$fastest->czechDate($to_date);?></div>
</div>