<?php
class WysiwygHelper extends AppHelper {
	var $helpers = array('HtmlExt');
	
	function render($field,$value=null,$domain=null){
		$lang = explode('/',$field);
		$out = "
			<div class='wysiwyg'>
			<div class='toolbar'>
				<button class='bold nda' title='Tučné'></button>
				<button class='italic nda' title='Kurzíva'></button>
				<button class='underline nda' title='Podtrženo'></button>
				<button class='strikethrough nda' title='Přeškrnuto'></button>
				<button class='justifyleft nda'  title='Zarovnat doleva'></button>
				<button class='justifyright nda' title='Zarovnat doprava'></button>
				<button class='justifycenter nda' title='Zarovnat na střed'></button>
				<button class='justifyfull nda'  title='Zarovnat do bloku'></button>
				
				<select class='format' title='Formát textu' style='width:100px'>
					<option value=''>Normální text</option>
					<option value='<p>'>Odstavec</option>
					<option value='<address>'>Adresa</option>
					<option value='<pre>'>Předformát</option>
					<option value='<h2>'>Nadpis 2</option>
					<option value='<h3>'>Nadpis 3</option>
					<option value='<h4>'>Nadpis 4</option>
					<option value='<h5>'>Nadpis 5</option>
					<option value='<h6>'>Nadpis 6</option>
				
				</select>
				<button class='cut' title='Vyjmout'></button>
				<button class='copy' title='Kopírovat'></button>
				<button class='paste nda' title='Vložit'></button>
				
				<button class='insertorderedlist nda' title='Číselný seznam'></button>
				<button class='insertunorderedlist nda' title='Odrážkový seznam'></button>
				<button class='link' title='Odkaz'></button>
				<button class='unlink' title='Zrušit odkaz'></button>
				<button class='anchor' title='Kotva'></button>
				<button class='image' title='Obrázek'></button>
				<button class='fullscreen nda'  title='Na celou obrazovku'></button>
				<button class='html' title='Editace HTML'></button>
				<button class='superscript nda' title='Horní index'></button>
				<button class='subscript nda' title='Dolní index'></button>
                <select class='insertPanel none' title='Formát textu' style='width:100px'></select>
				<br class='clear'/>
				<input value='".$domain."' type='hidden' id='FotoEditorDomain'/>
			</div>
			<div class='textarea' style='clear:left'  contenteditable='true' id='edit_area_".$lang[1]."'>".$value."</div>
			".$this->HtmlExt->textarea($field,array('class'=>'text_area'))."
			<div class='footer'></div>
			<div class='over_div' style='background:#aaa; opacity:0.3; position:absolute; top:20px; left:0px'></div>
			<div class='popupWindow'>
				<h3 class='popuph3'>Nadpis <span class='popupClose'></span></h3>
				<div class='popup'></div>
			</div>
		</div>";
		return $out;
	}
}
?>