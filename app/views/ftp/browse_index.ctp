<div class="domtabs admin_file_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Správce souborů</a></li>
		</ul>
</div>
<div class="domtabs admin_file">
<div class="domtabs field">


<div id="filemanager_nahled">
	<strong class="title">Náhled</strong>
		<var>Vyberte si soubor, který  si přejete vložit</var>
		<br />
		<div id='file_preview'>
		
		</div>
		
	
	<div class="win_save">
		<?php if (isset($JSinput)):?>
			<input type='hidden' id='JSinput' value='<?php echo $JSinput;?>'/>
			<?php echo $htmlExt->button('Vložit',array('id'=>'insert_image'));?>
		<?php endif;?>
		
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close_browser'));?>
	</div>
</div>

<div id='file_list'>
	<?php echo $this->renderElement('../ftp/browse_items');?>
</div>
<br />
</div>
</div>
<script language="JavaScript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_file'}); 
	$('close_browser').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('filemanager');});
	
	if ($('insert_image')){
		$('insert_image').addEvent('click',function(e){
			
			new Event(e).stop();
			id = $('JSinput').value;
			if ($(id)){
				$(id).value = $('selected_file').value;
				domwin.closeWindow('filemanager');
			} else {
				alert('Vystupni ID prvku neexistuje');
				domwin.closeWindow('filemanager');
			}
		});
	}
	
 </script>