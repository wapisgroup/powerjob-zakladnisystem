<div id="filemanager_obal">
<?php //$logged_user['ftp_jedno'] = 'systém/uploaded/';?>
    <div class="sll" >
        <?php 
            
        ?>
        <var><?php echo 'Využito '.round((($usage_size_mb/$limit)*100),2).' %';?></var><br />
        <var><?php echo 'Využito '.$fastest->get_size($usage_size).' z dostupných '.round(($limit/1024),2).' GB ';?></var><br />
        <?php if (!empty($ftp_jedno)){ ?>	
        	<?php 
        		if ($directory.'/' != $ftp_jedno) 
        			$dir_t = Str_Replace($ftp_jedno,'',$directory); 
        		else 
        			$dir_t ='';
        	?>
        	
        	<var><?php echo 'Právě se nacházíte: usershare'.$dir_t ?></var>
        <?php } else { ?>	
        	<var><?php echo 'Právě se nacházíte: usershare'.$directory ?></var>
        <?php } ?>
    
    </div>
    <?php if(isset($permission['create_folder']) && $permission['create_folder'] == true){ ?>
	<div class="slr" ><label><?php echo 'Vytvořit nový adresář' ?>:</label><input type='text' id='new_directory'/><?php echo $html->image(MASTER_SERVER.'/css/fastest/filebrowser/add.png',array('title'=>'Vytvořit nový adresář','alt'=>'Vytvořit','id'=>'create_directory','class'=>'icon')) ?></div>
	<?php } ?>
    <div id="filebrowser_preloader"></div>
	<br />
	<br />
	
	
	
<div id="filebrowser_obal">
<table class='table filemanager'>

<tr>
	<th>Název</th>
	<th class="datum">Velikost</th>
	<th class="datum">Datum</th>
	<th class="datum">Možnosti</th>
</tr>
<?php 
//pr($dir);
	if (($ftp_jedno.'/' == $current_dir) || ($ftp_jedno == $current_dir))  
		unset ($dir['..']);
		
		foreach ($dir as $dirname => $dirinfo) {
			//if (($dirname <> '.') AND ($current_dir.$dirname != '/..'))){
			
			if (($dirname <> '.') AND ($current_dir.$dirname != '/..')){
			
				echo '<tr>';
					echo '<td><strong>'.$html->image(MASTER_SERVER.'/css/fastest/filebrowser/folder.png',array('alt'=>'Adresar','class'=>'icons')).$html->link($dirname,'/ftp/usershare/'.$current_dir.$dirname,array('title'=>'Otevřít adresář','class'=>'change_directory','onclick'=>'return false;')).'</strong></td>';
					echo '<td class="file_size">DIR</td>';
					echo '<td>'.$dirinfo['day'].' '.$dirinfo['month'].' '.$dirinfo['time'].'</td>';
					
					echo '<td>';
						if ($dirname <> '..' && (isset($permission['delete_folder']) && $permission['delete_folder'] == true))
							echo $html->link($html->image(MASTER_SERVER.'/css/fastest/filebrowser/delete.png',array('alt'=>'Smazat adresář', 'title'=>'Smazat adresář','class'=>'icons')),'/ftp/delete_directory/'.$current_dir.$dirname,array('class'=>'delete_directory'),null,false);
					echo '</td>';
				echo '</tr>';
			}	
		}
		foreach ($file as $filename => $fileinfo) {
				echo '<tr>';
                    
                
					if ($current_dir <> '')
						$dir = '/'.$current_dir.$filename;
					else
						$dir = $current_dir.$filename;
                        
                        $file_dl = Str_Replace($ftp_jedno,'',$dir);
						//$file = .$path_to_domena.'/'.ltrim($dir,"/");
						$pripona = strtolower(end(Explode(".", $filename)));
						$pripony_img = array ('jpg','png','gif','jpeg');
						if (in_array($pripona,$pripony_img)){
							if (!empty($ftp_jedno)){
								$dir_ = Str_Replace($ftp_jedno,'',$dir);
								
								$file = 'load_image/usershare|'.Str_Replace('/','|',ltrim($dir_,"/"));
							} else {
								$dir_ = Str_Replace($ftp_jedno,'',$dir);
								$file = 'load_image/usershare|'.Str_Replace('/','|',ltrim($dir_,"/"));
							}
						}
						else 
							$file = 'load_file/'.Str_Replace('/','|',ltrim($dir,"/"));
						
					echo '<td>'.$fastest->file_icon($filename).' '.$html->link($filename,$file,array('class'=>(in_array($pripona,$pripony_img))?'show_img':'show_file')).'</td>';
					echo '<td class="file_size">'.$fastest->get_size($fileinfo['size']).'</td>';
					echo '<td>'.$fileinfo['day'].' '.$fileinfo['month'].' '.$fileinfo['time'].'</td>';
					echo '<td>';
                    
                    if(isset($permission['delete_file']) && $permission['delete_file'] == true)
                        echo $html->link($html->image(MASTER_SERVER.'/css/fastest/filebrowser/delete.png',array('alt'=>'Smazat soubor', 'title'=>'Smazat soubor','class'=>'icons')),'/ftp/delete_file/'.ltrim($dir,"/"),array('class'=>'delete_file'),null,false).' ';
                    
                    if(isset($permission['download_file']) && $permission['download_file'] == true)
                        echo "<a title='Stáhnout přílohu'class='ta download' href='/ftp/attachs_download/".Str_Replace('/','|',ltrim($file_dl,"/"))."/".$filename."/'/>download</a></td>";

				echo '</tr>';
		}
?>

</table>
</div>
<br />
<div class="sll" >  

<?php $fileinput_setting = array(
		'label'			=> 'Nahrát soubor',
		'upload_type' 	=> 'ftp',
		'filename_type'	=> 'alias',
		'paths' 		=> array(
			'path_to_file'	=> $directory.'/',
			'upload_script'	=> '/ftp/upload_file/',
			'status_path' 	=> '/get_status.php',
		),
		
		'onComplete'	=> 'refresh_dir'
	);?>
	<?php 
    if(isset($permission['upload_file']) && $permission['upload_file'] == true){ 
        echo $fileInput->render('UserShare/file',$fileinput_setting);
    }
    ?>
 </div>
 <div class="slr" >  
 </div>
 <br />

</div>
 <br />
	
<script>
	function refresh_dir(){
		new Request.HTML({
			url : '/<?php echo $this->params['url']['url'];?>',
			update: $('file_list_max')
		}).send();
	}

    if($('create_directory'))
	$('create_directory').addEvent('click',function(e){
		new Event(e).stop();
		var name = $('new_directory').value;
		if (name != ''){
			filebrowser_preloader();
			new Request.JSON({
				url : '/ftp/create_directory/' + '/<?php echo $this->params['url']['url'];?>/' + name,
				onComplete:function(){
					refresh_dir()
				}
			}).send();
		}
	});
    
    if($$('.change_directory'))
	$$('.change_directory').addEvent('click',function(e){
		new Event(e).stop();
		filebrowser_preloader();
		new Request.HTML({
			url : this.href,
			update: $('file_list_max')
		}).send();
	});

	if($$('.show_img'))
	$$('.show_img').addEvent('click',function(e){
		new Event(e).stop();
        /*
		$('file_preview').addClass('preload');
		new Request.HTML({
				url : this.href,
				update: $('file_preview'),
				onComplete:function(){
					$('file_preview').removeClass('preload');
				}
			}).send();
	   */
    });
    
    if($$('.show_file'))
	$$('.show_file').addEvent('click',function(e){
		new Event(e).stop();
        /*
		$('file_preview').addClass('preload');
		new Request.HTML({
				url : this.href,
				update: $('file_preview'),
				onComplete:function(){
					$('file_preview').removeClass('preload');
				}
			}).send();
        */
	});
	
    if($$('.delete_directory'))
	$$('.delete_directory').addEvent('click',function(e){
		new Event(e).stop()
		if (confirm('Opravdu chcete smazat adresář?')){
			filebrowser_preloader();
			new Request.JSON({
				url : this.href,
				onComplete:function(){
					refresh_dir()
				}
			}).send();
		}
	});
	
    if($$('.delete_file'))
	$$('.delete_file').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu chcete smazat soubor?')){
			new Request.JSON({
				url : this.href,
				onComplete:function(){
					refresh_dir()
				}
			}).send();
		}
	});
	
	
</script>