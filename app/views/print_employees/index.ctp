<div id="filtration"><?php echo $viewIndex->filtration($renderSetting,$this->viewVars, $logged_user);?></div>
<div class="lista_add" id='lista_add'><?php echo $viewIndex->top_action($renderSetting, $logged_user);?></div>
<h1><?php echo $renderSetting['page_caption'];?></h1>
<form action='#' method='post' id='items_form'>
	<div id='items'><?php echo $this->renderElement('../print_employees/items');?></div>
</form>
<?php //pr($items);?>
<script>
$('filtr_button').addEvent('click', function(e){
	new Event(e).stop();
	new Request.HTML({
		update: 'items',
		url: $('filtration_view_index').action
	}).get($('filtration_view_index'));
});

if ($('filtr_button_cancel')){
	$('filtr_button_cancel').addEvent('click', function(e){
		new Event(e).stop();
		$('filtration_view_index').getElements('.fltr_input, .fltr_select').each(function(item){item.value = ''})
		//preloader(true);
		new Request.HTML({
			update: 'items',
			url: $('filtration_view_index').getProperty('action'),
			onComplete: function(){
			//	preloader(false);
			}
		}).get($('filtration_view_index'));
	});
}

$('lista_add').getElements('.export_excel').addEvent('click', function(e){
    e.stop();
    var url = $('filtration_view_index').action + '?excel=true&' + $('filtration_view_index').toQueryString();
    window.location = url;
})



</script>