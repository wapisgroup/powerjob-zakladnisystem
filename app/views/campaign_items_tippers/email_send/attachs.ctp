<div id="attachs_box">
    <div class="win_fce top">
    <a href='#' class='button edit' id='company_attach_add_new' title='Přidání nové přílohy'>Přidat přílohu</a>
    </div>
    
    <!--  Pridani prilohy -->
    <script type="text/javascript">
    	function upload_file_complete(url){
    		alert('Soubor byl nahrán.');
    		$('CampaignTemplatesAttachmentFile').value = url;
    	}
    </script>

        <div id='attachs_edit_form'>
            <?php echo $htmlExt->input('CampaignTemplatesAttachment/name',array('tabindex'=>1,'label'=>'Název','class'=>'to_clear long','label_class'=>'long'));?> <br class="clear">
            <div class='sll'>
            			<?php 
            						$upload_setting = array(
            							'paths' => array(
            								'upload_script' => '/campaign_items_tippers/upload_attach/',
            								'path_to_file'	=> 'uploaded/campaign_items_tippers/attachment/'
            							),
            							'upload_type'	=> 'php',
            							'onComplete'	=> 'upload_file_complete',
            							'label'			=> 'Soubor'
            						);
            					?>
            					<?php echo $fileInput->render('CampaignTemplatesAttachment/file',$upload_setting);?> <br class="clear">
            </div>
            <div class='slr'>
            	<?php echo $htmlExt->selectTag('CampaignTemplatesAttachment/setting_attachment_type_id',$setting_attachment_type_list,null,array('tabindex'=>4,'label'=>'Typ přílohy','class'=>'to_clear'));?> <br class="clear">
            </div>
            <br />    
        	<div class='formular_action'>
        		<input type='button' value='Přidat' id='AttachmentSave' />
        	</div>
        </div>
     <br />
    <!--  Pridani prilohy -->
 
    <table class='table' id='table_attachs'>
    	<tr>
    		<th>Název</th>
    		<th>Typ</th>
    		<th>Vytvořeno</th>
    		<th>Možnosti</th>
    	</tr>
    </table>
</div>
<script>
    // zobrazovani komentare
	var add_attach = new Fx.Slide('attachs_edit_form');
	add_attach.hide();
		
	$('company_attach_add_new').addEvent('click',function(e){
		e.stop();
		add_attach.toggle();
		return false;
	});
    

	$('table_attachs').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tuto přílohu?')){
			new Request.HTML({
				url:this.href,
				update: $('domwin_group_edit').getElement('.CB_ImgContainer'),
				onComplete: function(){}
			}).send();
		}
	});
	
	$('AttachmentSave').addEvent('click',function(e){
		new Event(e).stop();
        
        uid = uniqid();
  
        var tbody = $('table_attachs');
		var td_html = '<td>' + $('CampaignTemplatesAttachmentName').value + '</td>';
		   td_html += '<td>' + $('CampaignTemplatesAttachmentSettingAttachmentTypeId').options[$('CampaignTemplatesAttachmentSettingAttachmentTypeId').selectedIndex].title + '</td>';
		   td_html += '<td>dnes</td>';   	
		   td_html += '<td><a href="#" class="ta trash" title="Smazat">Smazat</a></td>';
           td_html += '<input name="data[prilohy]['+uid+'][asfile]" type="hidden" value="'+$('CampaignTemplatesAttachmentName').value+'" />';
           td_html += '<input name="data[prilohy]['+uid+'][filename]" type="hidden" value="'+$('CampaignTemplatesAttachmentFile').value+'" />';
            
		var tr = new Element('tr').inject(tbody).setHTML(td_html);
		tr.getElement('.trash').addEvent('click',delete_attach.bindWithEvent(this));  
        
        //vynuluj prvky
        $('attachs_edit_form').getElements('.to_clear').setValue('');
         
         var id = $('CampaignTemplatesAttachmentFileUploader');   
            // Nastaveni ikon
			// Zobrazeni na pozadi fileinput
			// Vymazani fake fileinputu
			id.getElements('.browse_icon, .seznam_icon').setStyle('display','inline');
			id.getElements('.upload_icon, .smazat_icon').setStyle('display','none');
			id.getElement('.input_file_over').value = '';
			id.getElement('.input_file').setStyle('display','block');
            
        //skyrti div pridani
        add_attach.toggle();
	});
    
    //funkce na odstraneni tr pouze
    function delete_attach(e){
        var event = new Event(e); 
		obj = new Event(e).target;
 		event.stop();
        if (confirm('Opravdu si přejet smazat tuto přílohu?'))
            obj.getParent('tr').dispose();
    }
    
    
</script>