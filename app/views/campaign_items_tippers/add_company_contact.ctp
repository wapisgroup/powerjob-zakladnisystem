<form action='/campaign_items_tippers/add_company_contact/' method='post' id='company_contact_edit_formular'>
	<?php echo $htmlExt->hidden('CompanyContact/company_id');?>
	<fieldset>
		<legend>Vyplňte následující údaje</legend>
		<div class='sll'>
			<?php echo $htmlExt->input('CompanyContact/jmeno',array('tabindex'=>1,'label'=>'Jméno'));?> <br class="clear">
			<?php echo $htmlExt->input('CompanyContact/telefon1',array('tabindex'=>3,'label'=>'Telefon'));?> <br class="clear">
			<?php echo $htmlExt->input('CompanyContact/email',array('tabindex'=>5,'label'=>'Email'));?> <br class="clear">
					
		</div>
		<div class='slr'>
			<?php echo $htmlExt->input('CompanyContact/prijmeni',array('tabindex'=>2,'label'=>'Příjmení'));?> <br class="clear">
			<?php echo $htmlExt->input('CompanyContact/telefon2',array('tabindex'=>4,'label'=>'Telefon'));?> <br class="clear">
			<?php echo $htmlExt->input('CompanyContact/position',array('tabindex'=>6,'label'=>'Pozice'));?> <br class="clear">		
		</div>
		<br/>
		<?php echo $htmlExt->textarea('CompanyContact/comment',array('tabindex'=>7,'label'=>'Komentář','class'=>'long','label_class'=>'long'));?> <br class="clear">
	</fieldset>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditCompanyContactSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyContactClose'/>
	</div>
</form>
<script>
	$('AddEditCompanyContactClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_add_company_contact');});
	
	$('AddEditCompanyContactSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('company_contact_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('company_contact_edit_formular').action,		
				onComplete:function(json){
					if (json){
						if (json.result === true){
							$('CampaignItemCompanyContactId').empty();
							new Element('option',{html:'', value:'', title:''}).inject($('CampaignItemCompanyContactId'));
							$each(json.data, function(caption, id){
								new Element('option',{html:caption, value:id, title:caption}).inject($('CampaignItemCompanyContactId'));
							})
							$('CampaignItemCompanyContactId').setValue(json.select_id);
							$('CampaignItemCompanyContactId').removeClass('invalid').removeClass('require').addClass('valid');
							$('CampaignItemTelefon').value 	= json.contact.CompanyContact.telefon1;
							$('CampaignItemMail').value 	= json.contact.CompanyContact.email;
							$('CampaignItemMobil').value 	= json.contact.CompanyContact.telefon2;
							domwin.closeWindow('domwin_add_company_contact');
						} else {
							alert(json.message);
						}
					} else {
						alert('Chyba aplikace');
					}
				}
			}).post($('company_contact_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('company_contact_edit_formular',{
		'CompanyContactJmeno': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit jméno'}
		},
		'CompanyContactPrijmeni': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit příjmení'}
		}
	});
	validation.generate('company_contact_edit_formular',false);
	
</script>