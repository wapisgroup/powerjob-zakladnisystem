 <form action='/campaign_items_tippers/campaign_stav/' method='post' id='kampan_akt_edit_formular'>
	<?php echo $htmlExt->hidden('CampaignItem/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
			<li class="ousko"><a href="#krok1">Historie</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Zadejte údaje</legend>
				<?php echo $htmlExt->selectTag('CampaignItem/stav',array(''=>'Zvolte typ aktivity')+$campaign_item_stavs2,null,array('tabindex'=>1,'label'=>'Stav','class'=>'long','label_class'=>'long'),null,false);?> <br class="clear">
				<?php echo $htmlExt->textarea('CampaignItemHistory/text',array('tabindex'=>2,'label'=>'Popis','class'=>'long','label_class'=>'long'));?> <br class="clear">				
			</fieldset>
			<div class="win_save">
				<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','class'=>'button'));?>
				<?php echo $htmlExt->button('Zavřít',array('id'=>'close','class'=>'button'));?>
			</div>
		</div>
		<div class="domtabs field">
			<table class='table'>
			<?php if (isset($history_list) && count($history_list)>0):?>
				<tr>
					<th>Stav</th>
					<th>Uživatel</th>
					<th>Popis</th>
				</tr>
				<?php foreach($history_list as $history):?>
					<tr>
						<td><?php echo $campaign_item_stavs[$history['CampaignItemHistory']['stav']];?></td>
						<td><?php echo $history['CmsUser']['name']?></td>
						<td title='<?php echo $history['CampaignItemHistory']['text'];?>'><?php echo $fastest->orez($history['CampaignItemHistory']['text'],100);?></td>
					</tr>
				<?php endforeach;?>
			<?php else:?>
				<tr><td colspan='3'>Nebyla nalezena žádná historie stavů</td></tr>
			<?php endif;?>
			</table>
		</div>
	</div>
	
</form>
<script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'});
		
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		new Request.JSON({
			url:$('kampan_akt_edit_formular').action,		
			onComplete:function(json){
				if (json){
					if (json.result === true){
						click_refresh($('CampaignItemId').value);
						domwin.closeWindow('domwin_campaign_stav');
					} else {
						alert(json.message);
					}
				} else {
					alert('Chyba aplikace');
				}
			}
		}).post($('kampan_akt_edit_formular'));
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_campaign_stav');});
	
</script>