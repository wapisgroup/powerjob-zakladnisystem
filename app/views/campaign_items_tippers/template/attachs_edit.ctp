<script type="text/javascript">
	function upload_file_complete(url){
		alert('Soubor byl nahrán.');
		$('CampaignTemplatesAttachmentFile').value = url;
	}
</script>
<form action='/campaign_items_tippers/attachs_edit/' method='post' id='client_attachs_edit_formular'>
	<div class="domtabs admin_dom2_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Příloha</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom2">
		<div class="domtabs field">
			<?php echo $htmlExt->hidden('CampaignTemplatesAttachment/id');?>
			<?php echo $htmlExt->hidden('CampaignTemplatesAttachment/campaign_template_id');?>
			<fieldset>
				<legend>Prosím vyplňte následující údaje</legend>
				<?php echo $htmlExt->input('CampaignTemplatesAttachment/name',array('tabindex'=>1,'label'=>'Název','class'=>'long','label_class'=>'long'));?> <br class="clear">
				<div class='sll'>
					<?php 
						$upload_setting = array(
							'paths' => array(
								'upload_script' => '/campaign_items_tippers/upload_attach/',
								'path_to_file'	=> 'uploaded/campaign_items_tippers/attachment/'
							),
							'upload_type'	=> 'php',
							'onComplete'	=> 'upload_file_complete',
							'label'			=> 'Soubor'
						);
					?>
					<?php echo $fileInput->render('CampaignTemplatesAttachment/file',$upload_setting);?> <br class="clear">
				</div>
				<div class='slr'>
					<?php echo $htmlExt->selectTag('CampaignTemplatesAttachment/setting_attachment_type_id',$setting_attachment_type_list,null,array('tabindex'=>4,'label'=>'Typ přílohy'));?> <br class="clear">
				</div>
			</fieldset>
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditCampaignTemplatesAttachmentSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCampaignTemplatesAttachmentClose'/>
	</div>
</form>
<script>
	var domtab = new DomTabs({'className':'admin_dom2'}); 
	
	$('AddEditCampaignTemplatesAttachmentClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_attach_add');});
	
	$('AddEditCampaignTemplatesAttachmentSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('client_attachs_edit_formular');
		if (valid_result == true){
			new Request.HTML({
				url:$('client_attachs_edit_formular').action,		
				update: $('refresh_edit_attach'),
				onComplete:function(){
					domwin.closeWindow('domwin_attach_add');
				}
			}).post($('client_attachs_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('client_attachs_edit_formular',{
		'CampaignTemplatesAttachmentName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
		}
	});
	validation.generate('client_attachs_edit_formular',<?php echo (isset($this->data['CampaignTemplatesAttachment']['id']))?'true':'false';?>);