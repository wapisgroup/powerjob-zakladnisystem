<form id='add_edit_template_formular' action='/campaign_items_tippers/template_edit/' method='post'>
	<?php echo $htmlExt->hidden('CampaignTemplate/id');?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
			<li class="ousko"><a href="#krok2">Přílohy</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Základní</legend>
				<?php echo $htmlExt->input('CampaignTemplate/name',array('label_class'=>'long','class'=>'long','label'=>'Předmět emailu'));?><br />
				<label class='long'>Text:</label>
				<?php echo $wysiwyg->render('CampaignTemplate/text');?><br />
			</fieldset>
		</div>
        <div class="domtabs field">
            <div id="refresh_edit_attach">
            <?php echo $this->renderElement('../campaign_items_tippers/template/attachs');?>
            </div>
        </div>
	</div>	
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditTemplateSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditTemplateClose'/>
	</div>
</form>
<script>
$$('.wysiwyg').makeWswg();
var domtab = new DomTabs({'className':'admin_dom'});

$('AddEditTemplateClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_group_edit');});
$('AddEditTemplateSaveAndClose').addEvent('click', function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('add_edit_template_formular');
		if (valid_result == true){
			$$('.wysiwyg').killWswg();
			new Request.JSON({
				url:$('add_edit_template_formular').action,		
				onComplete:function(json){
					if (json){
						if (json.result === true){
							domwin.loadContent('domwin_groups');
							domwin.closeWindow('domwin_group_edit');
						} else {
							alert(json.message);
						}
					} else {
						alert('Chyba aplikace');
					}
					
				}
			}).post($('add_edit_template_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	
	validation.define('add_edit_template_formular',{
		'CampaignTemplateName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit Předmět'}
		}		
	});
	validation.generate('add_edit_template_formular',<?php echo (isset($this->data['CampaignTemplate']['id']))?'true':'false';?>);

</script>