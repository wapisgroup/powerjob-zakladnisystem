<form id='add_edit_company_formular' action='/campaign_items_tippers/add_company/' method='post'>
	<?php echo $htmlExt->hidden('Company/id');?>
	<div class="domtabs admin_dom2_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom2">
		<div class="domtabs field">
			<?php echo $htmlExt->input('Company/name',array('label'=>'Název firmy', 'class'=>'long','label_class'=>'long','tabindex'=>1));?><br />
			<div class='sll'>	
				<?php echo $htmlExt->selectTag('Company/stat_id',$company_stat_list,null,array('label'=>'Stát','tabindex'=>2),null,false);?><br />
				<?php echo $htmlExt->input('Company/ulice',	array('label'=>'Ulice','tabindex'=>4));?><br />
				<?php echo $htmlExt->input('Company/psc',	array('label'=>'PSČ','tabindex'=>6));?><br />
				<?php echo $htmlExt->input('Company/ico',	array('label'=>'IČO','tabindex'=>8));?><br />	
			</div>
			<div class='slr'>
				<?php echo $htmlExt->selectTag('Company/province_id',$province_list,null,array('label'=>'Kraj','tabindex'=>3));?><br />
				<?php echo $htmlExt->input('Company/mesto',	array('label'=>'Město','tabindex'=>5));?><br />
				<?php echo $htmlExt->input('Company/www',	array('label'=>'WWW','tabindex'=>7));?><br />
				<?php echo $htmlExt->input('Company/dic',	array('label'=>'DIČ','tabindex'=>9));?><br />	
			</div>
			<br />
			<?php echo $htmlExt->textarea('Company/address2',	array('class'=>'long','label'=>'Korespondeční adresa:','label_class'=>'long','tabindex'=>10));?><br />
			<?php echo $htmlExt->textarea('Company/note',	array('class'=>'long','label'=>'Popis','label_class'=>'long','tabindex'=>11));?><br />
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditCompanySaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyClose'/>
	</div>
</form>
<script>
	
	var domtab = new DomTabs({'className':'admin_dom2'}); 
	$('AddEditCompanyClose').addEvent('click',function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin_add_company');
	});
	
	$('CompanyStatId').ajaxLoad('/companies/load_province/', ['CompanyProvinceId']);
	
	$('AddEditCompanySaveAndClose').addEvent('click', function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('add_edit_company_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('add_edit_company_formular').action,		
				onComplete:function(json){
					if (json){
						if (json.result === true){
							$('CampaignItemCompanyId').empty();
							new Element('option',{html:'', value:'', title:''}).inject($('CampaignItemCompanyId'));
							$each(json.data, function(caption, id){
								new Element('option',{html:caption, value:id, title:caption}).inject($('CampaignItemCompanyId'));
							});
							$('CampaignItemCompanyId').setValue(json.company_id);
							$('CampaignItemCompanyId').removeClass('invalid').removeClass('require').addClass('valid');
						} else {
							alert(json.message);
						}
					} else {
						alert('Chyba aplikace');
					}

				
					domwin.closeWindow('domwin_add_company');

				}
			}).post($('add_edit_company_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('add_edit_company_formular',{
		'CompanyName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit nazev firmy'}
		},
		'CompanyIco': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit ičo'},
			'isUnique':{'condition':{'model':'Company','field':'ico'},'err_message':'Toto ičo je již použito'}
		}
	});
	validation.generate('add_edit_company_formular',false);
	
</script>
