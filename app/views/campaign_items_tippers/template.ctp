<br/>
<a href='/campaign_items_tippers/template_edit/' class='button add_new_template' title='Přidat novou šablonu'>Přidat novou šablonu</a>
<br /><br />
<table class='table' id='groups_table'>
	<tr>
		<th>ID</th>
		<th>Název</th>
		<th>Stav</th>
		<th>Možnosti</th>
	</tr>
<?php if (isset($template_list) && count($template_list)>0): ?>
	<?php foreach($template_list as $template):?>
	<tr>
		<td><?php echo $template['CampaignTemplate']['id'];?></td>
		<td><?php echo $template['CampaignTemplate']['name'];?></td>
		<td><?php echo $kampane_stav[$template['CampaignTemplate']['stav']];?></td>
		<td>
			<?php if ($template['CampaignTemplate']['stav'] == 0):?>
			<a href='/campaign_items_tippers/template_edit/<?php echo $template['CampaignTemplate']['id'];?>' class='ta edit' title='Editace šablony: <?php echo $template['CampaignTemplate']['name'];?>'>Edit</a>
			<?php endif;?>
			<!-- a href='/campaign_items/groups/delete/<?php echo $template['CampaignTemplate']['id'];?>' class='ta trash' title='Smazání kmapaně: <?php echo $template['CampaignTemplate']['name'];?>'>Trash</a-->
		</td>
	</tr>
	<?php endforeach;?>
<?php else: ?>
	<tr>
		<td colspan='3'>Nenalezeno</td>
	</tr>
<?php endif; ?>
</table>
<div class="win_save">
	<?php echo $htmlExt->button('Zavřít',array('id'=>'domwin_template_close','class'=>'button','tabindex'=>2));?>
</div>
<script>
$('domwin_template_close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_groups');});
$('domwin_groups').getElements('.add_new_template,.edit').addEvent('click', function(e){
	new Event(e).stop();
	domwin.newWindow({
		id			: 'domwin_group_edit',
		sizes		: [900,580],
		scrollbars	: false,
		languages	: false,
		title		: this.title,
		ajax_url	: this.href,
		closeConfirm: true,
		max_minBtn	: false,
		modal_close	: false,
		remove_scroll: false
	}); 
});

$('groups_table').getElements('.trash').addEvent('click', function(e){
	new Event(e).stop();
	if (confirm('Opravdu si přejete odstranit tuto položku?')){
		new Request.HTML({
			url: this.href,
			update: $('domwin_groups').getElement('.CB_ImgContainer')
		}).send();
	}
});

//$$('.wysiwyg')
</script>
