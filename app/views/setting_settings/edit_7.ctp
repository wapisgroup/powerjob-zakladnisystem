<form action='/setting_settings/edit/<?php echo $this->data["Setting"]["id"];?>' method='post' id='setting_edit_formular'>
	<?php echo $htmlExt->hidden('Setting/id');?>
	<?php echo $htmlExt->hidden('Setting/no_serialize',array('value'=>true));?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Reporty aktivity koo</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Cena srážky</legend>
				<?php echo $htmlExt->input('Setting/value',array('tabindex'=>1,'label'=>'Srážka CZ (CZK)','class'=>'integer','label_class'=>'long'));?> <br class="clear">
			</fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	$('setting_edit_formular').getElements('.integer').inputLimit();
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();

			new Request.JSON({
				url:$('setting_edit_formular').action,		
				onComplete:function(){
					click_refresh($('SettingId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('setting_edit_formular'));

	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
</script>