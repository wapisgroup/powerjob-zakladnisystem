<form action='/setting_settings/edit/<?php echo $this->data["Setting"]["id"];?>' method='post' id='setting_edit_formular'>
	<?php echo $htmlExt->hidden('Setting/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Kalkulace Firem - SK</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom" id="company_work_money_edit_formular">
		<div class="domtabs field">
			<?php echo $htmlExt->input('Setting/value/mhhd',array('tabindex'=>6,'label'=>'Minimlní hrubá mzda','class'=>'integer long','label_class'=>'long'));?> <br class="clear">
			<?php echo $htmlExt->input('Setting/value/dohoda_limit',array('tabindex'=>6,'label'=>'Dohoda - Limit','class'=>'integer long','label_class'=>'long'));?> <br class="clear">
			 
            <fieldset>
				<legend>Náklady mimo mzdu</legend>
				<?php echo $htmlExt->input('Setting/value/cena_ubytovani_na_mesic',array('tabindex'=>6,'label'=>'Cena ubytování za měsíc','class'=>'integer long','label_class'=>'long'));?> <br class="clear">
			</fieldset>
			<fieldset>
				<legend>Počet odpracovaných hodin</legend>
				<?php echo $htmlExt->input('Setting/value/pocet_dni_v_mesici',array('tabindex'=>6,'label'=>'Počet pracovních dnů v měsíci','class'=>'integer long','label_class'=>'long'));?> <br class="clear">	
				<?php echo $htmlExt->input('Setting/value/normo_hodina',array('tabindex'=>6,'label'=>'Normo hodina','class'=>'integer long','label_class'=>'long'));?> <br class="clear">	
				<?php echo $htmlExt->input('Setting/value/pocet_hodin_prescasu_mesicne',array('tabindex'=>6,'label'=>'Počet hodin přesčasů měsíčně','class'=>'integer long','label_class'=>'long'));?> <br class="clear">	
			</fieldset>
			<fieldset>
				<legend>Formy mzdy</legend>
				<?php echo $htmlExt->input('Setting/value/pocet_dni_dovolene_svatku_zapocitanych_do_nakladu',array('tabindex'=>6,'label'=>'Počet dní dovolené a svátků ročne započítaných do nákladů','class'=>'integer long','label_class'=>'long'));?> <br class="clear">
				<?php echo $htmlExt->input('Setting/value/cista_mzda_1001',array('tabindex'=>6,'label'=>'Čistá mzda v případě kombinace 1001','class'=>'integer long','label_class'=>'long'));?> <br class="clear">
				<?php echo $htmlExt->input('Setting/value/vlozeny_prispevek_na_stravne',array('tabindex'=>6,'label'=>'Hodnota příspěvku na stravné na den','class'=>'integer long','label_class'=>'long'));?> <br />
				<?php echo $htmlExt->input('Setting/value/vlozeny_prispevek_na_cestovne',array('tabindex'=>6,'label'=>'Hodnota příspěvku na dopravné na měsíc','class'=>'integer long','label_class'=>'long'));?> <br />
			</fieldset>
			<fieldset>
				<legend>Nastaveni</legend>
				<?php echo $htmlExt->input('Setting/value/procentuelni_odvod_zamestnance',array('tabindex'=>6,'label'=>'Procentuální odvod zaměstnance','class'=>'integer long','label_class'=>'long'));?> <br class="clear">
				<?php echo $htmlExt->input('Setting/value/procentuelni_odvod_zamestnavatele',array('tabindex'=>6,'label'=>'Procentuální odvod zaměstnavatele','class'=>'integer long','label_class'=>'long'));?> <br class="clear">
				<?php echo $htmlExt->input('Setting/value/procenta_dane_z_prijmu',array('tabindex'=>6,'label'=>'Procenta daně z příjmu','class'=>'integer long','label_class'=>'long'));?> <br class="clear">
				<?php echo $htmlExt->input('Setting/value/procenta_dane_z_prijmu_dohoda',array('tabindex'=>6,'label'=>'Procenta daně z příjmu - dohoda','class'=>'integer long','label_class'=>'long'));?> <br class="clear">
				<?php echo $htmlExt->input('Setting/value/odvody_dohoda',array('tabindex'=>6,'label'=>'Odvody dohoda','class'=>'integer long','label_class'=>'long'));?> <br class="clear">
				<?php echo $htmlExt->input('Setting/value/odpocitatelna_polozka',array('tabindex'=>6,'label'=>'Odpočitatelná položka','class'=>'integer long','label_class'=>'long'));?> <br class="clear">
				<?php echo $htmlExt->input('Setting/value/vlozena_procenta_nakladu_na_zivnostnika',array('tabindex'=>6,'label'=>'Vlož procenta nákladů na živnostníka','class'=>'integer long','label_class'=>'long'));?> <br class="clear">
				<?php echo $htmlExt->input('Setting/value/vlozena_procenta_nakladu_na_cash',array('tabindex'=>6,'label'=>'Vlož procenta nákladů  na abc','class'=>'integer long','label_class'=>'long'));?> <br class="clear">
				<?php echo $htmlExt->input('Setting/value/pocet_deti',array('tabindex'=>6,'label'=>'Počet dětí','class'=>'integer long','label_class'=>'long'));?> <br class="clear">
				<?php echo $htmlExt->input('Setting/value/danovy_bonus_za_kazde_dite',array('tabindex'=>6,'label'=>'Daň. bonus za každé dítě','class'=>'integer long','label_class'=>'long'));?> <br class="clear">
			</fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	$('setting_edit_formular').getElements('.integer').inputLimit();
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();

			new Request.JSON({
				url:$('setting_edit_formular').action,		
				onComplete:function(){
					click_refresh($('SettingId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('setting_edit_formular'));

	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
</script>