<div class="win_fce top">
<a href='/faqs/attachs_edit/<?php echo $faq_id;?>' class='button edit' id='company_attach_add_new' title='Přidání nové přílohy'>Přidat přílohu</a>
</div>
<table class='table' id='table_attachs'>
	<tr>
		<th>#ID</th>
		<th>Název</th>
		<th>Typ</th>
		<th>Vytvořeno</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($attachment_list) && count($attachment_list) >0):?>
	<?php foreach($attachment_list as $attachment_item):?>
	<tr>
		<td><?php echo $attachment_item['FaqAttachment']['id'];?></td>
		<td><?php echo $attachment_item['FaqAttachment']['name'];?></td>
		<td><?php echo $attachment_item['SettingAttachmentType']['name'];?></td>
		<td><?php echo $fastest->czechDateTime($attachment_item['FaqAttachment']['created']);?></td>
		<td>
			<a title='Editace přílohy "<?php echo $attachment_item['FaqAttachment']['name'];?>"' 	class='ta edit' href='/faqs/attachs_edit/<?php echo $faq_id;?>/<?php echo $attachment_item['FaqAttachment']['id'];?>'/>edit</a>
			<a title='Odstranit přílohu'class='ta trash' href='/faqs/attachs_trash/<?php echo $faq_id;?>/<?php echo $attachment_item['FaqAttachment']['id'];?>'/>trash</a>
			<a title='Stáhnout přílohu'class='ta download' href='/faqs/attachs_download/faqs|attachment|<?php echo $attachment_item['FaqAttachment']['file'];?>/<?php echo $attachment_item['FaqAttachment']['alias_'];?>/'/>download</a>
		</td>
	</tr>
	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='5'>K této otázce nebyla nadefinována příloha</td>
	</tr>
	<?php endif;?>
</table>
<?php // pr($attachment_list);?>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='ListFaqAttachmentClose' class='button'/>
	</div>
<script>
	$('ListFaqAttachmentClose').addEvent('click', function(e){new Event(e).stop(); domwin.closeWindow('domwin_attach');});

	$('table_attachs').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tuto přílohu?')){
			new Request.HTML({
				url:this.href,
				update: $('domwin_attach').getElement('.CB_ImgContainer'),
				onComplete: function(){}
			}).send();
		}
	});
	
	$('domwin_attach').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_attach_add',
			sizes		: [580,390],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>