<form id='add_edit_company_formular' action='/faqs/edit/' method='post'>
	<?php echo $htmlExt->hidden('Faq/id');?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<div class='sll'>	
				<?php echo $htmlExt->selectTag('Faq/faq_group_id',$faq_group_list,null,array('label'=>'Kategorie'),null,false);?><br />
			</div>
            <br />
				
			<?php echo $htmlExt->textarea('Faq/question',	array('class'=>'long','label'=>'Otázka:','label_class'=>'long'));?><br />
			<?php echo $htmlExt->textarea('Faq/answer',	array('class'=>'long','label'=>'Odpověď','label_class'=>'long'));?><br />
			<?php echo $htmlExt->input('Faq/contact',array('class'=>'long','label'=>'Kontaktní osoba','label_class'=>'long'));?><br />
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditFaqSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditFaqClose'/>
	</div>
</form>
<script>
	
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('AddEditFaqClose').addEvent('click',function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin');
	});
	
	$('AddEditFaqSaveAndClose').addEvent('click', function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('add_edit_company_formular');
		if (valid_result == true){
			button_preloader($('AddEditFaqSaveAndClose'));
			new Request.JSON({
				url:$('add_edit_company_formular').action,		
				onComplete:function(){
					
					click_refresh($('FaqId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('add_edit_company_formular'));
            button_preloader_disable($('AddEditFaqSaveAndClose'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('add_edit_company_formular',{
		'FaqQuestion': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit otázku'}
		},
        'FaqAnswer': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit odpověď'}
		}
	});
	validation.generate('add_edit_company_formular',<?php echo (isset($this->data['Faq']['id']))?'true':'false';?>);
	
</script>
