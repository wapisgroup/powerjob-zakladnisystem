<form action='/accountant_client_employees/import_csv/' method='post' id='setting_import_edit_formular' enctype="multipart/form-data">
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
    <script>
        function import_complete(){
             $('import_file').setHTML('Hotovo');
             $$('.counter_file').setHTML($('ImportFile').value);
             $('import').getChildren('div').removeClass('none');
             $('setting_import_edit_formular').reset();
             run_script();
        }
    </script>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Soubor</legend>
					<?php 
						$upload_setting = array(
							'paths' => array(
								'upload_script' => '/accountant_client_employees/import/',
								'path_to_file'	=> 'uploaded/imports/',
								'status_path' 	=> '/get_status.php',
							),
							'upload_type'	=> 'php',
							'label'			=> 'Soubor',
                            'filename_type' => 'alias',
                            'file_ext'      => array('csv'),
                            'onComplete'    => 'import_complete'
						);
					?>
					<?php echo $fileInput->render('Import/file',$upload_setting);?> <br class="clear">
			</fieldset>
            <fieldset>
                <legend>Průběh importu</legend>
                <div id="import">
                    &nbsp;&nbsp;<strong>Zpracování importu:</strong> <span id="import_file">nahrajte soubor...</span><br />
                    
                    <div class="none">
                        <br />
                        &nbsp;&nbsp;Probíhá párování záznamu s DB systému. Vyčkejte na dokončení. <br />
                        <br />
                        &nbsp;&nbsp;<strong>Celkovy počet záznamu:</strong> <span class="counter_file">0</span><br />
                        &nbsp;&nbsp;<strong>Průběh importu <span id="num">0</span> z <span class="counter_file">0</span></strong><br />
                    </div>
                </div>
            </fieldset>
		</div>
	</div>
    <div style="text-align: center;">    
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'});
   	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
    
    function run_script(){
        new Request.JSON({
            url: '/accountant_client_employees/load_row/',
            onComplete: function(json){
                if (json){
                    if (json.result === true && !json.done){
                        $('num').setHTML($('num').getHTML().toInt() + 1); 
                        run_script();
                    }
                    if(json.result === true && json.done === true){
                        //alert("Import dokončen.\n\n Úspěšně spárovaných klientů:"+json.data.success+"\n Neúspěšně spárovaných klientů:"+json.data.unsuccess+"\n");
                        alert("Import dokončen.");
                    }  
                } else {
                    alert('chyba aplikace');
                }
                
            }
        }).send();
    }
</script>