<?php
$sum = 0;
$currency = 0;
$centers = $companies = array();

function mydate($type,$date_db,$date_selected, $format = false){
    list($year,$month,$date) = explode('-',$date_db);
    $date = $year.'-'.$month;
    $fastest = new FastestHelper();
    /**
     * Jedná se o stejný datum tedy pouzžijeme záznam z db
     */
    if($date == $date_selected){
        $datum = $date_db;
    }
    else{//JINE datum pouzijem tedy od prvniho/posledniho v mesici podle typu
        //$_d = new DateTime$date_selected);
        list($year,$month) = explode('-',$date_selected);
        $pocet_dnu_v_mesici = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $datum = $date_selected.($type == 0 ?'-01':'-'.$pocet_dnu_v_mesici);
    }    
    
    if ($format === true)
	return $fastest->czechDate($datum);
    else
	return $datum;
}

function date_dif($start, $end){
    return round(((strtotime($end) - strtotime($start))/60/60/24) + 1,0);
}

function get_stredisko($data){
    if($data['ConnectionAuditEstate']['centre'] != '')
        return $data['ConnectionAuditEstate']['centre'];
    else if($data['ConnectionAuditEstate']['company_foreign_id'] > 0)
        return $data['CompanyForeign']['ulice'].', '.$data['CompanyForeign']['mesto'];
    else if($data['ConnectionAuditEstate']['client_foreign_id'] > 0)
        return $data['ClientForeign']['ulice'].', '.$data['ClientForeign']['mesto'];
    else
        return null;        
}

?>
<div id="obal_tabulky">
<table class='table' id='rating_table'>
			<tr>
			    <th>Pracovník</th>
			    <th>Odběratel</th>
			    <th>Středisko</th>
                <th>Pozice</th>
                <th>Cena</th>
			    <th>Pronájem od</th>
			</tr>
			<?php 
			    if (isset($orders) && count($orders) > 0){

				foreach($orders as $item):
                    ?>
        			<tr>
        			    <td><?php echo $item['Client']['name']; ?></td>
        			    <td><?php echo $item['OdberatelCmsUser']['name']; ?></td>
        			    <td><?php echo (isset($center_list[$item['Odberatel']['at_project_centre_id']]) ? $center_list[$item['Odberatel']['at_project_centre_id']] : '');?></td>
        			    <td><?php echo $item['SettingCareerItem']['name'];?></td>
        			    <td><?php  $price = $item['SettingCareerItem']['price_2'] ; echo $price;?></td>
        			    <td><?php echo $fastest->czechDate($item['ClientBuy']['from_date']);?></td>
        			</tr>
            <?php
    			    $sum += $price;

    			    //pridani do listu dle stredisek
    			   $centers[$center_list[$item['Odberatel']['at_project_centre_id']]] = (isset($center_list[$item['Odberatel']['at_project_centre_id']])? $centers[$center_list[$item['Odberatel']['at_project_centre_id']]] : 0) + $price;

        	?>
			<?php endforeach; ?>
			<tr>
			    <td colspan=7>&nbsp;</td>
			</tr>
			<tr id='Sum'>
			    <td colspan=3></td>
			    <td><strong>Celkem</strong></td>
			    <td><?= $sum; ?></td>
			    <td colspan = 2>&nbsp;</td>
			</tr>
			<?php
			    } else { ?>
			<tr id='NoneSearch'>
			    <td colspan=7>Žádné osoby</td>
			</tr>
			<?php };?>
		    </table>
</div> 
<br />
<div class="sll">
<?php foreach($centers as $name=>$center){
echo 'Celkem za středisko: <strong>'.$name.'</strong> = '.$center.'<br />';
}?>
</div>

<br/>