<form action='' method='post' id='setting_activity_edit_formular'>
		
	<div class="domtabs admin2_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Seznam majetků</a></li>
		</ul>
</div>
	<div class="domtabs admin2_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Majetek</legend>
                <div id="obal_tabulky">
				<table class='table' id='rating_table'>
					<tr>
						<th>Majetek</th>
						<th>Pořizovací hodnota</th>
						<th>Měsíčně</th>
						<th>Pronájem od</th>
						<th>Možnosti</th>
					</tr>
					<?php 
					if (isset($estate_list) && count($estate_list) > 0){
						foreach($estate_list as $item):
						?>
						<tr>
							<td><?php echo $item['AuditEstate']['name'];?></td>
							<td><?php echo $fastest->price($item['AuditEstate']['first_cost'],$currency_list[$item['AuditEstate']['currency']],null,2);?></td>
							<td><?php echo $fastest->price($item['AuditEstate']['hire_price'],$currency_list[$item['AuditEstate']['currency']],null,2);?></td>
							<td><?php echo $fastest->czechDate($item['ConnectionAuditEstate']['created']);?></td>
							
							<td>
                            <?php if($item['ConnectionAuditEstate']['to_date'] != '0000-00-00'){
                                echo 'Odevzdáno dne '.$fastest->czechDate($item['ConnectionAuditEstate']['to_date']);
                            }
                            else{
                            ?>
							     <a href="/person_audit_estates/give_off/<?php echo $item['ConnectionAuditEstate']['id']; ?>" class="give_off" title="Odevzdat majetek">Odevzdat majetek</a>
							<?php }?>
							&nbsp;&nbsp;
                             <a href="/audit_estates/edit/<?php echo $item['ConnectionAuditEstate']['audit_estate_id']; ?>" class="ta edit" title="Detail majetek">Detail majetek</a>
						    </td>  
						</tr>
					<?php 
						endforeach;
					}
					else echo "<tr id='NoneSearch'><td></td>Žádný majetek</td></tr>"; 
					?>
					</table></div> 
				<br />
			</fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Zavřít',array('id'=>'closeshow','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin2_dom'}); 

	$$('.give_off').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_detail',
			sizes		: [400,190],
			scrollbars	: false,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
            remove_scroll : false
		}); 
	});
    
    $$('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin',
			sizes		: [800,800],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
            remove_scroll : false
		}); 
	});
	
  
	$('closeshow').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_show');});
</script>