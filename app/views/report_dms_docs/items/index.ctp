<div class="domtabs admin_dom_links">
	<ul class="zalozky">
		<li class="ousko"><a href="#krok1">Číselník - Typ dokumentů</a></li>
        <li class="ousko"><a href="#krok2">Číselník - Způsob doručení</a></li>
	</ul>
</div>
<div class="domtabs admin_dom">
	<div class="domtabs field">   
        <br/>
        <a href='/report_dms_docs/item_edit/ReportDmsType/' class='button add_new_group' title='Přidat nový typ'>Přidat nový typ</a>
        <br /><br />
        <table class='table' id='groups_table'>
        	<tr>
        		<th>ID</th>
        		<th>Název</th>
        		<th>Vytvořeno</th>
        		<th>Možnosti</th>
        	</tr>
            <?php if (isset($type_list) && count($type_list)>0): ?>
            	<?php foreach($type_list as $group):?>
            	<tr>
            		<td><?php echo $group['ReportDmsType']['id'];?></td>
            		<td><?php echo $group['ReportDmsType']['name'];?></td>
            		<td><?php echo $fastest->czechDateTime($group['ReportDmsType']['created']);?></td>
            		<td>
            			<a href='/report_dms_docs/item_edit/ReportDmsType/<?php echo $group['ReportDmsType']['id'];?>' class='ta edit' title='Editace: <?php echo $group['ReportDmsType']['name'];?>'>Edit</a>
            			<a href='/report_dms_docs/item_trash/ReportDmsType/<?php echo $group['ReportDmsType']['id'];?>' class='ta trash' title='Smazání: <?php echo $group['ReportDmsType']['name'];?>'>Trash</a>
             		</td>
            	</tr>
            	<?php endforeach;?>
            <?php else: ?>
        	<tr>
        		<td colspan='3'>Nenalezeno</td>
        	</tr>
        <?php endif; ?>
        </table>
    </div>  
    <div class="domtabs field">   
        <br/>
        <a href='/report_dms_docs/item_edit/ReportDmsTypeDelivery/' class='button add_new_group' title='Přidat nový způsob'>Přidat nový způsob</a>
        <br /><br />
        <table class='table' id='groups_table2'>
        	<tr>
        		<th>ID</th>
        		<th>Název</th>
        		<th>Vytvořeno</th>
        		<th>Možnosti</th>
        	</tr>
            <?php if (isset($type_delivery_list) && count($type_delivery_list)>0): ?>
            	<?php foreach($type_delivery_list as $group):?>
            	<tr>
            		<td><?php echo $group['ReportDmsTypeDelivery']['id'];?></td>
            		<td><?php echo $group['ReportDmsTypeDelivery']['name'];?></td>
            		<td><?php echo $fastest->czechDateTime($group['ReportDmsTypeDelivery']['created']);?></td>
            		<td>
            			<a href='/report_dms_docs/item_edit/ReportDmsTypeDelivery/<?php echo $group['ReportDmsTypeDelivery']['id'];?>' class='ta edit' title='Editace: <?php echo $group['ReportDmsTypeDelivery']['name'];?>'>Edit</a>
            			<a href='/report_dms_docs/item_trash/ReportDmsTypeDelivery/<?php echo $group['ReportDmsTypeDelivery']['id'];?>' class='ta trash' title='Smazání: <?php echo $group['ReportDmsTypeDelivery']['name'];?>'>Trash</a>
            		</td>
            	</tr>
            	<?php endforeach;?>
            <?php else: ?>
        	<tr>
        		<td colspan='3'>Nenalezeno</td>
        	</tr>
        <?php endif; ?>
        </table>
    </div>  
</div>       
<div class="win_save">
	<?php echo $htmlExt->button('Zavřít',array('id'=>'DomwinGroupsClose','class'=>'button','tabindex'=>2));?>
</div>
<script>
	var domtab = new DomTabs({'className':'admin_dom'}); 


$('DomwinGroupsClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_groups');});
$('domwin_groups').getElements('.add_new_group').addEvent('click', function(e){
	new Event(e).stop();
	domwin.newWindow({
		id			: 'domwin_item_edit',
		sizes		: [550,180],
		scrollbars	: false,
		languages	: false,
		title		: this.title,
		ajax_url	: this.href,
		closeConfirm: true,
		max_minBtn	: false,
		modal_close	: false,
		remove_scroll: false
	}); 
});

$('domwin_groups').getElements('.edit').addEvent('click', function(e){
	new Event(e).stop();
	domwin.newWindow({
		id			: 'domwin_item_edit',
		sizes		: [550,180],
		scrollbars	: false,
		languages	: false,
		title		: this.title,
		ajax_url	: this.href,
		closeConfirm: true,
		max_minBtn	: false,
		modal_close	: false,
		remove_scroll: false
	}); 
});

$('domwin_groups').getElements('.trash').addEvent('click', function(e){
	new Event(e).stop();
	if (confirm('Opravdu si přejete odstranit tuto položku?')){
		new Request.HTML({
			url: this.href,
			onComplete:(function(json){
			     this.getParent('tr').dispose();
			}).bind(this) 
		}).send();
	}
});
</script>
