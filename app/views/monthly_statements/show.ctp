<?php
    if (!isset($cinnost_list))
        $cinnost_list = array();

 $cinnost_list[-1] = 'Bez činnosti';?>
<form action='' method='post' id='setting_activity_edit_formular'>
    <div class="domtabs admin2_dom_links noprint">
	<ul class="zalozky">
	    <li class="ousko"><a href="#krok1">Seznam majetků</a></li>
	</ul>
    </div>
    <div class="domtabs admin2_dom">
	<div class="domtabs field">
        <div class="sll3">
    	    Doklad k faktuře:<br />
    	    Dodavatel: <?php echo $info['AtCompanyE']['name'];?><br />
    	    Středisko: <?php echo $info['AtProjectCentre']['name'];?><br />
    	    odběratel: <?php echo $info['AtCompanyU']['name'];?><br />
	    </div>
        <div class="sll3 noprint">
             <?php echo $htmlExt->selectTag('order_by',$order_list,null,array('label'=>'Seřadit podle','label_class'=>''));?><br />
             <?php echo $htmlExt->selectTag('spolecnost',$spol_list,null,array('label'=>'Filtrovat zakázku','label_class'=>''));?><br />
             <?php echo $htmlExt->selectTag('stredisko',$stredisko_list,null,array('label'=>'Filtrovat středisko','label_class'=>''));?><br />
   
        </div>
        <div class="sll3 noprint">
            <?php echo $htmlExt->selectTag('cinnost',isset($cinnost_list)?$cinnost_list:array(),null,array('label'=>'Filtrovat činnost','label_class'=>''));?><br />
        </div>
        <br />
		<div id="table_items">
		    <?php echo $this->renderElement('../monthly_statements/items');?>
        </div>    
	</div>
    </div>
    <div class="win_save noprint">
	<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	<?php echo $html->link('Tisk',$refresh_url.'/1',array('id'=>'print_button','class'=>'mr5 button','target'=>'_blank'));?>
    </div>
</form>

<script language="javascript" type="text/javascript">


    var pprint = "<?= $print;?>";

    if(pprint == 1) {
        window.print();
    } else {
        var domtab = new DomTabs({'className':'admin2_dom'}); 
	    $('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_show');});
        $('OrderBy').addEvent('change',function(e){
            new Event(e).stop();
            if(this.value != ''){ 
                new Request.HTML({
        			url:'<?= $refresh_url;?>/0/'+this.value,
                    update:'table_items',		
        			onComplete:(function(){
        			     $('print_button').setProperty('href','<?= $refresh_url;?>/1/'+this.value);
        			}).bind(this)
    	        }).send();
            }
            else
                domwin.loadContent('domwin_show');
        });
        $('Spolecnost').addEvent('change',function(e){
            new Event(e).stop();
            if(this.value != ''){ 
                new Request.HTML({
        			url:'<?= $refresh_url;?>/0/0/'+this.value,
                    update:'table_items',		
        			onComplete:(function(){
        			     $('print_button').setProperty('href','<?= $refresh_url;?>/1/0/'+this.value);
        			}).bind(this)
    	        }).send();
            }
            else
                domwin.loadContent('domwin_show');
        });
        $('Stredisko').addEvent('change',function(e){
            new Event(e).stop();
            if(this.value != ''){ 
                new Request.HTML({
        			url:'<?= $refresh_url;?>/0/0/0/'+this.value,
                    update:'table_items',		
        			onComplete:(function(){
        			     $('print_button').setProperty('href','<?= $refresh_url;?>/1/0/0/'+this.value);
        			}).bind(this)
    	        }).send();
            }
            else
                domwin.loadContent('domwin_show');
        });

        $('Cinnost').addEvent('change',function(e){
            new Event(e).stop();
            if(this.value != ''){
                new Request.HTML({
                    url:'<?= $refresh_url;?>/0/0/0/0/'+this.value,
                    update:'table_items',
                    onComplete:(function(){
                         $('print_button').setProperty('href','<?= $refresh_url;?>/1/0/0/0/'+this.value);
                    }).bind(this)
                }).send();
            }
            else
                domwin.loadContent('domwin_show');
        });
    }    
</script>