<form action='/internal_regulations/group_edit/' method='post' id='setting_stav_edit_formular'>
	<?php echo $htmlExt->hidden('InternalRegulationGroup/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Nastavení</legend>
				<div class="sll100">  
					<?php echo $htmlExt->input('InternalRegulationGroup/name',array('tabindex'=>1,'label'=>'Název'));?> <br class="clear">
				</div>
			</fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('setting_stav_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('setting_stav_edit_formular').action,		
				onComplete:function(){
					domwin.loadContent('domwin_groups');
                    domwin.closeWindow('domwin_edit');
				}
			}).post($('setting_stav_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_edit');});
	validation.define('setting_stav_edit_formular',{
	   'InternalRegulationGroupName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
		}
	});
	validation.generate('setting_stav_edit_formular',<?php echo (isset($this->data['InternalRegulationGroup']['id']))?'true':'false';?>);
</script>