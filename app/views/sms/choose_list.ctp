<ul id='choose_list'>
<?php if (isset($choose_list) && count ($choose_list)>0):?>
	<?php foreach($choose_list as $index => $item):?>
		<li rel='<?php echo $index;?>' class='choose_li'>
			<span class='col0'>
			<?php echo $item['Client']['name'];?>
			</span>
			<span class='col1'>
				<?php echo $item['Client']['mobil'];?>
			</span>
			<span class='col2 sms_stav wait_for_delivery none'>
				<span class='title'>
					<span class='ta sms_wait'></span>
				</span>
				<input type='hidden' value='<?php echo $item['Client']['mobil'];?>' class='phone_number' />
				<input type='hidden' value='<?php echo $item['Client']['id'];?>' 	class='client_id' />
				<input type='hidden' value='<?php echo $item['Client']['name'];?>' 	class='client_name' />
			</span>
		</li>
	<?php endforeach;?>
<?php endif;?>		
</ul>
<div>
<?php
    if(isset($count_find)){
        $stranek = ceil($count_find / 400);
        echo '<ul id="pager">';
            for($i=1;$i<=$stranek;$i++){
                echo '<li style="float:left; margin-left:10px;">
                        <a href="#" rel="'.$i.'" class="'.($page == $i?'active':'').'">Page '.$i.'</a>
                </li>';}
        echo '</ul>';
    }
?>
</div>

 	
<script type="text/javascript">
<!--
	<?php if (isset($count_limit_over) && $count_limit_over === true):?>
		var error_message = new MyAlert();
		error_message.show(['Je zobrazeno pouze prvních <?php echo $load_limit; ?> záznamů. Prosím použite podrobnější filtrování.'],{button_label:'Ok',caption:'Upozornění'});
	<?php endif;?>

	var sel_index = 0;
    
    if($('pager'))
    $('pager').getElements('a').addEvent('click',function(e){
        $('LoadClientPage').value = this.getProperty('rel');
        $('filtr_client_go').fireEvent('click',e);
    })

	$('choose_list').getElements('.choose_li')
		.addEvent('click',select_unselect.bindWithEvent(this))
		.setStyle('MozUserSelect',"none");

	

	$('choose_list').setStyle('MozUserSelect',"none");

	
//-->
</script>