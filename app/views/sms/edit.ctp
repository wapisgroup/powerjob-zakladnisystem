
	<div class="domtabs admin_dom_sms_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Text</a></li>
			<li class="ousko"><a href="#krok2">Odběratelé</a></li>
			<li class="ousko"><a href="#krok3">Logace</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom_sms">
		<div class="domtabs field">

		 	<?php echo $htmlExt->selectTag('Sms/sms_template_id',$template_list,null,array('label'=>'Šablony kampaně','class'=>'long','label_class'=>'long'));?><br/>
		 	<?php echo $htmlExt->input('Sms/name',array('readonly'=>(isset($this->data['Sms']['id']))?'readonly':false,'label'=>'Název kampaně','class'=>'long','label_class'=>'long'));?><br/>
		 	<?php echo $htmlExt->textarea('Sms/text',array('readonly'=>(isset($this->data['Sms']['id']))?'readonly':false,'label'=>'Text SMS','class'=>'long','label_class'=>'long','style'=>'height:300px'));?><br/>
		 	<label class='long'>Počet možných znaků:</label><var class='long' id='sms_len'>160</var><br />
		</div>
		<div class="domtabs field">
		<?php if (!isset($this->data['Sms']['id'])):?>
			<form action='/sms/load_client/' method='post' id='load_client_form' onsubmit="return false;">
                <?php echo $htmlExt->hidden('LoadClient/page',array('value'=>1));?><br/>
				<div style='border:1px dotted #EEE; margin-bottom:5px '>
					<div style='float:left; width:27%;'>
						<?php echo $htmlExt->selectTag('LoadClient/pohlavi',$pohlavi_list,null,array('label'=>'Pohlaví'))?><br/>
						<?php echo $htmlExt->selectTag('LoadClient/profese',$setting_career_item_list,null,array('label'=>'Profese'))?><br/>
						<label>Jen uchazeči</label> <?php echo $htmlExt->checkbox('LoadClient/uchazeci',null,array('checked'=>'checked','disabled'=>'disabled'));?><br/>
                        <label>Rok přidání:</label><?= $htmlExt->selectTag('LoadClient/created_year',$actual_years_list);?>
					</div>
					<div style='float:left; width:27%;'>
						<?php echo $htmlExt->selectTag('LoadClient/certifikat',$setting_certificate_list,null,array('label'=>'Certifikát'))?><br/>
						<?php echo $htmlExt->selectTag('LoadClient/vzdelani',$setting_education_list,null,array('label'=>'Vzdělání'))?><br/>
						<label>Interní nábor</label> <?php echo $htmlExt->checkbox('LoadClient/interni');?><br/>
                        <label>Měsíc přidání:</label><?= $htmlExt->selectTag('LoadClient/created_month',$mesice_list2);?>
					</div>
					<div style='float:left;width:29%;'>
						<?php //echo $htmlExt->input('LoadClient/vek',array('label'=>'Věk'))?><br/>
                        <?php echo $htmlExt->input('LoadClient/first_name',array('label'=>'Jméno'))?><br/>
                        <?php echo $htmlExt->input('LoadClient/last_name',array('label'=>'Přijmení'))?><br/>
                        <?php echo $htmlExt->input('LoadClient/vek_from',array('label'=>'Věk od'))?><br/>
                        <?php echo $htmlExt->input('LoadClient/vek_to',array('label'=>'Věk do'))?><br/>
						<?php echo $htmlExt->input('LoadClient/mesto',array('label'=>'Město'))?><br/>
						<?php echo $htmlExt->selectTag('LoadClient/okres',array('Vyberte kraj'),null,array('label'=>'Okres'))?><br/>
						<?php echo $htmlExt->selectTag('LoadClient/kraj',array('Vyberte stát'),null,array('label'=>'Kraj'))?><br/>
                        <?php echo $htmlExt->selectTag('LoadClient/stat',$setting_stat_list,null,array('label'=>'Stát'))?><br/>
					    

					   
                    </div>
					<div style='float:left;width:15%;'>
						<input type='button' value='Filtrovat'	onclick='return false;'  id='filtr_client_go' 		style='margin-left:30px; margin-top:3px; width:90px'/>
						<input type='button' value='Zrušit' 	onclick='return false;'  id='filtr_client_cancel'	style='margin-left:30px; margin-top:3px; width:90px'/>
					</div>
					<br/>
				</div>
			</form>
			<?php endif;?>
			<?php if (!isset($this->data['Sms']['id'])):?>
				<div style='border:1px dotted #EEE; width:426px' id='choose_parent_div'>
					<?php echo $this->renderElement('../sms/choose_list');?>
				</div>
			<?php endif;?>
			<?php if (!isset($this->data['Sms']['id'])):?>
			<div style='float:left; width:42px; text-align:center; padding-top:12%; padding-left:8px'>
				<a href='#' id='move_right_selected' class='ta right_select' 	title='Vybrat vybrané položky'></a> <br />
				<a href='#' id='move_right_all' 	 class='ta right_all'		title='Vybrat všechny položky'></a> <br /><br /><br />
				<a href='#' id='move_left_selected'  class='ta left_select'		title='Zrušit vybrané položky'></a><br /> 
				<a href='#' id='move_left_all' 		 class='ta left_all'		title='Zrušit všechny položky'></a><br />
			</div>
			<?php endif;?>
			<div id='send_parent_div' style='width:<?php echo (isset($this->data['Sms']['id']))?'876px':'390px';?>; float:right; border:1px dotted #EEE; height:300px;'>
				<ul id='send_list'>
					<?php if (isset($sended_messages)):?>
						<?php foreach($sended_messages as $msg):?>
						<li>
							<span class='col0'><?php echo $msg['SmsMessage']['client_name'];?></span>
							<span class='col1'><?php echo $msg['SmsMessage']['phone'];?></span>
							<span class='col2'><span class='title'><span class='ta <?php echo ($msg['SmsMessage']['status_sms'] == '200')?'sms_success':'sms_error';?>'></span></span></span>
						</li>
						<?php endforeach;?>
					<?php endif;?>
                    
                    <?php if (isset($client_to_list)):?>
						<?php foreach($client_to_list as $item):?>
                		<li rel='0' class='choose_li'>
                			<span class='col0'>
                			<?php echo $item['Client']['name'];?>
                			</span>
                			<span class='col1'>
                				<?php echo $item['Client']['mobil'];?>
                			</span>
                			<span class='col2 sms_stav wait_for_delivery'>
                				<span class='title'>
                					<span class='ta sms_wait'></span>
                				</span>
                				<input type='hidden' value='<?php echo $item['Client']['mobil'];?>' class='phone_number' />
                				<input type='hidden' value='<?php echo $item['Client']['id'];?>' 	class='client_id' />
                				<input type='hidden' value='<?php echo $item['Client']['name'];?>' 	class='client_name' />
                			</span>
                		</li>
						<?php endforeach;?>
					<?php endif;?>
				</ul>
		        <?php echo $htmlExt->input('WithoutSystem/number',array('class'=>'integer'))?>
						  
                <input type='button' value='Přidat číslo mimo system'	onclick='return false;'  id='add_number_without_system' style='margin-top:1px; width:150px'/>
                <br />
						 	
			</div>
			<br />
		</div>
		<div class="domtabs field">
		 	<?php echo $htmlExt->textarea('Sms/logs',array('label'=>'Logace','class'=>'long','label_class'=>'long','style'=>'height:300px','readonly'=>'readonly'));?><br/>
		</div>
	</div>
	<div class='formular_action' id='form_buttons'>
		<?php if (!isset($this->data['Sms']['id'])):?>
		<input type='button' value='Odeslat' id='send_sms_now' />
		<?php endif;?>
		<input type='button' value='Zavřít' id='close_sms_sender'/>
	</div>
	<div id='progress' class='none' style='width:200px; height:16px; padding:1px; position:absolute; top:45px; right:32px; border:1px solid #EFEFEF; text-align:center'>
		<div class='bar' style='background:green url(/css/fastest/action/progress_bar.png); width:0%; height:16px;'></div>
		<div class='label' style='position:absolute; top:1px; left:1px; width:200px; height:16px;'></div>
	</div>
<script type="text/javascript">
<!--
	var domtab = new DomTabs({'className':'admin_dom_sms'});

	$('sms_len').setHTML(160 - $('SmsText').value.length);
	
    $('domwin').getElements('.integer').inputLimit();

	$('SmsText').addEvents({
		'keyup': function(){
		
			$('sms_len').setHTML(160 - this.value.length);
			if (this.value.length >= 160)
				this.value = this.value.substring(0,160);
		},
        'keypress':function(e){
            var event = new Event(e);
            if(event.code == 38)
				return false;
        }
	});
    

    if($('LoadClientStat')){
           /* $('LoadClientStat').ajaxLoad('/clients/load_ajax_okresy/',['LoadClientOkres']);*/
    		$('LoadClientStat').ajaxLoad('/clients/load_ajax_kraje/',['LoadClientKraj']);
    }

    if($('LoadClientKraj')){
            $('LoadClientKraj').ajaxLoad('/clients/load_ajax_okresy_kraje/',['LoadClientOkres']);
    }

	function select_unselect(e){
		var event = new Event(e),obj = event.target, ul = obj.getParent('ul');
		event.stop();

		if (obj.get('tag') != 'li') 
			obj = obj.getParent('li');
		
		if (!event.shift){
			if (!event.control){
				ul.getElements('.choose_li').removeClass('selected');
			}	
			if (obj.hasClass('selected'))
				obj.removeClass('selected');
			else
				obj.addClass('selected');
		} else {
			if (sel_index < obj.getProperty('rel').toInt()){
				for (i=sel_index; i <= obj.getProperty('rel'); i++){	
					ul.getElements('.choose_li')[i].addClass('selected');
				}
			} else {
				for (i=sel_index; i >= obj.getProperty('rel'); i--){	
					ul.getElements('.choose_li')[i].removeClass('selected');
				}
			}
		}
		sel_index = obj.getProperty('rel').toInt();
	}
	
	// select item
	if ($('choose_list'))
		$('choose_list').getElements('.choose_li').addEvent('click',select_unselect.bindWithEvent(this)); 

	// move all left
	if($('move_left_all'))
		$('move_left_all').addEvent('click',function(e){
			new Event(e).stop();
			$('send_list').getElements('.choose_li').addClass('selected');
			move_left();
		});
	
	// move selected right
	if($('move_left_selected'))
		$('move_left_selected').addEvent('click',function(e){
			new Event(e).stop();
			move_left();
		});

	function move_left(){
		$('send_list').getElements('.selected').each(function(item){
			$('choose_list').adopt(item);
            item.getElement('.sms_stav').addClass('none');
		});
	}

	
	

	// nalezeni dle filtrace
	if($('filtr_client_go'))
		$('filtr_client_go').addEvent('click',function(e){
			new Event(e).stop();
			$('choose_list').fade(0);
			$('choose_parent_div').addClass('preloader');
            $('LoadClientUchazeci').removeProperty('disabled');
			new Request.HTML({
				url: '/sms/load_client/',
				update: 'choose_parent_div',
				onComplete: function(){
					$('choose_list').getElements('.choose_li').each(function(item){
						if($('send_list').getElement('.phone_number[value=' + item.getElement('.phone_number').value + ']')){
							item.dispose();
						}
					});
					reindex('choose_list');
                    $('LoadClientUchazeci').setProperty('disabled','disabled');
					$('choose_parent_div').removeClass('preloader');
					$('choose_list').fade(1);
				}
			}).send($('load_client_form'));
		})
	
	
	function send_sms_item(group_id){
		var item = $('send_list').getElement('.wait_for_delivery');
		if (item){
			var icon = item.getElement('.title').getElement('.ta');
			icon.removeClass('sms_wait');
			icon.addClass('sms_progress');
			var request = new Request.JSON({
				url: '/sms/send_item/',
				data: {
					'data[Sms][number]'		: 	item.getElement('.phone_number').value,
					'data[Sms][client_id]'	: 	item.getElement('.client_id').value,
					'data[Sms][client_name]':	item.getElement('.client_name').value,
					'data[Sms][group_id]'	:	group_id,
					'data[Sms][text]'		:	$('SmsText').value
				},
				onComplete: function(json){
					if (json){
						icon_c = item.getElement('.title').getElement('.ta');
						if (json.result === true){
							item.removeClass('wait_for_delivery');
							item.addClass('delivered');
							icon_c.setProperty('title','Odeslano');
							$('SmsLogs').value += "Odeslano na " + json.phone + "\n"
							
							icon_c.removeClass('sms_progress');
							icon_c.addClass('sms_success');
							
						} else {
							icon_c.setProperty('title','Chyba během odesilani: ' + json.message);
							$('SmsLogs').value += 'Chyba během odesilani: ' + json.message + ' Tel: ' + json.phone + "\n";
							icon_c.removeClass('sms_progress');
							icon_c.addClass('sms_error');
							item.removeClass('wait_for_delivery');
							item.addClass('undelivered');
						}
						progress_step ++;
						redraw_progress();
						send_sms_item(group_id);
					} else {
						alert('Chyba aplikace');
					}
				}
			}).send();
		} else {
			new Request.JSON({
				url: '/sms/save_log/',
				data: {
					'data[Sms][id]' 	: group_id,
					'data[Sms][logs]' 	: $('SmsLogs').value
				},
				onComplete: function(json){
					if (json){
						if (json.result === true){
							progress_step ++;
							redraw_progress();
							$('progress').getElement('.bar').setStyle('width','0%');
							$('progress').getElement('.label').setHTML('Odesílání dokončeno.');
							progress_count = 0;
							progress_step = 0;
							click_refresh(group_id);
							$('form_buttons').fade(1);
						} else {
							alert(json.message);
						}
					} else {
						alert('Chyba aplikace');
					}
				}
			}).send();
		}
	}

	var progress_count = 0;
	var progress_step = 0;
	// validace
	function valid_sms_sender(){
		error = [];
		if ($('SmsName').value == '') error.push('Musíte vyplnit název SMS kampaně');
		if ($('SmsText').value == '') error.push('Musíte vyplnit text SMSky');
		if ($('send_list').getElements('.wait_for_delivery').length == 0) error.push('Musíte zvolit příjemce SMS');

		if (error.length > 0){
			var error_message = new MyAlert();
			error_message.show(error);
			return false;
		} else {
			return true;
		}
	}
	
	// odeslani SMS
	if($('send_sms_now'))
		$('send_sms_now').addEvent('click',function(e){
			new Event(e).stop();
			if (valid_sms_sender()){
				$('form_buttons').fade(0);
				$('send_sms_now').addClass('none');
				progress_count = $('send_list').getElements('.wait_for_delivery').length + 2;
				$('progress').removeClass('none');
				new Request.JSON({
					url: '/sms/edit/',
					data: {
						'data[Sms][name]' : $('SmsName').value,
						'data[Sms][text]' : $('SmsText').value
					},
					onComplete: function(json){
						if (json){
							if (json.result === true) {
								send_sms_item(json.id);
								progress_step ++;
								redraw_progress();
							} else {
								$('SmsLogs').value += json.message + "\n";
								alert(json.message);
							}
						} else {
							alert('Chyba aplikace')
						}
					}
				}).send();
			}
		});
	
	// reindexace rel
	function reindex(parent_){
		if($type (parent_) != 'array') parent_ = [parent_];
		$each(parent_, function(parentE){
			var parentE = $(parentE);
			parentE.getElements('.choose_li').each(function(li,index){
				li.setProperty('rel',index);
			});
		});
	}

	function redraw_progress(){
		var bar = $('progress').getElement('.bar');
		var label = $('progress').getElement('.label');
		var percento = 100 * progress_step / progress_count;
		bar.setStyle('width',percento + '%');
		label.setHTML('Odesílání SMS - ' + Math.round(percento) + '%')
	}

	//move all right
	if($('move_right_all'))
	$('move_right_all').addEvent('click',function(e){
		new Event(e).stop();
		$('choose_list').getElements('.choose_li').addClass('selected');
		move_right();
	});
	
	// move selected right
	if($('move_right_selected'))
	$('move_right_selected').addEvent('click',function(e){
		new Event(e).stop();
		move_right();
	});
	
	function move_right(){
		$('send_parent_div').addClass('preloader');
		var error_move = [];
		$('choose_list').getElements('.selected').each(function(item){
			var exist_user = $('send_list').getElement('.phone_number[value=' + item.getElement('.phone_number').value + ']');
			if(!exist_user){
				$('send_list').adopt(item);
				item.removeClass('selected');
				item.getElement('.sms_stav').removeClass('none');
			} else {
				item.removeClass('selected');
				error_move.push('Klient ' + item.getElement('.client_name').value + ' nebyl přidán do seznamu, toto cislo již v seznamu je u jména ' + exist_user.getParent('li').getElement('.client_name').value + '.');
			}
		});
		if (error_move.length > 0){
			
			var error_move_message = new MyAlert();
			error_move_message.show(error_move,{button_label:'Ok','caption':'Upozornění'});
		}
		reindex(['choose_list','send_list']);
		$('send_parent_div').removeClass('preloader');
	}

	$('close_sms_sender').addEvent('click',function(e){
		new Event(e).stop();
        <?php if(isset($from) && $from=='editace'){?>
        domwin.closeWindow('domwin_sms');
        <?php }
        else  {?>
		domwin.closeWindow('domwin');
        <?php }?>
    });
    
    
    
    // natahnuti sablony SMS
	if($('SmsSmsTemplateId'))
		$('SmsSmsTemplateId').addEvent('change',function(e){
		  if(this.value != ''){
			new Event(e).stop();
				new Request.JSON({
					url: '/sms/load_sms_template/'+this.value,
					onComplete: function(json){
						if (json){
							if (json.result === true) {
								$('SmsName').value = json.name;
                                $('SmsText').value = json.text;
                                //kontrola textu a uprava pocet znaku
                                $('SmsText').fireEvent('keyup');

                                $('SmsLogs').value += "Nacteni sablony "+json.name+"\n";
							} else {
							    $('SmsLogs').value += "Chyba nacteni sablony\n";
								alert('Chyba nacteni sablony');
							}
						} else {
							alert('Chyba aplikace')
						}
					}
				}).send();
          }else{
            $('SmsName').value = '';
            $('SmsText').value = '';
            $('SmsText').fireEvent('keyup');
          }  
		});
        
    
	if($('add_number_without_system')){
		$('add_number_without_system').addEvent('click',function(e){
			new Event(e).stop();
            var number = $('WithoutSystemNumber').value;
            //validace
    		if(number == ''){
               alert('Hodnota telefonu je prázdna, telefon musí být vyplnen aby mohl být platný.');
            }
            else if(number.length < 14){
               alert('Telefon musí mít 14 znaků, aby mohl být platný.');
            }
            
            //existuje jiz v listu?!
            var exist_user = $('send_list').getElement('.phone_number[value=' + number + ']');
			if(!exist_user){
				var li = new Element('li',{'class':'choose_li','rel':$('send_list').getElements('li').length,'style':'-moz-user-select:none;'}).inject($('send_list'));
                li.addEvent('click',select_unselect.bindWithEvent(this)); 
                new Element('span',{'class':'col0'}).setHTML('mimo-system').inject(li);
                new Element('span',{'class':'col1'}).setHTML('&nbsp;'+number+'&nbsp;').inject(li);
                var span = new Element('span',{'class':'col2 sms_stav wait_for_delivery'}).inject(li);
                    var title = new Element('span',{'class':'title'}).inject(span);
                        new Element('span',{'class':'ta sms_wait'}).inject(title);
                    new Element('input',{'class':'phone_number','type':'hidden',value:number}).inject(span);
                    new Element('input',{'class':'client_id','type':'hidden',value:'-1'}).inject(span);    
                    new Element('input',{'class':'client_name','type':'hidden',value:'mimo-system'}).inject(span);
			} else {
				alert('Toto cislo již v seznamu je u jména ' + exist_user.getParent('li').getElement('.client_name').value + '.');
			}
            

		});    
    }    
//-->
</script>