# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from file: Revision: 7945 \webroot\test.php
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2009-02-05 08:28+0900\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: \plugins\api_generator\controllers\api_generator_controller.php:117
msgid "No file exists with that name"
msgstr ""

#: \plugins\api_generator\controllers\api_generator_controller.php:144
msgid "No class name was given"
msgstr ""

#: \plugins\api_generator\controllers\api_generator_controller.php:150;178
msgid "No class exists in the index with that name"
msgstr ""

#: \plugins\api_generator\controllers\api_generator_controller.php:165
msgid "Oops, seems we couldn't get the documentation for that class."
msgstr ""

#: \plugins\api_generator\vendors\shells\api_index.php:64
msgid "Your database configuration was not found. Take a moment to create one."
msgstr ""

#: \plugins\api_generator\vendors\shells\api_index.php:100
msgid "Routes file updated"
msgstr ""

#: \plugins\api_generator\vendors\shells\api_index.php:103
msgid "Routes file NOT updated"
msgstr ""

#: \plugins\api_generator\views\api_generator\classes.ctp:53
msgid "Index"
msgstr ""

#: \plugins\api_generator\views\api_generator\classes.ctp:75
msgid "(cont.)"
msgstr ""

#: \plugins\api_generator\views\api_generator\files.ctp:7
msgid "All files"
msgstr ""

#: \plugins\api_generator\views\api_generator\files.ctp:17
#: \plugins\api_generator\views\api_generator\source.ctp:29
#: \plugins\api_generator\views\elements\sidebar\file_sidebar.ctp:28
msgid "No files"
msgstr ""

#: \plugins\api_generator\views\api_generator\no_class.ctp:7
msgid "No classes were found in the requested file"
msgstr ""

#: \plugins\api_generator\views\api_generator\search.ctp:10
msgid "Search Results"
msgstr ""

#: \plugins\api_generator\views\api_generator\search.ctp:12
msgid "Your search returned no results"
msgstr ""

#: \plugins\api_generator\views\elements\api_menu.ctp:4
msgid "Classes"
msgstr ""

#: \plugins\api_generator\views\elements\api_menu.ctp:11
msgid "Source"
msgstr ""

#: \plugins\api_generator\views\elements\api_menu.ctp:18
msgid "Files"
msgstr ""

#: \plugins\api_generator\views\elements\class_info.ctp:12
msgid "Class Declaration:"
msgstr ""

#: \plugins\api_generator\views\elements\class_info.ctp:15
msgid "File name:"
msgstr ""

#: \plugins\api_generator\views\elements\class_info.ctp:18
msgid "Summary:"
msgstr ""

#: \plugins\api_generator\views\elements\class_info.ctp:22
msgid "Class Inheritance"
msgstr ""

#: \plugins\api_generator\views\elements\class_info.ctp:27
msgid "Interfaces Implemented"
msgstr ""

#: \plugins\api_generator\views\elements\element_toc.ctp:10
msgid "Defined Classes"
msgstr ""

#: \plugins\api_generator\views\elements\element_toc.ctp:22
msgid "Declared Functions"
msgstr ""

#: \plugins\api_generator\views\elements\function_summary.ctp:18
#: \plugins\api_generator\views\elements\method_detail.ctp:24
msgid "Parameters:"
msgstr ""

#: \plugins\api_generator\views\elements\function_summary.ctp:29
#: \plugins\api_generator\views\elements\method_detail.ctp:35
msgid "(no default)"
msgstr ""

#: \plugins\api_generator\views\elements\function_summary.ctp:38
msgid "Function defined in file:"
msgstr ""

#: \plugins\api_generator\views\elements\header_search.ctp:20
msgid "Search"
msgstr ""

#: \plugins\api_generator\views\elements\method_detail.ctp:44
msgid "Method defined in class:"
msgstr ""

#: \plugins\api_generator\views\elements\method_detail.ctp:47
msgid "Method defined in file:"
msgstr ""

#: \plugins\api_generator\views\elements\method_detail.ctp:52
msgid " on line "
msgstr ""

#: \plugins\api_generator\views\elements\method_summary.ctp:10
msgid "Method Summary:"
msgstr ""

#: \plugins\api_generator\views\elements\method_summary.ctp:13
msgid "Show/Hide parent methods"
msgstr ""

#: \plugins\api_generator\views\elements\paging.ctp:9
msgid "previous"
msgstr ""

#: \plugins\api_generator\views\elements\paging.ctp:17
msgid "next"
msgstr ""

#: \plugins\api_generator\views\elements\properties.ctp:9
msgid "Properties:"
msgstr ""

#: \plugins\api_generator\views\elements\properties.ctp:13
msgid "Show/Hide parent properties"
msgstr ""

#: \plugins\api_generator\views\elements\sidebar\class_sidebar.ctp:7
msgid "Class Index"
msgstr ""

#: \plugins\api_generator\views\elements\sidebar\file_sidebar.ctp:7
msgid "File browser"
msgstr ""

#: \plugins\api_generator\views\elements\sidebar\file_sidebar.ctp:10
msgid "Up one folder"
msgstr ""

#: \plugins\api_generator\views\layouts\default.ctp:29;51
msgid "CakePHP: API Generator"
msgstr ""

#: \plugins\api_generator\views\layouts\default.ctp:68
msgid "CakePHP: the rapid development php framework"
msgstr ""

#: \webroot\test.php:99
msgid "Debug setting does not allow access to this url."
msgstr ""
