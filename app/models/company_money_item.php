<?php
class CompanyMoneyItem extends AppModel {
    var $name = 'CompanyMoneyItem';
	//var $useTable = 'company_work_money';
	
	function beforeSave(){
		if (
			isset($this->data[$this->name]['checkbox_pracovni_smlouva']) &&
			isset($this->data[$this->name]['checkbox_faktura']) &&
			isset($this->data[$this->name]['checkbox_dohoda']) &&
			isset($this->data[$this->name]['checkbox_cash']) &&
            $this->data[$this->name]['pausal'] == 0 &&
            $this->data[$this->name]['h1000'] == 0
		) {
            /*if($this->data[$this->name]['checkbox_pracovni_smlouva'] == 1){
                $CMIname = 'HPP';
            }else if($this->data[$this->name]['checkbox_dohoda'] == 1){
                $CMIname = 'DPC';
            }else if($this->data[$this->name]['checkbox_faktura'] == 1){
                $CMIname = 'ZL';
            }else if($this->data[$this->name]['checkbox_cash'] == 1){
                $CMIname = 'CASH';
            }else{
                $CMIname = '';
            }*/
			$this->data[$this->name]['name'] = $this->data[$this->name]['checkbox_pracovni_smlouva'].$this->data[$this->name]['checkbox_dohoda'].$this->data[$this->name]['checkbox_faktura'].$this->data[$this->name]['checkbox_cash'];
			//$this->data[$this->name]['name'] = $CMIname;
        }
        else if(isset($this->data[$this->name]['pausal']) && $this->data[$this->name]['pausal'] == 1) //novy pausal
            $this->data[$this->name]['name'] = '00001';
            //$this->data[$this->name]['name'] = 'PAUSAL';
        else if(isset($this->data[$this->name]['h1000']) && $this->data[$this->name]['h1000'] == 1) { //novy pausal
            $this->data[$this->name]['name'] = '1000H';
            //$this->data[$this->name]['name'] = 'HPAUSAL';
        }
		return $this->data;
	}
}
?>