<?php
/**
 * 
 * @author Zbyněk Strnad - Fastest Solution s.r.o.
 * @created 16.9.2009
 */
class Sms extends AppModel {
    var $name = 'Sms';
    
    /**
     * 
     * @return data to save
     */
	function beforeSave(){	
		if (isset($this->data[$this->name]['odberatele'])){
				$this->data[$this->name]['odberatele'] = serialize($this->data[$this->name]['odberatele']);
		}
		return $this->data;
    }
	
    /**
     * 
     * @param $data, ARRAY from SQL
     * @return Array after modify
     */
	function afterFind($data){
		if (isset($data) && count($data)>0){
			foreach ($data as $key=>$item){
				if (isset($data[$key][$this->name]['odberatele'])){
					$data[$key][$this->name]['odberatele'] = unserialize($data[$key][$this->name]['odberatele']);
				}
			}
		}
		return $data;
    }
}
?>