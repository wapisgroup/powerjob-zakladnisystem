<?php

class ClientView extends AppModel
{
    var $name = 'ClientView';
    private $ignore_en_condition = false;
    //var $useTable = 'clients';
    /*
     CREATE ALGORITHM=UNDEFINED DEFINER=`powerjob`@`%` SQL SECURITY DEFINER VIEW `wapis__client_views` AS
    select
    (case `Client`.`mobil_active`
        when 'mobil1' then `Client`.`mobil1`
        when 'mobil2' then `Client`.`mobil2`
        when 'mobil3' then `Client`.`mobil3` end) AS `mobil`,
    if((`cacwp`.`id` <> 0),3,if((`ConnectionClientRequirement`.`id` <> 0),
    (case `ConnectionClientRequirement`.`type`
        when 2 then 2
        when 1 then 1 end),0)) AS `stav`,
    if((`Client`.`datum_narozeni` <> ''), timestampdiff(YEAR,`Client`.`datum_narozeni`,now()),'-') AS `vek`,
    `Client`.`mobil_active` AS `mobil_active`,
    `Client`.`id` AS `id`,
    `Client`.`import_stav` AS `import_stav`,
    `Company`.`client_manager_id` AS `client_manager_id`,
    `Client`.`stav` AS `status`,
    `Company`.`coordinator_id` AS `coordinator_id`,
    `Company`.`coordinator_id2` AS `coordinator_id2`,
    `Company`.`self_manager_id` AS `self_manager_id`,
    `Client`.`client_type` AS `client_type`,
    `Client`.`name` AS `name`,
    `Company`.`name` AS `company`,
    `ClientManager`.`name` AS `client_manager`,
    `Coordinator`.`name` AS `coordinator`,
    `Coordinator2`.`name` AS `coordinator2`,
    `Client`.`created` AS `created`,
    `Client`.`updated` AS `updated`,
    `Client`.`kos` AS `kos`,
    `Client`.`express` AS `express`,
    `Client`.`import_adresa` AS `import_adresa`,
    group_concat(distinct `SettingCareerItem`.`name` separator '<br/>') AS `Profese`,
    `Client`.`mobil1` AS `mobil1`,
    `Client`.`mobil2` AS `mobil2`,
    `Client`.`mobil3` AS `mobil3`,
    `uk`.`stav` AS `chybna_dokumentace`,
    `ConnectionClientRequirement`.`company_money_item_id` AS `company_money_item_id`,
    `Client`.`cislo_uctu` AS `cislo_uctu`,
    `Client`.`cislo_uctu_control` AS `cislo_uctu_control`,
    `Client`.`mesto` AS `mesto`,
    `Client`.`datum_narozeni` AS `datum_narozeni`,
    `Client`.`countries_id` AS `countries_id`,
    `Client`.`externi_nabor` AS `externi_nabor`,
    `Client`.`email` AS `email`,
    `Client`.`private_email` AS `private_email`,
    `Client`.`os_cislo` AS `os_cislo`,
    `Client`.`zbyva_dovolene` AS `zbyva_dovolene`,
    `Client`.`parent_id` AS `parent_id`,
    `Client`.`pohlavi_list` AS `pohlavi_list`,
    `Client`.`location_work` AS `location_work`,
    `Client`.`jmeno` AS `jmeno`,`Client`.`prijmeni` AS `prijmeni`,
    `Countrie`.`name` AS `okres`,
    `Client`.`os_pohovor` AS `os_pohovor`,
    `Client`.`os_pohovor_poznamka` AS `os_pohovor_poznamka`,
    `Client`.`rodne_cislo` AS `rodne_cislo`,
    `Client`.`info_about_job` AS `info_about_job`,
    `Client`.`ulice` AS `ulice`,
    `Client`.`psc` AS `psc`,
    `Client`.`stat_id` AS `stat_id` from

    (((((((((((`wapis__clients` `Client` left join `wapis__connection_client_career_items` `ConnectionClientCareerItem` on((`ConnectionClientCareerItem`.`client_id` = `Client`.`id`)))
    left join `wapis__setting_career_items` `SettingCareerItem` on((`SettingCareerItem`.`id` = `ConnectionClientCareerItem`.`setting_career_item_id`)))
    left join `wapis__connection_client_requirements` `ConnectionClientRequirement` on(((`ConnectionClientRequirement`.`client_id` = `Client`.`id`) and ((`ConnectionClientRequirement`.`type` = 2) or (`ConnectionClientRequirement`.`type` = 1)) and (`ConnectionClientRequirement`.`to` = '0000-00-00'))))
    left join `wapis__companies` `Company` on((`ConnectionClientRequirement`.`company_id` = `Company`.`id`))) left join `wapis__cms_users` `ClientManager` on((`Company`.`client_manager_id` = `ClientManager`.`id`)))
    left join `wapis__cms_users` `Coordinator` on((`Company`.`coordinator_id` = `Coordinator`.`id`)))
    left join `wapis__cms_users` `Coordinator2` on((`Company`.`coordinator_id2` = `Coordinator2`.`id`)))
    left join `wapis__connection_client_at_company_work_positions` `cacwp` on(((`cacwp`.`client_id` = `Client`.`id`) and (`cacwp`.`datum_to` = '0000-00-00'))))
    left join `wapis__company_money_items` `cmi` on((`cmi`.`id` = `ConnectionClientRequirement`.`company_money_item_id`)))
    left join `wapis__client_ucetni_dokumentace` `uk` on(((`uk`.`connection_client_requirement_id` = `ConnectionClientRequirement`.`id`) and (`uk`.`forma` = `cmi`.`name`))))
    left join `wapis__countries` `Countrie` on((`Countrie`.`id` = `Client`.`countries_id`)))
    group by `Client`.`id`

    */
    /* - MAIN
    DROP VIEW if exists `fastest__client_views`;
    CREATE VIEW fastest__client_views AS
        SELECT (
        CASE Client.mobil_active
         When 'mobil1' THEN Client.mobil1
         When 'mobil2' THEN Client.mobil2
         When 'mobil3' THEN Client.mobil3
        END
        ) as mobil,

        (
             IF(cacwp.id <> 0,3,
                    IF(ConnectionClientRequirement.id <> 0,(
                        CASE ConnectionClientRequirement.type
                         When 2 THEN 2
                         When 1 THEN 1
                        END),0)
             )

         ) as  stav,
         (
            IF(Client.datum_narozeni <> '',TIMESTAMPDIFF(YEAR,Client.datum_narozeni,NOW()),'-')
         ) as vek,
        Client.mobil_active,Client.id, Client.import_stav, Company.client_manager_id as client_manager_id,
                    Company.coordinator_id as coordinator_id, Company.coordinator_id2 as coordinator_id2,
                    Company.self_manager_id as self_manager_id,
                    Client.client_type as client_type,
                    Client.name, Company.name as company, ClientManager.name as client_manager,
                    Coordinator.name as coordinator,Coordinator2.name as coordinator2,
                    Client.created, Client.updated, Client.kos, Client.express, Client.import_adresa,
                    GROUP_CONCAT(DISTINCT SettingCareerItem.name SEPARATOR '<br/>') as Profese,
                    Client.mobil1,Client.mobil2,Client.mobil3,
                     uk.stav as chybna_dokumentace,ConnectionClientRequirement.company_money_item_id,
                     Client.cislo_uctu,Client.cislo_uctu_control,Client.mesto,Client.datum_narozeni,
                     Client.countries_id,Client.externi_nabor,Client.email,Client.private_email,
                     Client.os_cislo,Client.zbyva_dovolene,Client.parent_id,Client.pohlavi_list,
                     Client.jmeno,Client.prijmeni, Countrie.name as okres,Client.os_pohovor,
                     Client.os_pohovor_poznamka,Client.rodne_cislo,Client.info_about_job,
                     Client.ulice,Client.psc,Client.stat_id
            FROM `fastest__clients` AS Client
                LEFT JOIN fastest__connection_client_career_items AS ConnectionClientCareerItem ON (ConnectionClientCareerItem.client_id = Client.id)
                LEFT JOIN fastest__setting_career_items AS SettingCareerItem ON (SettingCareerItem.id = ConnectionClientCareerItem.setting_career_item_id)
                LEFT JOIN `fastest__connection_client_requirements` as ConnectionClientRequirement ON (ConnectionClientRequirement.client_id = Client.id AND (ConnectionClientRequirement.type = 2 OR ConnectionClientRequirement.type = 1) AND ConnectionClientRequirement.to='0000-00-00')
                LEFT JOIN  fastest__companies as Company ON (ConnectionClientRequirement.company_id = Company.id)
                LEFT JOIN fastest__cms_users as ClientManager ON (Company.client_manager_id = ClientManager.id)
                LEFT JOIN fastest__cms_users as Coordinator ON (Company.coordinator_id = Coordinator .id)
                LEFT JOIN fastest__cms_users as Coordinator2 ON (Company.coordinator_id2 = Coordinator2 .id)
                LEFT JOIN fastest__connection_client_at_company_work_positions as cacwp ON (cacwp.client_id = Client.id AND cacwp.datum_to='0000-00-00')
                LEFT JOIN fastest__company_money_items as cmi ON (cmi.id = ConnectionClientRequirement.company_money_item_id )
                LEFT JOIN fastest__client_ucetni_dokumentace as uk ON (uk.connection_client_requirement_id = ConnectionClientRequirement.id AND uk.forma = cmi.name )
                LEFT JOIN fastest__countries as Countrie ON (Countrie.id = Client.countries_id)

    GROUP BY Client.id
    */

    /* - ALTERNATIVE - OLD
    CREATE VIEW fastest__client_views AS
    SELECT Client.id, Client.import_stav, Company.client_manager_id as client_manager_id,
            Client.name, Company.name as company, ClientManager.name as client_manager,
            Coordinator.name as coordinator, Client.created, Client.updated, Client.telefon1,
            Client.stav, Client.kos, SettingCareerItem.name as profese_name,SettingCareerItem.id as profese_id
    FROM `fastest__clients` AS Client
        LEFT JOIN fastest__connection_client_career_items AS ConnectionClientCareerItem ON (ConnectionClientCareerItem.client_id = Client.id)
        LEFT JOIN fastest__setting_career_items AS SettingCareerItem ON (SettingCareerItem.id = ConnectionClientCareerItem.setting_career_item_id)
        LEFT JOIN `fastest__connection_client_requirements` as ConnectionClientRequirement ON (ConnectionClientRequirement.client_id = Client.id AND ConnectionClientRequirement.type = 2)
        LEFT JOIN  fastest__companies as Company ON (ConnectionClientRequirement.company_id = Company.id)
        LEFT JOIN fastest__cms_users as ClientManager ON (Company.client_manager_id = ClientManager.id)
        LEFT JOIN fastest__cms_users as Coordinator ON (Company.coordinator_id = Coordinator .id)

    */

    /*  New FULL with Client - only for export
    
    	DROP VIEW if exists `fastest__client_views`;
	CREATE VIEW fastest__client_views AS  
		SELECT (
        CASE Client.mobil_active
         When 'mobil1' THEN Client.mobil1
         When 'mobil2' THEN Client.mobil2
         When 'mobil3' THEN Client.mobil3
        END
        ) as mobil, Company.client_manager_id as client_manager_id, 
        			Company.coordinator_id as coordinator_id, Company.coordinator_id2 as coordinator_id2, 
        			Company.self_manager_id as self_manager_id, 
        		    Company.name as company, ClientManager.name as client_manager, 
        			Coordinator.name as coordinator,Coordinator2.name as coordinator2, 
                    GROUP_CONCAT(DISTINCT SettingCareerItem.name SEPARATOR '<br/>') as Profese,
                    GROUP_CONCAT(DISTINCT SettingCertificate.name SEPARATOR '<br/>') as Certifikaty,
                    GROUP_CONCAT(DISTINCT Recruiter.name SEPARATOR '<br/>') as Recruiters,
                     uk.stav as chybna_dokumentace,ConnectionClientRequirement.company_money_item_id,
                    Client.*
        	FROM `fastest__clients` AS Client 
        		LEFT JOIN fastest__connection_client_career_items AS ConnectionClientCareerItem ON (ConnectionClientCareerItem.client_id = Client.id) 
        		LEFT JOIN fastest__connection_client_certifikaty_items AS ConnectionClientCertifikatyItem ON (ConnectionClientCertifikatyItem.client_id = Client.id) 
        		LEFT JOIN fastest__connection_client_recruiters AS ConnectionClientRecruiter ON (ConnectionClientRecruiter.client_id = Client.id) 
        		LEFT JOIN fastest__setting_career_items AS SettingCareerItem ON (SettingCareerItem.id = ConnectionClientCareerItem.setting_career_item_id) 
        		LEFT JOIN fastest__setting_certificates AS SettingCertificate ON (SettingCertificate.id = ConnectionClientCertifikatyItem.setting_certificate_id) 
        		LEFT JOIN `fastest__connection_client_requirements` as ConnectionClientRequirement ON (ConnectionClientRequirement.client_id = Client.id AND (ConnectionClientRequirement.type = 2 OR ConnectionClientRequirement.type = 1) AND ConnectionClientRequirement.to='0000-00-00')  
        		LEFT JOIN  fastest__companies as Company ON (ConnectionClientRequirement.company_id = Company.id) 
        		LEFT JOIN fastest__cms_users as ClientManager ON (Company.client_manager_id = ClientManager.id) 
        		LEFT JOIN fastest__cms_users as Coordinator ON (Company.coordinator_id = Coordinator .id) 
        		LEFT JOIN fastest__cms_users as Coordinator2 ON (Company.coordinator_id2 = Coordinator2 .id) 
        		LEFT JOIN fastest__cms_users as Recruiter ON (Recruiter.id = ConnectionClientRecruiter.cms_user_id) 
        		LEFT JOIN fastest__company_money_items as cmi ON (cmi.id = ConnectionClientRequirement.company_money_item_id ) 
        		LEFT JOIN fastest__client_ucetni_dokumentace as uk ON (uk.connection_client_requirement_id = ConnectionClientRequirement.id AND uk.forma = cmi.name ) 
	GROUP BY Client.id
    
    */

    /**
     * funkce obsluhujici status a jeho nastavnei pro
     * potvrzeni ci vyvraceni podminky pro EN nabor
     */
    function get_ignore_status()
    {
        return $this->ignore_en_condition;
    }

    function set_ignore_status($status)
    {
        $this->ignore_en_condition = $status;
    }


    function beforeFind($queryData)
    {
        $queryData = parent::beforeFind($queryData);
        /**
         * pokud jsou nastaveny conditions pridej k nim podminky pro ATEP
         * videt pouze od interniho naboru
         * tedy externi_nabor = 0
         */
        if (isset($queryData['conditions']) && !$this->ignore_en_condition) {
            $my_con = array(
                'externi_nabor' => 0
            );

            $queryData['conditions'] = am($queryData['conditions'], $my_con);
        }

        //pr($queryData);

        return $queryData;
    }
}

?>