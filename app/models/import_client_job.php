<?php
class ImportClientJob extends AppModel {
    var $name = 'ImportClientJob';
	var $useTable = 'client_jobsps';
	var $useDbConfig = 'at_cms_jobs';
	
	function beforeSave(){
		if (isset($this->data[$this->name]['jmeno']) && isset($this->data[$this->name]['prijmeni']))
			$this->data[$this->name]['name'] = $this->data[$this->name]['prijmeni'] . ' ' . $this->data[$this->name]['jmeno'];
			
		return $this->data;
    }
    
    function afterFind($data){
		if (isset($data) && count($data)>0){
			foreach ($data as $key=>$item){
				
				if (isset($data[$key][$this->name]['files']))
					$data[$key][$this->name]['files'] = unserialize($data[$key][$this->name]['files']);
				
				
			}
		}
		return $data;
    }

}
?>