<?php
class ReportFuelConsumptionAttachment extends AppModel {
    var $name = 'ReportFuelConsumptionAttachment';
	
    function beforeSave(){
		if (isset($this->data[$this->name]['name']))
			$this->data[$this->name]['alias_'] = $this->createAlias($this->data[$this->name]['name']);
		return $this->data;
    }
}
?>