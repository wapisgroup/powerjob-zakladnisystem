<?php
class ClientWorkingHoursInt extends AppModel {
    var $name = 'ClientWorkingHoursInt';
    var $useTable = 'client_working_hours_int';

    public function sum_accommodation_days($accommodation_days = null){  
        $count = 0;
        
        if($accommodation_days <> null){
            foreach($accommodation_days as $day)
                if($day == 1)
                    $count++;
        }
        
        return $count;    
    }
    
    public function odpracoval($d){
        $result = false;
        
        foreach($d as $key=>$value){
            if($value > 0)
                $result = true;
        }
        
        return $result;
    }
	
	function beforeSave(){	
		if (isset($this->data[$this->name]['days'])){ 
		     $count_days = 0;
             $days = array(); 
          
             foreach($this->data[$this->name]['days'] as $key=>$day){
                    list($null,$x,$y) = explode('_',$key);
                    $days[$y][$x] = $day;
             }
             
             if(!empty($days)){
                 foreach($days as $d){
                    if(self::odpracoval($d))
                        $count_days++;
                 }
             }  
		  
             $this->data[$this->name]['working_days_count'] = $count_days;
          
		     $this->data[$this->name]['days'] = serialize($this->data[$this->name]['days']); 
        }
		if (isset($this->data[$this->name]['food_days'])){ 			
		  
              /**
               * vytvoreni sloupce food_days_count
               * pocet zaskrtnutych stravnych dnu
               */
              $this->data[$this->name]['food_days_count'] = self::sum_accommodation_days($this->data[$this->name]['food_days']);
           
              
              $this->data[$this->name]['food_days'] 			= serialize($this->data[$this->name]['food_days']); 
        }
		if (isset($this->data[$this->name]['accommodation_days'])){ 
 
              /**
               * vytvoreni sloupce accommodation_days_count
               * pocet zaskrtnutych ubytovacich dnu
               */
              $this->data[$this->name]['accommodation_days_count'] = self::sum_accommodation_days($this->data[$this->name]['accommodation_days']);
       
              $this->data[$this->name]['accommodation_days'] 	= serialize($this->data[$this->name]['accommodation_days']); 
        
        }
		return $this->data;
    }
	
	function afterFind($data){
		if (isset($data) && count($data)>0){
			foreach ($data as $key=>$item){
				if (isset($data[$key][$this->name]['days'])){ 				$data[$key][$this->name]['days'] = unserialize($data[$key][$this->name]['days']); }
				if (isset($data[$key][$this->name]['food_days'])){ 			$data[$key][$this->name]['food_days'] = unserialize($data[$key][$this->name]['food_days']); }
				
                if (isset($data[$key][$this->name]['accommodation_days'])){ 
				    $data[$key][$this->name]['accommodation_days'] = unserialize($data[$key][$this->name]['accommodation_days']); 
                }
			}
		}
		return $data;
    }
}
?>