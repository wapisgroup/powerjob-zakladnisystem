<?php
class EbClientWorkingHour extends AppModel {
    var $name = 'EbClientWorkingHour';
	

	function beforeSave(){	
		if (isset($this->data[$this->name]['days'])){ 				$this->data[$this->name]['days'] 				= serialize($this->data[$this->name]['days']); }
		if (isset($this->data[$this->name]['food_days'])){ 			$this->data[$this->name]['food_days'] 			= serialize($this->data[$this->name]['food_days']);         }
		if (isset($this->data[$this->name]['accommodation_days'])){ $this->data[$this->name]['accommodation_days'] 	= serialize($this->data[$this->name]['accommodation_days']);  }
		if (isset($this->data[$this->name]['ukolovky'])){ 			$this->data[$this->name]['ukolovky'] 			= serialize($this->data[$this->name]['ukolovky']);  }

		return $this->data;
    }
	
	function afterFind($data){
		if (isset($data) && count($data)>0){
			foreach ($data as $key=>$item){
				if (isset($data[$key][$this->name]['days'])){ 				$data[$key][$this->name]['days'] = unserialize($data[$key][$this->name]['days']); }
				if (isset($data[$key][$this->name]['food_days'])){ 			$data[$key][$this->name]['food_days'] = unserialize($data[$key][$this->name]['food_days']); }
                if (isset($data[$key][$this->name]['accommodation_days'])){ $data[$key][$this->name]['accommodation_days'] = unserialize($data[$key][$this->name]['accommodation_days']); }
                if (isset($data[$key][$this->name]['ukolovky'])){ 			$data[$key][$this->name]['ukolovky'] = unserialize($data[$key][$this->name]['ukolovky']); }
			}
		}
		return $data;
    }
}
?>