<?php
class NewMoneyItemForm extends AppModel {
    var $name = 'NewMoneyItemForm';
	
    
    public function generate_name($data){
        $form_name = '0000';
        
        if (
			isset($data['pp']) &&
			isset($data['do']) &&
			isset($data['zl']) &&
			isset($data['cash'])
		) {
			$form_name = $data['pp'].$data['do'].$data['zl'].$data['cash'];
		}
        
        return $form_name;
    }
}
?>