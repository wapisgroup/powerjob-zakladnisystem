<?php
class CmsGroup extends AppModel {
    var $name = 'CmsGroup';
	
	function beforeSave(){
		if (isset($this->data[$this->name]['permission']))
			$this->data[$this->name]['permission'] = serialize($this->data[$this->name]['permission']);
        if (isset($this->data[$this->name]['usershare_folders']))
			$this->data[$this->name]['usershare_folders'] = serialize($this->data[$this->name]['usershare_folders']);    
		
		return $this->data;
    }
	
	function afterFind($data){
		if (isset($data) && count($data)>0){
			foreach ($data as $key=>$item){
				if (isset($item[$this->name]['permission']))
					$data[$key][$this->name]['permission'] = unserialize($item[$this->name]['permission']);
                if (isset($item[$this->name]['usershare_folders']))
					$data[$key][$this->name]['usershare_folders'] = unserialize($item[$this->name]['usershare_folders']);    
			}
		}
		return $data;
    }
}
?>