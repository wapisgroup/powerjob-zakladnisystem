<?php
class ConnectionClientCareerItem extends AppModel {
    var $name = 'ConnectionClientCareerItem';
    var $ignore_en_condition = false;
    var $is_bind_client = false;
	//var $belongsTo = array('SettingCareerItem');
    
    function bindModel($array){
        parent::bindModel($array);

        
        if(in_array('Client',$array['belongsTo'])){
            $this->is_bind_client = true;
            
            if($this->Client->bind_en)
                $this->ignore_en_condition = true;
        }    
    }
    
    
    function beforeFind($queryData){
		
        /**
         * pokud jsou nastaveny conditions pridej k nim podminky pro ATEP
         * videt pouze od interniho naboru
         * tedy externi_nabor = 0
         */
        if(isset($queryData['conditions']) && !$this->ignore_en_condition){
            $my_con = array(
                'Client.externi_nabor' => 0
            );
            
            //pokud client neni nabindovan nemuzem ho podminkovat
            if(!$this->is_bind_client)
                $my_con = array();
    
            $queryData['conditions'] = am($queryData['conditions'],$my_con);
        }
        
        //pr($queryData);
        
		return $queryData;
    }
    
}
?>