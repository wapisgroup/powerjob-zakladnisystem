<?php
class ClientManagerFeeView extends AppModel {
    var $name = 'ClientManagerFeeView';
	var $useTable = 'client_manager_fee_views';
	
	/*
	create VIEW fastest__client_manager_fee_views as 
	
    SELECT 
        `ClientWorkingHour1`.`year`, 
        `ClientWorkingHour1`.`month`, 
        `ClientWorkingHour1`.`id`, 
        `Company1`.`client_manager_id` as user_id, 
        `Company1`.`id` as company_id,
        `Company1`.`name`, 
        SUM(`ClientWorkingHour1`.`celkem_hodin`) as zaklad, 
        'client_manager' as user_type, 
        '0' as kos 
    FROM `fastest__client_working_hours` AS `ClientWorkingHour1` 
    LEFT JOIN `fastest__companies` AS `Company1` ON (`ClientWorkingHour1`.`company_id` = `Company1`.`id`) 
    GROUP BY `Company1`.`id`, `ClientWorkingHour1`.`year`, `ClientWorkingHour1`.`month` 
	
    UNION
	
    SELECT 
        `ClientWorkingHour2`.`year`, 
        `ClientWorkingHour2`.`month`, 
        `ClientWorkingHour2`.`id`, 
        `Company2`.`coordinator_id` as user_id,  
        `Company2`.`id` as company_id,
        `Company2`.`name`, 
        SUM(`ClientWorkingHour2`.`celkem_hodin`) as zaklad, 
        'coordinator' as user_type, 
        '0' as kos 
    FROM `fastest__client_working_hours` AS `ClientWorkingHour2` 
    LEFT JOIN `fastest__companies` AS `Company2` ON (`ClientWorkingHour2`.`company_id` = `Company2`.`id`) 
    GROUP BY `Company2`.`id`, `ClientWorkingHour2`.`year`, `ClientWorkingHour2`.`month` 
	*/
}
?>