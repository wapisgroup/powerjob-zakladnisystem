<?php
class MyClientsView extends AppModel {
    var $name = 'MyClientsView';
    private $ignore_en_condition = false;
	var $useTable = 'my_clients_view';

	/* - MAIN
CREATE VIEW fastest__my_clients_view  AS
SELECT 	ClientBuy.id,ClientBuy.from_date, ClientBuy.to_date, ClientBuy.price, ClientBuy.pay_date , ClientBuy.to_cms_user_id, ClientBuy.from_cms_user_id, ClientBuy.status,
	Client.cms_user_id ,Client.name as client_name, Client.id as client_id, Client.created,
	SettingCareerItem.name as position_name, SettingCareerItem.price_2, SettingCareerItem.price_1
From 	fastest__clients as Client JOIN
	fastest__client_buys as ClientBuy ON ClientBuy.client_id = Client.id JOIN
	fastest__setting_career_items as SettingCareerItem ON SettingCareerItem.id = ClientBuy.setting_career_item_id
UNION
SELECT 	ClientBuy.id,ClientBuy.from_date, ClientBuy.to_date, ClientBuy.price, ClientBuy.pay_date , ClientBuy.to_cms_user_id, ClientBuy.from_cms_user_id, ClientBuy.status,
	Client.cms_user_id ,Client.name as client_name, Client.id as client_id, Client.created,
	SettingCareerItem.name as position_name, SettingCareerItem.price_2, SettingCareerItem.price_1
From 	fastest__clients as Client LEFT JOIN
	fastest__client_buys as ClientBuy ON ClientBuy.client_id = Client.id LEFT JOIN
	fastest__setting_career_items as SettingCareerItem ON SettingCareerItem.id = ClientBuy.setting_career_item_id
    
    */


}
?>