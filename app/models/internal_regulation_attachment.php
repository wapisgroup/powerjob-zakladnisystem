<?php
class InternalRegulationAttachment extends AppModel {
    var $name = 'InternalRegulationAttachment';
	
	function beforeSave(){
		if (isset($this->data[$this->name]['name']))
			$this->data[$this->name]['alias_'] = $this->createAlias($this->data[$this->name]['name']);
		return $this->data;
    }
}
?>