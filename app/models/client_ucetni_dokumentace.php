<?php
class ClientUcetniDokumentace extends AppModel {
    var $name = 'ClientUcetniDokumentace';
    var $useTable = 'client_ucetni_dokumentace';
	
	function beforeSave(){	
		if (isset($this->data[$this->name]['dokumenty'])){ 				
            $this->data[$this->name]['dokumenty'] = serialize($this->data[$this->name]['dokumenty']); 
        }
		
        return $this->data;
    }
	
	function afterFind($data){
		if (isset($data) && count($data)>0){
			foreach ($data as $key=>$item){
				if (isset($data[$key][$this->name]['dokumenty'])){ 				
				    $data[$key][$this->name]['dokumenty'] = unserialize($data[$key][$this->name]['dokumenty']); 
                }			
            }
		}
		return $data;
    }
}
?>