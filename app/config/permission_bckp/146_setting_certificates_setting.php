<?php
	$modul_name = 'Nastavení certifikáty';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní',
			'status'=>	'Status'
		),
		'checkbox' => array(
		
		)
	);
	
	$modul_menu = array(
		'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(
			'setting_certificates' =>array(
				'name' 		=> 	'setting_certificates',
				'url'		=>	'/setting_certificates/',
				'caption'	=> 	'Nastavení certifikáty',
				'child'		=> 	null
			)
		)
	);

?>