<?php
	$modul_name = 'Typy kategorií příloh';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(
			'setting_attachment_types' =>array(
				'name' 		=> 	'setting_attachment_types',
				'url'		=>	'/setting_attachment_types/',
				'caption'	=> 	'Typy kategorií příloh',
				'child'		=> 	null
			)
		)
	);

?>