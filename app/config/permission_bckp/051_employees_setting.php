<?php

	$modul_name = 'Docházky zaměstnanců';



	$modul_permission = array(

		'radio' => array(

			'index'	=>	'Zobrazení',

			'edit'	=>	'Editace',

			'uzavrit_odpracovane_hodiny'=>	'Uzavření docházky',

			'ucetni_dokumentace'=>	'Učetní  dokumentace',

			'client_info'=>	'Detail Klienta',
            
            'modify_from_date'=>	'Editace datumu nástupu/ukončení',

		),

		'checkbox' => array(
          'editace_maximalek'	=> 'Editace maximálek',
		  'editable_money_item_workposition'	=> 'Možnost editovat profesi a formu odmeny na kartě docházky',
          'allow_transfer_dohoda_to_cash'	=> 'Přenos částky z dohody do abc',
          'allow_ignore_dpc'	=> 'Možnost ignorace DPČ', 
          'allow_uses_payment_amount'	=> 'Zadávání záloh', 
		),

		'select' => array(

			'group_id'=> array(

				'caption' 	=> 'Typ napojení',

				'data'		=> array(

					'self_manager_id'	=>	'self_manager_id',

					'coordinator_id'	=>	'coordinator_id',

					'coordinator_double'	=>	'coordinator_double',
                    'coordinator_new'	=>	'coordinator_new',

					'client_manager_id'	=>	'client_manager_id'

				)

			)

		)

	);

	$modul_menu = array(

	
	    'name' 		=> 	'modul_employees',

		'url'		=>	'#',

		'caption'	=> 	'Zaměstnanci',
		'child'		=> 	array(	

			'report_down_payments' =>array(

				'name' 		=> 	'employees',

				'url'		=>	'/employees/',

				'caption'	=> 	'Docházky zaměstnanců',

				'child'		=> 	'null'

			)

		)

	);

	



?>