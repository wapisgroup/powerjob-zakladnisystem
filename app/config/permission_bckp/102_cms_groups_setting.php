<?php
	$modul_name = 'Skupiny uživatelů';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Editace',
			'add'	=>	'Přidat',
            'groups'=>	'Nadřazené skupiny',
            'klon'=>	'Klonovat skupinu',
			'trash'=>	'Smazaní',
			'status'=>	'Status'
		),
		'checkbox' => array(
		
		)
	);
	
	$modul_menu = array(
		'name' 		=> 	'administration',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Administrace',
		'child'		=> 	array(
			'cms_groups' => array(
				'name' 		=> 	'cms_groups',
				'url'		=>	'/cms_groups/',
				'caption'	=> 	'Skupiny uživatelů systému',
				'child'		=> 	null
			)
		)
	);

?>