<?php
	$modul_name = 'Nastavení typů ubytování';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(
			'setting_accommodation_types' =>array(
				'name' 		=> 	'setting_accommodation_types',
				'url'		=>	'/setting_accommodation_types/',
				'caption'	=> 	'Nastavení typů ubytování',
				'child'		=> 	null
			)
		)
	);

?>