<?php
	Router::connect('/login/', array('controller' => 'cms_users', 'action' => 'login'));
	Router::connect('/logout/', array('controller' => 'cms_users', 'action' => 'logout'));
	Router::connect('/pages/isUnique/*', array('controller' => 'pages','action'=>'isUnique'));
	
	//formular
	Router::connect('/pro-zajemce/formular/', array('controller' => 'clients', 'action' => 'formular'));
/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
	Router::connect('/', array('controller' => 'pages', 'action' => 'main_page'));
?>