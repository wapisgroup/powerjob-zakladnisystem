<?php
	$modul_name = 'Nastavení důvodu propuštení';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(
			'setting_reason_at_sackings' =>array(
				'name' 		=> 	'setting_reason_at_sackings',
				'url'		=>	'/setting_reason_at_sackings/',
				'caption'	=> 	'Nastavení důvodu propuštení',
				'child'		=> 	null
			)
		)
	);

?>