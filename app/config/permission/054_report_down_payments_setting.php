<?php

	$modul_name = 'Reporty - Žádosti o zálohy';



	$modul_permission = array(

		'radio' => array(

			'index'	=>	'Zobrazení',

			'edit'	=>	'Editace',

			'add'	=>	'Přidání',

			'trash'=>	'Smazaní',

            'multi_edit'	=>	'Hromadné uhrazení',

            'uhrazeno'	=>	'Uhrazeno',

            'kb'	=>	'KB Export',

            'company_exception'	=>	'Firmy vyjimky'

		

			//'status'=>	'Status'

		),

		'checkbox' => array(

		

		),

		'select' => array(

			'group_id'=> array(

				'caption' 	=> 'Typ napojení',

				'data'		=> array(

					'self_manager_id'	=>	'self_manager_id',

					'coordinator_id'	=>	'coordinator_id',

					'coordinator_double'	=>	'coordinator_double',
                    'coordinator_new'	=>	'coordinator_new',
					'client_manager_id'	=>	'client_manager_id'

				)

			)

		)

	);

	$modul_menu = array(

	
	    'name' 		=> 	'modul_employees',

		'url'		=>	'#',

		'caption'	=> 	'Zaměstnanci',

		'child'		=> 	array(	

			'report_down_payments' =>array(

				'name' 		=> 	'report_down_payments',

				'url'		=>	'/report_down_payments/',

				'caption'	=> 	'Žádosti o zálohy',

				'child'		=> 	null

			)

		)

	);



?>