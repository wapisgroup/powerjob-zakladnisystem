<?php
	$modul_name = 'Nastavení typů úkolů';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(
			'setting_task_types' =>array(
				'name' 		=> 	'setting_task_types',
				'url'		=>	'/setting_task_types/',
				'caption'	=> 	'Nastavení typů úkolů',
				'child'		=> 	null
			)
		)
	);

?>