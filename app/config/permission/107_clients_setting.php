<?php

	$modul_name = 'Klienti';



	$modul_permission = array(

		'radio' => array(

			'index'	=>	'Zobrazení',

			'edit'	=>	'Editace',

			'add'	=>	'Přidání',

			'add_express'	=>	'Přidání Express',

			'trash'=>	'Smazaní',

			'attach'=>	'Přílohy',

			'add_recruiter'=>	'Přidání recruitera',

			'add_historie'=>	'Přidání historie',

			'add_hodnoceni'=>	'Přidání hodnocení',

			'rozvazat_prac_pomer'=>	'Odebrání ze zaměstnaní',

			'zmena_pp'=>	'Změna pracovního poměru',

			'stats'=>	'Statistiky spojení',

			'export_excel'=>	'Export excel',
            'domwin_add_activity'=>	'Přidat aktivitu'
		),

		'checkbox' => array(
    		'delete_activity'	=> 'Mazání aktivit',
            'add_bank_account'	=> 'Zadání bankovního účtu',
            'edit_bank_account'	=> 'Kontrola bankovního účtu',
             'upload_file'=>'Nahrát soubor',
            'delete_file'=>'Smazat soubor',
            'download_file'=>'Stáhnout soubor'
		),

		'select' => array(

			'group_id'=> array(

				'caption' 	=> 'Typ napojení',

				'data'		=> array(

					'self_manager_id'	=>	'self_manager_id',

					'coordinator_id'	=>	'coordinator_id',

					'coordinator_double'	=>	'coordinator_double',

					'client_manager_id'	=>	'client_manager_id'

				)

			)

		)

	);

	$modul_menu = array(
	'name' 		=> 	'administration',
		'url'		=>	'#',
		
		'caption'	=> 	'Administrace',

		'child'		=> 	array(

			'client_alls' => array(

				'name' 		=> 	'clients',

				'url'		=>	'/clients/',

				'caption'	=> 	'Klienti - všichni',

				'child'		=> 	null

			),

			/* 'client_zamestnans' => array(

				'name' 		=> 	'clientszam',

				'url'		=>	'/clients/?filtration_ClientView-stav=2',

				'caption'	=> 	'Klienti - všichni zaměstnani',

				'child'		=> 	null

			) */

			// ,'client_uchazecs' => array(

				// 'name' 		=> 	'clients',

				// 'url'		=>	'/clients/?filtration_ClientView-stav=0|1',

				// 'caption'	=> 	'Klienti - uchazeči',

				// 'child'		=> 	null

			// )

		)

	);

	



?>