<?php
	$modul_name = 'Úkoly v podnicích';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Editace'
			//'trash'=>	'Smazaní',
			//'status'=>	'Status'
		),
		'checkbox' => array(
		
		)
	);
	
	$modul_menu = array(
		'name' 		=> 	'companies',
		'url'		=>	'/companies/',
		'caption'	=> 	'Firmy',
		'child'		=> 	array(
			'company_tasks' =>array(
				'name' 		=> 	'company_tasks',
				'url'		=>	'/company_tasks/',
				'caption'	=> 	'Úkoly v podnicích',
				'child'		=> 	null
			)
		)
	);

?>