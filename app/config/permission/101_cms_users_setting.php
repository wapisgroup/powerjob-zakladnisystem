<?php
	$modul_name = 'Uživatelé systému';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Editace',
			'add'	=>	'Přidat',
			'trash'=>	'Smazaní',
			'status'=>	'Status'
		),
		'checkbox' => array(
		
		)
	);
	
	$modul_menu = array(
		'name' 		=> 	'administration',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Administrace',
		'child'		=> 	array(
			'cms_users' =>array(
				'name' 		=> 	'cms_users',
				'url'		=>	'/cms_users/',
				'caption'	=> 	'Uživatelé systému',
				'child'		=> 	null
			)
		)
	);

?>