<?php

	$modul_name = 'Klienti - černá listina';
    $modul_permission = array(

		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Karta klienta',
			'add'	=>	'Pridat klienta',
		),

		'checkbox' => array(
		),

		'select' => array(
		)

	);

	$modul_menu = array(
		'name' 		=> 	'report_requirements_for_recruitments',
		'url'		=>	'#',
		'caption'	=> 	'Nábor',
		'child'		=> 	array(
			'black_list_clients' => array(
				'name' 		=> 	'black_list_clients',
				'url'		=>	'/black_list_clients/',
				'caption'	=> 	'Klienti - černá listina',
				'child'		=> 	null
			)
		)
	);
?>