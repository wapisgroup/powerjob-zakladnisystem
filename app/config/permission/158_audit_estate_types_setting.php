<?php
	$modul_name = 'Typy majetku';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(
			'audit_estate_types' =>array(
				'name' 		=> 	'audit_estate_types',
				'url'		=>	'/audit_estate_types/',
				'caption'	=> 	'Typy majetku',
				'child'		=> 	null
			)
		)
	);

?>