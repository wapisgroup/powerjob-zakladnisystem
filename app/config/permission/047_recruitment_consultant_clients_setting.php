<?php

	$modul_name = 'Nábor - počet vložených uchazečů';
    $modul_permission = array(

		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Přehled klientů',
            'export_excel'=>	'Export excel',
		),

		'checkbox' => array(
		),

		'select' => array(
		)

	);

	$modul_menu = array(
		'name' 		=> 	'report_requirements_for_recruitments',
		'url'		=>	'#',
		'caption'	=> 	'Nábor',
		'child'		=> 	array(
			'recruitment_consultant_clients' => array(
				'name' 		=> 	'recruitment_consultant_clients',
				'url'		=>	'/recruitment_consultant_clients/',
				'caption'	=> 	'Nábor - počet vložených uchazečů',
				'child'		=> 	null
			)
		)
	);
?>