<?php

	$modul_name = 'Tisk docházky zaměstnanců';



	$modul_permission = array(

		'radio' => array(

			'index'	=>	'Zobrazení',
            'export_excel'=>	'Export excel',

		),

		'checkbox' => array(

		

		),

		'select' => array(

			'group_id'=> array(

				'caption' 	=> 'Typ napojení',

				'data'		=> array(

					'self_manager_id'	=>	'self_manager_id',

					'coordinator_id'	=>	'coordinator_id',

					'coordinator_double'	=>	'coordinator_double',

					'client_manager_id'	=>	'client_manager_id'

				)

			)

		)

	);

	$modul_menu = array(

	    'name' 		=> 	'modul_employees',

		'url'		=>	'#',

		'caption'	=> 	'Zaměstnanci',

		'child'		=> 	array(	

			'print_employees' =>array(

				'name' 		=> 	'print_employees',

				'url'		=>	'/print_employees/',

				'caption'	=> 	'Tisk docházky zaměstnanců',

				'child'		=> 	'null'

			)

		)

	);

	



?>