<?php

	$modul_name = 'Podklady pro fakturace za odpracovane hodiny';



	$modul_permission = array(

		'radio' => array(

			'index'	=>	'Zobrazení',

			'show'	=>	'Detail',

			'edit'	=>	'Editace'

			//'trash'=>	'Smazaní',

			//'status'=>	'Status'

		),

		'checkbox' => array(

		

		),
        'select' => array(
			'group_id'=> array(
				'caption' 	=> 'Typ napojení',
				'data'		=> array(
					'self_manager_id'	=>	'self_manager_id',
					'coordinator_id'	=>	'coordinator_id',
					'client_manager_id'	=>	'client_manager_id',
					'coordinator_double'	=>	'coordinator_double'
				)
			)
		)

	);

	$modul_menu = array(

		'name' 		=> 	'modul_reports',

		'url'		=>	'#',

		'caption'	=> 	'Reporty',

		'child'		=> 	array(

			'report_detail_to_invoice_items' =>array(

				'name' 		=> 	'report_detail_to_invoice_items',

				'url'		=>	'/report_detail_to_invoice_items/',

				'caption'	=> 	'Podklady pro fakturace za odpracovane hodiny',

				'child'		=> 	null

			)

		)

	);



?>