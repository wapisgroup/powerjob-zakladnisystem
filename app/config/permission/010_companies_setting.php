<?php
	$modul_name = 'Firmy';

	$modul_permission = array(
		'radio' => array(
			'index'			=>	'Zobrazení',
			'edit'			=>	'Editace',
			'trash'			=>	'Smazaní',
			'add'			=>	'Přidání',
			'kontakty'		=>	'Zobrazení kontaktů',
			'aktivity'		=>	'Zobrazení aktivit',
			'tasks'			=>	'Zobrazení úkolů',
			'attach'		=>	'Přílohy',
			'definice'		=>	'Definice profesí',
			'faktury'		=>	'Vystavené faktury a úhrady',
            'template'		=>	'Šablony objednavek',
			'order'			=>	'Objednavky',
			'form_template' =>	'Šablony formulářu',
            'client_working_template'=>	'Šablony docházek',
            'convert_users'	=>	'Převod uživatelů ve firmách',
            'export_excel'=>	'Export excel',
		),
		'checkbox' => array(
			'choose_self_manager'	=> 'Výběr Sales Managera',
			'choose_coordinator'	=> 'Výběr Koordinatora',
			'choose_client_manager'	=> 'Výběr Client Managera',
            'choose_asistent_obchodu'	=> 'Výběr Asistenta Obchodu',
            'choose_manazer_realizace'	=> 'Výběr Manažera Realizace',
			'access_to_autorizate_money_item'	=> 'Autorizace kalkulaci',
			'setting_payment'	=> 'Nastavení záloh/VVH',
            'update_order'	=> 'Znovu uložení objednávek',
            'delete_activity'	=> 'Mazání aktivit',
		),
		'select' => array(
			'group_id'=> array(
				'caption' 	=> 'Typ napojení',
				'data'		=> array(
                    'asistent_obchodu_id'	=>	'asistent_obchodu_id',
					'self_manager_id'	=>	'self_manager_id',
					'coordinator_id'	=>	'coordinator_id',
					'client_manager_id'	=>	'client_manager_id',
					'coordinator_double'	=>	'coordinator_double',
                    'coordinator_new'	=>	'coordinator_new',
                    'manazer_realizace_id'	=>	'manazer_realizace_id',
				)
			),
            'group_id_posibility'=> array(
				'caption' 	=> 'Typ napojení možnosti',
				'data'		=> array(
                    'asistent_obchodu_id'	=>	'asistent_obchodu_id',
					'self_manager_id'	=>	'self_manager_id',
					'coordinator_id'	=>	'coordinator_id',
					'client_manager_id'	=>	'client_manager_id',
					'coordinator_double'	=>	'coordinator_double',
                    'konzultant_dy_id'	=>	'konzultant_dy_id',
                    'manazer_realizace_id'	=>	'manazer_realizace_id',
				)
			)
		)
	);
	
	$modul_menu = array(
		'name' 		=> 	'companies',
		'url'		=>	'/companies/',
		'caption'	=> 	'Seznam zakázek',
		'child'		=> 	array(	
			'companies' =>array(
				'name' 		=> 	'companies',
				'url'		=>	'/companies/',
				'caption'	=> 	'Seznam zakázek - personální leasing',
				'child'		=> 	null
			)
		)
	);

?>