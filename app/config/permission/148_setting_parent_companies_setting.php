<?php
	$modul_name = 'Nastavení nadřazené společnosti';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní',
			'status'=>	'Status'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(
			'setting_parent_companies' =>array(
				'name' 		=> 	'setting_parent_companies',
				'url'		=>	'/setting_parent_companies/',
				'caption'	=> 	'Nastavení nadřazené společnosti',
				'child'		=> 	null
			)
		)
	);

?>