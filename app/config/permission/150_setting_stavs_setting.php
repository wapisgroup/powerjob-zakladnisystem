<?php
	$modul_name = 'Nastavení stavů firem';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní',
			'status'=>	'Status'
		),
		'checkbox' => array(
		
		)
	);
	
	$modul_menu = array(
		'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(
			'setting_stavs' =>array(
				'name' 		=> 	'setting_stavs',
				'url'		=>	'/setting_stavs/',
				'caption'	=> 	'Nastavení stavů firem',
				'child'		=> 	null
			)
		)
	);

?>