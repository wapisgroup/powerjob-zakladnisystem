<?php

	$modul_name = 'Interní zaměstnanci';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Editace',
			'rozvazat_prac_pomer_int'=>	'Odebrání ze zaměstnaní',
            'edit_datum_zamestnani'=>'Úprava datumu zaměstnání', 
            'edit_prac_pomer'=>'Úprava zaměstnání',
            'zmena_pp_int'=>	'Změna pracovního poměru',
            'export_excel'=>	'Export excel',
            'attach'=>	'Přílohy',
            'cinnost'=>	'Činnost',
		),
		'checkbox' => array(
           
		),
        'select' => array(
			'at_project_id'=> array(
				'caption' 	=> 'Napojení na projekt',
				'data_list'	=> 'project_list'
			)
         )  
	);

	$modul_menu = array(
	    'name' 		=> 	'modul_employees',
		'url'		=>	'#',
		'caption'	=> 	'Zaměstnanci',
		'child'		=> 	array(	
			'at_employees' =>array(
				'name' 		=> 	'at_employees',
				'url'		=>	'/at_employees/',
				'caption'	=> 	'Interní zaměstnanci',
				'child'		=> 	null
			)
		)
	);
?>