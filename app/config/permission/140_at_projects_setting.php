<?php
	$modul_name = 'Nastavení projektů, středisek, pozic';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní',
            'money_item'=> 'Forma zaměstnání',
            'office_manager'=> 'Office manager',
            'project_spravce_majetku'=> 'Správce majetku',
            'project_info_mails'=> 'Příjemci emailu o int. zaměstancích',
            'project_money_items'=> 'Nastavení forem zaměstnání',
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(
			'at_projects' =>array(
				'name' 		=> 	'at_projects',
				'url'		=>	'/at_projects/',
				'caption'	=> 	'Nastavení projektu, středisek, pozic',
				'child'		=> 	null
			)
		)
	);

?>