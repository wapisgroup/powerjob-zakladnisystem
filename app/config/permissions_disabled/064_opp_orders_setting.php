<?php

	$modul_name = 'Objednávky na OPP';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Editace',
			'add'	=>	'Přidání',
			'trash'=>	'Smazaní',
		),

		'checkbox' => array(
		)
	);

	$modul_menu = array(
	    'name' 		=> 	'modul_employees',
		'url'		=>	'#',
		'caption'	=> 	'Zaměstnanci',
		'child'		=> 	array(	
			'opp_orders' =>array(
				'name' 		=> 	'opp_orders',
				'url'		=>	'/opp_orders/',
				'caption'	=> 	'Objednávky na OPP',
				'child'		=> 	null
			)
		)
	);
?>