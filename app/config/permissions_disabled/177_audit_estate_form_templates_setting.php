<?php
	$modul_name = 'Nastavení formulářových šablon';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní',
            //'groups'=>	'Skupiny šablon',
            //'export_to_companies'=>'Export do firem'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
    	'name' 		=> 	'modul_ev_majektu',
		'url'		=>	'#',
		'caption'	=> 	'Evidence Majetku',
		'child'		=> 	array(
			'audit_estate_form_templates' =>array(
				'name' 		=> 	'audit_estate_form_templates',
				'url'		=>	'/audit_estate_form_templates/',
				'caption'	=> 	'Nastavení formulářových šablon',
				'child'		=> 	null
			)
		)
	);

?>