<?php
	$modul_name = 'Evidence OPP';
	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'import_csv'	=>	'Importovat',
            'add' => 'Přidat',
			'setting' => 'Nastaveni kurzu',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní',
		),
		'checkbox' => array()
	);


	$modul_menu = array(
		'name' 		=> 	'modul_reports',
		'url'		=>	'#',
		'caption'	=> 	'Reporty',
		'child'		=> 	array(
			'opp_items' =>array(
				'name' 		=> 	'opp_items',
				'url'		=>	'/opp_items/',
				'caption'	=> 	'Evidence OPP',
				'child'		=> 	'null'
			)
		)
	);
?>