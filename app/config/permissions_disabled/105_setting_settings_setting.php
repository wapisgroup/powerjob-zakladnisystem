<?php
	$modul_name = 'Nastavení';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Editace',
			//'trash'=>	'Smazaní'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'administration',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Administrace',
		'child'		=> 	array(
			'setting_settings' =>array(
				'name' 		=> 	'setting_settings',
				'url'		=>	'/setting_settings/',
				'caption'	=> 	'Nastavení',
				'child'		=> 	null
			)
		)
	);

?>