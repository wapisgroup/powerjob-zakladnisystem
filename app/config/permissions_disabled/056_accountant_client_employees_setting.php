<?php

//accountant_client_employees

	$modul_name = 'Zaměstnanci AT - pro účetní';



	$modul_permission = array(

		'radio' => array(

			'index'	=>	'Zobrazení',

			'edit'	=>	'Editace',

			'add'	=>	'Přidání',

			'trash'=>	'Smazaní',

			'attach'=>	'Přílohy',

			'add_recruiter'=>	'Přidání recruitera',

			'add_historie'=>	'Přidání historie',

			'add_hodnoceni'=>	'Přidání hodnocení',

			'rozvazat_prac_pomer'=>	'Odebrání ze zaměstnaní',

			'zmena_pp'=>	'Změna pracovního poměru',

            'ucetni_dokumentace'=>	'Učetní  dokumentace',
            'import_csv'=>'Import CSV'

		),

		'checkbox' => array(

		

		),

		'select' => array(

			'group_id'=> array(

				'caption' 	=> 'Typ napojení',

				'data'		=> array(

					'self_manager_id'	=>	'self_manager_id',

					'coordinator_id'	=>	'coordinator_id',

					'coordinator_double'	=>	'coordinator_double',

					'client_manager_id'	=>	'client_manager_id'

				)

			)

		)

	);

	$modul_menu = array(

	   
	    'name' 		=> 	'modul_employees',

		'url'		=>	'#',

		'caption'	=> 	'Zaměstnanci',

		'child'		=> 	array(

			'accountant_client_employees' => array(

				'name' 		=> 	'accountant_client_employees',

				'url'		=>	'/accountant_client_employees/',

				'caption'	=> 	'Zaměstnanci AT - pro účetní',

				'child'		=> 	null

			)

		)

	);

	

?>