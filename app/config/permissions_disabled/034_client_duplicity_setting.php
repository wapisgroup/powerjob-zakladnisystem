<?php

	$modul_name = 'Duplicitní klienti';



	$modul_permission = array(

		'radio' => array(

			'index'	=>	'Zobrazení',
	        'edit'	=>	'Editace',
            'show' => 'Detail',

			'trash'=>	'Smazaní'

		),

		'checkbox' => array(

		

		)

	);

	$modul_menu = array(
	'name' 		=> 	'report_requirements_for_recruitments',

		'url'		=>	'#',

		'caption'	=> 	'Nábor',


		'child'		=> 	array(

			'client_duplicity' => array(

				'name' 		=> 	'client_duplicity',

				'url'		=>	'/client_duplicity/',

				'caption'	=> 	'Duplicitní klienti',

				'child'		=> 	null

			)

		)

	);

	



?>