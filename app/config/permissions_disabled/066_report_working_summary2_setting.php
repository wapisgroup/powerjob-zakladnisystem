<?php

	$modul_name = 'Evidencia datumov vystup';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'export'=>	'Excel',
			
		),

		'checkbox' => array(
		)
	);

	$modul_menu = array(
	    'name' 		=> 	'modul_employees',
		'url'		=>	'#',
		'caption'	=> 	'Zaměstnanci',
		'child'		=> 	array(	
			'report_working_summary2' =>array(
				'name' 		=> 	'report_working_summary2',
				'url'		=>	'/report_working_summary2/',
				'caption'	=> 	'Evidencia datumov vystup',
				'child'		=> 	null
			)
		)
	);
?>