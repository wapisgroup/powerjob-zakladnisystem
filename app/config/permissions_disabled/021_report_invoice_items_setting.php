<?php

	$modul_name = 'Fakturace firem';



	$modul_permission = array(

		'radio' => array(

			'index'	=>	'Zobrazení',

			'add'	=>	'Přidání',

			'show'	=>	'Detail',

			'edit'	=>	'Editace',

			'trash'=>	'Smazaní',

			//'status'=>	'Status'

		),

		'checkbox' => array(

		

		)

	);

	$modul_menu = array(
		
		'name' 		=> 	'companies',
		'url'		=>	'#',
		'caption'	=> 	'Firmy',
		'child'		=> 	array(
			'report_invoice_items' =>array(
				'name' 		=> 	'report_invoice_items',
				'url'		=>	'/report_invoice_items/',
				'caption'	=> 	'Fakturace podniku',
				'child'		=> 	null
			)
		)
	);



?>