<?php
	$modul_name = 'Kalkulace - nastavení bonusů';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace klientu',
			'trash'=>	'Smazaní'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'administration',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Administrace',
		'child'		=> 	array(
			'money_item_bonuses' =>array(
				'name' 		=> 	'money_item_bonuses',
				'url'		=>	'/money_item_bonuses/',
				'caption'	=> 	'Kalkulace - nastavení bonusů',
				'child'		=> 	null
			)
		)
	);

?>