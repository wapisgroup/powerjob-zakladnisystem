<?php
	$modul_name = 'Vyřazený majetek';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
            'history'=>'Historie přiřazení majetku'
		),
		'checkbox' => array(
		
		)
	);
	
	$modul_menu = array(
		'name' 		=> 	'modul_ev_majektu',
		'url'		=>	'#',
		'caption'	=> 	'Evidence Majetku',
		'child'		=> 	array(
			'in_trash_audit_estates' =>array(
				'name' 		=> 	'in_trash_audit_estates',
				'url'		=>	'/in_trash_audit_estates/',
				'caption'	=> 	'Vyřazený majetek',
				'child'		=> 	null
			)
		)
	);

?>