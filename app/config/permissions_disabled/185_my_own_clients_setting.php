<?php
	$modul_name = 'Klienti - mé vlastnictví';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'show'	=>	'Detail',
            'fakturovano'=>'Fakturace'
		),
		'checkbox' => array(
		
		)
	);
	
	$modul_menu = array(
		'name' 		=> 	'modul_ev_majektu',
		'url'		=>	'#',
		'caption'	=> 	'Evidence Majetku',
		'child'		=> 	array(
			'monthly_statements' =>array(
				'name' 		=> 	'my_own_clients',
				'url'		=>	'/my_own_clients/',
				'caption'	=> 	'Klienti - mé vlastnictví',
				'child'		=> 	null
			)
		)
	);

?>