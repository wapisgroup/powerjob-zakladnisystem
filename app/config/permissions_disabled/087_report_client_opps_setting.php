<?php
	$modul_name = 'Report vydaných OPP - klienti';
	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
		),
		'checkbox' => array()
	);


	$modul_menu = array(
		'name' 		=> 	'modul_reports',
		'url'		=>	'#',
		'caption'	=> 	'Reporty',
		'child'		=> 	array(
			'report_client_opps' =>array(
				'name' 		=> 	'report_client_opps',
				'url'		=>	'/report_client_opps/',
				'caption'	=> 	'Report vydaných OPP - klienti',
				'child'		=> 	'null'
			)
		)
	);
?>