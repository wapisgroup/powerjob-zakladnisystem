<?php
	$modul_name = 'Estor building – stavebny zamestnanci';

	$modul_permission = array(

		'radio' => array(
			'index'	        =>	'Zobrazení',
			'edit'	        =>	'Editace',
			'add'	        =>	'Přidání',
            'trash'         =>	'Smazaní',
//			'add_express'	=>	'Přidání Express',
//			'attach'        =>	'Přílohy',
//			'add_recruiter' =>	'Přidání recruitera',
//			'add_historie'  =>	'Přidání historie',
//			'add_hodnoceni' =>	'Přidání hodnocení',
//			'rozvazat_prac_pomer'=>	'Odebrání ze zaměstnaní',
//			'zmena_pp'      =>	'Změna pracovního poměru',
//			'stats'         =>	'Statistiky spojení',
//			'export_excel'  =>	'Export excel',
//          'domwin_add_activity'=>	'Přidat aktivitu'
		),
		'checkbox' => array(),
		'select' => array(
//			'group_id'=> array(
//				'caption' 	=> 'Typ napojení',
//				'data'		=> array(
//					'self_manager_id'	=>	'self_manager_id',
//					'coordinator_id'	=>	'coordinator_id',
//					'coordinator_double'	=>	'coordinator_double',
//					'client_manager_id'	=>	'client_manager_id'
//				)
//			)
		)
	);

	$modul_menu = array(
	'name' 		=> 	'administration',
		'url'		=>	'#',
		'caption'	=> 	'Administrace',
		'child'		=> 	array(
			'client_estors' => array(
				'name' 		=> 	'client_estors',
				'url'		=>	'/client_estors/',
				'caption'	=> 	'Estor building – stavebny zamestnanci',
				'child'		=> 	null
			)
		)
	);
?>