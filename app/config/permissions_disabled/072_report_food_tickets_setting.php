<?php

	$modul_name = 'Report stravných lístků';



	$modul_permission = array(

		'radio' => array(

			'index'		=>	'Zobrazení',

			'show'		=>	'Detail',

            'print_detail'	=>	'Tisk'

		),

		'checkbox' => array(

		

		),

		

		'select' => array(
			'group_id'=> array(
				'caption' 	=> 'Typ napojení',
				'data'		=> array(
					'self_manager_id'	=>	'self_manager_id',
					'coordinator_id'	=>	'coordinator_id',
					'client_manager_id'	=>	'client_manager_id',
					'coordinator_double'	=>	'coordinator_double'
				)
			)
		)

	

	);

	$modul_menu = array(

		
		'name' 		=> 	'modul_reports',

		'url'		=>	'#',

		'caption'	=> 	'Reporty',

		'child'		=> 	array(	

			'report_food_tickets' =>array(

				'name' 		=> 	'report_food_tickets',

				'url'		=>	'/report_food_tickets/',

				'caption'	=> 	'Report stravných lístků',

				'child'		=> 	null

			)

		)

	);

	



?>