<?php
	$modul_name = 'Historie - itms';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Editace',
			'add'	=>	'Přidání',
			'trash'=>	'Smazaní',
            'export_excel'=>	'Export excel',
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'knowledges',
		'url'		=>	'#',
		'caption'	=> 	'Pomocník',
		'child'		=> 	array(
			'history_items' =>array(
				'name' 		=> 	'history_items',
				'url'		=>	'/history_items/',
				'caption'	=> 	'Historie - itms',
				'child'		=> 	null
			)
		)
	);

?>