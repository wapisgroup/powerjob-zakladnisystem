<?php
	$modul_name = 'Klienti - mé objednávky';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'show'	=>	'Detail',
            'fakturovano'=>'Fakturace'
		),
		'checkbox' => array(
		
		)
	);
	
	$modul_menu = array(
		'name' 		=> 	'modul_ev_majektu',
		'url'		=>	'#',
		'caption'	=> 	'Evidence Majetku',
		'child'		=> 	array(
			'monthly_statements' =>array(
				'name' 		=> 	'my_clients',
				'url'		=>	'/my_clients/',
				'caption'	=> 	'Klienti - mé objednávky',
				'child'		=> 	null
			)
		)
	);

?>