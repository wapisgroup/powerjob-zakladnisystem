<?php
	$modul_name = 'Majetek podle firem';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'show'=>	'Detail'
		),
		'checkbox' => array(
		
		)
	);
	
	$modul_menu = array(
		'name' 		=> 	'modul_ev_majektu',
		'url'		=>	'#',
		'caption'	=> 	'Evidence Majetku',
		'child'		=> 	array(
			'company_audit_estates' =>array(
				'name' 		=> 	'company_audit_estates',
				'url'		=>	'/company_audit_estates/',
				'caption'	=> 	'Majetek podle firem',
				'child'		=> 	null
			)
		)
	);

?>