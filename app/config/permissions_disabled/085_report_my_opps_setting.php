<?php
	$modul_name = 'Report - moje OPP';
	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení'
		),
		'checkbox' => array(
		)
	);

	$modul_menu = array(
		'name' 		=> 	'modul_reports',
		'url'		=>	'#',
		'caption'	=> 	'Reporty',
		'child'		=> 	array(
  	         'report_my_opps' =>array(
				'name' 		=> 	'report_my_opps',
				'url'		=>	'/report_my_opps/',
				'caption'	=> 	'Report - moje OPP',
				'child'		=> 	null
			)
		)
	);
?>