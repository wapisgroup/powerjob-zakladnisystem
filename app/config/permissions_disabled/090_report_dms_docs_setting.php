<?php
	$modul_name = 'DMS - správa dokumentů';
	$modul_permission = array(
		'radio' => array(
			'index'			=>	'Zobrazení',
			'edit'			=>	'Editace',
			'trash'			=>	'Smazaní',
			'add'			=>	'Přidání',
            'groups'		=>	'Čísleniky',
            'attach'		=>	'Přílohy',
		),
		'checkbox' => array()
	);

	$modul_menu = array(		
		'name' 		=> 	'modul_reports',
		'url'		=>	'#',
		'caption'	=> 	'Reporty',
		'child'		=> 	array(
			'report_dms_docs' =>array(
				'name' 		=> 	'report_dms_docs',
				'url'		=>	'/report_dms_docs/',
				'caption'	=> 	'DMS - správa dokumentů',
				'child'		=> 	null
			)
		)
	);



?>