<?php
	$modul_name = 'Kalkulace - nastavení srážek';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace klientu',
			'trash'=>	'Smazaní'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'administration',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Administrace',
		'child'		=> 	array(
			'money_item_drawbacks' =>array(
				'name' 		=> 	'money_item_drawbacks',
				'url'		=>	'/money_item_drawbacks/',
				'caption'	=> 	'Kalkulace - nastavení srážek',
				'child'		=> 	null
			)
		)
	);

?>