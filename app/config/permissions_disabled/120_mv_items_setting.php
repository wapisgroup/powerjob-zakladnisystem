<?php
$modul_name = 'Evidencia dolnkoveho tovaru MV';


$modul_permission = array(
    'radio' => array(
        'index' => 'Zobrazení',
        'add' => 'Přidat',
        'edit' => 'Editace',
        'trash' => 'Smazaní'
    ),
    'checkbox' => array(

    ),

);

$modul_menu = array(
    'name' => 'modul_reports',
    'url' => '#',
    'caption' => 'Reporty',
    'child' => array(
        'mv_items' => array(
            'name' => 'mv_items',
            'url' => '/mv_items/',
            'caption' => 'Evidencia dolnkoveho tovaru MV',
            'child' => null
        )
    )
);

?>