<?php
	$modul_name = 'Kampaně - SM';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Editace'
			//'trash'=>	'Smazaní',
			//'status'=>	'Status'
		),
		'checkbox' => array(
		
		)
	);
	
	$modul_menu = array(
	   		'name' 		=> 	'companies',
		'url'		=>	'/companies/',
		'caption'	=> 	'Firmy',
		'child'		=> 	array(
			'campaign_items_sm' =>array(
				'name' 		=> 	'campaign_items_sm',
				'url'		=>	'/company_tasks/?filtration_setting_task_type_id=10',
				'caption'	=> 	'Kampaně - SM',
				'child'		=> 	null
			)
		)
	);

?>