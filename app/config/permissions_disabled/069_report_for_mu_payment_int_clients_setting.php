<?php
	$modul_name = 'Výplaty pro mzdovou účetní - Int. zam.';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Vyplatit',
			'multi_edit'	=>	'Hrmonadné vyplácení',
			'stop_status' =>	'Pozastavení výplaty',
			'show'	=>	'Detail docházky',
			'export_excel'=>	'Export excel',
		),
		'checkbox' => array(
		
		)
	);
	$modul_menu = array(
	  'name' 		=> 	'modul_employees',
		'url'		=>	'#',
		'caption'	=> 	'Zaměstnanci',
		'child'		=> 	array(	
			'report_for_mu_payment_int_clients' =>array(
				'name' 		=> 	'report_for_mu_payment_int_clients',
				'url'		=>	'/report_for_mu_payment_int_clients/',
				'caption'	=> 	'Výplaty pro mzdovou účetní  - Int. zam.',
				'child'		=> 	'null'
			)
		)
	);
	

?>