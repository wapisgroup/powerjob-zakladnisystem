<?php
	$modul_name = 'Nastavení formulářových šablon';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní',
            'groups'=>	'Skupiny šablon',
            'export_to_companies'=>'Export do firem'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'administration',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Administrace',
		'child'		=> 	array(
			'form_templates' =>array(
				'name' 		=> 	'form_templates',
				'url'		=>	'/form_templates/',
				'caption'	=> 	'Nastavení formulářových šablon',
				'child'		=> 	null
			)
		)
	);

?>