<?php
	$modul_name = 'Nastavení A&T firem';

	$modul_permission = array(
		'radio' => array(
			'index'			=>	'Zobrazení',
			'edit'			=>	'Editace',
			'trash'			=>	'Smazaní',
			'add'			=>	'Přidání',
            'sharing_company'=> 'Sdílení pro projekt',
		),
		'checkbox' => array(),
		'select' => array()
	);
	
	$modul_menu = array(
		'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(	
			'at_companies' =>array(
				'name' 		=> 	'at_companies',
				'url'		=>	'/at_companies/',
				'caption'	=> 	'Nastavení A&T firem',
				'child'		=> 	null
			)
		)
	);

?>