<?php
	$modul_name = 'Reporty - kalkulace firem CM/KOO';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'show'	=>	'Detail',
			'edit'	=>	'Editace'
			//'trash'=>	'Smazaní',
			//'status'=>	'Status'
		),
		'checkbox' => array(
		
		),
		'select' => array(
			'group_id'=> array(
				'caption' 	=> 'Typ napojení',
				'data'		=> array(
					'self_manager_id'	=>	'self_manager_id',
					'coordinator_id'	=>	'coordinator_id',
					'coordinator_double'	=>	'coordinator_double',
					'client_manager_id'	=>	'client_manager_id'
				)
			)
		)
	);
	$modul_menu = array(
		'name' 		=> 	'companies',
		'url'		=>	'/companies/',
		'caption'	=> 	'Firmy',
		'child'		=> 	array(
			'report_coo_cm_money_items' =>array(
				'name' 		=> 	'report_coo_cm_money_items',
				'url'		=>	'/report_coo_cm_money_items/',
				'caption'	=> 	'Kalkulace podniku CM/KOO',
				'child'		=> 	null
			)
		)
	);

?>