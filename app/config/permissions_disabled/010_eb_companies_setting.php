<?php
	$modul_name = 'Seznam zakázek - estor building';

	$modul_permission = array(
		'radio' => array(
			'index'			=>	'Zobrazení',
			'edit'			=>	'Editace',
			'trash'			=>	'Smazaní',
			'add'			=>	'Přidání',
            'rozpocet'	      =>	'Nákladový rozpočet',
			'slepy_rozpocet'  =>	'Hlášení stavbyvedoucího',
            'vynosovy_rozpocet'=>   'Výnosový rozpočet',
			'aktivity'		=>	'Zobrazení aktivit',
		),
		'checkbox' => array(
            'tab_1'	=> 'Záložka - základní informace',
			'tab_2'	=> 'Záložka - příprava a rozpočet',
            'tab_3'	=> 'Záložka - popis zakázky',
            'tab_4'	=> 'Záložka - realizace',
            'tab_5'	=> 'Záložka - vyhodnocení',
            'tab_6'	=> 'Záložka - archiv',
            'tab_7'	=> 'Záložka - zaměstnanci',
            'go_to_realization'	=> 'Posunout zakázku k realizaci',
		),
		'select' => array(
		)
	);
	
	$modul_menu = array(
		'name' 		=> 	'companies',
		'url'		=>	'/companies/',
		'caption'	=> 	'Seznam zakázek',
		'child'		=> 	array(	
			'eb_companies' =>array(
				'name' 		=> 	'eb_companies',
				'url'		=>	'/eb_companies/',
				'caption'	=> 	'Seznam zakázek - estor building',
				'child'		=> 	null
			)
		)
	);

?>