<?php
	$modul_name = 'Nastavení dokumentů';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(
			'client_documents' =>array(
				'name' 		=> 	'client_documents',
				'url'		=>	'/client_documents/',
				'caption'	=> 	'Nastavení dokumentů',
				'child'		=> 	null
			)
		)
	);

?>