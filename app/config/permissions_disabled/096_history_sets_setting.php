<?php
	$modul_name = 'Historie - definice';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Editace',
			'add'	=>	'Přidání',
			'trash'=>	'Smazaní',
            'status'=>	'Status'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'knowledges',
		'url'		=>	'#',
		'caption'	=> 	'Pomocník',
		'child'		=> 	array(
			'history_sets' =>array(
				'name' 		=> 	'history_sets',
				'url'		=>	'/history_sets/',
				'caption'	=> 	'Historie - definice',
				'child'		=> 	null
			)
		)
	);

?>