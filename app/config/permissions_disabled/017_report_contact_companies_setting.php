<?php
	$modul_name = 'Kontakty firem';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			//'show'	=>	'Detail',
			'add'	=>	'Přidání',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní',
			//'status'=>	'Status'
            
			'export_excel'=>	'Export excel',
		),
		'checkbox' => array(
		
		)
	);
	$modul_menu = array(
		'name' 		=> 	'companies',
		'url'		=>	'/companies/',
		'caption'	=> 	'Firmy',
		'child'		=> 	array(
			'report_contact_companies' =>array(
				'name' 		=> 	'report_contact_companies',
				'url'		=>	'/report_contact_companies/',
				'caption'	=> 	'Kontakty podniku',
				'child'		=> 	null
			)
		)
	);

?>