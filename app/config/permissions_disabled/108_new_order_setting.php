<?php
	$modul_name = 'NEW - Obsazení objednávek';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			//'add'	=>	'Přidat',
			'edit'	=>	'Editace klientu',
			//'trash'=>	'Smazaní'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'administration',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Administrace',
		'child'		=> 	array(
			'new_order' =>array(
				'name' 		=> 	'new_order',
				'url'		=>	'/new_order/',
				'caption'	=> 	'NEW - Obsazení objednávek',
				'child'		=> 	null
			)
		)
	);

?>