<?php

	$modul_name = 'Reporty - Žádosti na pracovní pomůcky';



	$modul_permission = array(

		'radio' => array(

			'index'	=>	'Zobrazení',

			'edit'	=>	'Editace',

			'add'	=>	'Přidání',

			'trash'=>	'Smazaní',


            'pick_up'=> 'Převzetí pomůcek'

			//'status'=>	'Status'

		),

		'checkbox' => array(

		

		),

		'select' => array(

			'group_id'=> array(

				'caption' 	=> 'Typ napojení',

				'data'		=> array(

					'self_manager_id'	=>	'self_manager_id',

					'coordinator_id'	=>	'coordinator_id',

					'coordinator_double'	=>	'coordinator_double',

					'client_manager_id'	=>	'client_manager_id'

				)

			)

		)

	);

	$modul_menu = array(

	
	    'name' 		=> 	'modul_employees',

		'url'		=>	'#',

		'caption'	=> 	'Zaměstnanci',

		'child'		=> 	array(	

			'report_down_payments' =>array(

				'name' 		=> 	'report_request_for_working_items',

				'url'		=>	'/report_request_for_working_items/',

				'caption'	=> 	'Žádosti na pracovní pomůcky',

				'child'		=> 	null

			)

		)

	);



?>