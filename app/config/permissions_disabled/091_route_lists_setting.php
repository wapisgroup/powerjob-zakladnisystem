<?php
	$modul_name = 'Seznam tras';
	$modul_permission = array(
		'radio' => array(
			'index'			=>	'Zobrazení',
			'edit'			=>	'Editace',
			'trash'	=>	'Smazaní',
			'add'			=>	'Přidání',
		),
		'checkbox' => array(
        )
	);

	$modul_menu = array(		
		'name' 		=> 	'modul_reports',
		'url'		=>	'#',
		'caption'	=> 	'Reporty',
		'child'		=> 	array(
			'route_lists' =>array(
				'name' 		=> 	'route_lists',
				'url'		=>	'/route_lists/',
				'caption'	=> 	'Seznam tras',
				'child'		=> 	null
			)
		)
	);



?>