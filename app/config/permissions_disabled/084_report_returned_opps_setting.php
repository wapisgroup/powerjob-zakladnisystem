<?php
	$modul_name = 'Report - vrácených OPP';
	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení'
		),
		'checkbox' => array(
		)
	);

	$modul_menu = array(
		'name' 		=> 	'modul_reports',
		'url'		=>	'#',
		'caption'	=> 	'Reporty',
		'child'		=> 	array(
  	         'report_returned_opps' =>array(
				'name' 		=> 	'report_returned_opps',
				'url'		=>	'/report_returned_opps/',
				'caption'	=> 	'Report - vrácených OPP',
				'child'		=> 	null
			)
		)
	);
?>