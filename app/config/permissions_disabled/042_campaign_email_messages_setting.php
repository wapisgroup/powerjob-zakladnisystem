<?php

	$modul_name = 'Email - Zprávy';
	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Editace',
            	'trash'=>	'Smazaní'
		),

		'checkbox' => array(
		),

	);


	$modul_menu = array(
    		'name' 		=> 	'report_requirements_for_recruitments',

		'url'		=>	'#',

		'caption'	=> 	'Nábor',


		'child'		=> 	array(
			'campaign_email_messages' =>array(
				'name' 		=> 	'campaign_email_messages',
				'url'		=>	'/campaign_email_messages/',
				'caption'	=> 	'Email - Zprávy',
				'child'		=> 	null
			)
    	)
	);
?>