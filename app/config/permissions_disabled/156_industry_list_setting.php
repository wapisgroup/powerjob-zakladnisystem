<?php
	$modul_name = 'Průmysl číselník';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(
			'industry_list' =>array(
				'name' 		=> 	'industry_list',
				'url'		=>	'/industry_list/',
				'caption'	=> 	'Průmysl číselník',
				'child'		=> 	null
			)
		)
	);

?>