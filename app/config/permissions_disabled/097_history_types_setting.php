<?php
	$modul_name = 'Historie - typy';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Editace',
			'add'	=>	'Přidání',
			'trash'=>	'Smazaní'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'knowledges',
		'url'		=>	'#',
		'caption'	=> 	'Pomocník',
		'child'		=> 	array(
			'history_types' =>array(
				'name' 		=> 	'history_types',
				'url'		=>	'/history_types/',
				'caption'	=> 	'Historie - typy',
				'child'		=> 	null
			)
		)
	);

?>