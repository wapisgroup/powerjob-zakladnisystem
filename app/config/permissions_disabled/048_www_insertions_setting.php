<?php

	$modul_name = 'WWW Inzerce';
    $modul_permission = array(

		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Editace',
			'add'	=>	'Přidání',
			'trash'=>	'Smazaní',
            'status'=>	'Status'
		),
		'checkbox' => array(
		),
		'select' => array(
		)

	);

	$modul_menu = array(
		'name' 		=> 	'report_requirements_for_recruitments',
		'url'		=>	'#',
		'caption'	=> 	'Nábor',
		'child'		=> 	array(
			'www_insertions' => array(
				'name' 		=> 	'www_insertions',
				'url'		=>	'/www_insertions/',
				'caption'	=> 	'WWW Inzerce',
				'child'		=> 	null
			)
		)
	);
?>