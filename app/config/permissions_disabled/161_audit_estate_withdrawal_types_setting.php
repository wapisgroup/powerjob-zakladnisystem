<?php
	$modul_name = 'Kategorie vyřazení majetku';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní',
            //'groups'=>	'Skupiny šablon',
            //'export_to_companies'=>'Export do firem'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
    	'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(
			'audit_estate_withdrawal_types' =>array(
				'name' 		=> 	'audit_estate_withdrawal_types',
				'url'		=>	'/audit_estate_withdrawal_types/',
				'caption'	=> 	'Kategorie vyřazení majetku',
				'child'		=> 	null
			)
		)
	);

?>