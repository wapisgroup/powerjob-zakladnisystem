<?php
	$modul_name = 'Nástěnka';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			//'add'	=>	'Přidat',
			//'edit'	=>	'Editace',
			//'trash'=>	'Smazaní',
			//'status'=>	'Status'
		),
		'checkbox' => array(
		
		)
	);
	
	$modul_menu = array(
		'name' 		=> 	'knowledges',
		'url'		=>	'#',
		'caption'	=> 	'Pomocník',
		'child'		=> 	array(
			'noticeboards' =>array(
				'name' 		=> 	'noticeboards',
				'url'		=>	'/noticeboards/',
				'caption'	=> 	'Nástěnka',
				'child'		=> 	null
			)
		)
	);

?>