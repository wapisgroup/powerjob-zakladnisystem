<?php
	$modul_name = 'Často kladné otázky';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'group'	=>	'Skupiny',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní',
            'access'=>	'Přístupy',
            'attach'=> 'Přílohy'
		),
		'checkbox' => array(
		
		)
	);
	$modul_menu = array(
		'name' 		=> 	'knowledges',
		'url'		=>	'#',
		'caption'	=> 	'Pomocník',
		'child'		=> 	array(
			'faqs' =>array(
				'name' 		=> 	'faqs',
				'url'		=>	'/faqs/',
				'caption'	=> 	'Často kladné otázky',
				'child'		=> 	'null'
			)
		)
	);
	

?>