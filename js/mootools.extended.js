translation = {
	lang_smazat	: 'Smazat'
};

function serialize( mixed_value ) {
    // http://kevin.vanzonneveld.net
    // +   original by: Arpad Ray (mailto:arpad@php.net)
    // +   improved by: Dino
    // +   bugfixed by: Andrej Pavlovic
    // +   bugfixed by: Garagoth
    // %          note: We feel the main purpose of this function should be to ease the transport of data between php & js
    // %          note: Aiming for PHP-compatibility, we have to translate objects to arrays
    // *     example 1: serialize(['Kevin', 'van', 'Zonneveld']);
    // *     returns 1: 'a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}'
    // *     example 2: serialize({firstName: 'Kevin', midName: 'van', surName: 'Zonneveld'});
    // *     returns 2: 'a:3:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";s:7:"surName";s:9:"Zonneveld";}'
 
    var _getType = function( inp ) {
        var type = typeof inp, match;
        var key;
        if (type == 'object' && !inp) {
            return 'null';
        }
        if (type == "object") {
            if (!inp.constructor) {
                return 'object';
            }
            var cons = inp.constructor.toString();
            if (match = cons.match(/(\w+)\(/)) {
                cons = match[1].toLowerCase();
            }
            var types = ["boolean", "number", "string", "array"];
            for (key in types) {
                if (cons == types[key]) {
                    type = types[key];
                    break;
                }
            }
        }
        return type;
    };
    var type = _getType(mixed_value);
    var val, ktype = '';
    
    switch (type) {
        case "function": 
            val = ""; 
            break;
        case "undefined":
            val = "N";
            break;
        case "boolean":
            val = "b:" + (mixed_value ? "1" : "0");
            break;
        case "number":
            val = (Math.round(mixed_value) == mixed_value ? "i" : "d") + ":" + mixed_value;
            break;
        case "string":
            val = "s:" + mixed_value.length + ":\"" + mixed_value + "\"";
            break;
        case "array":
        case "object":
            val = "a";
            /*
            if (type == "object") {
                var objname = mixed_value.constructor.toString().match(/(\w+)\(\)/);
                if (objname == undefined) {
                    return;
                }
                objname[1] = serialize(objname[1]);
                val = "O" + objname[1].substring(1, objname[1].length - 1);
            }
            */
            var count = 0;
            var vals = "";
            var okey;
            var key;
            for (key in mixed_value) {
                ktype = _getType(mixed_value[key]);
                if (ktype == "function") { 
                    continue; 
                }
                
                okey = (key.match(/^[0-9]+$/) ? parseInt(key) : key);
                vals += serialize(okey) +
                        serialize(mixed_value[key]);
                count++;
            }
            val += ":" + count + ":{" + vals + "}";
            break;
    }
    if (type != "object" && type != "array") val += ";";
    return val;
}

function doporuceni_na_prezentaci(href,title){
		domwin.newWindow({
			id:'doporuceni_win',
			title: title,
			url:href,
			languages:false,
			sizes:[500,200]
		});
		return false;		

}

function min_delka(id,text,alert_text,delka){
		if ($(id).value.length < delka || $(id).value == text){
			alert(alert_text);
			return false;		
		} else {
			return true;
		}
}

Element.implement({
	inputLimit: function(){
		if (this.hasClass('float')){
			this.addEvent('keypress',function(e){
				var event = new Event(e); 
				var allow_key = ['1','2','3','4','5','6','7','8','9','0',',','backspace','left','right','tab','f5'];	
				if (allow_key.indexOf(event.key) != -1) 
					return true; 
				else 
					return false;			
			});
		} else if (this.hasClass('integer')){
			this.addEvent('keypress',function(e){
				var event = new Event(e); 
				var allow_key = ['1','2','3','4','5','6','7','8','9','0','backspace','left','right','tab','f5'];	
				if (allow_key.indexOf(event.key) != -1) 
					return true; 
				else 
					return false;			
			});
		}
	}
});
function addSearchEngine($cesta,$title,$desc) {
  if ((typeof window.sidebar == "object") &&
      (typeof window.sidebar.addSearchEngine == "function")) {

    window.sidebar.addSearchEngine(
      $cesta+"/search-ff.src",
      $cesta+"/css/fastest/icons/favicon.ico",
      $title,
      $desc);

  } else
    alert("Váš prohlížeč nepodporuje tuto funkci. Zkuste Mozillu.");

}



function tisk(){
window.print();
return false;
}

function popup(url) {
var win = window.open(url,'','width=800,height=600,left=0,top=0,scrollbars=yes,toolbar=no,location=no,directories=no,resizable=yes');
if (win){ return false;}


}

/* START ZOBRAZENI DIV */
function show_div(id){
el=document.getElementById(id).style;
el.display=(el.display == 'block')?'none':'block';
}

/* END  ZOBRAZENI DIV */

function ismaxlength(obj){
var mlength=obj.getAttribute? parseInt(obj.getAttribute("maxlength")) : ""
if (obj.getAttribute && obj.value.length>mlength)
obj.value=obj.value.substring(0,mlength)
} 




/* pro meni item */
Element.implement({
	depth: function(contain){		
		depth = 0;
		if(this != contain){
			if(this.getTag() != contain.getTag())
				depth++;
			depth += this.getParent().depth(contain);
		}	
 
		return depth;
	},
	
	getUlItems:function(){
		if (this.nodeName != 'UL') alert('Tato metoda lze pouze na UL element');
		var items = [];
		var list = this.getElements('li');
		list.each(function(item){
			if (this == item.parentNode){
				items[items.length] = item;
			}
		},this);
		return items;
	},
	
	getIndex:function(){
		var items = [], list;
		if (this.nodeName != 'LI') alert('Tato metoda lze pouze na LI element');
		list = this.parentNode.getElements('li');
		list.each(function(item,key){
			if (this.parentNode == item.parentNode)
				items[items.length] = item.id
		},this);
		
		if (this.id != undefined)
			return items.indexOf(this.id);
		else 
			return items.length;
	},
	
	getLiText: function(){
		if (this.nodeName != 'LI') alert('Tato metoda lze pouze na LI element');
		var p = this.firstChild;
		if (p.nodeType == 3)
			return p
		else 
			return false;
	},
	
	getParentElements: function(node,endNode){
		var pnode = this, output = [];
		while(pnode.parentNode && pnode.parentNode != endNode)
			pnode = pnode.parentNode;
			if (pnode.nodeName == node)
				output[output.length] = pnode;
		return output;
	},
	
	center: function(){
		/* velikost alert okna */
		var OH = this.getSize().y;
		var OW = this.getSize().x;

			
		/* velikosti window */
		var WH = window.getHeight();
		var WW = window.getWidth();
			
		var top = (WH/2 - OH/2)+window.getScrollTop();
			
		this.setStyles({
			'top'	: 	(top>0)?top:0,
			'left'	:	((WW/2 - OW/2)+window.getScrollLeft())
		});
			
		return this;
	}

});


/* ========= START POUZE CISLA =========== */
Element.implement({
	onlyNumeric: function(){
		this.addEvent('keypress',function(e){
			var event = new Event(e);
			var key = event.key;
			var code = event.code;

			if ((code==46) ||(code==null) || (code==0) || (code==8) || (code==9) || (code==13) || (code==27) || (code==37)|| (code==39)|| (code==36)|| (code==35))
				return true;
			else if (("0123456789").indexOf(key) != -1)
				return true;
			else
				return false;
		});
		return this;
	}
});
 /* ========= END POUZE CISLA =========== */

 /* ========= START CLANKY SPRAVA DOPLNKU / PRILOH =========== */
  Element.implement({
 initAddons: function(options){ 
  this.addEvent('click',(function(e,options){
   
   var input = $(options.input);
   var file = $(options.file);
   var ul = $(options.ul);
   var name = options.hidden;
   new Event (e)  .stop();
   if (input.value =='') { alert('Zadejte název přílohy'); return;}

  
   if (file){
    var newLi = new Element('li').inject(ul);
    var sll = new Element('div',{'class':'sll'}).setHTML('<strong>Název:</strong> ' + input.value).inject(newLi);
    var slr = new Element('div',{'class':'slr'}).setHTML('<strong>Soubor:</strong> '+file.value).inject(newLi);
    new Element('input',{'name':name,'type':'hidden','class':'hidden','value':input.value + ';;' + file.value}).inject(slr);
    new Element('img',{src:'/css/icons/table/delete.gif','alt':translation.lang_smazat,'class':'del_priloha icon'})
    .addEvent('click',function(){
     newLi.remove();
      })
    .inject(slr);
   } else{
    var newLi = new Element('li').setHTML('<strong>URL:</strong> '+input.value).inject(ul);    
    new Element('input',{'name':name,'type':'hidden','class':'hidden','value':input.value}).inject(newLi);
   }
     input.value='';
  file.value='';
  }).bindWithEvent(this,options));

 }
 });
 /* ========= END CLANKY SPRAVA DOPLNKU / PRILOH =========== */

 
 
/* ========= START PRESUN MEZI 2-MI LISTY  =========== */
Element.implement({
	list_find_selected: function(){
		var output = false
		var options = this.getElements('option');
		options.each(function(option){
			if (option.getProperty('selected'))
				output =  option;
		},this);

		return output;
	},
	list_move_one:function(start,end){
		var list1 = $(start);
		var list2 = $(end);
		var selOption = list1.list_find_selected();
		if (!selOption) {alert('Není vybrána položka'); return;}
		list2.options[list2.length] = new Option(selOption.text, selOption.value);
		selOption.remove();
	},
	
	list_move_all: function(start,end){
		var list1 = $(start);
		var list2 = $(end);
		var options = list1.getElements('option');
		options.each(function(option){
			list2.adopt(option);
		});
  	},
	list_select_all:function(){
		this.getElements('option').setProperty('selected','selected');
	},
	findIndexAndRemove: function(list,value){
		$(list).adopt(this.getElement('option[value=' + value + ']'));
	}
});
/* ========= END PRESUN MEZI 2-MI LISTY  =========== */
 
 
 
/* getHighestZIndex */
	function getHighestIndex(){				
		var allElems = document.getElementsByTagName?document.getElementsByTagName("*"): document.all; 
		var maxZIndex = 0;
		for(var i=0;i<allElems.length;i++) {
			var elem = allElems[i];
			var cStyle = null;
			if (elem.currentStyle) {cStyle = elem.currentStyle;
			} else if (document.defaultView && document.defaultView.getComputedStyle){
				cStyle = document.defaultView.getComputedStyle(elem,"");
			}
			var sNum;
			if (cStyle) {
				sNum = Number(cStyle.zIndex);
			} else {
				sNum = Number(elem.style.zIndex);
			}
			if (!isNaN(sNum)) {
				maxZIndex = Math.max(maxZIndex,sNum);
			}
		}
		return maxZIndex;
	} 
// number format like php
function number_format( number, decimals, dec_point, thousands_sep ) {
    // http://kevin.vanzonneveld.net
    // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +     bugfix by: Michael White (http://crestidg.com)
    // +     bugfix by: Benjamin Lupton
    // +     bugfix by: Allan Jensen (http://www.winternet.no)
    // +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)    
    // *     example 1: number_format(1234.5678, 2, '.', '');
    // *     returns 1: 1234.57     
 
    var n = number, c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;
    var d = dec_point == undefined ? "," : dec_point;
    var t = thousands_sep == undefined ? "." : thousands_sep, s = n < 0 ? "-" : "";
    var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}