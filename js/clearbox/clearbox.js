var ImageObject = new Class({
	name: 'ImageObject',
	Implements: [Options,Events],
	options: {
		onComplete: $empty
	},
	initialize: function(src,options){
		this.pass = this.pass || this;
		this.setOptions(options);
		this.onComplete = this.options.onComplete;
		this.temp_image = new Image();
		this.temp_image.src = src;
		this.load_fnc = this.loading.periodical(10,this); 
	},
	loading: function(){
		if(this.temp_image.complete){
			$clear(this.load_fnc);
			output = {
				width: this.temp_image.width,
				height: this.temp_image.height,
				src: this.temp_image.src
			}
			this.complete(output);
		} 
	},
	complete: function(output){
		return this.fireEvent('onComplete', output);
	}
});

var slideShow = new Class({
	Implements: [Events,Options],
	options: {
		HideColor:	'#000',
		ZIndex:	100,
		HideOpacity:	0.75,
		OpacitySpeed:	25,
		BasicWinW:	120,
		BasicWinH:	110,
		AnimSpeed:	5,
		ImgBorder:	1,
		ImgBorderColor:	'#ccc',
		ShowImgURL:	true,
		ImgNum:		true,
		SlShowTime:	3,
		LoadingText:	'Nahrávám...',
		PicDir:		'/js/clearbox/pic',
		Preload:	true,
		TextNav:	true,
		NavTextPrv:	true,
		NavTextNxt:	true,
		PictureStart:	'start.png',
		PicturePause:	'pause.png',
		PictureClose:	'close.png',
		PictureLoading:	'loading.gif',
		rel: 		'clearbox'
	},
	gallery: [],
	initialize: function(options){
		this.setOptions(options);
		this.init();		
	},
	init: function(){ 
		var links = $$('[rel^=' + this.options.rel + ']');
		links.each(function(href){
			var rel = href.getProperty('rel');
			var gallery_name = rel.substring(this.options.rel.length+1,rel.length-1);
			href.addEvent('click',this.openWindow.bindWithEvent(this, gallery_name));
			this.addToGallery(gallery_name, href);
		},this);
		
		if (!$('slideShow'))
			new Element('div',{id:'slideShow'}).inject($(document.body));
	},
	ajax_init: function(id){
		this.gallery = [];
		var obj = $(id);
		var links = obj.getElements('[rel^=' + this.options.rel + ']');
		links.each(function(href){
			var rel = href.getProperty('rel');
			var gallery_name = rel.substring(this.options.rel.length+1,rel.length-1);
			href.addEvent('click',this.openWindow.bindWithEvent(this, gallery_name));
			this.addToGallery(gallery_name, href);
		},this);
	},
	addToGallery: function (gallery_name, href){
		if (!this.gallery[gallery_name]){
			this.gallery[gallery_name] = [];
		}
		this.gallery[gallery_name][(this.gallery[gallery_name]).length] = {href:href.getProperty('href'), img: href.getElement('img').getProperty('src'),title: href.getProperty('title')}
	},
	openWindow: function(e,gallery_name){
		var win =$('slideShow'), table, wi, event, target;
		
		event = new Event(e).stop();
		target = event.target;
		if (target.getTag() != 'A')
			target = target.getParent('a');
		
		wi = '<table cellspacing="0" cellpadding="0" class="ss_window"> <tr class="ss_header"> <td class="ss_topleft"><div class="fix"></div></td> <td class="ss_top"></td> <td class="ss_topright"><div class="fix"></div></td> </tr> <tr class="ss_body"> <td class="ss_left"></td> <td class="ss_content" valign="top" align="left"> <div class="ss_padding"> <div id="ss_imgcontainer"><div class="ss_title"></div>  <img class="ss_image" alt="" src="' + this.options.PicDir + 'blank.gif" /> </div><img class="ss_loading" alt="' + this.options.LoadingText + '" src="' + this.options.PicDir + '/' + this.options.PictureLoading + '" /><div class="fnc_element"><div class="ss_btn_closewindow"></div></div> <div class="image_preview"><div class="image_preview_container"></div></div>  <div class="ss_text"></div> </td> <td class="ss_right"></td> </tr> <tr class="ss_footer"> <td class="ss_btmleft"><div class="fix"></div></td> <td class="ss_btm"></td> <td class="ss_btmright"><div class="fix"></div></td> </tr> </table>';
		win.setHTML(wi);
		
		this.createBackground();
		table = win.getElement('table');
		table_position = this.getWinCenter({x:table.getSize().x, y: table.getSize().y});
		
		table.setStyles({
			visibility: 'visible',
			width:	this.options.BasicWinW,
			height:	this.options.BasicWinH,
			left:	table_position.left,
			top:	table_position.top
				
		});
		
		win.getElement('.ss_btn_closewindow').addEvent('click',function(){win.empty()});
		
		this.preloader = win.getElement('.ss_loading');
		this.preloader.setStyle('visibility','visible');
		
		this.init_small_preview(gallery_name);
		
		this.preview_fx = new Fx.Elements(win.getElement('.image_preview'),{
				onComplete: function(){
					win.getElement('.image_preview').setStyle('visibility','visible');
				}
		});
		this.table_fx = new Fx.Elements(table,{
				onComplete: function(){
					win.getElement('.ss_image').fade(1);
					win.getElement('.ss_image').setStyle('display','block');
					win.getElement('.image_preview').setStyle('display','block')
				}
		});
		
		new Element('a',{href:'#',title:'Další fotografie'}).inject(win.getElement('.fnc_element')).addClass('ss_btn_next').addEvents({
				'click': (function(e){this.goNext(e)}).bind(this),
				'mouseover': function(){this.addClass('hover')},
				'mouseout': function(){this.removeClass('hover')}
		});
		new Element('a',{href:'#',title:'Předchozí fotografie'}).inject(win.getElement('.fnc_element')).addClass('ss_btn_prev').addEvent('click', (function(e){this.goPrev(e)}).bind(this));

		this.loadImage(e,target.getProperty('href'),target.getProperty('title'));
		
	},
	goNext:function(e){
		var next = this.e_next();
//		alert(next);
		if (next) {
			this.loadImage(e, next.getProperty('href'),next.getProperty('title'));	
		}
	},
	goPrev:function(e){
		var prev = this.e_prev();
		if (prev) {
			this.loadImage(e, prev.getProperty('href'),prev.getProperty('title'));	
		}
	},
	
	loadImage: function(e, href,title){
		var win =  $('slideShow'), table = win.getElement('table'), small_prev = $('slideShow').getElement('.image_preview_container'), selected_prev = small_prev.getElement('.selected_prev');
		if (selected_prev) selected_prev.removeClass('selected_prev');
		//win.getElement('.ss_image').setStyle('display','none');
		win.getElement('.ss_image').fade(0);
		win.getElement('.image_preview').setStyle('display','none');
		this.preloader.setStyle('display','block');

		small_prev
			.setStyles({
					opacity:0,
					visibility:'visible'
			})
			.addEvents({
					'mouseover':function(){this.fade(1);},
					'mouseout':function(){this.fade(0.01);}
			});
		new Event(e).stop();
		
		temp_image = new Image();
		new ImageObject(href,{
			onComplete: (function(img){
				var position = this.getWinCenter({x:img.width + 24, y:img.height + 24});
				win.getElement('.ss_image').setProperty('src',href);
				//alert(title);
				
				this.table_fx.start({0: {
					width:[table.getStyle('width').toInt(),img.width + 24],
					height:[table.getStyle('height').toInt(), img.height + 24],
					left: [table.getStyle('left').toInt(), position.left],
					top:[table.getStyle('top').toInt(), position.top]
				}});
				
				// definice velikosti
				preview = $('slideShow').getElement('.image_preview');
				preview.setStyle('width',img.width);
				
				win.getElement('.image_preview').setStyle('display','block');
				if (Browser.Engine.trident)
					scrollsize = preview.getElement('div').getScrollSize().x
				else 
					scrollsize = preview.getScrollSize().x
				dif_x = (scrollsize - img.width);
				pomer =  (img.width / 2) / dif_x ;
				win.getElement('.image_preview').setStyle('display','none');

				preview.addEvent('mousemove',function(e){
						var event = new Event(e);
						mouse_x = event.client.x - table.getStyle('left').toInt() - 12;
						mouse_x = this.getSize().x / 2 - mouse_x;
						if (mouse_x < 0) {
							this.getElement('.image_preview_container').setStyle('position','relative').setStyle('left', mouse_x / pomer );
						} else {
							this.getElement('.image_preview_container').setStyle('left',0);	
						}
					});
				this.preloader.setStyle('display','none');
				if (title!=null)
					win.getElement('.ss_title').setHTML(title);
				else
					win.getElement('.ss_title').setStyle('display','none')
			}).bind(this)
		});	
		
		if (small_prev){small_prev.getElement('a[href=' + href + ']').addClass('selected_prev');}
		
		if (!this.e_next()) win.getElement('.ss_btn_next').setStyle('display','none');
		else win.getElement('.ss_btn_next').setStyle('display','block');
		
		if (!this.e_prev()) win.getElement('.ss_btn_prev').setStyle('display','none');
		else win.getElement('.ss_btn_prev').setStyle('display','block');
			
	},
	e_next: function(){
		var small_prev = $('slideShow').getElement('.image_preview_container'), selected_prev = small_prev.getElement('.selected_prev');
		
		if (selected_prev && selected_prev.getNext('a'))
			return selected_prev.getNext('a');
		else
			return false;
	},
	e_prev: function(){
		var small_prev = $('slideShow').getElement('.image_preview'), selected_prev = small_prev.getElement('.selected_prev');
		if (selected_prev && selected_prev.getPrevious('a'))
			return selected_prev.getPrevious('a')
		else
			return false;
	},
	init_small_preview: function(gallery_name){
		var current_gallery = this.gallery[gallery_name], small_prev = $('slideShow').getElement('.image_preview_container');
		current_gallery.each(function(item){
				new Element('img',{src:item.img})
					.setProperties({
						'height':40,
						'width':40
					})
					//.setStyle('height','40px')
					.addClass('prev')
					.inject(
						new Element('a',{href:item.href,title:item.title})
							.addClass('prev')
							.inject(small_prev)
							.addEvent('click',this.loadImage.bindWithEvent(this,[item.href,item.title]))
						);
						
		},this);
		
	},
	getWinCenter: function(obj){
		wp = window.getSize();
		
		nx =  wp.x/2 - obj.x/2;
		ny = wp.y/2 - obj.y/2;
		
		return {
			left: (nx < 5)?5:nx,
			top: (ny<5)?5+ window.getScroll().y:ny + window.getScroll().y
		} 
	},
	createBackground: function(){
		new Element('div')
			.addClass('ss_background')
			.inject($('slideShow'))
			.setStyles({
				width:		window.getScrollSize().x, 
				height:		window.getScrollSize().y,
				background: 	this.options.HideColor,
				opacity:	this.options.HideOpacity
			
			})
			.addEvent('click',function(){$('slideShow').empty()});
	}
});
window.addEvent('load',function(){document.slideshow = new slideShow();});