/************************************/
/* TODOS
/* uprava pro nahravani povolenych souboru .... DONE
/* moznost vymazat soubor pri odstraneni ze seznamu ... PRCAT
/* zmenseni a vytvoreni kopii ... DONE
/* generovani unikatnich nazvu souboru ... DONE
/************************************/
var lang_smazat = 'Smazat';
var lang_soubor = 'Soubor'

//function info(text){
	//if (console) console.info(text);
//}

var uploader = new Class({
	Implements:[Options,Events],
	check_size: false,
	options:{
		id				: '',
		paths			: {
			path_to_file	: 'http://testftp.fastest.cz/',
			path_to_ftp		: '/',
			status_path		: '/ftp/get_status/',
			upload_script	: '/ftp/upload_file',
			file_exist		: '/file_exist/'
		},
		upload_type		: 'ftp',
		db_element		: 'data[Model][file]',
		onComplete		: $empty,
		onDelete		: $empty,
		filename_type	: 'unique', //unique|alias|name
		filename		: '',
		methods			: {},
		file_ext		: ['pdf','jpg','zip', 'rar', 'txt', 'doc','odt','html','xml','ods','xls','xlt','docx','xlsx','pps','ppsx','ppt'],
		check_exist_file: false,
		autoupload		: true
		
	},
	check_exist_file: function(){
		switch (this.options.filename_type){
			case 'unique':
				return true;
				break;
			case 'alias': case 'name':
				return true;
				break;
			default:
				return false;
				break;
		}
		return false;
	},
	go_upload: function(){
		var upload_div, upload_iframe, id = $(this.options.id + 'Uploader'), object;
		id.getElement('.input_file_over').addClass('progress');
		id.getElements('.browse_icon, .seznam_icon, .upload_icon, .smazat_icon').setStyle('display','none');
		id.getElement('.stop_upload_icon').setStyle('display','inline');
		upload_div = new Element('div',{id:'upload_div_process'}).inject($('addon'));
		upload_iframe = new Element('iframe',{'name':'upload_iframe', 'src':'about:blank'}).inject(upload_div);
		upload_iframe.addEvent('load',(function(e,object){
			if ($('upload_div_process')){
				object = $('upload_div_process').getElement('iframe');
				if ($(object.contentWindow.document.body).innerHTML != ''){
                    //console.log($(object.contentWindow.document.body).innerHTML);
					this.upload_done(JSON.decode($(object.contentWindow.document.body).innerHTML));
				}
			}
		}).bind(this,upload_iframe));
			
		var upload_form = new Element('form',{
			action	:	this.options.paths.upload_script,
			id		:	this.options.id + 'Form',
			method	:	"POST",
			enctype	:	"multipart/form-data",
			encoding:  "multipart/form-data",
			target	:	"upload_iframe"
		}).inject(upload_div);

		input_file = id.getElement('.input_file');
		upload_form.adopt(input_file);
		
		upload_file = upload_form.getElement('input[type=file]');
			
		if(id.getElement('.input_file')) id.getElement('.input_file').dispose();

		if (this.options.data){
			var temp = {};
			$each(this.options.data, function(obj){
				if ($(obj)) temp[obj] = $(obj).value;
			});
			this.options.data2 = temp;
		}
				
		new Element('input',{type:'hidden',name:'data[upload][setting]', value: JSON.encode(this.options)}).inject(upload_form);
			
		
			
		id.getElement('.progress_div').setStyle('display','block');
		id.getElement('.progress_div').style.zIndex = getHighestIndex();
		id.getElement('.progress_div').getElement('.upload_title').getElement('span').setHTML(id.getElement('.input_file_over').value);
		//alert(upload_form.enctype);
		this.check_size = true;
		upload_form.submit();
		
		this.tmp_file = '';
		this.date = new Date();
		this.start_time = 0;
		this.last_upload_size = 0;
		this.upload_size = 0;
		this.upload_time = 0;
		this.i = 0;
		(function(){this.get_progress_data()}).delay(1000,this);
	},
	
	initialize: function(options){
		this.setOptions(options);
		//console.log(this.options); 
		if (options.onComplete) this.onComplete = options.onComplete;
		
		var id = $(this.options.id + 'Uploader'), input_file;	
		input_file = id.getElement('.input_file');
		
		// udalost pri zmene, respektive vybrani souboru
		input_file.addEvent('change',this.onchange_input.bind(this));
		
		// schovani btns
		$$('.smazat_icon, .upload_icon, .upload_icon, .stop_upload_icon, .nahled_icon').setStyle('display','none');
				
		// udalost pri smazani vybraneho souboru
		id.getElement('.smazat_icon').addEvent('click',function(e){
			// Nastaveni ikon
			// Zobrazeni na pozadi fileinput
			// Vymazani fake fileinputu
			id.getElements('.browse_icon, .seznam_icon').setStyle('display','inline');
			id.getElements('.upload_icon, .smazat_icon').setStyle('display','none');
			id.getElement('.input_file_over').value = '';
			id.getElement('.input_file').setStyle('display','block');
		});
		
		id.getElement('.upload_icon').addEvent('click',this.go_upload.bind(this));
		
		
		id.getElement('.stop_upload_icon').addEvent('click',(function(){
			$('upload_div_process').getElement('iframe')
				.removeEvents('load')
				.setProperty('src','about:blank');
			id.getElement('.input_file_over').removeClass('progress');
			id.getElement('.browse_icon').setStyle('display','inline');
			
			id.getElement('.stop_upload_icon').setStyle('display','none');
			id.getElement('.upload_icon,.smazat_icon').setStyle('display','none');
			id.getElement('.input_file_over').value = '';
		
			new Element('input',{type:'file','class':"input_file", title:"", name:"upload_file",value:''})
				.inject(id.getElement('.input_file_over'),'after')
				.addEvent('change',this.onchange_input.bind(this));
		
			id.getElement('.progress_div').setStyle('display','none');
		
			$('upload_div_process').dispose();
		}).bind(this));		
	},
	
	onchange_input:function(){
		// ziskani nazvu souboru
		var id = $(this.options.id + 'Uploader'), filename, flnms, ext;
		filename = id.getElement('.input_file').value.split('\\');
		filename = filename[filename.length-1];
		
		// overeni povolene pripony souboru
		flnms = filename.split('.');
		ext = flnms[flnms.length-1];
		if (this.options.file_ext.indexOf(ext.toLowerCase()) != -1){
			id.getElement('.input_file_over').value = filename;
			// Upraveni ikon
			id.getElements('.input_file, .browse_icon, .seznam_icon').setStyle('display','none');
			id.getElements('.upload_icon, .smazat_icon').setStyle('display','inline');
			
			if (this.options.check_exist_file == true){
				new Request.JSON({
					url: this.options.paths.file_exist,
					data: {
						path: '/' + this.options.paths.path_to_file,
						filename: filename
					},
					onComplete: function(json){
						if (json.result == false){
							if (!confirm('Daný soubor již na disku existuje, přejete si jej přehrát?'))
								id.getElement('.smazat_icon').fireEvent('click');
						}
					}
				}).send();
			}
			this.go_upload();
		} else {
			alert('Nepovolený typ souboru');
		}
	},
	
	upload_done:function(json){
		this.check_size = false; 
		var id = $(this.options.id + 'Uploader');
		//id.getElement('.input_file_over').removeClass('progress');
		
		id.getElements('.upload_icon, .smazat_icon, .stop_upload_icon, .progress_div').each(function(it){
			it.setStyle('display','none');
		});
				
		// Znovu vytvoreni fileinputu
		new Element('input',{type:'file','class':"input_file", title:"", name:"upload_file",value:''})
			.inject(id.getElement('.input_file_over'),'after')
			.addEvent('change',this.onchange_input.bind(this));
		// odstraneni uploadovaciho iframe		
		$('upload_div_process').getElement('iframe').setProperty('src','about:blank');
		$('upload_div_process').dispose();

        if(json.message){
            alert(json.message);
        }
        else{
    		id.getElement('.input_file').setStyle('display','none');
    		id.getElement('.input_file_over').value = json.upload_file.name;
    		id.getElement('.db_element').value = json.upload_file.name;
    		id.getElements('.smazat_icon').setStyle('display','inline');
    		//id.getElement('.solo_preview').setProperties({src:this.options.paths.path_to_file + json.upload_file.name,alt:json.upload_file.name,title:json.upload_file.name});
    		
    		//console.log(eval('' + this.onComplete));
    		eval('' + this.onComplete + '("' + json.upload_file.name + '")');
    		//if (this.onComplete) eval(this.onComplete(json.upload_file.name));
		}
	},
	
	get_progress_data: function(){
		this.date = new Date();
		this.start_time = this.date.getTime();
		this.json = new Request.JSON({
			url : this.options.paths.status_path + '?file=' + this.tmp_file,
			onCancel: (function(){
				delete this.json;
			}).bind(this),
			onComplete: (function(jsonObj) {
				this.i++;
				this.date = new Date();
				this.end_time = this.date.getTime();
				this.response_time = this.end_time - this.start_time;
				if (jsonObj.upload_info.size == 'done'){
					resume = false;
					 //console.log('Soubor byl nahran');
				} else {
					this.response_size = jsonObj.upload_info.size - this.last_upload_size;
					this.last_upload_size = jsonObj.upload_info.size;
					this.upload_size += this.response_size;
					this.upload_time += this.response_time;
					// console.info(Math.round(jsonObj.upload_info.size/1024) + 'Kb | ' + Math.round(this.response_size/1024) + 'Kb/s | ' + Math.round(this.upload_size/this.i/1024) + 'Kb/s');
					
					var progress_time = $(this.options.id + 'Uploader').getElement('.progress_time');
					var ems = progress_time.getElements('em');
															
					ems[0].setHTML(Math.round(this.response_size/1024) + 'Kb/s');
					ems[1].setHTML(Math.round(this.upload_size/this.i/1024) + 'Kb/s');
					ems[2].setHTML(Math.round(jsonObj.upload_info.size/1024) + 'Kb');
					this.tmp_file  = jsonObj.upload_info.file;
					resume = true;
				}
				if (this.check_size && resume == true) this.get_progress_data();
			}).bind(this)
		}).send();
	}
});