var predefinedReq = {
    not_empty:'[\\S]+(.*)$',
    //not_empty	: 	'^([a-zA-Z0-9ěščřžýáíéúůďťĚŠČŘŽÝÁÍÉÚŮĎŤ]{1})+([a-zA-Z0-9 ěščřžýáíéúůďťĚŠČŘŽÝÁÍÉÚŮĎŤ]?)+$',

    // vypnuto - 1.3.10
    //not_empty	: 	'^([a-zA-Z0-ěščřžýäáíĺŕéňúůďťĚŠČŘŽÝÁÍÉÚŮĎŤľĽ\+Ú]{1})+([a-zA-Z0-9\- %&\n##,/\+_<br /><p></p><br />!ěščřžýäáíĺŕéňúůďťĚŠČŘŽÝÁÍÉÚŮĎŤľĽ\.,\?\"()@)]?)+$',
    email:'^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$',
    password:'^(?=.*\d)(?=.*[a-zA-Z])(?!.*[\W_\x7B-\xFF]).{6,15}$' //Password validator Requires 6-20 characters including at least 1 upper or lower alpha, and 1 digit. It should disallow just about everything else, inluding extended characters.
}
var validation = new Class({
    predefined:{
        testReq:function (object, arguments) {
            if (predefinedReq[arguments]) {
                return (object.value.test(predefinedReq[arguments]));
            } else {
                return (object.value.test(arguments));
            }
        },
        isConfirm:function (object, arguments) {
            return (object.value == $(arguments.secObject).value);
        },
        not_equal:function (object, arguments) {
            return (object.value != arguments);
        },
        length:function (object, arguments) {
            var value = object.value;
            if (arguments.min == null) {
                if (value.length < arguments.min) {
                    return false;
                } else {
                    return true;
                }
            } else if (arguments.max == null) {
                if (value.length > arguments.max) {
                    return false;
                } else {
                    return true;
                }
            } else {
                if (value.length < arguments.min || value.length > arguments.max) {
                    return false;
                } else {
                    return true;
                }
            }
        },
        isUnique:function (object, arguments) {
            var value = object.value;
            object.removeClass('invalid');
            object.removeClass('require');
            object.removeClass('valid');
            object.addClass('wait');
            var id = '';
            if (arguments.id)
                id = arguments.id;

            new Request.JSON({
                url:'/pages/isUnique/' + arguments.model + '/' + arguments.field + '/' + value + '/' + id,
                onComplete:function (json) {
                    if (json['return'] == true) {
                        object.removeClass('invalid');
                        object.removeClass('require');
                        object.removeClass('wait');
                        object.addClass('valid');
                    } else {
                        object.removeClass('valid');
                        object.removeClass('require');
                        object.removeClass('wait');
                        object.addClass('invalid');
                        object.valid_error = fnc_string;
                    }
                }
            }).send();
            return null;
        },
        isChecked:function (object, arguments) {
            return object.checked;
        }
    },

    val:{},
    stav:false,
    valideForm:function (id_form) {
        var element = $(id_form);
        not_valid_el = element.getElements('.invalid, .require');
        if (not_valid_el.length == 0)
            return true;
        else {
            errors = [];
            not_valid_el.each(function (ivi) {
                errors[errors.length] = ivi.err_message['type_' + ivi.valid_error];
            });
            return errors;
        }
    },
    define:function (id_form, options) {
        if (!this.val[id_form]) this.val[id_form] = {};
        $extend(this.val[id_form], options);
    },
    remove:function (id_form, name) {
        this.val[id_form][name] = null;
        delete this.val[id_form][name];
        node = $(name);
        event_ = Array();
        if (node.nodeName == 'INPUT' && (node.getProperty('type') == 'text' || node.getProperty('type') == 'password')) {
            event_.push('keyup');
            event_.push('blur');
        } else if (node.nodeName == 'INPUT' && node.getProperty('type') == 'checkbox') {
            event_.push('click');
        } else if (node.nodeName == 'INPUT' && node.getProperty('type') == 'hidden') {
            event_.push('zmena');
        } else if (node.nodeName == 'SELECT') {
            event_.push('change');
        } else if (node.nodeName == 'TEXTAREA') {
            event_.push('keyup');
        }

        node.removeEvents('keyup');
        node.removeEvents('blur');
        node.removeEvent('keyup',validation.validation_event);
        node.removeEvent('blur',validation.validation_event);


    },
    setStav:function (stav) {
        if (stav) this.stav = stav;
    },
    show:function () {
        alert(JSON.encode(this.val));
    },
    generate:function (id_form, edit) {
        if (edit == true) className = 'valid'; else className = 'require';
        $each(this.val[id_form], function (items, id) {
            node = $(id);
            if (node) {
                node.err_message = [];
                start_valid = false;
                $each(items, function (item, fnc) {
                    if (start_valid == false) start_valid = fnc;
                    node.err_message['type_' + fnc] = item['err_message'];
                    node.stav = false;
                    if (item['stav'])
                        node.stav = item['stav'];
                });

                /**
                 * Pokud existuje nastaveni stavu
                 * tak budeme validovat jen ty co maji tento stav
                 * a nebo nemaji nastaveny zaden stav(plati tedy vzdy)
                 */
                if ((this.stav === false || node.stav === false)
                    ||
                    ( this.stav !== false && node.stav !== false && node.stav == this.stav)
                    ) {

                    event_ = Array();
                    if (node.nodeName == 'INPUT' && (node.getProperty('type') == 'text' || node.getProperty('type') == 'password')) {
                        event_.push('keyup');
                        event_.push('blur');
                    } else if (node.nodeName == 'INPUT' && node.getProperty('type') == 'checkbox') {
                        event_.push('click');
                    } else if (node.nodeName == 'INPUT' && node.getProperty('type') == 'hidden') {
                        event_.push('zmena');
                    } else if (node.nodeName == 'SELECT') {
                        event_.push('change');
                    } else if (node.nodeName == 'TEXTAREA') {
                        event_.push('keyup');
                    }

                    $each(event_, function (ev) {
                        node
                            .addEvent(ev, this.validation_event.bindWithEvent(this, items))
                            .addClass(className);

                    }, this)
                    node.valid_error = start_valid;
                }
            }
        }, this);
    },

    validation_event:function (e, items) {
        var target, parameters, fnc_string, func_return;
        if ($type(e) == 'element')
            target = e;
        else
            target = new Event(e).target;
        for (var fnc in items) {
            parameters = items[fnc]['condition'];
            var stav = false;
            if (items[fnc]['stav']) {
                stav = items[fnc]['stav'];
            }

            if ((this.stav === false || stav === false)
                ||
                ( this.stav !== false && stav !== false && stav == this.stav)
                ) {
                fnc_string = fnc;
                fnc = eval(this.predefined[fnc]);
                func_return = fnc(target, parameters);
                if (func_return == true) {
                    target.removeClass('invalid');
                    target.removeClass('require');
                    target.addClass('valid');
                } else if (func_return == false) {
                    target.removeClass('valid');
                    target.removeClass('require');
                    target.addClass('invalid');
                    target.valid_error = fnc_string;
                    break;
                } else {
                    target.removeClass('valid');
                    target.removeClass('require');
                    target.removeClass('invalid');
                }
            }
        }
    },
    check_form:function (id_form) {
        $each(this.val[id_form], function (items, id) {
            var node = $(id);
            if (node) {
                this.validation_event(node, items);
            }
        }, this)
    }


});
validation = new validation();