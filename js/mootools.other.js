function str_replace(search, replace, subject, count) {
	var i = 0, j = 0, temp = '', repl = '', sl = 0, fl = 0, f = [].concat(search), r = [].concat(replace), s = subject, ra = r instanceof Array, sa = s instanceof Array;
    s = [].concat(s);
    if (count) {
        this.window[count] = 0;
    }
 
    for (i=0, sl=s.length; i < sl; i++) {
        if (s[i] === '') {
            continue;
        }
        for (j=0, fl=f.length; j < fl; j++) {
            temp = s[i]+'';
            repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
            s[i] = (temp).split(f[j]).join(repl);
            if (count && s[i] !== temp) {
                this.window[count] += (temp.length-s[i].length)/f[j].length;}
        }
    }
    return sa ? s : s[0];
}




// translate text to google transaltor
function translate(translate_to,type,model,translation_model) {
	    
		
		// prepis Spanelstina
		  if (translate_to == 'sp') translate_to = 'es';
		  
		  
		
		  
		  if (!translation_model)
			translation_model = 'Translation';
		  else{
		    var translation_model_tmp = true;
			translation_model = translation_model;
		  }
		  
		  if (type=='Text')
			var value = $('edit_area_cz').innerHTML;
		  //alert(translation_model_tmp);
		  else {  
			if (!translation_model_tmp)
				if ($(translation_model+'Cz'+model+type))
					var value = $(translation_model+'Cz'+model+type).value;
				else
					alert('Nenalezen prvek: ' + translation_model+'Cz'+model+type);
			else 
				if ($(translation_model+'Cz'+type))
					var value = $(translation_model+'Cz'+type).value;
				else
					alert('Nenalezen prvek: ' + translation_model+'Cz'+type);
		  }
		  
		  //Pokud si zvolite moznost na dobirku, budete platit az po doruceni zbozi danou dopravou.
		  
		  
		  
		  var src = 'cs';
	      var dest = translate_to;
	      var delka_prekladu = value.length;
		  var pocet_prekladu = 0;
		  var dolni_mez = 0;
		  var dolni_mez_po = 0;
		  var horni_mez = 0;
		  var rozdelena_value = '';
		  var zbytek_value = '';
		  var tmp_translate = '';
		  var omezeni_google = 450;	
		 
		  pocet_prekladu = delka_prekladu/omezeni_google;
		  pocet_prekladu = Math.floor(pocet_prekladu);
		  
		  var zbytek_dolni_mez = pocet_prekladu * omezeni_google;
		  var zbytek_horni_mez = delka_prekladu;
			  
		 for(c= 0;c<pocet_prekladu; c++){
				horni_mez = slova(value,omezeni_google,c);
			if (dolni_mez_po ==0)
				rozdelena_value = value.substring(dolni_mez,horni_mez);
			else
				rozdelena_value = value.substring(dolni_mez_po,horni_mez);
					if (rozdelena_value !=''){
						  google.language.translate(rozdelena_value, src, dest, 
							function translateResult(result) {
								if (type=='Text'){
									if (result.translation) 
										tmp_translate += ' ' + result.translation;
									else 
										tmp_translate = 'Chyba překladu!';
								} else {
									if (!translation_model_tmp)
										var resultBody = $(translation_model+ucfirst(translate_to)+model+type);
									else
										var resultBody = $(translation_model+ucfirst(translate_to)+type);
									
									if (result.translation)
										resultBody.value += result.translation;
									else 
										resultBody.value = 'Chyba překladu!';
									
								}
							}
						  )
					  }
			dolni_mez_po = horni_mez;
		}
		
		zbytek_value = value.substring(zbytek_dolni_mez,zbytek_horni_mez);
			if (zbytek_value !=''){
				google.language.translate(zbytek_value, src, dest, 
					function translateResult(result) {
						if (type=='Text'){
							if (result.translation) 
								tmp_translate += ' ' + result.translation;
							else 
								tmp_translate = 'Chyba překladu!';
						} else {
								
								if (!translation_model_tmp){
									$(translation_model+ucfirst(translate_to)+model+type).value ='';
									var resultBody = $(translation_model+ucfirst(translate_to)+model+type);
								} else {
									$(translation_model+ucfirst(translate_to)+type).value ='';
									var resultBody = $(translation_model+ucfirst(translate_to)+type);
								}	
								
							
										
							if (result.translation)
								resultBody.value += result.translation;
							else 
								resultBody.value = 'Chyba překladu!';
						}
					}
				)
			}
		
		if (type == 'Text'){
			alert('Text byl přeložen');
			$('edit_area_'+translate_to).innerHTML = tmp_translate;
		}
	}

//prevod prvniho na velke
function ucfirst (str) {
	str += '';
	var f = str.charAt(0).toUpperCase();
	return f + str.substr(1);
}

// zjisteni postu slov    	
function slova(value,omezeni_google,c){
	var tmp_vypocet = omezeni_google * c + omezeni_google;
	if (value.charAt(tmp_vypocet) == ' ')
		return tmp_vypocet;
	else return slova(value,omezeni_google+1,c);
}

function filebrowser_preloader() {
	el=$('filebrowser_preloader').style;
	el.display=(el.display == 'block')?'none':'block';

}


function button_preloader(id){
		id.addClass('button_preloader');
		id.setProperty('disabled');
}
function button_preloader_disable(id){
		id.removeClass('button_preloader');
		id.removeProperty('disabled');
}

function uniqid( prefix, more_entropy) {
     if(typeof prefix == 'undefined') {
        prefix = "";
    }
 
    var retId;
    var formatSeed = function(seed, reqWidth) {
        seed = parseInt(seed,10).toString(16); 
        if (reqWidth < seed.length) { 
            return seed.slice(seed.length - reqWidth);
        }
        if (reqWidth > seed.length) { 
            return Array(1 + (reqWidth - seed.length)).join('0')+seed;
        }
        return seed;
    };
 
    if (!this.php_js) {
        this.php_js = {};
    }
    if (!this.php_js.uniqidSeed) { // init seed with big random int
        this.php_js.uniqidSeed = Math.floor(Math.random() * 0x75bcd15);
    }
    this.php_js.uniqidSeed++;
 
    retId  = prefix; // start with prefix, add current milliseconds hex string
    retId += formatSeed(parseInt(new Date().getTime()/1000,10),8);
    retId += formatSeed(this.php_js.uniqidSeed,5); // add seed hex string
 
    if(more_entropy) {
        // for more entropy we add a float lower to 10
        retId += (Math.random()*10).toFixed(8).toString();
    }
 
    return retId;
}


function getHighestIndex(){				
		var allElems = document.getElementsByTagName?document.getElementsByTagName("*"): document.all; 
		var maxZIndex = 0;
		for(var i=0;i<allElems.length;i++) {
			var elem = allElems[i];
			var cStyle = null;
			if (elem.currentStyle) {cStyle = elem.currentStyle;
			} else if (document.defaultView && document.defaultView.getComputedStyle){
				cStyle = document.defaultView.getComputedStyle(elem,"");
			}
			var sNum;
			if (cStyle) {
				sNum = Number(cStyle.zIndex);
			} else {
				sNum = Number(elem.style.zIndex);
			}
			if (!isNaN(sNum)) {
				maxZIndex = Math.max(maxZIndex,sNum);
			}
		}
		return maxZIndex;
} 
Element.implement({
	ajaxLoad: function(url, children, url_add){
		var empty_value = '--- Zvolte hodnotu ---';
        var url_param = '';
        
        if(typeof url_add != 'undefined')
       	    url_param = url_add;
            
        this.addEvent('change', function(){
			new Request.JSON({
				url: url + this.value + url_param,
				onComplete: (function(json){
				    if(json != false){
    					children.each(function(obj){
    						$(obj).empty();
    						new Element('option', {title:empty_value, value:''}).setHTML(empty_value).inject($(obj));
    					});
    					child = children[0];
 
        				$each(json, function(value, id){
        					new Element('option', {title:value, value:id}).setHTML(value).inject($(child));
        				}, this);
	
                    }
                    else{
                        /**
                         * vyresetuj pokud se jedna o prazdne pole
                         */
                   	    child = children[0];
                        $(child).empty();
                        new Element('option', {title:empty_value, value:''}).setHTML(empty_value).inject($(child));
                    }                     
				}).bind(this)
			}).send();
		});
	},
	
	getOptionText: function(){
		return this.options[this.selectedIndex].getHTML();
	}
});
String.implement({
	camelCase2: function(){
		return this.replace(/_\S/g, function(match){
			return match.charAt(1).toUpperCase().capitalize();
		});
	}
});
Element.implement({
	inputLimit: function(){
		if (this.hasClass('float')){
			this.addEvent('keypress',function(e){
				var event = new Event(e); 
				var allow_key = ['1','2','3','4','5','6','7','8','9','0',',','backspace','left','right','tab','f5','delete'];	
				if (allow_key.indexOf(event.key) != -1) 
					return true; 
				else 
					return false;			
			});
		} else if (this.hasClass('integer')){
			this.addEvent('keypress',function(e){
				var event = new Event(e); 
				var allow_key = ['1','2','3','4','5','6','7','8','9','0','backspace','left','right','tab','f5','delete'];	
				if (allow_key.indexOf(event.key) != -1) 
					return true; 
                else if(event.key == ','){
                    this.value += '.';
                    return false;   
                }        
				else 
					return false;			
			});
		}
	}
});


var ThunderBird = new Class({
    //Implements: [Chain, Events, Options],
    options: {
        items_id: "items",
        thunder_id: "ThunderBirdBox",
        items_id_size: 126,//sirka items
    },
    initialize: function () {
         this.init_rows();
    },
    init_rows: function(){
        var tb = this.options.thunder_id;
        var it = $(this.options.items_id);
        var size = this.options.items_id_size;        
    
        $(this.options.items_id).getElement('tbody').getElements('tr.thunderbird').addEvent('click',function(e){
            e.stop();
            	new Request.HTML({
    				url: this.getElement('a.edit').getProperty('href'),
                    update:tb,
    				onComplete: function(){
    				    it.getElement('tbody').setStyle('height',size).setStyle('overflow-y','auto').setStyle('overflow-x','hidden');
                        $(tb).getElement('div.formular_action').empty();
                        var el = new Element('input',{'type':'button','value':'Zavřít','style':'position:absolute; right:10px; z-index:10;'});
    				    el.addEvent('click',function(e){e.stop(); $(tb).empty(); it.getElement('tbody').removeProperty('style');});
                        el.inject($(tb),'top');
                    }
                }).send();    
        })
    }
});


var FormHint = new Class({
    options: {},
    initialize: function () {
         this.initRowHints();
    },
    initRowHints:function(){
            var forms = document.getElementsByTagName('form');
            if(!forms ) return null;
            var hintElements;
            
            for (var i=0; i<forms.length; i++){
                    hintElements = forms[i].getElements('p.row-hint');
                    
                    for(var k=0; k<hintElements.length; k++){
                        hintElements[k].addEvents({
                            'mouseover': this.showRowHint.bindWithEvent(this),
                            'mouseout': this.hideRowHint.bindWithEvent(this)
                        })

                        var id_hint = hintElements[k].getProperty('id');
                        var element_id = str_replace('-hint','',hintElements[k].getProperty('id'));
                        /**
                         * Znamena ze pozadujeme zobrazeni helpu i pri najeti na dany element
                         * ke kteremu patri napoveda
                         */
                        if($(element_id).hasClass('hint-focus')){
                            if($(element_id).nodeName == "INPUT" && $(element_id).getProperty('type') == 'checkbox'){
                                $(element_id).addEvents({
                                    'mouseover': this.showRowHint.bindWithEvent(this,hintElements[k]),
                                    'mouseout':this.hideRowHint.bindWithEvent(this,hintElements[k])
                                })
                            }
                            else{
                                $(element_id).addEvents({
                                    'focus': this.showRowHint.bindWithEvent(this,hintElements[k]),
                                    'blur': this.hideRowHint.bindWithEvent(this,hintElements[k])
                                })
                            }
                        }    
                     
                        
                        //checkbox - mouseover,mouserout    
                    }
            }
    },
    showRowHint:function(e,hint){
        if ($type(e) == 'element')
			target = e;
        else if (typeof hint != 'undefined')
			target = hint;    
		else
			target = new Event(e).target;
        
        target.addClass("row-hint-active");    
    },
    hideRowHint:function(e,hint){
        if ($type(e) == 'element')
			target = e;
        else if (typeof hint != 'undefined')
			target = hint;     
		else
			target = new Event(e).target;
            
        target.removeClass("row-hint-active");
    } 
})
